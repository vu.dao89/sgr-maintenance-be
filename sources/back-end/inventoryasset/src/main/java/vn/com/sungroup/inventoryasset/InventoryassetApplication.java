/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset;

import com.github.cloudyrock.spring.v5.EnableMongock;
import com.mindscapehq.raygun4java.core.RaygunClient;
import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import vn.com.sungroup.inventoryasset.config.CORSFilter;

import java.util.Locale;

/** InventoryassetApplication class. */
@EnableMongock
@SpringBootApplication
@EnableResourceServer
//@Import({CORSFilter.class})
@EnableScheduling
public class InventoryassetApplication {

    public static void main(String[] args) throws Throwable {
//        Thread.setDefaultUncaughtExceptionHandler(new RaygunExceptionHandler());
        Thread.setDefaultUncaughtExceptionHandler(new SentryExceptionHandler());
        SpringApplication.run(InventoryassetApplication.class, args);
    }

}

class RaygunExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Value("${raygun.key:pDA0JLDzDsiN8giw2ng}")
    private String raygunKey;
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        RaygunClient client = new RaygunClient(raygunKey);
        client.send(e);
    }
}

class SentryExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Sentry.captureException(e);
    }
}

@Configuration
class MessageConfig implements WebMvcConfigurer {

    @Bean("messageSource")
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("language/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        slr.setLocaleAttributeName("current.locale");
        slr.setTimeZoneAttributeName("current.timezone");
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("language");
        return localeChangeInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
}

package vn.com.sungroup.inventoryasset.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import com.mongodb.client.model.IndexOptions;
import org.bson.Document;
import vn.com.sungroup.inventoryasset.mongo.ActivityTypeMg;
import vn.com.sungroup.inventoryasset.mongo.EquipmentCategoryMg;
import vn.com.sungroup.inventoryasset.mongo.EquipmentMg;
import vn.com.sungroup.inventoryasset.mongo.WorkOrderTypeMg;

@ChangeLog(order = "001")
public class InitChangeLog {

  @ChangeSet(id = "001", order = "001", author = "001")
  public void dataInitializer(MongockTemplate mongockTemplate) {
    mongockTemplate.createCollection(EquipmentMg.class)
        .createIndex(new Document("equipmentId", 1), new IndexOptions().unique(true));
  }

  @ChangeSet(id = "002", order = "002", author = "002")
  public void createActivityTypeCollection(MongockTemplate mongockTemplate) {
    mongockTemplate.createCollection(ActivityTypeMg.class);
  }

  @ChangeSet(id = "003", order = "003", author = "003")
  public void createEquipmentCategoryCollection(MongockTemplate mongockTemplate) {
    mongockTemplate.createCollection(EquipmentCategoryMg.class);
  }

  @ChangeSet(id = "004", order = "004", author = "004")
  public void createWorkOrderTypeMgCollection(MongockTemplate mongockTemplate) {
    mongockTemplate.createCollection(WorkOrderTypeMg.class);
  }

}
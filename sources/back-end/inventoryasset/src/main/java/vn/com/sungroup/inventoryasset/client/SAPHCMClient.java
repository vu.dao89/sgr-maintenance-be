package vn.com.sungroup.inventoryasset.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.com.sungroup.inventoryasset.exception.ExternalApiException;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class SAPHCMClient {

    private static final String GET_METHOD = "GET";
    // User agent default system
    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)"
                    + " Chrome/54.0.2840.99 Safari/537";

    @Value("${external.rest.consume.SAP_HCM}")
    private String apiSAP_HCM;

    @Value("${external.rest.consume.SAP_HCM_USER}")
    private String externalSAPHcmUser;

    @Value("${external.rest.consume.SAP_HCM_PASS}")
    private String externalSapHcmPass;

    public <T> T call(
            String path,
            Map<String, Object> requestParam,
            Object requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeaders,
            Class<T> clazz,
            HttpMethod method,
            long timeOut,
            int retryTimes
    )
            throws ExternalApiException {
        String url = String.format(apiSAP_HCM + path);

        log.debug(
                "START: CALLEXTERNALAPI with url={}, requestParam={}, requestBody={},"
                        + " pathVariable={}, mapHeaders={}, Method={}",
                url,
                requestParam,
                requestBody,
                pathVariable,
                mapHeaders,
                method);

        RestTemplate restTemplate =
                new RestTemplateBuilder()
                        .setConnectTimeout(Duration.ofMillis(timeOut))
                        .setReadTimeout(Duration.ofMillis(timeOut))
                        .build();

        HttpHeaders headers = new HttpHeaders();
        String encoding =
                "Basic "
                        + Base64.getEncoder()
                        .encodeToString((externalSAPHcmUser + ":" + externalSapHcmPass).getBytes());
        mapHeaders.put("Authorization", encoding);
        mapHeaders.put("User-Agent", USER_AGENT);
        for (Map.Entry<String, String> map : mapHeaders.entrySet()) {
            headers.set(map.getKey(), map.getValue());
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (mapHeaders.get("Authorization") != null) {
            if (mapHeaders.get("Authorization").contains(" ")) {
                String authType = mapHeaders.get("Authorization").split(" ")[0];
                String credential = mapHeaders.get("Authorization").split(" ")[1];
                if (authType.equals("Bearer")) {
                    headers.setBearerAuth(credential);
                } else {
                    headers.setBasicAuth(credential);
                }
            } else {
                headers.setBearerAuth(mapHeaders.get("Authorization"));
            }
        }
        headers.set("User-Agent", USER_AGENT);
        HttpEntity<Object> request = new HttpEntity<>(requestBody, headers);

        if (GET_METHOD.equalsIgnoreCase(method.name())) {
            request = new HttpEntity<>(headers);
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
        uriBuilder.encode(StandardCharsets.UTF_8);
        if (null != requestParam && !requestParam.isEmpty()) {
            for (Map.Entry<String, Object> map : requestParam.entrySet()) {
                if (null != requestParam && !requestParam.isEmpty()) {
                    String key = map.getKey();
                    if (map.getValue() instanceof List) {
                        for (Object o : (List<?>) map.getValue()) {
                            if (o != null) {
                                uriBuilder.queryParam(key, o);
                            }
                        }
                    } else if (map.getValue() != null) {
                        uriBuilder.queryParam(key, map.getValue());
                    }
                }
            }
        }
        int retryCount = 0;
        boolean flgSuccess = false;
        while (!flgSuccess && retryCount <= retryTimes) {
            try {
                ResponseEntity<T> response =
                        restTemplate.exchange(
                                uriBuilder.buildAndExpand(pathVariable).toUri(),
                                method,
                                request,
                                clazz);
                if (HttpStatus.OK == response.getStatusCode()) {
                    flgSuccess = true;
                    return response.getBody();
                }
            } catch (RuntimeException e) {
                if (retryCount == retryTimes) {
                    throw new ExternalApiException("Error External Api Timeout", e);
                }
                log.debug("RETRY NUMBER: {}", retryCount + 1);
            } finally {
                if (flgSuccess || retryCount == retryTimes) {
                    log.debug("END: CALLEXTERNALAPI");
                }
                retryCount++;
            }
        }

        return null;
    }
}

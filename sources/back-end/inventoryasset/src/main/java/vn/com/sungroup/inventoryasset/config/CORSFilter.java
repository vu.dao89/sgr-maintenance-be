/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CORSFilter class.
 *
 * <p>CORS Filter is a generic solution for fitting Cross-Origin Resource Sharing (CORS) support to
 * Java web applications
 */
//@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CORSFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        final HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader(
                "Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE, PATCH");
        response.setHeader(
                "Access-Control-Allow-Headers",
                "Authorization, Access-Control-Allow-Origin,Origin, X-Requested-With, Content-Type,"
                    + " X-Codingpedia, accept, authorization,UserId,RefreshToken, X-DEBUG,"
                    + " Log-Detail-Id ");
        response.setHeader("Access-Control-Max-Age", "3600");
        if ("OPTIONS".equalsIgnoreCase(((HttpServletRequest) req).getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        // TODO: init FilterConfig
    }
}

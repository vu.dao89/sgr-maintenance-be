/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

/**
 * CustomPrincipalExtractor class.
 *
 * <p>Custom Principal Extractor
 */
public class CustomPrincipalExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        return map.get("principal");
    }
}

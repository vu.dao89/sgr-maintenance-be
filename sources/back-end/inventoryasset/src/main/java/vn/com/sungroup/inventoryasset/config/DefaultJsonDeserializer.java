package vn.com.sungroup.inventoryasset.config;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.security.access.AuthorizationServiceException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
@JsonComponent
public class DefaultJsonDeserializer extends JsonDeserializer<String> implements ContextualDeserializer {
    private static final Pattern htmlPattern = Pattern.compile(".*\\<[^>]+>.*", Pattern.DOTALL);
    @Override
    public String deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        String value = parser.getValueAsString();
        if (StringUtils.isEmpty(value))
            return value;
        else {

            boolean isHTML = htmlPattern.matcher(value).matches();
            if (isHTML) {
                throw new AuthorizationServiceException("Invalid parameter input.");
            }
            return value;
        }
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property)
            throws JsonMappingException {
        return this;
    }

}
package vn.com.sungroup.inventoryasset.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
@EnableConfigurationProperties(SftpProperties.class)
public class SftpAutoConfiguration {

    @Autowired
    public SftpAutoConfiguration(SftpProperties sftpProperties) {
        final String conditionText = "app.sftp.%s properties must not be null";
        Objects.requireNonNull(sftpProperties.getHost(), String.format(conditionText, "host"));
        Objects.requireNonNull(sftpProperties.getPort(), String.format(conditionText, "port"));
        Objects.requireNonNull(sftpProperties.getUsername(), String.format(conditionText, "username"));
        Objects.requireNonNull(sftpProperties.getPassword(), String.format(conditionText, "password"));
        Objects.requireNonNull(sftpProperties.getLocalPath(), String.format(conditionText, "localPath"));
        Objects.requireNonNull(sftpProperties.getRemotePath(), String.format(conditionText, "remotePath"));
    }
}

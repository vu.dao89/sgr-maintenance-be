package vn.com.sungroup.inventoryasset.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app.sftp", ignoreUnknownFields = false)
@Data
public class SftpProperties {

    private String host;
    private Integer port;
    private String username;
    private String password;
    private String localPath;
    private String remotePath;
    private String backupRemotePath;
}

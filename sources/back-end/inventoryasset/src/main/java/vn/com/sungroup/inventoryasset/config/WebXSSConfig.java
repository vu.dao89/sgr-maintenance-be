package vn.com.sungroup.inventoryasset.config;

import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

import vn.com.sungroup.inventoryasset.util.Constant;

@Configuration 
public class WebXSSConfig extends WebMvcConfigurerAdapter {
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jsonConverter());
    }

    @Bean
    public HttpMessageConverter<?> jsonConverter() {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(String.class, new DefaultJsonDeserializer());

        ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();
        module.addSerializer(new LocalDateSerializer(DateTimeFormatter.ofPattern(Constant.FORMAT_DATE)));
        module.addSerializer(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(Constant.FORMAT_DATE_TIME)));
        module.addSerializer(new LocalTimeSerializer(DateTimeFormatter.ofPattern(Constant.FORMAT_TIME)));
        objectMapper.registerModule(module);

        return new MappingJackson2HttpMessageConverter(objectMapper);
    }
}
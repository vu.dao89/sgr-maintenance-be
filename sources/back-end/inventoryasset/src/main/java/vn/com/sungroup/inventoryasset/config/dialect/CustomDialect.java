package vn.com.sungroup.inventoryasset.config.dialect;

import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import com.vladmihalcea.hibernate.type.json.JsonType;

import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

public class CustomDialect extends org.hibernate.dialect.MySQL8Dialect {
    public CustomDialect() {
        this.registerHibernateType(Types.OTHER, StandardBasicTypes.UUID_CHAR.getName());
        this.registerHibernateType(2003, StringArrayType.class.getName());
        this.registerHibernateType(Types.OTHER, IntArrayType.class.getName());
        this.registerHibernateType(Types.OTHER, JsonStringType.class.getName());
        this.registerHibernateType(Types.OTHER, JsonBinaryType.class.getName());
        this.registerHibernateType(Types.OTHER, JsonNodeBinaryType.class.getName());
        this.registerHibernateType(Types.OTHER, JsonType.class.getName());
        this.registerHibernateType(Types.VARCHAR, StandardBasicTypes.STRING.getName());
        this.registerHibernateType(Types.OTHER, JsonNodeStringType.class.getName());
        this.registerColumnType(Types.JAVA_OBJECT, "json");
    }
}

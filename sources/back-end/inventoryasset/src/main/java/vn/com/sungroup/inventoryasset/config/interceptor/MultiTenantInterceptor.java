/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import vn.com.sungroup.inventoryasset.config.tenant.DatabaseContext;
import vn.com.sungroup.inventoryasset.config.tenant.TenantContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MultiTenantInterceptor class.
 *
 * <p>Contains information about MultiTenant Interceptor Object
 */
public class MultiTenantInterceptor extends HandlerInterceptorAdapter {

    public static final String TENANT_HEADER_NAME = "X-TENANT";

    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String tenantId = request.getHeader(TENANT_HEADER_NAME);
        TenantContext.setCurrentTenant(tenantId);

        return true;
    }

    @Override
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            ModelAndView model)
            throws Exception {
        DatabaseContext.forceCloseEntityManager();
        TenantContext.clear();
    }
}

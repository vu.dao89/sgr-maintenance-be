/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.tenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

/**
 * CurrentTenantIdentifierResolverImpl class.
 *
 * <p>Contains information about Current Tenant Identifier ResolverImpl.
 */
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

    public static String DEFAULT_TENANT = "DEFAULT";

    @Override
    public String resolveCurrentTenantIdentifier() {
        String currentTenant = TenantContext.getCurrentTenant();
        return currentTenant != null ? currentTenant : DEFAULT_TENANT;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.tenant;

import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;

import java.util.Map;

import javax.sql.DataSource;

/**
 * DataSourceBasedMultiTenantConnectionProviderImpl class.
 *
 * <p>Contains information about DataSource Based Multi Tenant Connection Provider.
 */
public class DataSourceBasedMultiTenantConnectionProviderImpl
        extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl {

    private static final long serialVersionUID = 1L;

    private static Map<String, DataSource> mDataSources;

    public DataSourceBasedMultiTenantConnectionProviderImpl(Map<String, DataSource> mDataSources) {
        if (DataSourceBasedMultiTenantConnectionProviderImpl.mDataSources == null) {
            DataSourceBasedMultiTenantConnectionProviderImpl.mDataSources = mDataSources;
        }
    }

    @Override
    protected DataSource selectAnyDataSource() {
        return DataSourceBasedMultiTenantConnectionProviderImpl.mDataSources
                .values()
                .iterator()
                .next();
    }

    @Override
    protected DataSource selectDataSource(String tenantId) {
        return DataSourceBasedMultiTenantConnectionProviderImpl.mDataSources.get(tenantId);
    }
}

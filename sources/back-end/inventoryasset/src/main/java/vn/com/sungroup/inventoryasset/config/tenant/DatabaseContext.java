/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.tenant;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class DatabaseContext {
    private static ThreadLocal<EntityManager> entityManager = new ThreadLocal<>();

    public static EntityManager getEntityManager(EntityManagerFactory emf) {
        if (entityManager.get() == null) {
            entityManager.set(emf.createEntityManager());
        }
        return entityManager.get();
    }

    public static void clearEntityManager() {
        if (entityManager.get() != null && !entityManager.get().getTransaction().isActive()) {
            if (entityManager.get() != null) {
                entityManager.get().close();
            }
            entityManager.remove();
        }
    }

    public static void forceCloseEntityManager() {
        if (entityManager.get() != null) {
            entityManager.get().close();
        }
        entityManager.remove();
    }
}

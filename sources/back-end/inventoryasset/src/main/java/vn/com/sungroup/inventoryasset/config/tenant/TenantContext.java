/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.tenant;

import java.util.ArrayList;
import java.util.List;

/**
 * TenantContext class.
 *
 * <p>Contains information about Tenant context.
 */
public class TenantContext {

    private static ThreadLocal<String> currentTenant = new ThreadLocal<>();
    private static final ThreadLocal<List<Object>> currentTenantDebug = new ThreadLocal<>();

    public static void setCurrentTenant(String tenantId) {
        currentTenant.set(tenantId);
    }

    public static String getCurrentTenant() {
        return currentTenant.get();
    }

    public static void setCurrentTenantDebug(List<Object> tenantDebug) {
        currentTenantDebug.set(tenantDebug);
    }

    public static List<Object> getCurrentTenantDebug() {
        if (currentTenantDebug.get() == null) {
            setCurrentTenantDebug(new ArrayList<Object>());
        }
        return currentTenantDebug.get();
    }

    public static void clear() {
        currentTenant.remove();
        currentTenantDebug.remove();
    }
}

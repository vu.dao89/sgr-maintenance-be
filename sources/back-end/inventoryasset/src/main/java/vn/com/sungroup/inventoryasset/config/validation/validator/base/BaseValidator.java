/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.config.validation.validator.base;

import javax.annotation.Nonnull;
import javax.validation.ConstraintValidatorContext;

public abstract class BaseValidator {

    protected void customValidatorContext(
            @Nonnull ConstraintValidatorContext constraintValidatorContext,
            @Nonnull String message) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext
                .buildConstraintViolationWithTemplate(message)
                .addConstraintViolation();
    }
}

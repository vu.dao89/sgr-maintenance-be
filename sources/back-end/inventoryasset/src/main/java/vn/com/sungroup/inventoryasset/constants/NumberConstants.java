package vn.com.sungroup.inventoryasset.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class NumberConstants {

  public static final int DEFAULT_SIZE = 100;
}
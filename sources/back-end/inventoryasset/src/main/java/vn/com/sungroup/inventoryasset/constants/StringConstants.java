package vn.com.sungroup.inventoryasset.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringConstants {

  public static final String INVALID_SORT_PROPERTY_LOG_MESSAGE = "Received invalid sort property: {}";
  public static final String INFO_NOT_FOUND_BASE_MESSAGE = "Get %s by %s %s not found.";

  public static final String OPERATION_TYPE_ADD = "ADD";
  public static final String OPERATION_TYPE_CHANGE = "CHANGE";
  public static final String OPERATION_TYPE_DELETE = "DELETE";
  public static final String DOT = ".";
  public static final String THUMBNAIL_SUFFIXES = "_thumbnail";
  public static final String MAINTENANCE_PLANT_FOLDER = "MPlant";
  public static final String WORK_CENTER_FOLDER = "WorkCenter";
  public static final String OBJ_TYPE_FOLDER = "Objtype";
  public static final String EQUIPMENT_FOLDER = "Equipment";
}
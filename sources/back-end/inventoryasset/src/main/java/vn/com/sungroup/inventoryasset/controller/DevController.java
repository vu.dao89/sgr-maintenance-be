package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.service.SyncMasterDataService;
import vn.com.sungroup.inventoryasset.service.impl.RaygunServiceImpl;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.SAP_DATA_NULL;
import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.SAP_WORK_ORDER_ERROR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dev")
public class DevController {

    private final Logger LOGGER = LoggerFactory.getLogger(DevController.class);
    private final SyncMasterDataService syncMasterDataService;
    private final RaygunServiceImpl raygunService;

    @Autowired
    MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Value("${datasource.hikari.schema:public}")
    private String schema;
    @PersistenceContext
    private EntityManager entityManager;

    @GetMapping("/sync/force")
    public String forceSyncMasterData() {
        Thread thread = new Thread() {
            public void run() {
                syncMasterDataService.syncMasterData();
            }
        };
        thread.start();
        return "Trigger sync master data at:" + LocalDateTime.now();
    }

    @GetMapping("/sync/employees")
    public String forceSyncEmployeesData() {
//        Thread thread = new Thread() {
//            public void run() {
//                syncMasterDataService.syncEmployee();
//            }
//        };
//        thread.start();

        syncMasterDataService.syncEmployee();
        return "Trigger sync master data at:" + LocalDateTime.now();
    }

    @GetMapping("/raygun/send-info")
    public void raygunSendInfo(@RequestParam String message) {
        Sentry.captureMessage(message, SentryLevel.INFO);
        raygunService.sendInfo(message);
    }

    @GetMapping("/raygun/send-error")
    public void raygunSendError(@RequestParam String message) {
        Sentry.captureException(new Exception(message));
        raygunService.sendError(new Exception(message));
    }

    @GetMapping("/truncate")
    @Transactional
    public String truncateTable(@RequestParam String tableName) {
        try {
            if (!schema.isEmpty()) {
                tableName = String.format("%s.%s", schema, tableName);
            }
            var query = "TRUNCATE TABLE " + tableName + " CASCADE";
            entityManager.createNativeQuery(query).executeUpdate();
            return "Truncate successful";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/truncate-all")
    @Transactional
    public String truncateAllTables() {
        try {
            List<String> tableNames = Arrays.asList("maintenance_plant",
                    "work_center",
                    "object_type",
                    "activity_type",
                    "equipment_category",
                    "work_order_type",
                    "notification_type",
                    "group_code",
                    "activity_key",
                    "unit_of_measurement",
                    "personnel",
                    "system_condition",
                    "storage_location",
                    "planner_group",
                    "functional_location",
                    "functional_location_characteristic",
                    "functional_location_measuring_point",
                    "equipment",
                    "equipment_characteristic",
                    "equipment_measuring_point",
                    "equipment_partner",
                    "partner",
                    "characteristic",
                    "measuring_point");

            tableNames.forEach(tableName -> {
                if (!schema.isEmpty()) {
                    tableName = String.format("%s.%s", schema, tableName);
                }
                var query = "TRUNCATE TABLE " + tableName + " CASCADE";
                entityManager.createNativeQuery(query).executeUpdate();
            });

            return "Truncate successful";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/test-log")
    public void TestLog(){
        LOGGER.info("App bao tri log info: " + LocalDateTime.now()+" - UTC " + LocalDateTime.now(ZoneOffset.UTC) );

        LOGGER.warn("App bao tri log warn: " + LocalDateTime.now() +" - UTC " + LocalDateTime.now(ZoneOffset.UTC) );

        LOGGER.error("App bao tri log error: " + LocalDateTime.now() +" - UTC " + LocalDateTime.now(ZoneOffset.UTC) );
    }

    @GetMapping("test-error-message")
    public BaseResponseSAP GetErrorMessage() throws MasterDataNotFoundException {
        var result = new BaseResponseSAP("op11","test","S");
        var message = MessageUtil.getMessage(messageSource, locale, "CREATE_OPERATION_SUCCESS_MESSAGE");
        message = String.format(message,result.getDocument());
        result.setMessage(message);
        return result;
        //var extendMessage = MessageUtil.getMessage(messageSource,locale,SAP_WORK_ORDER_ERROR);
        //var message = MessageUtil.getMessage(messageSource,locale,SAP_DATA_NULL);
        //return String.format(message, extendMessage);
        //return ErrorUtil.getErrorMessage(messageSource,locale,"UNKNOWN_ERROR");
    }
}

package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.dto.request.equipment.GetMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.response.equipment.sap.MeasPointSAPResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.service.EquipmentService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/equipment")
@Slf4j
public class EquipmentController {
    private final EquipmentService equipmentService;
    private final ParseUtils parseUtils;

    @PostMapping("/post/meas-point")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_MEAS_POST)")
    public BaseResponseSAP postEquipmentMeasPoint(@RequestBody PostMeasPointRequest measPoint) throws InvalidInputException, SAPApiException {
        log.info("Create equipment measPoint with input: {}", parseUtils.toJsonString(measPoint));
        log.debug("REST request to get Equipment MeasPoint : {}", measPoint);
        Sentry.captureMessage(
            "Create equipment measPoint with input: " + parseUtils.toJsonString(measPoint),
            SentryLevel.INFO);
        BaseResponseSAP measPointResponse = equipmentService.postEquipmentMeasPoint(measPoint);
        return measPointResponse;
    }

    @GetMapping("/hierarchy")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_VIEW)")
    public Collection<TreeNode<EquipmentHierarchyResponse>> getEquipmentHierarchy(
            @RequestParam String equipmentId) {
        log.info("Get equipment hierarchy with equipmentId: {}", equipmentId);
        Sentry.captureMessage("Get equipment hierarchy with equipmentId: " + equipmentId,
            SentryLevel.INFO);
        return equipmentService.getEquipmentHierarchy(equipmentId);
    }

    @GetMapping("/meas-point")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_MEAS_GET)")
    public MeasPointSAPResponse getEquipmentMeasPoint(GetMeasPointRequest measPoint)
        throws SAPApiException {
        log.info("Get equipment measPoint with input: {}", parseUtils.toJsonString(measPoint));
        log.debug("REST request to get Equipment MeasPoint : {}", measPoint);
        Sentry.captureMessage(
            "Get equipment measPoint with input: " + parseUtils.toJsonString(measPoint),
            SentryLevel.INFO);
        MeasPointSAPResponse measPointResponse = equipmentService.getEquipmentMeasPoint(measPoint);
        return measPointResponse;
    }
}

package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentDocument;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.service.EquipmentDocumentService;
import vn.com.sungroup.inventoryasset.service.GlobalDocumentService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
@Slf4j
public class EquipmentDocumentController {
    private final EquipmentDocumentService equipmentDocumentService;
    private final GlobalDocumentService globalDocumentService;
    private final ParseUtils parseUtils;

    @GetMapping("/equipment-documents")
    public PaginationResponse<EquipmentDocument> getGlobalDocuments(EquipmentDocumentRequest request,
                                                                    @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) throws InvalidSortPropertyException {
        log.info("Get equipment document with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Get equipment document with input: "
            + parseUtils.toJsonString(request), SentryLevel.INFO);
        Page<EquipmentDocument> page = equipmentDocumentService.findAllByEquipmentCode(request,pageable);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/equipment-document/{id}")
    public Optional<EquipmentDocument> getById(@PathVariable UUID id) {
        log.info("Get equipment document with id: {}", id);
        Sentry.captureMessage("Get equipment document with id: " + id, SentryLevel.INFO);
        return equipmentDocumentService.findOne(id);
    }

    @PostMapping("/equipment-document")
    public EquipmentDocument create(EquipmentDocumentCreate request) {
        log.info("Create equipment document with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
            "Create equipment document with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);
        return equipmentDocumentService.save(request);
    }


    @PostMapping("/equipment-document/uploads")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_ATTACHMENT_POST)")
    public List<EquipmentDocument> uploads(@RequestParam String equipmentId, @RequestParam("files") List<MultipartFile> files ) throws Exception {
        log.info("Upload equipment document with equipmentId: {}, files: {}", equipmentId,
            parseUtils.toJsonString(files));
        Sentry.captureMessage(
            String.format("Upload equipment document with equipmentId: %s, files: %s", equipmentId,
                parseUtils.toJsonString(files)), SentryLevel.INFO);
        List<GlobalDocument> documents = new ArrayList<>();
        for (var file : files) {
            var request = new GlobalDocumentUploadRequest();
            request.fileBytes = file.getBytes();
            request.fileName = file.getOriginalFilename();
            request.fileType = file.getContentType();
            var entity = globalDocumentService.uploadFile(request);
            if (entity != null) {
                documents.add(entity);
            }
        }
        List<EquipmentDocument> result = new ArrayList<>();
        for (var gbDoc : documents)
        {
            EquipmentDocumentCreate request = EquipmentDocumentCreate.builder()
                    .documentId(gbDoc.getId())
                    .equipmentCode(equipmentId)
                    .build();
            var equipmentDocument =  equipmentDocumentService.save(request);
            if (equipmentDocument != null){
                result.add(equipmentDocument);
            }
        }
        return result;
    }

}

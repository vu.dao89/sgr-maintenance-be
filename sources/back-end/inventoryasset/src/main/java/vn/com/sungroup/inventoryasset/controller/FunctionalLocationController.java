package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import java.util.Collection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.dto.functionallocation.FunctionalLocationHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.service.FuncLocationService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/functional-location")
@Slf4j
public class FunctionalLocationController {

  private final FuncLocationService funcLocationService;

  @GetMapping("/hierarchy")
  public Collection<TreeNode<FunctionalLocationHierarchyResponse>> getFunctionalLocationHierarchy(
      @RequestParam String functionalLocationId) {
    log.info("Get functional location hierarchy with functionalLocationId: {}",
        functionalLocationId);
    Sentry.captureMessage(
        "Get functional location hierarchy with functionalLocationId: " + functionalLocationId,
        SentryLevel.INFO);
    return funcLocationService.getFunctionalLocationHierarchy(functionalLocationId);
  }
}

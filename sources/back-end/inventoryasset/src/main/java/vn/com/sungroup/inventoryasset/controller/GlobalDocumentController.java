package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.exception.DocumentNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.service.GlobalDocumentService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
@Slf4j
public class GlobalDocumentController {
    private final GlobalDocumentService globalDocumentService;
    private final ParseUtils parseUtils;

    @PostMapping("/global-document/upload")
    public GlobalDocument uploadDocument(@RequestParam("file") MultipartFile file) throws Exception {
        log.info("Upload global document with file: {}", parseUtils.toJsonString(file));
        Sentry.captureMessage(
            "Upload global document with file: " + parseUtils.toJsonString(file),
            SentryLevel.INFO);

        var request = new GlobalDocumentUploadRequest();
        request.fileBytes = file.getBytes();
        request.fileName = file.getOriginalFilename();
        request.fileType = file.getContentType();
        return globalDocumentService.uploadFile(request);
    }

    @PostMapping("/global-document/uploads")
    public List<GlobalDocument> uploadDocument(@RequestParam("files") List<MultipartFile> files) throws Exception {
        log.info("Upload global document with file: {}", parseUtils.toJsonString(files));
        Sentry.captureMessage(
            "Upload global document with file: " + parseUtils.toJsonString(files),
            SentryLevel.INFO);

        List<GlobalDocument> result = new ArrayList<>();
        for (var file : files) {
            var request = new GlobalDocumentUploadRequest();
            request.fileBytes = file.getBytes();
            request.fileName = file.getOriginalFilename();
            request.fileType = file.getContentType();

            var entity = globalDocumentService.uploadFile(request);
            if (entity != null) {
                result.add(entity);
            }
        }
        return result;
    }

    @GetMapping("/global-documents")
    public PaginationResponse<GlobalDocument> getGlobalDocuments(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) throws InvalidSortPropertyException {
        log.info("Get all global document");
        Sentry.captureMessage("Get all global document", SentryLevel.INFO);
        Page<GlobalDocument> page = globalDocumentService.findAll(pageable);
        return new PaginationResponse<>(page.getTotalElements(),0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/global-document")
    public Optional<GlobalDocument> getByName(@RequestParam String fileName) {
        log.info("Get global document with fileName: {}", fileName);
        Sentry.captureMessage("Get global document with fileName: " + fileName, SentryLevel.INFO);
        return globalDocumentService.findByName(fileName);
    }

    @GetMapping("/global-document/{id}")
    public GlobalDocument getById(@PathVariable UUID id) throws DocumentNotFoundException {
        log.info("Get global document with id: {}", id);
        Sentry.captureMessage("Get global document with id: " + id, SentryLevel.INFO);
        return globalDocumentService.getDocument(id);
    }

    @DeleteMapping("/global-document/{id}")
    public void deleteGlobalDocument(@PathVariable UUID id) {
        log.info("Delete global document with id: {}", id);
        Sentry.captureMessage("Delete global document with id: " + id, SentryLevel.INFO);
        globalDocumentService.delete(id);
    }
}

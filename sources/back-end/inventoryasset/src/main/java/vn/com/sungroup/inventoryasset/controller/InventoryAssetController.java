/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.extern.slf4j.Slf4j;
import vn.com.sungroup.inventoryasset.dto.request.AssetDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetGetCostcenterRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryChgRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetListRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetModifyRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetSearchRequest;
import vn.com.sungroup.inventoryasset.dto.response.AssetDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetGetCostcenterResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryChgResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetListResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetModifyResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.MoveFileResponse;
import vn.com.sungroup.inventoryasset.service.InventoryAssetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/** InventoryAssetController class. */
@RestController
@RequestMapping("")
@Slf4j
public class InventoryAssetController {

    @Autowired InventoryAssetService inventoryAssetService;
    @Autowired HttpServletRequest requestHeader;
    @Autowired HttpServletResponse responseHeader;

    @PostMapping("asset-list")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetListResponse> assetList(
            @Valid @RequestBody(required = false) AssetListRequest input) throws Exception {
        try {
            AssetListResponse result = inventoryAssetService.assetList(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetList ERROR: {}", e);
            Sentry.captureMessage("Controller assetList ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-search")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetSearchResponse> assetSearch(
            @Valid @RequestBody(required = false) AssetSearchRequest input) throws Exception {
        try {
            AssetSearchResponse result = inventoryAssetService.assetSearch(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetSearch ERROR: {}", e);
            Sentry.captureMessage("Controller assetList ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-detail")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetDetailResponse> assetDetail(
            @Valid @RequestBody(required = false) AssetDetailRequest input) throws Exception {
        try {
            AssetDetailResponse result = inventoryAssetService.assetDetail(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetDetail ERROR: {}", e);
            Sentry.captureMessage("Controller assetDetail ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-modify")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_UPDATE, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetModifyResponse> assetModify(
            @Valid @RequestBody(required = false) AssetModifyRequest input) throws Exception {
        try {
            HttpStatus status = HttpStatus.OK;
            AssetModifyResponse result = inventoryAssetService.assetModify(input);
            if (null != responseHeader) {
                status = HttpStatus.valueOf(responseHeader.getStatus());
            }
            return ResponseEntity.status(status).body(result);
        } catch (Exception e) {
            log.error("Controller assetModify ERROR: {}", e);
            Sentry.captureMessage("Controller assetModify ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-get-costcenter")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetGetCostcenterResponse> assetGetCostcenter(
            @Valid @RequestBody(required = false) AssetGetCostcenterRequest input)
            throws Exception {
        try {
            AssetGetCostcenterResponse result = inventoryAssetService.assetGetCostcenter(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetGetCostcenter ERROR: {}", e);
            Sentry.captureMessage("Controller assetGetCostcenter ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-inventory")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_UPDATE, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetInventoryResponse> assetInventory(
            @Valid @RequestBody(required = false) AssetInventoryRequest input) throws Exception {
        try {
            HttpStatus status = HttpStatus.OK;
            AssetInventoryResponse result = inventoryAssetService.assetInventory(input);
            if (null != responseHeader) {
                status = HttpStatus.valueOf(responseHeader.getStatus());
            }
            return ResponseEntity.status(status).body(result);
        } catch (Exception e) {
            log.error("Controller assetInventory ERROR: {}", e);
            Sentry.captureMessage("Controller assetInventory ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-history-inventory")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetHistoryInventoryResponse> assetHistoryInventory(
            @Valid @RequestBody(required = false) AssetHistoryInventoryRequest input)
            throws Exception {
        try {
            AssetHistoryInventoryResponse result =
                    inventoryAssetService.assetHistoryInventory(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetHistoryInventory ERROR: {}", e);
            Sentry.captureMessage("Controller assetHistoryInventory ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping("asset-history-chg")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_SAP_DISPLAY, T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).ASSET_APPLICATION)")
    public ResponseEntity<AssetHistoryChgResponse> assetHistoryChg(
            @Valid @RequestBody(required = false) AssetHistoryChgRequest input) throws Exception {
        try {
            AssetHistoryChgResponse result = inventoryAssetService.assetHistoryChg(input);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } catch (Exception e) {
            log.error("Controller assetHistoryChg ERROR: {}", e);
            Sentry.captureMessage("Controller assetHistoryChg ERROR: " + e, SentryLevel.ERROR);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}

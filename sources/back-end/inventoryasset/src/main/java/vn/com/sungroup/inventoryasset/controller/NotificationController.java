package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemDto;
import vn.com.sungroup.inventoryasset.dto.notification.search.NotificationSearchResultDto;
import vn.com.sungroup.inventoryasset.dto.request.notification.*;
import vn.com.sungroup.inventoryasset.dto.request.notification.metric.NotificationMetricRequest;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationCloseResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationMetricResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationItemCreateResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.service.NotificationService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import javax.validation.Valid;
import java.util.Locale;
import java.util.Objects;

import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.RESPONSE_SUCCESS_CODE;

@RestController
@RequestMapping("/notification")
@Slf4j
@RequiredArgsConstructor
public class NotificationController {

  private final NotificationService notificationService;
  private final PayloadUtil payloadUtil;
  private final ParseUtils parseUtils;

  @Autowired
  private final MessageSource messageSource;
  private final Locale locale = LocaleContextHolder.getLocale();

  @GetMapping("/get/notification-search")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_SEARCH)")
  public PaginationResponse<NotificationSearchResultDto> notificationSearch(
      @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
      NotificationSearchRequest input) throws Exception {
    log.info("Search notification with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Search notification with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    NotificationSearchResponse result = notificationService.notificationSearch(pageable, input);
    return new PaginationResponse<>(result.getTotalItems(),result.getTotalPage(),
        pageable.getPageNumber(), pageable.getPageSize(), result.getResult());
  }

  @GetMapping("/get/notification")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_VIEW)")
  public ResponseEntity<NotificationDetailResponse> notificationDetail(
      NotificationDetailRequest input) throws Exception {
    log.info("Search notification detail with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Search notification detail with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    payloadUtil.validateInput(input);
    NotificationDetailResponse result = notificationService.notificationDetail(input);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @PostMapping("/post/notification-post")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_POST)")
  public ResponseEntity<BaseResponseSAP> notificationCreate(
      @RequestBody NotificationCreateRequest input) throws Exception {
    log.info("Create notification with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Create notification with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);
    payloadUtil.validateNotificationCreate(input);
    payloadUtil.validateEquipmentAndFunctionalLocation(input.getHeader().getEquipmentId(),
            input.getHeader().getFunctionalLocationId());
    BaseResponseSAP result = notificationService.notificationCreate(input);
    if (!result.getDocument().isEmpty() && Objects.equals(result.getCode(), RESPONSE_SUCCESS_CODE)){
      var message = MessageUtil.getMessage(messageSource, locale, "CREATE_NOTIFICATION_SUCCESS_MESSAGE");
      message = String.format(message,result.getDocument());
      result.setMessage(message);
    }
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @PostMapping("/post/notification-item")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_CHANGE)")
  public NotificationItemCreateResponse notificationItemCreate(@RequestBody NotificationItemDto input) throws Exception {
    log.info("Create notification item with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Create notification item with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    payloadUtil.validateNotificationItemCreate(input);
    return notificationService.notificationItemCreate(input);
  }

  @DeleteMapping("/delete/notification-item")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_CHANGE)")
  public BaseResponseSAP notificationItemCreate(@RequestBody NotificationDeleteRequest input) throws Exception {
    log.info("Delete notification with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Delete notification with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    return notificationService.notificationItemDelete(input);
  }

  @PutMapping("/update/notification-item")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_CHANGE)")
  public BaseResponseSAP notificationItemUpdate(@RequestBody NotificationItemDto input) throws Exception {
    log.info("Change notification item with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Change notification item with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    payloadUtil.validateNotificationItemUpdate(input);
    return notificationService.notificationItemUpdate(input);
  }

  @PostMapping("/post/notification-close")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_CONFIRM_COMPLETE)")
  public ResponseEntity<NotificationCloseResponse> notificationClose(
      @Valid @RequestBody(required = false) NotificationCloseRequest input) throws Exception {
    log.info("Close notification with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Close notification with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    NotificationCloseResponse result = notificationService.notificationClose(input);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @PostMapping("/post/notification-change")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_CHANGE)")
  public ResponseEntity<BaseResponseSAP> notificationChange(
      @RequestBody NotificationCreateRequest input) throws Exception {
    log.info("Change notification with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Change notification with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);

    payloadUtil.validateInput(input.getHeader());
    payloadUtil.validateEquipmentAndFunctionalLocation(input.getHeader().getEquipmentId(),
            input.getHeader().getFunctionalLocationId());
    BaseResponseSAP result = notificationService.notificationChange(input, null);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @GetMapping("metric")
  @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_VIEW)")
  public NotificationMetricResponse getNotificationMetric(
      NotificationMetricRequest input
  ) throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
    log.info("Search notification metric with input: {}", parseUtils.toJsonString(input));
    Sentry.captureMessage(
        "Search notification metric with input: " + parseUtils.toJsonString(input),
        SentryLevel.INFO);
    payloadUtil.validateInput(input);
    return notificationService.getNotificationMetricFromSAP(input);
  }
}

package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;
import vn.com.sungroup.inventoryasset.service.GlobalDocumentService;
import vn.com.sungroup.inventoryasset.service.NotificationDocumentService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
@Slf4j
public class NotificationDocumentController {
    private final NotificationDocumentService notificationDocumentService;
    private final GlobalDocumentService globalDocumentService;
    private final ParseUtils parseUtils;

    @GetMapping("/notification-documents")
    public PaginationResponse<NotificationDocument> getGlobalDocuments(NotificationDocumentRequest request,
                                                                       @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) {
        log.info("Get notification document with input: {}",
            parseUtils.toJsonString(request));
        Sentry.captureMessage(
            "Get notification document with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);
        Page<NotificationDocument> page = notificationDocumentService.findAllByNotificationCode(request,pageable);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/notification-document/{id}")
    public Optional<NotificationDocument> getById(@PathVariable UUID id){
        log.info("Get notification document detail with id: {}", id);
        Sentry.captureMessage(
            "Get notification document detail with id: " + id,
            SentryLevel.INFO);
        return notificationDocumentService.findOne(id);
    }

    @PostMapping("/notification-document")
    public NotificationDocument create(NotificationDocumentCreate request){
        log.info("Create notification document with input: {}",
            parseUtils.toJsonString(request));
        Sentry.captureMessage(
            "Create notification document  with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);
        return notificationDocumentService.save(request);
    }


    @PostMapping("/notification-document/uploads")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_ATTACHMENT_POST)")
    public List<NotificationDocument> uploads(@RequestParam String notificationId, @RequestParam("files") List<MultipartFile> files ) throws Exception {
        log.info("Upload notification document with notificationId: {}, files: {}", notificationId,
            parseUtils.toJsonString(files));
        Sentry.captureMessage(
            String.format("Upload notification document with notificationId: %s, files: %s",
                notificationId, parseUtils.toJsonString(files)), SentryLevel.INFO);
        List<GlobalDocument> documents = new ArrayList<>();
        for (var file : files) {
            var request = new GlobalDocumentUploadRequest();
            request.fileBytes = file.getBytes();
            request.fileName = file.getOriginalFilename();
            request.fileType = file.getContentType();
            var entity = globalDocumentService.uploadFile(request);
            if (entity != null) {
                documents.add(entity);
            }
        }
        List<NotificationDocument> result = new ArrayList<>();
        for (var gbDoc : documents)
        {
            NotificationDocumentCreate request = NotificationDocumentCreate.builder()
                    .documentId(gbDoc.getId())
                    .notificationCode(notificationId)
                    .build();
            var notificationDocument =  notificationDocumentService.save(request);
            if (notificationDocument != null){
                result.add(notificationDocument);
            }
        }
        return result;
    }

    @DeleteMapping("/notification-document/delete")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).NOTI_ATTACHMENT_POST)")
    public BaseResponseSAP delete(@RequestParam String notificationId, @RequestParam String documentId ) {
        return notificationDocumentService.delete(notificationId, UUID.fromString(documentId));
    }

}

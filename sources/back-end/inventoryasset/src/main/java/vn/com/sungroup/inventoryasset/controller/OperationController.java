package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryCreate;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryUpdate;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.*;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse.OperationMetricsSAP;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.exception.*;
import vn.com.sungroup.inventoryasset.service.OperationHistoryService;
import vn.com.sungroup.inventoryasset.service.OperationService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.RESPONSE_SUCCESS_CODE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/operation")
@Slf4j
public class OperationController {

    private final PayloadUtil payloadUtil;
    private final OperationService operationService;
    private final OperationHistoryService operationHistoryService;
    private final ParseUtils parseUtils;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    @GetMapping("detail")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_VIEW)")
    public OperationDetailResponse getOperationDetail(OperationDetailRequest input)
            throws MissingRequiredFieldException, SAPApiException {
        log.info("Get operation detail with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
                "Get operation detail with input: " + parseUtils.toJsonString(input),
                SentryLevel.INFO);
        payloadUtil.validateInput(input);
        return operationService.getDetailFromSap(input);
    }

    @GetMapping("sub/detail")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_SUB_OPS_VIEW)")
    public SubOperationDetailResponse getSubOperationDetail(OperationDetailRequest input)
            throws MissingRequiredFieldException, SAPApiException {
        log.info("Get sub operation detail with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
                "Get sub operation detail with input: " + parseUtils.toJsonString(input),
                SentryLevel.INFO);
        payloadUtil.validateInput(input);
        return operationService.getSubDetailFromSap(input);
    }

    @GetMapping("search")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_SEARCH)")
    public PaginationResponse<OperationSearchDetailItem> searchOperation(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
                                                                         OperationSearchRequest input)
            throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        log.info("Search operation with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
                "Search operation with input: " + parseUtils.toJsonString(input),
                SentryLevel.INFO);
        payloadUtil.validateInput(input);
        var result = operationService.searchOperation(pageable, input);

        return new PaginationResponse<>(result.getTotalItems(), result.getTotalPage(),
                pageable.getPageNumber(), pageable.getPageSize(), result.getOperation().getItem());
    }

    @GetMapping("prt")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_VIEW)")
    public OperationMeasuresSAP getOperationMeasures(String workOrderId, String operationId)
            throws SAPApiException {
        log.info("Get operation measure with workOrderId: {}, operationId: {}", workOrderId,
                operationId);
        Sentry.captureMessage(
                String.format("Get operation measure with workOrderId: %s, operationId: %s", workOrderId,
                        operationId), SentryLevel.INFO);
        return operationService.getOperationMeasures(OperationMeasuresRequestSAP.builder()
                .workOrderId(workOrderId)
                .operationId(operationId)
                .build());
    }

    @GetMapping("material")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_VIEW)")
    public OperationComponentResponse getOperationComponent(OperationDetailRequest input)
            throws MissingRequiredFieldException, SAPApiException {
        log.info("Get operation material with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
                "Get operation material with input: " + parseUtils.toJsonString(input),
                SentryLevel.INFO);
        payloadUtil.validateInput(input);
        return operationService.getOperationComponentFromSAP(input);
    }

    @GetMapping("metric")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_VIEW)")
    public OperationMetricsSAP getOperationMetrics(OperationMetricsRequest request)
            throws SAPApiException, InvalidInputException {
        log.info("Get operation metric with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Get operation metric with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        return operationService.getOperationMetrics(request);
    }

    @GetMapping("current")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_SEARCH)")
    public OperationDetailResponse getCurrentOperation()
            throws SAPApiException {
        log.info("Get operation current");
        Sentry.captureMessage("Get operation current", SentryLevel.INFO);
        var staringOperation = operationHistoryService.getStartingOperation(null);
        return operationHistoryService.collectOperationHistory(staringOperation);
    }

    @PostMapping("sub/change")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_SUB_OPS_POST)")
    public BaseResponseSAP changeSubOperation(@RequestBody OperationChangeRequest request) throws SAPApiException,
            MissingRequiredFieldException, InvalidInputException {
        log.info("Change sub operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Change sub operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        payloadUtil.validateSubOperationChange(request);
        return operationService.changeSubOperation(request);
    }

    @PostMapping("create")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_POST)")
    public UpdateOperationsResponse createOperation(@RequestBody OperationCreateRequest request) throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        log.info("Create operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Create operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        payloadUtil.validateOperationCreate(request);
        var result = operationService.createOperation(request);
        if (!result.getDocument().isEmpty() && Objects.equals(result.getCode(), RESPONSE_SUCCESS_CODE)) {
            var message = MessageUtil.getMessage(messageSource, locale, "CREATE_OPERATION_SUCCESS_MESSAGE");
            message = String.format(message, result.getDocument());
            result.setMessage(message);
        }
        return result;
    }

    @PostMapping("update")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_POST)")
    public UpdateOperationsResponse updateOperation(@RequestBody OperationCreateRequest request) throws MissingRequiredFieldException, SAPApiException, InvalidInputException, OperationNotFoundException, MasterDataNotFoundException {
        log.info("Change operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Change operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        payloadUtil.validateOperationUpdate(request);
        return operationService.updateOperation(request);
    }

    @DeleteMapping("delete")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_POST)")
    public BaseResponseSAP deleteOperation(OperationDeleteRequest request)
            throws MissingRequiredFieldException, SAPApiException, OperationNotFoundException, InvalidInputException, MasterDataNotFoundException {
        log.info("Delete operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Delete operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        payloadUtil.validateInput(request);
        return operationService.deleteOperation(request);
    }


    @PostMapping("/confirmation")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_CONFIRM_COMPLETE)")
    public OperationConfirmationResponse confirmation(
            @RequestBody OperationConfirmationRequest request
    ) throws SAPApiException, MissingRequiredFieldException {
        return operationService.operationConfirmation(request);
    }

    @PostMapping("/material/create")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_POST)")
    public MaterialDocumentResponse createMaterialDocument(@RequestBody MaterialDocumentRequest request) throws InvalidInputException, MissingRequiredFieldException, SAPApiException {
        log.info("Create operation material with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Create operation material with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        payloadUtil.validateMaterialDocumentCreate(request);

        return operationService.createMaterialDocument(request);
    }


    @PostMapping("/start")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_CONFIRM_START)")
    public OperationHistoryResponse create(@RequestBody OperationHistoryCreate input) throws SAPApiException, InvalidInputException, ApplicationException {
        log.info("Start operation with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
                "Start operation with input: " + parseUtils.toJsonString(input),
                SentryLevel.INFO);
        return operationService.operationStart(input);
    }

    @PostMapping("/hold")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_CONFIRM_COMPLETE)")
    public OperationHistoryResponse hold(
            @RequestParam(required = true) String workOrderId,
            @RequestParam(required = true) String operationId,
            @RequestParam(required = false) String superOperationId,
            @RequestParam(required = true) String actualWork,
            @RequestParam(required = true) String workStartDate,
            @RequestParam(required = true) String workStartTime,
            @RequestParam(required = true) String workFinishDate,
            @RequestParam(required = true) String workFinishTime,
            @RequestParam(required = false) String confirmation,
            @RequestParam(value = "files", required = false) List<MultipartFile> files) throws InvalidInputException, SAPApiException {
        OperationHistoryUpdate input = OperationHistoryUpdate.builder()
                .workOrderId(workOrderId)
                .operationId(operationId)
                .superOperationId(superOperationId)
                .workStartDate(workStartDate)
                .workStartTime(workStartTime)
                .workFinishDate(workFinishDate)
                .workFinishTime(workFinishTime)
                .varianceReasonCode("X") // X = Tạm dừng
                .confirmation(confirmation)
                .actualWork(actualWork)
                .build();
        log.info("Hold operation with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(String.format("Hold operation with input: %s", parseUtils.toJsonString(input)), SentryLevel.INFO);
        return operationService.operationHold(input, files);
    }

    @PostMapping("/complete")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_CONFIRM_COMPLETE)")
    public OperationHistoryResponse complete(
            @RequestParam(required = true) String workOrderId,
            @RequestParam(required = true) String operationId,
            @RequestParam(required = true) String actualWork,
            @RequestParam(required = true) String workStartDate,
            @RequestParam(required = true) String workStartTime,
            @RequestParam(required = true) String workFinishDate,
            @RequestParam(required = true) String workFinishTime,
            @RequestParam(required = true) String varianceReasonCode,
            @RequestParam(required = false) String confirmation,
            @RequestParam(value = "files", required = false) List<MultipartFile> files) throws InvalidInputException, SAPApiException, MasterDataNotFoundException, MissingRequiredFieldException {
        OperationHistoryUpdate input = OperationHistoryUpdate.builder()
                .workOrderId(workOrderId)
                .operationId(operationId)
                .workStartDate(workStartDate)
                .workStartTime(workStartTime)
                .workFinishDate(workFinishDate)
                .workFinishTime(workFinishTime)
                .varianceReasonCode(varianceReasonCode)
                .confirmation(confirmation)
                .actualWork(actualWork)
                .build();

        log.info("Complete operation with input: {}",
                parseUtils.toJsonString(input));
        Sentry.captureMessage(String.format("Complete operation with input: %s",
                parseUtils.toJsonString(input)), SentryLevel.INFO);
        return operationService.operationComplete(input, files);
    }

    @PutMapping("/parent/personnel-change")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_CHANGE_MAIN_PERSON)")
    public void changePersonnelOperationParent(@RequestBody OperationPersonnelChangeRequest request)
            throws MissingRequiredFieldException, SAPApiException, OperationNotFoundException {
        log.info("Change personnel operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Change personnel operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        operationService.changePersonnelOperationParent(request);
    }

    @PutMapping("/sub/personnel-change")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_SUB_OPS_CHANGE_MAIN_PERSON)")
    public void changePersonnelOperationSub(@RequestBody OperationSubPersonnelChangeRequest request)
            throws MissingRequiredFieldException, SAPApiException, OperationNotFoundException {
        log.info("Change personnel sub operation with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Change personnel sub operation with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        operationService.changePersonnelOperationSub(request);
    }

    @PostMapping("measuring-point")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).MTN_WO_OPS_MEASURING_DOC_POST)")
    public BaseResponseSAP postMeasuringPoint(@RequestBody PostMeasPointRequest measPoint) throws InvalidInputException, SAPApiException {
        log.info("Create operation measPoint with input: {}", parseUtils.toJsonString(measPoint));
        log.debug("Create operation measPoint with input: {}", parseUtils.toJsonString(measPoint));
        Sentry.captureMessage(
                "Create operation measPoint with input: " + parseUtils.toJsonString(measPoint),
                SentryLevel.INFO);
        return operationService.postMeasuringPoint(measPoint);
    }
}

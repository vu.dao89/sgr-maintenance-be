package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.service.GlobalDocumentService;
import vn.com.sungroup.inventoryasset.service.OperationDocumentService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
@Slf4j
public class OperationDocumentController {

    private final OperationDocumentService operationDocumentService;
    private final GlobalDocumentService globalDocumentService;
    private final ParseUtils parseUtils;

    @GetMapping("/operation-documents")
    public PaginationResponse<OperationDocument> getGlobalDocuments(OperationDocumentRequest request,
                                                                    @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) {
        log.info("Get operation documents with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Get operation documents with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        Page<OperationDocument> page = operationDocumentService.findAllByOperationCode(request, pageable);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/operation-document/{id}")
    public Optional<OperationDocument> getById(@PathVariable UUID id) {
        log.info("Get operation documents with id: {}", id);
        Sentry.captureMessage("Get operation documents with id: " + id, SentryLevel.INFO);
        return operationDocumentService.findOne(id);
    }

    @PostMapping("/operation-document")
    public OperationDocument create(OperationDocumentCreate request) {
        log.info("Create operation documents with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
                "Create operation documents with input: " + parseUtils.toJsonString(request),
                SentryLevel.INFO);
        return operationDocumentService.save(request);
    }

    @PostMapping("/operation-document/uploads")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_ATTACHMENT_POST)")
    public List<OperationDocument> uploads(@RequestParam String workOrderId,
                                           @RequestParam String operationId,
                                           @RequestParam(required = false) String superOperationId,
                                           @RequestParam("files") List<MultipartFile> files) throws Exception {
        log.info(
                "Upload operation documents with workOrderId: {}, operationId: {}, superOperationId: {}, files: {}",
                workOrderId, operationId, superOperationId,
                parseUtils.toJsonString(files));
        Sentry.captureMessage(String.format(
                "Upload operation documents with workOrderId: %s, operationId: %s, superOperationId: %s, files: %s",
                workOrderId, operationId, superOperationId,
                parseUtils.toJsonString(files)), SentryLevel.INFO);
        List<GlobalDocument> documents = new ArrayList<>();
        for (var file : files) {
            var request = new GlobalDocumentUploadRequest();
            request.fileBytes = file.getBytes();
            request.fileName = file.getOriginalFilename();
            request.fileType = file.getContentType();
            var entity = globalDocumentService.uploadFile(request);
            if (entity != null) {
                documents.add(entity);
            }
        }
        List<OperationDocument> result = new ArrayList<>();
        for (var gbDoc : documents) {
            OperationDocumentCreate request = OperationDocumentCreate.builder()
                    .workOrderId(workOrderId)
                    .documentId(gbDoc.getId())
                    .operationCode(operationId)
                    .superOperationCode(superOperationId)
                    .build();
            var operationDoc = operationDocumentService.save(request);
            if (operationDoc != null) {
                result.add(operationDoc);
            }
        }
        return result;
    }

    @DeleteMapping("/operation-document/delete")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_OPS_ATTACHMENT_POST)")
    public BaseResponseSAP delete(@RequestParam String workOrderId,
                                  @RequestParam String operationId,
                                  @RequestParam String documentId,
                                  @RequestParam(required = false) String superOperationId) {
        return operationDocumentService.delete(workOrderId, operationId, documentId, superOperationId);
    }

}
package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryRequest;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.service.OperationHistoryService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;

@RestController
@RequestMapping("")
@Slf4j
@RequiredArgsConstructor
public class OperationHistoryController {
    private final OperationHistoryService operationHistoryService;
    private final ParseUtils parseUtils;

    @GetMapping("/operation-histories")
    public PaginationResponse<OperationHistory> getCommonStatus(
        @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
        OperationHistoryRequest request) throws InvalidSortPropertyException {

      log.info("Get operation history with input: {}", parseUtils.toJsonString(request));
      Sentry.captureMessage("Get operation history with input: " + parseUtils.toJsonString(request), SentryLevel.INFO);

      Page<OperationHistory> page = operationHistoryService.getByFilter(request, pageable);

      return new PaginationResponse<>(page.getTotalElements(), 0,
          page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/operation-history/{id}")
    public Optional<OperationHistory> getById(@PathVariable UUID id){
      log.info("Get operation history by id: {}", id);
      Sentry.captureMessage("Get operation history by id: " + id, SentryLevel.INFO);
        return operationHistoryService.findOne(id);
    }

}

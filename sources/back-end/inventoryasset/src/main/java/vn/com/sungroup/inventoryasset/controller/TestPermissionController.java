package vn.com.sungroup.inventoryasset.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.dto.User;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

@RestController
@RequiredArgsConstructor
@RequestMapping("/test-permission")
public class TestPermissionController {
    @GetMapping("equip-view")
    @PreAuthorize("checkPermission('MTN_EQUIP_VIEW')")
    public User getUserInfo(){
        return  SecurityUtil.getCurrentUser();
    }

    @GetMapping("equip-view-create")
    @PreAuthorize("checkPermissions('MTN_EQUIP_VIEW', 'MTN_EQUIP_CREATE')")
    public User getUserInfo2(){
        return  SecurityUtil.getCurrentUser();
    }
}

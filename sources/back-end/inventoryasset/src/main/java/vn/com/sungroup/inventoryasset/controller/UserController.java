package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.dto.User;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final SAPService sapService;
    @GetMapping("/user-info")
    public User getUserInfo(@RequestHeader(name = "Authorization") String token){
        log.info("Get user info with token: {}", token);
        Sentry.captureMessage("Get user info with token: " + token, SentryLevel.INFO);
        return  sapService.getUserInfo(token);
    }
}

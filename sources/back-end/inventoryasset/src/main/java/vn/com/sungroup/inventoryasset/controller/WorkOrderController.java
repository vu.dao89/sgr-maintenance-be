package vn.com.sungroup.inventoryasset.controller;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivityDto;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangePersonalRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangeRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangeStatusRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.complete.WorkOrderCompleteRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.metric.WorkOrderMetricRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreate;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreateRequest;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeStatusResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.entity.Equipment;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.service.EquipmentService;
import vn.com.sungroup.inventoryasset.service.FuncLocationService;
import vn.com.sungroup.inventoryasset.service.NotificationService;
import vn.com.sungroup.inventoryasset.service.WorkOrderService;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.List;

import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.*;
import static vn.com.sungroup.inventoryasset.error.Errors.WORK_ORDER_NOT_FOUND;

@RestController
@RequestMapping("/api/work-order")
@Slf4j
@RequiredArgsConstructor
public class WorkOrderController {

    private final WorkOrderService workOrderService;

    private final EquipmentService equipmentService;

    private final FuncLocationService funcLocationService;
    private final PayloadUtil payloadUtil;

    private final NotificationService notificationService;

    private final ParseUtils parseUtils;

    @GetMapping
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_VIEW)")
    public WorkOrderDetailsSAP getWorkOrderDetails(String workOrderId)
            throws MasterDataNotFoundException, SAPApiException {
        log.info("Work order detail with workOrderId: {}", workOrderId);
        Sentry.captureMessage("Work order detail with workOrderId: " + workOrderId, SentryLevel.INFO);
        WorkOrderDetailsRequest request = new WorkOrderDetailsRequest(workOrderId);
        return workOrderService.getDetailsWorkOrder(request);
    }

    @GetMapping("/search")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_SEARCH)")
    public PaginationResponse<WorkOrderSearchItemSAP> workOrderSearch(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
                                                                      WorkOrderSearchRequest input)
            throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        log.info("Work order search with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Work order search with input: " + parseUtils.toJsonString(input), SentryLevel.INFO);
        var result = workOrderService.workOrderSearch(pageable, input);

        return new PaginationResponse<>(result.getTotalItems(), result.getTotalPage(),
                pageable.getPageNumber(), pageable.getPageSize(), result.getData());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_POST)")
    public BaseResponseSAP createWorkOrder(
            @RequestBody WorkOrderCreateRequest input) throws Exception {
        log.info("Work order create with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Work order create with input: " + parseUtils.toJsonString(input), SentryLevel.INFO);
        payloadUtil.validateWorkOrderCreate(input);
        payloadUtil.validateEquipmentAndFunctionalLocation(input.getWorkOrder().getEquipmentId(),
                input.getWorkOrder().getFunctionalLocation());
//    validateWorkOrderCreateWithDatabase(input.getWorkOrder());
        return workOrderService.createToSap(input);
    }

    @PutMapping
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_POST)")
    public ResponseEntity<WorkOrderChangeResponseSAP> changeWorkOrder(
            @RequestBody WorkOrderChangeRequest request)
            throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        log.info("Work order change with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Work order change with input: " + parseUtils.toJsonString(request), SentryLevel.INFO);
        payloadUtil.validateInput(request);
        payloadUtil.validateEquipmentAndFunctionalLocation(request.getEquiId(), request.getFuncLocId());
        WorkOrderChangeResponseSAP response = workOrderService.changeToSap(request);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("metric")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_VIEW)")
    public WorkOrderMetricResponse getWorkOrderMetric(WorkOrderMetricRequest input)
            throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        log.info("Work order metric with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Work order change with input: " + parseUtils.toJsonString(input), SentryLevel.INFO);
        payloadUtil.validateInput(input);
        return workOrderService.getWorkOrderMetricFromSAP(input);

    }

    @PostMapping("/start")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_CONFIRM_START)")
    public WorkOrderChangeStatusResponseSAP startWorkOrder(@RequestBody WorkOrderChangeStatusRequest input)
            throws SAPApiException, MasterDataNotFoundException, InvalidInputException {
        return workOrderService.changeStatus(input.getWorkOrderId(), WorkOrder_Release_Code,null);
    }

    @PostMapping("/lock")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).MTN_WO_LOCK)")
    public WorkOrderChangeStatusResponseSAP lockWorkOrder(@RequestBody WorkOrderChangeStatusRequest input)
            throws SAPApiException, MasterDataNotFoundException, InvalidInputException {
        return workOrderService.changeStatus(input.getWorkOrderId(), WorkOrder_Lock_Code, input.getReasonCode());
    }

    @PostMapping("/complete")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_CONFIRM_COMPLETE)")
    public WorkOrderChangeStatusResponseSAP completeWorkOrder(@RequestBody WorkOrderCompleteRequest input)
            throws Exception {

        payloadUtil.validateInput(input);

        workOrderService.validateWorkOrderComplete(input);

        if (StringUtils.hasText(input.getNotificationId())) {
            NotificationDetailResponse notificationDetail = notificationService.notificationDetail(
                    NotificationDetailRequest.builder().notificationId(input.getNotificationId()).build());
            List<NotificationActivityDto> notificationActivities = notificationDetail.getActivities()
                    .getNotificationActivities();
            notificationActivities.addAll(input.getActivities().getNotificationActivities());

            NotificationCreateRequest notificationChangeRequest = NotificationCreateRequest.builder()
                    .header(notificationDetail.getHeader())
                    .items(notificationDetail.getItems())
                    .activities(notificationDetail.getActivities())
                    .build();

            notificationService.notificationChange(notificationChangeRequest, notificationDetail.getHeader().getEquipmentStatId());
        }
        return workOrderService.changeStatus(input.getWorkOrderId(), WorkOrder_Complete_Code,null);
    }

    @PutMapping("personnel-change")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).WO_CHANGE_MAIN_PERSON)")
    public void changePersonal(@RequestBody WorkOrderChangePersonalRequest input)
            throws SAPApiException {
        workOrderService.changePersonal(input);
    }

    private void validateWorkOrderCreateWithDatabase(WorkOrderCreate workOrderCreate)
            throws InvalidInputException, SAPApiException {
        Equipment equipment;
        try {
            equipment = equipmentService.getByEquipmentId(workOrderCreate.getEquipmentId());
            payloadUtil.validateWorkOrderCreateByEquipment(workOrderCreate, equipment);
        } catch (MasterDataNotFoundException e) {
            log.warn("Equipment not found with equipmentId: {}", workOrderCreate.getEquipmentId());
            validateWorkOrderCreateByFunctionalLocation(workOrderCreate);
        }
    }

    private void validateWorkOrderCreateByFunctionalLocation(WorkOrderCreate workOrderCreate)
            throws InvalidInputException {
        FunctionalLocation functionalLocation;
        try {
            functionalLocation = funcLocationService.getByCode(workOrderCreate.getFunctionalLocation());
        } catch (MasterDataNotFoundException e) {
            log.error(e.getMessage());
            throw new InvalidInputException(WORK_ORDER_NOT_FOUND);
        }
        payloadUtil.validateWorkOrderCreateByFunctionalLocation(workOrderCreate, functionalLocation);
    }
}

package vn.com.sungroup.inventoryasset.controller.exceptionHandler;

import com.jayway.jsonpath.InvalidJsonException;
import io.sentry.Sentry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;
import vn.com.sungroup.inventoryasset.exception.ApplicationException;
import vn.com.sungroup.inventoryasset.exception.CloudException;
import vn.com.sungroup.inventoryasset.exception.DocumentNotFoundException;
import vn.com.sungroup.inventoryasset.exception.GenerateThumbnailException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.exception.OperationNotFoundException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.service.impl.RaygunServiceImpl;

import static vn.com.sungroup.inventoryasset.error.Errors.*;

@ControllerAdvice
@Slf4j
@ResponseBody
@RequiredArgsConstructor
public class
ApplicationExceptionHandler {

  private static final String FOUND_AN_ERROR = "Found an error: {}";

  private final RaygunServiceImpl raygunService;
  @Autowired
  MessageSource messageSource;
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  private ErrorDetail handleUnknownException(Exception ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ErrorDetail.builder()
        .errorCode(UNKNOWN_ERROR)
        .errorMessage(ex.getMessage())
        .build();
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.FORBIDDEN)
  private ErrorDetail handleUnknownException(AccessDeniedException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ErrorDetail.builder()
        .errorMessage(ex.getMessage())
        .build();
  }

  @ExceptionHandler({InvalidSortPropertyException.class, CloudException.class,
      GenerateThumbnailException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleInvalidParamException(ApplicationException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ex.getErrorDetail();
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleInvalidParamException(PropertyReferenceException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ErrorDetail.builder()
        .errorCode(INVALID_SORT_PROPERTY)
        .errorMessage(INVALID_SORT_PROPERTY_MESSAGE)
        .build();
  }

  @ExceptionHandler({MasterDataNotFoundException.class, DocumentNotFoundException.class,
      OperationNotFoundException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  private ErrorDetail handleNotFoundException(ApplicationException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ex.getErrorDetail();
  }

  @ExceptionHandler({InvalidInputException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleInvalidInputException(InvalidInputException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ex.getErrorDetail();
  }

  @ExceptionHandler({MissingRequiredFieldException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleMissingRequiredFieldException(MissingRequiredFieldException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ex.getErrorDetail();
  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleSAPApiException(SAPApiException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ErrorDetail.builder()
        .errorCode(SAP_API_ERROR)
        .errorMessage(ex.getMessage())
        .build();
  }
  @ExceptionHandler(ApplicationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleApplicationException(ApplicationException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ex.getErrorDetail();
  }
  @ExceptionHandler(InvalidJsonException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private ErrorDetail handleInvalidJsonException(InvalidJsonException ex) {
    log.error(FOUND_AN_ERROR, ex.getMessage(), ex);
    Sentry.captureException(ex);
    raygunService.sendError(ex);
    return ErrorDetail.builder()
        .errorCode(INVALID_JSON)
        .errorMessage(INVALID_JSON_MESSAGE)
        .build();
  }

}

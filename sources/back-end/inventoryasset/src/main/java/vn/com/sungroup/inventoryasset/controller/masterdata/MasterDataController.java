package vn.com.sungroup.inventoryasset.controller.masterdata;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.sungroup.inventoryasset.constants.NumberConstants;
import vn.com.sungroup.inventoryasset.dto.PaginationResponse;
import vn.com.sungroup.inventoryasset.dto.request.*;
import vn.com.sungroup.inventoryasset.dto.response.QrCodeResponse;
import vn.com.sungroup.inventoryasset.dto.response.equipment.EquipmentResponse;
import vn.com.sungroup.inventoryasset.dto.response.groupcode.CodeGroupResponse;
import vn.com.sungroup.inventoryasset.entity.*;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;
import vn.com.sungroup.inventoryasset.exception.*;
import vn.com.sungroup.inventoryasset.service.*;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

@RestController
@RequiredArgsConstructor
@RequestMapping("/masterdata")
@Slf4j
public class MasterDataController {

    private final ActivityKeyService activityKeyService;
    private final ActivityTypeService activityTypeService;
    private final CommonService commonService;
    private final EquipmentService equipmentService;
    private final EquipmentCategoryService equipmentCategoryService;
    private final NotificationTypeService notificationTypeService;
    private final ObjectTypeService objectTypeService;
    private final PersonalService personalService;
    private final MaintenancePlantService maintenancePlantService;
    private final SystemConditionService systemConditionService;
    private final UnitOfMeasureService unitOfMeasureService;
    private final WorkCenterService workCenterService;
    private final CharacteristicService characteristicService;

    private final WorkOrderTypeService workOrderTypeService;
    private final FuncLocationService funcLocationService;
    private final MasterDataProcessedHistoryService masterDataProcessedHistoryService;
    private final PlannerGroupService plannerGroupService;
    private final GroupCodeService groupCodeService;
    private final EmployeeService employeeService;
    private final LockReasonService lockReasonService;
    private final ParseUtils parseUtils;
    private final PayloadUtil payloadUtil;

    @GetMapping("/activity-keys")
    public PaginationResponse<ActivityKey> getActivityKeys(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, ActivityKeyRequest input)
            throws InvalidSortPropertyException {
        log.info("Get activity key with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get activity key with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<ActivityKey> result = activityKeyService.getActivityKeys(pageable, input);
        return new PaginationResponse<>(result.getTotalElements(), 0,
                result.getNumber(), result.getSize(), result.getContent());
    }

    @GetMapping("/activity-types")
    public PaginationResponse<ActivityType> getActivityTypes(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, ActivityTypeRequest input)
            throws InvalidSortPropertyException {
        log.info("Get activity type with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get activity type with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<ActivityType> page = activityTypeService.getActivityTypes(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/characteristics")
    public PaginationResponse<Characteristic> getCharacteristics(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, CharacteristicsRequest input)
            throws InvalidSortPropertyException {
        log.info("Get characteristic with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get characteristic with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<Characteristic> page = characteristicService.getCharacteristics(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/control-keys")
    public PaginationResponse<ControlKey> getControlKeys(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, ControlKeyRequest input)
            throws InvalidSortPropertyException {
        log.info("Get control jey with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get control jey with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<ControlKey> page = commonService.getControlKeys(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/control-key/{code}")
    public ControlKey getControlKey(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get control key with code: {}", code);
        Sentry.captureMessage("Get control key with code: " + code, SentryLevel.INFO);
        return commonService.getControlKey(code);
    }

    @GetMapping("/priorities")
    public PaginationResponse<PriorityType> getPriorities(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, PriorityTypeRequest input)
            throws InvalidSortPropertyException {
        log.info("Get priorities with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get priorities with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<PriorityType> priorityTypePage = commonService.getPriorityTypeByFilter(input, pageable);

        return new PaginationResponse<>(priorityTypePage.getTotalElements(), 0,
                priorityTypePage.getNumber(), priorityTypePage.getSize(), priorityTypePage.getContent());
    }

    @GetMapping("/notification-types")
    public PaginationResponse<NotificationType> getNotificationTypes(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            NotificationTypeRequest input) throws InvalidSortPropertyException {
        log.info("Get notification type with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get notification type with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<NotificationType> notificationTypePage = notificationTypeService.getNotificationTypeByFilter(
                input, pageable);

        return new PaginationResponse<>(notificationTypePage.getTotalElements(), 0,
                notificationTypePage.getNumber(), notificationTypePage.getSize(),
                notificationTypePage.getContent());
    }

    @GetMapping("/notification-type/{code}/{maintenancePlantCode}")
    public NotificationType getNotificationType(@PathVariable String code, @PathVariable String maintenancePlantCode) throws MasterDataNotFoundException {
        log.info("Get notification type with code: {}", code);
        Sentry.captureMessage("Get notification type with code: " + code,
            SentryLevel.INFO);
        return notificationTypeService.findByType(code, maintenancePlantCode);
    }

    @GetMapping("/object-types")
    public PaginationResponse<ObjectType> getObjectTypes(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            ObjectTypeRequest input
    ) throws InvalidSortPropertyException {
        log.info("Get object type with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get object type with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<ObjectType> objectTypePage = objectTypeService.getObjectTypeByFilter(input, pageable);

        return new PaginationResponse<>(objectTypePage.getTotalElements(), 0, objectTypePage.getNumber(),
                objectTypePage.getSize(), objectTypePage.getContent());
    }

    @GetMapping("/object-type/{code}")
    public ObjectType getObjectType(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get object type with code: {}", code);
        Sentry.captureMessage("Get object type with code: " + code, SentryLevel.INFO);
        return objectTypeService.getByCode(code);
    }

    @GetMapping("/personnel")
    public PaginationResponse<Personnel> getPersonnel(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            PersonnelRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get personnel with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get personnel with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<Personnel> response = personalService.getByFilter(input, pageable);
        return new PaginationResponse<>(response.getTotalElements(), 0,
                response.getNumber(), response.getSize(), response.getContent());
    }

    @GetMapping("/personnel/{code}/{workCenterCode}")
    public Personnel getPersonnel(@PathVariable String code, @PathVariable String workCenterCode) throws MasterDataNotFoundException {
        log.info("Get personnel with code: {}", code);
        Sentry.captureMessage("Get personnel with code: " + code,
            SentryLevel.INFO);
        return personalService.getByCode(code, workCenterCode);
    }

    @GetMapping("/maintenance-plants")
    public PaginationResponse<MaintenancePlant> getMaintenancePlants(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            MaintenancePlantRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get maintenance plant with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get maintenance plant with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);
        Page<MaintenancePlant> page = maintenancePlantService.getMaintenancePlants(pageable, input);

        return new PaginationResponse<>(page.getTotalElements(), 0, page.getNumber(),
                page.getSize(), page.getContent());
    }

    @GetMapping("/maintenance-plant/{code}")
    public MaintenancePlant getMaintenancePlant(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get maintenance plant with code: {}", code);
        Sentry.captureMessage("Get maintenance plant with code: " + code,
            SentryLevel.INFO);
        return maintenancePlantService.getByCode(code);
    }

    //region SystemCondition
    @GetMapping("/system-conditions")
    public PaginationResponse<SystemCondition> getSystemConditions(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, SystemConditionRequest input) throws InvalidSortPropertyException {
        log.info("Get system condition with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get system condition with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<SystemCondition> page = systemConditionService.getSystemConditions(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/system-condition/{code}")
    public SystemCondition getSystemCondition(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get system condition with code: {}", code);
        Sentry.captureMessage("Get system condition with code: " +code,
            SentryLevel.INFO);

        return systemConditionService.getByCode(code);
    }

    @GetMapping("/unit-of-measures")
    public PaginationResponse<UnitOfMeasurement> getUnitOfMeasures(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, UnitOfMeasurementRequest input) throws InvalidSortPropertyException {
        log.info("Get unit of measure with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get unit of measure with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<UnitOfMeasurement> page = unitOfMeasureService.getUnitOfMeasures(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/unit-of-measure/{code}")
    public UnitOfMeasurement getUnitOfMeasure(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get unit of measure with code: {}", code);
        Sentry.captureMessage("Get unit of measure with code: " + code, SentryLevel.INFO);

        return unitOfMeasureService.getByCode(code);
    }

    @GetMapping("/work-centers")
    public PaginationResponse<WorkCenter> getWorkCenters(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            WorkCenterRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get work center with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get work center with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<WorkCenter> workCenterResponsePage = workCenterService.getWorkCenterByFilter(input,
                pageable);
        return new PaginationResponse<>(workCenterResponsePage.getTotalElements(), 0,
                workCenterResponsePage.getNumber(), workCenterResponsePage.getSize(),
                workCenterResponsePage.getContent());
    }

    @GetMapping("/work-center/{code}")
    public WorkCenter getWorkCenter(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get work center with code: {}", code);
        Sentry.captureMessage("Get work center with code: " + code,
            SentryLevel.INFO);
        return workCenterService.getByCode(code);
    }

    @GetMapping("/work-order-types")
    public PaginationResponse<WorkOrderType> getWorkOrderTypes(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable, WorkOrderTypeRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get work order type with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get work order type with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<WorkOrderType> page = workOrderTypeService.getWorkOrderTypes(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/functional-locations")
    public PaginationResponse<FunctionalLocation> getFunctionalLocations(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            FunctionalLocationRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get functional location with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage(
            "Get functional location with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<FunctionalLocation> functionalLocationPage = funcLocationService.getFunctionalLocationByFilter(
                input, pageable);
        return new PaginationResponse<>(functionalLocationPage.getTotalElements(), 0,
                functionalLocationPage.getNumber(), functionalLocationPage.getSize(),
                functionalLocationPage.getContent());
    }

    @GetMapping("/functional-location/{code}")
    public FunctionalLocation getFunctionalLocation(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get functional location with code: {}", code);
        Sentry.captureMessage(
            "Get functional location with code: " + code,
            SentryLevel.INFO);

        return funcLocationService.getByCode(code);
    }

    @GetMapping("/equipments")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_SEARCH)")
    public PaginationResponse<EquipmentResponse> getEquipments(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            EquipmentRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get equipment with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get equipment with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<EquipmentResponse> page = equipmentService.getEquipments(pageable, input);

        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/equipment/{code}")
    @PreAuthorize("checkPermission(T(vn.com.sungroup.inventoryasset.util.security.PermissionCode).EQUIP_VIEW)")
    public Equipment getEquipment(@PathVariable String code)
        throws MasterDataNotFoundException, SAPApiException {
        log.info("Get equipment with code: {}", code);
        Sentry.captureMessage("Get equipment with code: " + code, SentryLevel.INFO);

        return equipmentService.getByEquipmentId(code);
    }

    @GetMapping("/equipment-categories")
    public PaginationResponse<EquipmentCategory> getEquipmentCategories(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            EquipmentCategoryRequest input
    ) throws InvalidSortPropertyException {
        log.info("Get equipment category with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get equipment category with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<EquipmentCategory> equipmentCategoryPage = equipmentCategoryService.findEquipmentCategoryByFilter(
                input, pageable);

        return new PaginationResponse<>(equipmentCategoryPage.getTotalElements(), 0,
                equipmentCategoryPage.getNumber(), equipmentCategoryPage.getSize(),
                equipmentCategoryPage.getContent());
    }

    @GetMapping("/equipment-category/{code}")
    public EquipmentCategory getEquipmentCategory(
            @PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get equipment with code: {}", code);
        Sentry.captureMessage("Get equipment with code: " + code,
            SentryLevel.INFO);
        return equipmentCategoryService.getByCode(code);
    }

    @GetMapping("/master-data-process-histories")
    public Page<MasterDataProcessedHistory> getFunctionalLocationById(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable)
            throws InvalidSortPropertyException {
        log.info("Get all master data process history");
        Sentry.captureMessage("Get all master data process history",
            SentryLevel.INFO);
        return masterDataProcessedHistoryService.findAll(pageable);
    }

    @GetMapping("/common-status")
    public PaginationResponse<CommonStatus> getCommonStatus(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            CommonStatusRequest request) throws InvalidSortPropertyException {
        log.info("Get common status with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Get common status with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);
        Page<CommonStatus> page = commonService.getCommonStatusByFilter(request, pageable);

        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/variance-reasons")
    public PaginationResponse<VarianceReason> getVarianceReasons(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            VarianceReasonRequest request) throws InvalidSortPropertyException {
        log.info("Get variance reason with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Get variance reason with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);
        Page<VarianceReason> page = commonService.getVarianceReasons(pageable, request);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/variance-reason/{code}")
    public VarianceReason getVarianceReason(@PathVariable String code) throws MasterDataNotFoundException {
        log.info("Get variance reason with code: {}", code);
        Sentry.captureMessage("Get variance reason with code: " + code,
            SentryLevel.INFO);
        return commonService.getVarianceReasonByCode(code);
    }

    @GetMapping("/system-status")
    public PaginationResponse<SystemStatus> getSystemStatus(
            @PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
            SystemStatusRequest request) throws InvalidSortPropertyException {
        log.info("Get system status with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Get system status with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);

        Page<SystemStatus> page = commonService.getSystemStatus(pageable, request);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/planner-groups")
    public PaginationResponse<PlannerGroup> getPlannerGroups(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
                                                             PlannerGroupRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        log.info("Get planner group with input: {}", parseUtils.toJsonString(input));
        Sentry.captureMessage("Get planner group with input: " + parseUtils.toJsonString(input),
            SentryLevel.INFO);

        Page<PlannerGroup> page = plannerGroupService.getPlannerGroups(pageable, input);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/qrcode")
    public QrCodeResponse getByQRCode(QrCodeRequest qrCodeRequest) throws InvalidSortPropertyException {
        log.info("Get qrcode with input: {}", parseUtils.toJsonString(qrCodeRequest));
        Sentry.captureMessage(
            "Get qrcode with input: " + parseUtils.toJsonString(qrCodeRequest),
            SentryLevel.INFO);

        var equipment = equipmentService.getByQrCode(qrCodeRequest.getQrCode());
        var functionalLocation = funcLocationService.getByQrCode(qrCodeRequest.getQrCode());
        return QrCodeResponse.builder().equipment(equipment).functionalLocation(functionalLocation).build();
    }

    @GetMapping("/code-groups")
    public PaginationResponse<CodeGroupResponse> getCodeGroups(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
                                                               CodeGroupRequest request) throws InvalidSortPropertyException, MissingRequiredFieldException {
        log.info("Get code group with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage(
            "Get code group with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);

        payloadUtil.validateInput(request);
        Page<CodeGroupResponse> page = groupCodeService.getCodeGroups(pageable, request);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/codes")
    public PaginationResponse<GroupCode> getCodes(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable,
                                                  CodeRequest request) throws InvalidSortPropertyException, MissingRequiredFieldException {
        log.info("Get code with input: {}", parseUtils.toJsonString(request));
        Sentry.captureMessage("Get code with input: " + parseUtils.toJsonString(request),
            SentryLevel.INFO);

        payloadUtil.validateInput(request);
        Page<GroupCode> page = groupCodeService.getCodes(pageable, request);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/employees")
    public PaginationResponse<Employee> getEmployees(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) throws InvalidSortPropertyException {
        log.info("Get all employee");
        Sentry.captureMessage("Get all employee",
            SentryLevel.INFO);
        Page<Employee> page = employeeService.getAll(pageable);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }

    @GetMapping("/lock-reasons")
    public PaginationResponse<LockReason> getLockReasons(@PageableDefault(size = NumberConstants.DEFAULT_SIZE) Pageable pageable) {
        Page<LockReason> page = lockReasonService.findAll(pageable);
        return new PaginationResponse<>(page.getTotalElements(), 0,
                page.getNumber(), page.getSize(), page.getContent());
    }
}

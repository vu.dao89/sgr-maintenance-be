/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Data;

/** AssignLog class. */
@Data
public class AssignLog {
    private String key;
    private Object value;

    public AssignLog(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "\n\tField: " + key + "\n\tValue: " + (value != null ? value.toString() : null);
    }

    public String toStringStart() {
        return "\n\t" + key + ": " + value;
    }
}

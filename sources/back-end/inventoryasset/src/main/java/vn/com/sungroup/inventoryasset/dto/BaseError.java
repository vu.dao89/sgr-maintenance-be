/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * BaseError class.
 *
 * <p>Contains information about Base Error Object
 */
@Getter
@Setter
public class BaseError {

    private String code;
    private String message;

    public BaseError(String code, String message) {
        this.code = code;
        this.message = message;
    }
}

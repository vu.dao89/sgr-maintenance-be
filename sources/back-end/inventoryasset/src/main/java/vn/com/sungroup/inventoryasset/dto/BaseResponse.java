/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * BaseResponse class.
 *
 * <p>Contains information about Base Response Object
 */
@Getter
@Setter
public class BaseResponse<T> {

    private boolean success;
    private T data;
    private ArrayList<BaseError> error = new ArrayList<>();

    public void addError(BaseError error) {
        this.error.add(error);
    }
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Data;

import org.springframework.core.io.InputStreamResource;

/**
 * FileResponseModel class.
 *
 * <p>Contains file information
 */
@Data
public class FileResponseModel {
    public String fullName;
    public String name;
    public String base64Code;
    public String path;
    public byte[] binary;
    public String extension;
    public InputStreamResource fileStream;
}

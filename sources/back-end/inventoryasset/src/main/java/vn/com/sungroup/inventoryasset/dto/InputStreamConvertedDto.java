package vn.com.sungroup.inventoryasset.dto;

import java.io.InputStream;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class InputStreamConvertedDto {
  private long size;
  private InputStream inputStream;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Data;

import java.util.List;

/**
 * JPQLModel class.
 *
 * <p>Contains information about JPQL Object
 */
@Data
public class JPQLModel<T> {
    public List<T> List;
    public int Size;
    public int Total;
}

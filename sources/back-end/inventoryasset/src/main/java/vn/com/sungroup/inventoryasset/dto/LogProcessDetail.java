/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * LogProcessDetail class.
 *
 * <p>Contains information about Log process detail
 */
@Getter
@Setter
public class LogProcessDetail {
    public static final String FORMAT_LOG_INFO =
            "PROCESS: %s \n"
                    + " Start AT: %s \n"
                    + " End AT: %s \n"
                    + " DURATION: %d ms \n"
                    + " INPUT: %s \n"
                    + " OUTPUT: %s \n";

    private String processName;
    private LocalDateTime startAt;
    private LocalDateTime endAt;
    private String duration;
    private String input;
    private String output;

    public LogProcessDetail(
            String processName,
            LocalDateTime startAt,
            LocalDateTime endAt,
            String duration,
            String input,
            String output) {
        this.processName = processName;
        this.startAt = startAt;
        this.endAt = endAt;
        this.duration = duration;
        this.input = input;
        this.output = output;
    }

    @Override
    public String toString() {
        return "\n PROCESS: "
                + processName
                + "\n START AT: "
                + startAt
                + "\n END AT: "
                + endAt
                + "\n DURATION: "
                + (startAt != null && endAt != null
                        ? Duration.between(startAt, endAt).toMillis()
                        : 0)
                + "\n INPUT: "
                + input
                + "\n OUTPUT: "
                + output;
    }
}

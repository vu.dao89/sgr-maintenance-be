package vn.com.sungroup.inventoryasset.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
@Builder
public class PaginationResponse<T> {

  private Pagination pagination;
  private List<T> items;

  public PaginationResponse(long totalItems,long totalPages, int page, int size, List<T> items) {
    this.pagination = new Pagination(totalItems,totalPages, page + 1, size);
    this.items = items;
  }

  @AllArgsConstructor
  @RequiredArgsConstructor
  @Data
  @Builder
  private static class Pagination {

    private long totalItems;
    private long totalPages;
    private int page;
    private int size;
  }
}

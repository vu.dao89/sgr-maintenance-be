/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

/**
 * RedirectUrl class.
 *
 * <p>Redirect Url
 */
public class RedirectUrl extends Exception {
    private static final long serialVersionUID = 1L;

    private String url;

    public RedirectUrl() {
        super();
    }

    public RedirectUrl(String url) {
        super();
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import vn.com.sungroup.inventoryasset.dto.permission.Department;
import vn.com.sungroup.inventoryasset.dto.permission.DepartmentRole;

/**
 * User class.
 *
 * <p>Contains information about User
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String email;
    private String picture;
    private String name;
    private String employeeId;
    private String employeeCode;
    private String departmentCode;
    private List<Department> departments;
    private List<DepartmentRole> departmentRoles;
    private String token;
}

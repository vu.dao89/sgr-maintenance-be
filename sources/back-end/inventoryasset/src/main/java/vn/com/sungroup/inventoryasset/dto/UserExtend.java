/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.permission.Department;
import vn.com.sungroup.inventoryasset.dto.permission.DepartmentRole;

import java.io.Serializable;
import java.util.List;

/**
 * User class.
 *
 * <p>Contains information about User
 */
@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserExtend extends User {
    private List<String> employeeCodes;
}

package vn.com.sungroup.inventoryasset.dto.activitykey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ActivityKeyDto {

  private UUID id;

  private String idMg;
  @CsvBindByName(column = "WO_TYPE")
  @JsonProperty("WO_TYPE")
  private String workOrderType;

  @CsvBindByName(column = "MAIN_ACT_TYPE")
  @JsonProperty("MAIN_ACT_TYPE")
  private String type;

  @CsvBindByName(column = "MAIN_ACT_TYPE_DES")
  @JsonProperty("MAIN_ACT_TYPE_DES")
  private String description;
}

package vn.com.sungroup.inventoryasset.dto.activitytype;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ActivityTypeDto extends BaseDto {

    @JsonProperty("_id")
    private Long id;

    @CsvBindByName(column = "COST_CENTER")
    @JsonProperty("COST_CENTER")
    public String costCenter;

    @CsvBindByName(column = "COST_CENTER_DES")
    @JsonProperty("COST_CENTER_DES")
    public String costCenterDes;

    @CsvBindByName(column = "ACT_TYPE")
    @JsonProperty("ACT_TYPE")
    public String actType;

    @CsvBindByName(column = "ACT_TYPE_DES")
    @JsonProperty("ACT_TYPE_DES")
    public String actTypeDes;

}

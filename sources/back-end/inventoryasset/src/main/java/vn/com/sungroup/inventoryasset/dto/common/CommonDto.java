package vn.com.sungroup.inventoryasset.dto.common;

import vn.com.sungroup.inventoryasset.dto.common.controlkey.ControlKeyDto;
import vn.com.sungroup.inventoryasset.dto.common.priority.PriorityDto;
import vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusDto;
import vn.com.sungroup.inventoryasset.dto.common.variancereason.VarianceReasonDto;

import java.util.List;

public class CommonDto {
    public List<PriorityDto> Priorities;

    public List<ControlKeyDto> ControlKeys;

    public List<VarianceReasonDto> VarianceReasons;

    public List<CommonStatusDto> CommonStatus;
}

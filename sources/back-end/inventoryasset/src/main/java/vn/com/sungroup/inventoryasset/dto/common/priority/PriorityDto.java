package vn.com.sungroup.inventoryasset.dto.common.priority;

public class PriorityDto {
    public String PRIORITY_TYPE;
    public String PRIORITY;
    public String PRIORITY_DES;
}

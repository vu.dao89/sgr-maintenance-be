package vn.com.sungroup.inventoryasset.dto.common.status;

public class CommonStatusCodeConsts {
    public static final String Notification = "1 - Notification";
    public static final String WorkOrder = "2 -Work order";
    public static final String Operation = "3 - Operation";

    public static final String  WorkOrder_New_Code = "I0001";
    public static final String WorkOrder_Release_Code = "I0002";
    public static final String WorkOrder_Complete_A_Half_Code = "I0010";
    public static final String  WorkOrder_Lock_Code = "I0043";
    public static final String  WorkOrder_Complete_Code = "I0045";
    public static final String  WorkOrder_Finish_Code = "I0009";
    public static final String Operation_Complete_A_Half_Code = "I0010";
    public static final String  Operation_Release_Code = "I0002";
    public static final String  Operation_New_Code = "I0001";
    public static final String  Operation_Complete_Code = "I0009";

    public static final String Operation_Activity_Start = "start";
    public static final String Operation_Activity_Hold = "hold";
    public static final String Operation_Activity_Complete = "complete";

    public static final String Notification_New_Code = "I0001";
    public static final String Notification_Complete_Code = "I0072";
    public static final String Notification_Release_Code = "I0002";
}

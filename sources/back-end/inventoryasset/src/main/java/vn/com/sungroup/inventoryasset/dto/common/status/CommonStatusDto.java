package vn.com.sungroup.inventoryasset.dto.common.status;

public class CommonStatusDto {
    public String OBJECT;
    public String STATUS;
    public String STATUS_DES;
}

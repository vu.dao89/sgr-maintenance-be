package vn.com.sungroup.inventoryasset.dto.equipment;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class BaseDto {

    @JsonProperty("CREATED_BY")
    private String createdBy;

    @JsonProperty("UPDATED_BY")
    private String updatedBy;

    @JsonProperty("CREATED_AT")
    private LocalDateTime createdAt;

    @JsonProperty("UPDATED_AT")
    private LocalDateTime updatedAt;

    @JsonIgnore
    private boolean deleted;
}

package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CharacteristicDto extends BaseDto {

    private UUID _id;
    private String idMg;
    @JsonProperty("EQUIPID")
    @JsonAlias("FLOC_ID")
    private String id;

    @JsonProperty("VALUEREL")
    @JsonAlias("FLOC_ATCOD")
    private String valueRel;

    @JsonProperty("OBJCLASSFLAG")
    @JsonAlias("FLOC_MAFID")
    private String objectClassFlag;

    @JsonProperty("INTERNCOUNTER")
    @JsonAlias("FLOC_ADZHL")
    private String internalCounter;

    @JsonProperty("CLASSTYPE")
    @JsonAlias("FLOC_KLART")
    private String classType;

    @JsonProperty("CHARVALCOUNTER")
    @JsonAlias("FLOC_ATZHL")
    private String charValCounter;

    @JsonProperty("CHARID")
    @JsonAlias("FLOC_ATINN")
    private String charId;

    @JsonProperty("CHARVALDESC")
    @JsonAlias("FLOC_ATWTB")
    private String charValDesc;

    @JsonProperty("CLASSID")
    @JsonAlias("FLOC_CLASS")
    private String classId;

    @JsonProperty("CHARVALUE")
    @JsonAlias("FLOC_ATWRT")
    private String charValue;

    @JsonIgnore
    private EquipmentDto equipmentDto;

    @JsonIgnore
    private FunctionalLocationDto functionalLocationDto;
}

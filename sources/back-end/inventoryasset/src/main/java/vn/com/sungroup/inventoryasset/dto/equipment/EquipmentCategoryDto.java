package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class EquipmentCategoryDto extends BaseDto{

    @CsvBindByName(column = "EQUI_CAT")
    @JsonProperty("EQUI_CAT")
    private String equipmentCategoryId;

    @CsvBindByName(column = "EQUI_CAT_DES")
    @JsonProperty("EQUI_CAT_DES")
    private String equipmentCategoryDes;
}

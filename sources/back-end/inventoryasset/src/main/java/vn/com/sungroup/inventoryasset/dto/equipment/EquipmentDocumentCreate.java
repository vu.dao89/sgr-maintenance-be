package vn.com.sungroup.inventoryasset.dto.equipment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class EquipmentDocumentCreate {
    private String equipmentCode;
    private UUID documentId;
}

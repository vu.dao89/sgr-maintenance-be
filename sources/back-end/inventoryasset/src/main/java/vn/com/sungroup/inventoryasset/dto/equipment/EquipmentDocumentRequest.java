package vn.com.sungroup.inventoryasset.dto.equipment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor()
public class EquipmentDocumentRequest {
    private String equipmentCode;
}

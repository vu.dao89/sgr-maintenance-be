package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class EquipmentDto extends BaseDto {

    private String id;
    @JsonProperty("EQUIPID")
    private String equipmentId;

    @JsonProperty("SUPERIOREQUIP")
    private String parentEquipmentId;

    @JsonProperty("CONSTYEAR")
    private String constYear;

    @JsonProperty("CONSTMONTH")
    private String constMonth;

    @JsonProperty("MAINTPLANT")
    private String maintenancePlantId;

    @JsonProperty("FUNCLOCIDINTERN")
    private String functionalLocationId;

    @JsonProperty("EQUIPCATEGORY")
    private String equipmentCategoryId;

    @JsonProperty("EQUIPDESC")
    private String description;

    @JsonProperty("LONGTEXT")
    private String longDesc;

    @JsonProperty("EQUIPTYPE")
    private String equipmentTypeId;

    @JsonProperty("LOCATION")
    private String location;

    @JsonProperty("MAINTWORKCENTER")
    private String maintenanceWorkCenterId;

    @JsonProperty("COSTCENTER")
    private String costCenter;
    @JsonProperty("COSTCENTERNAME")
    private String costCenterName;

    @JsonProperty("MANUFCOUNTRY")
    private String manuCountry;

    @JsonProperty("MANUFPARTNO")
    private String manuPartNo;

    @JsonProperty("MANUFSERIALNO")
    private String manuSerialNo;

    @JsonProperty("MANUFACTURER")
    private String manufacturer;

    @JsonProperty("MODELNUM")
    private String modelNumber;

    @JsonProperty("PLANNERGROUP")
    private String plannerGroup;

    @JsonProperty("PLANNINGPLANT")
    private String planningPlantId;

    @JsonProperty("PLANTSECTION")
    private String plantSection;

    @JsonProperty("SIZE")
    private String size;

    @JsonProperty("WEIGHT")
    private String weight;

    @JsonProperty("CREATEDATE")
    private String createDate;

    @JsonProperty("CREATETIME")
    private String createTime;

    @JsonProperty("CUS_WARRANTYEND")
    private String cusWarrantyEnd;

    @JsonProperty("CUS_WARRANTYDATE")
    private String cusWarrantyDate;

    @JsonProperty("VEN_WARRANTYEND")
    private String venWarrantyEnd;

    @JsonProperty("VEN_WARRANTYDATE")
    private String venWarrantyDate;

    @JsonProperty("QRCODE")
    private String qrCode;

    @JsonProperty("START_UP_DATE")
    private String startUpdate;

    @JsonProperty("END_OF_USE_DATE")
    private String endOfUseDate;

    @JsonProperty("CHARACTERISTICS")
    private List<CharacteristicDto> characteristics;

    @JsonProperty("MEASUREPOINTS")
    private List<MeasuringPointDto> measurePoint;

    @JsonProperty("PARTNERS")
    private List<PartnerDto> partners;

    @JsonProperty("STATUSES")
    private SystemStatusDto status;

    @JsonProperty("SORT_FIELD")
    private String sortField;
}

package vn.com.sungroup.inventoryasset.dto.equipment;

public interface EquipmentHierarchyDto {
  String getEquipmentId();
  String getDescription();
  String getParentEquipmentId();
  Integer getLevel();
}

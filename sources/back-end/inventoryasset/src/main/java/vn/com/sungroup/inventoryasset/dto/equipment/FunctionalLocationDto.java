package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class FunctionalLocationDto {

  private UUID id;
  private String idMg;
  @JsonProperty("FLOC_ID")
  private String functionalLocationId;

  @JsonProperty("SUPER_FLOC")
  private String parentFunctionalLocationId;

  @JsonProperty("CONST_YEAR")
  private String constYear;

  @JsonProperty("CONS_MONTH")
  private String constMonth;

  @JsonProperty("MAIN_PLANT")
  private String maintenancePlantId;

  @JsonProperty("FLOC_TYPE")
  private String functionalLocationTypeId;

  // TODO FIELD IN XML DOES NOT EXISTS
  @JsonProperty("MISS")
  private String functionalLocationCategory;

  @JsonProperty("FLOC_DES")
  private String description;

  @JsonProperty("LONGTEXT")
  private String longDesc;

  @JsonProperty("OBJ_TYPE")
  private String objectTypeId;

  // TODO FIELD IN XML DOES NOT EXISTS
  @JsonProperty("LOCATION")
  private String location;

  @JsonProperty("FLOC_MAIN_WC")
  private String maintenanceWorkCenterId;

  @JsonProperty("FLOC_COST_CENTER")
  private String costCenter;

  @JsonProperty("FLOC_MANU_COUTR")
  private String manuCountry;

  @JsonProperty("FLOC_MANU_PART")
  private String manuPartNo;

  @JsonProperty("FLOC_MANU_SERI")
  private String manuSerialNo;

  @JsonProperty("FLOC_MANUFACTURER")
  private String manufacturer;

  @JsonProperty("FLOC_MODEL")
  private String modelNumber;

  @JsonProperty("FLOC_PLANNER_GR")
  private String plannerGroup;

  @JsonProperty("FLOC_PLANNING_PLANT")
  private String planningPlantId;

  @JsonProperty("FLOC_PLANT_SECTION")
  private String plantSection;

  @JsonProperty("FLOC_SIZE")
  private String size;

  @JsonProperty("FLOC_WEIGHT")
  private String weight;

  @JsonProperty("FLOC_ERDAT")
  private String createDate;

  @JsonProperty("FLOC_GWLEN_1")
  private String cusWarrantyEnd;

  @JsonProperty("FLOC_GWLDT_1")
  private String cusWarrantyDate;

  @JsonProperty("FLOC_GWLEN_2")
  private String venWarrantyEnd;

  @JsonProperty("FLOC_GWLDT_2")
  private String venWarrantyDate;

  @JsonProperty("QRCODE")
  private String qrCode;

  @JsonProperty("START_UP_DATE")
  private String startupDate;

  @JsonProperty("END_OF_USE_DATE")
  private String endOfUseDate;

  @JsonProperty("CHARACTERISTICS")
  private List<CharacteristicDto> characteristics;

  @JsonProperty("MEASUREPOINTS")
  private List<MeasuringPointDto> measurePoint;
}

package vn.com.sungroup.inventoryasset.dto.equipment;

import java.util.List;

public class GetEquipmentsInput {
    public String Filter;
    public List<String> WorkCenterId;
    public List<String> FunctionalLocationId;
    public List<String> EquipmentCategoryId;
    public List<String> ObjectTypeId;
    public List<String> PlannerGroupId;
    public String SortField;
}

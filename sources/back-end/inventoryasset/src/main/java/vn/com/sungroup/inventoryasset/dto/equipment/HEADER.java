package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class HEADER {
    public List<EquipmentDto> EQUIPMENTDtos;
}

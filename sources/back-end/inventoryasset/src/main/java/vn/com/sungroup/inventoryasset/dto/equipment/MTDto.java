package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class MTDto<T> {
    @JsonProperty("HEADER")
    public List<T> header;
    @JsonProperty("CHARACTERISTICS")
    public List<CharacteristicDto> characteristics;
    @JsonProperty("PARTNERS")
    public List<PartnerDto> partners;
    @JsonProperty("STATUSES")
    public List<SystemStatusDto> statuses;
    @JsonProperty("MEASUREPOINT")
    public List<MeasuringPointDto> measurePoint;
}

package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MeasPointDto {

    private String measPoint;
    private String measDocDate;
    private String measDocTime;
    private String measDocText;
    private String measDocLText;
    private String readBy;
    private String measRead;
    private String countRead;
    private String diff;
    private String workOrder;
    private String operation;
    private String countReadId;
    private Boolean isMeasDiff;
}

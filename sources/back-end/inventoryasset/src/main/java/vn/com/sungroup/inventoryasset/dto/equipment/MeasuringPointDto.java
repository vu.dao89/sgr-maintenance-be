package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MeasuringPointDto extends BaseDto {

    private UUID _id;
    private String idMg;
    @JsonProperty("EQUIPID")
    @JsonAlias("FLOC_ID")
    private String id;

    @JsonProperty("MEAS_POINT")
    private String point;

    @JsonProperty("MEAS_POINT_POSITION")
    @JsonAlias("MEAS_POSITION")
    private String position;

    @JsonProperty("DESCRIPTION")
    @JsonAlias("MEAS_POINT_DES")
    private String description;

    @JsonProperty("CHARACTERISTIC")
    private String characteristic;

    @JsonProperty("TARGET_VALUE")
    private String targetValue;

    @JsonProperty("COUNTER")
    private String counter;

    @JsonProperty("LOWER_RANGE")
    @JsonAlias("MEAS_LOWER")
    private String lowerRangeLimit;

    @JsonProperty("UPPER_RANGE")
    @JsonAlias("MEAS_UPPER")
    private String upperRangeLimit;

    @JsonProperty("RANGEUNIT")
    @JsonAlias("MEAS_UNIT")
    private String rangeUnit;

    @JsonProperty("TEXT")
    private String text;

    @JsonProperty("CODE_GROUP")
    @JsonAlias("MEAS_CODE_GROUP")
    private String codeGroup;
}

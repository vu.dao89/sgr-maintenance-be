package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PartnerDto extends BaseDto {

    private UUID _id;
    @JsonProperty("EQUIPID")
    @JsonAlias("FLOC_ID")
    private String id;

    @JsonProperty("COUNTER")
    @JsonAlias("FLOC_COUNTER")
    private String counter;

    @JsonProperty("PARTNERFUNCTION")
    @JsonAlias("FLOC_PARVW")
    private String partnerFunction;

    @JsonProperty("OBJECTTYPE")
    @JsonAlias("FLOC_OBTYP")
    private String objectTypeCode;

    @JsonProperty("PARTNERNUM")
    @JsonAlias("FLOC_PARNR")
    private String partnerNumber;


}

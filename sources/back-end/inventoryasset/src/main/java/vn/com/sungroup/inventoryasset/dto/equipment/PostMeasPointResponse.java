package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.*;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class PostMeasPointResponse {

    @JsonSetter("Document")
    private String document;

    @JsonSetter("Code")
    private String code;

    @JsonSetter("Message")
    private String message;
}

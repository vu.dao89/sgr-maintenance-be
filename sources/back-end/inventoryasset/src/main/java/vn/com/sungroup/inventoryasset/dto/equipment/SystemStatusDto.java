package vn.com.sungroup.inventoryasset.dto.equipment;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SystemStatusDto extends BaseDto {

    @JsonProperty("EQUIPID")
    @JsonAlias("FLOC_ID")
    private String id;

    @JsonProperty("ID")
    @JsonAlias("FLOC_STAT_ID")
    private String statusId;

    @JsonProperty("CODE")
    @JsonAlias("FLOC_STAT_COD")
    private String code;

    @JsonProperty("PROFILE")
    @JsonAlias("FLOC_USER_PROF")
    private String profile;

    @JsonProperty("USERSTATUS")
    @JsonAlias("FLOC_USER_STATUS")
    private String userStatus;

    @JsonProperty("DESCRIPTION")
    @JsonAlias("FLOC_STAT_DES")
    private String description;
}

package vn.com.sungroup.inventoryasset.dto.equipment.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
public class MeasPoint {

    @JsonSetter("MEAS_POINT")
    private String measPoint;

    @JsonSetter("MEAS_DOC_DATE")
    private String measDocDate;

    @JsonSetter("MEAS_DOC_TIME")
    private String measDocTime;

    @JsonSetter("MEAS_DOC_TEXT")
    private String measDocText;

    @JsonSetter("MEAS_DOC_LTEXT")
    private String measDocLText;

    @JsonSetter("READ_BY")
    private String readBy;

    @JsonSetter("MEAS_READ")
    private String measRead;

    @JsonSetter("COUNT_READ")
    private String countRead;

    @JsonSetter("DIFF")
    private String diff;
    @JsonSetter("WORK_ORDER")
    private String workOrder;
    @JsonSetter("OPERATION")
    private String operation;
    @JsonSetter("COUNT_READ_ID")
    private String countReadId;

    @Setter
    private String email;
}
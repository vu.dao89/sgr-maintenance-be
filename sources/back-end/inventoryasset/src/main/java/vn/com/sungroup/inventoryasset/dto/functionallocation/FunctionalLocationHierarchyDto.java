package vn.com.sungroup.inventoryasset.dto.functionallocation;

public interface FunctionalLocationHierarchyDto {
  String getFunctionalLocationId();
  String getDescription();
  String getParentFunctionalLocationId();
  Integer getLevel();
}

package vn.com.sungroup.inventoryasset.dto.functionallocation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.hierarchy.Element;

@Builder
@Getter
@Setter
public class FunctionalLocationHierarchyResponse implements Element<String> {

  private String id;
  private String description;
  private String parentId;
  private Integer level;
  private boolean isSelected;
}

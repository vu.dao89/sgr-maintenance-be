package vn.com.sungroup.inventoryasset.dto.functionallocation;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GetFunctionalLocationsRequest {

    public GetFunctionalLocationsRequest()
    {
        maintenancePlantCodes = new ArrayList<>();
    }
    private List<String> maintenancePlantCodes;
}

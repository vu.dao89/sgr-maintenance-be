package vn.com.sungroup.inventoryasset.dto.globaldocument;

public class GlobalDocumentUploadRequest {
    public byte[] fileBytes;
    public String fileName;
    public String fileType;
}

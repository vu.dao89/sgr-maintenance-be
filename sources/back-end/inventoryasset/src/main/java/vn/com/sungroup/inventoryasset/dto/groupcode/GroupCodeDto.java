package vn.com.sungroup.inventoryasset.dto.groupcode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class GroupCodeDto extends BaseDto {
    private UUID id;
    private String idMg;
    @CsvBindByName(column = "CATALOG_PROF")
    @JsonProperty("CATALOG_PROF")
    private String catalogProfile;
    @CsvBindByName(column = "CATALOG_PROF_DES")
    @JsonProperty("CATALOG_PROF_DES")
    private String catalogProfileDes;
    @CsvBindByName(column = "CATALOG")
    @JsonProperty("CATALOG")
    private String catalog;
    @CsvBindByName(column = "CATALOG_DES")
    @JsonProperty("CATALOG_DES")
    private String catalogDes;
    @CsvBindByName(column = "CODE_GROUP")
    @JsonProperty("CODE_GROUP")
    private String codeGroup;
    @CsvBindByName(column = "CODE_GROUP_DES")
    @JsonProperty("CODE_GROUP_DES")
    private String codeGroupDes;
    @CsvBindByName(column = "CODE")
    @JsonProperty("CODE")
    private String code;
    @CsvBindByName(column = "CODE_DES")
    @JsonProperty("CODE_DES")
    private String codeDes;
}

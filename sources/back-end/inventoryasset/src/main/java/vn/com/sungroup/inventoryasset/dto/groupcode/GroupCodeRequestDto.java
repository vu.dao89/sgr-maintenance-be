package vn.com.sungroup.inventoryasset.dto.groupcode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupCodeRequestDto {

  private String catalogProfile;
  private String catalog;
  private String codeGroup;
  private String codeGroupDescription;
  private String code;
  private String codeDescription;
}

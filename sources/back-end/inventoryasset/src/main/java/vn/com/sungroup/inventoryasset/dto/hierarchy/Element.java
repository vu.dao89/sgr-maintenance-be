package vn.com.sungroup.inventoryasset.dto.hierarchy;

public interface Element<R> {

  R getId();

  R getParentId();

  default boolean checkId(R id) {
    return this.getId().equals(id);
  }
}

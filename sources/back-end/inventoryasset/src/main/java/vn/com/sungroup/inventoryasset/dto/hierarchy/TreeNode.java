package vn.com.sungroup.inventoryasset.dto.hierarchy;

import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TreeNode<T extends Element<?>> {

  private T element;

  private Collection<TreeNode<T>> children;

  public TreeNode(T element) {
    this.element = element;
  }
}

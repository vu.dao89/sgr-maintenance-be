/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * AssetDetail class.
 *
 * <p>Contains information about AssetDetail
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetDetail implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String asset;
    private String equipment;
    private String costcenter;
    private String asset_group;
    private String model;
    private String avatar;
    private String person;
    private String description;
    private String serial;
    private String quantity;
    private String unit;
    private String use_date;
    private String manufacture;
    private String inventory_type;
    private String vendor;
    private String latitude;
    private String longitude;
    private String qrcode;
    private List<ImageModel> image;
    private String costcenter_name;
    private String asset_groupname;
    private String room;
    private String person_name;
    private InventoryDTO inventory;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetGetCostcenterData class.
 *
 * <p>Contains information about AssetGetCostcenterData
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetGetCostcenterData implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String costcenter;
    private String costcenter_name;
}

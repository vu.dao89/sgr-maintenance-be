/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * AssetHistoryChgDTO class.
 *
 * <p>Contains information about AssetHistoryChgDTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetHistoryChgDTO implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String qrcode;
    private String name;
    private String serial;
    private String quantity;
    private String asset_group;
    private String use_date;
    private String costcenter;
    private String inventory_type;
    private String manufacture;
    private String vendor;
    private String model;
    private String person;
    private String latitude;
    private String longitude;
    private String user_email;
    private String room;
    private String type;
    private String change_date;
    private String change_time;
    private String equipment;
    private String unit;
    private List<ImageModel> image;
    private String asset;
}

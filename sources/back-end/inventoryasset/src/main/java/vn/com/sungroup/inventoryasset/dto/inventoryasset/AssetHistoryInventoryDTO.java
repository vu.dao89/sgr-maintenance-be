/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * AssetHistoryInventoryDTO class.
 *
 * <p>Contains information about AssetHistoryInventoryDTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetHistoryInventoryDTO implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String qrcode;
    private String asset;
    private String costcenter;
    private String costcenter_name;
    private String quantity;
    private String status;
    private String description;
    private String latitude;
    private String longitude;
    private String date;
    private String time;
    private List<ImageModel> image;
    private String equipment;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetModify class.
 *
 * <p>Contains information about AssetModify
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetModify implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String id;
    private String number;
    private String msg;
}

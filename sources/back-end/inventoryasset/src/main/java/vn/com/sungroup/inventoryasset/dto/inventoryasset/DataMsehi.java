/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DataMsehi class.
 *
 * <p>Contains information about DataMsehi
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataMsehi implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String unit;
    private String unit_description;
}

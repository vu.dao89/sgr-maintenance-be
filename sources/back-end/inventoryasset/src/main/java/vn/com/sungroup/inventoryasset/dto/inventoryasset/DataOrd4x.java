/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DataOrd4x class.
 *
 * <p>Contains information about DataOrd4x
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataOrd4x implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String asset_group;
    private String asset_groupname;
}

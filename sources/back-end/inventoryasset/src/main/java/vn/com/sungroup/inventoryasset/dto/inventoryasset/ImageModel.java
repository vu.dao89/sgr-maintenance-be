/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ImageModel class.
 *
 * <p>Contains information about ImageModel
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageModel implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String imageid;
}

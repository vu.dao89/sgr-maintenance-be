/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * InventoryDTO class.
 *
 * <p>Contains information about InventoryDTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryDTO implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String inv_date;
    private String inv_time;
    private String status;
    private String description;
    private String quantity;
}

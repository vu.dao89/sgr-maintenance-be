/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.inventoryasset;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ResponseMoveFileDto class.
 *
 * <p>Contains information about ResponseMoveFileDto
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMoveFileDto implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String container;
    private String blob;
    private String url;
    private String type;
    private String appCode;
    private String createdId;
    private String createdName;
}

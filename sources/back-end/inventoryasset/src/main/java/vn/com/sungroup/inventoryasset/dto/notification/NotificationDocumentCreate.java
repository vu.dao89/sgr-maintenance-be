package vn.com.sungroup.inventoryasset.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class NotificationDocumentCreate {
    private String notificationCode;
    private UUID documentId;
}

package vn.com.sungroup.inventoryasset.dto.notification;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor()
public class NotificationDocumentRequest {
    private String notificationCode;
}

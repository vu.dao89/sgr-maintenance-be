package vn.com.sungroup.inventoryasset.dto.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NotificationTypeDto extends BaseDto {

  @Setter
  private UUID id;
  @Setter
  private String idMg;
  @CsvBindByName(column = "NOTI_TYPE")
  @JsonProperty("NOTI_TYPE")
  private String notiType;

  @CsvBindByName(column = "NOTI_TYPE_DES")
  @JsonProperty("NOTI_TYPE_DES")
  private String description;

  @CsvBindByName(column = "CATALOG_PROF")
  @JsonProperty("CATALOG_PROF")
  private String catalogProfile;

  @CsvBindByName(column = "PROBLEMS")
  @JsonProperty("PROBLEMS")
  private String problems;

  @CsvBindByName(column = "CAUSES")
  @JsonProperty("CAUSES")
  private String causes;

  @CsvBindByName(column = "ACTIVITIES")
  @JsonProperty("ACTIVITIES")
  private String activities;

  @CsvBindByName(column = "OBJECTPARTS")
  @JsonProperty("OBJECTPARTS")
  private String objectParts;

  @CsvBindByName(column = "WO_TYPE")
  @JsonProperty("WO_TYPE")
  private String workOrderTypeId;

  @CsvBindByName(column = "PRIORITY_TYPE")
  @JsonProperty("PRIORITY_TYPE")
  private String priorityTypeId;

  @CsvBindByName(column = "PARVW_VERA")
  @JsonProperty("PARVW_VERA")
  private String personResponsible;
}

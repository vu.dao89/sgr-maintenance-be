package vn.com.sungroup.inventoryasset.dto.notification.detail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NotificationActivitiesDto {
    private List<NotificationActivityDto> notificationActivities;
}

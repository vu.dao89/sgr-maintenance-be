package vn.com.sungroup.inventoryasset.dto.notification.detail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationActivityDto {
    private String id;
    private String activityId;
    private String activity;
    private String activityDesc;
    private String activityCode;
    private String activityCodeDesc;
    private String activityText;
}

package vn.com.sungroup.inventoryasset.dto.notification.detail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationHeaderDto {

    private String id;
    @NotBlank(message = "type is required")
    private String type;
    @NotBlank(message = "description is required")
    private String desc;
    @JsonProperty("lText")
    private String longText;
    private String priorityType;

    @NotBlank(message = "priority is required")
    private String priority;
    private String createOn;
    private String createAt;
    private String equipmentId;
    private String equipmentDesc;
    private String functionalLocationId;
    @NotBlank(message = "date is required")
    private String date;
    @NotBlank(message = "time is required")
    private String time;
    private String reportBy;
    private String reportByName;
    @NotBlank(message = "requestStart is required")
    private String requestStart;
    @NotBlank(message = "requestStartTime is required")
    private String requestStartTime;
    private String requestEnd;
    private String requestEndTime;
    @NotBlank(message = "malfunctionStart is required")
    private String malfunctionStart;
    @NotBlank(message = "malfunctionStartTime is required")
    private String malfunctionStartTime;
    private String malfunctionEnd;
    private String malfunctionEndTime;
    private String breakDown;
    private String breakDownDuration;
    private String breakDownDurationUnit;
    private String order;
    private String completedDate;
    private String completedTime;
    private String categoryProfile;
    private String plannerGroup;
    private String plannerGroupText;
    private String sortField;
    private String workCenter;
    private String workCenterDesc;
    private String maintenancePlant;
    private String equipmentStatId;
    private String equipmentStatIdDesc;
    private String maintenancePlantDesc;
    @NotBlank(message = "assignTo is required")
    private String assignTo;
    private String assignName;

    private String typeDesc;

    private String functionalLocationDesc;

    private PriorityType priorityTypeObject;
    private CommonStatus commonStatus;
    private String email;
}

package vn.com.sungroup.inventoryasset.dto.notification.detail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationItemDto {
    private String Id;
    private String item;
    private String part;
    private String partDesc;
    private String partCode;
    private String partCodeDesc;
    private String problem;
    private String problemDesc;
    private String dame;
    private String dameDesc;
    private String cause;
    private String causeDesc;
    private String causeCode;
    private String causeCodeDesc;
    private String idText;
    private String causeText;
}

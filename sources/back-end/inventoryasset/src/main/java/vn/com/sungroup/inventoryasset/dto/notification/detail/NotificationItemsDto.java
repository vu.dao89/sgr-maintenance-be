package vn.com.sungroup.inventoryasset.dto.notification.detail;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationItemsDto {
    private List<NotificationItemDto> notificationItems;
}

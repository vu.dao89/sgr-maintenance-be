package vn.com.sungroup.inventoryasset.dto.notification.detail.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationActivities implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("ACTIVITY") private List<NotificationActivity> notificationActivities;
}

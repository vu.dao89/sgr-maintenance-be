package vn.com.sungroup.inventoryasset.dto.notification.detail.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationActivity implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("NOTIF_ID") private String id;
    @JsonProperty("NOTIF_ACTIVITY_ID") private String activityId;
    @JsonProperty("NOTIF_ACTIVITY") private String activity;
    @JsonProperty("NOTIF_ACTIVITY_DES") private String activityDesc;
    @JsonProperty("NOTIF_ACTIVITY_CODE") private String activityCode;
    @JsonProperty("NOTIF_ACTIVITY_CODE_DES") private String activityCodeDesc;
    @JsonProperty("ACTIVITY_TEXT") private String activityText;
}

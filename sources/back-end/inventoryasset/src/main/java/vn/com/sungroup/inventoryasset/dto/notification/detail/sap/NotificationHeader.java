package vn.com.sungroup.inventoryasset.dto.notification.detail.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationHeader implements Serializable {

    @JsonProperty("NOTIF_ID")
    private String id;

    @JsonProperty("NOTIF_TYPE")
    @NotBlank(message = "Notification Type")
    private String type;

    @JsonProperty("NOTIF_DES")
    private String desc;

    @JsonProperty("NOTIF_LTEXT")
    private String longText;

    @JsonProperty("PRIORITY_TYPE")
    private String priorityType;

    @JsonProperty("PRORITY")
    @NotBlank(message = "Priority")
    private String priority;

    @JsonProperty("NOTIF_CREAT_ON")
    private String createOn;

    @JsonProperty("NOTIF_CREAT_AT")
    private String createAt;

    @JsonProperty("EQUI_ID")
    @NotBlank(message = "Equipment")
    private String equipmentId;

    @JsonProperty("EQ_EQUI_DES")
    private String equipmentDesc;

    @JsonProperty("FUNC_LOC_ID")
    @NotBlank(message = "Functional Location")
    private String functionalLocationId;

    @JsonProperty("NOTIF_DATE")
    @NotBlank(message = "Notification Date")
    private String date;

    @JsonProperty("NOTIF_TIME")
    @NotBlank(message = "Notification Time")
    private String time;

    @JsonProperty("NOTIF_REPORT_BY")
    private String reportBy;

    @JsonProperty("REPORT_BY_NAME")
    private String reportByName;

    @JsonProperty("REQ_START")
    @NotBlank(message = "Required start")
    private String requestStart;

    @JsonProperty("REQ_START_TIME")
    @NotBlank(message = "Required Start Time")
    private String requestStartTime;

    @JsonProperty("REQ_END")
    private String requestEnd;

    @JsonProperty("REQ_END_TIME")
    private String requestEndTime;

    @JsonProperty("MAIF_START")
    @NotBlank(message = "Malfunction Start")
    private String malfunctionStart;

    @JsonProperty("MAIF_START_TIME")
    @NotBlank(message = "Malfunction Start Time")
    private String malfunctionStartTime;

    @JsonProperty("MAIF_END")
    private String malfunctionEnd;
    @JsonProperty("MAIF_END_TIME")
    private String malfunctionEndTime;
    @JsonProperty("BREAKDOWN")
    private String breakDown;
    @JsonProperty("BREAKDOWN_DURATION")
    private String breakDownDuration;
    @JsonProperty("BREAKDOWN_DURATION_UNIT")
    private String breakDownDurationUnit;
    @JsonProperty("ORDER")
    private String order;
    @JsonProperty("NOTIF_COMP_DATE")
    private String completedDate;
    @JsonProperty("NOTIF_COMP_TIME")
    private String completedTime;
    @JsonProperty("CAT_PROFILE")
    private String categoryProfile;
    @JsonProperty("PLANNER_GROUP")
    private String plannerGroup;
    @JsonProperty("PLANNER_GROUP_TEXT")
    private String plannerGroupText;
    @JsonProperty("SORT_FIELD")
    private String sortField;
    @JsonProperty("WORK_CENTER")
    private String workCenter;
    @JsonProperty("WORK_CENTER_DES")
    private String workCenterDesc;
    @JsonProperty("MAIN_PLANT")
    private String maintenancePlant;
    @JsonProperty("EQ_STAT_ID")
    private String equipmentStatId;
    @JsonProperty("EQ_STAT_ID_DES")
    private String equipmentStatIdDesc;
    @JsonProperty("MAIN_PLANT_DES")
    private String maintenancePlantDesc;

    @JsonProperty("ASSIGN_TO")
    @NotBlank(message = "Assign To")
    private String assignTo;
    @JsonProperty("ASSIGN_NAME")
    private String assignName;

    @JsonProperty("NOTI_TYPE_DES")
    private String typeDesc;
    @JsonProperty("FLOC_DES")
    private String functionalLocationDesc;
    @JsonProperty("EMAIL")
    private String email;
}

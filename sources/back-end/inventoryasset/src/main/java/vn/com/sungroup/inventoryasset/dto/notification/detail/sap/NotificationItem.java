package vn.com.sungroup.inventoryasset.dto.notification.detail.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationItem implements Serializable {

    @JsonProperty("NOTIF_ID") private String Id;
    @JsonProperty("NOTIF_ITEM") private String item;
    @JsonProperty("NOTIF_PART") private String part;
    @JsonProperty("NOTIF_PART_DES") private String partDesc;
    @JsonProperty("NOTIF_PART_CODE") private String partCode;
    @JsonProperty("NOTIF_PART_CODE_DES") private String partCodeDesc;
    @JsonProperty("NOTIF_PROBLEM") private String problem;
    @JsonProperty("NOTIF_PROBLEM_DES") private String problemDesc;
    @JsonProperty("NOTIF_DAME") private String dame;
    @JsonProperty("NOTIF_DAME_DES") private String dameDesc;
    @JsonProperty("CAUSE") private String cause;
    @JsonProperty("CAUSE_DES") private String causeDesc;
    @JsonProperty("CAUSE_CODE") private String causeCode;
    @JsonProperty("CAUSE_CODE_DES") private String causeCodeDesc;
    @JsonProperty("NOTIF_ID_TEXT") private String idText;
    @JsonProperty("NOTIF_CAUSE_TEXT") private String causeText;
}

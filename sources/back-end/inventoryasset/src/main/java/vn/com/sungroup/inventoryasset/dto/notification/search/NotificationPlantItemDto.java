package vn.com.sungroup.inventoryasset.dto.notification.search;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Builder
@Getter
@Setter

public class NotificationPlantItemDto implements Serializable {

    private String maintenancePlan;
}

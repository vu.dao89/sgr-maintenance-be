package vn.com.sungroup.inventoryasset.dto.notification.search;

import lombok.*;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationSearchResultDto implements Serializable {
    private String id;
    private String notificationType;
    private String notificationTypeDesc;
    private String description;
    private String priority;
    private String createdOn;
    private String createdAt;
    private String equipmentId;
    private String equipmentDescription;
    private String functionalLocationId;
    private String functionalLocationDesc;
    private String notificationDate;
    private String notificationTime;
    private String notificationReportedBy;
    private String reportedByName;
    private String requestStart;
    private String requestStartTime;
    private String requestEnd;
    private String requestEndTime;
    private String maifStart;
    private String maifStartTime;
    private String maifEnd;
    private String maifEndTime;
    private String completeDate;
    private String completeTime;
    private String plannerGroup;
    private String plannerGroupDesc;
    private String workCenter;
    private String maintenancePlant;
    private String equipmentStatId;
    private String equipmentStatDesc;
    private String assignTo;
    private String assignName;
    private String causeText;

    private PriorityType priorityType;
    private CommonStatus commonStatus;
    private List<NotificationDocument> notificationDocuments;
}

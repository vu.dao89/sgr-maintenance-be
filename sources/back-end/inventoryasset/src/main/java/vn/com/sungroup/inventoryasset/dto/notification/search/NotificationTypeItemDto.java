package vn.com.sungroup.inventoryasset.dto.notification.search;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class NotificationTypeItemDto implements Serializable {
    private String notificationType;
}

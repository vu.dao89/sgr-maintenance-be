package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.io.Serializable;

@Builder
public class NotificationEquipmentItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("EQUI_ID") private String EQUI_ID;
}

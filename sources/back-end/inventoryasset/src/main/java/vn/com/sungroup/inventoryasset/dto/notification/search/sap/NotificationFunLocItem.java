package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.io.Serializable;

@Builder
public class NotificationFunLocItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("FUNC_LOC_ID") private String FUNC_LOC_ID;
}

package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.io.Serializable;

@Builder
public class NotificationPlannerGroupItem implements Serializable {
    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("PLANNER_GROUP") private String PLANNER_GROUP;

}

package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationPlantItem implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("MAIN_PLANT")
    private String MAIN_PLANT;
}

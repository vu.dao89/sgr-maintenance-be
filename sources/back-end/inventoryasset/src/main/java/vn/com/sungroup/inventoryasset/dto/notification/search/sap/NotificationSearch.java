package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationSearch implements Serializable {

    @JsonProperty("NOTIF_ID") private String id;
    @JsonProperty("NOTIF_TYPE") private String notificationType;
    @JsonProperty("NOTI_TYPE_DES") private String notificationTypeDesc;
    @JsonProperty("NOTIF_DES") private String description;
    @JsonProperty("PRORITY") private String priority;
    @JsonProperty("NOTIF_CREAT_ON") private String createdOn;
    @JsonProperty("NOTIF_CREAT_AT") private String createdAt;
    @JsonProperty("EQUI_ID") private String equipmentId;
    @JsonProperty("EQ_EQUI_DES") private String equipmentDescription;
    @JsonProperty("FUNC_LOC_ID") private String functionalLocationId;
    @JsonProperty("FLOC_DES") private String functionalLocationDesc;
    @JsonProperty("NOTIF_DATE") private String notificationDate;
    @JsonProperty("NOTIF_TIME") private String notificationTime;
    @JsonProperty("NOTIF_REPORT_BY") private String notificationReportedBy;
    @JsonProperty("REPORT_BY_NAME") private String reportedByName;
    @JsonProperty("REQ_START") private String requestStart;
    @JsonProperty("REQ_START_TIME") private String requestStartTime;
    @JsonProperty("REQ_END") private String requestEnd;
    @JsonProperty("REQ_END_TIME") private String requestEndTime;
    @JsonProperty("MAIF_START") private String maifStart;
    @JsonProperty("MAIF_START_TIME") private String maifStartTime;
    @JsonProperty("MAIF_END") private String maifEnd;
    @JsonProperty("MAIF_END_TIME") private String maifEndTime;
    @JsonProperty("NOTIF_COMP_DATE") private String completeDate;
    @JsonProperty("NOTIF_COMP_TIME") private String completeTime;
    @JsonProperty("PLANNER_GROUP") private String plannerGroup;
    @JsonProperty("PLANNER_GROUP_DES") private String plannerGroupDesc;
    @JsonProperty("WORK_CENTER") private String workCenter;
    @JsonProperty("MAIN_PLANT") private String maintenancePlant;
    @JsonProperty("EQ_STAT_ID") private String equipmentStatId;
    @JsonProperty("EQ_STAT_ID_DES") private String equipmentStatDesc;
    @JsonProperty("ASSIGN_TO") private String assignTo;
    @JsonProperty("ASSIGN_NAME") private String assignName;
    @JsonProperty("NOTIF_CAUSE_TEXT") private String causeText;
}

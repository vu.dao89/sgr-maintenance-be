package vn.com.sungroup.inventoryasset.dto.notification.search.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.io.Serializable;

@Builder
public class NotificationStatusItem implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("EQ_STAT_ID") private String EQ_STAT_ID;
}

package vn.com.sungroup.inventoryasset.dto.objecttype;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ObjectTypeDto extends BaseDto {
    @JsonProperty("_id")
    private Long id;

    @CsvBindByName(column = "OBJ_TYPE")
    @JsonProperty("OBJ_TYPE")
    public String objectTypeId;

    @CsvBindByName(column = "OBJ_TYPE_DES")
    @JsonProperty("OBJ_TYPE_DES")
    public String objectDescription;
}

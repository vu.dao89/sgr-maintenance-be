package vn.com.sungroup.inventoryasset.dto.operation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class OperationDocumentCreate {
    private String workOrderId;
    private String operationCode;
    private String superOperationCode;
    private UUID documentId;
}

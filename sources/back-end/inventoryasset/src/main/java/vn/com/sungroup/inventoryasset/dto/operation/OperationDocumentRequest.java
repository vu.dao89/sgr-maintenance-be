package vn.com.sungroup.inventoryasset.dto.operation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class OperationDocumentRequest {
    private String workOrderId;
    private String operationCode;
    private String superOperationCode;
}

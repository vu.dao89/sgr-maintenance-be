/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** AssignLog class. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionRequestWF {
    
    private String msgCode;
    private String msg;
}

package vn.com.sungroup.inventoryasset.dto.personal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvIgnore;
import java.time.LocalDate;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PersonalDto extends BaseDto {
    @JsonProperty("_id")
    private UUID id;

    private String idPer;

    @CsvBindByName(column = "PERSONNEL")
    @JsonProperty("PERSONNEL")
    public String personelId;

    @CsvBindByName(column = "PER_START_DATE")
    @JsonIgnore
    public String perStartDateCsv;

    @JsonProperty("PER_START_DATE")
    @CsvIgnore
    public LocalDate perStartDate;

    @CsvBindByName(column = "PER_END_DATE")
    @JsonIgnore
    public String perEndDateCsv;

    @JsonProperty("PER_END_DATE")
    @CsvIgnore
    public LocalDate perEndDate;
    @CsvBindByName(column = "PERSONNEL_NAME")
    @JsonProperty("PERSONNEL_NAME")
    public String personelName;

    @CsvBindByName(column = "WORK_CENTER")
    @JsonIgnore
    public String workCenterId;

    @CsvBindByName(column = "PLANT")
    @JsonIgnore
    public String plantId;

    @JsonProperty("WORK_CENTER")
    @CsvIgnore
    public String workCenter;

    @JsonProperty("PLANT")
    @CsvIgnore
    public String plant;
}

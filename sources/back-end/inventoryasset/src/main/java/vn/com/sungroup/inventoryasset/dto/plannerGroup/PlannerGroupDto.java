package vn.com.sungroup.inventoryasset.dto.plannerGroup;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlannerGroupDto {
    @Setter
    private String id;
    @CsvBindByName(column = "MAIN_PLANT")
    private String maintenancePlantId;

    @CsvBindByName(column = "PLANNER_GROUP")
    private String plannerGroupId;
    @CsvBindByName(column = "PLANNER_GROUP_NAME")
    private String name;
}

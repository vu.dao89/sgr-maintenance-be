package vn.com.sungroup.inventoryasset.dto.plant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MaintenancePlantDto extends BaseDto {

  @CsvBindByName(column = "PLANT")
  @JsonProperty("PLANT")
  private String code;

  @CsvBindByName(column = "PLANT_DES")
  @JsonProperty("PLANT_DES")
  private String plantName;

  @CsvBindByName(column = "PLANT_PL")
  @JsonProperty("PLANT_PL")
  private String planningPlant;
  @CsvBindByName(column = "BUSINESS_PLACE")
  @JsonProperty("BUSINESS_PLACE")
  private String businessPlace;
}

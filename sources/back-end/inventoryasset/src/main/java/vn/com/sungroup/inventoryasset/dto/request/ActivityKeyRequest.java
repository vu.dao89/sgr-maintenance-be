package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ActivityKeyRequest extends BaseRequest {
    public ActivityKeyRequest()
    {
        workOrderTypes = new ArrayList<>();
        types = new ArrayList<>();
    }
    private List<String> workOrderTypes;
    private List<String> types;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class ActivityTypeRequest extends BaseRequest {
    public ActivityTypeRequest()
    {
        costCenterCodes = new ArrayList<>();
        types = new ArrayList<>();
    }
    private List<String> types;
    private List<String> costCenterCodes;
}

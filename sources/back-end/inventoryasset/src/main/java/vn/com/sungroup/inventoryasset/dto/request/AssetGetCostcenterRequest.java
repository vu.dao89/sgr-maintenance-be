/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetGetCostcenterRequest class.
 *
 * <p>Contains information about AssetGetCostcenterRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetGetCostcenterRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String employeeid;
}

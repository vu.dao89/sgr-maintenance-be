/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetHistoryInventoryRequest class.
 *
 * <p>Contains information about AssetHistoryInventoryRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetHistoryInventoryRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String qrcode;
    private String from_date;
    private String to_date;
    private String page_index;
}

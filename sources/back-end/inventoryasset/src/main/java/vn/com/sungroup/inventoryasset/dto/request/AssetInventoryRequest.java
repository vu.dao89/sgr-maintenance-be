/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetInventoryImage;

import java.io.Serializable;
import java.util.List;

/**
 * AssetInventoryRequest class.
 *
 * <p>Contains information about AssetInventoryRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetInventoryRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String qrcode;
    private String costcenter;
    private String quantity;
    private String status;
    private String description;
    private String latitude;
    private String longitude;
    private String user_email;
    private List<AssetInventoryImage> image;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetListRequest class.
 *
 * <p>Contains information about AssetListRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetListRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String unit;
    private String asset_group;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.ImageModel;

import java.io.Serializable;
import java.util.List;

/**
 * AssetModifyRequest class.
 *
 * <p>Contains information about AssetModifyRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetModifyRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String name;
    private String serial;
    private String quantity;
    private String unit;
    private String asset_group;
    private String use_date;
    private String costcenter;
    private String inventory_type;
    private String manufacture;
    private String vendor;
    private String model;
    private String person;
    private String latitude;
    private String longitude;
    private String user_email;
    private String qrcode;
    private String type;
    private String room;
    private String status;
    private String description;
    private List<ImageModel> image;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetSearchRequest class.
 *
 * <p>Contains information about AssetSearchRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetSearchRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String all;
    private String asset;
    private String equipment;
    private String description;
    private String costcenter;
    private String person;
    private String asset_group;
    private Integer page;
}

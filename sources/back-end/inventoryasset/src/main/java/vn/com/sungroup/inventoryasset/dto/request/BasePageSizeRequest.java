package vn.com.sungroup.inventoryasset.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasePageSizeRequest {

    @JsonProperty("PAGE")
    private String page;
    @JsonProperty("PAGE_SIZE")
    private String pageSize;

    public void setPage(String page) {
        page =  String.valueOf(Integer.parseInt(page) + 1);
        this.page = page;
    }
}

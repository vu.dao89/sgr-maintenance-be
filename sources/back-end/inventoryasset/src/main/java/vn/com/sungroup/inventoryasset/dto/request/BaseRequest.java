package vn.com.sungroup.inventoryasset.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRequest {

  private String filterText;
}

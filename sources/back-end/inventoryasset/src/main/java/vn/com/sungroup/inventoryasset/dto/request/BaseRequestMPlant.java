package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
public class BaseRequestMPlant extends BaseRequest {
    public BaseRequestMPlant()
    {
        maintenancePlantCodes = new ArrayList<>();
    }
    @NotEmpty(message = "required maintenancePlantCodes")
    private List<String> maintenancePlantCodes;
}

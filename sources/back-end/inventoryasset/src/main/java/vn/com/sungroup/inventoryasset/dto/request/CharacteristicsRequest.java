package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CharacteristicsRequest extends BaseRequest {
    public CharacteristicsRequest()
    {
        charIds = new ArrayList<>();
        classIds = new ArrayList<>();
    }
    private List<String> charIds;
    private List<String> classIds;
}

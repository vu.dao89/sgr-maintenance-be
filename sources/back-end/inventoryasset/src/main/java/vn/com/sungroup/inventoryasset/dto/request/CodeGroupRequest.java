package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class CodeGroupRequest extends BaseRequest {
    public CodeGroupRequest() {
        catalogProfiles = new ArrayList<>();
        catalogs = new ArrayList<>();
    }
    @Size(min = 1, message = "Catalog Profiles are required")
    private List<String> catalogProfiles;
    @Size(min = 1, message = "Catalogs are required")
    private List<String> catalogs;
    private String codeGroup;
    private String codeGroupDesc;
}

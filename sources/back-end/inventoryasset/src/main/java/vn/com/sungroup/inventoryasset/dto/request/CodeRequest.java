package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class CodeRequest extends BaseRequest{
    public CodeRequest(){
        codeGroups = new ArrayList<>();
    }

    @Size(min = 1, message = "Code Groups are required")
    private List<String> codeGroups;
    private String code;
    private String codeDesc;
    @NotBlank(message = "Catalog Profile is required")
    private String catalogProfile;

    @NotBlank(message = "Catalog is required")
    private String catalog;
}

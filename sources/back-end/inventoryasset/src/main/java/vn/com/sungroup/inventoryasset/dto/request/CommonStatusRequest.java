package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CommonStatusRequest extends BaseRequest {
  public CommonStatusRequest() {
    codes = new ArrayList<>();
  }

  private List<String> codes;

  private String status;

  private String description;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ControlKeyRequest extends BaseRequest {
    public ControlKeyRequest()
    {
        codes = new ArrayList<>();
    }
    private List<String> codes;
}

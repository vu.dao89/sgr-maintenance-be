package vn.com.sungroup.inventoryasset.dto.request;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
public class EquipmentRequest extends BaseRequestMPlant {

    public EquipmentRequest()
    {
        functionalLocationCodes = new ArrayList<>();
        workCenterCodes = new ArrayList<>();
        equipmentCategoryCodes = new ArrayList<>();
        objectTypeCodes = new ArrayList<>();
        plannerGroups = new ArrayList<>();
    }

    private String description;
    private List<String> functionalLocationCodes;
    private List<String> workCenterCodes;
    private List<String> equipmentCategoryCodes;
    private List<String> objectTypeCodes;
    private List<String> plannerGroups;
}

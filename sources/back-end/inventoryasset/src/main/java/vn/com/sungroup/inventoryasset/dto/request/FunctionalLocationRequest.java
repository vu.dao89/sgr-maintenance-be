package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class FunctionalLocationRequest extends BaseRequestMPlant {
  public FunctionalLocationRequest() {

    functionalLocationIds = new ArrayList<>();
    workCenterCodes = new ArrayList<>();
    plannerGroups = new ArrayList<>();
    objectTypes = new ArrayList<>();
    categories = new ArrayList<>();
  }

  private List<String> functionalLocationIds;
  private List<String> workCenterCodes;
  private List<String> plannerGroups;
  private List<String> objectTypes;
  private List<String> categories;

  private String description;

}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MaintenancePlantRequest extends BaseRequestMPlant {
    private String name;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class NotificationTypeRequest extends BaseRequest {
  public NotificationTypeRequest()
  {
    notiTypes = new ArrayList<>();
  }

  private List<String> notiTypes;

  private String description;
}

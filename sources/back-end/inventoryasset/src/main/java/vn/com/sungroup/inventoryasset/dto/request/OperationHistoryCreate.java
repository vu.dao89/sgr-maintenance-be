package vn.com.sungroup.inventoryasset.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
public class OperationHistoryCreate implements Serializable {
    public OperationHistoryCreate(){}
    @JsonProperty
    private String workOrderId;
    @JsonProperty
    private String operationId;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class OperationHistoryRequest  extends BaseRequest{
    public OperationHistoryRequest(){
        operationIds = new ArrayList<>();
        userIds = new ArrayList<>();
    }
    private List<String> operationIds;
    private List<String> userIds;
}

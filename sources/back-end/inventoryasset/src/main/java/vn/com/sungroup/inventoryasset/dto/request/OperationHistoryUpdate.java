package vn.com.sungroup.inventoryasset.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@AllArgsConstructor
public class OperationHistoryUpdate extends OperationHistoryCreate implements Serializable {
    public OperationHistoryUpdate(){}
    @JsonProperty
    private String workOrderId;
    @JsonProperty
    private String operationId;
    @JsonProperty
    private String superOperationId;
    @JsonProperty
    private String workStartDate;
    @JsonProperty
    private String workStartTime;
    @JsonProperty
    private String workFinishDate;
    @JsonProperty
    private String workFinishTime;

    @JsonProperty
    private String varianceReasonCode;

    @JsonProperty
    private String confirmation;

    @JsonProperty
    private String actualWork;

    private String confirmationLongText;

    public void setConfirmationLongText()
    {
        if (confirmation != null && !confirmation.isEmpty() && confirmation.length() > 40) {
            confirmationLongText = confirmation;
            confirmation = confirmation.substring(0, 39);
        }
    }
}

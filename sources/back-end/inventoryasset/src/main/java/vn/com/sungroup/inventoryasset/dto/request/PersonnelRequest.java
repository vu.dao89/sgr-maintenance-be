package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PersonnelRequest extends BaseRequestMPlant {
  public PersonnelRequest()
  {
    codes = new ArrayList<>();
    workCenterIds = new ArrayList<>();
  }

  private List<String> codes;

  private String name;

  private List<String> workCenterIds;

}

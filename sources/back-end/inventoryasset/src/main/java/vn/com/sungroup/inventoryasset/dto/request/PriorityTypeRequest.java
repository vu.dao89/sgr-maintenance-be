package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PriorityTypeRequest extends BaseRequest {
  public PriorityTypeRequest()
  {
    types = new ArrayList<>();
    priorities = new ArrayList<>();
  }
  private List<String> types;

  private List<String> priorities;
}

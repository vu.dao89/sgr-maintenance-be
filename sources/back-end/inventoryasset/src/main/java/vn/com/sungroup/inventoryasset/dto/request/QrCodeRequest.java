package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QrCodeRequest {
    private String qrCode;
}

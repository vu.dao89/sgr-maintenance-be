package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class SystemStatusRequest extends BaseRequest {
    public SystemStatusRequest() {
        codes = new ArrayList<>();
        statusIds = new ArrayList<>();
    }
    private List<String> codes;
    private List<String> statusIds;
    private String description;

}

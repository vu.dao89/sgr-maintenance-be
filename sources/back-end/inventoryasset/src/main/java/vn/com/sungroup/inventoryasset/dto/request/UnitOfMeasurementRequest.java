package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UnitOfMeasurementRequest extends BaseRequest {
    public UnitOfMeasurementRequest()
    {
        codes = new ArrayList<>();
    }
    private List<String> codes;
    private String description;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
public class VarianceReasonRequest extends BaseRequest {
    public VarianceReasonRequest() {
        codes = new ArrayList<>();
    }
    private List<String> codes;
    private String description;
}

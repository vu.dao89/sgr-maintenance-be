package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class WorkCenterRequest extends BaseRequestMPlant {
  public WorkCenterRequest()
  {
    workCenterCategories = new ArrayList<>();
    codes = new ArrayList<>();
  }

  private List<String> codes;
  private List<String> workCenterCategories;

  private String description;
}

package vn.com.sungroup.inventoryasset.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class WorkOrderTypeRequest extends BaseRequestMPlant {
    public WorkOrderTypeRequest(){
        types = new ArrayList<>();
    }
    private List<String> types;
    private String name;
}

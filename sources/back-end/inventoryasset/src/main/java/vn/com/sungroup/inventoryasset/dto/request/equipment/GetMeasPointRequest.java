package vn.com.sungroup.inventoryasset.dto.request.equipment;

import lombok.*;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.MeasPointSAPRequest;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class GetMeasPointRequest {

    private List<String> measPointIds;

    public MeasPointSAPRequest toMeasPointSAPRequest()
    {
        var measPointSAP = measPointIds.stream()
            .map(s -> MeasPointSAPRequest.MeasPointSAP.builder()
                .measPoint(s)
                .build())
            .collect(Collectors.toList());
        return MeasPointSAPRequest.builder().measPoints(measPointSAP).build();
    }
}

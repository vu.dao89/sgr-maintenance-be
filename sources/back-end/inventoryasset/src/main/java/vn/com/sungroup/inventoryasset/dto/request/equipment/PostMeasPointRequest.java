package vn.com.sungroup.inventoryasset.dto.request.equipment;

import lombok.*;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasPointDto;
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class PostMeasPointRequest {
    private MeasPointDto data;
}

package vn.com.sungroup.inventoryasset.dto.request.equipment.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Builder
public class MeasPointSAPRequest {
    @JsonProperty("MSP")
    private List<MeasPointSAP> measPoints;

    @Getter
    @Setter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor
    @Builder
    public static class MeasPointSAP {

        @JsonProperty("MEAS_POINT")
        private String measPoint;
    }
}

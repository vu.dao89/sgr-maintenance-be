package vn.com.sungroup.inventoryasset.dto.request.equipment.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class PostMeasPointSAPRequest {

    @JsonProperty("DATA")
    private MeasPointRequest data;

    @Getter
    @Setter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor
    public static class MeasPointRequest{
        @JsonProperty("MEAS_POINT")
        private String measPoint;

        @JsonProperty("MEAS_DOC_DATE")
        private String measDocDate;

        @JsonProperty("MEAS_DOC_TIME")
        private String measDocTime;

        @JsonProperty("MEAS_DOC_TEXT")
        private String measDocText;

        @JsonProperty("MEAS_DOC_LTEXT")
        private String measDocLText;

        @JsonProperty("READ_BY")
        private String readBy;

        @JsonProperty("MEAS_READ")
        private String measRead;

        @JsonProperty("COUNT_READ")
        private String countRead;

        @JsonProperty("DIFF")
        private String diff;
        @JsonProperty("WORK_ORDER")
        private String workOrder;
        @JsonProperty("OPERATION")
        private String operation;
        @JsonProperty("COUNT_READ_ID")
        private String countReadId;

        @Setter
        @JsonProperty("EMAIL")
        private String email;
    }
}

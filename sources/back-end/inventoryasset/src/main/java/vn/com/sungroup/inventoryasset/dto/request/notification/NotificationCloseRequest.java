package vn.com.sungroup.inventoryasset.dto.request.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivityDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationCloseRequest {
    private String id;
    private String referDate;
    private String referDateTime;

    private List<NotificationActivityDto> notificationActivities;
}

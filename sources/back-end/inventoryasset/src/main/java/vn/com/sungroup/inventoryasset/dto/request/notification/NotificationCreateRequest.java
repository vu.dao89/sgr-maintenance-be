package vn.com.sungroup.inventoryasset.dto.request.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivitiesDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationHeaderDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemsDto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationCreateRequest {
    private NotificationHeaderDto header;

    private NotificationItemsDto items;
    private NotificationActivitiesDto activities;

    private List<UUID> documentIds;
}

package vn.com.sungroup.inventoryasset.dto.request.notification;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationDeleteRequest {
    public String notificationId;
    public String notificationItemId;
}

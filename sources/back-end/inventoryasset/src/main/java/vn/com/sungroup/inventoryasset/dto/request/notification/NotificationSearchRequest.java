package vn.com.sungroup.inventoryasset.dto.request.notification;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.notification.search.sap.*;
import vn.com.sungroup.inventoryasset.dto.request.BaseRequestMPlant;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationSearchSAPRequest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class NotificationSearchRequest extends BaseRequestMPlant {
    public NotificationSearchRequest() {
//        plants = new ArrayList<>();
//        properties = new ArrayList<>();
//        types = new ArrayList<>();
//        plannerGroups = new ArrayList<>();
//        status = new ArrayList<>();
//        equipments = new ArrayList<>();
//        functionalLocations = new ArrayList<>();
    }

    private static final long serialVersionUID = 1L;

    @JsonSetter(nulls = Nulls.SKIP)
    private String createFrom = "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String createTo = "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String Id = "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String description = "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String dateFrom= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String dateTo= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String requestStartDateFrom= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String requestStartDateTo= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String requestEndDateFrom= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String requestEndDateTo= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String reportBy= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String assignTo= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private String qrCode= "";
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> priorities = Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> types= Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> plannerGroups= Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> status= Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> equipments= Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> functionalLocations= Arrays.asList("");
    @JsonSetter(nulls = Nulls.SKIP)
    private String employeeId = "";

    public NotificationSearchSAPRequest toNotificationSearchSAPRequest(Pageable pageable) {
        var notificationPlantItems =
                getMaintenancePlantCodes().stream().map(notificationPlantItemDto -> NotificationPlantItem.builder().MAIN_PLANT(notificationPlantItemDto).build()).collect(Collectors.toList());
        var notificationPriorityItems = priorities.stream().map(notificationPriorityItemDto -> NotificationPriorityItem.builder().PRORITY(notificationPriorityItemDto).build()).collect(Collectors.toList());
        var notificationTypeItems = types.stream().map(notificationTypeItemDto -> NotificationTypeItem.builder().NOTIF_TYPE(notificationTypeItemDto).build()).collect(Collectors.toList());
        var notificationPlannerGroupItems = plannerGroups.stream().map(groupItemDto -> NotificationPlannerGroupItem.builder().PLANNER_GROUP(groupItemDto).build()).collect(Collectors.toList());
        var notificationStatusItems = status.stream().map(statusItemDto -> NotificationStatusItem.builder().EQ_STAT_ID(statusItemDto).build()).collect(Collectors.toList());
        var notificationEquipmentItems = equipments.stream().map(equipmentItemDto -> NotificationEquipmentItem.builder().EQUI_ID(equipmentItemDto).build()).collect(Collectors.toList());
        var notificationFunLocItems = functionalLocations.stream().map(funLocItemDto -> NotificationFunLocItem.builder().FUNC_LOC_ID(funLocItemDto).build()).collect(Collectors.toList());

        var request = NotificationSearchSAPRequest.builder()
                .PLANTS(NotificationSearchSAPRequest.Item.<NotificationPlantItem>builder().value(notificationPlantItems).build())
                .PRORITIES(NotificationSearchSAPRequest.Item.<NotificationPriorityItem>builder().value(notificationPriorityItems).build())
                .NOTIFTYPES(NotificationSearchSAPRequest.Item.<NotificationTypeItem>builder().value(notificationTypeItems).build())
                .PLANNER_GROUPS(NotificationSearchSAPRequest.Item.<NotificationPlannerGroupItem>builder().value(notificationPlannerGroupItems).build())
                .STATUSES(NotificationSearchSAPRequest.Item.<NotificationStatusItem>builder().value(notificationStatusItems).build())
                .EQUIPMENT(NotificationSearchSAPRequest.Item.<NotificationEquipmentItem>builder().value(notificationEquipmentItems).build())
                .FUNC_LOC(NotificationSearchSAPRequest.Item.<NotificationFunLocItem>builder().value(notificationFunLocItems).build())
                .NOTIF_CREAT_FROM(createFrom)
                .NOTIF_CREAT_TO(createTo)
                .NOTIF_ID(Id)
                .NOTIF_DES(description)
                .NOTIF_DATE_FROM(dateFrom)
                .NOTIF_DATE_TO(dateTo)
                .REQ_START_DATE_FROM(requestStartDateFrom)
                .REQ_START_DATE_TO(requestStartDateTo)
                .REQ_END_DATE_FROM(requestEndDateFrom)
                .REQ_END_DATE_TO(requestEndDateTo)
                .NOTIF_REPORT_BY(reportBy)
                .ASSIGN_TO(assignTo)
                .QRCODE(qrCode)
                .EMPLOYEEID(employeeId).build();

        request.setPage(String.valueOf(pageable.getPageNumber()));
        request.setPageSize(String.valueOf(pageable.getPageSize()));

        return request;

    }
}

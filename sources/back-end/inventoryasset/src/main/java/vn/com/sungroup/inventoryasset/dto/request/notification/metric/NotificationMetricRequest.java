package vn.com.sungroup.inventoryasset.dto.request.notification.metric;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric.NotificationMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric.NotificationMetricRequestSAP.MaintenancePlant;
import vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric.NotificationMetricRequestSAP.MaintenancePlantInfo;
import vn.com.sungroup.inventoryasset.util.DatetimeUtils;

import javax.validation.constraints.NotEmpty;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationMetricRequest {

    @NotEmpty(message = "Maintenance Plant is required")
    private List<String> mainPlantIds;

    private String notificationCreateOn;

    private String notificationDateFrom;

    private String notificationDateTo;

    private String notificationReportBy;

    private String assignTo;
    private String employeeId;

    private MaintenancePlant convertToMaintenancePlant() {
        var items = this.mainPlantIds.stream()
                .map(id -> MaintenancePlantInfo.builder().maintenancePlantId(id).build())
                .collect(Collectors.toList());

        return MaintenancePlant.builder().items(items).build();
    }

    public NotificationMetricRequestSAP toNotificationMetricRequestSAP() {
        String dateFrom = this.notificationDateFrom;
        String dateTo = this.notificationDateTo;

        if (!StringUtils.hasText(dateFrom) && !StringUtils.hasText(dateTo)) {
            dateFrom = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getFirstDayOfMonth(),
                    DateTimeFormatter.BASIC_ISO_DATE);
            dateTo = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getLastDayOfMonth(),
                    DateTimeFormatter.BASIC_ISO_DATE);

        }

        return NotificationMetricRequestSAP.builder()
                .maintenancePlant(convertToMaintenancePlant())
                .notificationCreateOn(this.notificationCreateOn)
                .notificationDateFrom(dateFrom)
                .notificationDateTo(dateTo)
                .notificationReportBy(this.notificationReportBy)
                .assignTo(this.assignTo)
                .employeeId(this.employeeId)
                .build();
    }
}

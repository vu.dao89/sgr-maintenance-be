package vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationCloseSAPRequest implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    @JsonProperty("NOTIF_ID") private String id;
    @JsonProperty("NOTIF_REFER_DATE") private String referDate;
    @JsonProperty("NOTIF_REFER_TIME") private String referDateTime;
    @JsonProperty("EMAIL") private String email;
}

package vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationActivities;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationHeader;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationItems;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationCreateSAPRequest implements Serializable {

    @JsonProperty("HEADER")
    @NotNull
    @Valid
    private NotificationHeader header;

    @JsonProperty("ITEMS") private NotificationItems items;
    @JsonProperty("ACTIVITIES") private NotificationActivities activities;
}

package vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class NotificationDetailSAPRequest implements Serializable {

    @JsonProperty("NOTIF_ID")
    private String notificationId;
}

package vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import vn.com.sungroup.inventoryasset.dto.notification.search.sap.*;
import vn.com.sungroup.inventoryasset.dto.request.BasePageSizeRequest;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationSearchSAPRequest extends BasePageSizeRequest {
    @JsonProperty("NOTIF_CREAT_FROM")
    private String NOTIF_CREAT_FROM;
    @JsonProperty("NOTIF_CREAT_TO") private String NOTIF_CREAT_TO;
    @JsonProperty("NOTIF_ID") private String NOTIF_ID;
    @JsonProperty("NOTIF_DES") private String NOTIF_DES;
    @JsonProperty("NOTIF_DATE_FROM") private String NOTIF_DATE_FROM;
    @JsonProperty("NOTIF_DATE_TO") private String NOTIF_DATE_TO;
    @JsonProperty("REQ_START_DATE_FROM") private String REQ_START_DATE_FROM;
    @JsonProperty("REQ_START_DATE_TO") private String REQ_START_DATE_TO;
    @JsonProperty("REQ_END_DATE_FROM") private String REQ_END_DATE_FROM;
    @JsonProperty("REQ_END_DATE_TO") private String REQ_END_DATE_TO;
    @JsonProperty("NOTIF_REPORT_BY") private String NOTIF_REPORT_BY;
    @JsonProperty("ASSIGN_TO") private String ASSIGN_TO;
    @JsonProperty("QRCODE") private String QRCODE;
    @JsonProperty("PLANTS") private Item<NotificationPlantItem> PLANTS;
    @JsonProperty("PRORITIES") private Item<NotificationPriorityItem> PRORITIES;
    @JsonProperty("NOTIFTYPES") private Item<NotificationTypeItem> NOTIFTYPES;
    @JsonProperty("PLANNER_GROUPS") private Item<NotificationPlannerGroupItem> PLANNER_GROUPS;
    @JsonProperty("STATUSES") private Item<NotificationStatusItem> STATUSES;
    @JsonProperty("EQUIPMENT") private Item<NotificationEquipmentItem> EQUIPMENT;
    @JsonProperty("FUNC_LOC") private Item<NotificationFunLocItem> FUNC_LOC;

    @JsonProperty("EMPLOYEEID") private String EMPLOYEEID;

    @Getter
    @Setter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor
    @Builder
    public static class Item<T> {

        @JsonProperty("ITEM") private List<T> value;
    }
}

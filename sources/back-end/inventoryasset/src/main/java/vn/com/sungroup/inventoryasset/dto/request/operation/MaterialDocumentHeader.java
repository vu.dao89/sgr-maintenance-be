package vn.com.sungroup.inventoryasset.dto.request.operation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class MaterialDocumentHeader {
    public MaterialDocumentHeader() {
        var dateFormat = new SimpleDateFormat("yyyyMMdd");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        documentDate = dateFormat.format(timestamp);
        postingDate = dateFormat.format(timestamp);
    }

    private String documentDate;
    private String postingDate;
    private String documentText;
    private String email;
}

package vn.com.sungroup.inventoryasset.dto.request.operation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class MaterialDocumentItem {
    @NotBlank(message = "reservation is required")
    private String reservation;
    @NotBlank(message = "reservationItem is required")
    private String reservationItem;
    @NotBlank(message = "material is required")
    private String material;
    @NotBlank(message = "quantity is required")
    private String quantity;

}

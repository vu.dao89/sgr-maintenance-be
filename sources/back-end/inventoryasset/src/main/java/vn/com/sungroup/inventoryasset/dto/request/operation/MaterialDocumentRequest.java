package vn.com.sungroup.inventoryasset.dto.request.operation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentHeaderSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentItemSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentRequestSAP;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationComponentItemSAP;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class MaterialDocumentRequest {
    //    private MaterialDocumentHeader header;
    @NotEmpty(message = "items should not be empty")
    private List<MaterialDocumentItem> items;
    @NotBlank(message = "workOrderId is required")
    private String workOrderId;
    @NotBlank(message = "operationId is required")
    private String operationId;

    public MaterialDocumentRequestSAP toMaterialDocumentRequestSAP(List<OperationComponentItemSAP> items,
                                                                   String personnel, String email) {
        var dateFormat = new SimpleDateFormat("yyyyMMdd");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        var header = MaterialDocumentHeaderSAP.builder()
                .email(email)
                .documentDate(dateFormat.format(timestamp))
                .postingDate(dateFormat.format(timestamp))
                .build();

        List<MaterialDocumentItemSAP> materialDocumentItemsSAP = new ArrayList<>();
        items.forEach(operationComponentItemSAP -> {
            MaterialDocumentItem item = this.getItems().stream().filter(materialDocumentItem ->
                    materialDocumentItem.getReservation().equals(operationComponentItemSAP.getReservation()) &&
                            materialDocumentItem.getReservationItem().equals(operationComponentItemSAP.getReservationItem()) &&
                            materialDocumentItem.getMaterial().equals(operationComponentItemSAP.getMaterial())).findFirst().orElse(null);
            if (item != null) {
                var materialDocumentItemSAP = MaterialDocumentItemSAP.builder()
                        .reservation(item.getReservation())
                        .reservationItem(item.getReservationItem())
                        .material(item.getMaterial())
                        .quantity(item.getQuantity())
                        .maintenancePlantId(operationComponentItemSAP.getPlant())
                        .storageLocation(operationComponentItemSAP.getStorageLocation())
                        .batch(operationComponentItemSAP.getBatch())
                        .itemText(operationComponentItemSAP.getNote())
                        .remainQuantity(operationComponentItemSAP.getRemainQuantity())
                        .personnel(personnel).build();
                materialDocumentItemsSAP.add(materialDocumentItemSAP);
            }
        });

        return MaterialDocumentRequestSAP.builder().header(header).items(materialDocumentItemsSAP).build();
    }
}

package vn.com.sungroup.inventoryasset.dto.request.operation;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationDetailRequest {

  @NotBlank(message = "Work Order Id is required")
  private String workOrderId;

  @NotBlank(message = "Operation Id is required")
  private String operationId;

}

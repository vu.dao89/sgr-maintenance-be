package vn.com.sungroup.inventoryasset.dto.request.operation;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OperationPersonnelChangeRequest {

  @NotBlank(message = "Work Order Id is required")
  private String workOrderId;

  @NotBlank(message = "Operation Id is required")
  private String operationId;

  @NotBlank(message = "Personnel Id is required")
  private String personnelId;
}

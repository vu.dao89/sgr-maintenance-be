package vn.com.sungroup.inventoryasset.dto.request.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.request.BaseRequestMPlant;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationSearchRequest extends BaseRequestMPlant {

  private String workOrderDateForm;

  private String workOrderDateTo;

  private String workOrderFinishFrom;

  private String workOrderFinishTo;

  private List<String> workOrderIds;

  private List<String> operationIds;

  private List<String> priorityIds;

  private List<String> equipmentIds;

  private List<String> functionLocationIds;

  private List<String> status;

  @JsonSetter(nulls = Nulls.SKIP)
  private List<String> personnels = Arrays.asList();

  private List<String> workCenterIds;

  public WorkOrderOperationSearchSAP convertToWorkOrderSAP() {
    List<WorkOrderOperationSearchItemSAP> workOrderOperationSearchItems = this
        .workOrderIds.stream()
        .map(id -> WorkOrderOperationSearchItemSAP.builder().workOrderId(id).build())
        .collect(Collectors.toList());
    return WorkOrderOperationSearchSAP.builder().item(workOrderOperationSearchItems).build();
  }

  public OperationSearchSAP convertToOperationSearchSAP() {
    List<OperationSearchItemSAP> operationSearchItems = this
        .operationIds.stream()
        .map(id -> OperationSearchItemSAP.builder().operationId(id).build())
        .collect(Collectors.toList());
    return OperationSearchSAP.builder().item(operationSearchItems).build();
  }

  public PrioritySearchSAP convertToPrioritySearchSAP() {
    List<PrioritySearchItemSAP> prioritySearchItems = this
        .priorityIds.stream()
        .map(id -> PrioritySearchItemSAP.builder().priorityId(id).build())
        .collect(Collectors.toList());
    return PrioritySearchSAP.builder().item(prioritySearchItems).build();
  }

  public EquipmentSearchSAP convertToEquipmentSearchSAP() {
    List<EquipmentSearchItemSAP> equipmentSearchItems = this
        .equipmentIds.stream()
        .map(id -> EquipmentSearchItemSAP.builder().equipmentId(id).build())
        .collect(Collectors.toList());
    return EquipmentSearchSAP.builder().item(equipmentSearchItems).build();
  }

  public FunctionalLocationSearchSAP convertToFunctionalLocationSearchSAP() {
    List<FunctionalLocationSearchItemSAP> functionalLocationSearchItems = this
        .functionLocationIds.stream()
        .map(id -> FunctionalLocationSearchItemSAP.builder().functionalLocationId(id).build())
        .collect(Collectors.toList());
    return FunctionalLocationSearchSAP.builder().item(functionalLocationSearchItems).build();
  }

  public StatusesSearchSAP convertToStatusesSearchSAP() {
    List<StatusesSearchItemSAP> statusesSearchItems = this
        .status.stream()
        .map(id -> StatusesSearchItemSAP.builder().status(id).build())
        .collect(Collectors.toList());
    return StatusesSearchSAP.builder().item(statusesSearchItems).build();
  }

  public PersonnelSAP convertToPersonnelSAP() {
    List<PersonnelItemSAP> personnelItems = this
        .personnels.stream()
        .map(id -> PersonnelItemSAP.builder().personnel(id).build())
        .collect(Collectors.toList());
    return PersonnelSAP.builder().item(personnelItems).build();
  }

  public PlantsSearchSAP convertToPlantsSearchSAP() {
    List<PlantsSearchItemSAP> plantsSearchItems = this
        .getMaintenancePlantCodes().stream()
        .map(id -> PlantsSearchItemSAP.builder().mainPlant(id).build())
        .collect(Collectors.toList());
    return PlantsSearchSAP.builder().item(plantsSearchItems).build();
  }

  public WorkCenterSearchSAP convertToWorkCenterSearchSAP() {
    List<WorkCenterSearchItemSAP> workCenterSearchItems = this
        .workCenterIds.stream()
        .map(id -> WorkCenterSearchItemSAP.builder().workCenterId(id).build())
        .collect(Collectors.toList());
    return WorkCenterSearchSAP.builder().item(workCenterSearchItems).build();
  }
}

package vn.com.sungroup.inventoryasset.dto.request.operation;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationSubPersonnelChangeRequest {

  @NotBlank(message = "Work Order Id is required")
  private String workOrderId;

  @NotBlank(message = "Operation Id is required")
  private String operationId;

  @NotBlank(message = "Sub operation id is required")
  private String subOperationId;

  @NotBlank(message = "Personnel Id is required")
  private String personnelId;
}

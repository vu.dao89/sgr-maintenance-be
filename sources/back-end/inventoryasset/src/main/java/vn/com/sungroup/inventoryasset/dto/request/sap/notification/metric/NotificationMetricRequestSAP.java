package vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationMetricRequestSAP {

  @JsonProperty("NOTIF_CREAT_ON")
  private String notificationCreateOn;
  @JsonProperty("NOTIF_DATE_FROM")
  private String notificationDateFrom;
  @JsonProperty("NOTIF_DATE_TO")
  private String notificationDateTo;
  @JsonProperty("NOTIF_REPORT_BY")
  private String notificationReportBy;
  @JsonProperty("ASSIGN_TO")
  private String assignTo;
  @JsonProperty("EMPLOYEEID")
  private String employeeId;

  @JsonProperty("MAIN_PLANT")
  private MaintenancePlant maintenancePlant;

  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  public static class MaintenancePlant {

    @JsonProperty("ITEM")
    private List<MaintenancePlantInfo> items;
  }

  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  public static class MaintenancePlantInfo {

    @JsonProperty("MAIN_PLANT_ID")
    private String maintenancePlantId;
  }
}

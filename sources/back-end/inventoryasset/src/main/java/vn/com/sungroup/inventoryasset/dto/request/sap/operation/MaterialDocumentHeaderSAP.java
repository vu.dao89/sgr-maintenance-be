package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class MaterialDocumentHeaderSAP {
    @JsonProperty("DOCUMENT_DATE")
    private String documentDate;
    @JsonProperty("POSTING_DATE")
    private String postingDate;
    @JsonProperty("DOCUMENT_TEXT")
    private String documentText;
    @JsonProperty("EMAIL")
    private String email;
}

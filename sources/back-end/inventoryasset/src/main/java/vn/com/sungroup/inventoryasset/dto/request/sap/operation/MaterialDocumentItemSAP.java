package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaterialDocumentItemSAP {
    @JsonProperty("RESERVATION")
    private String reservation;
    @JsonProperty("RESERVATION_ITEM")
    private String reservationItem;
    @JsonProperty("MATERIAL")
    private String material;
    @JsonProperty("QUANTITY")
    private String quantity;
    @JsonProperty("PLANT")
    private String maintenancePlantId;
    @JsonProperty("STORAGE_LOCATION")
    private String storageLocation;
    @JsonProperty("BATCH")
    private String batch;
    @JsonProperty("ITEMTEXT")
    private String itemText;
    @JsonProperty("PERSONNEL")
    private String personnel;
    private String remainQuantity;
}

package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaterialDocumentRequestSAP {
    @JsonProperty("HEADER")
    private MaterialDocumentHeaderSAP header;
    @JsonProperty("ITEM")
    private List<MaterialDocumentItemSAP> items;
}

package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationDetailRequestSAP {

  @JsonProperty("WO_ID")
  private String workOrderId;

  @JsonProperty("OPER_ID")
  private String operationId;

}

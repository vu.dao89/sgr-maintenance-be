package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.request.BasePageSizeRequest;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class OperationSearchRequestSAP extends BasePageSizeRequest {

  @JsonProperty("KEYWORD")
  private String keyword;

  @JsonProperty("WO_START_DATE_FROM")
  private String workOrderDateForm;

  @JsonProperty("WO_START_DATE_TO")
  private String workOrderDateTo;

  @JsonProperty("WO_FINISH_DATE_FROM")
  private String workOrderFinishFrom;

  @JsonProperty("WO_FINISH_DATE_TO")
  private String workOrderFinishTo;

  @JsonProperty("WO")
  private WorkOrderOperationSearchSAP workOrder;

  @JsonProperty("OPER")
  private OperationSearchSAP operation;

  @JsonProperty("PRORITY")
  private PrioritySearchSAP priority;

  @JsonProperty("EQUIPMENT")
  private EquipmentSearchSAP equipment;

  @JsonProperty("FUNCLOCATION")
  private FunctionalLocationSearchSAP functionLocation;

  @JsonProperty("STATUSES")
  private StatusesSearchSAP statuses;

  @JsonProperty("PERS")
  private PersonnelSAP personnel;

  @JsonProperty("PLANTS")
  private PlantsSearchSAP plants;

  @JsonProperty("WORK_CENTER")
  private WorkCenterSearchSAP workCenter;
}

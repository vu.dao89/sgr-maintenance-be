package vn.com.sungroup.inventoryasset.dto.request.sap.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderOperationSearchItemSAP {

  @JsonProperty("WO_ID")
  private String workOrderId;
}

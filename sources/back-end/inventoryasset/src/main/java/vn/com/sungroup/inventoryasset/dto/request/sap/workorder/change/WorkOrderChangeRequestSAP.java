package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "WO_ID",
    "WO_DES",
    "WO_LONGTEXT",
    "WO_TYPE",
    "MAIN_ACT_KEY",
    "EQUI_ID",
    "FUNC_LOC_ID",
    "PRORITY",
    "MAIN_PERSON",
    "MAIN_PLANT",
    "WORK_CENTER",
    "PLANNER_GROUP",
    "SYSCOND",
    "WO_START_DATE",
    "WO_FINISH_DATE",
    "NOTIF_ID"
})
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkOrderChangeRequestSAP {

    @JsonProperty("WO_ID")
    private String woId;

    @JsonProperty("WO_DES")
    private String woDes;

    @JsonProperty("WO_LONGTEXT")
    private String woLongtext;

    @JsonProperty("MAIN_ACT_KEY")
    private String mainActKey;

    @JsonProperty("EQUI_ID")
    private String equiId;

    @JsonProperty("FUNC_LOC_ID")
    private String funcLocId;

    @JsonProperty("PRORITY")
    private String prority;

    @JsonProperty("MAIN_PERSON")
    private String mainPerson;

    @JsonProperty("MAIN_PLANT")
    private String mainPlant;

    @JsonProperty("WORK_CENTER")
    private String workCenter;

    @JsonProperty("PLANNER_GROUP")
    private String plannerGroup;

    @JsonProperty("SYSCOND")
    private String syscond;

    @JsonProperty("WO_START_DATE")
    private String woStartDate;
    @JsonProperty("WO_START_TIME")
    private String woStartTime;

    @JsonProperty("WO_FINISH_DATE")
    private String woFinishDate;
    @JsonProperty("WO_FINISH_TIME")
    private String woFinishTime;

    @JsonProperty("EMAIL")
    @Getter
    @Setter
    private String email;

    public void updateDescription() {
        if (woLongtext.length() <= 40) {
            woDes = woLongtext;
        } else {
            var subString = woLongtext.substring(0, 40);
            woDes = subString;
        }
    }
}

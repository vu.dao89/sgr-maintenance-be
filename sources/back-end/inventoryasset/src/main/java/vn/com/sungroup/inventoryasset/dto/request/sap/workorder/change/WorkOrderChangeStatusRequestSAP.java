package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderChangeStatusRequestSAP {
    @JsonProperty("WO_ID")
    private String workOrderId;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("REASON")
    private String reason;
    @JsonProperty("EMAIL")
    private String email;
}


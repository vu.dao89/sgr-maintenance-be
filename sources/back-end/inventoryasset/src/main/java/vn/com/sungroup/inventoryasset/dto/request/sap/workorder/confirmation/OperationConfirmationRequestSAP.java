package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.confirmation;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationConfirmationRequestSAP {

  @JsonProperty("CONFIRMATION")
  @NotNull
  private OperationConfirmationSAP confirmation;
}

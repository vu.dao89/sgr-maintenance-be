package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.confirmation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationConfirmationSAP {

  @JsonProperty("WO_ID")
  @NotBlank(message = "Work Order Id")
  private String workOrderId;

  @JsonProperty("OPER_ID")
  @NotBlank(message = "Operation Id")
  private String operationId;

  @JsonProperty("SUPER_OPER_ID")
  @NotBlank(message = "Sub Operation Id")
  private String superOperationId;

  @JsonProperty("WORK_CENTER")
  @NotBlank(message = "Work Center")
  private String workCenter;

  @JsonProperty("MAIN_PLANT")
  @NotBlank(message = "Maintenance Plant")
  private String maintenancePlant;

  @JsonProperty("PERSONEL")
  @NotBlank(message = "Personnel")
  private String personnel;

  @JsonProperty("ACTUAL_WORK")
  @NotBlank(message = "Actual work")
  private String actualWork;

  @JsonProperty("ACTUAL_WORK_UNIT")
  @NotBlank(message = "Actual work unit")
  private String actualWorkUnit;

  @JsonProperty("ACTIVITY_TYPE")
  @NotBlank(message = "Activity type")
  private String activityType;

  @JsonProperty("POSTING_DATE")
  @NotBlank(message = "Posting date")
  private String postingDate;

  @JsonProperty("FINAL_CONFIRMATION")
  private String finalConfirmation;

  @JsonProperty("WORK_START_DATE")
  @NotBlank(message = "Work start date")
  private String workStartDate;

  @JsonProperty("WORK_START_TIME")
  @NotBlank(message = "Work start time")
  private String workStartTime;

  @JsonProperty("WORK_FINISH_DATE")
  @NotBlank(message = "Work finish date")
  private String workFinishDate;

  @JsonProperty("WORK_FINISH_TIME")
  @NotBlank(message = "Work finish time")
  private String workFinishTime;

  @JsonProperty("REASON")
  @NotBlank(message = "Reason")
  private String reason;

  @JsonProperty("CONFIRMATION")
  private String confirmation;

  @JsonProperty("CONFIRMATION_LONG_TEXT")
  private String confirmLongText;

  @JsonProperty("EMAIL")
  @Email(message = "Email")
  @Getter
  @Setter
  private String email;
}

package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricPlantItemSAP {

  @JsonProperty("MAIN_PLANT_ID")
  private String mainPlantId;
}

package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricPlantRequestSAP {

  @JsonProperty("ITEM")
  private List<WorkOrderMetricPlantItemSAP> item;

}

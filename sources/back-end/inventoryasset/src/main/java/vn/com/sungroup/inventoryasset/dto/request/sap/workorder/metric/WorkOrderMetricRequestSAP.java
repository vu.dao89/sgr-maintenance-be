package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricRequestSAP {

  @JsonProperty("MAIN_PLANT")
  private WorkOrderMetricPlantRequestSAP mainPlant;

  @JsonProperty("MAIN_PERSON")
  private String mainPerson;

  @JsonProperty("PERSONNEL_RES")
  private String personnelResponse;

  @JsonProperty("WO_FINISH_FROM")
  private String workOrderFinishFrom;

  @JsonProperty("WO_FINISH_TO")
  private String workOrderFinishTo;

}

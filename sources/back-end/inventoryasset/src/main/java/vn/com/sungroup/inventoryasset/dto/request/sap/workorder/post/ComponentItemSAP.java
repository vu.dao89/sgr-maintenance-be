package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ComponentItemSAP {

    @JsonProperty("WO_ID")
    private String woId;
    @JsonProperty("OPER_ID")
    private String operId;
    @JsonProperty("MATERIAL")
    private String material;
    @JsonProperty("PLANT")
    private String plant;
    @JsonProperty("STORAGE_LOCATION")
    private String storageLocation;
    @JsonProperty("BATCH")
    private String batch;
    @JsonProperty("QUANTITY")
    private String quantity;
    @JsonProperty("UNIT")
    private String unit;
    @JsonProperty("REQUIREMENT_DATE")
    private String requirementDate;
    @JsonProperty("NOTE")
    private String note;

}
package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationItemSAP {

    @JsonProperty("WO_ID")
    private String woId;

    @JsonProperty("OPER_ID")
    private String operId;

    @JsonProperty("SUPER_OPER_ID")
    private String superOperId;

    @JsonProperty("OPER_DES")
    private String operDes;

    @JsonProperty("OPER_LONGTEXT")
    private String operLongtext;

    @JsonProperty("EQUI_ID")
    private String equiId;

    @JsonProperty("CONTROL_KEY")
    private String controlKey;

    @JsonProperty("PERSONEL")
    private String personel;

    @JsonProperty("ESTIMATE")
    private String estimate;

    @JsonProperty("UNIT")
    private String unit;
    @JsonProperty("MAIN_PLANT")
    private String maintenancePlantCode;
    @JsonProperty("WORK_CENTER")
    private String workCenterCode;
    @JsonProperty("FUNC_LOC_ID")
    private String functionalLocationCode;
    @JsonProperty("{ACTIVITY_TYPE")
    private String activityType;
}
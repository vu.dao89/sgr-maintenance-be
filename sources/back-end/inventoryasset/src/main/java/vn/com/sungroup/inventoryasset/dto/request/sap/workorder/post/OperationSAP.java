package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperationSAP {

    @JsonProperty("ITEM")
    private List<OperationItemSAP> item;

}


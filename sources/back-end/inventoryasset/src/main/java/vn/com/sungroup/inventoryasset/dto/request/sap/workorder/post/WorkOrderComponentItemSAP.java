package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderComponentItemSAP {
    @JsonProperty("WO_ID")
    private String workOrderId;
    @JsonProperty("OPER_ID")
    private String operationId;
    @JsonProperty("MATERIAL")
    private String material;
    @JsonProperty("PLANT")
    private String maintenancePlant;
    @JsonProperty("STORAGE_LOCATION")
    private String storageLocation;
    @JsonProperty("BATCH")
    private String batch;
    @JsonProperty("QUANTITY")
    private String requiredQuantity;
    @JsonProperty("UNIT")
    private String unitOfMeasure;
    @JsonProperty("REQUIREMENT_DATE")
    private String requireDate;
    @JsonProperty("NOTE")
    private String note;
}

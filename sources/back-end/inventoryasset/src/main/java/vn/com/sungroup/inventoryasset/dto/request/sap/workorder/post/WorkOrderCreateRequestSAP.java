package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderCreateRequestSAP {
    @JsonProperty("WO")
    private WorkOrderCreateSAP workOrder;
    @JsonProperty("OPERATION")
    private WorkOrderOperationSAP operation;
    @JsonProperty("COMPONENTS")
    private WorkOrderComponentSAP components;
}

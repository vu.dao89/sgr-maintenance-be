package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderCreateSAP {
    @JsonProperty("WO_ID")
    private String workOrderId;
    @JsonProperty("WO_DES")
    private String workOrderDesc;
    @JsonProperty("WO_LONGTEXT")
    private String workOrderLongText;
    @JsonProperty("WO_TYPE")
    private String workOrderType;
    @JsonProperty("MAIN_ACT_KEY")
    private String maintenanceActivityKey;
    @JsonProperty("EQUI_ID")
    private String equipmentId;
    @JsonProperty("FUNC_LOC_ID")
    private String functionalLocation;
    @JsonProperty("PRORITY")
    private String priority;
    @JsonProperty("MAIN_PERSON")
    private String mainPersonnel;
    @JsonProperty("MAIN_PLANT")
    private String maintenancePlant;
    @JsonProperty("WORK_CENTER")
    private String workCenter;
    @JsonProperty("PLANNER_GROUP")
    private String plannerGroup;
    @JsonProperty("SYSCOND")
    private String systemCondition;
    @JsonProperty("WO_START_DATE")
    private String startDate;
    @JsonProperty("WO_START_TIME")
    private String startTime;
    @JsonProperty("WO_FINISH_DATE")
    private String finishDate;
    @JsonProperty("WO_FINISH_TIME")
    private String finishTime;
    @JsonProperty("NOTIF_ID")
    private String notification;
    @JsonProperty("EMAIL")
    private String email;

    public void updateDescription() {
        if (workOrderLongText.length() <= 40) {
            workOrderDesc = workOrderLongText;
        } else {
            var subString = workOrderLongText.substring(0, 40);
            workOrderDesc = subString;
        }
    }
}

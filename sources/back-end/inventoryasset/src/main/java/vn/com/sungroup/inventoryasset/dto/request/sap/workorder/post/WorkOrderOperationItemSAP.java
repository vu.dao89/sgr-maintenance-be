package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderOperationItemSAP {
    @JsonProperty("WO_ID")
    private String workOrderId;
    @JsonProperty("OPER_ID")
    private String operationId;
    @JsonProperty("SUPER_OPER_ID")
    private String superOperationId;
    @JsonProperty("OPER_DES")
    private String operationDesc;
    @JsonProperty("OPER_LONGTEXT")
    private String operationLongText;
    @JsonProperty("EQUI_ID")
    private String equipmentId;
    @JsonProperty("MAIN_PLANT")
    private String maintenancePlant;
    @JsonProperty("WORK_CENTER")
    private String workCenter;
    @JsonProperty("FUNC_LOC_ID")
    private String functionalLocationId;
    @JsonProperty("CONTROL_KEY")
    private String controlKey;
    @JsonProperty("PERSONEL")
    private String personnel;
    @JsonProperty("ESTIMATE")
    private String estimate;
    @JsonProperty("UNIT")
    private String unit;
    @JsonProperty("ACTIVITY_TYPE")
    private String activityType;

    public void updateDescription() {
        if (operationLongText.length() <= 40) {
            operationDesc = operationLongText;
        } else {
            var subString = operationLongText.substring(0, 40);
            operationDesc = subString;
        }
    }
}

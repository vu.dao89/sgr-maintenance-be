package vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderOperationSAP {
    @JsonProperty("ITEM")
    private List<WorkOrderOperationItemSAP> items;
}

package vn.com.sungroup.inventoryasset.dto.request.workorder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ITEM"
})
public class Component {

    @JsonProperty("ITEM")
    private ComponentItem item;
}

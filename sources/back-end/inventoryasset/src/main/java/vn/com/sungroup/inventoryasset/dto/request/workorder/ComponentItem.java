package vn.com.sungroup.inventoryasset.dto.request.workorder;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "WO_ID",
        "OPER_ID",
        "MATERIAL",
        "PLANT",
        "STORAGE_LOCATION",
        "BATCH",
        "QUANTITY",
        "UNIT",
        "REQUIREMENT_DATE",
        "NOTE"
})
public class ComponentItem {

    @JsonProperty("WO_ID")
    private String woId;
    @JsonProperty("OPER_ID")
    private String operId;
    @JsonProperty("MATERIAL")
    private String material;
    @JsonProperty("PLANT")
    private String plant;
    @JsonProperty("STORAGE_LOCATION")
    private String storageLocation;
    @JsonProperty("BATCH")
    private String batch;
    @JsonProperty("QUANTITY")
    private String quantity;
    @JsonProperty("UNIT")
    private String unit;
    @JsonProperty("REQUIREMENT_DATE")
    private String requirementDate;
    @JsonProperty("NOTE")
    private String note;

}
package vn.com.sungroup.inventoryasset.dto.request.workorder;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "WO_ID",
        "OPER_ID",
        "SUPER_OPER_ID",
        "OPER_DES",
        "OPER_LONGTEXT",
        "EQUI_ID",
        "CONTROL_KEY",
        "PERSONEL",
        "ESTIMATE",
        "UNIT"
})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationItem {

    @JsonProperty("workOrderId")
    private String woId;

    @JsonProperty("opreationId")
    private String operId;

    @JsonProperty("superOperationId")
    private String superOperId;

    @JsonProperty("opreationDescription")
    private String operDes;

    @JsonProperty("operationLongText")
    private String operLongtext;

    @JsonProperty("equipmentId")
    @NotBlank(message = "Equipment")
    private String equiId;

    @JsonProperty("controlKey")
    private String controlKey;

    @JsonProperty("personnel")
    @NotBlank(message = "Personnel")
    private String personel;

    @JsonProperty("estimate")
    @NotBlank(message = "Estimate")
    private String estimate;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("documentIds")
    private List<UUID> documentIds;
    @JsonProperty("maintenancePlantCode")
    @NotBlank(message = "maintenancePlantCode")
    private String maintenancePlantCode;
    @JsonProperty("workCenterCode")
//    @NotBlank(message = "workCenterCode")
    private String workCenterCode;
    @JsonProperty("functionalLocationCode")
//    @NotBlank(message = "functionalLocationCode")
    private String functionalLocationCode;
    @JsonProperty("activityType")
    private String activityType;
}
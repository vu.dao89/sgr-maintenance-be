package vn.com.sungroup.inventoryasset.dto.request.workorder.change;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderChangePersonalRequest {

  private String workOrderId;

  private String personnelId;
}

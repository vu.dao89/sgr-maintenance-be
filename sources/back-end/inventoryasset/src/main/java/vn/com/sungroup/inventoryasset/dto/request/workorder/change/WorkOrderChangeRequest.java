package vn.com.sungroup.inventoryasset.dto.request.workorder.change;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderChangeRequest {

  @JsonProperty("workOrderId")
  private String woId;

  @JsonProperty("workOrderDescription")
  private String woDes;

  @JsonProperty("workOrderLongText")
  private String woLongtext;

  @JsonProperty("maintenanceActivityKey")
  @NotBlank(message = "Maintenance Activity Key")
  private String mainActKey;

  @JsonProperty("equipmentId")
  private String equiId;

  @JsonProperty("functionalLocationId")
  private String funcLocId;

  @JsonProperty("priority")
  private String prority;

  @JsonProperty("mainPerson")
  @NotBlank(message = "Main personnel")
  private String mainPerson;

  @JsonProperty("mainPlant")
  @NotBlank(message = "Maintenance Plant")
  private String mainPlant;

  @JsonProperty("workCenter")
  @NotBlank(message = "Work Center")
  private String workCenter;

  @JsonProperty("plannerGroup")
  @NotBlank(message = "Planner Group")
  private String plannerGroup;

  @JsonProperty("systemCondition")
  private String syscond;

  @JsonProperty("workOrderStartDate")
  @NotBlank(message = "Start Date")
  private String woStartDate;
  @JsonProperty("workOrderStartTime")
  @NotBlank(message = "Start Time")
  private String woStartTime;

  @JsonProperty("workOrderFinishDate")
  @NotBlank(message = "Finish Date")
  private String woFinishDate;
  @JsonProperty("workOrderFinishTime")
  @NotBlank(message = "Finish Time")
  private String woFinishTime;


}

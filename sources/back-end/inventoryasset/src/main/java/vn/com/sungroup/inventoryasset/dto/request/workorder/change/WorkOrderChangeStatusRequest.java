package vn.com.sungroup.inventoryasset.dto.request.workorder.change;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderChangeStatusRequest {
    private String workOrderId;
    private int reasonCode;
}

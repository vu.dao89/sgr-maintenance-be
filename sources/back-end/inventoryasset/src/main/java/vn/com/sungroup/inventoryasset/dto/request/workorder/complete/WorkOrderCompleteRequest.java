package vn.com.sungroup.inventoryasset.dto.request.workorder.complete;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivitiesDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkOrderCompleteRequest {

  @NotBlank(message = "Work Order Id is required")
  private String workOrderId;

  private String notificationId;

  private NotificationActivitiesDto activities;
}

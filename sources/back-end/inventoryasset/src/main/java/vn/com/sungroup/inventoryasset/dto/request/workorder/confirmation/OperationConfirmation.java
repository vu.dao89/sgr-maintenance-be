package vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationConfirmation {

  @NotBlank(message = "Work Order Id")
  private String workOrderId;

  @NotBlank(message = "Operation Id")
  private String operationId;

  @NotBlank(message = "Sub Operation Id")
  private String superOperationId;

  @NotBlank(message = "Work Center")
  private String workCenter;

  @NotBlank(message = "Maintenance Plant")
  private String maintenancePlant;

  @NotBlank(message = "Personnel")
  private String personnel;

  @NotBlank(message = "Actual work")
  private String actualWork;

  @NotBlank(message = "Actual work unit")
  private String actualWorkUnit;

  @NotBlank(message = "Activity type")
  private String activityType;

  @NotBlank(message = "Posting date")
  private String postingDate;

  private String finalConfirmation;

  @NotBlank(message = "Work start date")
  private String workStartDate;

  @NotBlank(message = "Work start time")
  private String workStartTime;

  @NotBlank(message = "Work finish date")
  private String workFinishDate;

  @NotBlank(message = "Work finish time")
  private String workFinishTime;

  @NotBlank(message = "Reason")
  private String reason;

  private String confirmation;

  private String confirmLongText;
  @NotBlank(message = "Email")
  private String email;

  public void setConfirmationLongText(){
    if (confirmation != null && !confirmation.isEmpty() && !confirmation.isBlank() && confirmation.length() > 40) {
      confirmLongText = confirmation;
      confirmation = confirmation.substring(0, 39);
    }
  }
}

package vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperationConfirmationRequest {

  @NotNull(message = "confirmation")
  private OperationConfirmation confirmation;
}

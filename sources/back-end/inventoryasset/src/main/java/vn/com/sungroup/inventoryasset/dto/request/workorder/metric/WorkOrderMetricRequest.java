package vn.com.sungroup.inventoryasset.dto.request.workorder.metric;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric.WorkOrderMetricPlantItemSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric.WorkOrderMetricPlantRequestSAP;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricRequest {

  @NotEmpty(message = "Maintenance Plant is required")
  private List<String> mainPlantIds;

  private String mainPerson;

  private String personnelResponse;

  private String workOrderFinishFrom;

  private String workOrderFinishTo;

  public WorkOrderMetricPlantRequestSAP convertPlantSAP() {
    return WorkOrderMetricPlantRequestSAP.builder().item(this.mainPlantIds.stream()
        .map(id -> WorkOrderMetricPlantItemSAP.builder().mainPlantId(id).build())
        .collect(Collectors.toList())).build();
  }
}

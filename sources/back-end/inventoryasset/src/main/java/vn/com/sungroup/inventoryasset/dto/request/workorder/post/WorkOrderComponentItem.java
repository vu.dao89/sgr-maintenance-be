package vn.com.sungroup.inventoryasset.dto.request.workorder.post;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderComponentItem {
    private String workOrderId;
    @NotBlank(message = "operationId is required")
    private String operationId;
    @NotBlank(message = "material is required")
    private String material;
    @NotBlank(message = "maintenancePlant is required")
    private String maintenancePlant;
    @NotBlank(message = "storageLocation is required")
    private String storageLocation;
    private String batch;
    @NotBlank(message = "requiredQuantity is required")
    private String requiredQuantity;
    @NotBlank(message = "unitOfMeasure is required")
    private String unitOfMeasure;
    private String requireDate;
    private String note;
}

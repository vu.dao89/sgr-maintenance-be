package vn.com.sungroup.inventoryasset.dto.request.workorder.post;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderCreate {
    private String workOrderId;
    @NotBlank(message = "workOrderDesc is required")
    private String workOrderDesc;
    private String workOrderLongText;
    @NotBlank(message = "workOrderType is required")
    private String workOrderType;
    @NotBlank(message = "maintenanceActivityKey is required")
    private String maintenanceActivityKey;
    private String equipmentId;
    private String functionalLocation;
    @NotBlank(message = "priority is required")
    private String priority;
    @NotBlank(message = "mainPersonnel is required")
    private String mainPersonnel;
    @NotBlank(message = "maintenancePlant is required")
    private String maintenancePlant;
    @NotBlank(message = "workCenter is required")
    private String workCenter;
    @NotBlank(message = "plannerGroup is required")
    private String plannerGroup;
    private String systemCondition;
    @NotBlank(message = "startDate is required")
    private String startDate;
    @NotBlank(message = "startTime is required")
    private String startTime;
    @NotBlank(message = "finishDate is required")
    private String finishDate;
    @NotBlank(message = "finishTime is required")
    private String finishTime;
    private String notification;
}

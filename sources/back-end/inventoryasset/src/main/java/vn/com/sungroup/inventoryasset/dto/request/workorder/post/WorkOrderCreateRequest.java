package vn.com.sungroup.inventoryasset.dto.request.workorder.post;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderCreateRequest {
    private WorkOrderCreate workOrder;
    private WorkOrderOperation operation;
    private WorkOrderComponent components;
}

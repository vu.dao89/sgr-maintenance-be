package vn.com.sungroup.inventoryasset.dto.request.workorder.post;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderOperation {
    private List<WorkOrderOperationItem> items;
}

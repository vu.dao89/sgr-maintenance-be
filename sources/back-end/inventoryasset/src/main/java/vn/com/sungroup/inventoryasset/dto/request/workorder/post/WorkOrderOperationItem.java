package vn.com.sungroup.inventoryasset.dto.request.workorder.post;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderOperationItem {
    private String workOrderId;
    @NotBlank(message = "operationId is required")
    private String operationId;
    private String superOperationId;
    @NotBlank(message = "operationDesc is required")
    private String operationDesc;
    private String operationLongText;
    private String equipmentId;
    private String maintenancePlant;
    private String workCenter;
    private String functionalLocationId;
    private String controlKey;
    private String personnel;
    private String estimate;
    private String unit;
    private String activityType;
    private List<UUID> documentIds;
    private List<WorkOrderOperationItem> subOperations;
}

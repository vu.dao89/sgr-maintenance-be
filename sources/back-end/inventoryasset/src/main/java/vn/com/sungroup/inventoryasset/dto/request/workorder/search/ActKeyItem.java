
package vn.com.sungroup.inventoryasset.dto.request.workorder.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ACTKEY_ID"
})
public class ActKeyItem {

    @JsonProperty("ACTKEY_ID")
    private String actkeyId;

}

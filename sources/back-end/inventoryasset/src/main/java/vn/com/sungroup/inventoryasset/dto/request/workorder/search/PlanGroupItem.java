
package vn.com.sungroup.inventoryasset.dto.request.workorder.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PLANNER_GROUP"
})
public class PlanGroupItem {

    @JsonProperty("PLANNER_GROUP")
    private String plannerGroup;

}

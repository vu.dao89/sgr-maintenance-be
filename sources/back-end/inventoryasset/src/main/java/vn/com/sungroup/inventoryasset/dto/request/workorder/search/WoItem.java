
package vn.com.sungroup.inventoryasset.dto.request.workorder.search;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "WO_ID"
})
public class WoItem {

    @JsonProperty("WO_ID")
    private String woId;

}


package vn.com.sungroup.inventoryasset.dto.request.workorder.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "WO_DES",
    "WO_START_DATE_FROM",
    "WO_START_DATE_TO",
    "WO_FINISH_DATE_FROM",
    "WO_FINISH_DATE_TO",
    "WO",
    "WOTYPE",
    "ACTKEY",
    "PRORITY",
    "EQUIPMENT",
    "FUNCLOCATION",
    "STATUSES",
    "PERSON",
    "PLANTS",
    "WORK_CENTER",
    "PLANNER_GROUPS",
    "SYS_CONDITION"
})
public class WorkOrderSearchRequest {

    @JsonProperty("WO_DES")
    private String woDes;
    @JsonProperty("WO_START_DATE_FROM")
    private String woStartDateFrom;
    @JsonProperty("WO_START_DATE_TO")
    private String woStartDateTo;
    @JsonProperty("WO_FINISH_DATE_FROM")
    private String woFinishDateFrom;
    @JsonProperty("WO_FINISH_DATE_TO")
    private String woFinishDateTo;
    @JsonProperty("WO")
    private Wo wo;
    @JsonProperty("WOTYPE")
    private Wotype wotype;
    @JsonProperty("ACTKEY")
    private Actkey actkey;
    @JsonProperty("PRORITY")
    private Prority prority;
    @JsonProperty("EQUIPMENT")
    private Equipment equipment;
    @JsonProperty("FUNCLOCATION")
    private Funclocation funclocation;
    @JsonProperty("STATUSES")
    private Statuses statuses;
    @JsonProperty("PERSON")
    private Person person;
    @JsonProperty("PLANTS")
    private Plants plants;
    @JsonProperty("WORK_CENTER")
    private WorkCenter workCenter;
    @JsonProperty("PLANNER_GROUPS")
    private PlannerGroups plannerGroups;
    @JsonProperty("SYS_CONDITION")
    private SysCondition sysCondition;

}

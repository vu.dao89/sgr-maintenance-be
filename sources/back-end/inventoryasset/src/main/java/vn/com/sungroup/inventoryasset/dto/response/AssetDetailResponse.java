/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetDetail;

import java.io.Serializable;

/**
 * AssetDetailResponse class.
 *
 * <p>Contains information about AssetDetailResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetDetailResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private AssetDetail data;
    private String error;
}

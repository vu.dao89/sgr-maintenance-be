/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetGetCostcenterData;

import java.io.Serializable;

/**
 * AssetGetCostcenterResponse class.
 *
 * <p>Contains information about AssetGetCostcenterResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetGetCostcenterResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private String error;
    private AssetGetCostcenterData data;
}

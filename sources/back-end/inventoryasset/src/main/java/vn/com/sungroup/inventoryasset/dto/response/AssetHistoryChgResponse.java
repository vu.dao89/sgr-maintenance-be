/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetHistoryChgDTO;

import java.io.Serializable;
import java.util.List;

/**
 * AssetHistoryChgResponse class.
 *
 * <p>Contains information about AssetHistoryChgResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetHistoryChgResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private List<AssetHistoryChgDTO> data;
    private Integer page_total;
    private String error;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetHistoryInventoryDTO;

import java.io.Serializable;
import java.util.List;

/**
 * AssetHistoryInventoryResponse class.
 *
 * <p>Contains information about AssetHistoryInventoryResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetHistoryInventoryResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private List<AssetHistoryInventoryDTO> data;
    private String error;
    private Integer page_total;
}

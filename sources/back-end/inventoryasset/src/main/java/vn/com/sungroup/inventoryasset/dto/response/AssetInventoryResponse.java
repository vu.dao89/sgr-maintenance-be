/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AssetInventoryResponse class.
 *
 * <p>Contains information about AssetInventoryResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetInventoryResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private String data;
    private String error;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.DataMsehi;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.DataOrd4x;

import java.io.Serializable;
import java.util.List;

/**
 * AssetListResponse class.
 *
 * <p>Contains information about AssetListResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetListResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private String error;
    private List<DataMsehi> data_msehi;
    private List<DataOrd4x> data_ord4x;
}

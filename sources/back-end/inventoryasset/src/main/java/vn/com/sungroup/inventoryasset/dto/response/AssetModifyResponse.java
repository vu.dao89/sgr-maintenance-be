/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetModify;

import java.io.Serializable;

/**
 * AssetModifyResponse class.
 *
 * <p>Contains information about AssetModifyResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetModifyResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private String qrcode;
    private String error;
    private AssetModify data;
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetSearchData;

import java.io.Serializable;
import java.util.List;

/**
 * AssetSearchResponse class.
 *
 * <p>Contains information about AssetSearchResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetSearchResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private List<AssetSearchData> data;
    private String code;
    private String error;
    private Integer page_total;
}

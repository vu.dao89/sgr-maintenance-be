/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * MoveFileResponse class.
 *
 * <p>Contains information about MoveFileResponse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoveFileResponse implements Serializable {

    /** Serial Version UID. */
    private static final long serialVersionUID = 1L;

    private String code;
    private String error;
    private String data;
}

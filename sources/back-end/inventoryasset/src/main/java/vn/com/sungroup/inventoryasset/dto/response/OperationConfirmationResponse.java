package vn.com.sungroup.inventoryasset.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OperationConfirmationResponse extends ErrorResponseSAP {

  @JsonSetter("CONFIRMATION_DOC")
  private String confirmationDoc;
}

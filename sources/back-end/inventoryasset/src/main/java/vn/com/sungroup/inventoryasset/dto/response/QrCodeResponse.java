package vn.com.sungroup.inventoryasset.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import vn.com.sungroup.inventoryasset.entity.Equipment;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class QrCodeResponse implements Serializable {
    private Equipment equipment;
    private FunctionalLocation functionalLocation;
}

package vn.com.sungroup.inventoryasset.dto.response.employee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentResponse {

  private String departmentId;

  private String departmentCode;

  private String departmentName;

  private String completeName;

  private String nameShort;

  private String employeeCode;
}

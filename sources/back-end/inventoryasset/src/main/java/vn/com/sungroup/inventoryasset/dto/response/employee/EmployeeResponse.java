package vn.com.sungroup.inventoryasset.dto.response.employee;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {

  private List<EmployeeDataResponse> data;

  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public static class EmployeeDataResponse {

    @JsonSetter("id")
    private String primaryId;

    private String name;

    private String employeeId;

    private String employeeCode;

    private String parentId;

    @JsonSetter("department")
    private DepartmentResponse department;

    private Boolean active;

    private String level;

    private Integer gender;

    private Integer marital;

    private String completeName;

    private String email;

    private String emailSending;

    private String userId;
  }
}

package vn.com.sungroup.inventoryasset.dto.response.equipment;

import lombok.*;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;
import vn.com.sungroup.inventoryasset.entity.ObjectType;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EquipmentResponse {
    private String equipmentId;
    private String description;
    private String functionalLocationId;
    private FunctionalLocation functionalLocation;
    private String equipmentTypeId;
    private ObjectType equipmentType;
    private String equipmentCategoryId;
    private EquipmentCategory equipmentCategory;
    private String systemStatusId;
    private SystemStatus systemStatus;
}

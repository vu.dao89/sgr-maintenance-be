package vn.com.sungroup.inventoryasset.dto.response.equipment;

import lombok.*;
import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class GetMeasPointResponse {
    private List<MeasuringPoint> measuringPoints;
}

package vn.com.sungroup.inventoryasset.dto.response.equipment.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.equipment.sap.MeasPoint;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MeasPointItemsSAP {
    @JsonSetter("ITEM")
    List<MeasPoint> item = new ArrayList<>();
}

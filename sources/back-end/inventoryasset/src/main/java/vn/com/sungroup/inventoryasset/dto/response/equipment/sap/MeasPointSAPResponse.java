package vn.com.sungroup.inventoryasset.dto.response.equipment.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MeasPointSAPResponse extends ErrorResponseSAP {
    @JsonSetter("MSP")
    private MeasPointItemsSAP msp;
}

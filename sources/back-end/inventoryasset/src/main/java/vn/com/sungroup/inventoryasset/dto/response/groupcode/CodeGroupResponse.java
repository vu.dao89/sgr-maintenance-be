package vn.com.sungroup.inventoryasset.dto.response.groupcode;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CodeGroupResponse {
    private String catalogProfile;

    private String catalogProfileText;

    private String catalog;

    private String catalogDesc;

    private String codeGroup;

    private String codeGroupDesc;
}

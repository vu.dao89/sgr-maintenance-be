package vn.com.sungroup.inventoryasset.dto.response.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivitiesDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationHeaderDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemsDto;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotificationDetailResponse {
    private String code;
    private NotificationHeaderDto header;
    private NotificationItemsDto items;
    private NotificationActivitiesDto activities;

    private List<NotificationDocument> notificationDocuments;
}

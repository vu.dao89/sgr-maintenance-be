package vn.com.sungroup.inventoryasset.dto.response.notification;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.notification.search.NotificationSearchResultDto;

import java.io.Serializable;
import java.util.List;

@Builder
@Getter
@Setter
public class NotificationSearchResponse implements Serializable {
    private List<NotificationSearchResultDto> result;
    private int totalPage;
    private int totalItems;
}

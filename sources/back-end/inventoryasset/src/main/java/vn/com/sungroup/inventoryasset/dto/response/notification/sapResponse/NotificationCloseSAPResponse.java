package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

import java.io.Serializable;

@Data
@AllArgsConstructor

public class NotificationCloseSAPResponse extends ErrorResponseSAP implements Serializable {

}

package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationActivities;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationHeader;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.NotificationItems;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDetailSAPResponse extends ErrorResponseSAP implements Serializable {

    @JsonProperty("HEADER") private NotificationHeader header;
    @JsonProperty("ITEMS") private NotificationItems items;
    @JsonProperty("ACTIVITIES") private NotificationActivities activities;
}

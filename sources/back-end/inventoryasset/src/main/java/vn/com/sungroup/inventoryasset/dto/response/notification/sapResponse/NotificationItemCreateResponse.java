package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;

@Getter
public class NotificationItemCreateResponse extends BaseResponseSAP {
    @Builder(builderMethodName = "notificationItemCreateResponseBuilder")
    public NotificationItemCreateResponse(String document, String message, String code, String notificationItemId) {
        super(document, message, code);
        this.notificationItemId = notificationItemId;
    }
    @Setter
    private String notificationItemId;
}

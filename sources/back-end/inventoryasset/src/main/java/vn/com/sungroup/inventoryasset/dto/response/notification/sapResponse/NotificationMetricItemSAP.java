package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NotificationMetricItemSAP {

  @JsonSetter("STAT_ID")
  private String statId;

  @JsonSetter("STAT_ID_DES")
  private String statIdDescription;

  @JsonSetter("NOTIF_COUNT_STATUS")
  private String notificationCountStatus;

  @JsonSetter("PRIORITY_ITEM")
  private List<NotificationMetricPriorityItemSAP> priorityItem;

  @JsonSetter("OUTDATE_ITEM")
  private List<NotificationMetricOutDateSAP> outDateItem;

}

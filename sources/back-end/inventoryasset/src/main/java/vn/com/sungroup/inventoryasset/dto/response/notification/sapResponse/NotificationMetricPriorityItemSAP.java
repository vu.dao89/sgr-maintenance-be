package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NotificationMetricPriorityItemSAP {

  @JsonSetter("EQ_STAT_ID")
  private String eqStatId;

  @JsonSetter("PRIORITY")
  private Integer priority;

  @JsonSetter("PRIORITY_TEXT")
  private String priorityText;

  @JsonSetter("NOTIF_COUNT_PRIORITY")
  private String notificationCountPriority;

}

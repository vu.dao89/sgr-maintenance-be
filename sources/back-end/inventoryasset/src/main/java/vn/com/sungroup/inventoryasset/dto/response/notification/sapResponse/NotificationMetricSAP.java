package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NotificationMetricSAP {

  @JsonSetter("ITEM")
  private List<NotificationMetricItemSAP> item;
}

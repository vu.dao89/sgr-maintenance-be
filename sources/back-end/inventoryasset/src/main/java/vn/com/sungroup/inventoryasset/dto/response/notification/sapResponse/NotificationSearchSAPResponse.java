package vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.notification.search.sap.NotificationSearch;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationSearchSAPResponse extends ErrorResponseSAP implements Serializable {
    @JsonProperty("DATA")
    private List<NotificationSearch> DATA;
    @JsonProperty("TotalPage")
    private int totalPage;

    @JsonProperty("TotalItems")
    private int totalItems;
}

package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ComponentDetail {

  @JsonSetter("ITEM")
  private List<ComponentDetailItem> item;
}

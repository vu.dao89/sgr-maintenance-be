package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MaterialDocumentResponse extends ErrorResponseSAP {
    @JsonSetter("Document")
    private String document;
}

package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OperationComponentItemSAP {

  @JsonSetter("WO_ID")
  private String workOrderId;

  @JsonSetter("WO_DES")
  private String workOrderDescription;

  @JsonSetter("OPER_ID")
  private String operationId;

  @JsonSetter("OPER_DES")
  private String operationDescription;

  @JsonSetter("RESERVATION")
  private String reservation;

  @JsonSetter("RESERVATION_ITEM")
  private String reservationItem;

  @JsonSetter("MATERIAL")
  private String material;

  @JsonSetter("MATERIAL_DESCRIPTION")
  private String materialDescription;

  @JsonSetter("PLANT")
  private String plant;

  @JsonSetter("PLANT_NAME")
  private String plantName;

  @JsonSetter("STORAGE_LOCATION")
  private String storageLocation;

  @JsonSetter("STORAGE_LOCATION_DESCRIPTION")
  private String storageLocationDescription;

  @JsonSetter("BATCH")
  private String batch;

  @JsonSetter("QUANTITY")
  private Float quantity;

  @JsonSetter("UNIT")
  private String unit;

  @JsonSetter("REQUIREMENT_DATE")
  private String requirementDate;

  @JsonSetter("NOTE")
  private String note;

  @JsonSetter("STATUS")
  private String status;

  @JsonSetter("REMAIN_QUANTITY")
  private String remainQuantity;

  @JsonSetter("WITHDRAWAL_QUANTITY")
  private Float withdrawalQuantity;

  @Setter
  private UnitOfMeasurement unitOfMeasurement;
}

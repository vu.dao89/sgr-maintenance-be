package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OperationComponentResponse extends ErrorResponseSAP {

  @JsonSetter("COMPONENT")
  private OperationComponentSAP component;
}

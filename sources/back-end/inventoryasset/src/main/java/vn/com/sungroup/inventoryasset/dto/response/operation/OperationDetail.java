package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.*;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OperationDetail {

  @JsonSetter("WO_ID")
  private String workOrderId;

  @JsonSetter("WO_DES")
  private String workOrderDescription;

  @JsonSetter("EQUI_ID")
  private String equipmentId;

  @JsonSetter("EQ_EQUI_DES")
  private String equipmentDescription;

  @JsonSetter("FUNC_LOC_ID")
  private String functionalLocationId;

  @JsonSetter("FLOC_DES")
  private String functionalLocationDescription;

  @JsonSetter("OPER_ID")
  private String operationId;

  @JsonSetter("OPER_DES")
  private String operationDescription;

  @JsonSetter("OPER_LONGTEXT")
  private String operationLongText;

  @JsonSetter("CONTROL_KEY")
  private String controlKey;

  @JsonSetter("PERSONEL")
  private String personnel;

  @JsonSetter("PERSONNEL_NAME")
  private String personnelName;

  @Setter
  private Employee employee;

  @JsonSetter("ESTIMATE")
  private Integer estimate;

  @JsonSetter("UNIT")
  private String unit;

  @JsonSetter("OPER_SYS_STATUS")
  private String operationSystemStatus;
  @JsonSetter("MAIN_PLANT")
  private String maintenancePlantCode;
  @JsonSetter("PLANT_DES")
  private String plantDescription;
  @JsonSetter("WORK_CENTER")
  private String workCenterCode;
  @JsonSetter("WC_DES")
  private String workCenterDescription;
  @JsonSetter("ACTUAL_START")
  private String actualStart;
  @JsonSetter("ACTUAL_START_TIME")
  private String actualStartTime;
  @JsonSetter("ACTUAL_FINISH")
  private String actualFinish;
  @JsonSetter("ACTUAL_FINISH_TIME")
  private String actualFinishTime;
  @JsonSetter("ACTIVITY_TYPE")
  private String activityType;
  @JsonSetter("RESULT")
  private String result;

  @Getter
  @Setter
  private CommonStatus commonStatus;
  @Getter
  @Setter
  private ActivityType activityTypeData;
  @Getter
  @Setter
  private ControlKey controlKeyData;


  @JsonSetter("OPER_SYS_STATUS_DES")
  private String operationSystemStatusDescription;
  @Getter
  @Setter
  private List<OperationDocument> documents;

  @Getter
  @Setter
  private OperationHistoryResponse operationHistory;
  @Getter
  @Setter
  private VarianceReason varianceReason;
}

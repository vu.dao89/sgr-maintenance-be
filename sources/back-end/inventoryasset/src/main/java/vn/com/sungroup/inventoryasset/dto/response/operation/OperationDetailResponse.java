package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;
import vn.com.sungroup.inventoryasset.dto.sap.OperationMeasureDetailSAP;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OperationDetailResponse extends ErrorResponseSAP {

  @JsonSetter("OPERATION")
  private OperationDetail operation;

  @JsonSetter("SUB_OPERATION")
  private SubOperationDetail subOperation;

  @JsonSetter("COMPONENT")
  private ComponentDetail component;

  @JsonSetter("MEAS")
  private Measure measure;

  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  public static class Measure {
    @JsonSetter("ITEM")
    private List<OperationMeasureDetailSAP> items;
  }
}

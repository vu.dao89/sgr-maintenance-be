package vn.com.sungroup.inventoryasset.dto.response.operation;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OperationHistoryResponse {
    private String operationCode;
    private String superOperationCode;
    private String userId;
    private String transferredUserId;
    private String activity;
    private String note;
    private String workOrderId;
    private long startDateTime;
}

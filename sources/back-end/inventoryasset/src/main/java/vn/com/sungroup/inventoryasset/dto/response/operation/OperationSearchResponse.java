package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OperationSearchResponse extends ErrorResponseSAP {

  @JsonSetter("OPERATION")
  @Getter
  @Setter
  private OperationSearchDetail operation;
  @JsonProperty("TotalPage")
  @Getter
  @Setter
  private int totalPage;
  @JsonProperty("TotalItems")
  private int totalItems;
}

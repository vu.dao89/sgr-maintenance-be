package vn.com.sungroup.inventoryasset.dto.response.operation;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.ControlKey;
import vn.com.sungroup.inventoryasset.entity.Employee;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SubOperationDetailItem {

  @JsonSetter("WO_ID")
  private String workOrderId;

  @JsonSetter("OPER_ID")
  private String operationId;

  @JsonSetter("SUPER_OPER_ID")
  private String superOperationId;

  @JsonSetter("OPER_DES")
  private String operationDescription;

  @JsonSetter("OPER_LONGTEXT")
  private String operationLongText;

  @JsonSetter("CONTROL_KEY")
  private String controlKey;

  @JsonSetter("PERSONEL")
  private String personnel;

  @JsonSetter("PERSONNEL_NAME")
  private String personnelName;

  @Setter
  private Employee employee;

  @JsonSetter("ESTIMATE")
  private Integer estimate;

  @JsonSetter("UNIT")
  private String unit;

  @JsonSetter("OPER_SYS_STATUS")
  private String operationSystemStatus;


  @JsonSetter("OPER_SYS_STATUS_DES")
  private String operationSystemStatusDescription;
  @JsonSetter("MAIN_PLANT")
  private String maintenancePlantCode;
  @JsonSetter("PLANT_DES")
  private String maintenancePlantDesc;
  @JsonSetter("WORK_CENTER")
  private String workCenterCode;
  @JsonSetter("WC_DES")
  private String workCenterDesc;
  @JsonSetter("RESULT")
  private String result;

  @Getter
  @Setter
  private CommonStatus commonStatus;
  @Getter
  @Setter
  private List<OperationDocument> documents;
  @Getter
  @Setter
  private OperationHistoryResponse operationHistory;
  @Getter
  @Setter
  private ControlKey controlKeyData;
  @Getter
  @Setter
  private VarianceReason varianceReason;
}

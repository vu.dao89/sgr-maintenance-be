package vn.com.sungroup.inventoryasset.dto.response.operation;

import lombok.*;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
@Getter

public class SubOperationResponse extends BaseResponseSAP {
    @Builder(builderMethodName = "subOperationResponseBuilder")
    public SubOperationResponse(String document, String message, String code, String localId) {
        super(document, message, code);
        this.localId = localId;
    }
    @Setter
    private String localId;
}

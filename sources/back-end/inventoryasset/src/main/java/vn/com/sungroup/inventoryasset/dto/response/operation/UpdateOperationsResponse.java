package vn.com.sungroup.inventoryasset.dto.response.operation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;

import java.util.List;
@Getter
public class UpdateOperationsResponse extends BaseResponseSAP {
    @Builder(builderMethodName = "updateOperationsResponseBuilder")
    public UpdateOperationsResponse(String document, String message, String code,
                                    List<SubOperationResponse> subOperations){
        super(document, message, code);
        this.subOperations = subOperations;
    }
    @Setter
    private List<SubOperationResponse> subOperations;
}

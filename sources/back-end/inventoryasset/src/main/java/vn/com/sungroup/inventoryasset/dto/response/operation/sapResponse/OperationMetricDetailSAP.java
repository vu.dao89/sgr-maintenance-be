package vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMetricDetailSAP {

  @JsonSetter("STAT_ID")
  private String statId;
  @JsonSetter("STAT_ID_DES")
  private String statIdDescription;
  @JsonSetter("OPER_COUNT_STATUS")
  private String operationCountStatus;
  @JsonSetter("PRIORITY_ITEM")
  private List<OperationMetricPriorityItem> priorityItem;
  @JsonSetter("OUTDATE_ITEM")
  private List<OperationMetricOutOfDateItem> outDateItem;
}

package vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMetricOutOfDateItem {
    @JsonSetter("EQ_STAT_ID")
    private String eqStatId;
    @JsonSetter("OUT_OF_DATE_TEXT")
    private String outOfDateText;
    @JsonSetter("COUNT_OUT_OF_DATE")
    private String countOutOfDate;
}

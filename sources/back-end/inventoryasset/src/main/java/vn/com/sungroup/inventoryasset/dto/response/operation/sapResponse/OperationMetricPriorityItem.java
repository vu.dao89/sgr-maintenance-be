package vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMetricPriorityItem {
    @JsonSetter("EQ_STAT_ID")
    private String eqStatId;
    @JsonSetter("PRIORITY")
    private String priority;
    @JsonSetter("PRIORITY_TEXT")
    private String priorityText;
    @JsonSetter("OPER_COUNT_PRIORITY")
    private String operationCountPriority;
}

package vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMetricsSAP extends ErrorResponseSAP {

  @JsonSetter("OPERATION")
  private Operation operation;

  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  private static class Operation {
    @JsonSetter("ITEM")
    private List<OperationMetricDetailSAP> item;
  }
}

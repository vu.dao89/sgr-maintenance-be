package vn.com.sungroup.inventoryasset.dto.response.workorder;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
@Getter
public class WorkOrderChangeResponseSAP extends BaseResponseSAP {
    public WorkOrderChangeResponseSAP(String document) {
        super(document);
    }

    public WorkOrderChangeResponseSAP() {

    }
}

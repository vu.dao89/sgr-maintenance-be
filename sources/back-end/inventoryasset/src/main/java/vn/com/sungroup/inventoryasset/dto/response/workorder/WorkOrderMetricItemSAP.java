package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricItemSAP {

  @JsonSetter("STAT_ID")
  private String statId;

  @JsonSetter("STAT_ID_DES")
  private String statIdDescription;

  @JsonSetter("WO_COUNT_STATUS")
  private String workOrderCountStatus;

  @JsonSetter("PRIORITY_ITEM")
  private List<WorkOrderMetricPriorityItemSAP> priorityItem;

  @JsonSetter("OUTDATE_ITEM")
  private List<WorkOrderMetricOutDateSAP> outDateItem;
}

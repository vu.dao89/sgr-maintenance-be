package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricOutDateSAP {

  @JsonSetter("EQ_STAT_ID")
  private String eqStatId;

  @JsonSetter("OUT_OF_DATE_TEXT")
  private String outOfDateText;

  @JsonSetter("COUNT_OUT_OF_DATE")
  private String countOutOfDate;

}

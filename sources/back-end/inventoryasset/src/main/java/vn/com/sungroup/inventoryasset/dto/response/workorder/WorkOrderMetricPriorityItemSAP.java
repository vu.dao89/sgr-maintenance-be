package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricPriorityItemSAP {

  @JsonSetter("EQ_STAT_ID")
  private String eqStatId;

  @JsonSetter("PRIORITY")
  private Integer priority;

  @JsonSetter("PRIORITY_TEXT")
  private String priorityText;

  @JsonSetter("WO_COUNT_PRIORITY")
  private String workOrderCountPriority;
}

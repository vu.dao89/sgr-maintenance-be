package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class WorkOrderMetricResponse extends ErrorResponseSAP {

  @JsonSetter("WORKORDER")
  private WorkOrderMetricSAP workOrder;
}

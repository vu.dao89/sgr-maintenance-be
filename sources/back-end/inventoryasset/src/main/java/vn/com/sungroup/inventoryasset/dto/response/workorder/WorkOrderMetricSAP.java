package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderMetricSAP {

  @JsonSetter("ITEM")
  private List<WorkOrderMetricItemSAP> item;
}

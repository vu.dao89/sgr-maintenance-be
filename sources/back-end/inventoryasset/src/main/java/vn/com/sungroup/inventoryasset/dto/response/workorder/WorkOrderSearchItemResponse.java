package vn.com.sungroup.inventoryasset.dto.response.workorder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "WO_ID",
        "WO_DES",
        "WO_TYPE",
        "MAIN_ACT_KEY",
        "PRORITY",
        "EQUI_ID",
        "FUNC_LOC_ID",
        "STATUS",
        "MAIN_PERSON",
        "MAIN_PLANT",
        "WORK_CENTER",
        "PLANNER_GROUP",
        "SYSCOND",
        "WO_START_DATE",
        "WO_FINISH_DATE"
})
public class WorkOrderSearchItemResponse implements Serializable {
    @JsonProperty("WO_ID")
    private String woId;
    @JsonProperty("WO_DES")
    private String woDes;
    @JsonProperty("WO_TYPE")
    private String woType;
    @JsonProperty("MAIN_ACT_KEY")
    private String mainActKey;
    @JsonProperty("PRORITY")
    private String prority;
    @JsonProperty("EQUI_ID")
    private String equiId;
    @JsonProperty("FUNC_LOC_ID")
    private String funcLocId;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("MAIN_PERSON")
    private String mainPerson;
    @JsonProperty("MAIN_PLANT")
    private String mainPlant;
    @JsonProperty("WORK_CENTER")
    private String workCenter;
    @JsonProperty("PLANNER_GROUP")
    private String plannerGroup;
    @JsonProperty("SYSCOND")
    private String syscond;
    @JsonProperty("WO_START_DATE")
    private String woStartDate;
    @JsonProperty("WO_FINISH_DATE")
    private String woFinishDate;
}

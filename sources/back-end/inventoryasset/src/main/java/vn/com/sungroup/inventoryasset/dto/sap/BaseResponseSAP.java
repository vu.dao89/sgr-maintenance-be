package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import vn.com.sungroup.inventoryasset.dto.response.operation.SubOperationResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.UpdateOperationsResponse;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class BaseResponseSAP extends ErrorResponseSAP implements Serializable {
    @JsonSetter("Document")
    private String document;

    public BaseResponseSAP(String document, String message, String code) {
        super(code, message);
        this.document = document;
    }

    public BaseResponseSAP(){

    }

    public SubOperationResponse toSubOperationResponse() {
        var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                .document(document)
                .message(getMessage())
                .code(getCode())
                .build();
        return subOperationResponse;
    }

    public UpdateOperationsResponse toUpdateOperationsResponse() {
        var updateOperationsResponse = UpdateOperationsResponse.updateOperationsResponseBuilder()
                .document(document)
                .message(getMessage())
                .code(getCode())
                .build();

        return updateOperationsResponse;
    }
}

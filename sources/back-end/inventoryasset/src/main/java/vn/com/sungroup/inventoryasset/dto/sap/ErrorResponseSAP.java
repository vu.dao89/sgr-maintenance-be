package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponseSAP  implements Serializable {

  @JsonSetter("Code")
  private String code;
  @JsonSetter("Message")
  private String message;

  public void setMessage(String messageValue){
    message = messageValue;
  }
}

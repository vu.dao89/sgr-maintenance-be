package vn.com.sungroup.inventoryasset.dto.sap;

import lombok.*;
import vn.com.sungroup.inventoryasset.mapper.OperationMapper;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class OperationChangeRequest {
    @NotBlank(message = "workOrderId is required")
    private String workOrderId;
    @NotBlank(message = "operationId is required")
    private String operationId;
    private String superOperationId;
    @NotBlank(message = "operationDesc is required")
    private String operationDesc;
    private String operationLongText;
    private String equipmentId;
    @NotBlank(message = "controlKey is required")
    private String controlKey;
    private String maintenancePlantId;
    private String workCenterId;
    private String functionalLocationId;
    private String personnel;
    @NotBlank(message = "estimate is required")
    private String estimate;
    @NotBlank(message = "unit is required")
    private String unit;

    private String activityType;
    @NotBlank(message = "Type update not correct(add/change/delete)")
    private String type;

    private List<UUID> documentIds;

    public OperationCreateRequest toOperationCreateRequest() {
        return OperationMapper.INSTANCE.toOperationCreateRequest(this);
    }
}

package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class OperationChangeRequestSAP {
    @JsonProperty("WO_ID")
    private String workOrderId;
    @JsonProperty("OPER_ID")
    private String operationId;
    @JsonProperty("SUPER_OPER_ID")
    private String superOperationId;
    @JsonProperty("OPER_DES")
    private String operationDesc;
    @JsonProperty("OPER_LONGTEXT")
    private String operationLongText;
    @JsonProperty("EQUI_ID")
    private String equipmentId;
    @JsonProperty("CONTROL_KEY")
    private String controlKey;
    @JsonProperty("MAIN_PLANT")
    private String maintenancePlantCode;
    @JsonProperty("WORK_CENTER")
    private String workCenterCode;
    @JsonProperty("FUNCTIONAL_LOCATION")
    private String functionalLocationId;
    @JsonProperty("PERSONNEL")
    private String personnel;
    @JsonProperty("ESTIMATE")
    private String estimate;
    @JsonProperty("UNIT")
    private String unit;
    @JsonProperty("ACTIVITY_TYPE")
    private String activityType;
    @JsonProperty("TYPE")
    private String type;
    @JsonProperty("EMAIL")
    private String email;

    public void updateDescription() {
        if (operationLongText.length() <= 40) {
            operationDesc = operationLongText;
        } else {
            var subString = operationLongText.substring(0, 40);
            operationDesc = subString;
        }
    }
}

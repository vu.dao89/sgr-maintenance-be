package vn.com.sungroup.inventoryasset.dto.sap;

import lombok.*;
import vn.com.sungroup.inventoryasset.mapper.OperationMapper;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class OperationCreateRequest {

    @NotBlank(message = "workOrderId is required")
    private String workOrderId;
    private String operationId;
    private String superOperationId;
    private String operationDesc;
    private String operationLongText;
    private String equipmentId;
    @NotBlank(message = "controlKey is required")
    private String controlKey;
    @NotBlank(message = "maintenancePlantId is required")
    private String maintenancePlantId;
    @NotBlank(message = "workCenterId is required")
    private String workCenterId;
    private String functionalLocationId;
    @NotBlank(message = "personnel is required")
    private String personnel;
    @NotBlank(message = "estimate is required")
    private String estimate;
    @NotBlank(message = "unit is required")
    private String unit;

    private String activityType;
    @NotBlank(message = "Type should be ADD/CHANGE")
    private String type;

    @NotBlank(message = "localId is required")
    private String localId;
    private List<UUID> documentIds;

    private List<OperationCreateRequest> subOperations;

    public OperationChangeRequest toOperationChangeRequest() {
        return OperationMapper.INSTANCE.toOperationChangeRequest(this);
    }
}

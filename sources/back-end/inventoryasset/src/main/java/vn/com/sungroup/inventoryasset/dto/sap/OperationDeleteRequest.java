package vn.com.sungroup.inventoryasset.dto.sap;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
public class OperationDeleteRequest {
    @NotBlank(message = "workOrderId is required")
    private String workOrderId;
    @NotBlank(message = "operationId is required")
    private String operationId;
    private String subOperationId;
}

package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationHistoryResponse;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

import java.util.List;

@Getter
@NoArgsConstructor
public class OperationItemSAP {

  @JsonSetter("WO_ID")
  private String workOrderId;

  @JsonSetter("OPER_ID")
  private String operationId;

  @JsonSetter("SUPER_OPER_ID")
  private String superOperationId;

  @JsonSetter("OPER_DES")
  private String operationDescription;

  @JsonSetter("OPER_LONGTEXT")
  private String operationLongText;

  @JsonSetter("EQUI_ID")
  private String equipmentId;

  @JsonSetter("EQ_EQUI_DES")
  private String equipmentDescription;

  @JsonSetter("FUNC_LOC_ID")
  private String functionalLocationId;

  @JsonSetter("FLOC_DES")
  private String functionalLocationDescription;

  @JsonSetter("MAIN_PLANT")
  private String mainPlant;

  @JsonSetter("PLANT_DES")
  private String plantDes;

  @JsonSetter("WORK_CENTER")
  private String workCenter;

  @JsonSetter("WC_DES")
  private String workCenterDes;

  @JsonSetter("CONTROL_KEY")
  private String controlKey;

  @JsonSetter("PERSONNEL")
  private String personnel;

  @JsonSetter("PERSONNEL_NAME")
  private String personnelName;

  @JsonSetter("OPER_SYS_STATUS")
  private String operationSystemStatus;

  @JsonSetter("OPER_SYS_STATUS_DES")
  private String operationSystemStatusDescription;

  @JsonSetter("ESTIMATE")
  private String estimate;

  @JsonSetter("UNIT")
  private String unit;

  @JsonSetter("RESERVATION")
  private String reservation;

  @JsonSetter("RESERVATION_WITHDRAW")
  private String reservationWithdraw;

  @JsonSetter("PRT_REQUIRE")
  private String prtRequire;

  @JsonSetter("PRT_CONFIRM")
  private String prtConfirm;

  @JsonSetter("SUB_OPERATION_REQUIRE")
  private String subOperationRequire;

  @JsonSetter("SUB_OPERATION_CONFIRM")
  private String subOperationConfirm;
  @JsonSetter("RESULT")
  private String result;
  @JsonSetter("ACTUAL_START")
  private String actualStart;
  @JsonSetter("ACTUAL_START_TIME")
  private String actualStarTime;
  @JsonSetter("ACTUAL_FINISH")
  private String actualFinish;
  @JsonSetter("ACTUAL_FINISH_TIME")
  private String actualFinishTime;
  @Setter
  private List<OperationDocument> documents;
  @Setter
  private OperationHistoryResponse operationHistory;
  @Getter
  @Setter
  private VarianceReason varianceReason;
  @Getter
  @Setter
  private CommonStatus operationStatus;
  @Getter
  @Setter
  private WorkCenter workCenterDetail;
}
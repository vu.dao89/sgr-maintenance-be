package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMeasureDetailSAP {

  @JsonSetter("WO_ID")
  private String workOrderId;
  @JsonSetter("WO_DES")
  private String workOrderDescription;
  @JsonSetter("OPER_ID")
  private String operationId;
  @JsonSetter("OPER_DES")
  private String operationDescription;
  @JsonSetter("PRT_ITEM")
  private String prtItem;
  @JsonSetter("MEAS_POINT")
  private String measurePoint;
  @JsonSetter("EQUI_ID")
  private String equipmentId;
  @JsonSetter("MEAS_POINT_POSITION")
  private String measurePointPosition;
  @JsonSetter("DESCRIPTION")
  private String description;
  @JsonSetter("INTERNAL_CHAR_NO")
  private String internalCharNo;
  @JsonSetter("TARGET_VALUE")
  private String targetValue;
  @JsonSetter("COUNTER")
  private String counter;
  @JsonSetter("LOWER_RANGE")
  private Float lowerRange;
  @JsonSetter("UPPER_RANGE")
  private Float upperRange;
  @JsonSetter("RANGEUNIT")
  private String rangeUnit;
  @JsonSetter("TEXT")
  private String text;
  @JsonSetter("CODE_GROUP")
  private String codeGroup;
  @JsonSetter("RESULT")
  private Float result;

  @Setter
  private UnitOfMeasurement unitOfMeasurement;
}

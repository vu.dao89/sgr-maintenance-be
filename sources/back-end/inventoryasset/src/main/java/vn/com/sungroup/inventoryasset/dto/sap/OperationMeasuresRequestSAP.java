package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperationMeasuresRequestSAP {

  @JsonProperty("WO_ID")
  private String workOrderId;

  @JsonProperty("OPER_ID")
  private String operationId;
}

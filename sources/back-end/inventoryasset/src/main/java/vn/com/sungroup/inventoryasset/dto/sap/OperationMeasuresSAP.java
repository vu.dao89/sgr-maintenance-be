package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OperationMeasuresSAP extends ErrorResponseSAP {

  @JsonSetter("MEAS")
  private Measure measure;

  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  private static class Measure {
    @JsonSetter("ITEM")
    private List<OperationMeasureDetailSAP> items;
  }
}

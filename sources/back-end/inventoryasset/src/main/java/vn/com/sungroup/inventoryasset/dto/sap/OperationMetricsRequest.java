package vn.com.sungroup.inventoryasset.dto.sap;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.sap.OperationMetricsRequestSAP.MaintenancePlant;
import vn.com.sungroup.inventoryasset.dto.sap.OperationMetricsRequestSAP.MaintenancePlantInfo;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class OperationMetricsRequest {

  private List<String> maintenancePlantIds;
  private String mainPerson;
  private String personnelRes;
  private String operationFinishFrom;
  private String operationFinishTo;

  public MaintenancePlant convertToMaintenancePlant() {
    var items = this.maintenancePlantIds.stream()
        .map(id -> MaintenancePlantInfo.builder().maintenancePlantId(id).build())
        .collect(Collectors.toList());

    return MaintenancePlant.builder().items(items).build();
  }
}

package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperationMetricsRequestSAP {

  @JsonProperty("MAIN_PERSON")
  private String mainPerson;
  @JsonProperty("PERSONNEL_RES")
  private String personnelRes;
  @JsonProperty("OP_FINISH_FROM")
  private String operationFinishFrom;
  @JsonProperty("OP_FINISH_TO")
  private String operationFinishTo;

  @JsonProperty("MAIN_PLANT")
  private MaintenancePlant maintenancePlant;

  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  public static class MaintenancePlant {

    @JsonProperty("ITEM")
    private List<MaintenancePlantInfo> items;
  }

  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  public static class MaintenancePlantInfo {

    @JsonProperty("MAIN_PLANT_ID")
    private String maintenancePlantId;
  }
}

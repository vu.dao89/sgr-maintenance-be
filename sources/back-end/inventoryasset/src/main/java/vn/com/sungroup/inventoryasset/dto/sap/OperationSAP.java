package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class OperationSAP {

  @JsonSetter("ITEM")
  private List<OperationItemSAP> item;

}


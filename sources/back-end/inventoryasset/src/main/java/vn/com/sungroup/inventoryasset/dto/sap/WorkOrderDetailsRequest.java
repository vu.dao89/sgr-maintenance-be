package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkOrderDetailsRequest {

  @JsonProperty("WO_ID")
  private String workOrderId;
}

package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonInclude(Include.NON_NULL)
public class WorkOrderDetailsSAP extends ErrorResponseSAP {

  @JsonSetter("WO")
  private WorkOrderSearchItemSAP workOrder;

  @JsonSetter("OPERATION")
  private OperationSAP operation;
}

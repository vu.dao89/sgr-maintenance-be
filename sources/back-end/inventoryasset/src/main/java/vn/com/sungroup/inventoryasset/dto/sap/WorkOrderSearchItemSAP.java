package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WorkOrderSearchItemSAP {

  @JsonSetter("WO_ID")
  private String workOrderId;
  @JsonSetter("WO_DES")
  private String workOrderDescription;
  @JsonSetter("WO_LONGTEXT")
  private String workOrderLongDescription;
  @JsonSetter("WO_TYPE")
  private String workOrderType;
  @JsonSetter("WO_TYPE_DES")
  private String workOrderTypeDescription;
  @JsonSetter("MAIN_ACT_KEY")
  private String mainActKey;
  @JsonSetter("MAIN_ACT_KEY_DES")
  private String mainActivityKeyDescription;
  @JsonSetter("PRORITY")
  private String priority;
  @Setter
  private PriorityType priorityType;
  @JsonSetter("PRORITY_TEXT")
  private String priorityText;
  @JsonSetter("EQUI_ID")
  private String equipmentId;
  @JsonSetter("EQ_EQUI_DES")
  private String equipmentDescription;
  @JsonSetter("FUNC_LOC_ID")
  private String functionalLocationId;
  @JsonSetter("FLOC_DES")
  private String functionalLocationDescription;
  @JsonSetter("STATUS")
  private String status;
  @JsonSetter("WO_SYS_STATUS")
  private String workOrderSystemStatus;
  @JsonSetter("WO_SYS_STATUS_DES")
  private String workOrderSystemStatusDescription;
  @JsonSetter("MAIN_PERSON")
  private String mainPerson;
  @JsonSetter("MAIN_PLANT")
  private String mainPlant;
  @JsonSetter("MAIN_PLANT_DES")
  private String maintenancePlantDescription;
  @JsonSetter("WORK_CENTER")
  private String workCenter;
  @JsonSetter("WORK_CENTER_DES")
  private String workCenterDescription;
  @JsonSetter("PLANNER_GROUP")
  private String plannerGroup;
  @JsonSetter("PLANNER_GROUP_DES")
  private String plannerGroupDescription;
  @JsonSetter("SYS_CONDITION")
  private String systemCondition;
  @JsonSetter("SYS_CONDITION_TEXT")
  private String systemConditionText;
  @JsonSetter("WO_START_DATE")
  private String woStartDate;
  @JsonSetter("WO_START_TIME")
  private String woStartTime;
  @JsonSetter("WO_FINISH_DATE")
  private String woFinishDate;
  @JsonSetter("WO_FINISH_TIME")
  private String woFinishTime;
  @JsonSetter("SORT_FIELD")
  private String sortField;
  @JsonSetter("NOTIF_ID")
  private String notificationId;
  @JsonSetter("PERSONNEL_NAME")
  private String personnelName;
  @Setter
  @Getter
  private String thumbnailUrl;
  @Setter
  @Getter
  private String originalUrl;
  @Setter
  @Getter
  private String latitude;
  @Setter
  @Getter
  private String longitude;
  @Setter
  @Getter
  private CommonStatus commonStatus;
  @Setter
  @Getter
  private WorkCenter workCenterDetail;
}


package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.request.BaseRequestMPlant;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class WorkOrderSearchRequest extends BaseRequestMPlant {

    private List<String> workCenterIds;
    private List<String> workOrderTypeIds;
    private List<String> equipmentIds;
    private List<String> functionalLocationIds;

    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> statusIds = Arrays.asList("");
    private List<String> priorityIds;
    @JsonSetter(nulls = Nulls.SKIP)
    private List<String> mainPersonIds = Arrays.asList();;

    private String keyword;
    private String workOrderDescription;
    private String workOrderStartDateFrom;
    private String workOrderStartDateTo;
    private String workOrderFinishDateFrom;
    private String workOrderFinishDateTo;

    public PlantsSAP convertToPlantsSAP() {
        var mainPlanItems = this.getMaintenancePlantCodes().stream()
                .map(id -> MainPlanItemSAP.builder().mainPlant(id).build())
                .collect(Collectors.toList());

        return PlantsSAP.builder().item(mainPlanItems).build();
    }

    public WorkCenterSAP convertToWorkCenterSAP() {
        var workCenterItems = this.workCenterIds.stream()
                .map(id -> WorkCenterItemSAP.builder().workCenterId(id).build())
                .collect(Collectors.toList());

        return WorkCenterSAP.builder().item(workCenterItems).build();
    }

    public WotypeSAP convertToWorkOrderTypeSAP() {
        var items = this.workOrderTypeIds.stream()
                .map(id -> WoTypeItemSAP.builder().workOrderTypeId(id).build())
                .collect(Collectors.toList());

        return WotypeSAP.builder().item(items).build();
    }

    public EquipmentSAP convertToEquipmentSAP() {
        var items = this.equipmentIds.stream()
                .map(id -> EquiItemSAP.builder().equipmentId(id).build())
                .collect(Collectors.toList());

        return EquipmentSAP.builder().item(items).build();
    }

    public FunclocationSAP convertToFunctionalLocationSAP() {
        var items = this.functionalLocationIds.stream()
                .map(id -> FuncLocItemSAP.builder().functionalLocationId(id).build())
                .collect(Collectors.toList());

        return FunclocationSAP.builder().item(items).build();
    }

    public StatusesSAP convertToStatusesSAP() {
        var items = this.statusIds.stream()
                .map(id -> StatusItemSAP.builder().statusId(id).build())
                .collect(Collectors.toList());

        return StatusesSAP.builder().item(items).build();
    }

    public PrioritySAP convertToPrioritySAP() {
        var items = this.priorityIds.stream()
                .map(id -> PriorityItemSAP.builder().priorityId(id).build())
                .collect(Collectors.toList());

        return PrioritySAP.builder().item(items).build();
    }

    public PersonSAP convertToMainPersonSAP() {
        var items = this.mainPersonIds.stream()
                .map(id -> PersonItemSAP.builder().personId(id).build())
                .collect(Collectors.toList());

        return PersonSAP.builder().item(items).build();
    }
}

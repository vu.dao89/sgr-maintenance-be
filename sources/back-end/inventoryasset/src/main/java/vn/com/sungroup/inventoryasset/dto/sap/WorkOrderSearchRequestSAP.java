
package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.request.BasePageSizeRequest;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkOrderSearchRequestSAP extends BasePageSizeRequest {

  @JsonProperty("KEYWORD")
  private String keyword;
  @JsonProperty("WO_DES")
  private String workOrderDescription;
  @JsonProperty("WO_START_DATE_FROM")
  private String workOrderStartDateFrom;
  @JsonProperty("WO_START_DATE_TO")
  private String workOrderStartDateTo;
  @JsonProperty("WO_FINISH_DATE_FROM")
  private String workOrderFinishDateFrom;
  @JsonProperty("WO_FINISH_DATE_TO")
  private String workOrderFinishDateTo;
  @JsonProperty("WO")
  private WoSAP workOrder;
  @JsonProperty("WOTYPE")
  private WotypeSAP workOrderType;
  @JsonProperty("ACTKEY")
  private ActkeySAP activityKey;
  @JsonProperty("PRORITY")
  private PrioritySAP priority;
  @JsonProperty("EQUIPMENT")
  private EquipmentSAP equipment;
  @JsonProperty("FUNCLOCATION")
  private FunclocationSAP functionalLocation;
  @JsonProperty("STATUSES")
  private StatusesSAP statuses;
  @JsonProperty("PERSON")
  private PersonSAP person;
  @JsonProperty("PLANTS")
  private PlantsSAP plants;
  @JsonProperty("WORK_CENTER")
  private WorkCenterSAP workCenter;
  @JsonProperty("PLANNER_GROUPS")
  private PlannerGroupsSAP plannerGroups;
  @JsonProperty("SYS_CONDITION")
  private SysConditionSAP sysCondition;
}

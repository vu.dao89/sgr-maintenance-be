package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Code",
        "DATA"
})
public class WorkOrderSearchResponse implements Serializable {
    @JsonProperty("Code")
    private String code;
    @JsonProperty("DATA")
    private List<WorkOrderSearchItemResponse> data = null;
}

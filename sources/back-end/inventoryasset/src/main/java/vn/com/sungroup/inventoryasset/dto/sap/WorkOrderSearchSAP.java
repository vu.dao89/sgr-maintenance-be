package vn.com.sungroup.inventoryasset.dto.sap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.*;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkOrderSearchSAP extends ErrorResponseSAP {

  @JsonSetter("DATA")
  @Getter
  @Setter
  private List<WorkOrderSearchItemSAP> data;
  @JsonProperty("TotalPage")
  private int totalPage;

  @JsonProperty("TotalItems")
  private int totalItems;
}

package vn.com.sungroup.inventoryasset.dto.storagelocation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class StorageLocationDto extends BaseDto {

    private String id;
    @CsvBindByName(column = "SLOC")
    @JsonProperty("SLOC")
    private String storageLocationId;

    @CsvBindByName(column = "SLOC_DES")
    @JsonProperty("SLOC_DES")
    private String description;

    @CsvBindByName(column = "PLANT")
    @JsonProperty("PLANT")
    private String plantId;
}

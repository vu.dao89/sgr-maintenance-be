package vn.com.sungroup.inventoryasset.dto.systemcondition;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.*;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SystemConditionDto extends BaseDto {

    @Setter
    private UUID id;

    @Setter
    private String idSys;

    @CsvBindByName(column = "SYS_COND")
    @JsonProperty("SYS_COND")
    private String sysCond;

    @CsvBindByName(column = "SYS_COND_TEXT")
    @JsonProperty("SYS_COND_TEXT")
    private String sysCondText;
}

package vn.com.sungroup.inventoryasset.dto.uom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UnitOfMeasureDto extends BaseDto {
    private UUID id;

    private String idUnit;

    @CsvBindByName(column = "UOM")
    @JsonProperty("UOM")
    private String uom;
    @CsvBindByName(column = "UOM_DES")
    @JsonProperty("UOM_DES")
    private String uomDes;
    @CsvBindByName(column = "DIMENSION")
    @JsonProperty("DIMENSION")

    private String dimension;
    @CsvBindByName(column = "ROUNDING")
    @JsonProperty("ROUNDING")
    private String rounding;
}

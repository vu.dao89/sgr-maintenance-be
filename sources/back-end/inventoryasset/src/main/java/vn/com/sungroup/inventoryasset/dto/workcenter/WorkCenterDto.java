package vn.com.sungroup.inventoryasset.dto.workcenter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class WorkCenterDto extends BaseDto {
    @CsvBindByName(column = "WORK_CENTER")
    @JsonProperty("WORK_CENTER")
    private String code;

    @CsvBindByName(column = "PLANT")
    @JsonProperty("PLANT")
    private String maintenancePlantId;

    @CsvBindByName(column = "WC_CAT")
    @JsonProperty("WC_CAT")
    private String category;

    @CsvBindByName(column = "WC_DES")
    @JsonProperty("WC_DES")
    private String description;
    @CsvBindByName(column = "COSTCENTER")
    @JsonProperty("COSTCENTER")
    private String costCenterCode;
}

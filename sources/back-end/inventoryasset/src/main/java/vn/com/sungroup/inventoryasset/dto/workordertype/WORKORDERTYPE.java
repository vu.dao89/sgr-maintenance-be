package vn.com.sungroup.inventoryasset.dto.workordertype;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.sungroup.inventoryasset.dto.equipment.BaseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class WORKORDERTYPE extends BaseDto {
    @JsonProperty("_id")
    private UUID id;

//    @CsvBindByName(column = "WO_TYPE")
//    @JsonProperty("WO_TYPE")
//    public String workOrderTypeId;

    @CsvBindByName(column = "WO_TYPE_DES")
    @JsonProperty("WO_TYPE_DES")
    public String desc;

//    @CsvBindByName(column = "PLANT")
//    @JsonProperty("PLANT")
//    public String plantId;

//    @JsonIgnore
//    private PLANT plant;
//    @JsonIgnore
//    private List<NOTIFICATIONTYPE> notificationTypes = new ArrayList<>();
}

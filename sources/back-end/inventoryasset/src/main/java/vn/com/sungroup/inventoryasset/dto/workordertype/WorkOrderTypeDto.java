package vn.com.sungroup.inventoryasset.dto.workordertype;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import java.util.UUID;
import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkOrderTypeDto {

  @Setter
  private String idMg;

  @Setter
  private UUID id;

  @JsonProperty("PLANT")
  @CsvBindByName(column = "PLANT")
  private String plant;

  @JsonProperty("WO_TYPE")
  @CsvBindByName(column = "WO_TYPE")
  private String woType;

  @JsonProperty("WO_TYPE_DES")
  @CsvBindByName(column = "WO_TYPE_DES")
  private String woTypeDes;

  @JsonProperty("PRIORITY_TYPE")
  @CsvBindByName(column = "PRIORITY_TYPE")
  private String priorityType;
}

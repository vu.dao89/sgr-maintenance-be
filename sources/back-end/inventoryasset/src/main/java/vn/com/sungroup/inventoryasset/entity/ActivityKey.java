package vn.com.sungroup.inventoryasset.entity;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "activity_key")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ActivityKey {

    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String workOrderType;

    @Column
    public String type;

    @Column
    public String description;
}

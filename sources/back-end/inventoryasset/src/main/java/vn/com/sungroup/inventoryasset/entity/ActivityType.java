package vn.com.sungroup.inventoryasset.entity;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "activity_type")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ActivityType {

    @Id
    @GeneratedValue
    private UUID id;

    private String type;

    private String description;

    private String costCenter;

    private String costCenterShortText;

}

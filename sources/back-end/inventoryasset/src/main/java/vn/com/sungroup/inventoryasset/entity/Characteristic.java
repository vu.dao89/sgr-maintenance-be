package vn.com.sungroup.inventoryasset.entity;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "characteristic")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Characteristic {

    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String charValue;

    @Column
    private String valueRel;

    @Column
    private String objectClassFlag;

    @Column
    private String internalCounter;

    @Column
    private String classType;

    @Column
    private String charValCounter;

    @Column
    private String charId;

    @Column
    private String charValDesc;

    @Column
    private String classId;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "characteristics")
    @JsonIgnore
    private List<Equipment> equipments;
}

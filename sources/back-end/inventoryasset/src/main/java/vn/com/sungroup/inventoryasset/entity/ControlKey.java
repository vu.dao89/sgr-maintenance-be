package vn.com.sungroup.inventoryasset.entity;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "control_key")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ControlKey {

  @Id
  @GeneratedValue
  private UUID id;

  private String code;

  private String description;
}

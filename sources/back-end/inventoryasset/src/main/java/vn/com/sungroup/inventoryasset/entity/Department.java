package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "department")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Department implements Serializable {

  @Id
  @GeneratedValue
  private UUID id;

  private String departmentId;

  private String departmentCode;

  private String departmentName;

  private String completeName;

  private String nameShort;

  private String employeeCode;

  @ManyToMany(fetch = FetchType.LAZY, mappedBy = "departments")
  @JsonIgnore
  private List<Employee> employees;
}

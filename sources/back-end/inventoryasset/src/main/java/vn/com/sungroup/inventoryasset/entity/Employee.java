package vn.com.sungroup.inventoryasset.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "employee")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Employee  implements Serializable {

  @Id
  @GeneratedValue
  private UUID id;

  private String primaryId;

  private String name;

  private String employeeId;

  private String employeeCode;

  private String parentId;

  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinTable(
      name = "department_employee",
      joinColumns = @JoinColumn(name = "employee_id"),
      inverseJoinColumns = @JoinColumn(name = "department_id")
  )
  private List<Department> departments = new ArrayList<>();

  private Boolean active;

  private String level;

  private Integer gender;

  private Integer marital;

  private String completeName;

  private String email;

  private String emailSending;

  private String userId;
}

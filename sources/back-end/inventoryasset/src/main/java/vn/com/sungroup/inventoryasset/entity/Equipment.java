package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
public class Equipment implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    private String equipmentId;

    private String parentEquipmentId;
    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "parentEquipmentId",
            referencedColumnName = "equipmentId", insertable = false,updatable = false)
    private Equipment parentEquipment;
    @Column(insertable = false, updatable = false)
    private String equipmentTypeId;
    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "equipmentTypeId",
            referencedColumnName = "code")
    private ObjectType equipmentType;

    private String constYear;

    private String constMonth;

    @Column(insertable = false, updatable = false)
    private String maintenancePlantId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "maintenancePlantId",
        referencedColumnName = "code")
    private MaintenancePlant maintenancePlant;

    @Column(insertable = false, updatable = false)
    private String functionalLocationId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "functionalLocationId",
        referencedColumnName = "functionalLocationId")
    private FunctionalLocation functionalLocation;

    @Column(insertable = false, updatable = false)
    private String equipmentCategoryId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "equipmentCategoryId",
        referencedColumnName = "code")
    private EquipmentCategory equipmentCategory;

    @Column(insertable = false, updatable = false)
    private String systemStatusId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "systemStatusId",
        referencedColumnName = "statusId")
    private SystemStatus systemStatus;

    private String description;

    private String longDesc;

    private String location;

    private String maintenanceWorkCenterId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "maintenanceWorkCenterId", referencedColumnName = "code", insertable = false, updatable = false),
        @JoinColumn(name = "maintenancePlantId", referencedColumnName = "maintenancePlantId", insertable = false, updatable = false)
    })
    private WorkCenter maintenanceWorkCenter;

    private String costCenter;
    private String costCenterName;

    private String manuCountry;

    private String manuPartNo;

    private String manuSerialNo;

    private String manufacturer;

    private String modelNumber;

    private String plannerGroup;
    @Transient
    private String plannerGroupName;

    private String planningPlantId;

    private String plantSection;

    private String size;

    private String weight;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate createDate;

    @JsonFormat(pattern = "hhmmss")
    private LocalTime createTime;

    private LocalDate cusWarrantyEnd;

    private LocalDate cusWarrantyDate;

    private LocalDate venWarrantyEnd;

    private LocalDate venWarrantyDate;

    private String qrCode;

    private String startupDate;

    private String endOfUseDate;

    private String sortField;
    private String thumbnailUrl;
    private String originalUrl;
    private String latitude;
    private String longitude;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "equipment_partner",
        joinColumns = @JoinColumn(name = "equipment_id"),
        inverseJoinColumns = @JoinColumn(name = "partner_id")
    )
    private List<Partner> partners = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "equipment_measuring_point",
        joinColumns = @JoinColumn(name = "equipment_id"),
        inverseJoinColumns = @JoinColumn(name = "measuring_point_id")
    )
    private List<MeasuringPoint> measuringPoints = new ArrayList<>();


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "equipment_characteristic",
        joinColumns = @JoinColumn(name = "equipment_id"),
        inverseJoinColumns = @JoinColumn(name = "characteristic_id")
    )
    private List<Characteristic> characteristics = new ArrayList<>();


    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "equipment_code",referencedColumnName = "equipmentId")
    @Fetch(value = FetchMode.SELECT)
    private List<EquipmentDocument> documents = new ArrayList<>();

}

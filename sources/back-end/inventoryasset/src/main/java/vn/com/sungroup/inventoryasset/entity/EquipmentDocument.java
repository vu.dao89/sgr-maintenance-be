package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;

@Entity
@Table(name = "equipment_document")
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EquipmentDocument {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "equipment_code")
    private String equipmentCode;

    @Column(name = "document_id", insertable = false, updatable = false)
    private UUID documentId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "document_id",
            referencedColumnName = "id")
    private GlobalDocument globalDocument;
}

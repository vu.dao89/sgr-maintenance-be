package vn.com.sungroup.inventoryasset.entity;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "functional_location")
@Builder
@JsonInclude(Include.NON_NULL)
public class FunctionalLocation implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    private String functionalLocationId;

    private String parentFunctionalLocationId;

    private String constYear;

    private String constMonth;

    @Column(insertable = false, updatable = false)
    private String maintenancePlantId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "maintenancePlantId",
        referencedColumnName = "code")
    private MaintenancePlant maintenancePlant;

    @Column(insertable = false, updatable = false)
    private String functionalLocationTypeId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "functionalLocationTypeId",
        referencedColumnName = "code")
    private ObjectType functionalLocationType;

    // TODO FIELD IN XML DOES NOT EXISTS
    private String functionalLocationCategory;

    private String description;

    private String longDesc;

    @Column(insertable = false, updatable = false)
    private String objectTypeId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
        name = "objectTypeId",
        referencedColumnName = "code")
    private ObjectType objectType;

    // TODO FIELD IN XML DOES NOT EXISTS
    private String location;

    private String maintenanceWorkCenterId;

    private String costCenter;

    private String manuCountry;

    private String manuPartNo;

    private String manuSerialNo;

    private String manufacturer;

    private String modelNumber;

    private String plannerGroup;

    private String planningPlantId;

    private String plantSection;

    private String size;

    private String weight;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate createDate;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate cusWarrantyEnd;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate cusWarrantyDate;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate venWarrantyEnd;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate venWarrantyDate;

    private String qrCode;

    private String startupDate;

    private String endOfUseDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "functional_location_measuring_point",
        joinColumns = @JoinColumn(name = "functional_location_id"),
        inverseJoinColumns = @JoinColumn(name = "measuring_point_id")
    )
    private List<MeasuringPoint> measuringPoints = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "functional_location_characteristic",
        joinColumns = @JoinColumn(name = "functional_location_id"),
        inverseJoinColumns = @JoinColumn(name = "characteristic_id")
    )
    private List<Characteristic> characteristics = new ArrayList<>();
}

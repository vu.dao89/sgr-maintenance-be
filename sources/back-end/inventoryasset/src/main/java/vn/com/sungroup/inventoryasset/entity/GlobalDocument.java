package vn.com.sungroup.inventoryasset.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@Entity
@Table(name = "document")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class GlobalDocument implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "filename")
    private String fileName;
    @Column(name = "file_type")
    private String fileType;
    @Column(name = "original_url")
    private String originalUrl;
    @Column(name = "thumbnail_url")
    private String thumbnailUrl;
    @Column(name = "uploaded_at")
    private LocalDate uploadedAt;
    @Column(name = "uploaded_at_time")
    private LocalTime uploadedAtTime;
    @Column
    private Long fileSize;
}

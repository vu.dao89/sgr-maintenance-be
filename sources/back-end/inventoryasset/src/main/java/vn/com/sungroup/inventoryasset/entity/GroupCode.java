package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
@Table(name = "group_code")
public class GroupCode {
    @Id
    @GeneratedValue
    private UUID id;

    private String catalogProfile;

    private String catalogProfileText;

    private String catalog;

    private String catalogDesc;

    private String codeGroup;

    private String codeGroupDesc;

    private String code;

    private String codeDesc;
}

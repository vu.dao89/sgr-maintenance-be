package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "maintenance_plant")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class MaintenancePlant implements Serializable {

  @Id
  @GeneratedValue
  private UUID id;

  @Column
  private String code;

  private String plantName;

  private String planningPlant;
  private String businessPlace;
}

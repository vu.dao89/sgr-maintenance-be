package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
@Table(name = "master_data_processed_history")
public class MasterDataProcessedHistory {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "foldername")
    private String folderName;
    @Column(name = "filename")
    private String fileName;
    @Column(name = "status")
    private String status;
    @Column(name = "processed_at")
    private LocalDate processedAt;
    @Column(name = "processed_at_time")
    private LocalTime processedAtTime;
    @Column(name = "backedup_at")
    private LocalDate backedupAt;
    @Column(name = "backedup_at_time")
    private LocalTime backedupAtTime;

}

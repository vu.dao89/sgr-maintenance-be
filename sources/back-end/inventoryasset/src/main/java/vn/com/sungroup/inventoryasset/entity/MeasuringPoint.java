package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "measuring_point")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class MeasuringPoint {

  @Id
  @GeneratedValue
  private UUID id;

  @Column
  private String point;

  @Column
  private String position;

  @Column
  private String description;

  @Column
  private String characteristic;

  @Column
  private String targetValue;

  @Column
  private String counter;

  @Column
  private String lowerRangeLimit;

  @Column
  private String upperRangeLimit;

  @Column
  private String rangeUnit;

  @Column
  private String text;

  @Column
  private String codeGroup;

  @ManyToMany(fetch = FetchType.LAZY, mappedBy = "measuringPoints")
  @JsonIgnore
  private List<Equipment> equipments;

  @Transient
  private String lastMeasuringPointValue;
}

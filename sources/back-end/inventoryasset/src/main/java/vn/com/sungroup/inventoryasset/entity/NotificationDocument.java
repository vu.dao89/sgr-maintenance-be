package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;

@Entity
@Table(name = "notification_document")
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationDocument {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "notification_code")
    private String notificationCode;

    @Column(name = "document_id", insertable = false, updatable = false)
    private UUID documentId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "document_id",
            referencedColumnName = "id")
    private GlobalDocument globalDocument;
}

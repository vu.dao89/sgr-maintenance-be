package vn.com.sungroup.inventoryasset.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "notification_type")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class NotificationType {
    @Id
    @GeneratedValue
    private UUID id;

    private String notiType;

    private String description;

    private String catalogProfile;

    private String problems;

    private String causes;

    private String activities;

    private String objectParts;

    private String workOrderTypeId;

    private String priorityTypeId;

    private String personResponsible;

    @Transient
    private WorkOrderType workOrderType;
}

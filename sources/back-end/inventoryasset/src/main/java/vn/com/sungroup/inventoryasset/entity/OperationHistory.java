package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationHistoryResponse;
import vn.com.sungroup.inventoryasset.util.DateTimeUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
@Table(name = "operation_history")
public class OperationHistory implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    @Column( name = "operation_code")
    private String operationCode;
    @Column( name = "super_operation_code")
    private String superOperationCode;

    @Column( name = "user_id")
    private String userId;
    @Column( name = "transferred_user_id")
    private String transferredUserId;
    @Column( name = "activity")
    private String activity;
    @Column( name = "note")
    private String note;

    @Column( name = "start_at_date")
    private LocalDate startAtDate;
    @Transient
    private String startAtDateStr;

    public String getStartAtDateStr() {
        return DateTimeUtil.formatDate(this.startAtDate);
    }
    @Column( name = "start_at_time")
    private LocalTime startAtTime;

    @Transient
    private String startAtTimeStr;

    public String getStartAtTimeStr() {
        return DateTimeUtil.formatTime(this.startAtTime);
    }

    @Column( name = "workorder_id")
    private String workOrderId;

    public OperationHistoryResponse toOperationHistoryResponse(){
        var operationResponse = OperationHistoryResponse.builder()
                .startDateTime((startAtDate.atTime(startAtTime)).toEpochSecond(ZoneOffset.of("Z")))
                .activity(activity)
                .operationCode(operationCode)
                .superOperationCode(superOperationCode)
                .note(note)
                .userId(userId)
                .transferredUserId(transferredUserId)
                .workOrderId(workOrderId)
                .build();
        return operationResponse;
    }
}

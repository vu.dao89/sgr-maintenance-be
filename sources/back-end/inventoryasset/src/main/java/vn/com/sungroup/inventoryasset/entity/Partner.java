package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Where;
@Entity
@Table(name = "partner")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Partner {

    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String counter;

    @Column
    private String partnerFunction;

    @Column
    private String partnerNumber;

    @Column
    private String objectTypeCode;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "partners")
    @JsonIgnore
    private List<Equipment> equipments;
}

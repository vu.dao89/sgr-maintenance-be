package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;

@Entity
@Table(name = "personnel")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Personnel implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String code;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private String workCenterId;

    private String maintenancePlantId;

    @OneToOne(optional = false)
    @JoinColumn(name = "code",
            referencedColumnName = "employeeId", insertable=false, updatable=false)
    private Employee employee;

}

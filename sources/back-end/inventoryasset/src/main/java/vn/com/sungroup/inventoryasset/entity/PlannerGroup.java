package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

import static javax.persistence.ConstraintMode.NO_CONSTRAINT;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
@Table(name = "planner_group")
public class PlannerGroup implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String maintenancePlantId;

    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(NO_CONSTRAINT),
            name = "maintenancePlantId",
            referencedColumnName = "code")
    private MaintenancePlant maintenancePlant;

    private String plannerGroupId;
    private String name;
}

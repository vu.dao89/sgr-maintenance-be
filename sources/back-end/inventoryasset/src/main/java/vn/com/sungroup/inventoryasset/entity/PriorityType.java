package vn.com.sungroup.inventoryasset.entity;

import java.util.UUID;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "priority_type")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class PriorityType {

  @Id
  @GeneratedValue
  private UUID id;

  private String type;

  private String priority;

  private String description;

  @Column(name = "color_name")
  public String colorName;
  @Column(name = "color_code")
  public String colorCode;
}

package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "system_status")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class SystemStatus implements Serializable {

  @Id
  @GeneratedValue
  private UUID id;

  @Column(unique = true)
  public String statusId;

  public String code;

  public String description;

  public String userStatusProfile;

  public String userStatus;
}

package vn.com.sungroup.inventoryasset.entity;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Table(name = "unit_of_measurement")
public class UnitOfMeasurement {

    @Id
    @GeneratedValue
    private UUID id;

    private String code;

    private String description;

    private String dimension;

    private String rounding;
}

package vn.com.sungroup.inventoryasset.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Builder
@Table(name = "work_center")
public class WorkCenter implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String code;

    @Column
    private String category;

    @Column
    private String description;

    private String maintenancePlantId;
    @Transient
    private String maintenancePlantName;
    private String costCenterCode;
}

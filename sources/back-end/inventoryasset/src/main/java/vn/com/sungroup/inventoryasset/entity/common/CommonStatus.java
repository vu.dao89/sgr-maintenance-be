package vn.com.sungroup.inventoryasset.entity.common;

import java.util.UUID;
import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "common_status")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CommonStatus {
    @Id
    @GeneratedValue
    private UUID id;

    public String code;
    public String status;
    public String description;
    @Column(name = "color_name")
    public String colorName;
    @Column(name = "color_code")
    public String colorCode;

}

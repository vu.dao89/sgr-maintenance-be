package vn.com.sungroup.inventoryasset.entity.common;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "variance_reason")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class VarianceReason {
    @Id
    @GeneratedValue
    private UUID id;
    public String code;
    public String description;
    public Boolean require;
    public String color_code;
    public String color_name;
}

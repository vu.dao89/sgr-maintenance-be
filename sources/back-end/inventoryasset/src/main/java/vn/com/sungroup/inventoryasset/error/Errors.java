package vn.com.sungroup.inventoryasset.error;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Errors {

  public static final Integer UNKNOWN_ERROR = 0;

  public static final Integer MISSING_REQUIRED_FIELD = 1;

  public static final Integer INVALID_FIELD = 2;

  public static final Integer INVALID_SORT_PROPERTY = 3;
  public static final String INVALID_SORT_PROPERTY_MESSAGE = "Invalid sort properties";

  public static final Integer DATA_NOT_FOUND = 4;

  public static final Integer SAP_API_ERROR = 5;

  public static final Integer PROCESS_GENERATED_THUMBNAIL_ERROR = 6;
  public static final Integer CLOUD_ERROR = 7;
  public static final Integer INVALID_JSON = 8;
  public static final String INVALID_JSON_MESSAGE = "Invalid json";

  public static final Integer PERMISSION_ERROR = 9;


  // work-order
  public static final Integer WORK_ORDER_NOT_FOUND = 31;
  public static final Integer WORK_ORDER_NOT_SAME = 32;
  public static final Integer WORK_ORDER_NOT_START = 33;
  public static final Integer WORK_ORDER_NOT_COMPLETE = 34;
  public static final Integer WORK_ORDER_NOT_LOCK = 35;
  public static final Integer INVALID_NOTIFICATION_WORK_ORDER_COMPLETED = 36;
  public static final String INVALID_NOTIFICATION_WORK_ORDER_COMPLETED_MESSAGE = "Notification id should be belong to" +
          " work order";

  //Operation
  public static final Integer OPERATION_NOT_START = 41;
  public static final Integer OPERATION_NOT_HOLD = 42;
  public static final Integer OPERATION_NOT_COMPLETE = 43;
  public static final String OPERATION_NOT_COMPLETE_MESSAGE = "Operation/SubOperation is not completed";
  public static final Integer OPERATION_NOT_FOUND = 44;
  public static final Integer OPERATION_NOT_OWNER = 45;
  public static final Integer OPERATION_PERSONNEL_IS_BUSY = 46;
  public static final Integer OPERATION_COMPLETED = 47;
  public static final Integer OPERATION_MEASURE_NOT_COMPLETED = 48;
  public static final String OPERATION_MEASURE_NOT_COMPLETED_MESSAGE = "Measuring point has not result";
  public static final Integer OPERATION_DELETE_CHANGE_ERROR = 49;
  public static final String OPERATION_DELETE_CHANGE_ERROR_MESSAGE = "Only accept deleting or changing a new/release " +
          "operation or sub operation";

  // Document
  public static final Integer DOCUMENT_NOT_FOUND = 51;
  public static final String DOCUMENT_NOT_FOUND_MESSAGE = "Document not found";

  // Notification
  public static final Integer NOTIFICATION_LINK_NOT_COMPLETE_WORK_ORDER = 60;
  public static final Integer NOTIFICATION_REQUIRE_ACTIVITIES = 61;
  public static final Integer NOTIFICATION_DELETE_CHANGE_ERROR = 62;
  public static final String NOTIFICATION_DELETE_CHANGE_ERROR_MESSAGE = "Only accept deleting or changing a new/release " +
          "notification";

  public static final String NOTIFICATION_LINK_NOT_COMPLETE_WORK_ORDER_MESSAGE = "The work order should be " +
          "completed before closing notification";

  public static final String NOTIFICATION_REQUIRE_ACTIVITIES_MESSAGE = "The Notification Activity should not be null " +
          "or empty";
}
package vn.com.sungroup.inventoryasset.exception;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class ApplicationException extends Exception {

  protected final transient ErrorDetail errorDetail;

  public ApplicationException(Integer errorCode, String errorMessage) {
    super(errorMessage);

    this.errorDetail = ErrorDetail.builder()
        .errorCode(errorCode)
        .errorMessage(errorMessage)
        .build();
  }

  public ApplicationException(Integer errorCode, String errorMessage, Throwable rootCause) {
    super(errorMessage, rootCause);

    this.errorDetail = ErrorDetail.builder()
        .errorCode(errorCode)
        .errorMessage(errorMessage)
        .build();
  }
}


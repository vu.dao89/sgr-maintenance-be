package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.CLOUD_ERROR;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class CloudException extends ApplicationException {

  protected final transient ErrorDetail errorDetail;

  public CloudException(String message) {
    super(CLOUD_ERROR, message);
    this.errorDetail = ErrorDetail.builder().errorCode(CLOUD_ERROR)
        .errorMessage(message)
        .build();
  }

}

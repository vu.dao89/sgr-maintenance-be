package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.DOCUMENT_NOT_FOUND;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class DocumentNotFoundException extends ApplicationException {

  protected final transient ErrorDetail errorDetail;


  public DocumentNotFoundException(String message) {
    super(DOCUMENT_NOT_FOUND, message);
    this.errorDetail = ErrorDetail.builder().errorCode(DOCUMENT_NOT_FOUND).errorMessage(message)
        .build();
  }

}

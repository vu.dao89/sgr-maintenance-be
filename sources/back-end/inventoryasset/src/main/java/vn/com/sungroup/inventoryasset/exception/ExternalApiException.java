/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.exception;

import org.springframework.web.client.RestClientException;

/**
 * ExternalApiException class.
 *
 * <p>Contains information about External Api Exception.
 */
public class ExternalApiException extends RestClientException {

    private static final long serialVersionUID = 1L;

    public ExternalApiException(String exMessage) {
        super(exMessage);
    }

    public ExternalApiException(String exMessage, Throwable exception) {
        super(exMessage, exception);
    }
}

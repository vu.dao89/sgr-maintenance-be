package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.PROCESS_GENERATED_THUMBNAIL_ERROR;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class GenerateThumbnailException extends ApplicationException {

  protected final transient ErrorDetail errorDetail;

  public GenerateThumbnailException(String message) {
    super(PROCESS_GENERATED_THUMBNAIL_ERROR, message);
    this.errorDetail = ErrorDetail.builder().errorCode(PROCESS_GENERATED_THUMBNAIL_ERROR)
        .errorMessage(message)
        .build();
  }

}

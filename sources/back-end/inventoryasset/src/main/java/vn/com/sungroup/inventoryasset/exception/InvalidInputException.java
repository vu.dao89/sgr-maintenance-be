package vn.com.sungroup.inventoryasset.exception;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class InvalidInputException extends Exception {

  protected final transient ErrorDetail errorDetail;

  private static final String MESSAGE = "Invalid input";

  public InvalidInputException(Integer errorCode) {
    super(MESSAGE);
    this.errorDetail = ErrorDetail.builder().errorCode(errorCode).errorMessage(MESSAGE).build();
  }

  public InvalidInputException(Integer errorCode, String message) {
    super(message);
    this.errorDetail = ErrorDetail.builder().errorCode(errorCode).errorMessage(message).build();
  }

}

package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.INVALID_SORT_PROPERTY;
import static vn.com.sungroup.inventoryasset.error.Errors.INVALID_SORT_PROPERTY_MESSAGE;

public class InvalidSortPropertyException extends ApplicationException {

  public InvalidSortPropertyException(Throwable rootCause) {
    super(INVALID_SORT_PROPERTY, INVALID_SORT_PROPERTY_MESSAGE, rootCause);
  }
}

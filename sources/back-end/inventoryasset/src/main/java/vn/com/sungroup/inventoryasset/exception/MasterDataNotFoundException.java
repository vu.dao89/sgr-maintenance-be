package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.DATA_NOT_FOUND;

public class MasterDataNotFoundException extends ApplicationException {

  public MasterDataNotFoundException(String errorMessage) {
    super(DATA_NOT_FOUND, errorMessage);
  }

  public MasterDataNotFoundException(Integer errorCode, String errorMessage) {
    super(errorCode, errorMessage);
  }
}

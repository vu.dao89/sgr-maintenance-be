package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.MISSING_REQUIRED_FIELD;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class MissingRequiredFieldException extends Exception {

  protected final transient ErrorDetail errorDetail;

  public MissingRequiredFieldException(String errorMessage) {
    super(errorMessage);
    this.errorDetail = ErrorDetail.builder()
        .errorCode(MISSING_REQUIRED_FIELD)
        .errorMessage(errorMessage)
        .build();
  }

  public MissingRequiredFieldException(Integer errorCode, String message) {
    super(message);
    this.errorDetail = ErrorDetail.builder().errorCode(errorCode).errorMessage(message).build();
  }

}

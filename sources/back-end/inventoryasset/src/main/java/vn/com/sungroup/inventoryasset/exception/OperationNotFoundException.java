package vn.com.sungroup.inventoryasset.exception;

import static vn.com.sungroup.inventoryasset.error.Errors.OPERATION_NOT_FOUND;

import lombok.Getter;
import vn.com.sungroup.inventoryasset.error.ErrorDetail;

@Getter
public class OperationNotFoundException extends ApplicationException {

  protected final transient ErrorDetail errorDetail;

  private static final String MESSAGE = "Operation not found";

  public OperationNotFoundException() {
    super(OPERATION_NOT_FOUND, MESSAGE);
    this.errorDetail = ErrorDetail.builder().errorCode(OPERATION_NOT_FOUND).errorMessage(MESSAGE)
        .build();
  }

  public OperationNotFoundException(String message) {
    super(OPERATION_NOT_FOUND, message);
    this.errorDetail = ErrorDetail.builder().errorCode(OPERATION_NOT_FOUND).errorMessage(message)
        .build();
  }

}

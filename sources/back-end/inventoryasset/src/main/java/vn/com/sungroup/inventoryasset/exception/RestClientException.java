package vn.com.sungroup.inventoryasset.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RestClientException extends Exception {

  private final HttpStatus responseStatus;

  public RestClientException(String errorMessage, HttpStatus responseStatus) {
    super(errorMessage);
    this.responseStatus = responseStatus;
  }

  public RestClientException(HttpStatus responseStatus, Throwable cause) {
    super(cause);
    this.responseStatus = responseStatus;
  }

  public RestClientException(String errorMessage, HttpStatus responseStatus, Throwable cause) {
    super(errorMessage, cause);
    this.responseStatus = responseStatus;
  }
}

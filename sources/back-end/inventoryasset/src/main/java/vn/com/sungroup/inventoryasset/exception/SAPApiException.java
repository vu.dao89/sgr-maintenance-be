package vn.com.sungroup.inventoryasset.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import vn.com.sungroup.inventoryasset.sapclient.dto.SAPApiErrorDetail;

@Getter
public class SAPApiException extends RestClientException {
  private final transient SAPApiErrorDetail sapApiErrorDetail;

  public SAPApiException(SAPApiErrorDetail errorDetail, HttpStatus responseStatus, Throwable cause) {
    super(errorDetail.getMessage(), responseStatus, cause);
    this.sapApiErrorDetail = errorDetail;
  }

  public SAPApiException(SAPApiErrorDetail errorDetail, HttpStatus httpStatus) {
    super(errorDetail.getMessage(), httpStatus);
    this.sapApiErrorDetail = errorDetail;
  }

  public SAPApiException(String errorMessage, HttpStatus httpStatus) {
    super(errorMessage, httpStatus);
    this.sapApiErrorDetail = SAPApiErrorDetail.builder()
        .message(errorMessage)
            .code("9999")
        .build();
  }

}

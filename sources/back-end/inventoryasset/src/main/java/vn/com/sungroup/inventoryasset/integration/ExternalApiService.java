/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.integration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.com.sungroup.inventoryasset.dto.inventoryasset.ResponseMoveFileDto;
import vn.com.sungroup.inventoryasset.dto.response.AssetDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetGetCostcenterResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryChgResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetListResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetModifyResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetSearchResponse;
import vn.com.sungroup.inventoryasset.util.Util;

import java.util.Map;

@Service
public class ExternalApiService {
    @Value("${external.rest.consume.SAP_HCM}")
    private String apiSAP_HCM;

    @Value("${external.rest.consume.MoveFile}")
    private String apiMoveFile;

    private static final String PATH_SAP_HCM_ASSETLIST = "/asset-list";
    private static final String PATH_SAP_HCM_ASSETSEARCH = "/asset-search";
    private static final String PATH_SAP_HCM_ASSETDETAIL = "/asset-detail";
    private static final String PATH_SAP_HCM_ASSETMODIFY = "/asset-modify";
    private static final String PATH_MOVEFILE_MOVEFILESTORAGE = "/global/{appCode}/document";
    private static final String PATH_SAP_HCM_ASSETGETCOSTCENTER = "/asset-get-costcenter";
    private static final String PATH_SAP_HCM_ASSETINVENTORY = "/asset-inventory";
    private static final String PATH_SAP_HCM_ASSETHISTORYINVENTORY = "/asset-history-inventory";
    private static final String PATH_SAP_HCM_ASSETHISTORYCHG = "/asset-history-chg";

    public AssetListResponse SAP_HCMassetList(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetListResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETLIST;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetListResponse.class, null);
        return result;
    }

    public AssetSearchResponse SAP_HCMassetSearch(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetSearchResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETSEARCH;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetSearchResponse.class, null);
        return result;
    }

    public AssetDetailResponse SAP_HCMassetDetail(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetDetailResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETDETAIL;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetDetailResponse.class, null);
        return result;
    }

    public AssetModifyResponse SAP_HCMassetModify(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetModifyResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETMODIFY;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetModifyResponse.class, null);
        return result;
    }

    public ResponseMoveFileDto MoveFilemoveFileStorage(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        ResponseMoveFileDto result = null;
        String path = apiMoveFile;
        String url = path + PATH_MOVEFILE_MOVEFILESTORAGE;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, ResponseMoveFileDto.class, null);
        return result;
    }

    public AssetGetCostcenterResponse SAP_HCMassetGetCostcenter(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetGetCostcenterResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETGETCOSTCENTER;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetGetCostcenterResponse.class, null);
        return result;
    }

    public AssetInventoryResponse SAP_HCMassetInventory(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetInventoryResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETINVENTORY;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetInventoryResponse.class, null);
        return result;
    }

    public AssetHistoryInventoryResponse SAP_HCMassetHistoryInventory(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetHistoryInventoryResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETHISTORYINVENTORY;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetHistoryInventoryResponse.class, null);
        return result;
    }

    public AssetHistoryChgResponse SAP_HCMassetHistoryChg(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        AssetHistoryChgResponse result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_ASSETHISTORYCHG;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, AssetHistoryChgResponse.class, null);
        return result;
    }
}

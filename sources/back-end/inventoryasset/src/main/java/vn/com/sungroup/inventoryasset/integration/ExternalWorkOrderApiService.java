package vn.com.sungroup.inventoryasset.integration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.util.Util;

import java.util.Map;

@Service
public class ExternalWorkOrderApiService {
    @Value("${external.rest.consume.SAP_HCM}")
    private String apiSAP_HCM;

    private static final String PATH_SAP_HCM_WORKORDERSEARCH = "/equipment/get/workorder-search";
    private static final String PATH_SAP_HCM_WORKORDERPOST = "/equipment/post/wo-post";

    public BaseResponseSAP SAP_HCM_WorkOrderCreate(
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeader,
            String method,
            long timeOut,
            int retryTimes)
            throws Exception {
        BaseResponseSAP result = null;
        String path = apiSAP_HCM;
        String url = path + PATH_SAP_HCM_WORKORDERPOST;
        String apiResponse =
                Util.callExternalApi(
                        url,
                        requestParam,
                        requestBody,
                        pathVariable,
                        mapHeader,
                        method,
                        timeOut,
                        retryTimes);
        result = Util.JSONDeserialize(apiResponse, BaseResponseSAP.class, null);
        return result;
    }

}

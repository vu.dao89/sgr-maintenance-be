package vn.com.sungroup.inventoryasset.mapper;

import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import vn.com.sungroup.inventoryasset.dto.activitykey.ActivityKeyDto;
import vn.com.sungroup.inventoryasset.mongo.ActivityKeyMg;

@Mapper(componentModel = "spring")
public interface ActivityKeyMgMapper {

  @Named("toMgEntity")
  @Mapping(target = "id", source = "idMg")
  ActivityKeyMg toMgEntity(ActivityKeyDto dto);

  @IterableMapping(qualifiedByName="toMgEntity")
  List<ActivityKeyMg> toListMgEntity(List<ActivityKeyDto> activityKeyDtos);
}

package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.Mapper;
import vn.com.sungroup.inventoryasset.dto.response.employee.DepartmentResponse;
import vn.com.sungroup.inventoryasset.entity.Department;

@Mapper(componentModel = "spring")
public interface DepartmentMapper {

  Department toEntity(DepartmentResponse dto);
}

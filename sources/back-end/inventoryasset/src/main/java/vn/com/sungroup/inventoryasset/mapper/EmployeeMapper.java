package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.Mapper;
import vn.com.sungroup.inventoryasset.dto.response.employee.EmployeeResponse.EmployeeDataResponse;
import vn.com.sungroup.inventoryasset.entity.Employee;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

  Employee toEntity(EmployeeDataResponse dto);
}

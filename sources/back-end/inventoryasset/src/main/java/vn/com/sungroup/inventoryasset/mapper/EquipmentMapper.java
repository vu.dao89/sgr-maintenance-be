package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.equipment.CharacteristicDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasPointDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasuringPointDto;
import vn.com.sungroup.inventoryasset.dto.equipment.PartnerDto;
import vn.com.sungroup.inventoryasset.dto.equipment.sap.MeasPoint;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.PostMeasPointSAPRequest;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;
import vn.com.sungroup.inventoryasset.entity.Partner;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EquipmentMapper {

  EquipmentMapper INSTANCE = Mappers.getMapper(EquipmentMapper.class);

  @IterableMapping(qualifiedByName="mapCharacteristicWithOutId")
  List<Characteristic> toCharacteristicEntityList(List<CharacteristicDto> dtos);

  @Named("mapCharacteristicWithOutId")
  @Mapping(target = "id", source = "_id")
  Characteristic mapCharacteristicWithOutId(CharacteristicDto dtos);

  @IterableMapping(qualifiedByName="mapMeasuringPointWithOutId")
  List<MeasuringPoint> toMeasuringPointEntityList(List<MeasuringPointDto> dtos);

  @Named("mapMeasuringPointWithOutId")
  @Mapping(target = "id", source = "_id")
  MeasuringPoint mapMeasuringPointWithOutId(MeasuringPointDto dtos);

  @IterableMapping(qualifiedByName="mapPartnerWithOutId")
  List<Partner> toPartnerEntityList(List<PartnerDto> dtos);

  @Named("mapPartnerWithOutId")
  @Mapping(target = "id", source = "_id")
  Partner mapPartnerWithOutId(PartnerDto dtos);

  @Named("toMeasPoint")
  MeasPoint toMeasPoint(MeasPointDto measPointDto);
  @Named("toPostMeasPointSAPRequest")
  PostMeasPointSAPRequest toPostMeasPointSAPRequest(PostMeasPointRequest postMeasPointRequest);

  @Named("toMeasPointSAP")
  PostMeasPointSAPRequest.MeasPointRequest toMeasPointSAP(MeasPointDto measPointDto);
}

package vn.com.sungroup.inventoryasset.mapper;

import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.equipment.CharacteristicDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasuringPointDto;
import vn.com.sungroup.inventoryasset.dto.equipment.PartnerDto;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;
import vn.com.sungroup.inventoryasset.entity.Partner;

@Mapper
public interface FunctionalLocationMapper {

  FunctionalLocationMapper INSTANCE = Mappers.getMapper(FunctionalLocationMapper.class);

  @IterableMapping(qualifiedByName="mapCharacteristicWithOutId")
  List<Characteristic> toCharacteristicEntityList(List<CharacteristicDto> dtos);

  @Named("mapCharacteristicWithOutId")
  @Mapping(target = "id", ignore = true)
  Characteristic mapCharacteristicWithOutId(CharacteristicDto dtos);

  @IterableMapping(qualifiedByName="mapMeasuringPointWithOutId")
  List<MeasuringPoint> toMeasuringPointEntityList(List<MeasuringPointDto> dtos);

  @Named("mapMeasuringPointWithOutId")
  @Mapping(target = "id", ignore = true)
  MeasuringPoint mapMeasuringPointWithOutId(MeasuringPointDto dtos);
}

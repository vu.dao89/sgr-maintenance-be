package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.notification.detail.*;
import vn.com.sungroup.inventoryasset.dto.notification.detail.sap.*;
import vn.com.sungroup.inventoryasset.dto.notification.search.NotificationSearchResultDto;
import vn.com.sungroup.inventoryasset.dto.notification.search.sap.NotificationSearch;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationCloseRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCloseSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCreateSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationDetailSAPRequest;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationCloseResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationCloseSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationDetailSAPResponse;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NotificationMapper {
    NotificationMapper INSTANCE = Mappers.getMapper(NotificationMapper.class);

    @Named("mapNotificationSearchResult")
    @Mapping(target = "priorityType", ignore = true)
    @Mapping(target = "commonStatus", ignore = true)
    NotificationSearchResultDto mapNotificationSearchResult(NotificationSearch notificationSearch);
    @IterableMapping(qualifiedByName="mapNotificationSearchResult")
    List<NotificationSearchResultDto> mapNotificationSearchResults(List<NotificationSearch> notificationSearch);
    @IterableMapping(qualifiedByName="toNotificationDetailSAPRequest")
    NotificationDetailSAPRequest toNotificationDetailSAPRequest(NotificationDetailRequest notificationDetailRequest);
    @IterableMapping(qualifiedByName="toNotificationActivitiesDto")
    NotificationActivitiesDto toNotificationActivitiesDto(NotificationActivities notificationActivities);
    @IterableMapping(qualifiedByName="toNotificationActivityDto")
    NotificationActivityDto toNotificationActivityDto(NotificationActivity notificationActivities);
    @Named("toNotificationActivity")
    NotificationActivity toNotificationActivity(NotificationActivityDto notificationActivityDto);
    @IterableMapping(qualifiedByName="toNotificationActivity")
    List<NotificationActivity> toNotificationActivityDto(List<NotificationActivityDto> notificationActivityDto);
    @IterableMapping(qualifiedByName="toNotificationHeaderDto")
    NotificationHeaderDto toNotificationHeaderDto(NotificationHeader notificationHeader);
    @IterableMapping(qualifiedByName="toNotificationItemDto")
    NotificationItemDto toNotificationItemDto(NotificationItem notificationItem);
    @Named("toNotificationItem")
    NotificationItem toNotificationItem(NotificationItemDto notificationItem);
    @IterableMapping(qualifiedByName="toNotificationItem")
    List<NotificationItem> toNotificationItemSAP(List<NotificationItemDto> notificationItems);
    @IterableMapping(qualifiedByName="toNotificationItemsDto")
    NotificationItemsDto toNotificationItemsDto(NotificationItems notificationItems);
    @IterableMapping(qualifiedByName="toNotificationDetailResponse")
    NotificationDetailResponse toNotificationDetailResponse(NotificationDetailSAPResponse notificationDetailSAPResponse);
    @IterableMapping(qualifiedByName="toNotificationCreateSAPRequest")
    NotificationCreateSAPRequest toNotificationCreateSAPRequest(NotificationCreateRequest notificationCreateRequest);
    @IterableMapping(qualifiedByName="toNotificationCloseSAPRequest")
    NotificationCloseSAPRequest toNotificationCloseSAPRequest(NotificationCloseRequest notificationCloseRequest);
    @IterableMapping(qualifiedByName="toNotificationCloseResponse")
    NotificationCloseResponse toNotificationCloseResponse(NotificationCloseSAPResponse notificationCloseSAPResponse);
}

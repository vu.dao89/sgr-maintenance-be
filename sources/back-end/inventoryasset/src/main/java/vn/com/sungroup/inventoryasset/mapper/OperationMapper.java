package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.request.operation.MaterialDocumentHeader;
import vn.com.sungroup.inventoryasset.dto.request.operation.MaterialDocumentItem;
import vn.com.sungroup.inventoryasset.dto.request.operation.MaterialDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.OperationDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentHeaderSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentItemSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationDetailRequestSAP;
import vn.com.sungroup.inventoryasset.dto.sap.OperationChangeRequest;
import vn.com.sungroup.inventoryasset.dto.sap.OperationChangeRequestSAP;
import vn.com.sungroup.inventoryasset.dto.sap.OperationCreateRequest;

@Mapper(componentModel = "spring")
public interface OperationMapper {
  OperationMapper INSTANCE = Mappers.getMapper(OperationMapper.class);
  OperationDetailRequestSAP toOperationSAP(OperationDetailRequest input);
  OperationChangeRequestSAP toOperationChangeRequestSAP(OperationChangeRequest input);
  OperationChangeRequestSAP toOperationChangeRequestSAP(OperationCreateRequest input);
  MaterialDocumentRequestSAP toMaterialDocumentRequestSAP(MaterialDocumentRequest input);
  MaterialDocumentHeaderSAP toMaterialDocumentHeaderSAP(MaterialDocumentHeader input);
  MaterialDocumentItemSAP toMaterialDocumentItemSAP(MaterialDocumentItem input);
  OperationChangeRequest toOperationChangeRequest(OperationCreateRequest input);
  OperationCreateRequest toOperationCreateRequest(OperationChangeRequest input);
}

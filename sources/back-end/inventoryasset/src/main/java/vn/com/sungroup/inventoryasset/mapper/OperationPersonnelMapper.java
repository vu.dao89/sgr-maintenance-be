package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationDetail;
import vn.com.sungroup.inventoryasset.dto.response.operation.SubOperationDetailItem;
import vn.com.sungroup.inventoryasset.dto.sap.OperationChangeRequestSAP;

@Mapper
public interface OperationPersonnelMapper {
  OperationPersonnelMapper INSTANCE = Mappers.getMapper(OperationPersonnelMapper.class);

  OperationChangeRequestSAP operationDetailToOperationChangeRequest(OperationDetail operationDetail);

  OperationChangeRequestSAP subOperationDetailItemToOperationChangeRequest(
      SubOperationDetailItem operationDetail);
}

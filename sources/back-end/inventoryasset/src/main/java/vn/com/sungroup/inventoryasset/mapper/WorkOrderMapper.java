package vn.com.sungroup.inventoryasset.mapper;

import org.mapstruct.Mapper;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.confirmation.OperationConfirmationRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post.WorkOrderCreateRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post.WorkOrderOperationItemSAP;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangeRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderOperationItem;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WorkOrderMapper {

  WorkOrderCreateRequestSAP toSapCreateRequest(WorkOrderCreateRequest request);
  WorkOrderOperationItemSAP toWorkOrderOperationItemSAP(WorkOrderOperationItem input);
  List<WorkOrderOperationItemSAP> toWorkOrderOperationItemSAP(List<WorkOrderOperationItem> input);
  WorkOrderChangeRequestSAP toSapChangeRequest(WorkOrderChangeRequest request);

  OperationConfirmationRequestSAP toSapConfirmationRequest(OperationConfirmationRequest request);
}

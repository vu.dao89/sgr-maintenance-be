package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.ActivityKeyMg;

public interface ActivityKeyMgRepository extends MongoRepository<ActivityKeyMg, String> {

}

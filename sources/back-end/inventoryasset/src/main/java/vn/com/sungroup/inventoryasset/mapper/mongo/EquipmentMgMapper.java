package vn.com.sungroup.inventoryasset.mapper.mongo;

import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.equipment.CharacteristicDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasuringPointDto;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;

@Mapper(componentModel = "spring")
public interface EquipmentMgMapper {

  EquipmentMgMapper INSTANCE = Mappers.getMapper(EquipmentMgMapper.class);

  @IterableMapping(qualifiedByName="mapCharacteristicWithOutId")
  List<CharacteristicMg> toCharacteristicEntityList(List<CharacteristicDto> dtos);

  @Named("mapCharacteristicWithOutId")
  @Mapping(target = "id", source = "idMg")
  CharacteristicMg mapCharacteristicWithOutId(CharacteristicDto dto);

  @IterableMapping(qualifiedByName="mapMeasuringPointWithOutId")
  List<MeasuringPointMg> toMeasuringPointEntityList(List<MeasuringPointDto> dtos);

  @Named("mapMeasuringPointWithOutId")
  @Mapping(target = "id", source = "idMg")
  MeasuringPointMg mapMeasuringPointWithOutId(MeasuringPointDto dto);
}

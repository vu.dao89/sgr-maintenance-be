package vn.com.sungroup.inventoryasset.mapper.mongo;

import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import vn.com.sungroup.inventoryasset.dto.groupcode.GroupCodeDto;
import vn.com.sungroup.inventoryasset.mongo.GroupCodeMg;

@Mapper(componentModel = "spring")
public interface GroupCodeMgMapper {

  @Mapping(target = "id", source = "idMg")
  @Mapping(target = "catalogProfileText", source = "catalogProfileDes")
  @Mapping(target = "catalogDesc", source = "catalogDes")
  @Mapping(target = "codeGroupDesc", source = "codeGroupDes")
  @Mapping(target = "codeDesc", source = "codeDes")
  @Named("toMgEntity")
  GroupCodeMg toMgEntity(GroupCodeDto dto);

  @IterableMapping(qualifiedByName = "toMgEntity")
  List<GroupCodeMg> toListMgEntity(List<GroupCodeDto> groupCodes);
}

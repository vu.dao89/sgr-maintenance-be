package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.plant.MaintenancePlantDto;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;

@Mapper(componentModel = "spring")
public interface MaintenancePlantMapper {

  MaintenancePlantMapper INSTANCE = Mappers.getMapper(MaintenancePlantMapper.class);

  MaintenancePlantMg maintenancePlantDtoToDocument(MaintenancePlantDto dto);
}

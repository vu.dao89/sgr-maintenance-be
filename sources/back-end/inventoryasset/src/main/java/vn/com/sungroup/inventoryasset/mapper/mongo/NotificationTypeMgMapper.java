package vn.com.sungroup.inventoryasset.mapper.mongo;

import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationTypeDto;
import vn.com.sungroup.inventoryasset.mongo.NotificationTypeMg;

@Mapper(componentModel = "spring")
public interface NotificationTypeMgMapper {

  @Mapping(target = "id", source = "idMg")
  @Named("toMgEntity")
  NotificationTypeMg toMgEntity(NotificationTypeDto notificationType);

  @IterableMapping(qualifiedByName="toMgEntity")
  List<NotificationTypeMg> toListMgEntity(List<NotificationTypeDto> notificationTypes);
}

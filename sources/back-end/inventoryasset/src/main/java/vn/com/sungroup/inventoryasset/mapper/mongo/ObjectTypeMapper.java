package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.objecttype.ObjectTypeDto;
import vn.com.sungroup.inventoryasset.dto.plant.MaintenancePlantDto;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;
import vn.com.sungroup.inventoryasset.mongo.ObjectTypeMg;

@Mapper(componentModel = "spring")
public interface ObjectTypeMapper {

  ObjectTypeMapper INSTANCE = Mappers.getMapper(ObjectTypeMapper.class);

  @Mapping(source = "objectTypeId", target = "code")
  @Mapping(source = "objectDescription", target = "description")
  ObjectTypeMg maintenancePlantDtoToDocument(ObjectTypeDto dto);
}

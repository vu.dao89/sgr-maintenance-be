package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.personal.PersonalDto;
import vn.com.sungroup.inventoryasset.mongo.PersonnelMg;

@Mapper(componentModel = "spring")
public interface PersonnelMapper {
  PersonnelMapper INSTANCE = Mappers.getMapper(PersonnelMapper.class);

  @Mapping(source = "idPer", target = "id")
  @Mapping(source = "personelId", target = "code")
  @Mapping(source = "personelName", target = "name")
  @Mapping(source = "workCenterId", target = "workCenterId")
  @Mapping(source = "plantId", target = "maintenancePlantId")
  @Mapping(source = "perStartDate", target = "startDate")
  @Mapping(source = "perEndDate", target = "endDate")
  PersonnelMg personnelDtoToToDocument(PersonalDto personalDto);
}

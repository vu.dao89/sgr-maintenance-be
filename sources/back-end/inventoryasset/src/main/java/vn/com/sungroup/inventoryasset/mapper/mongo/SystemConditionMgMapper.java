package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.systemcondition.SystemConditionDto;
import vn.com.sungroup.inventoryasset.mongo.SystemConditionMg;

@Mapper(componentModel = "spring")
public interface SystemConditionMgMapper {
  SystemConditionMgMapper INSTANCE = Mappers.getMapper(SystemConditionMgMapper.class);

  @Mapping(source = "sysCond", target = "code")
  @Mapping(source = "sysCondText", target = "text")
  @Mapping(source = "idSys", target = "id")
  SystemConditionMg systemConditionMgDtoToDocument(SystemConditionDto dto);
}

package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.uom.UnitOfMeasureDto;
import vn.com.sungroup.inventoryasset.mongo.UnitOfMeasurementMg;

@Mapper(componentModel = "spring")
public interface UnitOfMeasureMapper {
  UnitOfMeasureMapper INSTANCE = Mappers.getMapper(UnitOfMeasureMapper.class);

  @Mapping(source = "uom", target = "code")
  @Mapping(source = "uomDes", target = "description")
  @Mapping(source = "idUnit", target = "id")
  UnitOfMeasurementMg maintenancePlantDtoToDocument(UnitOfMeasureDto dto);
}

package vn.com.sungroup.inventoryasset.mapper.mongo;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import vn.com.sungroup.inventoryasset.dto.plant.MaintenancePlantDto;
import vn.com.sungroup.inventoryasset.dto.workcenter.WorkCenterDto;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;
import vn.com.sungroup.inventoryasset.mongo.WorkCenterMg;

@Mapper(componentModel = "spring")
public interface WorkCenterMapper {

  WorkCenterMapper INSTANCE = Mappers.getMapper(WorkCenterMapper.class);

  WorkCenterMg workCenterDtoToDocument(WorkCenterDto dto);
}

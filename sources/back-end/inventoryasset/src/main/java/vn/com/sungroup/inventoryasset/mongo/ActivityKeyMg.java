package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "activity_key")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@CompoundIndex(name = "work_order_type_type_duplicate_key", unique = true, def = "{'workOrderType':1, 'type':1}")
public class ActivityKeyMg {

  @Id
  private String id;

  private String workOrderType;

  private String type;

  private String description;
}

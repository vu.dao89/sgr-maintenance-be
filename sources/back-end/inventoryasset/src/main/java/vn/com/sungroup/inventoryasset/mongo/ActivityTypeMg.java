package vn.com.sungroup.inventoryasset.mongo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document(collection = "activity_type")
public class ActivityTypeMg {

    @Id
    private String id;

    private String type;

    private String description;

    @Field(name = "cost_center")
    private String costCenter;

    @Field(name = "cost_center_short_text")
    private String costCenterShortText;

}

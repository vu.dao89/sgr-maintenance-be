package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "characteristic")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class CharacteristicMg {

    @Id
    private String id;

    private String charValue;

    private String valueRel;

    private String objectClassFlag;

    private String internalCounter;

    private String classType;

    private String charValCounter;

    private String charId;

    private String charValDesc;

    private String classId;
}

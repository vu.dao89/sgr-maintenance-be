package vn.com.sungroup.inventoryasset.mongo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.LocalTime;
import java.util.ArrayList;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document(collection = "equipment")
public class EquipmentMg {

    @Id
    private String id;

    @Indexed(unique = true)
    private String equipmentId;

    private String parentEquipmentId;

    // ObjectType
    @DBRef
    private ObjectTypeMg equipmentType;

    private String constYear;
    private String constMonth;

    @DBRef
    private MaintenancePlantMg maintenancePlant;

    // FunctionalLocation
    @DBRef
    private FunctionalLocationMg functionalLocation;

    // EquipmentCategory
    @DBRef
    private EquipmentCategoryMg equipmentCategory;

    private String description;
    private String longDesc;
    private String location;

    // WorkCenter
    @DBRef
    private WorkCenterMg maintenanceWorkCenter;

    private String costCenter;
    private String costCenterName;
    private String manuCountry;
    private String manuPartNo;
    private String manuSerialNo;
    private String manufacturer;
    private String modelNumber;
    private String plannerGroup;
    private String planningPlantId;
    private String plantSection;
    private String size;
    private String weight;

    @JsonFormat(pattern = "YYYYMMDD")
    private LocalDate createDate;

    @JsonFormat(pattern = "hhmmss")
    private LocalTime createTime;

    private LocalDate cusWarrantyEnd;
    private LocalDate cusWarrantyDate;
    private LocalDate venWarrantyEnd;
    private LocalDate venWarrantyDate;
    private String qrCode;
    private String startupDate;
    private String endOfUseDate;
    private String sortField;
    private String thumbnailUrl;
    private String originalUrl;
    private String latitude;
    private String longitude;

    @DBRef
    private SystemStatusMg systemStatus;

    @DBRef
    private List<PartnerMg> partners = new ArrayList<>();

    @DBRef
    private List<MeasuringPointMg> measuringPoints = new ArrayList<>();

    @DBRef
    private List<CharacteristicMg> characteristics = new ArrayList<>();
}

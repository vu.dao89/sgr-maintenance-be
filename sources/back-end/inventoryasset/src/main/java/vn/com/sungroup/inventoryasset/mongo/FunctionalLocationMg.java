package vn.com.sungroup.inventoryasset.mongo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Document(collection = "functional_location")
@Builder
@JsonInclude(Include.NON_NULL)
public class FunctionalLocationMg {

  @Id
  private String id;

  private String functionalLocationId;

  private String parentFunctionalLocationId;

  private String constYear;

  private String constMonth;

  @DBRef
  private MaintenancePlantMg maintenancePlant;

  @DBRef
  private ObjectTypeMg functionalLocationType;

  // TODO FIELD IN XML DOES NOT EXISTS
  private String functionalLocationCategory;

  private String description;

  private String longDesc;

  @DBRef
  private ObjectTypeMg objectType;

  // TODO FIELD IN XML DOES NOT EXISTS
  private String location;

  private String maintenanceWorkCenterId;

  private String costCenter;

  private String manuCountry;

  private String manuPartNo;

  private String manuSerialNo;

  private String manufacturer;

  private String modelNumber;

  private String plannerGroup;

  private String planningPlantId;

  private String plantSection;

  private String size;

  private String weight;

  @JsonFormat(pattern = "YYYYMMDD")
  private LocalDate createDate;

  @JsonFormat(pattern = "YYYYMMDD")
  private LocalDate cusWarrantyEnd;

  @JsonFormat(pattern = "YYYYMMDD")
  private LocalDate cusWarrantyDate;

  @JsonFormat(pattern = "YYYYMMDD")
  private LocalDate venWarrantyEnd;

  @JsonFormat(pattern = "YYYYMMDD")
  private LocalDate venWarrantyDate;

  private String qrCode;

  private String startupDate;

  private String endOfUseDate;


  @DBRef
  private List<MeasuringPointMg> measuringPoints = new ArrayList<>();

  @DBRef
  private List<CharacteristicMg> characteristics = new ArrayList<>();

}

package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "group_code")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@CompoundIndex(name = "catalog_profile_catalog_code_group_code_duplicate_key", unique = true, def = "{'catalogProfile':1, 'catalog':1, 'codeGroup':1, 'code':1}")
public class GroupCodeMg {

  @Id
  private String id;

  private String catalogProfile;

  private String catalogProfileText;

  private String catalog;

  private String catalogDesc;

  private String codeGroup;

  private String codeGroupDesc;

  private String code;

  private String codeDesc;
}

package vn.com.sungroup.inventoryasset.mongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "maintenance_plant")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class MaintenancePlantMg {

    @Id
    private String id;

    @Indexed
    private String code;

    @Indexed
    private String planningPlant;

    private String plantName;
    private String businessPlace;
}

package vn.com.sungroup.inventoryasset.mongo;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
import vn.com.sungroup.inventoryasset.entity.Equipment;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document(collection = "measuring_point")
public class MeasuringPointMg {

  @Id
  private String id;

  private String point;

  private String position;

  private String description;

  private String characteristic;

  private String targetValue;

  private String counter;

  private String lowerRangeLimit;

  private String upperRangeLimit;

  private String rangeUnit;

  private String text;

  private String codeGroup;

  @DocumentReference(lookup="{'measuringPoints':?#{#self._id} }")
  private List<Equipment> equipments;
}

package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("notification_type")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTypeMg {

  @Id
  private String id;

  @Indexed
  private String notiType;

  private String description;

  private String catalogProfile;

  private String problems;

  private String causes;

  private String activities;

  private String objectParts;

  private String workOrderTypeId;

  private String priorityTypeId;

  private String personResponsible;

}

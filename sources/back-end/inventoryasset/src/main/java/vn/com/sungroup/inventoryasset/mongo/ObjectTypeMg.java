package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "object_type")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ObjectTypeMg {

    @Id
    private String id;

    @Indexed
    private String code;
    private String description;
}

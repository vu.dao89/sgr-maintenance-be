package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "partner")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class PartnerMg {

    @Id
    private String id;

    private String counter;

    private String partnerFunction;

    private String partnerNumber;

    private String objectTypeCode;
}

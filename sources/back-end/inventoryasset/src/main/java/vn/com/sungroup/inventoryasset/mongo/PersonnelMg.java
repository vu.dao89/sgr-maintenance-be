package vn.com.sungroup.inventoryasset.mongo;

import java.time.LocalDate;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.com.sungroup.inventoryasset.entity.Employee;

@Document(collection = "personnel")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class PersonnelMg {
  @Id
  private String id;

  private String code;

  private String name;

  private LocalDate startDate;

  private LocalDate endDate;

  private String workCenterId;

  private String maintenancePlantId;

  @DBRef
  private Employee employee;
}

package vn.com.sungroup.inventoryasset.mongo;

import java.io.Serializable;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "planner_group")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class PlannerGroupMg implements Serializable {
    @Id
    private String id;

    @Indexed
    private String maintenancePlantId;

    @DBRef
    private MaintenancePlantMg maintenancePlant;

    @Indexed
    private String plannerGroupId;
    private String name;
}

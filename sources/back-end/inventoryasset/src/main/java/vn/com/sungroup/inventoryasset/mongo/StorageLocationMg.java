package vn.com.sungroup.inventoryasset.mongo;

import java.util.UUID;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "storage_location")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class StorageLocationMg {
    @Id
    private String id;
    @Indexed
    private String code;
    private String description;

    @Indexed
    private String maintenancePlantId;
}

package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "system_condition")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class SystemConditionMg {

  @Id
  private String id;

  private String code;

  private String text;
}

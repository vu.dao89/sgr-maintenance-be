package vn.com.sungroup.inventoryasset.mongo;

import java.io.Serializable;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "system_status")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class SystemStatusMg implements Serializable {

  @Id
  private String id;

  @Indexed
  public String statusId;

  public String code;

  public String description;

  public String userStatusProfile;

  public String userStatus;
}

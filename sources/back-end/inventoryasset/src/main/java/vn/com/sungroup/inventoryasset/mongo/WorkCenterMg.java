package vn.com.sungroup.inventoryasset.mongo;

import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "work_center")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class WorkCenterMg {

    @Id
    private String id;

    @Indexed
    private String code;

    @Indexed
    private String maintenancePlantId;

    @Indexed
    private String costCenterCode;

    private String category;
    private String description;

    @Transient
    private String maintenancePlantName;
}

package vn.com.sungroup.inventoryasset.mongo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document(collection = "work_order_type")
public class WorkOrderTypeMg {

    @Id
    private String idMg;

    private UUID id;

    private String name;

    private String type;

    @Field(name = "maintenance_plant_id")
    private String maintenancePlantId;

    @Field(name = "priority_type")
    private String priorityType;

}

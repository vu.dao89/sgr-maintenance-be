package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.ActivityKey;

import java.util.UUID;

@Repository
public interface ActivityKeyRepository extends JpaRepository<ActivityKey, UUID>, ActivityKeyRepositoryCustomized {

  ActivityKey findByWorkOrderType(String actType);

  @Query("SELECT s FROM ActivityKey s WHERE s.workOrderType = :conditions OR s.type = :conditions")
  Page<ActivityKey> getActivityKeyByWorkOrderTypeOrType(String conditions, Pageable pageable);
}

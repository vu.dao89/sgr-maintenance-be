package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.ActivityKeyRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityKey;

public interface ActivityKeyRepositoryCustomized {
    Page<ActivityKey> findAllByConditions(Pageable pageable, ActivityKeyRequest input);
}

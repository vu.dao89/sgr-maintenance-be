package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.ActivityKeyRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityKey;
import vn.com.sungroup.inventoryasset.entity.QActivityKey;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ActivityKeyRepositoryCustomizedImpl implements ActivityKeyRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<ActivityKey> findAllByConditions(Pageable pageable, ActivityKeyRequest input) {
        var entity = QActivityKey.activityKey;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.type.containsIgnoreCase(input.getFilterText())
                    .or(entity.description.containsIgnoreCase(input.getFilterText())));
        }

        if(!input.getTypes().isEmpty()) {
            jpaQuery.where(entity.type.in(input.getTypes()));
        }

        if(!input.getWorkOrderTypes().isEmpty()) {
            jpaQuery.where(entity.workOrderType.in(input.getWorkOrderTypes()));
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.type.asc()).orderBy(entity.workOrderType.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();
        // TODO implement sort

        return new PageImpl<>(data, pageable, totalData);
    }
}

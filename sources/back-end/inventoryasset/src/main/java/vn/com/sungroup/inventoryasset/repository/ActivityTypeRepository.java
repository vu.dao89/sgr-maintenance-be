package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.ActivityType;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ActivityTypeRepository extends JpaRepository<ActivityType, UUID>, ActivityTypeRepositoryCustomized {

  Optional<ActivityType> findByType(String type);
  Optional<ActivityType> findByTypeAndCostCenter(String type, String costCenter);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.ActivityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityType;

public interface ActivityTypeRepositoryCustomized {
    Page<ActivityType> findAllByConditions(Pageable pageable, ActivityTypeRequest input);
}

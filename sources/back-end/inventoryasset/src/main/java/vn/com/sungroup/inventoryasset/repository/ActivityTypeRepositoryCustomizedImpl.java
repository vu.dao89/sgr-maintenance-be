package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.ActivityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityType;
import vn.com.sungroup.inventoryasset.entity.QActivityType;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ActivityTypeRepositoryCustomizedImpl implements ActivityTypeRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<ActivityType> findAllByConditions(Pageable pageable, ActivityTypeRequest input) {
        var entity = QActivityType.activityType;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.type.containsIgnoreCase(input.getFilterText())
                            .or(entity.costCenter.containsIgnoreCase(input.getFilterText()))
                            .or(entity.description.containsIgnoreCase(input.getFilterText())));
        }
        else
        {
            if(!input.getTypes().isEmpty()) {
                jpaQuery.where(entity.type.in(input.getTypes()));
            }

            if(!input.getCostCenterCodes().isEmpty()){
                jpaQuery.where(entity.costCenter.in(input.getCostCenterCodes()));
            }
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.type.asc()).orderBy(entity.costCenter.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

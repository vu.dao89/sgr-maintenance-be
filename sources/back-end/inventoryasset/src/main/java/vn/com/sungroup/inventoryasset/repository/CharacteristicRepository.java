package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.Characteristic;

import java.util.UUID;

/**
 * Spring Data JPA repository for the Characteristic entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CharacteristicRepository extends JpaRepository<Characteristic, UUID>, CharacteristicRepositoryCustomized {
  @Query("SELECT S FROM Characteristic S WHERE S.charId = :condition OR S.classId = :condition")
  Page<Characteristic> findByCharIdOrClassId(String condition, Pageable pageable);
}

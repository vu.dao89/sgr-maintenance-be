package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.CharacteristicsRequest;
import vn.com.sungroup.inventoryasset.entity.Characteristic;

public interface CharacteristicRepositoryCustomized {
    Page<Characteristic> findAllByConditions(Pageable pageable, CharacteristicsRequest input);
}

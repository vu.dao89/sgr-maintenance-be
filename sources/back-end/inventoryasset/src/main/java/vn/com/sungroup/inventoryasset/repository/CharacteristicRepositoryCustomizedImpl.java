package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.CharacteristicsRequest;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.entity.QCharacteristic;

@Repository
@RequiredArgsConstructor
@Slf4j
public class CharacteristicRepositoryCustomizedImpl implements CharacteristicRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<Characteristic> findAllByConditions(Pageable pageable, CharacteristicsRequest input) {
        var entity = QCharacteristic.characteristic;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.charId.containsIgnoreCase(input.getFilterText())
                    .or(entity.classId.containsIgnoreCase(input.getFilterText())));
        }
        else
        {
            if(!input.getCharIds().isEmpty()) {
                jpaQuery.where(entity.charId.in(input.getCharIds()));
            }

            if(!input.getClassIds().isEmpty()) {
                jpaQuery.where(entity.classId.in(input.getClassIds()));
            }
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.charId.asc()).orderBy(entity.classId.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

package vn.com.sungroup.inventoryasset.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.CommonStatusRequest;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;

public interface CommonStatusRepositoryCustomized {

  Page<CommonStatus> getCommonStatusByFilter(CommonStatusRequest request, Pageable pageable);
}

package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.CommonStatusRequest;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.QCommonStatus;

@Repository
@RequiredArgsConstructor
public class CommonStatusRepositoryCustomizedImpl implements CommonStatusRepositoryCustomized {

  private final JPAQueryFactory queryFactory;

  @Override
  public Page<CommonStatus> getCommonStatusByFilter(CommonStatusRequest request, Pageable pageable) {
    QCommonStatus commonStatusEntity = QCommonStatus.commonStatus;
    var jpaQuery = queryFactory.selectFrom(commonStatusEntity);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where((commonStatusEntity.status.containsIgnoreCase(request.getFilterText())
              .or(commonStatusEntity.description.containsIgnoreCase(request.getFilterText()))));
    }
    else {
      if (StringUtils.hasText(request.getStatus())) {
        jpaQuery.where(commonStatusEntity.status.containsIgnoreCase(request.getStatus()));
      }

      if (StringUtils.hasText(request.getDescription())) {
        jpaQuery.where(commonStatusEntity.description.containsIgnoreCase(request.getDescription()));
      }
    }

    if (!request.getCodes().isEmpty()) {
      jpaQuery.where(commonStatusEntity.code.in(request.getCodes()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(commonStatusEntity.status.asc()).orderBy(commonStatusEntity.code.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

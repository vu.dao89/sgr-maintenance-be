package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.Employee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EmployeeRepository extends JpaRepository<Employee, UUID> {
    Optional<Employee> findByEmployeeId(String employeeId);

    List<Employee> findAllByEmployeeIdIsIn(List<String> employeeIds);

    Optional<Employee> findByPrimaryId(String primaryId);
}

package vn.com.sungroup.inventoryasset.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;

public interface EquipmentCategoryRepository extends JpaRepository<EquipmentCategory, UUID>, EquipmentCategoryRepositoryCustomized {

  Optional<EquipmentCategory> findByCode(String code);

  Optional<EquipmentCategory> findById(UUID id);

}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentCategoryRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;

public interface EquipmentCategoryRepositoryCustomized {

  Page<EquipmentCategory> findEquipmentCategoryByFilter(
      EquipmentCategoryRequest request, Pageable pageable);

}

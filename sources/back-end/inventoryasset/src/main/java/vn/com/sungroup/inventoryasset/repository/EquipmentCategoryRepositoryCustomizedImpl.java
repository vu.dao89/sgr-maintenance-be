package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentCategoryRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;
import vn.com.sungroup.inventoryasset.entity.QEquipmentCategory;

@Repository
@RequiredArgsConstructor
public class EquipmentCategoryRepositoryCustomizedImpl implements EquipmentCategoryRepositoryCustomized {

  private final JPAQueryFactory jpaQueryFactory;

  @Override
  public Page<EquipmentCategory> findEquipmentCategoryByFilter(EquipmentCategoryRequest request,
      Pageable pageable) {
    QEquipmentCategory equipmentCategory = QEquipmentCategory.equipmentCategory;

    var jpaQuery = jpaQueryFactory.selectFrom(equipmentCategory);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(equipmentCategory.code.containsIgnoreCase(request.getFilterText())
          .or(equipmentCategory.description.containsIgnoreCase(request.getFilterText())));
    }
    else {
      if (!request.getCodes().isEmpty()) {
        jpaQuery.where(equipmentCategory.code.in(request.getCodes()));
      }

      if (StringUtils.hasText(request.getDescription())) {
        jpaQuery.where(equipmentCategory.description.containsIgnoreCase(request.getDescription()));
      }
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(equipmentCategory.code.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

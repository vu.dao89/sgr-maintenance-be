package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.EquipmentDocument;

import java.util.List;
import java.util.UUID;

public interface EquipmentDocumentRepository extends JpaRepository<EquipmentDocument, UUID> {
    Page<EquipmentDocument> findAllByEquipmentCode(String equipmentCode, Pageable pageable);
    List<EquipmentDocument> findAllByEquipmentCode(String equipmentCode);
}



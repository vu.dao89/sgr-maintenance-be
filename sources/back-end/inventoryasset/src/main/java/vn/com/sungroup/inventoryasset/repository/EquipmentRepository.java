package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentHierarchyDto;
import vn.com.sungroup.inventoryasset.entity.Equipment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EquipmentRepository extends JpaRepository<Equipment, UUID>, EquipmentRepositoryCustomized {

  @Query("SELECT e FROM Equipment e WHERE e.qrCode = :qrCode ORDER BY e.createDate DESC")
  List<Equipment> findByQrCode(String qrCode);

  @Query(value = "WITH RECURSIVE root_parent AS ("
      + "SELECT equipment_id, description, parent_equipment_id "
      + "FROM equipment WHERE equipment.equipment_id = :equipmentId "
      + "UNION ALL "
      + "SELECT fl.equipment_id, fl.description, fl.parent_equipment_id "
      + "FROM equipment fl, root_parent "
      + "WHERE fl.equipment_id = root_parent.parent_equipment_id"
      + ") "
      + "SELECT equipment_id "
      + "FROM root_parent WHERE parent_equipment_id IS NULL OR TRIM(parent_equipment_id) = ''",
      nativeQuery = true)
  String findRootParentEquipmentId(String equipmentId);

  @Query(value = "WITH RECURSIVE equipment_hierarchy AS ("
      + "SELECT equipment_id, description, "
      + "parent_equipment_id, 0 as level "
      + "FROM equipment WHERE equipment.equipment_id = :rootParentEquipmentId "
      + "UNION ALL "
      + "SELECT fl.equipment_id, fl.description, fl.parent_equipment_id, "
      + "equipment_hierarchy.level + 1 "
      + "FROM equipment fl, equipment_hierarchy "
      + "WHERE fl.parent_equipment_id = equipment_hierarchy.equipment_id"
      + ") "
      + "SELECT equipment_id AS equipmentId, description, "
      + "parent_equipment_id AS parentEquipmentId, "
      + "level FROM equipment_hierarchy ORDER BY level ASC",
      nativeQuery = true)
  List<EquipmentHierarchyDto> findEquipmentHierarchy(String rootParentEquipmentId);

  Optional<Equipment> findByEquipmentId(String equipmentId);
}

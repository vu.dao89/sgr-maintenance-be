package vn.com.sungroup.inventoryasset.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentRequest;
import vn.com.sungroup.inventoryasset.dto.response.equipment.EquipmentResponse;

public interface EquipmentRepositoryCustomized {

  Page<EquipmentResponse> findAllByConditions(Pageable pageable, EquipmentRequest input);
}

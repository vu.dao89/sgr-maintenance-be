package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentRequest;
import vn.com.sungroup.inventoryasset.dto.response.equipment.EquipmentResponse;
import vn.com.sungroup.inventoryasset.entity.QEquipment;
import vn.com.sungroup.inventoryasset.entity.QEquipmentCategory;
import vn.com.sungroup.inventoryasset.entity.QFunctionalLocation;
import vn.com.sungroup.inventoryasset.entity.QObjectType;
import vn.com.sungroup.inventoryasset.entity.QSystemStatus;

@Repository
@RequiredArgsConstructor
public class EquipmentRepositoryCustomizedImpl implements EquipmentRepositoryCustomized {

  private final JPAQueryFactory queryFactory;

  @Override
  public Page<EquipmentResponse> findAllByConditions(Pageable pageable, EquipmentRequest input) {
    var equipment = QEquipment.equipment;
    var functionalLocation = QFunctionalLocation.functionalLocation;
    var equipmentType = QObjectType.objectType;
    var equipmentCategory = QEquipmentCategory.equipmentCategory;
    var systemStatus = QSystemStatus.systemStatus;
    var jpaQuery = queryFactory.selectFrom(equipment);

    if(StringUtils.hasText(input.getFilterText())) {
      jpaQuery.where(equipment.description.containsIgnoreCase(input.getFilterText())
              .or(equipment.equipmentId.containsIgnoreCase(input.getFilterText()))
              .or(equipment.sortField.containsIgnoreCase(input.getFilterText()))
              .or(equipment.qrCode.containsIgnoreCase(input.getFilterText())));
    }
    else
    {
      if (StringUtils.hasText(input.getDescription())) {
        jpaQuery.where(equipment.description.containsIgnoreCase(input.getDescription()));
      }
    }

    if(!input.getFunctionalLocationCodes().isEmpty()){
      jpaQuery.where(equipment.functionalLocationId.in(input.getFunctionalLocationCodes()));
    }

    if(!input.getWorkCenterCodes().isEmpty()){
      jpaQuery.where(equipment.maintenanceWorkCenterId.in(input.getWorkCenterCodes()));
    }

    if(!input.getEquipmentCategoryCodes().isEmpty()){
      jpaQuery.where(equipment.equipmentCategoryId.in(input.getEquipmentCategoryCodes()));
    }

    if(!input.getObjectTypeCodes().isEmpty()){
      jpaQuery.where(equipment.equipmentTypeId.in(input.getObjectTypeCodes()));
    }

    if(!input.getPlannerGroups().isEmpty()){
      jpaQuery.where(equipment.plannerGroup.in(input.getPlannerGroups()));
    }

    if(!input.getMaintenancePlantCodes().isEmpty()){
      jpaQuery.where(equipment.maintenancePlantId.in(input.getMaintenancePlantCodes()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.select(Projections.constructor(EquipmentResponse.class, equipment.equipmentId,
                    equipment.description,
                    functionalLocation.functionalLocationId,
                    functionalLocation,
                    equipmentType.code,
                    equipmentType,
                    equipmentCategory.code,
                    equipmentCategory,
                    systemStatus.code,
                    systemStatus
                )).leftJoin(QFunctionalLocation.functionalLocation).on(equipment.functionalLocationId.eq(functionalLocation.functionalLocationId))
                .leftJoin(QEquipmentCategory.equipmentCategory).on(equipment.equipmentCategoryId.eq(equipmentCategory.code))
                .leftJoin(QObjectType.objectType).on(equipment.equipmentTypeId.eq(equipmentType.code))
                .leftJoin(QSystemStatus.systemStatus).on(equipment.systemStatusId.eq(systemStatus.code))
                .orderBy(equipment.equipmentId.asc()).orderBy(equipment.maintenancePlantId.asc())
                    .offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

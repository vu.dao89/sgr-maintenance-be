package vn.com.sungroup.inventoryasset.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.com.sungroup.inventoryasset.dto.functionallocation.FunctionalLocationHierarchyDto;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;

public interface FuncLocationRepository extends JpaRepository<FunctionalLocation, UUID>, FunctionalLocationRepositoryCustomized {

  Optional<FunctionalLocation> findByFunctionalLocationId(String functionalLocationId);

  @Query("SELECT fl FROM FunctionalLocation fl WHERE fl.qrCode = :qrCode ORDER BY fl.createDate DESC")
  List<FunctionalLocation> findByQrCode(String qrCode);

  @Query("select f from FunctionalLocation f where" +
          "(COALESCE(:maintenancePlantCodes,NULL) IS NULL or f.maintenancePlantId in (:maintenancePlantCodes))")
  Page<FunctionalLocation> findAllByConditions(Pageable pageable, List<String> maintenancePlantCodes);

  @Query(value = "WITH RECURSIVE root_parent AS ("
      + "SELECT functional_location_id, description, parent_functional_location_id "
      + "FROM functional_location WHERE functional_location.functional_location_id = :functionalLocationId "
      + "UNION ALL "
      + "SELECT fl.functional_location_id, fl.description, fl.parent_functional_location_id "
      + "FROM functional_location fl, root_parent "
      + "WHERE fl.functional_location_id = root_parent.parent_functional_location_id"
      + ") "
      + "SELECT functional_location_id "
      + "FROM root_parent WHERE parent_functional_location_id IS NULL",
      nativeQuery = true)
  String findRootParentFunctionalLocationId(String functionalLocationId);

  @Query(value = "WITH RECURSIVE functional_location_hierarchy AS ("
      + "SELECT functional_location_id, description, "
      + "parent_functional_location_id, 0 as level "
      + "FROM functional_location WHERE functional_location.functional_location_id = :parentFunctionalLocationId "
      + "UNION ALL "
      + "SELECT fl.functional_location_id, fl.description, fl.parent_functional_location_id, "
      + "functional_location_hierarchy.level + 1 "
      + "FROM functional_location fl, functional_location_hierarchy "
      + "WHERE fl.parent_functional_location_id = functional_location_hierarchy.functional_location_id"
      + ") "
      + "SELECT functional_location_id AS functionalLocationId, description, "
      + "parent_functional_location_id AS parentFunctionalLocationId, "
      + "level FROM functional_location_hierarchy ORDER BY level ASC",
      nativeQuery = true)
  List<FunctionalLocationHierarchyDto> findFunctionalLocationHierarchy(String parentFunctionalLocationId);
}

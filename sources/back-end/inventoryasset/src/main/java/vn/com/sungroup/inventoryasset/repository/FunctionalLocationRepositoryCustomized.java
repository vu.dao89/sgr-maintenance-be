package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.FunctionalLocationRequest;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;

public interface FunctionalLocationRepositoryCustomized {

  Page<FunctionalLocation> findFunctionalLocationByFilter(FunctionalLocationRequest request,
      Pageable pageable);
}

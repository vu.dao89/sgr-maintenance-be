package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.FunctionalLocationRequest;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;
import vn.com.sungroup.inventoryasset.entity.QFunctionalLocation;

@Repository
@RequiredArgsConstructor
public class FunctionalLocationRepositoryCustomizedImpl implements FunctionalLocationRepositoryCustomized {

  private final JPAQueryFactory queryFactory;

  @Override
  public Page<FunctionalLocation> findFunctionalLocationByFilter(FunctionalLocationRequest request,
      Pageable pageable) {
    QFunctionalLocation functionalLocation = QFunctionalLocation.functionalLocation;

    var jpaQuery = queryFactory.selectFrom(functionalLocation);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(functionalLocation.description.containsIgnoreCase(request.getFilterText())
          .or(functionalLocation.functionalLocationId.containsIgnoreCase(request.getFilterText())));
    }
    else {
      if (StringUtils.hasText(request.getDescription())) {
        jpaQuery.where(functionalLocation.description.containsIgnoreCase(request.getDescription()));
      }
    }

    if (!request.getWorkCenterCodes().isEmpty()) {
      jpaQuery.where(functionalLocation.maintenanceWorkCenterId.in(request.getWorkCenterCodes()));
    }

    if (!request.getPlannerGroups().isEmpty()) {
      jpaQuery.where(functionalLocation.plannerGroup.in(request.getPlannerGroups()));
    }

    if (!request.getObjectTypes().isEmpty()) {
      jpaQuery.where(functionalLocation.objectTypeId.in(request.getObjectTypes()));
    }

    if (!request.getCategories().isEmpty()) {
      jpaQuery.where(functionalLocation.functionalLocationCategory.in(request.getCategories()));
    }

    if (!request.getMaintenancePlantCodes().isEmpty()) {
      jpaQuery.where(functionalLocation.maintenancePlantId.in(request.getMaintenancePlantCodes()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(functionalLocation.functionalLocationId.asc()).orderBy(functionalLocation.maintenancePlantId.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;

import java.util.Optional;
import java.util.UUID;

public interface GlobalDocumentRepository extends JpaRepository<GlobalDocument, UUID> {
    Optional<GlobalDocument> findByFileNameContainsIgnoreCase(String fileName);
}

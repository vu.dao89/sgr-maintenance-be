package vn.com.sungroup.inventoryasset.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.GroupCode;

public interface GroupCodeRepository extends JpaRepository<GroupCode, UUID>,
    GroupCodeRepositoryCustomized {

}

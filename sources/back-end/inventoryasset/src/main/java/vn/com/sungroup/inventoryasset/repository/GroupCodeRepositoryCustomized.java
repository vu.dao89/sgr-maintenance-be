package vn.com.sungroup.inventoryasset.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.CodeGroupRequest;
import vn.com.sungroup.inventoryasset.dto.request.CodeRequest;
import vn.com.sungroup.inventoryasset.dto.response.groupcode.CodeGroupResponse;
import vn.com.sungroup.inventoryasset.entity.GroupCode;

public interface GroupCodeRepositoryCustomized {

  Page<CodeGroupResponse> findCodeGroupsByConditions(Pageable pageable, CodeGroupRequest request);
  Page<GroupCode> findCodesByConditions(Pageable pageable, CodeRequest request);
}

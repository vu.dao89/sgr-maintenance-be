package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.CodeGroupRequest;
import vn.com.sungroup.inventoryasset.dto.request.CodeRequest;
import vn.com.sungroup.inventoryasset.dto.response.groupcode.CodeGroupResponse;
import vn.com.sungroup.inventoryasset.entity.GroupCode;
import vn.com.sungroup.inventoryasset.entity.QGroupCode;

@Repository
@RequiredArgsConstructor
public class GroupCodeRepositoryCustomizedImpl implements GroupCodeRepositoryCustomized {

  private final JPAQueryFactory queryFactory;

  @Override
  public Page<CodeGroupResponse> findCodeGroupsByConditions(Pageable pageable, CodeGroupRequest request) {
    var entity = QGroupCode.groupCode;
    var jpaQuery = queryFactory.selectFrom(entity);

    if(StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(entity.codeGroup.containsIgnoreCase(request.getFilterText())
              .or(entity.codeGroupDesc.containsIgnoreCase(request.getFilterText())));
    }
    else
    {
      if(StringUtils.hasText(request.getCodeGroup())) {
        jpaQuery.where(entity.codeGroup.containsIgnoreCase(request.getCodeGroup()));
      }

      if(StringUtils.hasText(request.getCodeGroupDesc())) {
        jpaQuery.where(entity.codeGroupDesc.containsIgnoreCase(request.getCodeGroupDesc()));
      }
    }

    if(!request.getCatalogProfiles().isEmpty()) {
      jpaQuery.where(entity.catalogProfile.in(request.getCatalogProfiles()));
    }

    if(!request.getCatalogs().isEmpty()) {
      jpaQuery.where(entity.catalog.in(request.getCatalogs()));
    }

    var query = jpaQuery.select(Projections.constructor(CodeGroupResponse.class,
            entity.catalogProfile,
            entity.catalogProfileText,
            entity.catalog,
            entity.catalogDesc,
            entity.codeGroup,
            entity.codeGroupDesc
            )).distinct();

    final long totalData = query.stream().count();
    var data =
            query.orderBy(entity.codeGroup.asc()).orderBy(entity.catalog.asc()).orderBy(entity.catalogProfile.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }

  @Override
  public Page<GroupCode> findCodesByConditions(Pageable pageable, CodeRequest request) {
    var entity = QGroupCode.groupCode;
    var jpaQuery = queryFactory.selectFrom(entity);

    if(StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(entity.code.containsIgnoreCase(request.getFilterText())
              .or(entity.codeDesc.containsIgnoreCase(request.getFilterText())));
    }
    else
    {
      if(StringUtils.hasText(request.getCode())) {
        jpaQuery.where(entity.code.containsIgnoreCase(request.getCode()));
      }

      if(StringUtils.hasText(request.getCodeDesc())) {
        jpaQuery.where(entity.codeDesc.containsIgnoreCase(request.getCodeDesc()));
      }
    }

    if(!request.getCodeGroups().isEmpty()) {
      jpaQuery.where(entity.codeGroup.in(request.getCodeGroups()));
    }

    if(StringUtils.hasText(request.getCatalogProfile())){
      jpaQuery.where(entity.catalogProfile.containsIgnoreCase(request.getCatalogProfile()));
    }

    if(StringUtils.hasText(request.getCatalog())){
      jpaQuery.where(entity.catalog.containsIgnoreCase(request.getCatalog()));
    }


    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(entity.code.asc()).orderBy(entity.codeGroup.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

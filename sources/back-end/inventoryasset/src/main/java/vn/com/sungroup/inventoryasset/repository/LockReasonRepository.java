package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.LockReason;

import java.util.Optional;
import java.util.UUID;

public interface LockReasonRepository extends JpaRepository<LockReason, UUID> {
    Optional<LockReason> findByCode(Integer code);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.MaintenancePlant;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MaintenancePlantRepository extends JpaRepository<MaintenancePlant, UUID>, MaintenancePlantRepositoryCustomized {

  Optional<MaintenancePlant> findByCode(String code);
  List<MaintenancePlant> findAllByCodeIn(List<String> code);
}

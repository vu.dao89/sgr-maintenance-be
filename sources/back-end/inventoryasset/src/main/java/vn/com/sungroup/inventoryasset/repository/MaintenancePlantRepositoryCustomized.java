package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.MaintenancePlantRequest;
import vn.com.sungroup.inventoryasset.entity.MaintenancePlant;

public interface MaintenancePlantRepositoryCustomized {
    Page<MaintenancePlant> findAllByConditions(Pageable pageable, MaintenancePlantRequest input);
}

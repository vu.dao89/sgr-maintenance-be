package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.MaintenancePlantRequest;
import vn.com.sungroup.inventoryasset.entity.MaintenancePlant;
import vn.com.sungroup.inventoryasset.entity.QMaintenancePlant;

@Repository
@RequiredArgsConstructor
@Slf4j
public class MaintenancePlantRepositoryCustomizedImpl implements MaintenancePlantRepositoryCustomized {

    private final JPAQueryFactory queryFactory;
    @Override
    public Page<MaintenancePlant> findAllByConditions(Pageable pageable, MaintenancePlantRequest input) {
        var entity = QMaintenancePlant.maintenancePlant;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.code.containsIgnoreCase(input.getFilterText())
                            .or(entity.plantName.containsIgnoreCase(input.getFilterText())));
        }
        else
        {
            if(StringUtils.hasText(input.getName())) {
                jpaQuery.where(entity.plantName.containsIgnoreCase(input.getName()));
            }
        }

        if(!input.getMaintenancePlantCodes().isEmpty()) {
            jpaQuery.where(entity.code.in(input.getMaintenancePlantCodes()));
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.code.asc()).orderBy(entity.plantName.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

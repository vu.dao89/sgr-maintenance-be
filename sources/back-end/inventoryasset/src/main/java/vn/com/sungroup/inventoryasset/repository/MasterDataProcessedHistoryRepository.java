package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.com.sungroup.inventoryasset.entity.MasterDataProcessedHistory;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

public interface MasterDataProcessedHistoryRepository extends JpaRepository<MasterDataProcessedHistory, UUID> {
    Optional<MasterDataProcessedHistory> findByFileName(String fileName);
    Optional<MasterDataProcessedHistory> findByProcessedAtBetween(LocalDate fromDate,LocalDate toDate);
    Optional<MasterDataProcessedHistory> findByBackedupAtBetween(LocalDate fromDate,LocalDate toDate);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM MasterDataProcessedHistory u WHERE u.fileName = :fileName")
    boolean checkExistFileName(String fileName);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Measurepoint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeasuringPointMgRepository extends MongoRepository<MeasuringPointMg, String> {
    List<MeasuringPointMg> getAllByPointIn(List<String> points);

    Optional<MeasuringPointMg> findByPoint(String point);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Spring Data JPA repository for the Measurepoint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeasuringPointRepository extends JpaRepository<MeasuringPoint, UUID> {
    List<MeasuringPoint> getAllByPointIn(List<String> points);
    Optional<MeasuringPoint> findByPoint(String point);
}

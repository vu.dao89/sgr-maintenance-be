package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NotificationDocumentRepository extends JpaRepository<NotificationDocument, UUID> {
    Page<NotificationDocument> findAllByNotificationCode(String notificationCode, Pageable pageable);
    List<NotificationDocument> findAllByNotificationCode(String notificationCode);

    Optional<NotificationDocument> findByNotificationCodeAndDocumentId(String notificationCode, UUID documentId);
}



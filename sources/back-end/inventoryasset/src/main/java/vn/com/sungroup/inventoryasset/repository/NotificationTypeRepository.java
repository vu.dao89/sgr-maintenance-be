package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.NotificationType;

import java.util.Optional;
import java.util.UUID;

public interface NotificationTypeRepository extends JpaRepository<NotificationType, UUID>, NotificationTypeRepositoryCustomized {
    Optional<NotificationType> findByNotiType(String notiType);
}

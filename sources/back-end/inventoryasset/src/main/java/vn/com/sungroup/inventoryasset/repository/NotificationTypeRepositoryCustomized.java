package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.NotificationTypeRequest;
import vn.com.sungroup.inventoryasset.entity.NotificationType;

public interface NotificationTypeRepositoryCustomized {

  Page<NotificationType> findNotificationTypeByFilter(NotificationTypeRequest request, Pageable pageable);
}

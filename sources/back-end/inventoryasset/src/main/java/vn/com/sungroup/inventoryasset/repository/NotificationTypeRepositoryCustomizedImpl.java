package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.NotificationTypeRequest;
import vn.com.sungroup.inventoryasset.entity.NotificationType;
import vn.com.sungroup.inventoryasset.entity.QNotificationType;

@Repository
@RequiredArgsConstructor
public class NotificationTypeRepositoryCustomizedImpl implements NotificationTypeRepositoryCustomized {

  private final JPAQueryFactory jpaQueryFactory;

  @Override
  public Page<NotificationType> findNotificationTypeByFilter(NotificationTypeRequest request,
      Pageable pageable) {

    QNotificationType notificationType = QNotificationType.notificationType;

    var jpaQuery = jpaQueryFactory.selectFrom(notificationType);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(notificationType.notiType.containsIgnoreCase(request.getFilterText())
          .or(notificationType.description.containsIgnoreCase(request.getFilterText())));
    }

    if (!request.getNotiTypes().isEmpty()) {
      jpaQuery.where(notificationType.notiType.in(request.getNotiTypes()));
    }

    if (StringUtils.hasText(request.getDescription())) {
      jpaQuery.where(notificationType.description.containsIgnoreCase(request.getDescription()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(notificationType.notiType.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.ObjectType;

import java.util.Optional;
import java.util.UUID;

public interface ObjectTypeRepository extends JpaRepository<ObjectType, UUID>, ObjectTypeRepositoryCustomized {
    Optional<ObjectType> findByCode(String code);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.ObjectTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ObjectType;

public interface ObjectTypeRepositoryCustomized {

  Page<ObjectType> findObjectTypeByFilter(ObjectTypeRequest request, Pageable pageable);
}

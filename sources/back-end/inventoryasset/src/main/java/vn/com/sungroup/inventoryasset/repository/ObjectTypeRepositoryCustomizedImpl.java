package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.ObjectTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ObjectType;
import vn.com.sungroup.inventoryasset.entity.QObjectType;

@RequiredArgsConstructor
@Repository
public class ObjectTypeRepositoryCustomizedImpl implements ObjectTypeRepositoryCustomized {

  private final JPAQueryFactory jpaQueryFactory;

  @Override
  public Page<ObjectType> findObjectTypeByFilter(ObjectTypeRequest request, Pageable pageable) {
    QObjectType objectType = QObjectType.objectType;

    var jpaQuery = jpaQueryFactory.selectFrom(objectType);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(objectType.code.containsIgnoreCase(request.getFilterText())
          .or(objectType.description.containsIgnoreCase(request.getFilterText())));
    }

    if (!request.getCodes().isEmpty()) {
      jpaQuery.where(objectType.code.in(request.getCodes()));
    }

    if (StringUtils.hasText(request.getDescription())) {
      jpaQuery.where(objectType.description.containsIgnoreCase(request.getDescription()));
    }

    final long totalData = jpaQuery.stream().count();
    var data = jpaQuery.orderBy(objectType.code.desc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OperationDocumentRepository extends JpaRepository<OperationDocument, UUID>, OperationDocumentRepositoryCustomized {
    Page<OperationDocument> findAllByOperationCode(String operationCode, Pageable pageable);
    List<OperationDocument> findByOperationCodeAndWorkOrderId(String operationCode, String workOrderId);
    List<OperationDocument> findByDocumentIdIn(List<UUID> documentIds);
    Optional<OperationDocument> findByWorkOrderIdAndOperationCodeAndSuperOperationCodeAndDocumentId(String workOrderId,
                                                                                                    String operationCode,
                                                                                                    String superOperationCode, UUID documentId);
}
package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;

import java.util.List;
import java.util.UUID;

public interface OperationDocumentRepositoryCustomized{
    List<OperationDocument> findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(String workOrderId,String operationCode,String superOperationCode);
}
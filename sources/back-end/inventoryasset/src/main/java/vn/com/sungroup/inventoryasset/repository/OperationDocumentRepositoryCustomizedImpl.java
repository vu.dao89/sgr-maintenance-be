package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.entity.QOperationDocument;

import java.util.List;
@RequiredArgsConstructor
@Repository
public class OperationDocumentRepositoryCustomizedImpl implements OperationDocumentRepositoryCustomized{
    private final JPAQueryFactory queryFactory;
    @Override
    public List<OperationDocument> findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(String workOrderId, String operationCode, String superOperationCode) {
        QOperationDocument operationDocumentEntity = QOperationDocument.operationDocument;
        var jpaQuery = queryFactory.selectFrom(operationDocumentEntity);
        jpaQuery.where(operationDocumentEntity.workOrderId.eq(workOrderId)
                .and(operationDocumentEntity.operationCode.eq(operationCode)));
        if (superOperationCode.isEmpty()){
            jpaQuery.where(operationDocumentEntity.superOperationCode.isNull().or(operationDocumentEntity.superOperationCode.eq(superOperationCode)));
        }else{
            jpaQuery.where(operationDocumentEntity.superOperationCode.eq(superOperationCode));
        }
        return jpaQuery.fetch();
    }
}

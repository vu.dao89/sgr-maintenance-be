package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OperationHistoryRepository extends JpaRepository<OperationHistory, UUID>, OperationHistoryRepositoryCustomized{
    Optional<OperationHistory> findById(UUID id);

    Optional<OperationHistory> findTopByWorkOrderIdAndOperationCodeAndActivity(String workOrderId,String operationCode,String activity);
}

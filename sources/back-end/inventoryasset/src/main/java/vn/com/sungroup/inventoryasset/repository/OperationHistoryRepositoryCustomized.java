package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryRequest;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;

import java.util.List;

public interface OperationHistoryRepositoryCustomized {
    Page<OperationHistory> getByFilter(OperationHistoryRequest request, Pageable pageable);

    OperationHistory getCurrentActive(String userId);

    List<OperationHistory> findByWorkOrderIdAndOperationCodeNotComplete(String userId);

    OperationHistory getStartingOperationHistory(String userId);

    OperationHistory getLatestByOperation(String workOrderId, String operationCode, String superOperationCode);


    OperationHistory findByWorkOrderIdAndOperationCodeAndSuperOperationCode(String workOrderId,String operationCode,String superOperationCode);
}

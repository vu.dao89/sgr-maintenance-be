package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryRequest;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;
import vn.com.sungroup.inventoryasset.entity.QOperationHistory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.Operation_Activity_Complete;
import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.Operation_Activity_Start;

@RequiredArgsConstructor
@Repository
public class OperationHistoryRepositoryCustomizedImpl implements OperationHistoryRepositoryCustomized{
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<OperationHistory> getByFilter(OperationHistoryRequest request, Pageable pageable) {
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);

        if (StringUtils.hasText(request.getFilterText())) {
            jpaQuery.where(operationHistoryEntity.operationCode.containsIgnoreCase(request.getFilterText())
                    .or(operationHistoryEntity.userId.containsIgnoreCase(request.getFilterText())));
        }

        if (!request.getOperationIds().isEmpty()) {
            jpaQuery.where(operationHistoryEntity.operationCode.in(request.getOperationIds()));
        }

        if (!request.getUserIds().isEmpty()) {
            jpaQuery.where(operationHistoryEntity.userId.in(request.getUserIds()));
        }

        final long totalData = jpaQuery.stream().count();
        var data = jpaQuery.offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();
        return new PageImpl<>(data, pageable, totalData);
    }

    @Override
    public OperationHistory getCurrentActive(String userId) {
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);
        var operationHistories = jpaQuery.where(operationHistoryEntity.userId.eq(userId)).fetch();
        var groups = operationHistories.stream().collect(Collectors.groupingBy(x->x.getOperationCode()));
        for (var g: groups.values()) {
            if (g.size() == 1){
                return g.stream().findFirst().get();
            }
        }
        return null;
    }

    @Override
    public List<OperationHistory> findByWorkOrderIdAndOperationCodeNotComplete(String userId) {
        List<OperationHistory>  result = new ArrayList<>();
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);
        if (userId != null && !userId.isEmpty()){
            jpaQuery.where(operationHistoryEntity.userId.eq(userId));
        }
        jpaQuery
                .orderBy(operationHistoryEntity.workOrderId.asc())
                .orderBy(operationHistoryEntity.operationCode.asc())
                .orderBy(operationHistoryEntity.startAtDate.desc())
                .orderBy(operationHistoryEntity.startAtTime.desc());
        var operationHistories = jpaQuery.fetch();
        var groups = operationHistories.stream().collect(Collectors.groupingBy(x-> Arrays.asList(x.getOperationCode()
                , x.getWorkOrderId()) ));
        for (var g: groups.values()) {
            var lastOperationHistory = g.stream().findFirst().orElse(null);
            if(lastOperationHistory != null &&
                    !lastOperationHistory.getActivity().equalsIgnoreCase(Operation_Activity_Complete) &&
                    (lastOperationHistory.getTransferredUserId() == null ||
                            lastOperationHistory.getTransferredUserId().isEmpty() ||
                            lastOperationHistory.getTransferredUserId().isBlank())){
                result.add(lastOperationHistory);
            }
        }
        return result;
    }

    @Override
    public OperationHistory getStartingOperationHistory(String userId){
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);
        if (userId != null && !userId.isEmpty()){
            jpaQuery.where(operationHistoryEntity.userId.eq(userId));
        }

        jpaQuery
                .orderBy(operationHistoryEntity.workOrderId.asc())
                .orderBy(operationHistoryEntity.operationCode.asc())
                .orderBy(operationHistoryEntity.startAtDate.desc())
                .orderBy(operationHistoryEntity.startAtTime.desc());
        var operationHistory = jpaQuery.fetch();
        var groups = operationHistory.stream().collect(Collectors.groupingBy(x-> Arrays.asList(x.getOperationCode()
                , x.getWorkOrderId())));
        for (List<OperationHistory> operationHistories : groups.values()){
            var lastOperationHistory = operationHistories.stream().findFirst().orElse(null);
            if(lastOperationHistory != null &&
                    lastOperationHistory.getActivity().equalsIgnoreCase(Operation_Activity_Start) &&
                    (lastOperationHistory.getTransferredUserId() == null ||
                            lastOperationHistory.getTransferredUserId().isEmpty() ||
                            lastOperationHistory.getTransferredUserId().isBlank())){
                return lastOperationHistory;
            }
        }

        return null;
    }

    @Override
    public OperationHistory getLatestByOperation(String workOrderId,String operationCode,String superOperationCode) {
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);
        jpaQuery.where(operationHistoryEntity.workOrderId.eq(workOrderId)
                        .and(operationHistoryEntity.operationCode.eq(operationCode)));
        if (superOperationCode.isEmpty()){
            jpaQuery.where(operationHistoryEntity.superOperationCode.isNull().or(operationHistoryEntity.superOperationCode.eq(superOperationCode)));
        }else{
            jpaQuery.where(operationHistoryEntity.superOperationCode.eq(superOperationCode));
        }
        jpaQuery.orderBy(operationHistoryEntity.startAtDate.desc()).orderBy(operationHistoryEntity.startAtTime.desc()).stream().findFirst();

        return jpaQuery.fetchFirst();
    }

    @Override
    public OperationHistory findByWorkOrderIdAndOperationCodeAndSuperOperationCode(String workOrderId, String operationCode, String superOperationCode) {
        QOperationHistory operationHistoryEntity = QOperationHistory.operationHistory;
        var jpaQuery = queryFactory.selectFrom(operationHistoryEntity);
        jpaQuery.where(operationHistoryEntity.workOrderId.eq(workOrderId)
                .and(operationHistoryEntity.operationCode.eq(operationCode)));
        if (superOperationCode.isEmpty()){
            jpaQuery.where(operationHistoryEntity.superOperationCode.isNull().or(operationHistoryEntity.superOperationCode.eq(superOperationCode)));
        }else{
            jpaQuery.where(operationHistoryEntity.superOperationCode.eq(superOperationCode));
        }

        jpaQuery.orderBy(operationHistoryEntity.startAtDate.desc()).orderBy(operationHistoryEntity.startAtTime.desc());

        return jpaQuery.fetchFirst();
    }
}

package vn.com.sungroup.inventoryasset.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.Partner;

/**
 * Spring Data JPA repository for the Partner entity.
 */
@Repository
public interface PartnerRepository extends JpaRepository<Partner, UUID> {

}

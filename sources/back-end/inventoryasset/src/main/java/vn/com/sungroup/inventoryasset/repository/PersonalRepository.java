package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.Personnel;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonalRepository extends JpaRepository<Personnel, UUID>,
    PersonalRepositoryCustomized {
  Optional<Personnel> findByCodeAndWorkCenterId(String code, String workCenterId);
}

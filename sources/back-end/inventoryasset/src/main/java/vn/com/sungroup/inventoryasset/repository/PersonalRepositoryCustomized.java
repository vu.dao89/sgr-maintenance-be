package vn.com.sungroup.inventoryasset.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.PersonnelRequest;
import vn.com.sungroup.inventoryasset.entity.Personnel;

public interface PersonalRepositoryCustomized {

  Page<Personnel> getByFilter(PersonnelRequest request, Pageable pageable);
}

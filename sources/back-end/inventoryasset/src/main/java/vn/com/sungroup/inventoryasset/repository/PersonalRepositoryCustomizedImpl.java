package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.PersonnelRequest;
import vn.com.sungroup.inventoryasset.entity.Personnel;
import vn.com.sungroup.inventoryasset.entity.QPersonnel;

@Repository
@RequiredArgsConstructor
@Slf4j
public class PersonalRepositoryCustomizedImpl implements PersonalRepositoryCustomized {

    private final JPAQueryFactory queryFactory;

    @Override
    public Page<Personnel> getByFilter(PersonnelRequest request, Pageable pageable) {
        QPersonnel personalEntity = QPersonnel.personnel;
        var jpaQuery = queryFactory.selectFrom(personalEntity);

        if (StringUtils.hasText(request.getFilterText())) {
            jpaQuery.where(personalEntity.code.containsIgnoreCase(request.getFilterText())
                    .or(personalEntity.name.containsIgnoreCase(request.getFilterText())));

        }

        if (!request.getCodes().isEmpty()) {
            jpaQuery.where(personalEntity.code.in(request.getCodes()));
        }

        if (StringUtils.hasText(request.getName())) {
            jpaQuery.where(personalEntity.name.containsIgnoreCase(request.getName()));
        }

        if (!request.getMaintenancePlantCodes().isEmpty()) {
            jpaQuery.where(personalEntity.maintenancePlantId.in(request.getMaintenancePlantCodes()));
        }

        if (!request.getWorkCenterIds().isEmpty()) {
            jpaQuery.where(personalEntity.workCenterId.in(request.getWorkCenterIds()));
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(personalEntity.code.asc()).orderBy(personalEntity.name.asc()).orderBy(personalEntity.maintenancePlantId.asc()).orderBy(personalEntity.workCenterId.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

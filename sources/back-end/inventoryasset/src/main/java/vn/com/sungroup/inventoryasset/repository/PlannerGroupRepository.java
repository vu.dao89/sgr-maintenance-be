package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.PlannerGroup;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PlannerGroupRepository extends JpaRepository<PlannerGroup, UUID>, PlannerGroupRepositoryCustomized {
    Optional<PlannerGroup> findByPlannerGroupIdAndMaintenancePlantId(String plannerGroupId, String maintenancePlantId);
}

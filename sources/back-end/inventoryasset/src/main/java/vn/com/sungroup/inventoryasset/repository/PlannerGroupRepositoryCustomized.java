package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.PlannerGroupRequest;
import vn.com.sungroup.inventoryasset.entity.PlannerGroup;

public interface PlannerGroupRepositoryCustomized {
    Page<PlannerGroup> findAllByConditions(Pageable pageable, PlannerGroupRequest input);
}

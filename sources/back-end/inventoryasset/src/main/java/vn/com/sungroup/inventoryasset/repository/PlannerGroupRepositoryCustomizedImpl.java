package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.PlannerGroupRequest;
import vn.com.sungroup.inventoryasset.entity.PlannerGroup;
import vn.com.sungroup.inventoryasset.entity.QPlannerGroup;

@Repository
@RequiredArgsConstructor
@Slf4j
public class PlannerGroupRepositoryCustomizedImpl implements PlannerGroupRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<PlannerGroup> findAllByConditions(Pageable pageable, PlannerGroupRequest input) {
        var plannerGroupEntity = QPlannerGroup.plannerGroup;
        var jpaQuery = queryFactory.selectFrom(plannerGroupEntity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(plannerGroupEntity.plannerGroupId.containsIgnoreCase(input.getFilterText())
                    .or(plannerGroupEntity.name.containsIgnoreCase(input.getFilterText())));

        }
        else
        {
            if(StringUtils.hasText(input.getPlannerGroupId())) {
                jpaQuery.where(plannerGroupEntity.plannerGroupId.eq(input.getPlannerGroupId()));
            }

            if(StringUtils.hasText(input.getName())) {
                jpaQuery.where(plannerGroupEntity.name.containsIgnoreCase(input.getName()));
            }
        }

        if(!input.getMaintenancePlantCodes().isEmpty()) {
            jpaQuery.where(plannerGroupEntity.maintenancePlantId.in(input.getMaintenancePlantCodes()));
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(plannerGroupEntity.plannerGroupId.asc()).orderBy(plannerGroupEntity.name.asc()).orderBy(plannerGroupEntity.maintenancePlantId.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

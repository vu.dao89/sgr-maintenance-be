package vn.com.sungroup.inventoryasset.repository;

import io.lettuce.core.dynamic.annotation.Param;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.com.sungroup.inventoryasset.entity.StorageLocation;

import java.util.List;

public interface StorageLocationRepository extends JpaRepository<StorageLocation, UUID> {
//    List<StorageLocation> findByStorageLocationId(String storageLocationId);

//    @Query("select s from StorageLocation s where s.plant.plantId not in (:plantIds) or s.storageLocationId not in (:storageLocationIds) or s.description not in (:descriptions)")
//    List<StorageLocation> findAll(@Param("plantIds") List<String> plantIds,
//                                     @Param("storageLocationIds") List<String> storageLocationIds,
//                                     @Param("descriptions") List<String> descriptions);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.SystemCondition;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SystemConditionRepository extends JpaRepository<SystemCondition, UUID>, SystemConditionRepositoryCustomized {

  Optional<SystemCondition> findByCode(String code);
}

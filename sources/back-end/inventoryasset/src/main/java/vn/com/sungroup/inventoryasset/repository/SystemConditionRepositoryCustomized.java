package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.SystemConditionRequest;
import vn.com.sungroup.inventoryasset.entity.SystemCondition;

public interface SystemConditionRepositoryCustomized {
    Page<SystemCondition> findAllByConditions(Pageable pageable, SystemConditionRequest input);
}

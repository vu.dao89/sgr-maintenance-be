package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.SystemConditionRequest;
import vn.com.sungroup.inventoryasset.entity.QSystemCondition;
import vn.com.sungroup.inventoryasset.entity.SystemCondition;

@Repository
@RequiredArgsConstructor
@Slf4j
public class SystemConditionRepositoryCustomizedImpl implements SystemConditionRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<SystemCondition> findAllByConditions(Pageable pageable, SystemConditionRequest input) {
        var entity = QSystemCondition.systemCondition;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.code.containsIgnoreCase(input.getFilterText())
                            .or(entity.text.containsIgnoreCase(input.getFilterText())));
        }
        else
        {
            if(!input.getCodes().isEmpty()) {
                jpaQuery.where(entity.code.in(input.getCodes()));
            }

            if(StringUtils.hasText(input.getText())) {
                jpaQuery.where(entity.text.containsIgnoreCase(input.getText()));
            }
        }

        final long totalData = jpaQuery.stream().count();
        var data = jpaQuery.orderBy(entity.code.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;

import java.util.Optional;
import java.util.UUID;

public interface SystemStatusRepository extends JpaRepository<SystemStatus, UUID>, SystemStatusRepositoryCustomized {

  Optional<SystemStatus> findByStatusId(String statusId);

  Optional<SystemStatus> findByStatusIdAndCode(String statusId, String Code);

  Optional<SystemStatus> findByCode(String Code);
}

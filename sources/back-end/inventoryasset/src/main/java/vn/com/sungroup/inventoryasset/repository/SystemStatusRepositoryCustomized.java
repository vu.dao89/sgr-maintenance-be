package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.SystemStatusRequest;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;

public interface SystemStatusRepositoryCustomized {
    Page<SystemStatus> findAllByConditions(Pageable pageable, SystemStatusRequest input);
}

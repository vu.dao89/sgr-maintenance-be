package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.SystemStatusRequest;
import vn.com.sungroup.inventoryasset.entity.QSystemStatus;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;

@Repository
@RequiredArgsConstructor
@Slf4j
public class SystemStatusRepositoryCustomizedImpl implements SystemStatusRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<SystemStatus> findAllByConditions(Pageable pageable, SystemStatusRequest input) {
        var entity = QSystemStatus.systemStatus;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.statusId.containsIgnoreCase(input.getFilterText())
                    .or(entity.code.containsIgnoreCase(input.getFilterText())
                            .or(entity.description.containsIgnoreCase(input.getFilterText()))));
        }
        else
        {
            if(!input.getStatusIds().isEmpty()) {
                jpaQuery.where(entity.statusId.in(input.getStatusIds()));
            }

            if(!input.getCodes().isEmpty()) {
                jpaQuery.where(entity.code.in(input.getCodes()));
            }

            if(StringUtils.hasText(input.getDescription())) {
                jpaQuery.where(entity.description.containsIgnoreCase(input.getDescription()));
            }
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.statusId.asc()).orderBy(entity.code.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

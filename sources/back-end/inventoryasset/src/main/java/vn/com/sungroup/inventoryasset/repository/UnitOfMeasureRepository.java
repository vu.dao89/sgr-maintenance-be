package vn.com.sungroup.inventoryasset.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UnitOfMeasureRepository extends JpaRepository<UnitOfMeasurement, UUID>, UnitOfMeasureRepositoryCustomized {

  Optional<UnitOfMeasurement> findByCode(String code);
  List<UnitOfMeasurement> findAllByCodeIn(List<String> codes);
}

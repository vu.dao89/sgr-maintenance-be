package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.UnitOfMeasurementRequest;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;

public interface UnitOfMeasureRepositoryCustomized {
    Page<UnitOfMeasurement> findAllByConditions(Pageable pageable, UnitOfMeasurementRequest input);
}

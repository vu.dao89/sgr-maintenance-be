package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;

import java.util.Optional;
import java.util.UUID;

public interface WorkCenterRepository extends JpaRepository<WorkCenter, UUID>, WorkCenterRepositoryCustomized {
    Optional<WorkCenter> findByCode(String code);
    Optional<WorkCenter> findByCodeAndMaintenancePlantId(String code, String maintenancePlantId);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.WorkCenterRequest;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;

public interface WorkCenterRepositoryCustomized {

  Page<WorkCenter> findByFilter(WorkCenterRequest request, Pageable pageable);

}

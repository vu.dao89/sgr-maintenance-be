package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.WorkCenterRequest;
import vn.com.sungroup.inventoryasset.entity.QWorkCenter;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;

@Repository
@RequiredArgsConstructor
public class WorkCenterRepositoryCustomizedImpl implements WorkCenterRepositoryCustomized {

  private final JPAQueryFactory queryFactory;

  @Override
  public Page<WorkCenter> findByFilter(WorkCenterRequest request, Pageable pageable) {
    QWorkCenter workCenter = QWorkCenter.workCenter;

    var jpaQuery = queryFactory.selectFrom(workCenter);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where(workCenter.code.containsIgnoreCase(request.getFilterText())
          .or(workCenter.description.containsIgnoreCase(request.getFilterText())));
    }
    else {
      if (StringUtils.hasText(request.getDescription())) {
        jpaQuery.where(workCenter.description.containsIgnoreCase(request.getDescription()));
      }
    }

    if (!request.getCodes().isEmpty()) {
      jpaQuery.where(workCenter.code.in(request.getCodes()));
    }

    if(!request.getWorkCenterCategories().isEmpty()) {
      jpaQuery.where(workCenter.category.in(request.getWorkCenterCategories()));
    }

    if (!request.getMaintenancePlantCodes().isEmpty()) {
      jpaQuery.where(workCenter.maintenancePlantId.in(request.getMaintenancePlantCodes()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(workCenter.code.asc()).orderBy(workCenter.maintenancePlantId.asc()).orderBy(workCenter.category.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

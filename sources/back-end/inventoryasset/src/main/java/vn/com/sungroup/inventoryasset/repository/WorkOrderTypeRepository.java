package vn.com.sungroup.inventoryasset.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;

import java.util.UUID;

public interface WorkOrderTypeRepository extends JpaRepository<WorkOrderType, UUID>, WorkOrderTypeRepositoryCustomized {

  Optional<WorkOrderType> findByTypeAndMaintenancePlantId(String workOrderType, String mainPlant);
}

package vn.com.sungroup.inventoryasset.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.WorkOrderTypeRequest;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;

public interface WorkOrderTypeRepositoryCustomized {
    Page<WorkOrderType> findAllByConditions(Pageable pageable, WorkOrderTypeRequest input);
}

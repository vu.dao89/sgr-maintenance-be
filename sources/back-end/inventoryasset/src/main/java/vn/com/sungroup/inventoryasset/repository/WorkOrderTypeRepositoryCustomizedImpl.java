package vn.com.sungroup.inventoryasset.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.WorkOrderTypeRequest;
import vn.com.sungroup.inventoryasset.entity.QWorkOrderType;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;

@Repository
@RequiredArgsConstructor
@Slf4j
public class WorkOrderTypeRepositoryCustomizedImpl implements WorkOrderTypeRepositoryCustomized {
    private final JPAQueryFactory queryFactory;
    @Override
    public Page<WorkOrderType> findAllByConditions(Pageable pageable, WorkOrderTypeRequest input) {
        var entity = QWorkOrderType.workOrderType;
        var jpaQuery = queryFactory.selectFrom(entity);

        if(StringUtils.hasText(input.getFilterText())) {
            jpaQuery.where(entity.type.containsIgnoreCase(input.getFilterText())
                            .or(entity.name.containsIgnoreCase(input.getFilterText())));
        }
        else
        {
            if(!input.getTypes().isEmpty()) {
                jpaQuery.where(entity.type.in(input.getTypes()));
            }

            if(StringUtils.hasText(input.getName())) {
                jpaQuery.where(entity.name.containsIgnoreCase(input.getName()));
            }
        }

        if(!input.getMaintenancePlantCodes().isEmpty()) {
            jpaQuery.where(entity.maintenancePlantId.in(input.getMaintenancePlantCodes()));
        }

        final long totalData = jpaQuery.stream().count();
        var data =
                jpaQuery.orderBy(entity.type.asc()).orderBy(entity.name.asc()).orderBy(entity.maintenancePlantId.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

        return new PageImpl<>(data, pageable, totalData);
    }
}

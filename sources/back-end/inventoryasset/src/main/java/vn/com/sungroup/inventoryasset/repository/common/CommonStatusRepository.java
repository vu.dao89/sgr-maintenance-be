package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.repository.CommonStatusRepositoryCustomized;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CommonStatusRepository extends JpaRepository<CommonStatus, UUID>,
    CommonStatusRepositoryCustomized {
    Optional<CommonStatus> findByStatusAndCode(String status,String code);
}

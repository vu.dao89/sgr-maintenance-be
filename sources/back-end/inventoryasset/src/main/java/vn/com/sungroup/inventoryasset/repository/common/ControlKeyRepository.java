package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.ControlKey;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ControlKeyRepository  extends JpaRepository<ControlKey, UUID>, ControlKeyRepositoryCustomized {

  Optional<ControlKey> findByCode(String code);
}
package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.ControlKeyRequest;
import vn.com.sungroup.inventoryasset.entity.ControlKey;

public interface ControlKeyRepositoryCustomized {
    Page<ControlKey> findAllByConditions(Pageable pageable, ControlKeyRequest input);
}

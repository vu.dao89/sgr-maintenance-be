package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.PriorityType;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PriorityTypeRepository extends JpaRepository<PriorityType, UUID>, PriorityTypeRepositoryCustomized {
    Optional<PriorityType> findByTypeAndPriority(String type, String priority);

    @Query("SELECT p FROM NotificationType nt INNER JOIN PriorityType p ON nt.priorityTypeId = p.type WHERE nt.notiType = :notificationType AND p.priority = :priority")
    Optional<PriorityType> findByNotificationTypeAndPriority(String notificationType, String priority);
}
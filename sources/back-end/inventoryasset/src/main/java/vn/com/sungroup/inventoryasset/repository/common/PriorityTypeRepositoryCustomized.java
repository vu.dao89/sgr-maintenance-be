package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.PriorityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.PriorityType;

public interface PriorityTypeRepositoryCustomized {

  Page<PriorityType> findPriorityTypeByFilter(PriorityTypeRequest request, Pageable pageable);
}

package vn.com.sungroup.inventoryasset.repository.common;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.request.PriorityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.QPriorityType;

@Repository
@RequiredArgsConstructor
public class PriorityTypeRepositoryCustomizedImpl implements PriorityTypeRepositoryCustomized {

  private final JPAQueryFactory jpaQueryFactory;

  @Override
  public Page<PriorityType> findPriorityTypeByFilter(PriorityTypeRequest request,
      Pageable pageable) {
    QPriorityType priorityType = QPriorityType.priorityType;

    var jpaQuery = jpaQueryFactory.selectFrom(priorityType);

    if (StringUtils.hasText(request.getFilterText())) {
      jpaQuery.where((priorityType.description.containsIgnoreCase(request.getFilterText())));
    }

    if (!request.getTypes().isEmpty()) {
      jpaQuery.where(priorityType.type.in(request.getTypes()));
    }

    if (!request.getPriorities().isEmpty()) {
      jpaQuery.where(priorityType.priority.in(request.getPriorities()));
    }

    final long totalData = jpaQuery.stream().count();
    var data =
            jpaQuery.orderBy(priorityType.type.asc()).orderBy(priorityType.priority.asc()).offset(pageable.getOffset()).limit(pageable.getPageSize()).fetch();

    return new PageImpl<>(data, pageable, totalData);
  }
}

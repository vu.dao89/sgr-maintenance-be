package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface VarianceReasonRepository  extends JpaRepository<VarianceReason, UUID>, VarianceReasonRepositoryCustomized {
    Optional<VarianceReason> findByCode(String code);
    Optional<VarianceReason> findByDescription(String description);
}

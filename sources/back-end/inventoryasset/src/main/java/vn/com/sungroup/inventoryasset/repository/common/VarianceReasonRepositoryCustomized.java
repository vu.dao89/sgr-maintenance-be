package vn.com.sungroup.inventoryasset.repository.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.VarianceReasonRequest;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

public interface VarianceReasonRepositoryCustomized {
    Page<VarianceReason> findAllByConditions(Pageable pageable, VarianceReasonRequest input);
}

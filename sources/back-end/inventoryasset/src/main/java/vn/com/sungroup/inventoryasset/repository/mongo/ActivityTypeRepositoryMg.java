package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.mongo.ActivityTypeMg;

@Repository
public interface ActivityTypeRepositoryMg extends MongoRepository<ActivityTypeMg, String> {

    Optional<ActivityTypeMg> findById(String id);

}

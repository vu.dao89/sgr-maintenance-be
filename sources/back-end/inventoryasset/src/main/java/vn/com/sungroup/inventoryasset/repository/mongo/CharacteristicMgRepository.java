package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;

public interface CharacteristicMgRepository extends MongoRepository<CharacteristicMg, String> {

}

package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.EquipmentCategoryMg;

public interface EquipmentCategoryRepositoryMg extends MongoRepository<EquipmentCategoryMg, String> {

    Optional<EquipmentCategoryMg> findEquipmentCategoryMgById(String equipmentCategoryId);

}

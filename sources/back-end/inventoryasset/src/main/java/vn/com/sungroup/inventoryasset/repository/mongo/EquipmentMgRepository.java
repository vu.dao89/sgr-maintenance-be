package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.EquipmentMg;

import java.util.Optional;

public interface EquipmentMgRepository extends MongoRepository<EquipmentMg, String> {

    Optional<EquipmentMg> findByEquipmentId(String equipmentId);
}

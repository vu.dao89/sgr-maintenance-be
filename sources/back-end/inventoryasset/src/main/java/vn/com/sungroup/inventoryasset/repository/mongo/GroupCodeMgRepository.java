package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.GroupCodeMg;

public interface GroupCodeMgRepository extends MongoRepository<GroupCodeMg, String> {

}

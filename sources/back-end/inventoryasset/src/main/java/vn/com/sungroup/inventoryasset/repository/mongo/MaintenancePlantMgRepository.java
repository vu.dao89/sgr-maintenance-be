package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;

public interface MaintenancePlantMgRepository extends MongoRepository<MaintenancePlantMg, String> {

  Optional<MaintenancePlantMg> findByCode(String maintenancePlantId);
}

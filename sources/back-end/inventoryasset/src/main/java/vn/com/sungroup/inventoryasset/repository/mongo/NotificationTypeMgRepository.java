package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.NotificationTypeMg;

public interface NotificationTypeMgRepository extends MongoRepository<NotificationTypeMg, String> {

}

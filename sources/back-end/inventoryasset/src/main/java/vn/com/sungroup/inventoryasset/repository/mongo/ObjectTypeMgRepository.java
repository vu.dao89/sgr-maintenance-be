package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.ObjectTypeMg;

public interface ObjectTypeMgRepository extends MongoRepository<ObjectTypeMg, String> {

  Optional<ObjectTypeMg> findByCode(String functionalLocationTypeId);
}

package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.mongo.PartnerMg;
import vn.com.sungroup.inventoryasset.mongo.WorkOrderTypeMg;

@Repository
public interface PartnerRepositoryMg extends MongoRepository<PartnerMg, String> {

}

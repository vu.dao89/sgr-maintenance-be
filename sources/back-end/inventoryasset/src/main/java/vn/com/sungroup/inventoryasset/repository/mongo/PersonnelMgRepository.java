package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.PersonnelMg;

public interface PersonnelMgRepository extends MongoRepository<PersonnelMg,String> {

}

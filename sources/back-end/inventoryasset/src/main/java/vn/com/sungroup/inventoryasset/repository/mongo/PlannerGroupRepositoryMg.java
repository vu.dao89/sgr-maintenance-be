package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.com.sungroup.inventoryasset.mongo.PlannerGroupMg;

@Repository
public interface PlannerGroupRepositoryMg extends MongoRepository<PlannerGroupMg, String> {
}

package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.SystemConditionMg;

public interface SystemConditionMgRepository extends MongoRepository<SystemConditionMg,String> {

}

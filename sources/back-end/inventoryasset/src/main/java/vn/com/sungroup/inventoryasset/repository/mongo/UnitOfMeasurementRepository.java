package vn.com.sungroup.inventoryasset.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.UnitOfMeasurementMg;

public interface UnitOfMeasurementRepository extends MongoRepository<UnitOfMeasurementMg, String> {

}

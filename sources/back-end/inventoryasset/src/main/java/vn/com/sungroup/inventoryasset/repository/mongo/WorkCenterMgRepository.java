package vn.com.sungroup.inventoryasset.repository.mongo;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import vn.com.sungroup.inventoryasset.mongo.WorkCenterMg;

public interface WorkCenterMgRepository extends MongoRepository<WorkCenterMg, String> {

  Optional<WorkCenterMg> findByCodeAndMaintenancePlantIdAndCostCenterCode(String code, String maintenancePlantId, String costCenterCode);
}

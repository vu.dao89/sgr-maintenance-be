package vn.com.sungroup.inventoryasset.sapclient;

import vn.com.sungroup.inventoryasset.dto.User;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.MeasPointSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.PostMeasPointSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCloseSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCreateSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationDetailSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationSearchSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric.NotificationMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationDetailRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationSearchRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeStatusRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.confirmation.OperationConfirmationRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric.WorkOrderMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post.WorkOrderCreateRequestSAP;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.dto.response.equipment.sap.MeasPointSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationCloseSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationDetailSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationMetricResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationSearchSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse.OperationMetricsSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeStatusResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

public interface SAPService {

    MeasPointSAPResponse getEquipmentMeasPoint(MeasPointSAPRequest request) throws SAPApiException;

    WorkOrderDetailsSAP getWorkOrder(WorkOrderDetailsRequest request) throws SAPApiException;

    OperationConfirmationResponse confirmation(OperationConfirmationRequestSAP request)
            throws SAPApiException;

    WorkOrderSearchSAP getWorkOrders(WorkOrderSearchRequestSAP requestToSAP) throws SAPApiException;

    WorkOrderChangeResponseSAP changeWorkOrder(WorkOrderChangeRequestSAP toSapChangeRequest)
            throws SAPApiException;

    WorkOrderChangeStatusResponseSAP changeWorkOrderStatus(WorkOrderChangeStatusRequestSAP changeStatusRequest)
            throws SAPApiException;

    OperationDetailResponse getOperationDetail(OperationDetailRequestSAP toOperationSAP)
            throws SAPApiException;

    SubOperationDetailResponse getSubOperationDetail(OperationDetailRequestSAP toOperationSAP)
            throws SAPApiException;

    OperationSearchResponse searchOperation(OperationSearchRequestSAP toOperationSearchSAP)
            throws SAPApiException;

    OperationMeasuresSAP getOperationMeasures(OperationMeasuresRequestSAP request)
            throws SAPApiException;

    OperationComponentResponse getOperationComponent(OperationDetailRequestSAP toOperationSAP)
            throws SAPApiException;

    BaseResponseSAP changeOperation(OperationChangeRequestSAP request) throws SAPApiException;

    OperationMetricsSAP getOperationMetrics(OperationMetricsRequestSAP requestSAP)
            throws SAPApiException;

    WorkOrderMetricResponse getWorkOrderMetric(WorkOrderMetricRequestSAP request)
            throws SAPApiException;

    NotificationMetricResponse getNotificationMetric(NotificationMetricRequestSAP request)
            throws SAPApiException;

    MaterialDocumentResponse createMaterialDocument(MaterialDocumentRequestSAP request) throws SAPApiException;

    BaseResponseSAP createWorkOrder(WorkOrderCreateRequestSAP input) throws SAPApiException;

    User getUserInfo(String token);

    NotificationSearchSAPResponse notificationSearch(NotificationSearchSAPRequest input) throws SAPApiException;

    NotificationCloseSAPResponse notificationClose(NotificationCloseSAPRequest input) throws SAPApiException;

    NotificationDetailSAPResponse notificationDetail(NotificationDetailSAPRequest input) throws SAPApiException;

    BaseResponseSAP notificationChange(NotificationCreateSAPRequest input) throws SAPApiException;

    BaseResponseSAP notificationCreate(NotificationCreateSAPRequest input) throws SAPApiException;

    BaseResponseSAP equipmentMeasPoint(PostMeasPointSAPRequest input) throws SAPApiException;
}

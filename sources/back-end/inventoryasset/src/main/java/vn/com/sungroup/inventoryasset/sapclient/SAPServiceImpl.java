package vn.com.sungroup.inventoryasset.sapclient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.com.sungroup.inventoryasset.dto.User;
import vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.MeasPointSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.sap.PostMeasPointSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCloseSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationCreateSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationDetailSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.notification.sapRequest.NotificationSearchSAPRequest;
import vn.com.sungroup.inventoryasset.dto.request.sap.notification.metric.NotificationMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationDetailRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationSearchRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeStatusRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.confirmation.OperationConfirmationRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric.WorkOrderMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post.WorkOrderCreateRequestSAP;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.dto.response.equipment.sap.MeasPointSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationCloseSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationDetailSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationMetricResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationSearchSAPResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse.OperationMetricsSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeStatusResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.entity.ActivityType;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.repository.ActivityTypeRepository;
import vn.com.sungroup.inventoryasset.repository.OperationDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.OperationHistoryRepository;
import vn.com.sungroup.inventoryasset.repository.WorkCenterRepository;
import vn.com.sungroup.inventoryasset.repository.common.CommonStatusRepository;
import vn.com.sungroup.inventoryasset.repository.common.ControlKeyRepository;
import vn.com.sungroup.inventoryasset.repository.common.VarianceReasonRepository;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.SEND_REQUEST_TO_SAP_ERROR;
import static vn.com.sungroup.inventoryasset.sapclient.constants.SAPEndpoints.*;

@RequiredArgsConstructor
@Service
@Slf4j
public class SAPServiceImpl implements SAPService {
    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)"
                    + " Chrome/54.0.2840.99 Safari/537";
    private final OperationDocumentRepository operationDocumentRepository;
    private final RestTemplate sapRestTemplate;
    private final ParseUtils parseUtils;
    private final SAPUtils sapUtils;
    private final CommonStatusRepository commonStatusRepository;
    private final OperationHistoryRepository operationHistoryRepository;
    private final ActivityTypeRepository activityTypeRepository;
    private final ControlKeyRepository controlKeyRepository;
    private final WorkCenterRepository workCenterRepository;
    private final VarianceReasonRepository varianceReasonRepository;
    private final RestTemplate restTemplate;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Value("${external.rest.consume.SAP_HCM_USER}")
    private String sapUser;

    @Value("${external.rest.consume.SAP_HCM_PASS}")
    private String sapPassword;

    @Override
    public MeasPointSAPResponse getEquipmentMeasPoint(MeasPointSAPRequest request)
            throws SAPApiException {

        return callToSAPService(request, GET_MEAS_POINT, MeasPointSAPResponse.class);
    }

    @Override
    public WorkOrderDetailsSAP getWorkOrder(WorkOrderDetailsRequest request)
            throws SAPApiException {
        return callToSAPService(request,WORK_ORDER_DETAILS,WorkOrderDetailsSAP.class);
    }

    @Override
    public WorkOrderSearchSAP getWorkOrders(WorkOrderSearchRequestSAP request)
            throws SAPApiException {
        var result =  callToSAPService(request, WORK_ORDERS, WorkOrderSearchSAP.class);
        if (result.getData() == null) result.setData(new ArrayList<>());
        return result;
    }

    @Override
    public WorkOrderChangeResponseSAP changeWorkOrder(WorkOrderChangeRequestSAP request)
            throws SAPApiException {
        return callToSAPService(request, WORK_ORDER_CHANGE, WorkOrderChangeResponseSAP.class);
    }

    @Override
    public WorkOrderChangeStatusResponseSAP changeWorkOrderStatus(WorkOrderChangeStatusRequestSAP changeStatusRequest) throws SAPApiException {
        return callToSAPService(changeStatusRequest, WORK_ORDER_CHANGE_STATUS, WorkOrderChangeStatusResponseSAP.class);
    }

    @Override
    public OperationDetailResponse getOperationDetail(OperationDetailRequestSAP request)
            throws SAPApiException {
        var result = callToSAPService(request,OPERATION_DETAIL, OperationDetailResponse.class);

        if (result.getOperation() == null) return result;

        var status = commonStatusRepository.findByStatusAndCode(result.getOperation().getOperationSystemStatus(), CommonStatusCodeConsts.Operation).orElse(null);
        result.getOperation().setCommonStatus(status);
        var operationDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(result.getOperation().getWorkOrderId(), result.getOperation().getOperationId(), "");
        result.getOperation().setDocuments(operationDocuments);

        var operationHistory = operationHistoryRepository.getLatestByOperation(result.getOperation().getWorkOrderId(), result.getOperation().getOperationId(), "");
        if(operationHistory != null){
            result.getOperation().setOperationHistory(operationHistory.toOperationHistoryResponse());
        }

        var activityType = getActivityType(result.getOperation().getWorkCenterCode(),
                result.getOperation().getActivityType(),
                result.getOperation().getMaintenancePlantCode());
        result.getOperation().setActivityTypeData(activityType);

        var controlKey = controlKeyRepository.findByCode(result.getOperation().getControlKey()).orElse(null);
        result.getOperation().setControlKeyData(controlKey);

        if (result.getSubOperation() != null && result.getSubOperation().getItem() != null &&
                !result.getSubOperation().getItem().isEmpty()) {
            result.getSubOperation().getItem().forEach(subOperationDetailItem -> {
                var controlKeySubOperation = controlKeyRepository.findByCode(subOperationDetailItem.getControlKey()).orElse(null);
                subOperationDetailItem.setControlKeyData(controlKeySubOperation);

                var varianceReason = varianceReasonRepository.findByDescription(subOperationDetailItem.getResult()).orElse(null);
                subOperationDetailItem.setVarianceReason(varianceReason);

                var subOperationDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(subOperationDetailItem.getWorkOrderId(), subOperationDetailItem.getOperationId(), subOperationDetailItem.getSuperOperationId());
                subOperationDetailItem.setDocuments(subOperationDocuments);

                var statusSub = commonStatusRepository.findByStatusAndCode(subOperationDetailItem.getOperationSystemStatus(), CommonStatusCodeConsts.Operation).orElse(null);
                subOperationDetailItem.setCommonStatus(statusSub);

                var subOperationHistory = operationHistoryRepository.getLatestByOperation(subOperationDetailItem.getWorkOrderId(), subOperationDetailItem.getOperationId(), subOperationDetailItem.getSuperOperationId());
                if(subOperationHistory != null){
                    subOperationDetailItem.setOperationHistory(subOperationHistory.toOperationHistoryResponse());
                }
            });
        }

        var varianceReason = varianceReasonRepository.findByDescription(result.getOperation().getResult()).orElse(null);
        result.getOperation().setVarianceReason(varianceReason);

        return result;
    }

    private ActivityType getActivityType(String workCenterCode, String activityTypeCode, String plantCode) {
        var workCenter = workCenterRepository.findByCodeAndMaintenancePlantId(workCenterCode, plantCode).orElse(null);
        if (workCenter == null) return null;
        return activityTypeRepository.findByTypeAndCostCenter(activityTypeCode, workCenter.getCostCenterCode()).orElse(null);
    }

    @Override
    public SubOperationDetailResponse getSubOperationDetail(OperationDetailRequestSAP request)
            throws SAPApiException {
        var result = callToSAPService(request, SUB_OPERATION_DETAIL, SubOperationDetailResponse.class);
        if (result.getSubOperation() != null) {
            for (var item : result.getSubOperation().getItem()) {
                var status = commonStatusRepository.findByStatusAndCode(item.getOperationSystemStatus(), CommonStatusCodeConsts.Operation).orElse(null);
                item.setCommonStatus(status);
                var operationDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(item.getWorkOrderId(), item.getOperationId(), item.getSuperOperationId());
                item.setDocuments(operationDocuments);
                var operationHistory = operationHistoryRepository.getLatestByOperation(item.getWorkOrderId(), item.getOperationId(), item.getSuperOperationId());
                if(operationHistory != null){
                    item.setOperationHistory(operationHistory.toOperationHistoryResponse());
                }
                var controlKeySubOperation = controlKeyRepository.findByCode(item.getControlKey()).orElse(null);
                item.setControlKeyData(controlKeySubOperation);
                var varianceReason = varianceReasonRepository.findByDescription(item.getResult()).orElse(null);
                item.setVarianceReason(varianceReason);
            }
        }

        return result;
    }

    @Override
    public OperationSearchResponse searchOperation(OperationSearchRequestSAP request)
            throws SAPApiException {
        var result = callToSAPService(request,OPERATION_SEARCH, OperationSearchResponse.class);

        if (result.getOperation() == null) {
            result.setOperation(new OperationSearchDetail());
            result.setTotalPage(0);

            result.getOperation().setItem(new ArrayList<>());
        }

        for (var item : result.getOperation().getItem()) {
            var status = commonStatusRepository.findByStatusAndCode(item.getOperationSystemStatus(), CommonStatusCodeConsts.Operation).orElse(null);
            item.setCommonStatus(status);
            var operationDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(item.getWorkOrderId(), item.getOperationId(), "");
            item.setDocuments(operationDocuments);
            var operationHistory = operationHistoryRepository.getLatestByOperation(item.getWorkOrderId(), item.getOperationId(), item.getSuperOperationId());
            if(operationHistory != null){
                item.setOperationHistory(operationHistory.toOperationHistoryResponse());
            }
            var varianceReason = varianceReasonRepository.findByDescription(item.getResult()).orElse(null);
            item.setVarianceReason(varianceReason);
        }

        return result;
    }

    @Override
    public OperationConfirmationResponse confirmation(OperationConfirmationRequestSAP request)
            throws SAPApiException {
        return callToSAPService(request, CONFIRMATION_POST, OperationConfirmationResponse.class);
    }

    @Override
    public OperationMeasuresSAP getOperationMeasures(OperationMeasuresRequestSAP request)
            throws SAPApiException {
        return callToSAPService(request, OPERATION_MEASURE, OperationMeasuresSAP.class);
    }

    @Override
    public OperationMetricsSAP getOperationMetrics(OperationMetricsRequestSAP request)
            throws SAPApiException {
        return callToSAPService(request, OPERATION_METRIC, OperationMetricsSAP.class);
    }

    @Override
    public OperationComponentResponse getOperationComponent(
            OperationDetailRequestSAP request) throws SAPApiException {
        return callToSAPService(request, OPERATION_COMPONENT, OperationComponentResponse.class);
    }

    @Override
    public BaseResponseSAP changeOperation(OperationChangeRequestSAP request) throws SAPApiException {
        return callToSAPService(request, OPERATION_CHANGE, BaseResponseSAP.class);
    }

    @Override
    public WorkOrderMetricResponse getWorkOrderMetric(WorkOrderMetricRequestSAP request)
            throws SAPApiException {
        return callToSAPService(request, WORK_ORDER_METRIC, WorkOrderMetricResponse.class);
    }

    @Override
    public NotificationMetricResponse getNotificationMetric(NotificationMetricRequestSAP request) throws SAPApiException {
        return callToSAPService(request, NOTIFICATION_METRIC, NotificationMetricResponse.class);
    }

    @Override
    public MaterialDocumentResponse createMaterialDocument(MaterialDocumentRequestSAP request) throws SAPApiException {
        return callToSAPService(request, MATERIAL_DOCUMENT_CREAT, MaterialDocumentResponse.class);
    }

    @Override
    public BaseResponseSAP createWorkOrder(WorkOrderCreateRequestSAP input) throws SAPApiException {
        return callToSAPService(input, WORK_ORDER_CREATE, BaseResponseSAP.class);
    }

    @Override
    public User getUserInfo(String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        // TODO token confirming
        if (token.isEmpty()) {
            var currentUser = SecurityUtil.getCurrentUser();
            httpHeaders.setBearerAuth(currentUser.getToken());
        } else {
            httpHeaders.set("Authorization", token);
        }
        HttpEntity<Object> entity = new HttpEntity<>(httpHeaders);

        ResponseEntity<User> response = restTemplate.exchange(
                "https://devmysgr.sungroup.com.vn/api/v1/cos/authorization/users/user-info", HttpMethod.GET, entity,
                User.class);

        return response.getBody();
    }

    @Override
    public NotificationSearchSAPResponse notificationSearch(NotificationSearchSAPRequest input) throws SAPApiException {

        return callToSAPService(input, NOTIFICATION_SEARCH, NotificationSearchSAPResponse.class);
    }

    @Override
    public NotificationCloseSAPResponse notificationClose(NotificationCloseSAPRequest input) throws SAPApiException {
        return callToSAPService(input, NOTIFICATION_CLOSE, NotificationCloseSAPResponse.class);
    }

    @Override
    public NotificationDetailSAPResponse notificationDetail(NotificationDetailSAPRequest input) throws SAPApiException {
        return callToSAPService(input, NOTIFICATION_DETAIL, NotificationDetailSAPResponse.class);
    }
    @Override
    public BaseResponseSAP notificationChange(NotificationCreateSAPRequest input) throws SAPApiException {
        return callToSAPService(input, NOTIFICATION_CHANGE, BaseResponseSAP.class);
    }

    @Override
    public BaseResponseSAP notificationCreate(NotificationCreateSAPRequest input) throws SAPApiException {
        return callToSAPService(input,NOTIFICATION_POST, BaseResponseSAP.class);
    }

    @Override
    public BaseResponseSAP equipmentMeasPoint(PostMeasPointSAPRequest input) throws SAPApiException {
        return callToSAPService(input,MEASURING_POINT_POST, BaseResponseSAP.class);
    }

    private <TRequest, TResponse extends ErrorResponseSAP> TResponse callToSAPService(TRequest request, String requestUrl,
                                                                                      Class<TResponse> responseClazz) throws SAPApiException {

        log.info("send data to SAP: {} {}",request.getClass().getName(),  parseUtils.toJsonString(request));
        var credential = Base64.getEncoder().encodeToString((sapUser + ":" + sapPassword).getBytes());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBasicAuth(credential);
        httpHeaders.set("User-Agent", USER_AGENT);
        HttpEntity<Object> entity = new HttpEntity<>(request, httpHeaders);

        ResponseEntity<TResponse> response = sapRestTemplate.exchange(
                requestUrl, HttpMethod.POST, entity,
                responseClazz);

        TResponse result = response.getBody();

        var message = MessageUtil.getMessage(messageSource,locale, SEND_REQUEST_TO_SAP_ERROR);
        sapUtils.validateSAPBodyResponse(result, message);
        return result;
    }
}

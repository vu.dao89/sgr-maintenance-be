package vn.com.sungroup.inventoryasset.sapclient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.sap.ErrorResponseSAP;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

import java.util.Objects;

@RequiredArgsConstructor
@Service
@Slf4j
public class SAPUtils {
    public static final String RESPONSE_SUCCESS_CODE = "S";
    private static final String RESPONSE_ERROR_CODE = "E";
    private static final String DATA_NOT_FOUND = "Không tìm th";

    public static final String SAP_DATA_NULL = "SAP_DATA_NULL";
    public static final String SAP_WORK_ORDER_ERROR = "SAP_WORK_ORDER_ERROR";
    public static final String SAP_NOTIFICATION_ID_NOT_FOUND = "SAP_NOTIFICATION_ID_NOT_FOUND";
    public static final String WORK_ORDER_GET_DETAIL_ERROR = "WORK_ORDER_GET_DETAIL_ERROR";
    public static final String POST_WORK_ORDER_ERROR = "POST_WORK_ORDER_ERROR";
    public static final String GET_OPERATION_DETAIL_ERROR = "GET_OPERATION_DETAIL_ERROR";
    public static final String GET_SUB_OPERATION_DETAIL_ERROR = "GET_SUB_OPERATION_DETAIL_ERROR";
    public static final String SEARCH_OPERATION_ERROR = "SEARCH_OPERATION_ERROR";
    public static final String POST_CONFIRM_OPERATION_ERROR = "POST_CONFIRM_OPERATION_ERROR";
    public static final String GET_OPERATION_MEASURE_ERROR = "GET_OPERATION_MEASURE_ERROR";
    public static final String GET_OPERATION_METRIC_ERROR = "GET_OPERATION_METRIC_ERROR";
    public static final String GET_OPERATION_COMPONENT_ERROR = "GET_OPERATION_COMPONENT_ERROR";
    public static final String CHANGE_OPERATION_ERROR = "CHANGE_OPERATION_ERROR";
    public static final String SEND_REQUEST_TO_SAP_ERROR = "SEND_REQUEST_TO_SAP_ERROR";

    public <T extends ErrorResponseSAP> void validateSAPBodyResponse(T result, String errorMessage)
            throws SAPApiException {
        if (result == null) {
            log.error(errorMessage);
            throw new SAPApiException(errorMessage, HttpStatus.NO_CONTENT);
        }

        if (Objects.equals(result.getCode(), RESPONSE_ERROR_CODE)) {
            log.error(errorMessage);
            throw new SAPApiException(result.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public <T extends ErrorResponseSAP> boolean isNotFoundData(T result) throws SAPApiException {
        if (result == null) {
            log.error("The data from SAP api is null.");
            throw new SAPApiException("The data from SAP api is null.", HttpStatus.NO_CONTENT);
        }

        if (Objects.equals(result.getCode(), RESPONSE_ERROR_CODE) &&
                result.getMessage().contains(DATA_NOT_FOUND)) {
            return true;
        }

        return false;
    }
}

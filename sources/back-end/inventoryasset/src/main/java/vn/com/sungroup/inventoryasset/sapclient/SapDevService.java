package vn.com.sungroup.inventoryasset.sapclient;

import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.com.sungroup.inventoryasset.dto.response.employee.EmployeeResponse;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

@Service
@RequiredArgsConstructor
public class SapDevService {

  private final RestTemplate restTemplate;

  public EmployeeResponse getEmployee() {

    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    // TODO token confirming
    var currentUser = SecurityUtil.getCurrentUser();
    httpHeaders.setBearerAuth(currentUser.getToken());
    HttpEntity<Object> entity = new HttpEntity<>(httpHeaders);

    ResponseEntity<EmployeeResponse> response = restTemplate.exchange(
        "https://devmysgr.sungroup.com.vn/api/v1/cos/masterdata/employees", HttpMethod.GET, entity,
        EmployeeResponse.class);

    return response.getBody();
  }
}

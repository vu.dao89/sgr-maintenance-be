package vn.com.sungroup.inventoryasset.sapclient.config;

import java.time.Duration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriTemplateHandler;

@Configuration
public class SAPClient {
  @Value("${external.rest.consume.SAP_HCM}")
  private String sapUrl;

  public static final Integer REST_TIMEOUT = 12000;

  @Bean
  public RestTemplate sapRestTemplate(UriTemplateHandler sapUriTemplateHandler) {
    RestTemplate rmgRestTemplate = new RestTemplateBuilder()
        .setConnectTimeout(Duration.ofMillis(REST_TIMEOUT))
        .setReadTimeout(Duration.ofMillis(0))
        .build();
    rmgRestTemplate.setUriTemplateHandler(sapUriTemplateHandler);

    return rmgRestTemplate;
  }

  @Bean
  public UriTemplateHandler sapUriTemplateHandler() {
    return new DefaultUriBuilderFactory(sapUrl);
  }

}
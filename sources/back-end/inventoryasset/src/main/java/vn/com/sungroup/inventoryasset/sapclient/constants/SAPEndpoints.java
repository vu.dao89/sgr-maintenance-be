package vn.com.sungroup.inventoryasset.sapclient.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SAPEndpoints {

  public static final String WORK_ORDER_DETAILS = "/equipment/post/wo-detail";

  public static final String WORK_ORDER_CHANGE = "/equipment/post/wo-change";
  public static final String WORK_ORDER_CREATE= "/equipment/post/wo-post";
  public static final String WORK_ORDER_CHANGE_STATUS = "/equipment/post/wo-complete";

  public static final String CONFIRMATION_POST = "/equipment/post/confirm-post";

  public static final String OPERATION_DETAIL = "/equipment/get/operation-detail";
  public static final String GET_MEAS_POINT = "/equipment/get/meas-point";
  public static final String SUB_OPERATION_DETAIL = "/equipment/get/operation-sub";
  public static final String OPERATION_SEARCH = "/equipment/get/operation-search";

  public static final String OPERATION_COMPONENT = "/equipment/get/operation-component";
  public static final String OPERATION_CHANGE= "/equipment/post/oper-change";
  public static final String MATERIAL_DOCUMENT_CREAT= "/equipment/post/matdoc";

  public static final String WORK_ORDERS = "/equipment/get/workorder-search";

  public static final String WORK_ORDER_METRIC = "/equipment/get/workorder-metric";

  public static final String OPERATION_MEASURE = "/equipment/get/operation-meas";

  public static final String OPERATION_METRIC = "/equipment/get/operation-metric";

  public static final String NOTIFICATION_METRIC = "/equipment/get/notification-metric";
  public static final String NOTIFICATION_SEARCH = "/equipment/get/notification-search";
  public static final String NOTIFICATION_CLOSE = "/equipment/post/notification-close";
  public static final String NOTIFICATION_DETAIL = "/equipment/get/notification";
  public static final String NOTIFICATION_CHANGE = "/equipment/post/notification-change";
  public static final String NOTIFICATION_POST = "/equipment/post/notification-post";
  public static final String MEASURING_POINT_POST = "/equipment/post/meas-point";
}

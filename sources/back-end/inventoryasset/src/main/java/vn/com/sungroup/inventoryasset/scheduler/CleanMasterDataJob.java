package vn.com.sungroup.inventoryasset.scheduler;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vn.com.sungroup.inventoryasset.entity.MasterDataProcessedHistory;
import vn.com.sungroup.inventoryasset.service.MasterDataProcessedHistoryService;
import vn.com.sungroup.inventoryasset.service.impl.RaygunServiceImpl;
import vn.com.sungroup.inventoryasset.service.impl.SftpServiceImpl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CleanMasterDataJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(CleanMasterDataJob.class);

    private final RaygunServiceImpl raygunService;
    private final SftpServiceImpl sftpService;
    private final MasterDataProcessedHistoryService masterDataProcessedHistoryService;
    @Scheduled(initialDelay = 1000, fixedDelay = Long.MAX_VALUE)
    public void triggerOnStartup() {
        var message = "Trigger CleanMasterDataJob on startup at " + LocalDateTime.now(ZoneOffset.UTC);
        LOGGER.info(message);
        Sentry.captureMessage(message, SentryLevel.INFO);
        raygunService.sendInfo(message);
        cleanMasterDataHistories();

        message = "CleanMasterDataHistories success at " + LocalDateTime.now(ZoneOffset.UTC);
        Sentry.captureMessage(message, SentryLevel.INFO);
        raygunService.sendInfo(message);
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void cleanMasterDataHistories(){
        var message = "CleanMasterData job run at " + LocalDateTime.now(ZoneOffset.UTC);
        LOGGER.info(message);
        Sentry.captureMessage(message, SentryLevel.INFO);
        raygunService.sendInfo(message);

        var masterData = masterDataProcessedHistoryService.findAll().stream().filter(x->x.getBackedupAt() == null).collect(Collectors.toList());
        var groups =  masterData.stream().collect(Collectors.groupingBy(MasterDataProcessedHistory::getFolderName));
        for (var group : groups.entrySet()){
            var sourceFolder = group.getKey();
            var entities = group.getValue();
            var values = entities.stream().map(MasterDataProcessedHistory::getFileName).collect(Collectors.toList());
            sftpService.moveFiles(sourceFolder,values);

            message ="Moved files in folder: " + sourceFolder;
            LOGGER.info(message);
            Sentry.captureMessage(message, SentryLevel.INFO);
            raygunService.sendInfo(message);
            masterDataProcessedHistoryService.saveBackedupFiles(entities);
        }
        message = "CleanMasterData job run at " + LocalDateTime.now(ZoneOffset.UTC);
        LOGGER.info(message);
        Sentry.captureMessage(message, SentryLevel.INFO);
        raygunService.sendInfo(message);
    }
}

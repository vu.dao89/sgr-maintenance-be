package vn.com.sungroup.inventoryasset.scheduler;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vn.com.sungroup.inventoryasset.service.SyncMasterDataService;
import vn.com.sungroup.inventoryasset.service.impl.RaygunServiceImpl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Component
@RequiredArgsConstructor
public class SyncMasterDataScheduler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncMasterDataScheduler.class);

  private final SyncMasterDataService syncMasterDataService;
  private final RaygunServiceImpl raygunService;
  @Scheduled(initialDelay = 1000, fixedDelay = Long.MAX_VALUE)
  public void triggerOnStartup() {
    var message = "Trigger on startup at " + LocalDateTime.now(ZoneOffset.UTC);
//    LOGGER.info(message);
//    Sentry.captureMessage(message, SentryLevel.INFO);
//    raygunService.sendInfo(message);
    syncMasterDataFromSftpToDB();
    message = "SyncMasterDataFromSftpToDB success at " + LocalDateTime.now(ZoneOffset.UTC);
    LOGGER.info(message);
    Sentry.captureMessage(message, SentryLevel.INFO);
    raygunService.sendInfo(message);
  }

  @Scheduled(cron = "${app.sync-data.cron:0 0 0 * * *}")
  public void syncMasterDataFromSftpToDB() {
    var message = "Trigger syncMasterData at " + LocalDateTime.now(ZoneOffset.UTC);
    LOGGER.info(message);
//    Sentry.captureMessage(message, SentryLevel.INFO);
//    raygunService.sendInfo(message);

    syncMasterDataService.syncMasterData();

    message = "SyncMasterData success at " + LocalDateTime.now(ZoneOffset.UTC);
    Sentry.captureMessage(message, SentryLevel.INFO);
    raygunService.sendInfo(message);
  }

}

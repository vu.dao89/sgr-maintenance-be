package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.activitykey.ActivityKeyDto;
import vn.com.sungroup.inventoryasset.dto.request.ActivityKeyRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityKey;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface ActivityKeyService {

  Page<ActivityKey> getActivityKeys(Pageable pageable, ActivityKeyRequest input) throws InvalidSortPropertyException;

  ActivityKey save(ActivityKeyDto activityKeyDto);

  List<ActivityKey> findAll();

}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.ActivityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityType;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.ActivityTypeMg;

public interface ActivityTypeService {

  void saveAll(List<ActivityTypeMg> activityTypes);

  List<ActivityTypeMg> findAll();

  Page<ActivityType> getActivityTypes(Pageable pageable, ActivityTypeRequest input) throws InvalidSortPropertyException;

}

package vn.com.sungroup.inventoryasset.service;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

/*
* Input D (Dto)
* */
public interface BaseService<Entity> {

    List<Entity> findAll();

    Page<Entity> findAll(Pageable pageable) throws InvalidSortPropertyException;

    Optional<Entity> findOne(UUID id);

    void saveAll(List<Entity> entities);

    void deleteAll(List<Entity> entities);

    void updateAll(List<Entity> entities);

}

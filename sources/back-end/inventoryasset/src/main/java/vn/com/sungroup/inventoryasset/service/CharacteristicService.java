package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.CharacteristicsRequest;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface CharacteristicService {

  Page<Characteristic> getCharacteristics(Pageable pageable, CharacteristicsRequest input) throws InvalidSortPropertyException;

  List<Characteristic> processSaveCharacteristics(List<Characteristic> toCharacteristicEntityList);
  List<Characteristic> findAll();
}

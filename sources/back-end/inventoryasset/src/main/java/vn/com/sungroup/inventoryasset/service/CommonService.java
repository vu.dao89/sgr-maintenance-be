package vn.com.sungroup.inventoryasset.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.*;
import vn.com.sungroup.inventoryasset.entity.ControlKey;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

public interface CommonService {


    Page<ControlKey> getControlKeys(Pageable pageable, ControlKeyRequest input) throws InvalidSortPropertyException;

    ControlKey getControlKey(String code) throws MasterDataNotFoundException;

    Page<CommonStatus> getCommonStatusByFilter(CommonStatusRequest request, Pageable pageable)
            throws InvalidSortPropertyException;

    Page<PriorityType> getPriorityTypeByFilter(PriorityTypeRequest request, Pageable pageable)
            throws InvalidSortPropertyException;

    Page<VarianceReason> getVarianceReasons(Pageable pageable, VarianceReasonRequest request) throws InvalidSortPropertyException;

    VarianceReason getVarianceReasonByCode(String code) throws MasterDataNotFoundException;

    Page<SystemStatus> getSystemStatus(Pageable pageable, SystemStatusRequest request) throws InvalidSortPropertyException;

    Optional<CommonStatus> findByStatusAndCode(String status, String workOrder);
}

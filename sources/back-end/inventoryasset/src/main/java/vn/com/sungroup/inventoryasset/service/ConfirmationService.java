package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

public interface ConfirmationService {

  OperationConfirmationResponse confirmToSap(OperationConfirmationRequest request)
      throws SAPApiException;
}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.entity.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

  void saveAll(List<Employee> employees);
  List<Employee> getAll();
  Page<Employee> getAll(Pageable pageable);
  Optional<Employee> getByEmployeeId(String employeeId);

  Optional<Employee> getByPrimaryId(String primaryId);
}

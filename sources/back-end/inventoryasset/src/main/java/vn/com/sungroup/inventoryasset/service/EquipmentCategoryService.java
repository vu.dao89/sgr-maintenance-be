package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentCategoryRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.EquipmentCategoryMg;

public interface EquipmentCategoryService {

    EquipmentCategory getByCode(String code) throws MasterDataNotFoundException;

    List<EquipmentCategoryMg> findAll();

    void saveAll(List<EquipmentCategoryMg> equipmentCategoryList);

    Page<EquipmentCategory> findEquipmentCategoryByFilter(EquipmentCategoryRequest request, Pageable pageable)
        throws InvalidSortPropertyException;

}

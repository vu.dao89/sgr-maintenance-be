package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentDocument;

import java.util.List;

@Service
@Transactional
public interface EquipmentDocumentService extends BaseService<EquipmentDocument>{
    Page<EquipmentDocument> findAllByEquipmentCode(EquipmentDocumentRequest request, Pageable pageable);

    List<EquipmentDocument> findAllByEquipmentCode(String equipmentCode);
    EquipmentDocument save(EquipmentDocumentCreate input);
}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDto;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.GetMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.response.equipment.EquipmentResponse;
import vn.com.sungroup.inventoryasset.dto.response.equipment.sap.MeasPointSAPResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.Equipment;
import vn.com.sungroup.inventoryasset.exception.*;

import java.util.Collection;
import java.util.List;

public interface EquipmentService {
    EquipmentDto processSave(EquipmentDto equipmentDTO);


    MeasPointSAPResponse getEquipmentMeasPoint(GetMeasPointRequest measPoint)
        throws SAPApiException;

    BaseResponseSAP postEquipmentMeasPoint(PostMeasPointRequest postMeasPointRequest) throws InvalidInputException, SAPApiException;


    Page<EquipmentResponse> getEquipments(Pageable pageable, EquipmentRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;

    Equipment getByQrCode(String qrCode) throws InvalidSortPropertyException;

    Collection<TreeNode<EquipmentHierarchyResponse>> getEquipmentHierarchy(String equipmentId);

    Equipment getByEquipmentId(String equiId) throws MasterDataNotFoundException, SAPApiException;

    List<Equipment> findAll();
}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.equipment.FunctionalLocationDto;
import vn.com.sungroup.inventoryasset.dto.functionallocation.FunctionalLocationHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.dto.request.FunctionalLocationRequest;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.util.Collection;
import java.util.List;

public interface FuncLocationService {

    void saveAll(List<FunctionalLocationDto> functionalLocationDtos);

    void processSave(FunctionalLocationDto functionalLocationDto);

    Collection<TreeNode<FunctionalLocationHierarchyResponse>> getFunctionalLocationHierarchy(
            String functionalLocationId);

    FunctionalLocation getByQrCode(String qrCode) throws InvalidSortPropertyException;

    ;

    Page<FunctionalLocation> getFunctionalLocationByFilter(FunctionalLocationRequest request, Pageable pageable)
            throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;

    FunctionalLocation getByCode(String code) throws MasterDataNotFoundException;

    List<FunctionalLocation> findAll();
}

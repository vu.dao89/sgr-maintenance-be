package vn.com.sungroup.inventoryasset.service;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;

import java.util.Optional;
import java.util.UUID;
import vn.com.sungroup.inventoryasset.exception.DocumentNotFoundException;

public interface GlobalDocumentService extends BaseService<GlobalDocument>{
    Optional<GlobalDocument> findByName(String fileName);
    GlobalDocument getDocument(UUID id) throws DocumentNotFoundException;
    GlobalDocument uploadFile(GlobalDocumentUploadRequest request) throws Exception;
    void delete(UUID id);
}

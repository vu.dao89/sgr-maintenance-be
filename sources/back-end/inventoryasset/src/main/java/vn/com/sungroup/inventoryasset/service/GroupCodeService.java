package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.groupcode.GroupCodeDto;
import vn.com.sungroup.inventoryasset.dto.request.CodeGroupRequest;
import vn.com.sungroup.inventoryasset.dto.request.CodeRequest;
import vn.com.sungroup.inventoryasset.dto.response.groupcode.CodeGroupResponse;
import vn.com.sungroup.inventoryasset.entity.GroupCode;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface GroupCodeService {

  void save(GroupCodeDto groupCode);
  List<GroupCode> findAll();

  Page<CodeGroupResponse> getCodeGroups(Pageable pageable, CodeGroupRequest request) throws InvalidSortPropertyException;;
  Page<GroupCode> getCodes(Pageable pageable, CodeRequest request) throws InvalidSortPropertyException;;
}

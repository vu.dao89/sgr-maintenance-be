/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.dto.request.AssetDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetGetCostcenterRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryChgRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetListRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetModifyRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetSearchRequest;
import vn.com.sungroup.inventoryasset.dto.response.AssetDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetGetCostcenterResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryChgResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetListResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetModifyResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.MoveFileResponse;

/**
 * InventoryAssetService class.
 *
 * <p>Description InventoryAsset
 */
public interface InventoryAssetService {

    /**
     * assetList Service.
     *
     * @param input
     * @throws Exception
     */
    public AssetListResponse assetList(AssetListRequest input) throws Exception;

    /**
     * assetSearch Service.
     *
     * @param input
     * @return AssetListResponse
     * @throws Exception
     */
    public AssetSearchResponse assetSearch(AssetSearchRequest input) throws Exception;

    /**
     * assetDetail Service.
     *
     * @param input
     * @return AssetSearchResponse
     * @throws Exception
     */
    public AssetDetailResponse assetDetail(AssetDetailRequest input) throws Exception;

    /**
     * assetModify Service.
     *
     * @param input
     * @return AssetDetailResponse
     * @throws Exception
     */
    public AssetModifyResponse assetModify(AssetModifyRequest input) throws Exception;

    /**
     * assetGetCostcenter Service.
     *
     * @param input
     * @return MoveFileResponse
     * @throws Exception
     */
    public AssetGetCostcenterResponse assetGetCostcenter(AssetGetCostcenterRequest input)
            throws Exception;
    /**
     * assetInventory Service.
     *
     * @param input
     * @return AssetGetCostcenterResponse
     * @throws Exception
     */
    public AssetInventoryResponse assetInventory(AssetInventoryRequest input) throws Exception;

    /**
     * assetHistoryInventory Service.
     *
     * @param input
     * @return AssetInventoryResponse
     * @throws Exception
     */
    public AssetHistoryInventoryResponse assetHistoryInventory(AssetHistoryInventoryRequest input)
            throws Exception;
    /**
     * assetHistoryChg Service.
     *
     * @param input
     * @return AssetHistoryInventoryResponse
     * @throws Exception
     */
    public AssetHistoryChgResponse assetHistoryChg(AssetHistoryChgRequest input) throws Exception;
}

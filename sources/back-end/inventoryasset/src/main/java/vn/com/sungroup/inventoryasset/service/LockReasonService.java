package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.entity.LockReason;

public interface LockReasonService {
    Page<LockReason> findAll(Pageable pageable);
}

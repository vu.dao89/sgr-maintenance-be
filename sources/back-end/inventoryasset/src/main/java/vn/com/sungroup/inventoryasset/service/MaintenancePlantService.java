package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.MaintenancePlantRequest;
import vn.com.sungroup.inventoryasset.entity.MaintenancePlant;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.util.List;

public interface MaintenancePlantService {

    MaintenancePlant getByCode(String code) throws MasterDataNotFoundException;
    void save(MaintenancePlant maintenancePlantDto);
    List<MaintenancePlant> getAll();

    Page<MaintenancePlant> getMaintenancePlants(Pageable pageable, MaintenancePlantRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;
}

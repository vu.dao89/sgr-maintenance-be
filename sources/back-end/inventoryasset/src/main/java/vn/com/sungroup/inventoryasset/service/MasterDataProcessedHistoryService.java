package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.entity.MasterDataProcessedHistory;

import java.util.List;

public interface MasterDataProcessedHistoryService extends BaseService<MasterDataProcessedHistory> {
    void saveProcessedFiles(String folderName,List<String> fileInfos);
    void saveBackedupFiles(List<MasterDataProcessedHistory> entities);
    void saveBackedupFile(MasterDataProcessedHistory entity);
}

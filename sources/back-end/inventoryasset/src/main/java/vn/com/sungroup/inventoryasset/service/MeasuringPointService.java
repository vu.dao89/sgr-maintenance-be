package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;

import java.util.List;
import java.util.UUID;

/**
 * Service Interface for managing {@link vn.com.sungroup.inventoryasset.entity.MeasuringPoint}.
 */
public interface MeasuringPointService {

    /**
     * Delete the "id" measurepoint.
     *
     * @param id the id of the entity.
     */
    void delete(UUID id);

    List<MeasuringPoint> processSaveMeasuringPoints(List<MeasuringPoint> toMeasuringPointEntityList);

    List<MeasuringPoint> findAll();
}

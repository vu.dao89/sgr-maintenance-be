package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public interface NotificationDocumentService  extends BaseService<NotificationDocument>{
    Page<NotificationDocument> findAllByNotificationCode(NotificationDocumentRequest request, Pageable pageable);
    List<NotificationDocument> findAllByNotificationCode(String notificationCode);
    NotificationDocument save(NotificationDocumentCreate input);

    BaseResponseSAP delete(String notificationId, UUID documentId);
}

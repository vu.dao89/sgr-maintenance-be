package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemDto;
import vn.com.sungroup.inventoryasset.dto.request.notification.*;
import vn.com.sungroup.inventoryasset.dto.request.notification.metric.NotificationMetricRequest;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationCloseResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationItemCreateResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

public interface NotificationService {

    /**
     * notificationSearch Service.
     *
     * @param input
     * @throws Exception
     */
    NotificationSearchResponse notificationSearch(Pageable pageable, NotificationSearchRequest input) throws Exception;

    /**
     * notificationSearch Service.
     *
     * @param input
     * @throws Exception
     */
    NotificationDetailResponse notificationDetail(NotificationDetailRequest input) throws Exception;

    /**
     * notificationCreate Service.
     *
     * @param input
     * @throws Exception
     */
    BaseResponseSAP notificationCreate(NotificationCreateRequest input) throws Exception;

    NotificationItemCreateResponse notificationItemCreate(NotificationItemDto input) throws Exception;
    BaseResponseSAP notificationItemUpdate(NotificationItemDto input) throws Exception;
    BaseResponseSAP notificationItemDelete(NotificationDeleteRequest input) throws Exception;

    /**
     * notificationClose Service.
     *
     * @param input
     * @throws Exception
     */
    NotificationCloseResponse notificationClose(NotificationCloseRequest input) throws Exception;

    /**
     * notificationChange Service.
     *
     * @param input
     * @throws Exception
     */
    BaseResponseSAP notificationChange(NotificationCreateRequest input, String notificationStatus) throws Exception;


    NotificationMetricResponse getNotificationMetricFromSAP(NotificationMetricRequest input)
            throws SAPApiException, InvalidInputException;
}

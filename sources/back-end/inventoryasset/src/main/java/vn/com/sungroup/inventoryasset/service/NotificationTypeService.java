package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationTypeDto;
import vn.com.sungroup.inventoryasset.dto.request.NotificationTypeRequest;
import vn.com.sungroup.inventoryasset.entity.NotificationType;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface NotificationTypeService {

    void save(NotificationTypeDto notificationType);
    List<NotificationType> findAll();

    Page<NotificationType> getNotificationTypeByFilter(NotificationTypeRequest request, Pageable pageable)
            throws InvalidSortPropertyException;

    NotificationType findByType(String code, String maintenancePlantCode) throws MasterDataNotFoundException;
}

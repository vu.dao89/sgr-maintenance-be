package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.ObjectTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ObjectType;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;

import java.util.List;

@Service
@Transactional
public interface ObjectTypeService {

  void saveAll(List<ObjectType> objectTypes);

  ObjectType save(ObjectType objectType);
  List<ObjectType> findAll();

  Page<ObjectType> getObjectTypeByFilter(ObjectTypeRequest request, Pageable pageable)
      throws InvalidSortPropertyException;
  ObjectType getByCode(String code) throws MasterDataNotFoundException;
}

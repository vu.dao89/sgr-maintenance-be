package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;

import java.util.List;

@Service
@Transactional
public interface OperationDocumentService extends BaseService<OperationDocument> {
    Page<OperationDocument> findAllByOperationCode(OperationDocumentRequest request, Pageable pageable);
    List<OperationDocument> findAllByWorkOrderIdAndOperationCode(OperationDocumentRequest request);
    OperationDocument save(OperationDocumentCreate input);

    BaseResponseSAP delete(String workOrderId, String operationId, String documentId, String superOperationId);
}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryRequest;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationDetailResponse;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

import java.util.List;

@Service
@Transactional
public interface OperationHistoryService extends BaseService<OperationHistory> {
    Page<OperationHistory> getByFilter(OperationHistoryRequest request, Pageable pageable) throws InvalidSortPropertyException;
    OperationHistory getCurrentActive();
    List<OperationHistory> getOperationsNotComplete(String employeeId);

    OperationHistory getStartingOperation(String employeeId);

    OperationDetailResponse collectOperationHistory(OperationHistory operationHistory) throws SAPApiException;

    OperationHistory saveOne(OperationHistory operationHistory);

    OperationHistory getOperationHistory(String workOrderId, String operationId,String activity);
}

package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryCreate;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryUpdate;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.*;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse.OperationMetricsSAP;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.exception.*;

import java.util.List;

public interface OperationService {

  OperationDetailResponse getDetailFromSap(OperationDetailRequest input) throws SAPApiException;

  SubOperationDetailResponse getSubDetailFromSap(OperationDetailRequest input)
      throws SAPApiException;

  OperationSearchResponse searchOperation(Pageable pageable, OperationSearchRequest input) throws SAPApiException, InvalidInputException;

  OperationMeasuresSAP getOperationMeasures(OperationMeasuresRequestSAP request)
      throws SAPApiException;

  OperationComponentResponse getOperationComponentFromSAP(OperationDetailRequest input)
      throws SAPApiException;

  OperationMetricsSAP getOperationMetrics(OperationMetricsRequest request) throws SAPApiException, InvalidInputException;
  BaseResponseSAP changeSubOperation(OperationChangeRequest request) throws SAPApiException, InvalidInputException;
  UpdateOperationsResponse createOperation(OperationCreateRequest request) throws SAPApiException, InvalidInputException;
  UpdateOperationsResponse updateOperation(OperationCreateRequest request) throws SAPApiException, InvalidInputException, OperationNotFoundException, MasterDataNotFoundException;
  BaseResponseSAP deleteOperation(OperationDeleteRequest request)
          throws SAPApiException, OperationNotFoundException, InvalidInputException, MasterDataNotFoundException;

  MaterialDocumentResponse createMaterialDocument(MaterialDocumentRequest materialDocumentRequest) throws SAPApiException, InvalidInputException;

  void changePersonnelOperationParent(OperationPersonnelChangeRequest request)
      throws SAPApiException, MissingRequiredFieldException, OperationNotFoundException;

  void changePersonnelOperationSub(OperationSubPersonnelChangeRequest request)
      throws MissingRequiredFieldException, SAPApiException, OperationNotFoundException;

  OperationHistoryResponse operationComplete(OperationHistoryUpdate input, List<MultipartFile> files) throws SAPApiException,
            InvalidInputException, MissingRequiredFieldException;

  boolean isCompletedOperationStatus(String subOperationStatus);

  OperationConfirmationResponse operationConfirmation(OperationConfirmationRequest input) throws MissingRequiredFieldException, SAPApiException;

  BaseResponseSAP postMeasuringPoint(PostMeasPointRequest measPoint) throws InvalidInputException, SAPApiException;

  OperationHistoryResponse operationStart(OperationHistoryCreate input) throws SAPApiException, InvalidInputException, ApplicationException;

  OperationHistoryResponse operationHold(OperationHistoryUpdate input, List<MultipartFile> files) throws SAPApiException,
          InvalidInputException;
}

package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.entity.Partner;

import java.util.List;
import java.util.UUID;

/**
 * Service Interface for managing {@link vn.com.sungroup.inventoryasset.entity.Partner}.
 */
public interface PartnerService {


    /**
     * Delete the "id" partner.
     *
     * @param id the id of the entity.
     */
    void delete(UUID id);

    List<Partner> processSavePartners(List<Partner> toPartnerEntityList);

    List<Partner> findAll();
}

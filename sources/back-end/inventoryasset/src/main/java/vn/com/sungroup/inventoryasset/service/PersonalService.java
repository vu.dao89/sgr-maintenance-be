package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.personal.PersonalDto;
import vn.com.sungroup.inventoryasset.dto.request.PersonnelRequest;
import vn.com.sungroup.inventoryasset.entity.Personnel;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.text.ParseException;
import java.util.List;

public interface PersonalService {

  Personnel getByCode(String code, String workCenterCode) throws MasterDataNotFoundException;

  Personnel save(PersonalDto personal) throws ParseException;
  List<Personnel> findAll();

  Page<Personnel> getByFilter(PersonnelRequest maintenancePlantId, Pageable pageable) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;
}

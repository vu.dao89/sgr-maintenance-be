package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.PlannerGroupRequest;
import vn.com.sungroup.inventoryasset.dto.plannerGroup.PlannerGroupDto;
import vn.com.sungroup.inventoryasset.entity.PlannerGroup;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.util.List;

public interface PlannerGroupService {
    void save(PlannerGroupDto plannerGroupDto);
    List<PlannerGroup> findAll();
    Page<PlannerGroup> getPlannerGroups(Pageable pageable, PlannerGroupRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;
}

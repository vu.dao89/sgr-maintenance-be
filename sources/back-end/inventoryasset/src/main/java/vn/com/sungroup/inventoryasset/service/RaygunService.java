package vn.com.sungroup.inventoryasset.service;

import java.io.IOException;

public interface RaygunService {
    void sendError(Exception exception);
    void sendInfo(String message) throws IOException;
}

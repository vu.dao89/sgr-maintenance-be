package vn.com.sungroup.inventoryasset.service;

import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.equipment.SystemStatusDto;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.com.sungroup.inventoryasset.entity.Status}.
 */
public interface StatusService {
    /**
     * Save a status.
     *
     * @param systemStatusDtoDTO the entity to save.
     * @return the persisted entity.
     */
    SystemStatusDto save(SystemStatusDto systemStatusDtoDTO);

    /**
     * Save a statuses.
     *
     * @param systemStatusDtoDTOS the entity to save.
     * @return the persisted entity.
     */
    List<SystemStatusDto> saveAll(List<SystemStatusDto> systemStatusDtoDTOS);

    /**
     * Updates a status.
     *
     * @param systemStatusDtoDTO the entity to update.
     * @return the persisted entity.
     */
    SystemStatusDto update(SystemStatusDto systemStatusDtoDTO);

    /**
     * Partially updates a status.
     *
     * @param systemStatusDtoDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<SystemStatusDto> partialUpdate(SystemStatusDto systemStatusDtoDTO);

    /**
     * Get all the statuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SystemStatusDto> findAll(Pageable pageable);

    /**
     * Get the "id" status.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SystemStatusDto> findOne(Long id);

    /**
     * Delete the "id" status.
     *
     * @param id the id of the entity.
     */
    void delete(UUID id);
}

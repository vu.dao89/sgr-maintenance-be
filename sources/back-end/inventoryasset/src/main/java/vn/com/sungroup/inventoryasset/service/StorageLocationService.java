package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.dto.storagelocation.StorageLocationDto;
import vn.com.sungroup.inventoryasset.entity.StorageLocation;

import java.util.List;

public interface StorageLocationService {


    void save(StorageLocationDto storageLocation);

    List<StorageLocation> findAll();
}

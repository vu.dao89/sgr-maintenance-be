package vn.com.sungroup.inventoryasset.service;

public interface SyncMasterDataService {
    void syncMasterData();
    void syncEmployee();
}

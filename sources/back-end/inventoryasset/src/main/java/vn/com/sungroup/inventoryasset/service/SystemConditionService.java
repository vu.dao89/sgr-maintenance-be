package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.SystemConditionRequest;
import vn.com.sungroup.inventoryasset.dto.systemcondition.SystemConditionDto;
import vn.com.sungroup.inventoryasset.entity.SystemCondition;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface SystemConditionService {

  SystemCondition getByCode(String code) throws MasterDataNotFoundException;
  void save(SystemConditionDto systemCondition);
  List<SystemCondition> findAll();
  Page<SystemCondition> getSystemConditions(Pageable pageable, SystemConditionRequest input) throws InvalidSortPropertyException;

}


package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.dto.equipment.SystemStatusDto;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;

import java.util.List;

public interface SystemStatusService {

  void processSaveSystemStatus(SystemStatusDto status);
  List<SystemStatus> findAll();
}

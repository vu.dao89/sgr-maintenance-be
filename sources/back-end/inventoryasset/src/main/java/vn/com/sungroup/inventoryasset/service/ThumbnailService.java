package vn.com.sungroup.inventoryasset.service;

import com.microsoft.azure.storage.StorageException;
import java.io.IOException;
import java.net.URISyntaxException;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.exception.GenerateThumbnailException;
import vn.com.sungroup.inventoryasset.exception.CloudException;

public interface ThumbnailService {

  String generateThumbnailUrl(GlobalDocumentUploadRequest request)
      throws IOException, URISyntaxException, StorageException, GenerateThumbnailException, CloudException;
}

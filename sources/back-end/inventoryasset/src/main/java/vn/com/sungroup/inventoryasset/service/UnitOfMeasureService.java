package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.UnitOfMeasurementRequest;
import vn.com.sungroup.inventoryasset.dto.uom.UnitOfMeasureDto;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;

import java.util.List;

public interface UnitOfMeasureService {
  UnitOfMeasurement getByCode(String code) throws MasterDataNotFoundException;
  void save(UnitOfMeasureDto unitOfMeasure);
  List<UnitOfMeasurement> findAll();

  Page<UnitOfMeasurement> getUnitOfMeasures(Pageable pageable, UnitOfMeasurementRequest input) throws InvalidSortPropertyException;


}
package vn.com.sungroup.inventoryasset.service;

import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;

public interface VarianceReasonService extends BaseService<VarianceReason> {

}

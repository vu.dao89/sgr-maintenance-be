package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.WorkCenterRequest;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.util.List;

public interface WorkCenterService {
  WorkCenter getByCode(String code) throws MasterDataNotFoundException;
  void save(WorkCenter build);
  List<WorkCenter> findAll();

  Page<WorkCenter> getWorkCenterByFilter(WorkCenterRequest param, Pageable pageable)
          throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;

}

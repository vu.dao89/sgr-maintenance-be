package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangePersonalRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangeRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.complete.WorkOrderCompleteRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.metric.WorkOrderMetricRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreateRequest;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeStatusResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;

public interface WorkOrderService {

  WorkOrderDetailsSAP getDetailsWorkOrder(WorkOrderDetailsRequest request)
      throws MasterDataNotFoundException, SAPApiException;

  WorkOrderSearchSAP workOrderSearch(Pageable pageable, WorkOrderSearchRequest input)
          throws MissingRequiredFieldException, SAPApiException, InvalidInputException;

  BaseResponseSAP createToSap(WorkOrderCreateRequest input) throws Exception;

  WorkOrderChangeResponseSAP changeToSap(WorkOrderChangeRequest request) throws SAPApiException;

  WorkOrderChangeStatusResponseSAP changeStatus(String workOrderId,String statusCode,Integer reason) throws SAPApiException
          , InvalidInputException, MasterDataNotFoundException;
  WorkOrderMetricResponse getWorkOrderMetricFromSAP(WorkOrderMetricRequest input)
          throws SAPApiException, InvalidInputException;

  void changePersonal(WorkOrderChangePersonalRequest input) throws SAPApiException;

  void validateWorkOrderComplete(WorkOrderCompleteRequest input) throws SAPApiException, InvalidInputException;
}

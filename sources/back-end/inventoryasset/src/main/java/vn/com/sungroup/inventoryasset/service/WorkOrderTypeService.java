package vn.com.sungroup.inventoryasset.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.sungroup.inventoryasset.dto.request.WorkOrderTypeRequest;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.WorkOrderTypeMg;

public interface WorkOrderTypeService {

  void saveAll(List<WorkOrderTypeMg> workOrderTypeList);

  List<WorkOrderTypeMg> findAll();

  Page<WorkOrderType> getWorkOrderTypes(Pageable pageable, WorkOrderTypeRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException;
}

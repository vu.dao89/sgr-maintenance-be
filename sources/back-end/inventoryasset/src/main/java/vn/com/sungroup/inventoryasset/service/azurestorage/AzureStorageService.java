package vn.com.sungroup.inventoryasset.service.azurestorage;

import com.microsoft.azure.storage.AccessCondition;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;

import java.io.InputStream;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.entity.blobstorage.BlobStorageEntity;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Spliterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import vn.com.sungroup.inventoryasset.exception.CloudException;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

@Service
@Slf4j
@RequiredArgsConstructor
public class AzureStorageService {
    private static final Logger LOGGER = Logger.getLogger(AzureStorageService.class.getName());

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Value("${azstorage.CONTAINER_NAME:dev-veek}")
    private String containerName;

    @Value("${azstorage.STORAGE_CONNECTION_STRING:DefaultEndpointsProtocol=https;AccountName=f6f86f5e9537a4619aafe1e;AccountKey=Cj0LGOIBR4ozUFPsm2Julvmws2VfgQhFbkroiHNYPPSDlZKWbyox/OxrTmrR7LxiBORm7w1fuwlOlQgsac2Fig==;EndpointSuffix=core.windows.net}")
    private String connectionString;

    private CloudBlobClient blobClient;

    @PostConstruct
    public void init() {
        try {
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(connectionString);
            blobClient = storageAccount.createCloudBlobClient();
            createContainer(containerName);
        } catch (URISyntaxException | InvalidKeyException ex) {
            LOGGER.log(Level.SEVERE, "Invalid Account", ex);
        }
    }

    private void createContainer(String containerName) {
        try {
            String lowercase = containerName.toLowerCase(); //if it include Uppercase 400 error
            CloudBlobContainer container = blobClient.getContainerReference(lowercase);
            if (!container.exists()) {
                container.createIfNotExists();

                BlobContainerPermissions permissions = new BlobContainerPermissions();
                permissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                container.uploadPermissions(permissions);
            }
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, "Invalid URISyntax", ex);
        } catch (StorageException ste) {
            LOGGER.log(Level.SEVERE, "Invalid Strage type", ste);
        }
    }

    public void uploadFile(byte[] file, String fileName) throws URISyntaxException, StorageException, IOException{
        CloudBlobContainer container;
        container = blobClient.getContainerReference(containerName);
        CloudBlockBlob blob = container.getBlockBlobReference(fileName);
        var requestOption = new BlobRequestOptions();
        requestOption.setMaximumExecutionTimeInMs(120000);
        blob.upload(new ByteArrayInputStream(file), file.length,new AccessCondition(),requestOption,new OperationContext());
    }

    public void uploadFile(InputStream inputStream, String fileName, long length)
        throws CloudException {
        try {
            CloudBlobContainer container = blobClient.getContainerReference(containerName);
            CloudBlockBlob blob = container.getBlockBlobReference(fileName);
            var requestOption = new BlobRequestOptions();
            requestOption.setMaximumExecutionTimeInMs(120000);
            blob.upload(inputStream, length,new AccessCondition(),requestOption,new OperationContext());
        } catch (URISyntaxException | StorageException | IOException ex) {
            log.error("Upload file to azure store error: " + ex);
            throw new CloudException(MessageUtil.getMessage(messageSource,locale,"UPLOAD_FILE_CLOUD_ERROR"));
        }
    }

    public BlobStorageEntity getFile(String fileName) throws CloudException {
        try {
            CloudBlobContainer container = blobClient.getContainerReference(containerName);
            var cloudBlob = container.getBlobReferenceFromServer(fileName);
            if (cloudBlob == null) {
                return null;
            }

            return convertEntity(cloudBlob);
        } catch (URISyntaxException | StorageException ex) {
            log.error("Get file to azure store error: " + ex);
            throw new CloudException(MessageUtil.getMessage(messageSource,locale,"GET_FILE_CLOUD_ERROR"));
        }
    }

    public List<BlobStorageEntity> getAllFiles() throws URISyntaxException, StorageException{
        List<BlobStorageEntity> entity = new ArrayList<>();

        CloudBlobContainer container = blobClient.getContainerReference(containerName);

        Iterable<ListBlobItem> items = container.listBlobs();
        Spliterator<ListBlobItem> spliterator = items.spliterator();
        Stream<ListBlobItem> stream = StreamSupport.stream(spliterator, false);

        List<CloudBlob> blockBlob = stream
                .filter(item -> item instanceof CloudBlob)
                .map(item -> (CloudBlob) item)
                .collect(Collectors.toList());

        entity = blockBlob.stream().map(blob -> convertEntity(blob)).collect(Collectors.toList());

        return entity;
    }

    private BlobStorageEntity convertEntity(CloudBlob blob) {
        BlobStorageEntity entity = new BlobStorageEntity();

        BlobProperties properties = blob.getProperties();

        entity.setLastModifyDate(properties.getLastModified());
        entity.setName(blob.getName());
        entity.setSize(properties.getLength());
        entity.setURI(blob.getUri().toString());

        return entity;
    }

    public void deleteAll(String containerName) {
        try {
            CloudBlobContainer container = blobClient.getContainerReference(containerName);
            Iterable<ListBlobItem> items = container.listBlobs();
            Spliterator<ListBlobItem> spliterator = items.spliterator();
            Stream<ListBlobItem> stream = StreamSupport.stream(spliterator, false);

            stream.filter(item -> item instanceof CloudBlob)
                    .map(item -> (CloudBlob) item)
                    .forEach(blob -> {
                        try {
                            String name = blob.getName();

                            CloudBlockBlob delFile;
                            delFile = container.getBlockBlobReference(name);
                            // Delete the blob.
                            delFile.deleteIfExists();
                        } catch (URISyntaxException | StorageException ex) {
                            LOGGER.log(Level.SEVERE, null, ex);
                        }
                    });
        } catch (URISyntaxException | StorageException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    public void delete(String fileName){
        try {
            CloudBlobContainer container = blobClient.getContainerReference(containerName);
            var cloudBlob =  container.getBlobReferenceFromServer(fileName);
            if (cloudBlob == null) return;
            // Delete the blob.
            cloudBlob.deleteIfExists();
        } catch (URISyntaxException | StorageException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
}

package vn.com.sungroup.inventoryasset.service.hierarchy;

import java.util.Collection;
import vn.com.sungroup.inventoryasset.dto.hierarchy.Element;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;

public interface HierarchyService<T extends Element<R>, R> {

  Collection<T> getChildren(T element);

  Collection<T> getRoots();

  TreeNode<T> getTree(T element);

  boolean isRoot(T element);
}

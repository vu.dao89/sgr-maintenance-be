package vn.com.sungroup.inventoryasset.service.hierarchy;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import vn.com.sungroup.inventoryasset.dto.hierarchy.Element;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;

@AllArgsConstructor
public class HierarchyServiceImpl<T extends Element<R>, R> implements HierarchyService<T, R> {

  private final Collection<T> all;

  @Override
  public Collection<T> getChildren(T parent) {
    return this.all.stream()
        .filter(element -> parent.checkId(element.getParentId()))
        .collect(Collectors.toSet());
  }

  @Override
  public Collection<T> getRoots() {
    return this.all.stream()
        .filter(this::isRoot)
        .collect(Collectors.toSet());
  }

  @Override
  public TreeNode<T> getTree(T element) {
    final Collection<T> children = this.getChildren(element);
    return children.isEmpty() ? new TreeNode<>(element) : new TreeNode<>(element, children.stream()
        .map(this::getTree)
        .collect(Collectors.toSet()));
  }

  @Override
  public boolean isRoot(T element) {
    return Optional.ofNullable(element.getParentId()).isEmpty();
  }
}

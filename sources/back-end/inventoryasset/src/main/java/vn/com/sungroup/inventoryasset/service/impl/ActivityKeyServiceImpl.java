package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.activitykey.ActivityKeyDto;
import vn.com.sungroup.inventoryasset.dto.request.ActivityKeyRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityKey;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.ActivityKeyRepository;
import vn.com.sungroup.inventoryasset.service.ActivityKeyService;

import java.util.List;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@RequiredArgsConstructor
public class ActivityKeyServiceImpl implements ActivityKeyService {

    private final Logger log = LoggerFactory.getLogger(ActivityKeyServiceImpl.class);
    private final ActivityKeyRepository activityKeyRepository;

    @Override
    public Page<ActivityKey> getActivityKeys(Pageable pageable, ActivityKeyRequest input)
            throws InvalidSortPropertyException {
        try {
            return activityKeyRepository.findAllByConditions(pageable, input);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public ActivityKey save(ActivityKeyDto activityKeyDto) {
        return activityKeyRepository.save(ActivityKey.builder()
                .id(activityKeyDto.getId())
                .workOrderType(activityKeyDto.getWorkOrderType())
                .type(activityKeyDto.getType())
                .description(activityKeyDto.getDescription())
                .build());
    }

    @Override
    public List<ActivityKey> findAll() {
        return activityKeyRepository.findAll();
    }

}

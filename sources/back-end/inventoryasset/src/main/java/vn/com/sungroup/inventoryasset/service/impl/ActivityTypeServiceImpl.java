package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.ActivityTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ActivityType;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.mongo.ActivityTypeMg;
import vn.com.sungroup.inventoryasset.repository.ActivityTypeRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.ActivityTypeRepositoryMg;
import vn.com.sungroup.inventoryasset.service.ActivityTypeService;

import java.util.List;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class ActivityTypeServiceImpl implements ActivityTypeService {

  private final Logger log = LoggerFactory.getLogger(ActivityTypeServiceImpl.class);
  private final ActivityTypeRepository activityTypeRepository;
  private final ActivityTypeRepositoryMg activityTypeRepositoryMg;

  @Override
  public void saveAll(List<ActivityTypeMg> activityTypes) {
    activityTypeRepositoryMg.saveAll(activityTypes);
  }

  @Override
  public List<ActivityTypeMg> findAll() {
    return activityTypeRepositoryMg.findAll();
  }

  @Override
  public Page<ActivityType> getActivityTypes(Pageable pageable, ActivityTypeRequest input) throws InvalidSortPropertyException{
    try {
      return activityTypeRepository.findAllByConditions(pageable, input);
    } catch (PropertyReferenceException ex) {
      log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
      throw new InvalidSortPropertyException(ex);
    }
  }
}

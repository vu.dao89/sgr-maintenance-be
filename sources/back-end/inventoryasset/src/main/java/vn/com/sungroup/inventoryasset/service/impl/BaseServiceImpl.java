package vn.com.sungroup.inventoryasset.service.impl;

import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

import java.util.*;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.util.security.PermissionCode.APPLICATION_ID;

public class BaseServiceImpl {
    protected List<String> getMaintenancePlantIds() {
        Set<String> maintenancePlantIds = new HashSet<>();
        var departmentRoles = SecurityUtil.getCurrentUser().getDepartmentRoles();
        departmentRoles.forEach(departmentRole -> {
            departmentRole.getRoles().forEach(role -> {
                role.getPermissions().forEach(permission -> {
                    var code = permission.getCode();
                    var appCode = permission.getAppCode();
                    if (code != null && appCode != null && code.contains("MTN_PLANT") &&
                            StringUtils.hasText(appCode) &&
                            appCode.contains(APPLICATION_ID)) {
                        var strings = code.split("_");
                        var maintenancePlantId = Arrays.stream(strings).collect(Collectors.toList()).get(strings.length - 1);
                        maintenancePlantIds.add(maintenancePlantId);
                    }
                });
            });
        });

        return new ArrayList<>(maintenancePlantIds);
    }

    protected String getEmail() {
        var currentUser = SecurityUtil.getCurrentUser();
        return currentUser.getEmail();
//        return "anhnt15@sungroup.com.vn";
    }

    protected String getPersonnel(){
        var currentUser = SecurityUtil.getCurrentUser();
        return currentUser.getEmployeeCode();
//        return "21718";
    }
}

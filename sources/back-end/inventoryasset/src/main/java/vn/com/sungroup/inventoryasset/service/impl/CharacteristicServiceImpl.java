package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.CharacteristicsRequest;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.CharacteristicRepository;
import vn.com.sungroup.inventoryasset.service.CharacteristicService;

import java.util.ArrayList;
import java.util.List;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class CharacteristicServiceImpl implements CharacteristicService {
    private final CharacteristicRepository characteristicRepository;

    @Override
    public Page<Characteristic> getCharacteristics(Pageable pageable, CharacteristicsRequest input) throws InvalidSortPropertyException{

         try {
             return characteristicRepository.findAllByConditions(pageable, input);
         } catch (PropertyReferenceException ex) {
             log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
             throw new InvalidSortPropertyException(ex);
         }
    }

    @Override
    public List<Characteristic> processSaveCharacteristics(List<Characteristic> characteristics) {
        List<Characteristic> response = new ArrayList<>();
        for (Characteristic characteristic : characteristics) {
            try {
                Characteristic characteristicEntity = characteristicRepository.save(characteristic);
                response.add(characteristicEntity);
            } catch (DataIntegrityViolationException e) {

            }
        }
        return response;
    }

    @Override
    public List<Characteristic> findAll() {
        return characteristicRepository.findAll();
    }
}

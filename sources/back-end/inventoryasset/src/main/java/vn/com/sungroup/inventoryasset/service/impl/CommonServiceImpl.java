package vn.com.sungroup.inventoryasset.service.impl;

import java.util.Locale;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.request.*;
import vn.com.sungroup.inventoryasset.entity.ControlKey;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;
import vn.com.sungroup.inventoryasset.entity.common.CommonStatus;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.SystemStatusRepository;
import vn.com.sungroup.inventoryasset.repository.common.CommonStatusRepository;
import vn.com.sungroup.inventoryasset.repository.common.ControlKeyRepository;
import vn.com.sungroup.inventoryasset.repository.common.PriorityTypeRepository;
import vn.com.sungroup.inventoryasset.repository.common.VarianceReasonRepository;
import vn.com.sungroup.inventoryasset.service.CommonService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@RequiredArgsConstructor
public class CommonServiceImpl implements CommonService {

    private final Logger log = LoggerFactory.getLogger(EquipmentServiceImpl.class);
    private final ControlKeyRepository controlKeyRepository;
    private final PriorityTypeRepository priorityTypeRepository;
    private final VarianceReasonRepository varianceReasonRepository;
    private final CommonStatusRepository commonStatusRepository;
    private final SystemStatusRepository systemStatusRepository;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Override
    public Page<ControlKey> getControlKeys(Pageable pageable, ControlKeyRequest input) throws InvalidSortPropertyException {
        try {
            return controlKeyRepository.findAllByConditions(pageable, input);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public ControlKey getControlKey(String code) throws MasterDataNotFoundException {
        return controlKeyRepository.findByCode(code).orElseThrow(()-> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

    @Override
    public Page<CommonStatus> getCommonStatusByFilter(CommonStatusRequest request, Pageable pageable)
            throws InvalidSortPropertyException {
        try {
            return commonStatusRepository.getCommonStatusByFilter(request, pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public Page<PriorityType> getPriorityTypeByFilter(PriorityTypeRequest request,
                                                      Pageable pageable) throws InvalidSortPropertyException {
        try {
            return priorityTypeRepository.findPriorityTypeByFilter(request, pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public Page<VarianceReason> getVarianceReasons(Pageable pageable, VarianceReasonRequest request) {
        return varianceReasonRepository.findAllByConditions(pageable, request);
    }

    @Override
    public VarianceReason getVarianceReasonByCode(String code) throws MasterDataNotFoundException {
        return varianceReasonRepository.findByCode(code).orElseThrow(()-> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

    @Override
    public Page<SystemStatus> getSystemStatus(Pageable pageable, SystemStatusRequest request)
            throws InvalidSortPropertyException {
        try {
            return systemStatusRepository.findAllByConditions(pageable, request);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

  @Override
  public Optional<CommonStatus> findByStatusAndCode(String status, String workOrder) {
    return commonStatusRepository.findByStatusAndCode(status, workOrder);
  }
}

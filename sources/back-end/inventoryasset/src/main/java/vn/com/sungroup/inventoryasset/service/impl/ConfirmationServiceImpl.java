package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.mapper.WorkOrderMapper;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;
import vn.com.sungroup.inventoryasset.service.ConfirmationService;

@Service
@RequiredArgsConstructor
public class ConfirmationServiceImpl extends BaseServiceImpl implements ConfirmationService {

  private final SAPService sapService;
  private final WorkOrderMapper workOrderMapper;

  @Override
  public OperationConfirmationResponse confirmToSap(OperationConfirmationRequest request)
      throws SAPApiException {

    var input = workOrderMapper.toSapConfirmationRequest(request);
    if (input.getConfirmation().getEmail().isEmpty()){
      input.getConfirmation().setEmail(getEmail());
    }
    return sapService.confirmation(input);
  }
}

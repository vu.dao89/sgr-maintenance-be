package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.entity.Employee;
import vn.com.sungroup.inventoryasset.repository.EmployeeRepository;
import vn.com.sungroup.inventoryasset.service.EmployeeService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

  private final EmployeeRepository employeeRepository;

  @Override
  public void saveAll(List<Employee> employees) {
    employeeRepository.saveAll(employees);
  }

  @Override
  public List<Employee> getAll() {
    return employeeRepository.findAll();
  }

  @Override
  public Page<Employee> getAll(Pageable pageable) {
    return employeeRepository.findAll(pageable);
  }

  @Override
  public Optional<Employee> getByEmployeeId(String employeeId) {
    return employeeRepository.findByEmployeeId(employeeId);
  }

  @Override
  public Optional<Employee> getByPrimaryId(String primaryId) {
    return employeeRepository.findByPrimaryId(primaryId);
  }
}

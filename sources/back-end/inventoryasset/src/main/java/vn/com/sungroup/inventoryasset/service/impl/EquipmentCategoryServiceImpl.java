package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentCategoryRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentCategory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.mongo.EquipmentCategoryMg;
import vn.com.sungroup.inventoryasset.repository.EquipmentCategoryRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.EquipmentCategoryRepositoryMg;
import vn.com.sungroup.inventoryasset.service.EquipmentCategoryService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class EquipmentCategoryServiceImpl implements EquipmentCategoryService {

    private final Logger log = LoggerFactory.getLogger(EquipmentCategoryServiceImpl.class);
    private final EquipmentCategoryRepository equipmentCategoryRepository;
    private final EquipmentCategoryRepositoryMg equipmentCategoryRepositoryMg;


    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Override
    public EquipmentCategory getByCode(String code) throws MasterDataNotFoundException {
        return equipmentCategoryRepository.findByCode(code).orElseThrow(()-> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

    @Override
    public List<EquipmentCategoryMg> findAll() {
        return equipmentCategoryRepositoryMg.findAll();
    }

    @Override
    public void saveAll(List<EquipmentCategoryMg> equipmentCategoryList) {
        equipmentCategoryRepositoryMg.saveAll(equipmentCategoryList);
    }

  @Override
  public Page<EquipmentCategory> findEquipmentCategoryByFilter(EquipmentCategoryRequest request,
      Pageable pageable) throws InvalidSortPropertyException {
      try {
          return equipmentCategoryRepository.findEquipmentCategoryByFilter(request, pageable);
      } catch (PropertyReferenceException ex) {
          log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
          throw new InvalidSortPropertyException(ex);
      }
  }
}

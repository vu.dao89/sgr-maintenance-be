package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDocumentRequest;
import vn.com.sungroup.inventoryasset.entity.EquipmentDocument;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.EquipmentDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.GlobalDocumentRepository;
import vn.com.sungroup.inventoryasset.service.EquipmentDocumentService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class EquipmentDocumentServiceImpl implements EquipmentDocumentService {

    private final Logger log = LoggerFactory.getLogger(EquipmentDocumentServiceImpl.class);
    private final EquipmentDocumentRepository equipmentDocumentRepository;
    private final GlobalDocumentRepository globalDocumentRepository;
    @Override
    public List<EquipmentDocument> findAll() {
        return equipmentDocumentRepository.findAll();
    }

    @Override
    public Page<EquipmentDocument> findAll(Pageable pageable) throws InvalidSortPropertyException {
        return equipmentDocumentRepository.findAll(pageable);
    }

    @Override
    public Optional<EquipmentDocument> findOne(UUID id) {
        return equipmentDocumentRepository.findById(id);
    }

    @Override
    public void saveAll(List<EquipmentDocument> equipmentDocuments) {
        equipmentDocumentRepository.saveAll(equipmentDocuments);
    }

    @Override
    public void deleteAll(List<EquipmentDocument> equipmentDocuments) {
        equipmentDocumentRepository.deleteAll(equipmentDocuments);
    }

    @Override
    public void updateAll(List<EquipmentDocument> equipmentDocuments) {
        equipmentDocumentRepository.saveAll(equipmentDocuments);
    }

    @Override
    public Page<EquipmentDocument> findAllByEquipmentCode(EquipmentDocumentRequest request, Pageable pageable) {
        return equipmentDocumentRepository.findAllByEquipmentCode(request.getEquipmentCode(), pageable);
    }

    @Override
    public List<EquipmentDocument> findAllByEquipmentCode(String equipmentCode) {
        return equipmentDocumentRepository.findAllByEquipmentCode(equipmentCode);
    }

    @Override
    public EquipmentDocument save(EquipmentDocumentCreate input) {
        var globalDocument = globalDocumentRepository.findById(input.getDocumentId()).orElse(null);
        var entity = EquipmentDocument.builder()
                .globalDocument(globalDocument)
                .equipmentCode(input.getEquipmentCode()).build();
        return equipmentDocumentRepository.save(entity);
    }
}

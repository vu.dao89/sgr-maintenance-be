package vn.com.sungroup.inventoryasset.service.impl;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDto;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentHierarchyDto;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.dto.request.EquipmentRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.GetMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.response.equipment.EquipmentResponse;
import vn.com.sungroup.inventoryasset.dto.response.equipment.sap.MeasPointSAPResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.*;
import vn.com.sungroup.inventoryasset.exception.*;
import vn.com.sungroup.inventoryasset.mapper.EquipmentMapper;
import vn.com.sungroup.inventoryasset.repository.*;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;
import vn.com.sungroup.inventoryasset.service.*;
import vn.com.sungroup.inventoryasset.service.hierarchy.HierarchyService;
import vn.com.sungroup.inventoryasset.service.hierarchy.HierarchyServiceImpl;
import vn.com.sungroup.inventoryasset.util.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;
import static vn.com.sungroup.inventoryasset.error.Errors.DATA_NOT_FOUND;
import static vn.com.sungroup.inventoryasset.error.Errors.INVALID_FIELD;
import static vn.com.sungroup.inventoryasset.util.PayloadUtil.*;

@Service
@RequiredArgsConstructor
//@Transactional
public class EquipmentServiceImpl extends BaseServiceImpl implements EquipmentService {
    private final Logger log = LoggerFactory.getLogger(EquipmentServiceImpl.class);
    private final SAPService sapService;
    private final EquipmentDocumentRepository equipmentDocumentRepository;
    private final EquipmentRepository equipmentRepository;
    private final MaintenancePlantRepository maintenancePlantRepository;
    private final EquipmentCategoryRepository equipmentCategoryRepository;
    private final SystemStatusRepository systemStatusRepository;
    private final ObjectTypeRepository objectTypeRepository;
    private final SystemStatusService systemStatusService;
    private final CharacteristicService characteristicService;
    private final MeasuringPointService measuringPointService;
    private final PartnerService partnerService;
    private final FuncLocationRepository funcLocationRepository;
    private final WorkCenterRepository workCenterRepository;
    private final MeasuringPointRepository measuringPointRepository;
    private final EquipmentMapper equipmentMapper;
    private final PayloadUtil payloadUtil;
    private final PlannerGroupRepository plannerGroupRepository;

    private final ParseUtils parseUtils;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    @Override
    public EquipmentDto processSave(EquipmentDto equipmentDTO) {
        log.debug("Request to save Equipment : {}", equipmentDTO);
        if (equipmentDTO.getCostCenterName() != null && !equipmentDTO.getCostCenterName().isBlank()) {
            log.debug("Request to save Equipment : {}", equipmentDTO);
        }

        LocalTime createTime = DateUtils.parseStringToLocalTime(equipmentDTO.getCreateTime(),
                DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        LocalDate createDate = DateUtils.parseStringToLocalDate(equipmentDTO.getCreateDate(),
                DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate cusWarrantyDate = DateUtils.parseStringToLocalDate(equipmentDTO.getCusWarrantyDate(),
                DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate cusWarrantyEnd = DateUtils.parseStringToLocalDate(equipmentDTO.getCusWarrantyEnd(),
                DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate venWarrantyDate = DateUtils.parseStringToLocalDate(equipmentDTO.getVenWarrantyDate(),
                DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate venWarrantyEnd = DateUtils.parseStringToLocalDate(equipmentDTO.getVenWarrantyEnd(),
                DateTimeFormatter.BASIC_ISO_DATE);
        List<Characteristic> characteristics = characteristicService.processSaveCharacteristics(
                EquipmentMapper.INSTANCE.toCharacteristicEntityList(equipmentDTO.getCharacteristics()));
        List<MeasuringPoint> measuringPoints = measuringPointService.processSaveMeasuringPoints(
                EquipmentMapper.INSTANCE.toMeasuringPointEntityList(equipmentDTO.getMeasurePoint()));
        List<Partner> partners = partnerService.processSavePartners(
                EquipmentMapper.INSTANCE.toPartnerEntityList(equipmentDTO.getPartners()));

        EquipmentCategory existEquipmentCategory = equipmentCategoryRepository.findByCode(
                equipmentDTO.getEquipmentCategoryId()).orElse(null);

        FunctionalLocation functionalLocation = funcLocationRepository.findByFunctionalLocationId(
                equipmentDTO.getFunctionalLocationId()).orElse(null);

        SystemStatus systemStatus = null;
        if (equipmentDTO.getStatus() != null) {
            systemStatus = systemStatusRepository.findByStatusId(
                    equipmentDTO.getStatus().getStatusId()).orElse(null);
            if (systemStatus == null) {
                systemStatusService.processSaveSystemStatus(equipmentDTO.getStatus());
            }
        }

        MaintenancePlant maintenancePlant = maintenancePlantRepository.findByCode(
                equipmentDTO.getMaintenancePlantId()).orElse(null);

        var workCenter =
                workCenterRepository.findByCodeAndMaintenancePlantId(equipmentDTO.getMaintenanceWorkCenterId(),
                        equipmentDTO.getMaintenancePlantId()).orElse(null);

        var equipmentType = objectTypeRepository.findByCode(equipmentDTO.getEquipmentTypeId()).orElse(null);

        Equipment parentEquipment = equipmentRepository.findByEquipmentId(equipmentDTO.getParentEquipmentId()).orElse(null);

        var equipment = Equipment.builder()
//                .id(equipmentDTO.getId())
                .equipmentId(equipmentDTO.getEquipmentId())
                .parentEquipmentId(equipmentDTO.getParentEquipmentId())
                .parentEquipment(parentEquipment)
                .equipmentType(equipmentType)
                .constYear(equipmentDTO.getConstYear())
                .constMonth(equipmentDTO.getConstMonth())
                .costCenterName(equipmentDTO.getCostCenterName())
                .maintenancePlant(maintenancePlant)
                .functionalLocation(functionalLocation)
                .equipmentCategory(existEquipmentCategory)
                .systemStatus(systemStatus)
                .description(equipmentDTO.getDescription())
                .longDesc(equipmentDTO.getLongDesc())
                .location(equipmentDTO.getLocation())
                .maintenanceWorkCenter(workCenter)
                .costCenter(equipmentDTO.getCostCenter())
                .manuCountry(equipmentDTO.getManuCountry())
                .manuPartNo(equipmentDTO.getManuPartNo())
                .manuSerialNo(equipmentDTO.getManuSerialNo())
                .manufacturer(equipmentDTO.getManufacturer())
                .modelNumber(equipmentDTO.getModelNumber())
                .plannerGroup(equipmentDTO.getPlannerGroup())
                .planningPlantId(equipmentDTO.getPlanningPlantId())
                .plantSection(equipmentDTO.getPlantSection())
                .size(equipmentDTO.getSize())
                .weight(equipmentDTO.getWeight())
                .createDate(createDate)
                .createTime(createTime)
                .cusWarrantyDate(cusWarrantyDate)
                .cusWarrantyEnd(cusWarrantyEnd)
                .venWarrantyDate(venWarrantyDate)
                .venWarrantyEnd(venWarrantyEnd)
                .qrCode(equipmentDTO.getQrCode())
                .startupDate(equipmentDTO.getStartUpdate())
                .endOfUseDate(equipmentDTO.getEndOfUseDate())
                .characteristics(characteristics)
                .measuringPoints(measuringPoints)
                .partners(partners)
                .originalUrl("https://f6f86f5e9537a4619aafe1e.blob.core.windows.net/dev-veek/equipment_desktop.jpg")
                .thumbnailUrl("https://f6f86f5e9537a4619aafe1e.blob.core.windows.net/dev-veek/equipment_mobile.jpg")
                .sortField(equipmentDTO.getSortField())
                .costCenterName(equipmentDTO.getCostCenterName())
                .build();

        if (workCenter != null) {
            equipment.setMaintenanceWorkCenterId(workCenter.getCode());
        }

        equipmentRepository.save(equipment);
        return equipmentDTO;
    }

    private SystemStatus createSystemStatus(String statusId) {
        if (statusId == null) {
            return null;
        }
        return systemStatusRepository.save(SystemStatus.builder().statusId(statusId).build());
    }

    private EquipmentCategory createEquipmentCategory(String equipcategory) {
        if (equipcategory != null) {
            try {
                return equipmentCategoryRepository.save(
                        EquipmentCategory.builder().code(equipcategory).build());
            } catch (DataIntegrityViolationException e) {
            }
        }
        return null;
    }

    @Transactional
    public MaintenancePlant createMaintenancePlant(String maintplant) {
        if (maintplant != null) {
            try {
                return maintenancePlantRepository.save(
                        MaintenancePlant.builder().code(maintplant).build());
            } catch (DataIntegrityViolationException e) {
            }
        }
        return null;
    }


    @Override
    public Page<EquipmentResponse> getEquipments(Pageable pageable, EquipmentRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        payloadUtil.validateInput(input);
        stopWatch.stop();
        Sentry.captureMessage("TIME LOGGER - getEquipments - validateInput: " + stopWatch.getTotalTimeMillis(),
                SentryLevel.INFO);
        stopWatch.start();
        var maintenancePlantCodesInput = input.getMaintenancePlantCodes();
        var functionalLocationCodesInput = input.getFunctionalLocationCodes();
        if ((maintenancePlantCodesInput == null || maintenancePlantCodesInput.isEmpty()) &&
                (functionalLocationCodesInput == null || functionalLocationCodesInput.isEmpty())) {
            var maintenancePlantIds = getMaintenancePlantIds();
            if (maintenancePlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());
            input.setMaintenancePlantCodes(maintenancePlantIds);
        }
        stopWatch.stop();
        Sentry.captureMessage("TIME LOGGER - getEquipments - setMaintenancePlantCodes: " + stopWatch.getTotalTimeMillis(),
                SentryLevel.INFO);
        stopWatch.start();
        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());
        stopWatch.stop();
        Sentry.captureMessage("TIME LOGGER - getEquipments - validateMaintenancePlantPermission: " + stopWatch.getTotalTimeMillis(),
                SentryLevel.INFO);
        stopWatch.start();
        Page<EquipmentResponse> equipments = equipmentRepository.findAllByConditions(pageable, input);
        stopWatch.stop();
        Sentry.captureMessage("TIME LOGGER - getEquipments - findAllByConditions: " + stopWatch.getTotalTimeMillis(),
                SentryLevel.INFO);

        return equipments;
    }

    @Override
    public Equipment getByQrCode(String qrCode) throws InvalidSortPropertyException {
        try {
            var equipments = equipmentRepository.findByQrCode(qrCode);
            return equipments.isEmpty() ? null : equipments.get(0);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public Collection<TreeNode<EquipmentHierarchyResponse>> getEquipmentHierarchy(
            String equipmentId) {
        String rootParentEquipmentId = equipmentRepository.findRootParentEquipmentId(equipmentId);

        List<EquipmentHierarchyDto> hierarchyDtos = equipmentRepository.findEquipmentHierarchy(
                rootParentEquipmentId);

        return processConvertEquipmentHierarchy(hierarchyDtos, equipmentId);
    }

    @Override
    public Equipment getByEquipmentId(String equiId) throws MasterDataNotFoundException, SAPApiException {
        Equipment equipment = equipmentRepository.findByEquipmentId(equiId)
                .orElseThrow(() -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource, locale, "DATA_NOT_FOUND")));

        var plannerGroup =
                plannerGroupRepository.findByPlannerGroupIdAndMaintenancePlantId(equipment.getPlannerGroup(),
                        equipment.getMaintenancePlantId()).orElse(null);
        if (plannerGroup != null) {
            equipment.setPlannerGroupName(plannerGroup.getName());
        }

        try {
            GetMeasPointRequest getMeasPointRequest = GetMeasPointRequest.builder()
                    .measPointIds(equipment.getMeasuringPoints().stream()
                            .map(MeasuringPoint::getPoint)
                            .collect(Collectors.toList()))
                    .build();
            MeasPointSAPResponse measPointResponse = sapService.getEquipmentMeasPoint(getMeasPointRequest.toMeasPointSAPRequest());

            updateMeasPointInDBByMeasPointSQP(equipment.getMeasuringPoints(), measPointResponse);
        } finally {
            return equipment;
        }
    }

    @Override
    public List<Equipment> findAll() {
        return equipmentRepository.findAll();
    }

    private Collection<TreeNode<EquipmentHierarchyResponse>> processConvertEquipmentHierarchy(
            List<EquipmentHierarchyDto> hierarchyDtos, String equipmentId) {
        List<EquipmentHierarchyResponse> resultFromDB = hierarchyDtos.stream()
                .map(dto -> EquipmentHierarchyResponse.builder()
                        .id(dto.getEquipmentId())
                        .parentId(
                                StringUtilsCustom.isEmpty(dto.getParentEquipmentId()) ? null
                                        : dto.getParentEquipmentId()
                        )
                        .description(dto.getDescription())
                        .level(dto.getLevel())
                        .isSelected(dto.getEquipmentId().equals(equipmentId))
                        .build())
                .collect(Collectors.toList());

        HierarchyService<EquipmentHierarchyResponse, String> service =
                new HierarchyServiceImpl<>(resultFromDB);

        return service.getRoots()
                .stream()
                .map(service::getTree)
                .collect(Collectors.toSet());
    }

    @Override
    public MeasPointSAPResponse getEquipmentMeasPoint(GetMeasPointRequest getmeasPointRequest)
            throws SAPApiException {
        MeasPointSAPResponse measPointSAPResponse = sapService.getEquipmentMeasPoint(getmeasPointRequest.toMeasPointSAPRequest());

        return measPointSAPResponse;
    }

    private void updateMeasPointInDBByMeasPointSQP(List<MeasuringPoint> measuringPointsInDatabase, MeasPointSAPResponse measPointSAPResponse) {
        measuringPointsInDatabase.forEach(measuringPoint -> {
            var measPointSAP = measPointSAPResponse.getMsp().getItem().stream()
                    .filter(measPoint -> measPoint.getMeasPoint().equals(measuringPoint.getPoint()))
                    .findFirst().orElse(null);

            if (measPointSAP != null) {
                if (measuringPoint.getCounter().equals("X")) {
                    measuringPoint.setLastMeasuringPointValue(measPointSAP.getCountRead());
                } else {
                    measuringPoint.setLastMeasuringPointValue(measPointSAP.getMeasRead());
                }
            }
        });
    }

    @Override
    public BaseResponseSAP postEquipmentMeasPoint(PostMeasPointRequest postMeasPointRequest) throws InvalidInputException, SAPApiException {
        validatePostMeasPointRequest(postMeasPointRequest);
        var request = equipmentMapper.toPostMeasPointSAPRequest(postMeasPointRequest);
        request.getData().setEmail(getEmail());
        return sapService.equipmentMeasPoint(request);
    }

    private void validatePostMeasPointRequest(PostMeasPointRequest postMeasPointRequest) throws InvalidInputException {
        var measPoint = measuringPointRepository.findByPoint(postMeasPointRequest.getData().getMeasPoint()).orElse(null);
        if (measPoint == null) {
            throw new InvalidInputException(DATA_NOT_FOUND, MessageUtil.getMessage(messageSource, locale, MEASPOINT_NOT_FOUND));
        }

        var counter = measPoint.getCounter();
        if (counter.equals("X")) {
            if (postMeasPointRequest.getData().getIsMeasDiff()) {
                if (postMeasPointRequest.getData().getDiff() == null ||
                        postMeasPointRequest.getData().getDiff().isEmpty() ||
                        postMeasPointRequest.getData().getDiff().isBlank()) {
                    throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, MEASPOINT_DIFF_REQUIRE));
                }
            } else {
                if (postMeasPointRequest.getData().getCountRead() == null ||
                        postMeasPointRequest.getData().getCountRead().isEmpty() ||
                        postMeasPointRequest.getData().getCountRead().isBlank()) {
                    throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, MEASPOINT_REQUIRE_COUNT_READ));
                }
            }
        } else {
            if (postMeasPointRequest.getData().getMeasRead() == null ||
                    postMeasPointRequest.getData().getMeasRead().isEmpty() ||
                    postMeasPointRequest.getData().getMeasRead().isBlank()) {
                throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, MEASPOINT_REQUIRE_MEAS_READ));
            }
        }
    }
}

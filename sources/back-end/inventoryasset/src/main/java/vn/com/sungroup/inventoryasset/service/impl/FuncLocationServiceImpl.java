package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.equipment.FunctionalLocationDto;
import vn.com.sungroup.inventoryasset.dto.functionallocation.FunctionalLocationHierarchyDto;
import vn.com.sungroup.inventoryasset.dto.functionallocation.FunctionalLocationHierarchyResponse;
import vn.com.sungroup.inventoryasset.dto.hierarchy.TreeNode;
import vn.com.sungroup.inventoryasset.dto.request.FunctionalLocationRequest;
import vn.com.sungroup.inventoryasset.entity.*;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.mapper.EquipmentMapper;
import vn.com.sungroup.inventoryasset.repository.FuncLocationRepository;
import vn.com.sungroup.inventoryasset.repository.MaintenancePlantRepository;
import vn.com.sungroup.inventoryasset.repository.ObjectTypeRepository;
import vn.com.sungroup.inventoryasset.service.CharacteristicService;
import vn.com.sungroup.inventoryasset.service.FuncLocationService;
import vn.com.sungroup.inventoryasset.service.MeasuringPointService;
import vn.com.sungroup.inventoryasset.service.hierarchy.HierarchyService;
import vn.com.sungroup.inventoryasset.service.hierarchy.HierarchyServiceImpl;
import vn.com.sungroup.inventoryasset.util.DateUtils;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Slf4j
@RequiredArgsConstructor
public class FuncLocationServiceImpl extends BaseServiceImpl implements FuncLocationService {

  private final FuncLocationRepository functionLocationRepository;
  private final CharacteristicService characteristicService;
  private final MeasuringPointService measuringPointService;
  private final MaintenancePlantRepository maintenancePlantRepository;
  private final ObjectTypeRepository objectTypeRepository;
  private final PayloadUtil payloadUtil;
  @Autowired
  private final MessageSource messageSource;
  private final Locale locale = LocaleContextHolder.getLocale();


  @Override
  public void saveAll(List<FunctionalLocationDto> functionalLocationDtos) {
    // TODO waiting sync functional location
    List<FunctionalLocation> functionalLocations = functionalLocationDtos.stream()
            .map(dto -> FunctionalLocation.builder().build()).collect(Collectors.toList());
    functionLocationRepository.saveAll(functionalLocations);
  }

  @Override
  public void processSave(FunctionalLocationDto functionalLocationDto) {
    LocalDate createDate = DateUtils.parseStringToLocalDate(functionalLocationDto.getCreateDate(),
            DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyDate = DateUtils.parseStringToLocalDate(
            functionalLocationDto.getCusWarrantyDate(),
            DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyEnd = DateUtils.parseStringToLocalDate(
            functionalLocationDto.getCusWarrantyEnd(),
            DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyDate = DateUtils.parseStringToLocalDate(
            functionalLocationDto.getVenWarrantyDate(),
            DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyEnd = DateUtils.parseStringToLocalDate(
            functionalLocationDto.getVenWarrantyEnd(),
            DateTimeFormatter.BASIC_ISO_DATE);

    List<Characteristic> characteristics = characteristicService.processSaveCharacteristics(
            EquipmentMapper.INSTANCE.toCharacteristicEntityList(
                    functionalLocationDto.getCharacteristics()));
    List<MeasuringPoint> measuringPoints = measuringPointService.processSaveMeasuringPoints(
            EquipmentMapper.INSTANCE.toMeasuringPointEntityList(
                    functionalLocationDto.getMeasurePoint()));

    MaintenancePlant maintenancePlant = maintenancePlantRepository.findByCode(
            functionalLocationDto.getMaintenancePlantId()).orElse(null);

    ObjectType functionalLocationType = objectTypeRepository.findByCode(
            functionalLocationDto.getFunctionalLocationTypeId()).orElse(null);

    ObjectType objectType = objectTypeRepository.findByCode(functionalLocationDto.getObjectTypeId())
            .orElse(null);

    functionLocationRepository.save(
            FunctionalLocation.builder()
                    .id(functionalLocationDto.getId())
                    .functionalLocationId(functionalLocationDto.getFunctionalLocationId())
                    .parentFunctionalLocationId(functionalLocationDto.getParentFunctionalLocationId())
                    .constYear(functionalLocationDto.getConstYear())
                    .constMonth(functionalLocationDto.getConstMonth())
                    .maintenancePlant(maintenancePlant)
                    .functionalLocationType(functionalLocationType)
                    // TODO field in xml does not exists
                    .functionalLocationCategory(null)
                    .description(functionalLocationDto.getDescription())
                    .longDesc(functionalLocationDto.getLongDesc())
                    .objectType(objectType)
                    .location(functionalLocationDto.getLocation())
                    .maintenanceWorkCenterId(functionalLocationDto.getMaintenanceWorkCenterId())
                    .costCenter(functionalLocationDto.getCostCenter())
                    .manuCountry(functionalLocationDto.getManuCountry())
                    .manuPartNo(functionalLocationDto.getManuPartNo())
                    .manuSerialNo(functionalLocationDto.getManuSerialNo())
                    .manufacturer(functionalLocationDto.getManufacturer())
                    .modelNumber(functionalLocationDto.getModelNumber())
                    .plannerGroup(functionalLocationDto.getPlannerGroup())
                    .planningPlantId(functionalLocationDto.getPlanningPlantId())
                    .plantSection(functionalLocationDto.getPlantSection())
                    .size(functionalLocationDto.getSize())
                    .weight(functionalLocationDto.getWeight())
                    .createDate(createDate)
                    .cusWarrantyDate(cusWarrantyDate)
                    .cusWarrantyEnd(cusWarrantyEnd)
                    .venWarrantyDate(venWarrantyDate)
                    .venWarrantyEnd(venWarrantyEnd)
                    .qrCode(functionalLocationDto.getQrCode())
                    .startupDate(functionalLocationDto.getStartupDate())
                    .endOfUseDate(functionalLocationDto.getEndOfUseDate())
                    .characteristics(characteristics)
                    .measuringPoints(measuringPoints)
                    .build());
  }

  @Override
  public Collection<TreeNode<FunctionalLocationHierarchyResponse>> getFunctionalLocationHierarchy(
      String functionalLocationId) {
    String rootParentFunctionalLocationId = functionLocationRepository
        .findRootParentFunctionalLocationId(functionalLocationId);

    List<FunctionalLocationHierarchyDto> hierarchyDtos = functionLocationRepository
        .findFunctionalLocationHierarchy(rootParentFunctionalLocationId);

    return processConvertFunctionalLocationHierarchy(hierarchyDtos, functionalLocationId);
  }

  private Collection<TreeNode<FunctionalLocationHierarchyResponse>> processConvertFunctionalLocationHierarchy(
      List<FunctionalLocationHierarchyDto> hierarchyDtos, String functionalLocationId) {
    List<FunctionalLocationHierarchyResponse> resultFromDB = hierarchyDtos.stream()
        .map(dto -> FunctionalLocationHierarchyResponse.builder()
            .id(dto.getFunctionalLocationId())
            .parentId(dto.getParentFunctionalLocationId())
            .description(dto.getDescription())
            .level(dto.getLevel())
            .isSelected(dto.getFunctionalLocationId().equals(functionalLocationId))
            .build())
        .collect(Collectors.toList());

    HierarchyService<FunctionalLocationHierarchyResponse, String> service =
        new HierarchyServiceImpl<>(resultFromDB);

    return service.getRoots()
        .stream()
        .map(service::getTree)
        .collect(Collectors.toSet());
  }

  @Override
  public FunctionalLocation getByQrCode(String qrCode) throws InvalidSortPropertyException {
    try {
      var functionalLocations = functionLocationRepository.findByQrCode(qrCode);
      return functionalLocations.isEmpty() ? null : functionalLocations.get(0);
    } catch (  PropertyReferenceException ex) {
      log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
      throw new InvalidSortPropertyException(ex);
    }

  }

  @Override
  public Page<FunctionalLocation> getFunctionalLocationByFilter(
      FunctionalLocationRequest request, Pageable pageable) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
    payloadUtil.validateInput(request);
    if(request.getMaintenancePlantCodes().isEmpty()) {
      var mPlantIds = getMaintenancePlantIds();
      if(mPlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());
      request.setMaintenancePlantCodes(mPlantIds);
    }

    payloadUtil.validateMaintenancePlantPermission(request.getMaintenancePlantCodes(), getMaintenancePlantIds());

    return functionLocationRepository.findFunctionalLocationByFilter(request, pageable);
  }

  @Override
  public FunctionalLocation getByCode(String code) throws MasterDataNotFoundException {
    return functionLocationRepository.findByFunctionalLocationId(code)
        .orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
  }

  @Override
  public List<FunctionalLocation> findAll() {
    return functionLocationRepository.findAll();
  }
}

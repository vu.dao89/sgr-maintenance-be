package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.exception.DocumentNotFoundException;
import vn.com.sungroup.inventoryasset.repository.GlobalDocumentRepository;
import vn.com.sungroup.inventoryasset.service.GlobalDocumentService;
import vn.com.sungroup.inventoryasset.service.ThumbnailService;
import vn.com.sungroup.inventoryasset.service.azurestorage.AzureStorageService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import javax.activation.MimetypesFileTypeMap;
import java.time.*;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class GlobalDocumentServiceImpl implements GlobalDocumentService {

  private final RaygunServiceImpl raygunService;
  private final ThumbnailService thumbnailService;
  private final GlobalDocumentRepository globalDocumentRepository;
  private final AzureStorageService azureStorageService;

  @Autowired
  private final MessageSource messageSource;
  private final Locale locale = LocaleContextHolder.getLocale();
  @Value("${external.rest.consume.SAP_HCM_USER}")
  private String externalSAPHcmUser;

  @Value("${external.rest.consume.SAP_HCM_PASS}")
  private String externalSapHcmPass;

  @Override
  public Optional<GlobalDocument> findByName(String fileName) {
    return globalDocumentRepository.findByFileNameContainsIgnoreCase(fileName);
  }

  @Override
  public GlobalDocument getDocument(UUID id) throws DocumentNotFoundException {
    return globalDocumentRepository.findById(id).orElseThrow(() -> new DocumentNotFoundException(MessageUtil.getMessage(messageSource,locale,"DOCUMENT_NOT_FOUND")));
  }

  @Override
  public GlobalDocument uploadFile(GlobalDocumentUploadRequest request) throws Exception {
    request.fileName = System.currentTimeMillis() + "_" + request.fileName;

    azureStorageService.uploadFile(request.fileBytes, request.fileName);
    var blobEntity = azureStorageService.getFile(request.fileName);
    String thumbnailURL =  blobEntity.getURI();
    MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();

    String mimeType =fileTypeMap.getContentType(request.fileName);
    if (mimeType.contains("image")){
      thumbnailURL = thumbnailService.generateThumbnailUrl(request);
    }
    if (blobEntity != null) {
      var globalDocument = GlobalDocument.builder()
          .fileName(request.fileName)
          .fileType(request.fileType)
          .uploadedAt(LocalDate.now(ZoneOffset.UTC))
          .uploadedAtTime(LocalTime.now(ZoneOffset.UTC))
          .originalUrl(blobEntity.getURI())
          .thumbnailUrl(thumbnailURL)
          .fileSize(blobEntity.getSize())
          .build();

      return globalDocumentRepository.save(globalDocument);
    }
    return null;
  }

  @Override
  public void delete(UUID id) {
    var entity = globalDocumentRepository.findById(id).get();
    if (entity != null) {
      azureStorageService.delete(entity.getFileName());
      globalDocumentRepository.deleteById(id);
    }
  }

  @Override
  public List<GlobalDocument> findAll() {
    return globalDocumentRepository.findAll();
  }

  @Override
  public Page<GlobalDocument> findAll(Pageable pageable) {
    return globalDocumentRepository.findAll(pageable);
  }

  @Override
  public Optional<GlobalDocument> findOne(UUID id) {
    return globalDocumentRepository.findById(id);
  }

  @Override
  public void saveAll(List<GlobalDocument> globalDocuments) {
    globalDocumentRepository.saveAll(globalDocuments);
  }

  @Override
  public void deleteAll(List<GlobalDocument> globalDocuments) {
    globalDocumentRepository.deleteAll(globalDocuments);
  }

  @Override
  public void updateAll(List<GlobalDocument> globalDocuments) {
    globalDocumentRepository.saveAll(globalDocuments);
  }
}

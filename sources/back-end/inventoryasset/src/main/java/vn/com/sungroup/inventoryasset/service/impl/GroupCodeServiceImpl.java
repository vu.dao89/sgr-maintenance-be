package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.groupcode.GroupCodeDto;
import vn.com.sungroup.inventoryasset.dto.request.CodeGroupRequest;
import vn.com.sungroup.inventoryasset.dto.request.CodeRequest;
import vn.com.sungroup.inventoryasset.dto.response.groupcode.CodeGroupResponse;
import vn.com.sungroup.inventoryasset.entity.GroupCode;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.GroupCodeRepository;
import vn.com.sungroup.inventoryasset.service.GroupCodeService;

import java.util.List;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class GroupCodeServiceImpl implements GroupCodeService {
    private final Logger log = LoggerFactory.getLogger(GroupCodeServiceImpl.class);
    private final GroupCodeRepository groupCodeRepository;

    @Override
    public void save(GroupCodeDto groupCode) {
        groupCodeRepository.save(GroupCode.builder()
                .id(groupCode.getId())
                .catalogProfile(groupCode.getCatalogProfile())
                .catalogProfileText(groupCode.getCatalogProfileDes())
                .catalog(groupCode.getCatalog())
                .catalogDesc(groupCode.getCatalogDes())
                .codeGroup(groupCode.getCodeGroup())
                .codeGroupDesc(groupCode.getCodeGroupDes())
                .code(groupCode.getCode())
                .codeDesc(groupCode.getCodeDes())
                .build());
    }

    @Override
    public List<GroupCode> findAll() {
        return groupCodeRepository.findAll();
    }

    @Override
    public Page<CodeGroupResponse> getCodeGroups(Pageable pageable, CodeGroupRequest request) throws InvalidSortPropertyException {
        try {
            return groupCodeRepository.findCodeGroupsByConditions(pageable, request);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public Page<GroupCode> getCodes(Pageable pageable, CodeRequest request) throws InvalidSortPropertyException {
        try {
            return groupCodeRepository.findCodesByConditions(pageable, request);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }
}

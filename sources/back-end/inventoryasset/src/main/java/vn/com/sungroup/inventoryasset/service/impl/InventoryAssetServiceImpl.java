/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.service.impl;

import lombok.extern.slf4j.Slf4j;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.AssetInventoryImage;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.ImageModel;
import vn.com.sungroup.inventoryasset.dto.inventoryasset.ResponseMoveFileDto;
import vn.com.sungroup.inventoryasset.dto.request.AssetDetailRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetGetCostcenterRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryChgRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetHistoryInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetInventoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetListRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetModifyRequest;
import vn.com.sungroup.inventoryasset.dto.request.AssetSearchRequest;
import vn.com.sungroup.inventoryasset.dto.response.AssetDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetGetCostcenterResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryChgResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetHistoryInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetInventoryResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetListResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetModifyResponse;
import vn.com.sungroup.inventoryasset.dto.response.AssetSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.MoveFileResponse;
import vn.com.sungroup.inventoryasset.exception.ExternalApiException;
import vn.com.sungroup.inventoryasset.integration.ExternalApiService;
import vn.com.sungroup.inventoryasset.service.InventoryAssetService;
import vn.com.sungroup.inventoryasset.util.BuildInFunction;
import vn.com.sungroup.inventoryasset.util.Constant;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * InventoryAssetServiceImpl class.
 *
 * <p>Contains logics about InventoryAsset.
 */
@Service
@Slf4j
public class InventoryAssetServiceImpl implements InventoryAssetService {

    private static final int STATUS_CODE_OK = 200;

    @Autowired private ExternalApiService externalApiService;

    @Autowired private HttpServletRequest httpServletRequest;

    @PersistenceContext private EntityManager entityManager;
    
    @Value("${external.rest.consume.SAP_HCM_USER}")
    private String externalSAPHcmUser;
    
    @Value("${external.rest.consume.SAP_HCM_PASS}")
    private String externalSapHcmPass;

    @SuppressWarnings("unchecked")
    @Override
    public AssetListResponse assetList(AssetListRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetList() - START");
        try {
            AssetListResponse output = new AssetListResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPIAssetList = new HashMap<>();
            Map<String, Object> requestParamexternalAPIAssetList = new HashMap<>();
            Map<String, String> mapHeaderexternalAPIAssetList = new HashMap<>();
            Map<String, Object> pathVariableexternalAPIAssetList = new HashMap<>();
            requestBodyexternalAPIAssetList.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPIAssetList.put("Authorization", encoding);
            AssetListResponse externalAPIAssetList =
                    externalApiService.SAP_HCMassetList(
                            requestParamexternalAPIAssetList,
                            requestBodyexternalAPIAssetList,
                            pathVariableexternalAPIAssetList,
                            mapHeaderexternalAPIAssetList,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPIAssetList;
            log.info("InventoryAssetServiceImpl assetList() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetList() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetSearchResponse assetSearch(AssetSearchRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetSearch() - START");
        try {
            AssetSearchResponse output = new AssetSearchResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPIAssetSearch = new HashMap<>();
            Map<String, Object> requestParamexternalAPIAssetSearch = new HashMap<>();
            Map<String, String> mapHeaderexternalAPIAssetSearch = new HashMap<>();
            Map<String, Object> pathVariableexternalAPIAssetSearch = new HashMap<>();
            requestBodyexternalAPIAssetSearch.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPIAssetSearch.put("Authorization", encoding);
            AssetSearchResponse externalAPIAssetSearch =
                    externalApiService.SAP_HCMassetSearch(
                            requestParamexternalAPIAssetSearch,
                            requestBodyexternalAPIAssetSearch,
                            pathVariableexternalAPIAssetSearch,
                            mapHeaderexternalAPIAssetSearch,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPIAssetSearch;
            log.info("InventoryAssetServiceImpl assetSearch() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetSearch() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetDetailResponse assetDetail(AssetDetailRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetDetail() - START");
        try {
            AssetDetailResponse output = new AssetDetailResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPIAssetDetail = new HashMap<>();
            Map<String, Object> requestParamexternalAPIAssetDetail = new HashMap<>();
            Map<String, String> mapHeaderexternalAPIAssetDetail = new HashMap<>();
            Map<String, Object> pathVariableexternalAPIAssetDetail = new HashMap<>();
            requestBodyexternalAPIAssetDetail.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPIAssetDetail.put("Authorization", encoding);
            AssetDetailResponse externalAPIAssetDetail =
                    externalApiService.SAP_HCMassetDetail(
                            requestParamexternalAPIAssetDetail,
                            requestBodyexternalAPIAssetDetail,
                            pathVariableexternalAPIAssetDetail,
                            mapHeaderexternalAPIAssetDetail,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPIAssetDetail;
            log.info("InventoryAssetServiceImpl assetDetail() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetDetail() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetModifyResponse assetModify(AssetModifyRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetModify() - START");
        Integer akdStatusCode = STATUS_CODE_OK;
        try {
            AssetModifyResponse output = new AssetModifyResponse();
            try {
                log.info("Asset Modify Input: " + input);
                // External Api Process
                Map<String, Object> requestBodyexternalAPIAssetModify = new HashMap<>();
                Map<String, Object> requestParamexternalAPIAssetModify = new HashMap<>();
                Map<String, String> mapHeaderexternalAPIAssetModify = new HashMap<>();
                Map<String, Object> pathVariableexternalAPIAssetModify = new HashMap<>();
                requestBodyexternalAPIAssetModify.put("input", input);
                String username = externalSAPHcmUser;
                String password = externalSapHcmPass;
                String encoding =
                        "Basic "
                                + Base64.getEncoder()
                                        .encodeToString((username + ":" + password).getBytes());
                mapHeaderexternalAPIAssetModify.put("Authorization", encoding);
                AssetModifyResponse externalAPIAssetModify =
                        externalApiService.SAP_HCMassetModify(
                                requestParamexternalAPIAssetModify,
                                requestBodyexternalAPIAssetModify,
                                pathVariableexternalAPIAssetModify,
                                mapHeaderexternalAPIAssetModify,
                                "POST",
                                Constant.REST_TIMEOUT,
                                0);

                if (null != input.getImage() && BuildInFunction.size(input.getImage()) > 0) {
                    for (ImageModel forEachListImage : input.getImage()) {
                        // External Api Process
                        Map<String, Object> requestBodymoveFileExternalAPI = new HashMap<>();
                        Map<String, Object> requestParammoveFileExternalAPI = new HashMap<>();
                        Map<String, String> mapHeadermoveFileExternalAPI = new HashMap<>();
                        Map<String, Object> pathVariablemoveFileExternalAPI = new HashMap<>();
                        mapHeadermoveFileExternalAPI.put(
                                "Authorization", SecurityUtil.getCurrentUser().getToken());
                        pathVariablemoveFileExternalAPI.put("appCode", "inventoryasset");
                        requestParammoveFileExternalAPI.put("name", forEachListImage.getImageid());
                        mapHeadermoveFileExternalAPI.put("x-ms-blob-type", "BlockBlob");
                        ResponseMoveFileDto moveFileExternalAPI =
                                externalApiService.MoveFilemoveFileStorage(
                                        requestParammoveFileExternalAPI,
                                        requestBodymoveFileExternalAPI,
                                        pathVariablemoveFileExternalAPI,
                                        mapHeadermoveFileExternalAPI,
                                        "POST",
                                        Constant.REST_TIMEOUT,
                                        0);
                    }
                }
                output = externalAPIAssetModify;
                RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
                HttpServletResponse response =
                        ((ServletRequestAttributes) requestAttributes).getResponse();
                if (null != response) {
                    response.setStatus(akdStatusCode);
                }
                log.info("InventoryAssetServiceImpl assetModify() - END");
                return output;

            } catch (ExternalApiException e) {

                akdStatusCode = 400;
                Map<String, Integer> setStatusCode = new HashMap<>();
                setStatusCode.put("Code", akdStatusCode);
                log.error("AssetModify ERROR: " + e.getMessage());
                output.setCode("100");
                output.setError(e.getMessage());
                RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
                HttpServletResponse response =
                        ((ServletRequestAttributes) requestAttributes).getResponse();
                if (null != response) {
                    response.setStatus(akdStatusCode);
                }
                log.info("InventoryAssetServiceImpl assetModify() - END");
                return output;
            }

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetModify() ERROR: ", e);
            throw e;
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public AssetGetCostcenterResponse assetGetCostcenter(AssetGetCostcenterRequest input)
            throws Exception {
        log.info("InventoryAssetServiceImpl assetGetCostcenter() - START");
        try {
            AssetGetCostcenterResponse output = new AssetGetCostcenterResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPICostcenter = new HashMap<>();
            Map<String, Object> requestParamexternalAPICostcenter = new HashMap<>();
            Map<String, String> mapHeaderexternalAPICostcenter = new HashMap<>();
            Map<String, Object> pathVariableexternalAPICostcenter = new HashMap<>();
            requestBodyexternalAPICostcenter.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPICostcenter.put("Authorization", encoding);
            AssetGetCostcenterResponse externalAPICostcenter =
                    externalApiService.SAP_HCMassetGetCostcenter(
                            requestParamexternalAPICostcenter,
                            requestBodyexternalAPICostcenter,
                            pathVariableexternalAPICostcenter,
                            mapHeaderexternalAPICostcenter,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPICostcenter;
            log.info("InventoryAssetServiceImpl assetGetCostcenter() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetGetCostcenter() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetInventoryResponse assetInventory(AssetInventoryRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetInventory() - START");
        Integer akdStatusCode = STATUS_CODE_OK;
        try {
            String imageType = null;
            List<AssetInventoryImage> listImageModel = new ArrayList<>();
            AssetInventoryResponse output = new AssetInventoryResponse();
            try {
                log.info("Asset Inventory Input: " + input);

                if (null != input.getImage() && BuildInFunction.size(input.getImage()) > 0) {
                    for (AssetInventoryImage forEachListImage : input.getImage()) {
                        imageType = forEachListImage.getType();

                        if (null != imageType && imageType.equals("I")) {
                            // External Api Process
                            Map<String, Object> requestBodymoveFileExternalAPI1 = new HashMap<>();
                            Map<String, Object> requestParammoveFileExternalAPI1 = new HashMap<>();
                            Map<String, String> mapHeadermoveFileExternalAPI1 = new HashMap<>();
                            Map<String, Object> pathVariablemoveFileExternalAPI1 = new HashMap<>();
                            mapHeadermoveFileExternalAPI1.put(
                                    "Authorization", SecurityUtil.getCurrentUser().getToken());
                            pathVariablemoveFileExternalAPI1.put("appCode", "inventoryasset");
                            requestParammoveFileExternalAPI1.put(
                                    "name", forEachListImage.getImageid());
                            mapHeadermoveFileExternalAPI1.put("x-ms-blob-type", "BlockBlob");
                            ResponseMoveFileDto moveFileExternalAPI1 =
                                    externalApiService.MoveFilemoveFileStorage(
                                            requestParammoveFileExternalAPI1,
                                            requestBodymoveFileExternalAPI1,
                                            pathVariablemoveFileExternalAPI1,
                                            mapHeadermoveFileExternalAPI1,
                                            "POST",
                                            Constant.REST_TIMEOUT,
                                            0);

                            listImageModel =
                                    (List<AssetInventoryImage>)
                                            BuildInFunction.listAppend(
                                                    listImageModel, forEachListImage);

                        } else if (null != imageType && imageType.equals("R")) {

                        } else {
                            listImageModel =
                                    (List<AssetInventoryImage>)
                                            BuildInFunction.listAppend(
                                                    listImageModel, forEachListImage);
                        }
                    }
                    input.setImage(listImageModel);
                }
                // External Api Process
                Map<String, Object> requestBodyexternalAPIAssetInventory = new HashMap<>();
                Map<String, Object> requestParamexternalAPIAssetInventory = new HashMap<>();
                Map<String, String> mapHeaderexternalAPIAssetInventory = new HashMap<>();
                Map<String, Object> pathVariableexternalAPIAssetInventory = new HashMap<>();
                requestBodyexternalAPIAssetInventory.put("input", input);
                String username = externalSAPHcmUser;
                String password = externalSapHcmPass;
                String encoding =
                        "Basic "
                                + Base64.getEncoder()
                                        .encodeToString((username + ":" + password).getBytes());
                mapHeaderexternalAPIAssetInventory.put("Authorization", encoding);
                AssetInventoryResponse externalAPIAssetInventory =
                        externalApiService.SAP_HCMassetInventory(
                                requestParamexternalAPIAssetInventory,
                                requestBodyexternalAPIAssetInventory,
                                pathVariableexternalAPIAssetInventory,
                                mapHeaderexternalAPIAssetInventory,
                                "POST",
                                Constant.REST_TIMEOUT,
                                0);

                output = externalAPIAssetInventory;
                RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
                HttpServletResponse response =
                        ((ServletRequestAttributes) requestAttributes).getResponse();
                if (null != response) {
                    response.setStatus(akdStatusCode);
                }
                log.info("InventoryAssetServiceImpl assetInventory() - END");
                return output;

            } catch (ExternalApiException e) {

                akdStatusCode = 400;
                Map<String, Integer> setStatusCode = new HashMap<>();
                setStatusCode.put("Code", akdStatusCode);
                log.error("Asset Inventory ERROR: " + e.getMessage());
                output.setCode("100");
                output.setError(e.getMessage());
                RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
                HttpServletResponse response =
                        ((ServletRequestAttributes) requestAttributes).getResponse();
                if (null != response) {
                    response.setStatus(akdStatusCode);
                }
                log.info("InventoryAssetServiceImpl assetInventory() - END");
                return output;
            }

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetInventory() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetHistoryInventoryResponse assetHistoryInventory(AssetHistoryInventoryRequest input)
            throws Exception {
        log.info("InventoryAssetServiceImpl assetHistoryInventory() - START");
        try {
            AssetHistoryInventoryResponse output = new AssetHistoryInventoryResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPIAssetHistoryInventory = new HashMap<>();
            Map<String, Object> requestParamexternalAPIAssetHistoryInventory = new HashMap<>();
            Map<String, String> mapHeaderexternalAPIAssetHistoryInventory = new HashMap<>();
            Map<String, Object> pathVariableexternalAPIAssetHistoryInventory = new HashMap<>();
            requestBodyexternalAPIAssetHistoryInventory.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPIAssetHistoryInventory.put("Authorization", encoding);
            AssetHistoryInventoryResponse externalAPIAssetHistoryInventory =
                    externalApiService.SAP_HCMassetHistoryInventory(
                            requestParamexternalAPIAssetHistoryInventory,
                            requestBodyexternalAPIAssetHistoryInventory,
                            pathVariableexternalAPIAssetHistoryInventory,
                            mapHeaderexternalAPIAssetHistoryInventory,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPIAssetHistoryInventory;
            log.info("InventoryAssetServiceImpl assetHistoryInventory() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetHistoryInventory() ERROR: ", e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public AssetHistoryChgResponse assetHistoryChg(AssetHistoryChgRequest input) throws Exception {
        log.info("InventoryAssetServiceImpl assetHistoryChg() - START");
        try {
            AssetHistoryChgResponse output = new AssetHistoryChgResponse();
            // External Api Process
            Map<String, Object> requestBodyexternalAPIAssetHistoryChg = new HashMap<>();
            Map<String, Object> requestParamexternalAPIAssetHistoryChg = new HashMap<>();
            Map<String, String> mapHeaderexternalAPIAssetHistoryChg = new HashMap<>();
            Map<String, Object> pathVariableexternalAPIAssetHistoryChg = new HashMap<>();
            requestBodyexternalAPIAssetHistoryChg.put("input", input);
            String username = externalSAPHcmUser;
            String password = externalSapHcmPass;
            String encoding =
                    "Basic "
                            + Base64.getEncoder()
                                    .encodeToString((username + ":" + password).getBytes());
            mapHeaderexternalAPIAssetHistoryChg.put("Authorization", encoding);
            AssetHistoryChgResponse externalAPIAssetHistoryChg =
                    externalApiService.SAP_HCMassetHistoryChg(
                            requestParamexternalAPIAssetHistoryChg,
                            requestBodyexternalAPIAssetHistoryChg,
                            pathVariableexternalAPIAssetHistoryChg,
                            mapHeaderexternalAPIAssetHistoryChg,
                            "POST",
                            Constant.REST_TIMEOUT,
                            0);

            output = externalAPIAssetHistoryChg;
            log.info("InventoryAssetServiceImpl assetHistoryChg() - END");
            return output;

        } catch (Exception e) {
            log.error("InventoryAssetServiceImpl assetHistoryChg() ERROR: ", e);
            throw e;
        }
    }
}

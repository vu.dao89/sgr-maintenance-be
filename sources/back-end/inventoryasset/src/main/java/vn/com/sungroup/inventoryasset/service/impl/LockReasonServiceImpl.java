package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.entity.LockReason;
import vn.com.sungroup.inventoryasset.repository.LockReasonRepository;
import vn.com.sungroup.inventoryasset.service.LockReasonService;

@Service
@Transactional
@RequiredArgsConstructor
public class LockReasonServiceImpl implements LockReasonService {
    private final LockReasonRepository lockReasonRepository;

    @Override
    public Page<LockReason> findAll(Pageable pageable) {
        return lockReasonRepository.findAll(pageable);
    }
}

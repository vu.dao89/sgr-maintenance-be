package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.MaintenancePlantRequest;
import vn.com.sungroup.inventoryasset.entity.MaintenancePlant;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.repository.MaintenancePlantRepository;
import vn.com.sungroup.inventoryasset.service.MaintenancePlantService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
@RequiredArgsConstructor
public class MaintenancePlantServiceImpl extends BaseServiceImpl implements MaintenancePlantService {

    private final Logger log = LoggerFactory.getLogger(MaintenancePlantServiceImpl.class);
    private final MaintenancePlantRepository maintenancePlantRepository;
    private final PayloadUtil payloadUtil;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Override
    public void save(vn.com.sungroup.inventoryasset.entity.MaintenancePlant maintenancePlant) {
        maintenancePlantRepository.save(maintenancePlant);
    }

    @Override
    public List<MaintenancePlant> getAll() {
        return maintenancePlantRepository.findAll();
    }

    @Override
    public Page<MaintenancePlant> getMaintenancePlants(Pageable pageable, MaintenancePlantRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        payloadUtil.validateInput(input);
        var maintenancePlantCodesInput = input.getMaintenancePlantCodes();
        if (maintenancePlantCodesInput.isEmpty()) {
            var maintenancePlantIds = getMaintenancePlantIds();
            if (maintenancePlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());
            input.setMaintenancePlantCodes(maintenancePlantIds);
        }

        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());

        return maintenancePlantRepository.findAllByConditions(pageable, input);
    }

    @Override
    public MaintenancePlant getByCode(String code) throws MasterDataNotFoundException {
        return maintenancePlantRepository.findByCode(code).orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }
}

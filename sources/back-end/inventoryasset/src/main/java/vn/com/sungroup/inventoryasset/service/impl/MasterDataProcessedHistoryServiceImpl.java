package vn.com.sungroup.inventoryasset.service.impl;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.entity.MasterDataProcessedHistory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.MasterDataProcessedHistoryRepository;
import vn.com.sungroup.inventoryasset.service.MasterDataProcessedHistoryService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class MasterDataProcessedHistoryServiceImpl implements MasterDataProcessedHistoryService {
    private final Logger LOGGER = LoggerFactory.getLogger(MasterDataProcessedHistoryServiceImpl.class);
    private final MasterDataProcessedHistoryRepository masterDataProcessedHistoryRepository;

    @Override
    public List<MasterDataProcessedHistory> findAll() {
        return masterDataProcessedHistoryRepository.findAll();
    }

    @Override
    public Page<MasterDataProcessedHistory> findAll(Pageable pageable)
        throws InvalidSortPropertyException {
        try {
            return masterDataProcessedHistoryRepository.findAll(pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public Optional<MasterDataProcessedHistory> findOne(UUID id) {
        return masterDataProcessedHistoryRepository.findById(id);
    }

    @Override
    public void saveAll(List<MasterDataProcessedHistory> masterDataProcessedHistories) {
        masterDataProcessedHistoryRepository.saveAll(masterDataProcessedHistories);
    }

    @Override
    public void deleteAll(List<MasterDataProcessedHistory> masterDataProcessedHistories) {
        masterDataProcessedHistoryRepository.deleteAll(masterDataProcessedHistories);
    }

    @Override
    public void updateAll(List<MasterDataProcessedHistory> masterDataProcessedHistories) {
        masterDataProcessedHistoryRepository.saveAll(masterDataProcessedHistories);
    }

    @Override
    public void saveProcessedFiles(String folderName,List<String> fileInfos) {
        for (var fileInfo: fileInfos) {
            var checkExist = masterDataProcessedHistoryRepository.checkExistFileName(fileInfo);
            if (!checkExist)
            {
                var entity = new MasterDataProcessedHistory();
                entity.setFolderName(folderName);
                entity.setFileName(fileInfo);
                entity.setStatus("processed");
                entity.setProcessedAt(LocalDate.now(ZoneOffset.UTC));
                entity.setProcessedAtTime(LocalTime.now(ZoneOffset.UTC));
                masterDataProcessedHistoryRepository.save(entity);
                LOGGER.info(String.format("Processed file: {0}",fileInfo));
            }
        }
    }

    @Override
    public void saveBackedupFiles(List<MasterDataProcessedHistory> entities) {
        for (var entity: entities) {
            if (entity == null)
            {
                entity.setStatus("backedup");
                entity.setBackedupAt(LocalDate.now(ZoneOffset.UTC));
                entity.setBackedupAtTime(LocalTime.now(ZoneOffset.UTC));
                LOGGER.info(String.format("Backedup file: {0}",entity.getFileName()));
                masterDataProcessedHistoryRepository.save(entity);
            }
        }
    }
    @Override
    public void saveBackedupFile(MasterDataProcessedHistory entity) {
        if (entity == null)
        {
            entity.setStatus("backedup");
            entity.setBackedupAt(LocalDate.now(ZoneOffset.UTC));
            entity.setBackedupAtTime(LocalTime.now(ZoneOffset.UTC));
            LOGGER.info(String.format("Backedup file: {0}",entity.getFileName()));
        }
    }
}

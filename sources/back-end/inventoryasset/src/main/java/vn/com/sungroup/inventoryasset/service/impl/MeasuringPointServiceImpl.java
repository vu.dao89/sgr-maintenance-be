package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.entity.MeasuringPoint;
import vn.com.sungroup.inventoryasset.repository.MeasuringPointRepository;
import vn.com.sungroup.inventoryasset.service.MeasuringPointService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Service Implementation for managing
 * {@link vn.com.sungroup.inventoryasset.entity.MeasuringPoint}.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class MeasuringPointServiceImpl implements MeasuringPointService {

    private final Logger log = LoggerFactory.getLogger(MeasuringPointServiceImpl.class);

    private final MeasuringPointRepository measuringPointRepository;

    @Override
    public void delete(UUID id) {
        log.debug("Request to delete Measurepoint : {}", id);
        measuringPointRepository.deleteById(id);
    }

    @Override
    public List<MeasuringPoint> processSaveMeasuringPoints(List<MeasuringPoint> measuringPoints) {
        List<MeasuringPoint> response = new ArrayList<>();
        for (MeasuringPoint measuringPoint : measuringPoints) {
            try {
                MeasuringPoint measuringPointEntity = measuringPointRepository.save(measuringPoint);
                response.add(measuringPointEntity);
            } catch (DataIntegrityViolationException e) {

            }
        }
        return response;
    }

    @Override
    public List<MeasuringPoint> findAll() {
        return measuringPointRepository.findAll();
    }
}

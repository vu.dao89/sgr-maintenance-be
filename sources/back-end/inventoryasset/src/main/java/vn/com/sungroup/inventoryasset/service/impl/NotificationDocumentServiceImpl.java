package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.GlobalDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.NotificationDocumentRepository;
import vn.com.sungroup.inventoryasset.service.NotificationDocumentService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Service
@Transactional
@RequiredArgsConstructor
public class NotificationDocumentServiceImpl implements NotificationDocumentService {

    private final Logger log = LoggerFactory.getLogger(NotificationDocumentServiceImpl.class);
    private final NotificationDocumentRepository notificationDocumentRepository;
    private final GlobalDocumentRepository globalDocumentRepository;
    @Override
    public List<NotificationDocument> findAll() {
        return notificationDocumentRepository.findAll();
    }

    @Override
    public Page<NotificationDocument> findAll(Pageable pageable) throws InvalidSortPropertyException {
        return notificationDocumentRepository.findAll(pageable);
    }

    @Override
    public Optional<NotificationDocument> findOne(UUID id) {
        return notificationDocumentRepository.findById(id);
    }

    @Override
    public void saveAll(List<NotificationDocument> notificationDocuments) {
        notificationDocumentRepository.saveAll(notificationDocuments);
    }

    @Override
    public void deleteAll(List<NotificationDocument> notificationDocuments) {
        notificationDocumentRepository.deleteAll(notificationDocuments);
    }

    @Override
    public void updateAll(List<NotificationDocument> notificationDocuments) {
        notificationDocumentRepository.saveAll(notificationDocuments);
    }

    @Override
    public Page<NotificationDocument> findAllByNotificationCode(NotificationDocumentRequest request, Pageable pageable) {
        return notificationDocumentRepository.findAllByNotificationCode(request.getNotificationCode(), pageable);
    }

    @Override
    public List<NotificationDocument> findAllByNotificationCode(String notificationCode) {
        return notificationDocumentRepository.findAllByNotificationCode(notificationCode);
    }

    @Override
    public NotificationDocument save(NotificationDocumentCreate input) {
        var globalDocument = globalDocumentRepository.findById(input.getDocumentId()).orElse(null);
        var entity = NotificationDocument.builder()
                .globalDocument(globalDocument)
                .notificationCode(input.getNotificationCode()).build();
        return notificationDocumentRepository.save(entity);
    }

    @Override
    public BaseResponseSAP delete(String notificationId, UUID documentId){
        var notificationDocument = notificationDocumentRepository.findByNotificationCodeAndDocumentId(notificationId,
                documentId).orElse(null);
        if(notificationDocument != null){
            notificationDocumentRepository.delete(notificationDocument);

        }
        return new BaseResponseSAP("","","S");
    }
}

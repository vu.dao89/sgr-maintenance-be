package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationActivitiesDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemDto;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemsDto;
import vn.com.sungroup.inventoryasset.dto.request.notification.*;
import vn.com.sungroup.inventoryasset.dto.request.notification.metric.NotificationMetricRequest;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationCloseResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationDetailResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.NotificationSearchResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationItemCreateResponse;
import vn.com.sungroup.inventoryasset.dto.response.notification.sapResponse.NotificationMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.dto.sap.WorkOrderDetailsRequest;
import vn.com.sungroup.inventoryasset.entity.NotificationDocument;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.mapper.NotificationMapper;
import vn.com.sungroup.inventoryasset.repository.common.CommonStatusRepository;
import vn.com.sungroup.inventoryasset.repository.common.PriorityTypeRepository;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;
import vn.com.sungroup.inventoryasset.service.NotificationDocumentService;
import vn.com.sungroup.inventoryasset.service.NotificationService;
import vn.com.sungroup.inventoryasset.service.WorkOrderService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.*;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.Notification_Complete_Code;
import static vn.com.sungroup.inventoryasset.error.Errors.*;
import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.SAP_NOTIFICATION_ID_NOT_FOUND;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotificationServiceImpl extends BaseServiceImpl implements NotificationService {

    private final PriorityTypeRepository priorityTypeRepository;
    private final CommonStatusRepository commonStatusRepository;
    private final NotificationDocumentService notificationDocumentService;

    private final SAPService sapService;
    private final PayloadUtil payloadUtil;
    @Autowired
    private final WorkOrderService workOrderService;

    private final ParseUtils parseUtils;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    @Override
    public NotificationSearchResponse notificationSearch(Pageable pageable,
                                                         NotificationSearchRequest input) throws Exception {
        payloadUtil.validatePersonPermission(Collections.singletonList(input.getEmployeeId()), getPersonnel(), "employeeId");

        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());

        log.info("NotificationServiceImpl notificationSearch() - START");

        long before = System.currentTimeMillis();
        log.info("start SAP: {}", before);
        var result = sapService.notificationSearch(input.toNotificationSearchSAPRequest(pageable));
        long after = System.currentTimeMillis() - before;
        log.info("Process SAP: {}", after);

        var resultDto = NotificationMapper.INSTANCE.mapNotificationSearchResults(result.getDATA());
        long beforeLoop = System.currentTimeMillis();
        log.info("start LOOP: {}", beforeLoop);
        resultDto.forEach(notificationSearchResultDto -> {
            var priorityType = priorityTypeRepository.findByNotificationTypeAndPriority(
                    notificationSearchResultDto.getNotificationType(),
                    notificationSearchResultDto.getPriority()).orElse(null);
            notificationSearchResultDto.setPriorityType(priorityType);

            var status = commonStatusRepository.findByStatusAndCode(notificationSearchResultDto.getEquipmentStatId(), CommonStatusCodeConsts.Notification).orElse(null);
            notificationSearchResultDto.setCommonStatus(status);
        });
        long endLoop = System.currentTimeMillis() - beforeLoop;
        log.info("Process LOOP: {}", endLoop);
        return NotificationSearchResponse.builder()
                .result(resultDto)
                .totalPage(result.getTotalPage())
                .totalItems(result.getTotalItems())
                .build();
    }

    @Override
    public NotificationDetailResponse notificationDetail(NotificationDetailRequest input) throws Exception {
        log.info("NotificationServiceImpl notificationDetail() - START");
        try {
            var result =
                    sapService.notificationDetail(NotificationMapper.INSTANCE.toNotificationDetailSAPRequest(input));
            var notificationDetailResponse = NotificationMapper.INSTANCE.toNotificationDetailResponse(result);
            var priorityType = priorityTypeRepository.findByTypeAndPriority(result.getHeader().getPriorityType(),
                    result.getHeader().getPriority()).orElse(null);
            notificationDetailResponse.getHeader().setPriorityTypeObject(priorityType);
            var status = commonStatusRepository.findByStatusAndCode(result.getHeader().getEquipmentStatId(), CommonStatusCodeConsts.Notification).orElse(null);
            notificationDetailResponse.getHeader().setCommonStatus(status);

            var notificationDocuments = notificationDocumentService.findAllByNotificationCode(notificationDetailResponse.getHeader().getId());
            notificationDetailResponse.setNotificationDocuments(notificationDocuments);
            return notificationDetailResponse;

        } catch (Exception e) {
            log.error("NotificationServiceImpl notificationDetail() ERROR: ", e);
            throw e;
        }
    }

    @Override
    public BaseResponseSAP notificationCreate(NotificationCreateRequest input) throws Exception {
        log.info("NotificationServiceImpl notificationCreate() - START");
        var request = NotificationMapper.INSTANCE.toNotificationCreateSAPRequest(input);
        request.getHeader().setEmail(getEmail());
        var result = sapService.notificationCreate(request);
        if (result != null && input.getDocumentIds() != null && !input.getDocumentIds().isEmpty()) {
            addDocument(result.getDocument(), input.getDocumentIds());
        }
        return result;
    }

    @Override
    public NotificationItemCreateResponse notificationItemCreate(NotificationItemDto input) throws Exception {
        input.setItem("");
        var notificationDetail = notificationDetail(NotificationDetailRequest.builder().notificationId(input.getId()).build());
        validateNotificationChange(notificationDetail.getHeader().getEquipmentStatIdDesc());
        List<String> notificationItemIds;
        if (notificationDetail.getItems() == null) {
            notificationItemIds = new ArrayList<>();
            notificationDetail.setItems(
                    NotificationItemsDto.builder().notificationItems(List.of(input)).build());
        } else {
            notificationItemIds =
                    notificationDetail.getItems().getNotificationItems().stream().map(NotificationItemDto::getItem).collect(Collectors.toList());
            notificationDetail.getItems().getNotificationItems().add(input);
        }

        var notificationChangeRequest = NotificationCreateRequest.builder().header(notificationDetail.getHeader())
                .items(notificationDetail.getItems())
                .activities(notificationDetail.getActivities())
                .build();
        var response = performNotificationChange(notificationChangeRequest);

        notificationDetail = notificationDetail(NotificationDetailRequest.builder().notificationId(input.getId()).build());
        var newNotificationItemIds =
                notificationDetail.getItems().getNotificationItems().stream().map(NotificationItemDto::getItem).collect(Collectors.toList());
        var id = newNotificationItemIds.stream().filter(s -> !notificationItemIds.contains(s)).findFirst().orElse(null);

        return NotificationItemCreateResponse.notificationItemCreateResponseBuilder()
                .notificationItemId(id)
                .document(response.getDocument())
                .message(response.getMessage())
                .code(response.getCode())
                .build();
    }

    @Override
    public BaseResponseSAP notificationItemUpdate(NotificationItemDto input) throws Exception {
        var notificationDetail =
                notificationDetail(NotificationDetailRequest.builder().notificationId(input.getId()).build());
        validateNotificationChange(notificationDetail.getHeader().getEquipmentStatIdDesc());
        var item =
                notificationDetail.getItems().getNotificationItems().stream().filter(notificationItemDto -> notificationItemDto.getItem().equals(input.getItem())).findFirst().orElse(null);
        if (item == null) {
            throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, SAP_NOTIFICATION_ID_NOT_FOUND));
        }

        notificationDetail.getItems().getNotificationItems().remove(item);
        notificationDetail.getItems().getNotificationItems().add(input);
        var notificationChangeRequest = NotificationCreateRequest.builder().header(notificationDetail.getHeader())
                .items(notificationDetail.getItems())
                .activities(notificationDetail.getActivities())
                .build();
        return performNotificationChange(notificationChangeRequest);
    }

    @Override
    public BaseResponseSAP notificationItemDelete(NotificationDeleteRequest input) throws Exception {
        var notificationDetail =
                notificationDetail(NotificationDetailRequest.builder().notificationId(input.getNotificationId()).build());
        validateNotificationChange(notificationDetail.getHeader().getEquipmentStatIdDesc());
        var item =
                notificationDetail.getItems().getNotificationItems().stream().filter(notificationItemDto -> notificationItemDto.getItem().equals(input.getNotificationItemId())).findFirst().orElse(null);
        if (item == null) {
            throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, SAP_NOTIFICATION_ID_NOT_FOUND));
        }
        notificationDetail.getItems().getNotificationItems().remove(item);
        var notificationChangeRequest = NotificationCreateRequest.builder().header(notificationDetail.getHeader())
                .items(notificationDetail.getItems())
                .activities(notificationDetail.getActivities())
                .build();
        return performNotificationChange(notificationChangeRequest);
    }

    @Override
    public NotificationCloseResponse notificationClose(NotificationCloseRequest input) throws Exception {
        log.info("NotificationServiceImpl notificationCreate() - START");

        var notificationDetail = notificationDetail(NotificationDetailRequest.builder().notificationId(input.getId()).build());

        validateNotificationClose(input, notificationDetail);

        if (!isLinkedWorkOrder(notificationDetail)) {
            notificationDetail.setActivities(new NotificationActivitiesDto());
            input.getNotificationActivities().forEach(notificationActivityDto -> notificationActivityDto.setId(input.getId()));
            notificationDetail.getActivities().setNotificationActivities(input.getNotificationActivities());
            var notificationChangeRequest = NotificationCreateRequest.builder().header(notificationDetail.getHeader())
                    .items(notificationDetail.getItems())
                    .activities(notificationDetail.getActivities())
                    .build();
            performNotificationChange(notificationChangeRequest);
        }

        var request = NotificationMapper.INSTANCE.toNotificationCloseSAPRequest(input);
        request.setEmail(getEmail());

        var result = sapService.notificationClose(request);

        return NotificationMapper.INSTANCE.toNotificationCloseResponse(result);
    }

    private void validateNotificationClose(NotificationCloseRequest input,
                                           NotificationDetailResponse notificationDetailResponse) throws SAPApiException,
            InvalidInputException, MasterDataNotFoundException {
        if (isLinkedWorkOrder(notificationDetailResponse)) {
            var workOrderCode = notificationDetailResponse.getHeader().getOrder();
            var workOrder =
                    workOrderService.getDetailsWorkOrder(WorkOrderDetailsRequest.builder().workOrderId(workOrderCode).build());
            if (!isCompletedWorkOrder(workOrder.getWorkOrder().getWorkOrderSystemStatus())) {
                throw new InvalidInputException(NOTIFICATION_LINK_NOT_COMPLETE_WORK_ORDER, NOTIFICATION_LINK_NOT_COMPLETE_WORK_ORDER_MESSAGE);
            }
        } else {
            if (input.getNotificationActivities() == null || input.getNotificationActivities().stream().anyMatch(Objects::isNull)) {
                throw new InvalidInputException(NOTIFICATION_REQUIRE_ACTIVITIES,
                        NOTIFICATION_REQUIRE_ACTIVITIES_MESSAGE);
            }
        }
    }

    private boolean isLinkedWorkOrder(NotificationDetailResponse input) {
        var workOrderCode = input.getHeader().getOrder();
        return !workOrderCode.isBlank() && !workOrderCode.isEmpty();
    }

    private boolean isCompletedWorkOrder(String workOrderStatus) {
        return workOrderStatus.equals("I0009") ||
                workOrderStatus.equals("I0045") ||
                workOrderStatus.equals("I0043") ||
                workOrderStatus.equals("I0046");
    }

    @Override
    public BaseResponseSAP notificationChange(NotificationCreateRequest input, String notificationStatus) throws Exception {
        log.info("NotificationServiceImpl notificationChange() - START");
        try {
            if (!StringUtils.hasText(notificationStatus)) {
                var notificationDetail =
                        sapService.notificationDetail(NotificationMapper.INSTANCE.toNotificationDetailSAPRequest(NotificationDetailRequest.builder().notificationId(input.getHeader().getId()).build()));
                notificationStatus = notificationDetail.getHeader().getEquipmentStatId();
            }

            validateNotificationChange(notificationStatus);

            var result = performNotificationChange(input);
            // update document
            var existingDocuments = notificationDocumentService.findAllByNotificationCode(result.getDocument());
            var documentIds = input.getDocumentIds();
            if (documentIds != null && !documentIds.isEmpty()) {
                var existingDocumentIds =
                        existingDocuments.stream().map(NotificationDocument::getDocumentId).collect(Collectors.toList());
                var newDocumentIds =
                        documentIds.stream().filter(uuid -> !existingDocumentIds.contains(uuid)).collect(Collectors.toList());
                var deleteDocumentIds =
                        existingDocumentIds.stream().filter(uuid -> !documentIds.contains(uuid)).collect(Collectors.toList());

                if (!newDocumentIds.isEmpty()) {
                    addDocument(result.getDocument(), newDocumentIds);
                }

                if (!deleteDocumentIds.isEmpty()) {
                    deleteDocuments(existingDocuments, deleteDocumentIds);
                }
            } else {
                if (!existingDocuments.isEmpty()) {
                    notificationDocumentService.deleteAll(existingDocuments);
                }
            }

            return result;

        } catch (Exception e) {
            log.error("NotificationServiceImpl notificationChange() ERROR: ", e);
            throw e;
        }
    }

    private void validateNotificationChange(String status) throws
            InvalidInputException {

        if (status.equalsIgnoreCase(Notification_Complete_Code)) {
            throw new InvalidInputException(NOTIFICATION_DELETE_CHANGE_ERROR, MessageUtil.getMessage(messageSource,
                    locale, "NOTIFICATION_DELETE_CHANGE_ERROR_MESSAGE"));
        }
    }

    private BaseResponseSAP performNotificationChange(NotificationCreateRequest input) throws SAPApiException {
        var request = NotificationMapper.INSTANCE.toNotificationCreateSAPRequest(input);
        request.getHeader().setEmail(getEmail());
        log.info("Change notification with input: {}", parseUtils.toJsonString(request));
        return sapService.notificationChange(request);
    }

    private void deleteDocuments(List<NotificationDocument> existingDocuments, List<UUID> deleteDocumentIds) {
        var deleteDocuments =
                existingDocuments.stream().filter(notificationDocument -> deleteDocumentIds.contains(notificationDocument.getDocumentId())).collect(Collectors.toList());
        notificationDocumentService.deleteAll(deleteDocuments);
    }

    private void addDocument(String notificationId, List<UUID> newDocumentIds) {
        newDocumentIds.forEach(newDocumentId -> {
            notificationDocumentService.save(NotificationDocumentCreate.builder()
                    .documentId(newDocumentId)
                    .notificationCode(notificationId)
                    .build());
        });
    }

    @Override
    public NotificationMetricResponse getNotificationMetricFromSAP(NotificationMetricRequest input)
            throws SAPApiException, InvalidInputException {
        log.info("NotificationServiceImpl getNotificationMetric() - START");
        payloadUtil.validatePersonPermission(Collections.singletonList(input.getEmployeeId()), getPersonnel(), "employeeId");
        return sapService.getNotificationMetric(input.toNotificationMetricRequestSAP());
    }
}

package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationTypeDto;
import vn.com.sungroup.inventoryasset.dto.request.NotificationTypeRequest;
import vn.com.sungroup.inventoryasset.entity.NotificationType;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.repository.NotificationTypeRepository;
import vn.com.sungroup.inventoryasset.repository.WorkOrderTypeRepository;
import vn.com.sungroup.inventoryasset.service.NotificationTypeService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class NotificationTypeServiceImpl implements NotificationTypeService {

    private final Logger log = LoggerFactory.getLogger(NotificationTypeServiceImpl.class);
    private final NotificationTypeRepository notificationTypeRepository;
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Autowired
    private WorkOrderTypeRepository workOrderTypeRepository;

    @Override
    public Page<NotificationType> getNotificationTypeByFilter(NotificationTypeRequest request,
                                                              Pageable pageable) throws InvalidSortPropertyException {
        try {
            return notificationTypeRepository.findNotificationTypeByFilter(request, pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public NotificationType findByType(String code, String maintenancePlantCode) throws MasterDataNotFoundException {
        var notificationType = notificationTypeRepository.findByNotiType(code).orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
        var workOrderType = workOrderTypeRepository.findByTypeAndMaintenancePlantId(notificationType.getWorkOrderTypeId(),
                maintenancePlantCode).orElse(null);
        notificationType.setWorkOrderType(workOrderType);

        return notificationType;
    }

    @Override
    public void save(NotificationTypeDto notificationType) {
        notificationTypeRepository.save(NotificationType.builder()
                .id(notificationType.getId())
                .notiType(notificationType.getNotiType())
                .description(notificationType.getDescription())
                .catalogProfile(notificationType.getCatalogProfile())
                .problems(notificationType.getProblems())
                .causes(notificationType.getCauses())
                .activities(notificationType.getActivities())
                .objectParts(notificationType.getObjectParts())
                .workOrderTypeId(notificationType.getWorkOrderTypeId())
                .priorityTypeId(notificationType.getPriorityTypeId())
                .personResponsible(notificationType.getPersonResponsible())
                .build());
    }

    @Override
    public List<NotificationType> findAll() {
        return notificationTypeRepository.findAll();
    }
}

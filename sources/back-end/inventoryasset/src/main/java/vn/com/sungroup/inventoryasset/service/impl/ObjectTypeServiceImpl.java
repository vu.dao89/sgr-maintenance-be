package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.ObjectTypeRequest;
import vn.com.sungroup.inventoryasset.entity.ObjectType;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.repository.ObjectTypeRepository;
import vn.com.sungroup.inventoryasset.service.ObjectTypeService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class ObjectTypeServiceImpl implements ObjectTypeService {

    private final Logger log = LoggerFactory.getLogger(ObjectTypeServiceImpl.class);
    private final ObjectTypeRepository objectTypeRepository;
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    @Override
    public void saveAll(List<ObjectType> objectTypes) {
        objectTypeRepository.saveAll(objectTypes);
    }

    @Override
    public ObjectType save(ObjectType objectType) {
        return objectTypeRepository.save(objectType);
    }

    @Override
    public List<ObjectType> findAll() {
        return objectTypeRepository.findAll();
    }

    @Override
    public Page<ObjectType> getObjectTypeByFilter(ObjectTypeRequest request, Pageable pageable)
            throws InvalidSortPropertyException {
        try {
            return objectTypeRepository.findObjectTypeByFilter(request, pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public ObjectType getByCode(String code) throws MasterDataNotFoundException {
        return objectTypeRepository.findByCode(code).orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }
}

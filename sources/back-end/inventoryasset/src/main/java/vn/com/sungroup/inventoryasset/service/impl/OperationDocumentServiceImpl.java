package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.sap.BaseResponseSAP;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.repository.GlobalDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.OperationDocumentRepository;
import vn.com.sungroup.inventoryasset.service.OperationDocumentService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class OperationDocumentServiceImpl implements OperationDocumentService {
    private final Logger log = LoggerFactory.getLogger(OperationDocumentServiceImpl.class);
    private final OperationDocumentRepository operationDocumentRepository;
    private final GlobalDocumentRepository globalDocumentRepository;

    @Override
    public List<OperationDocument> findAll() {
        return operationDocumentRepository.findAll();
    }

    @Override
    public Page<OperationDocument> findAll(Pageable pageable) throws InvalidSortPropertyException {
        return operationDocumentRepository.findAll(pageable);
    }

    @Override
    public Optional<OperationDocument> findOne(UUID id) {
        return operationDocumentRepository.findById(id);
    }

    @Override
    public void saveAll(List<OperationDocument> operationDocuments) {
        operationDocumentRepository.saveAll(operationDocuments);
    }

    @Override
    public void deleteAll(List<OperationDocument> operationDocuments) {
        operationDocumentRepository.deleteAll(operationDocuments);
    }

    @Override
    public void updateAll(List<OperationDocument> operationDocuments) {
        operationDocumentRepository.saveAll(operationDocuments);
    }

    @Override
    public Page<OperationDocument> findAllByOperationCode(OperationDocumentRequest request, Pageable pageable) {
        return operationDocumentRepository.findAllByOperationCode(request.getOperationCode(), pageable);
    }

    @Override
    public List<OperationDocument> findAllByWorkOrderIdAndOperationCode(OperationDocumentRequest request) {
        return operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(request.getWorkOrderId(), request.getOperationCode(), request.getSuperOperationCode());
    }

    @Override
    public OperationDocument save(OperationDocumentCreate input) {
        var globalDocument = globalDocumentRepository.findById(input.getDocumentId()).orElse(null);
        var entity = OperationDocument.builder()
                .workOrderId(input.getWorkOrderId())
                .globalDocument(globalDocument)
                .superOperationCode(input.getSuperOperationCode())
                .operationCode(input.getOperationCode()).build();
        return operationDocumentRepository.save(entity);
    }

    @Override
    public BaseResponseSAP delete(String workOrderId, String operationId, String documentId, String superOperationId) {
        OperationDocument operationDocument = operationDocumentRepository.findByWorkOrderIdAndOperationCodeAndSuperOperationCodeAndDocumentId(workOrderId,
                operationId, superOperationId, UUID.fromString(documentId)).orElse(null);
        if(operationDocument != null){
            operationDocumentRepository.delete(operationDocument);
        }

        return new BaseResponseSAP("","","S");
    }
}

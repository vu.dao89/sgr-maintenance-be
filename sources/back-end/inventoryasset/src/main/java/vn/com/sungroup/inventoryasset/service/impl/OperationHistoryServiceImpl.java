package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.OperationDetailRequest;
import vn.com.sungroup.inventoryasset.dto.response.operation.OperationDetailResponse;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.repository.OperationHistoryRepository;
import vn.com.sungroup.inventoryasset.service.OperationHistoryService;
import vn.com.sungroup.inventoryasset.service.OperationService;
import vn.com.sungroup.inventoryasset.service.PersonalService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;
import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.*;

@Service
@Transactional
@RequiredArgsConstructor
public class OperationHistoryServiceImpl extends BaseServiceImpl implements OperationHistoryService {
    private final Logger log = LoggerFactory.getLogger(OperationHistoryServiceImpl.class);
    private final OperationHistoryRepository operationHistoryRepository;
    private final OperationService operationService;
    private final PersonalService personalService;

    @Override
    public List<OperationHistory> findAll() {
        return operationHistoryRepository.findAll();
    }

    @Override
    public Page<OperationHistory> findAll(Pageable pageable) throws InvalidSortPropertyException {
        return operationHistoryRepository.findAll(pageable);
    }

    @Override
    public Optional<OperationHistory> findOne(UUID id) {
        return operationHistoryRepository.findById(id);
    }

    @Override
    public void saveAll(List<OperationHistory> operationHistories) {
        operationHistoryRepository.saveAll(operationHistories);
    }

    @Override
    public void deleteAll(List<OperationHistory> operationHistories) {
        operationHistoryRepository.deleteAll(operationHistories);
    }

    @Override
    public void updateAll(List<OperationHistory> operationHistories) {
        operationHistoryRepository.saveAll(operationHistories);
    }

    @Override
    public Page<OperationHistory> getByFilter(OperationHistoryRequest request, Pageable pageable)
            throws InvalidSortPropertyException {
        try {
            return operationHistoryRepository.getByFilter(request, pageable);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }

    @Override
    public OperationHistory getCurrentActive() {
        var operationCurrent = operationHistoryRepository.getCurrentActive(getPersonnel());
        return operationCurrent;
    }

    @Override
    public List<OperationHistory> getOperationsNotComplete(String employeeId) {
        if (employeeId == null || employeeId.trim().isEmpty() || employeeId.trim().isBlank()) {
            employeeId = getPersonnel();
        }
        return operationHistoryRepository.findByWorkOrderIdAndOperationCodeNotComplete(employeeId);
    }

    @Override
    public OperationHistory getStartingOperation(String employeeId) {
        if (employeeId == null || employeeId.trim().isEmpty() || employeeId.trim().isBlank()) {
            employeeId = getPersonnel();
        }

        return operationHistoryRepository.getStartingOperationHistory(employeeId);
    }

    @Override
    public OperationDetailResponse collectOperationHistory(OperationHistory operationHistory) throws SAPApiException {
        if (operationHistory != null) {
            var request = OperationDetailRequest.builder()
                    .workOrderId(operationHistory.getWorkOrderId())
                    .operationId(operationHistory.getOperationCode()).build();
            var operationDetailResponse = operationService.getDetailFromSap(request);

            updateMissingHistories(operationDetailResponse, operationHistory);

            var status = operationDetailResponse.getOperation().getOperationSystemStatus();
            if (getPersonnel().equals(operationDetailResponse.getOperation().getPersonnel())
                    && (status.equalsIgnoreCase(Operation_Complete_A_Half_Code) ||
                        status.equalsIgnoreCase(Operation_Release_Code))
            ) {
                return operationDetailResponse;
            }
        }

        return null;
    }

    private void updateMissingHistories(OperationDetailResponse operationDetailResponse, OperationHistory latestHistory) {
        List<String> completedStatuses = new ArrayList<>();
        completedStatuses.add("I0009"); // done - Thực hiện xong công việc
        completedStatuses.add("I0045"); // closed/completed - Đã đóng
        completedStatuses.add("I0043"); // locked - Đã khóa
        completedStatuses.add("I0046"); // Đã tính giá thành
        var currentActivitySapOperation = "";
        if (completedStatuses.contains(operationDetailResponse.getOperation().getOperationSystemStatus())) {
            currentActivitySapOperation = Operation_Activity_Complete;
        }

        if (!currentActivitySapOperation.isEmpty()
                && !latestHistory.getActivity().equalsIgnoreCase(currentActivitySapOperation)) {
            // add new record: new status + new userId if user Id changed
            var entity = OperationHistory.builder()
                    .workOrderId(operationDetailResponse.getOperation().getWorkOrderId())
                    .operationCode(operationDetailResponse.getOperation().getOperationId())
                    .userId(operationDetailResponse.getOperation().getPersonnel())
                    .activity(currentActivitySapOperation)
                    .startAtDate(LocalDate.now(ZoneOffset.UTC))
                    .startAtTime(LocalTime.now(ZoneOffset.UTC))
                    .note("Fill Missing Histories")
                    .build();
            saveOne(entity);

            // update old history
            if (!latestHistory.getUserId().equals(operationDetailResponse.getOperation().getPersonnel())) {
                addTransferredUserId(operationDetailResponse, latestHistory);
            }
        } else {
            if (!latestHistory.getUserId().equals(operationDetailResponse.getOperation().getPersonnel())) {
                var entity = OperationHistory.builder()
                        .workOrderId(operationDetailResponse.getOperation().getWorkOrderId())
                        .operationCode(operationDetailResponse.getOperation().getOperationId())
                        .userId(operationDetailResponse.getOperation().getPersonnel())
                        .activity(latestHistory.getActivity())
                        .startAtDate(LocalDate.now(ZoneOffset.UTC))
                        .startAtTime(LocalTime.now(ZoneOffset.UTC))
                        .note("Fill Missing Histories")
                        .build();
                saveOne(entity);

                addTransferredUserId(operationDetailResponse, latestHistory);
            }
        }
    }

    private void addTransferredUserId(OperationDetailResponse operationDetailResponse, OperationHistory latestHistory) {
        latestHistory.setTransferredUserId(operationDetailResponse.getOperation().getPersonnel());
        latestHistory.setNote("Update Transferred User Id");
        saveOne(latestHistory);
    }

    @Override
    public OperationHistory saveOne(OperationHistory operationHistory) {
        return operationHistoryRepository.save(operationHistory);
    }

    @Override
    public OperationHistory getOperationHistory(String workOrderId, String operationId, String activity) {
        var check = operationHistoryRepository.findTopByWorkOrderIdAndOperationCodeAndActivity(workOrderId, operationId, activity);
        if (check.isPresent()) {
            return check.get();
        } else {
            return null;
        }
    }
}

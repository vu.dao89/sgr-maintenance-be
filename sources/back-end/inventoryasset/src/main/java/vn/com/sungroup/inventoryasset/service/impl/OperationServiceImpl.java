package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.dto.operation.OperationDocumentCreate;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryCreate;
import vn.com.sungroup.inventoryasset.dto.request.OperationHistoryUpdate;
import vn.com.sungroup.inventoryasset.dto.request.equipment.PostMeasPointRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.*;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.MaterialDocumentItemSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationDetailRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationSearchRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.operation.OperationSearchRequestSAP.OperationSearchRequestSAPBuilder;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmation;
import vn.com.sungroup.inventoryasset.dto.request.workorder.confirmation.OperationConfirmationRequest;
import vn.com.sungroup.inventoryasset.dto.response.OperationConfirmationResponse;
import vn.com.sungroup.inventoryasset.dto.response.operation.*;
import vn.com.sungroup.inventoryasset.dto.response.operation.sapResponse.OperationMetricsSAP;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.entity.GlobalDocument;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.entity.OperationHistory;
import vn.com.sungroup.inventoryasset.error.Errors;
import vn.com.sungroup.inventoryasset.exception.*;
import vn.com.sungroup.inventoryasset.mapper.OperationMapper;
import vn.com.sungroup.inventoryasset.mapper.OperationPersonnelMapper;
import vn.com.sungroup.inventoryasset.repository.EmployeeRepository;
import vn.com.sungroup.inventoryasset.repository.GlobalDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.OperationDocumentRepository;
import vn.com.sungroup.inventoryasset.repository.UnitOfMeasureRepository;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;
import vn.com.sungroup.inventoryasset.service.*;
import vn.com.sungroup.inventoryasset.util.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.OPERATION_TYPE_CHANGE;
import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.*;
import static vn.com.sungroup.inventoryasset.error.Errors.*;
import static vn.com.sungroup.inventoryasset.util.PayloadUtil.QUANTITY_MATERIAL_INVALID;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@Slf4j
public class OperationServiceImpl extends BaseServiceImpl implements OperationService {

    private final SAPService sapService;
    private final OperationMapper operationMapper;
    private final PayloadUtil payloadUtil;
    private final GlobalDocumentRepository globalDocumentRepository;
    private final OperationDocumentRepository operationDocumentRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;
    private final EmployeeRepository employeeRepository;
    @Lazy
    private final WorkOrderService workOrderService;
    private final ParseUtils parseUtils;
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    private final ConfirmationService confirmationService;

    private final OperationHistoryService operationHistoryService;

    private final GlobalDocumentService globalDocumentService;
    private final OperationDocumentService operationDocumentService;
    private final EquipmentService equipmentService;
    private final CommonService commonService;

    private static void validateVarianceReasonCode(String varianceReasonCode, String confirmation,
                                                   List<MultipartFile> files, boolean isValidateFiles) throws MissingRequiredFieldException {
        var code = varianceReasonCode.toUpperCase();
        if (code.equals("S/M") ||
                code.equals("F") ||
                code.equals("N/A")) {
            if (confirmation == null || confirmation.isEmpty() || confirmation.isBlank()) {
                throw new MissingRequiredFieldException(MISSING_REQUIRED_FIELD, "confirmation");
            }

            if (isValidateFiles && files == null || files.isEmpty()) {
                throw new MissingRequiredFieldException(MISSING_REQUIRED_FIELD, "files");
            }
        }
    }

    private static void validateOperationMeasuringPoint(OperationDetailResponse operationDetailResponse) throws InvalidInputException {
        if (operationDetailResponse.getMeasure() != null &&
                operationDetailResponse.getMeasure().getItems() != null &&
                !operationDetailResponse.getMeasure().getItems().isEmpty()) {
            for (OperationMeasureDetailSAP operationMeasureDetail : operationDetailResponse.getMeasure().getItems()) {
                if (operationMeasureDetail.getResult() == null) {
                    throw new InvalidInputException(OPERATION_MEASURE_NOT_COMPLETED, OPERATION_MEASURE_NOT_COMPLETED_MESSAGE);
                }
            }
        }
    }

    @Override
    public OperationDetailResponse getDetailFromSap(OperationDetailRequest input)
            throws SAPApiException {

        var response = sapService.getOperationDetail(operationMapper.toOperationSAP(input));
        var personnelCode = response.getOperation().getPersonnel();
        var employee = employeeRepository.findByEmployeeId(personnelCode).orElse(null);
        response.getOperation().setEmployee(employee);

        collectEmployeeInSubOperation(response);
        collectUnitOfMeasurement(response);

        return response;
    }

    private void collectUnitOfMeasurement(OperationDetailResponse response) {
        List<String> unitCodes = new ArrayList<>();
        if (response.getComponent() != null && response.getComponent().getItem() != null &&
                !response.getComponent().getItem().isEmpty()) {
            var componentUnits =
                    response.getComponent().getItem().stream().map(ComponentDetailItem::getUnit).collect(Collectors.toList());

            unitCodes.addAll(componentUnits);
        }

        if (response.getMeasure() != null && response.getMeasure().getItems() != null &&
                !response.getMeasure().getItems().isEmpty()) {
            var measureUnits =
                    response.getMeasure().getItems().stream().map(OperationMeasureDetailSAP::getRangeUnit).collect(Collectors.toList());

            unitCodes.addAll(measureUnits);
        }

        var units = unitOfMeasureRepository.findAllByCodeIn(unitCodes.stream().distinct().collect(Collectors.toList()));
        if (response.getComponent() != null && response.getComponent().getItem() != null &&
                !response.getComponent().getItem().isEmpty()) {
            response.getComponent().getItem().forEach(componentDetailItem -> {
                units.stream().filter(unitOfMeasurement -> unitOfMeasurement.getCode().equals(componentDetailItem.getUnit())).findFirst().ifPresent(componentDetailItem::setUnitOfMeasurement);
            });
        }

        if (response.getMeasure() != null && response.getMeasure().getItems() != null &&
                !response.getMeasure().getItems().isEmpty()) {
            response.getMeasure().getItems().forEach(operationMeasureDetailSAP -> {
                units.stream().filter(unitOfMeasurement -> unitOfMeasurement.getCode().equals(operationMeasureDetailSAP.getRangeUnit())).findFirst().ifPresent(operationMeasureDetailSAP::setUnitOfMeasurement);
            });
        }
    }

    private void collectEmployeeInSubOperation(OperationDetailResponse response) {
        if (response.getSubOperation() != null && response.getSubOperation().getItem() != null &&
                !response.getSubOperation().getItem().isEmpty()) {
            var personnelCodes =
                    response.getSubOperation().getItem().stream().map(SubOperationDetailItem::getPersonnel).distinct().collect(Collectors.toList());
            var employees = employeeRepository.findAllByEmployeeIdIsIn(personnelCodes);
            response.getSubOperation().getItem().forEach(subOperationDetailItem -> {
                var existingEmployee =
                        employees.stream().filter(employee1 -> employee1.getEmployeeId().equals(subOperationDetailItem.getPersonnel())).findFirst().orElse(null);
                subOperationDetailItem.setEmployee(existingEmployee);
            });

        }
    }

    @Override
    public SubOperationDetailResponse getSubDetailFromSap(OperationDetailRequest input)
            throws SAPApiException {
        return sapService.getSubOperationDetail(operationMapper.toOperationSAP(input));
    }

    @Override
    public OperationSearchResponse searchOperation(Pageable pageable, OperationSearchRequest input)
            throws SAPApiException, InvalidInputException {
        payloadUtil.validatePersonPermission(input.getPersonnels(), getPersonnel(), "personnel");
        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());
        return sapService.searchOperation(buildOperationSearchRequestSAP(pageable, input));
    }

    @Override
    public OperationMeasuresSAP getOperationMeasures(OperationMeasuresRequestSAP request)
            throws SAPApiException {
        return sapService.getOperationMeasures(request);
    }

    @Override
    public OperationComponentResponse getOperationComponentFromSAP(OperationDetailRequest input)
            throws SAPApiException {
        var operationComponentResponse = sapService.getOperationComponent(operationMapper.toOperationSAP(input));
        if (operationComponentResponse.getComponent() != null && operationComponentResponse.getComponent().getItem() != null
                && !operationComponentResponse.getComponent().getItem().isEmpty()) {
            var unitCodes =
                    operationComponentResponse.getComponent().getItem().stream().map(OperationComponentItemSAP::getUnit).distinct().collect(Collectors.toList());
            var unitOfMeasurements = unitOfMeasureRepository.findAllByCodeIn(unitCodes);

            operationComponentResponse.getComponent().getItem().forEach(operationComponentItemSAP -> {
                unitOfMeasurements.stream().filter(unitOfMeasurement -> unitOfMeasurement.getCode().equals(operationComponentItemSAP.getUnit())).findFirst().ifPresent(operationComponentItemSAP::setUnitOfMeasurement);
            });
        }

        return operationComponentResponse;
    }

    @Override
    public OperationMetricsSAP getOperationMetrics(OperationMetricsRequest request)
            throws SAPApiException, InvalidInputException {
        payloadUtil.validatePersonPermission(Collections.singletonList(request.getMainPerson()), getPersonnel(), "mainPerson");
        // validate request
        String finishFrom = request.getOperationFinishFrom();
        String finishEnd = request.getOperationFinishTo();

        if (!StringUtils.hasText(finishFrom) && !StringUtils.hasText(finishEnd)) {
            finishFrom = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getFirstDayOfMonth(),
                    DateTimeFormatter.BASIC_ISO_DATE);
            finishEnd = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getLastDayOfMonth(),
                    DateTimeFormatter.BASIC_ISO_DATE);
        }

        OperationMetricsRequestSAP requestSAP = OperationMetricsRequestSAP.builder()
                .maintenancePlant(request.convertToMaintenancePlant())
                .mainPerson(request.getMainPerson())
                .personnelRes(request.getPersonnelRes())
                .operationFinishFrom(finishFrom)
                .operationFinishTo(finishEnd)
                .build();
        return sapService.getOperationMetrics(requestSAP);
    }

    @Override
    public BaseResponseSAP changeSubOperation(OperationChangeRequest request) throws SAPApiException, InvalidInputException {
        validateAddDeleteSubOperation(request);

        var response = changeOperation(request);
        addUpdateSubDocuments(request.getDocumentIds(), response.getDocument(),
                request.getWorkOrderId(), request.getSuperOperationId());

        return response;
    }

    private void validateAddDeleteSubOperation(OperationChangeRequest request) throws SAPApiException, InvalidInputException {
        var subOperationDetailResponse =
                getSubDetailFromSap(OperationDetailRequest.builder().workOrderId(request.getWorkOrderId()).operationId(request.getOperationId()).build());
        if(subOperationDetailResponse.getSubOperation() != null && subOperationDetailResponse.getSubOperation().getItem() != null &&
        !subOperationDetailResponse.getSubOperation().getItem().isEmpty()){
            if(request.getType().equalsIgnoreCase("delete") || request.getType().equalsIgnoreCase("change")){
                var subOperation =
                        subOperationDetailResponse.getSubOperation().getItem().stream().filter(subOperationDetailItem ->
                                subOperationDetailItem.getOperationId().equalsIgnoreCase(request.getOperationId()))
                                .findFirst().orElse(null);
                if(subOperation != null){
                    validateUpdateDeleteOperationStatus(subOperation.getOperationSystemStatus());
                }
            }
        }
    }

    private BaseResponseSAP changeOperation(OperationChangeRequest request)
            throws SAPApiException {
        var requestSAP = operationMapper.toOperationChangeRequestSAP(request);
        requestSAP.updateDescription();
        requestSAP.setEmail(getEmail());
        requestSAP.setType(request.getType().toUpperCase());
        return sapService.changeOperation(requestSAP);
    }

    @Override
    public UpdateOperationsResponse createOperation(OperationCreateRequest request)
            throws SAPApiException, InvalidInputException {
        payloadUtil.validateEquipmentAndFunctionalLocation(request.getEquipmentId(), request.getFunctionalLocationId());
        var subOperations = request.getSubOperations();
        if (subOperations != null && !subOperations.isEmpty()) {
            for (OperationCreateRequest subOperation : subOperations) {
                payloadUtil.validateEquipmentAndFunctionalLocation(subOperation.getEquipmentId(), subOperation.getFunctionalLocationId());
            }
        }
        var email = getEmail();

        var requestSAP = operationMapper.toOperationChangeRequestSAP(request);
        requestSAP.updateDescription();
        requestSAP.setEmail(email);
        requestSAP.setType(request.getType().toUpperCase());
        var operation = sapService.changeOperation(requestSAP);
        // add attachment file
        addDocuments(request.getDocumentIds(), operation.getDocument(), request.getWorkOrderId());

        var updateOperationsResponse = operation.toUpdateOperationsResponse();
        List<SubOperationResponse> subOperationResponses = new ArrayList<>();


        if (subOperations != null && !subOperations.isEmpty()) {
            subOperations.forEach(subOperation -> {
                try {
                    subOperation.setSuperOperationId(updateOperationsResponse.getDocument());
                    var subOperationSAP = operationMapper.toOperationChangeRequestSAP(subOperation);
                    subOperationSAP.setEmail(email);
                    subOperationSAP.updateDescription();
                    subOperationSAP.setType(subOperation.getType().toUpperCase());
                    var operationResponse = sapService.changeOperation(subOperationSAP);
                    // add attachment files
                    addSubDocuments(subOperation.getDocumentIds(), operationResponse.getDocument(),
                            subOperation.getWorkOrderId(),
                            subOperation.getSuperOperationId());

                    var subOperationResponse = operationResponse.toSubOperationResponse();
                    subOperationResponse.setLocalId(subOperation.getLocalId());
                    subOperationResponses.add(subOperationResponse);

                } catch (SAPApiException e) {
                    log.error(e.getMessage());
                    var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                            .message(e.getMessage())
                            .code("E").build();
                    subOperationResponse.setLocalId(subOperation.getLocalId());
                    subOperationResponses.add(subOperationResponse);
                }
            });
        }
        updateOperationsResponse.setSubOperations(subOperationResponses);
        return updateOperationsResponse;
    }

    private void addDocuments(List<UUID> documentIds, String operationCode, String workOrderCode) {
        if (documentIds != null && !documentIds.isEmpty()) {
            documentIds.forEach(documentId -> {
                var document = globalDocumentRepository.findById(documentId);
                if (document.isPresent()) {
                    var operationDocument = OperationDocument.builder()
                            .workOrderId(workOrderCode)
                            .operationCode(operationCode)
                            .globalDocument(document.get())
                            .build();
                    operationDocumentRepository.save(operationDocument);
                }
            });
        }
    }

    private void addSubDocuments(List<UUID> documentIds, String operationCode, String workOrderCode,
                                 String superOperationCode) {
        if (documentIds != null && !documentIds.isEmpty()) {
            documentIds.forEach(documentId -> {
                var document = globalDocumentRepository.findById(documentId);
                if (document.isPresent()) {
                    var operationDocument = OperationDocument.builder()
                            .workOrderId(workOrderCode)
                            .operationCode(operationCode)
                            .superOperationCode(superOperationCode)
                            .globalDocument(document.get())
                            .build();
                    operationDocumentRepository.save(operationDocument);
                }
            });
        }
    }

    private void deleteAllDocuments(String operationCode, String workOrderCode) {
        var existingDocuments = operationDocumentRepository.findByOperationCodeAndWorkOrderId(operationCode,
                workOrderCode);
        if (!existingDocuments.isEmpty()) {
            existingDocuments.forEach(operationDocument -> {
                operationDocumentRepository.delete(operationDocument);
            });
        }
    }

    private void deleteDocuments(List<UUID> documentIds) {
        var existingDocuments = operationDocumentRepository.findByDocumentIdIn(documentIds);
        if (!existingDocuments.isEmpty()) {
            existingDocuments.forEach(operationDocument -> {
                operationDocumentRepository.delete(operationDocument);
            });
        }
    }

    private void addUpdateDocuments(List<UUID> documentIds, String operationCode, String workOrderCode) {
        if (documentIds == null || documentIds.isEmpty()) {
            // delete all existing document
            deleteAllDocuments(operationCode, workOrderCode);
        } else {
            var existingDocuments = operationDocumentRepository.findByOperationCodeAndWorkOrderId(operationCode,
                    workOrderCode);
            var existingDocumentIds =
                    existingDocuments.stream().map(OperationDocument::getDocumentId).collect(Collectors.toList());
            var newDocumentIds =
                    documentIds.stream().filter(uuid -> !existingDocumentIds.contains(uuid)).collect(Collectors.toList());
            var deleteDocumentIds =
                    existingDocumentIds.stream().filter(uuid -> !documentIds.contains(uuid)).collect(Collectors.toList());
            if (!newDocumentIds.isEmpty()) {
                addDocuments(newDocumentIds, operationCode, workOrderCode);
            }

            if (!deleteDocumentIds.isEmpty()) {
                deleteDocuments(deleteDocumentIds);
            }
        }
    }

    private void addUpdateSubDocuments(List<UUID> documentIds, String operationCode, String workOrderCode,
                                       String superOperationCode) {
        if (documentIds == null || documentIds.isEmpty()) {
            deleteAllDocuments(operationCode, workOrderCode);
        } else {
            var existingDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(
                    workOrderCode, operationCode, superOperationCode);
            var existingDocumentIds =
                    existingDocuments.stream().map(OperationDocument::getDocumentId).collect(Collectors.toList());
            var newDocumentIds =
                    documentIds.stream().filter(uuid -> !existingDocumentIds.contains(uuid)).collect(Collectors.toList());
            var deleteDocumentIds =
                    existingDocumentIds.stream().filter(uuid -> !documentIds.contains(uuid)).collect(Collectors.toList());
            if (!newDocumentIds.isEmpty()) {
                addSubDocuments(newDocumentIds, operationCode, workOrderCode, superOperationCode);
            }

            if (!deleteDocumentIds.isEmpty()) {
                deleteDocuments(deleteDocumentIds);
            }
        }
    }

    @Override
    public UpdateOperationsResponse updateOperation(OperationCreateRequest request)
            throws SAPApiException, InvalidInputException, OperationNotFoundException, MasterDataNotFoundException {

        var operationDetail =
                sapService.getOperationDetail(operationMapper.toOperationSAP(OperationDetailRequest.builder().workOrderId(request.getWorkOrderId()).operationId(request.getOperationId()).build()));
        var status = operationDetail.getOperation().getOperationSystemStatus();
        validateUpdateDeleteOperationStatus(status);
        payloadUtil.validateEquipmentAndFunctionalLocation(request.getEquipmentId(), request.getFunctionalLocationId());
        var subOperations = request.getSubOperations();
        if (subOperations != null && !subOperations.isEmpty()) {
            for (OperationCreateRequest subOperation : subOperations) {
                payloadUtil.validateEquipmentAndFunctionalLocation(subOperation.getEquipmentId(), subOperation.getFunctionalLocationId());
            }
        }

        var email = getEmail();
        var requestSAP = operationMapper.toOperationChangeRequestSAP(request);
        requestSAP.setEmail(email);
        requestSAP.updateDescription();
        requestSAP.setType(request.getType().toUpperCase());
        var operation = sapService.changeOperation(requestSAP);
        var response = operation.toUpdateOperationsResponse();
        // add/update document
        addUpdateDocuments(request.getDocumentIds(), operation.getDocument(), request.getWorkOrderId());

        List<SubOperationResponse> subOperationResponses = new ArrayList<>();

        if (subOperations != null && !subOperations.isEmpty()) {
            var existingSubOperations = getSubDetailFromSap(OperationDetailRequest.builder()
                    .operationId(request.getOperationId())
                    .workOrderId(request.getWorkOrderId()).build());
            if (existingSubOperations.getSubOperation() == null ||
                    existingSubOperations.getSubOperation().getItem().isEmpty()) {
                // Add new sub operation
                addSubOperations(request.getOperationId(), subOperationResponses, subOperations);
            } else {
                var existingSubOperationIds =
                        existingSubOperations.getSubOperation().getItem().stream()
                                .map(SubOperationDetailItem::getOperationId)
                                .collect(Collectors.toList());
                var updateSubOperations =
                        request.getSubOperations().stream().filter(subOperation -> existingSubOperationIds.contains(subOperation.getOperationId())).collect(Collectors.toList());
                var addSubOperations =
                        request.getSubOperations().stream().filter(subOperation -> subOperation.getOperationId() == null || subOperation.getOperationId().isEmpty()).collect(Collectors.toList());
                var updateSubOperationIds =
                        updateSubOperations.stream().map(OperationCreateRequest::getOperationId).collect(Collectors.toList());
                var deleteOperationIds =
                        existingSubOperationIds.stream().filter(id -> !updateSubOperationIds.contains(id)).collect(Collectors.toList());
                var deleteOperations =
                        existingSubOperations.getSubOperation().getItem().stream().filter(subOperationDetailItem -> deleteOperationIds.contains(subOperationDetailItem.getOperationId())).collect(Collectors.toList());

                if (!addSubOperations.isEmpty()) {
                    // Add new sub operation
                    addSubOperations(request.getOperationId(), subOperationResponses, addSubOperations);
                }

                if (!updateSubOperations.isEmpty()) {
                    // update sub operation
                    updateSubOperations(subOperationResponses, updateSubOperations);
                }

                if (!deleteOperations.isEmpty()) {
                    // delete operation
                    setDeleteOperations(deleteOperations);
                }
            }
        }

        response.setSubOperations(subOperationResponses);
        return response;
    }

    private void validateUpdateDeleteOperationStatus(String status) throws InvalidInputException {
        if(!status.equalsIgnoreCase(Operation_New_Code) && !status.equalsIgnoreCase(Operation_Release_Code)){
            throw new InvalidInputException(OPERATION_DELETE_CHANGE_ERROR, MessageUtil.getMessage(messageSource,
                    locale, "OPERATION_DELETE_CHANGE_ERROR_MESSAGE"));
        }
    }

    private void setDeleteOperations(List<SubOperationDetailItem> deleteOperations) throws InvalidInputException, OperationNotFoundException, SAPApiException, MasterDataNotFoundException {
        for (SubOperationDetailItem subOperationDetailItem : deleteOperations) {
            deleteOperation(OperationDeleteRequest.builder()
                    .operationId(subOperationDetailItem.getSuperOperationId())
                    .subOperationId(subOperationDetailItem.getOperationId())
                    .workOrderId(subOperationDetailItem.getWorkOrderId()).build());

            var documents = subOperationDetailItem.getDocuments();
            if (documents != null && !documents.isEmpty()) {
                deleteDocuments(documents.stream().map(OperationDocument::getDocumentId).collect(Collectors.toList()));
            }
        }
    }

    private void updateSubOperations(List<SubOperationResponse> subOperationResponses,
                                     List<OperationCreateRequest> updateSubOperations) {
        updateSubOperations.forEach(updateSubOperation -> {
            var operationChangeRequest = updateSubOperation.toOperationChangeRequest();
            operationChangeRequest.setType("CHANGE");
            try {
                var operationChangeResponse = changeOperation(operationChangeRequest);
                addUpdateSubDocuments(updateSubOperation.getDocumentIds(), operationChangeResponse.getDocument(),
                        updateSubOperation.getWorkOrderId(), updateSubOperation.getSuperOperationId());
                var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                        .document(operationChangeResponse.getDocument())
                        .code(operationChangeResponse.getCode())
                        .message(operationChangeResponse.getMessage()).build();
                subOperationResponse.setLocalId(updateSubOperation.getLocalId());
                subOperationResponses.add(subOperationResponse);
            } catch (SAPApiException e) {
                log.error(e.getMessage());
                var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                        .message(e.getMessage())
                        .code("E")
                        .build();
                subOperationResponse.setLocalId(updateSubOperation.getLocalId());
                subOperationResponses.add(subOperationResponse);
            }
        });
    }

    private void addSubOperations(String operationId, List<SubOperationResponse> subOperationResponses,
                                  List<OperationCreateRequest> subOperations) {
        subOperations.forEach(subOperation -> {
            try {
                var operationChangeRequest = subOperation.toOperationChangeRequest();
                operationChangeRequest.setSuperOperationId(operationId);
                operationChangeRequest.setType("ADD");
                var operationChangeResponse = changeOperation(operationChangeRequest);
                // add attachments
                addSubDocuments(subOperation.getDocumentIds(), operationChangeResponse.getDocument(),
                        subOperation.getWorkOrderId(), subOperation.getSuperOperationId());

                var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                        .document(operationChangeResponse.getDocument())
                        .code(operationChangeResponse.getCode())
                        .message(operationChangeResponse.getMessage()).build();
                subOperationResponse.setLocalId(subOperation.getLocalId());
                subOperationResponses.add(subOperationResponse);

            } catch (SAPApiException e) {
                log.error(e.getMessage());
                var subOperationResponse = SubOperationResponse.subOperationResponseBuilder()
                        .message(e.getMessage())
                        .code("E")
                        .build();
                subOperationResponse.setLocalId(subOperation.getLocalId());
                subOperationResponses.add(subOperationResponse);
            }
        });
    }

    @Override
    public MaterialDocumentResponse createMaterialDocument(
            MaterialDocumentRequest materialDocumentRequest) throws SAPApiException, InvalidInputException {
        var operationComponentFromSAP = getOperationComponentFromSAP(OperationDetailRequest.builder()
                .operationId(materialDocumentRequest.getOperationId())
                .workOrderId(materialDocumentRequest.getWorkOrderId()).build());

        var materialDocumentRequestSAP =
                materialDocumentRequest.toMaterialDocumentRequestSAP(operationComponentFromSAP.getComponent().getItem(),
                        getPersonnel(), getEmail());

        for (MaterialDocumentItemSAP materialDocumentItemSAP : materialDocumentRequestSAP.getItems()) {
            if (Integer.parseInt(materialDocumentItemSAP.getQuantity()) > (int) Double.parseDouble(materialDocumentItemSAP.getRemainQuantity())) {
                throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale, QUANTITY_MATERIAL_INVALID));
            }
        }

        log.info("Create operation material with input: {}", parseUtils.toJsonString(materialDocumentRequestSAP));

        return sapService.createMaterialDocument(materialDocumentRequestSAP);
    }

    @Override
    public void changePersonnelOperationParent(OperationPersonnelChangeRequest request)
            throws SAPApiException, MissingRequiredFieldException, OperationNotFoundException {
        payloadUtil.validateInput(request);
        var operation = sapService.getOperationDetail(OperationDetailRequestSAP.builder()
                .operationId(request.getOperationId())
                .workOrderId(request.getWorkOrderId())
                .build());

        if (operation.getOperation() == null) {
            throw new OperationNotFoundException();
        }
        OperationChangeRequestSAP operationPersonnelChangeRequestSAP =
                OperationPersonnelMapper.INSTANCE.operationDetailToOperationChangeRequest(
                        operation.getOperation());
        operationPersonnelChangeRequestSAP.setPersonnel(request.getPersonnelId());
        operationPersonnelChangeRequestSAP.setEmail(getEmail());
        operationPersonnelChangeRequestSAP.setType(OPERATION_TYPE_CHANGE);

        sapService.changeOperation(operationPersonnelChangeRequestSAP);
    }

    @Override
    public BaseResponseSAP deleteOperation(OperationDeleteRequest request)
            throws SAPApiException, OperationNotFoundException, InvalidInputException, MasterDataNotFoundException {
        var operation = getDetailFromSap(OperationDetailRequest.builder().operationId(request.getOperationId())
                .workOrderId(request.getWorkOrderId()).build());
        if (operation.getOperation() == null) {
            throw new OperationNotFoundException();
        }
        var subOperationId = request.getSubOperationId();
        SubOperationDetailItem subOperation = null;
        if (subOperationId != null && !subOperationId.isBlank()) {
            subOperation =
                    operation.getSubOperation().getItem().stream().filter(subOperationDetailItem -> subOperationDetailItem.getOperationId().equals(subOperationId)).findFirst().orElse(null);
            if (subOperation == null) {
                throw new OperationNotFoundException();
            }

            validateUpdateDeleteOperationStatus(subOperation.getOperationSystemStatus());
        }
        else {
            validateUpdateDeleteOperationStatus(operation.getOperation().getOperationSystemStatus());
        }

        var workOrder =
                workOrderService.getDetailsWorkOrder(WorkOrderDetailsRequest.builder().workOrderId(request.getWorkOrderId()).build());

        payloadUtil.validateDeletingOperation((long) workOrder.getOperation().getItem().size());

        var operationChangeRequest = OperationChangeRequest.builder()
                .workOrderId(operation.getOperation().getWorkOrderId())
                .operationId(operation.getOperation().getOperationId())
                .operationDesc(operation.getOperation().getOperationDescription())
                .operationLongText(operation.getOperation().getOperationLongText())
                .equipmentId(operation.getOperation().getEquipmentId())
                .controlKey(operation.getOperation().getControlKey())
                .maintenancePlantId(operation.getOperation().getMaintenancePlantCode())
                .workCenterId(operation.getOperation().getWorkCenterCode())
                .functionalLocationId(operation.getOperation().getFunctionalLocationId())
                .personnel(operation.getOperation().getPersonnel())
                .estimate(operation.getOperation().getEstimate().toString())
                .unit(operation.getOperation().getUnit())
                .build();
        if (subOperation != null) {
            operationChangeRequest = OperationChangeRequest.builder()
                    .workOrderId(subOperation.getWorkOrderId())
                    .operationId(subOperation.getOperationId())
                    .superOperationId(subOperation.getSuperOperationId())
                    .operationDesc(subOperation.getOperationDescription())
                    .operationLongText(subOperation.getOperationLongText())
                    .controlKey(subOperation.getControlKey())
                    .maintenancePlantId(subOperation.getMaintenancePlantCode())
                    .workCenterId(subOperation.getWorkCenterCode())
                    .personnel(subOperation.getPersonnel())
                    .estimate(subOperation.getEstimate().toString())
                    .unit(subOperation.getUnit()).build();
        }
        operationChangeRequest.setType("DELETE");
        return changeOperation(operationChangeRequest);
    }

    @Override
    public void changePersonnelOperationSub(OperationSubPersonnelChangeRequest request)
            throws MissingRequiredFieldException, SAPApiException, OperationNotFoundException {
        payloadUtil.validateInput(request);
        SubOperationDetailResponse subOperationDetailResponse = sapService.getSubOperationDetail(
                OperationDetailRequestSAP.builder()
                        .operationId(request.getOperationId())
                        .workOrderId(request.getWorkOrderId())
                        .build());

        if (subOperationDetailResponse.getSubOperation() == null) {
            throw new OperationNotFoundException();
        }

        var subOperation = subOperationDetailResponse.getSubOperation().getItem().stream()
                .filter(operation -> operation.getOperationId().equals(request.getSubOperationId()))
                .findFirst()
                .orElseThrow(OperationNotFoundException::new);

        OperationChangeRequestSAP operationPersonnelChangeRequestSAP =
                OperationPersonnelMapper.INSTANCE.subOperationDetailItemToOperationChangeRequest(
                        subOperation);
        operationPersonnelChangeRequestSAP.setPersonnel(request.getPersonnelId());
        operationPersonnelChangeRequestSAP.setEmail(getEmail());
        operationPersonnelChangeRequestSAP.setType(OPERATION_TYPE_CHANGE);

        sapService.changeOperation(operationPersonnelChangeRequestSAP);
    }

    private OperationSearchRequestSAP buildOperationSearchRequestSAP(Pageable pageable,
                                                                     OperationSearchRequest input) {
        OperationSearchRequestSAPBuilder builder = OperationSearchRequestSAP.builder();

        if (StringUtils.hasText(input.getFilterText())) {
            builder.keyword(input.getFilterText());
        }

        if (StringUtils.hasText(input.getWorkOrderDateForm())) {
            builder.workOrderDateForm(input.getWorkOrderDateForm());
        }

        if (StringUtils.hasText(input.getWorkOrderDateTo())) {
            builder.workOrderDateTo(input.getWorkOrderDateTo());
        }

        if (StringUtils.hasText(input.getWorkOrderFinishFrom())) {
            builder.workOrderFinishFrom(input.getWorkOrderFinishFrom());
        }

        if (StringUtils.hasText(input.getWorkOrderFinishTo())) {
            builder.workOrderFinishTo(input.getWorkOrderFinishTo());
        }

        if (!ObjectUtils.isEmpty(input.getWorkOrderIds())) {
            builder.workOrder(input.convertToWorkOrderSAP());
        }

        if (!ObjectUtils.isEmpty(input.getOperationIds())) {
            builder.operation(input.convertToOperationSearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getPriorityIds())) {
            builder.priority(input.convertToPrioritySearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getEquipmentIds())) {
            builder.equipment(input.convertToEquipmentSearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getFunctionLocationIds())) {
            builder.functionLocation(input.convertToFunctionalLocationSearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getStatus())) {
            builder.statuses(input.convertToStatusesSearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getPersonnels())) {
            builder.personnel(input.convertToPersonnelSAP());
        }

        if (!ObjectUtils.isEmpty(input.getMaintenancePlantCodes())) {
            builder.plants(input.convertToPlantsSearchSAP());
        }

        if (!ObjectUtils.isEmpty(input.getWorkCenterIds())) {
            builder.workCenter(input.convertToWorkCenterSearchSAP());
        }

        var requestSAP = builder.build();

        requestSAP.setPage(String.valueOf(pageable.getPageNumber()));
        requestSAP.setPageSize(String.valueOf(pageable.getPageSize()));

        return requestSAP;
    }

    @Override
    public OperationHistoryResponse operationComplete(OperationHistoryUpdate input, List<MultipartFile> files) throws SAPApiException,
            InvalidInputException, MissingRequiredFieldException {
        var operationDetail = getDetailFromSap(OperationDetailRequest.builder()
                .workOrderId(input.getWorkOrderId())
                .operationId(input.getOperationId()).build());

        validateOperationComplete(input, operationDetail, files);
        setVarianceReasonCode(operationDetail, input);

        input.setConfirmationLongText();

        OperationConfirmation operationConfirmation = OperationConfirmation.builder()
                .workOrderId(input.getWorkOrderId())
                .operationId(input.getOperationId())
                .superOperationId(input.getSuperOperationId())
                .workCenter(operationDetail.getOperation().getWorkCenterCode())
                .maintenancePlant(operationDetail.getOperation().getMaintenancePlantCode())
                .personnel(operationDetail.getOperation().getPersonnel())
                .postingDate(input.getWorkFinishDate())
                .activityType(operationDetail.getOperation().getActivityType())
                .actualWork(input.getActualWork())
                .actualWorkUnit(operationDetail.getOperation().getUnit())
                .finalConfirmation("X")
                .workStartDate(input.getWorkStartDate())
                .workStartTime(input.getWorkStartTime())
                .workFinishDate(input.getWorkFinishDate())
                .workFinishTime(input.getWorkFinishTime())
                .reason(input.getVarianceReasonCode())
                .confirmation(input.getConfirmation())
                .confirmLongText(input.getConfirmationLongText())
                .email(getEmail())
                .build();

        confirmationService.confirmToSap(OperationConfirmationRequest.builder().confirmation(operationConfirmation).build());

        var entity = OperationHistory.builder()
                .workOrderId(input.getWorkOrderId())
                .operationCode(input.getOperationId())
                .superOperationCode(input.getSuperOperationId())
                .userId(getPersonnel())
                .activity(Operation_Activity_Complete)
                .startAtDate(LocalDate.now(ZoneOffset.UTC))
                .startAtTime(LocalTime.now(ZoneOffset.UTC)).build();
        var result = operationHistoryService.saveOne(entity);

        if (files != null) {
            ProcessOperationAttachments(result.getWorkOrderId(), result.getOperationCode(), input.getSuperOperationId(), files);
        }

        return result.toOperationHistoryResponse();
    }

    private void ProcessOperationAttachments(String workOrderId, String operationId, String superOperationId, List<MultipartFile> files) {
        List<GlobalDocument> documents = new ArrayList<>();
        for (var file : files) {
            var request = new GlobalDocumentUploadRequest();
            try {
                request.fileBytes = file.getBytes();
            } catch (IOException e) {
                //throw new RuntimeException(e);
            }
            request.fileName = file.getOriginalFilename();
            request.fileType = file.getContentType();
            GlobalDocument entity = null;
            try {
                entity = globalDocumentService.uploadFile(request);
            } catch (Exception e) {
                //throw new RuntimeException(e);
            }
            if (entity != null) {
                documents.add(entity);
            }
        }
        List<OperationDocument> result = new ArrayList<>();
        for (var gbDoc : documents) {
            OperationDocumentCreate request = OperationDocumentCreate.builder()
                    .workOrderId(workOrderId)
                    .documentId(gbDoc.getId())
                    .operationCode(operationId)
                    .superOperationCode(superOperationId)
                    .build();
            var operationDoc = operationDocumentService.save(request);
            if (operationDoc != null) {
                result.add(operationDoc);
            }
        }
    }

    private void setVarianceReasonCode(OperationDetailResponse operationDetailResponse, OperationHistoryUpdate input) {
        if (operationDetailResponse.getSubOperation() != null &&
                operationDetailResponse.getSubOperation().getItem() != null &&
                !operationDetailResponse.getSubOperation().getItem().isEmpty()) {
            var isAnyFailed =
                    operationDetailResponse.getSubOperation().getItem().stream().anyMatch(subOperationDetailItem -> subOperationDetailItem.getResult().equalsIgnoreCase("F"));
            if (isAnyFailed) {
                input.setVarianceReasonCode("F");
                return;
            }
            var isAllPassed =
                    operationDetailResponse.getSubOperation().getItem().stream().allMatch(subOperationDetailItem -> subOperationDetailItem.getResult().equalsIgnoreCase("P"));
            if (isAllPassed) {
                input.setVarianceReasonCode("P");
                return;
            }
        }

        if (operationDetailResponse.getMeasure() != null &&
                operationDetailResponse.getMeasure().getItems() != null &&
                !operationDetailResponse.getMeasure().getItems().isEmpty()) {
            var isAnyNotMatchMeasuringPoint =
                    operationDetailResponse.getMeasure().getItems().stream().anyMatch(operationMeasureDetailSAP -> operationMeasureDetailSAP.getResult() < operationMeasureDetailSAP.getLowerRange() ||
                            operationMeasureDetailSAP.getResult() > operationMeasureDetailSAP.getUpperRange());
            if (isAnyNotMatchMeasuringPoint) {
                input.setVarianceReasonCode("F");
            }
        }
    }

    private void validateOperationComplete(OperationHistoryUpdate input,
                                           OperationDetailResponse operationDetailResponse,
                                           List<MultipartFile> files) throws InvalidInputException, MissingRequiredFieldException {
        validateSubOperation(operationDetailResponse);

        validateOperationMeasuringPoint(operationDetailResponse);

        validateVarianceReasonCode(input.getVarianceReasonCode(), input.getConfirmation(), files, true);

        if (!getPersonnel().equalsIgnoreCase(operationDetailResponse.getOperation().getPersonnel()))
            throw new InvalidInputException(OPERATION_NOT_OWNER, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_OWNER"));
    }

    private void validateSubOperation(OperationDetailResponse operationDetailResponse) throws InvalidInputException {
        if (operationDetailResponse.getSubOperation() != null &&
                operationDetailResponse.getSubOperation().getItem() != null &&
                !operationDetailResponse.getSubOperation().getItem().isEmpty()) {
            for (SubOperationDetailItem subOperationDetailItem : operationDetailResponse.getSubOperation().getItem()) {
                if (!isCompletedOperationStatus(subOperationDetailItem.getOperationSystemStatus())) {
                    throw new InvalidInputException(OPERATION_NOT_COMPLETE, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_COMPLETE_MESSAGE"));
                }
            }
        }
    }

    @Override
    public boolean isCompletedOperationStatus(String subOperationStatus) {
        if (subOperationStatus.equals("I0045") || // Đã đóng
                subOperationStatus.equals("I0046") || // Đã tính giá thành
                subOperationStatus.equals("I0009") || // Thực hiện xong công việc
                subOperationStatus.equals("I0043") // Đã khóa
        ) {
            return true;
        }
        return false;
    }

    @Override
    public OperationConfirmationResponse operationConfirmation(OperationConfirmationRequest input) throws MissingRequiredFieldException, SAPApiException {
        validateVarianceReasonCode(input.getConfirmation().getReason(),
                input.getConfirmation().getConfirmation(), List.of(), false);

        //TODO Huy.Nguyen: Need check rules and data with SAP
        //initial missing data
        var startDate = LocalDateTime.now();
        var finishDate = LocalDateTime.now();

        input.getConfirmation().setEmail(getEmail());

        input.getConfirmation().setWorkStartDate(DateTimeUtil.formatDate(startDate));
        input.getConfirmation().setWorkStartTime(DateTimeUtil.formatTime(startDate));

        input.getConfirmation().setWorkFinishDate(DateTimeUtil.formatDate(finishDate));
        input.getConfirmation().setWorkFinishTime(DateTimeUtil.formatTime(finishDate));

        input.getConfirmation().setActualWork("1");
        input.getConfirmation().setActualWorkUnit("min");
        input.getConfirmation().setPostingDate(DateTimeUtil.formatDate(finishDate));

        input.getConfirmation().setFinalConfirmation("X");

        //Confirmation post
        input.getConfirmation().setConfirmationLongText();

        payloadUtil.validateInput(input);

        return confirmationService.confirmToSap(input);
    }

    @Override
    public BaseResponseSAP postMeasuringPoint(PostMeasPointRequest measPoint) throws InvalidInputException, SAPApiException {
        validatePostOperationMeasuringPoint(measPoint);
        return equipmentService.postEquipmentMeasPoint(measPoint);
    }

    private void validatePostOperationMeasuringPoint(PostMeasPointRequest measPoint) throws InvalidInputException, SAPApiException {
        if (measPoint.getData().getOperation() == null || measPoint.getData().getOperation().isEmpty() || measPoint.getData().getOperation().isBlank()) {
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, "Operation is required");
        }

        if (measPoint.getData().getWorkOrder() == null || measPoint.getData().getWorkOrder().isEmpty() || measPoint.getData().getWorkOrder().isBlank()) {
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, "WorkOrder is required");
        }

        // validate WO and Operation
        sapService.getOperationDetail(operationMapper.toOperationSAP(OperationDetailRequest.builder()
                .operationId(measPoint.getData().getOperation())
                .workOrderId(measPoint.getData().getWorkOrder())
                .build()));
    }

    @Override
    public OperationHistoryResponse operationStart(OperationHistoryCreate input) throws SAPApiException, InvalidInputException, ApplicationException {
        validateOperationStart(input);
        var entity = OperationHistory.builder()
                .operationCode(input.getOperationId())
                .workOrderId(input.getWorkOrderId())
                .userId(getPersonnel())
                .activity(Operation_Activity_Start)
                .startAtDate(LocalDate.now(ZoneOffset.UTC))
                .startAtTime(LocalTime.now(ZoneOffset.UTC)).build();
        return operationHistoryService.saveOne(entity).toOperationHistoryResponse();
    }

    private void validateOperationStart(OperationHistoryCreate input) throws SAPApiException, InvalidInputException, ApplicationException {
        var workOrderRequest = WorkOrderDetailsRequest.builder().workOrderId(input.getWorkOrderId()).build();
        var workOrderDetail = sapService.getWorkOrder(workOrderRequest);

        if (workOrderDetail == null)
            throw new InvalidInputException(WORK_ORDER_NOT_FOUND, "Work order not found");

        var commonStatus =
                commonService.findByStatusAndCode(workOrderDetail.getWorkOrder().getWorkOrderSystemStatus(), CommonStatusCodeConsts.WorkOrder).orElse(null);

        if (!commonStatus.getStatus().equalsIgnoreCase(WorkOrder_Release_Code) &&
                !commonStatus.getStatus().equalsIgnoreCase(WorkOrder_Complete_A_Half_Code)
        ) {
            throw new InvalidInputException(WORK_ORDER_NOT_START, "Work order need start before operation");
        }

        var operationRequest = OperationDetailRequestSAP.builder()
                .workOrderId(input.getWorkOrderId())
                .operationId(input.getOperationId()).build();
        var operationResponse = sapService.getOperationDetail(operationRequest);
        if (operationResponse == null)
            throw new InvalidInputException(OPERATION_NOT_FOUND, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_FOUND"));

        if (!getPersonnel().equalsIgnoreCase(operationResponse.getOperation().getPersonnel()))
            throw new InvalidInputException(OPERATION_NOT_OWNER, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_OWNER"));

        // Need to check the personnel is free
        var startingOperation =
                operationHistoryService.getStartingOperation(operationResponse.getOperation().getPersonnel());

        if (startingOperation != null) {
            throw new ApplicationException(Errors.OPERATION_PERSONNEL_IS_BUSY, MessageUtil.getMessage(messageSource, locale, "OPERATION_PERSONNEL_IS_BUSY"));
        }
    }

    @Override
    public OperationHistoryResponse operationHold(OperationHistoryUpdate input, List<MultipartFile> files) throws SAPApiException,
            InvalidInputException {
        var operationResponse = validateOperationHold(input);

        OperationConfirmation operationConfirmation = OperationConfirmation.builder()
                .workOrderId(input.getWorkOrderId())
                .operationId(operationResponse.getOperation().getOperationId())
                .superOperationId(input.getSuperOperationId())
                .workCenter(operationResponse.getOperation().getWorkCenterCode())
                .maintenancePlant(operationResponse.getOperation().getMaintenancePlantCode())
                .personnel(operationResponse.getOperation().getPersonnel())
                .postingDate(input.getWorkFinishDate())
                .activityType(operationResponse.getOperation().getActivityType())
                .actualWork(input.getActualWork())
                .actualWorkUnit(operationResponse.getOperation().getUnit())
                .workStartDate(input.getWorkStartDate())
                .workStartTime(input.getWorkStartTime())
                .workFinishDate(input.getWorkFinishDate())
                .workFinishTime(input.getWorkFinishTime())
                .reason(input.getVarianceReasonCode())
                .email(getEmail())
                .build();

        operationConfirmation.setConfirmationLongText();

        OperationConfirmationRequest operationConfirmationRequest = OperationConfirmationRequest.builder().confirmation(operationConfirmation).build();
        confirmationService.confirmToSap(operationConfirmationRequest);

        var entity = OperationHistory.builder()
                .workOrderId(input.getWorkOrderId())
                .operationCode(input.getOperationId())
                .superOperationCode(input.getSuperOperationId())
                .userId(getPersonnel())
                .activity(Operation_Activity_Hold)
                .startAtDate(LocalDate.now(ZoneOffset.UTC))
                .startAtTime(LocalTime.now(ZoneOffset.UTC)).build();
        var result = operationHistoryService.saveOne(entity);
        if (result != null && files != null) {
            ProcessOperationAttachments(result.getWorkOrderId(), result.getOperationCode(), input.getSuperOperationId(), files);
        }
        return result.toOperationHistoryResponse();
    }

    private OperationDetailResponse validateOperationHold(OperationHistoryUpdate input) throws SAPApiException, InvalidInputException {
        var workOrderRequest = WorkOrderDetailsRequest.builder().workOrderId(input.getWorkOrderId()).build();
        var workOrderDetail = sapService.getWorkOrder(workOrderRequest);

        if (workOrderDetail == null) {
            throw new InvalidInputException(WORK_ORDER_NOT_FOUND, MessageUtil.getMessage(messageSource, locale, "WORK_ORDER_NOT_FOUND"));
        }

        var operationRequest = OperationDetailRequestSAP.builder()
                .workOrderId(input.getWorkOrderId())
                .operationId(input.getOperationId()).build();
        var operationResponse = sapService.getOperationDetail(operationRequest);
        if (operationResponse == null) {
            throw new InvalidInputException(OPERATION_NOT_FOUND, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_FOUND"));
        }
        if (operationResponse.getOperation().getOperationSystemStatus().equalsIgnoreCase(Operation_Complete_Code))
            throw new InvalidInputException(OPERATION_COMPLETED, MessageUtil.getMessage(messageSource, locale, "OPERATION_COMPLETED"));

        if (!getPersonnel().equalsIgnoreCase(operationResponse.getOperation().getPersonnel()))
            throw new InvalidInputException(OPERATION_NOT_OWNER, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_OWNER"));

        return operationResponse;
    }
}

package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.entity.Partner;
import vn.com.sungroup.inventoryasset.repository.PartnerRepository;
import vn.com.sungroup.inventoryasset.service.PartnerService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Service Implementation for managing {@link vn.com.sungroup.inventoryasset.entity.Partner}.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class PartnerServiceImpl implements PartnerService {

    private final Logger log = LoggerFactory.getLogger(PartnerServiceImpl.class);

    private final PartnerRepository partnerRepository;


    @Override
    public void delete(UUID id) {
        log.debug("Request to delete Partner : {}", id);
        partnerRepository.deleteById(id);
    }

    @Override
    public List<Partner> processSavePartners(List<Partner> partners) {
        List<Partner> response = new ArrayList<>();
        for (Partner partner : partners) {
            try {
                Partner partnerEntity = partnerRepository.save(partner);
                response.add(partnerEntity);
            } catch (DataIntegrityViolationException e) {

            }
        }
        return response;
    }

    @Override
    public List<Partner> findAll() {
        return partnerRepository.findAll();
    }
}

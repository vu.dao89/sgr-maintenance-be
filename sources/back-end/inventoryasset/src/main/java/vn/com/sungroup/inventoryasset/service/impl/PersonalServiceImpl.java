package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.personal.PersonalDto;
import vn.com.sungroup.inventoryasset.dto.request.PersonnelRequest;
import vn.com.sungroup.inventoryasset.entity.Personnel;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.repository.PersonalRepository;
import vn.com.sungroup.inventoryasset.service.PersonalService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
@RequiredArgsConstructor
public class PersonalServiceImpl extends BaseServiceImpl implements PersonalService {
    private final Logger log = LoggerFactory.getLogger(PersonalServiceImpl.class);
    private final PersonalRepository personalRepository;
    private final PayloadUtil payloadUtil;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Override
    public Personnel save(PersonalDto personal) throws ParseException {
        var personnel = Personnel.builder()
                .id(personal.getId())
                .maintenancePlantId(personal.getPlantId())
                .workCenterId(personal.getWorkCenterId())
                .name(personal.getPersonelName())
                .startDate(LocalDate.parse(personal.getPerStartDateCsv(), DateTimeFormatter.ofPattern("yyyyMMdd")))
                .endDate(LocalDate.parse(personal.getPerEndDateCsv(), DateTimeFormatter.ofPattern("yyyyMMdd")))
                .code(personal.getPersonelId())
                .build();

        return personalRepository.save(personnel);
    }

    @Override
    public List<Personnel> findAll() {
        return personalRepository.findAll();
    }

    @Override
    public Page<Personnel> getByFilter(PersonnelRequest request,
                                       Pageable pageable) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        payloadUtil.validateInput(request);
        if (request.getMaintenancePlantCodes().isEmpty()) {
            var mPlantIds = getMaintenancePlantIds();
            if (mPlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());

            request.setMaintenancePlantCodes(mPlantIds);
        }

        payloadUtil.validateMaintenancePlantPermission(request.getMaintenancePlantCodes(), getMaintenancePlantIds());

        return personalRepository.getByFilter(request, pageable);

    }

    @Override
    public Personnel getByCode(String code, String workCenterCode) throws MasterDataNotFoundException {
        return personalRepository.findByCodeAndWorkCenterId(code, workCenterCode).orElseThrow(()->new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

}

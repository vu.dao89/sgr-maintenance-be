package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.plannerGroup.PlannerGroupDto;
import vn.com.sungroup.inventoryasset.dto.request.PlannerGroupRequest;
import vn.com.sungroup.inventoryasset.entity.PlannerGroup;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.repository.MaintenancePlantRepository;
import vn.com.sungroup.inventoryasset.repository.PlannerGroupRepository;
import vn.com.sungroup.inventoryasset.service.PlannerGroupService;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PlannerGroupServiceImpl extends BaseServiceImpl implements PlannerGroupService {
    private final Logger log = LoggerFactory.getLogger(PlannerGroupServiceImpl.class);
    private final PlannerGroupRepository plannerGroupRepository;
    private final MaintenancePlantRepository maintenancePlantRepository;
    private final PayloadUtil payloadUtil;

    @Override
    public void save(PlannerGroupDto plannerGroupDto) {
        var maintenancePlant = maintenancePlantRepository.findByCode(plannerGroupDto.getMaintenancePlantId()).orElse(null);
        plannerGroupRepository.save(PlannerGroup.builder()
//                .id(plannerGroupDto.getId())
                .plannerGroupId(plannerGroupDto.getPlannerGroupId())
                .maintenancePlant(maintenancePlant)
                .name(plannerGroupDto.getName())
                .build());
    }

    @Override
    public List<PlannerGroup> findAll() {
        return plannerGroupRepository.findAll();
    }

    @Override
    public Page<PlannerGroup> getPlannerGroups(Pageable pageable, PlannerGroupRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        payloadUtil.validateInput(input);
        if (input.getMaintenancePlantCodes().isEmpty()) {
            var maintenancePlantIds = getMaintenancePlantIds();
            if (maintenancePlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());
            input.setMaintenancePlantCodes(maintenancePlantIds);
        }

        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());

        return plannerGroupRepository.findAllByConditions(pageable, input);
    }
}

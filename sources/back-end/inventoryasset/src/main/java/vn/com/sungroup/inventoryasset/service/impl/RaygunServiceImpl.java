package vn.com.sungroup.inventoryasset.service.impl;

import ch.qos.logback.classic.spi.IThrowableProxy;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.mindscapehq.raygun4java.core.RaygunClient;
import com.mindscapehq.raygun4java.core.RaygunMessageBuilder;
import com.mindscapehq.raygun4java.core.messages.RaygunErrorMessage;
import com.mindscapehq.raygun4java.core.messages.RaygunErrorStackTraceLineMessage;
import com.mindscapehq.raygun4java.core.messages.RaygunMessage;
import com.mindscapehq.raygun4java.core.messages.RaygunMessageDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.service.RaygunService;

import java.io.IOException;
import java.util.Map;

@Service
@Transactional
public class RaygunServiceImpl implements RaygunService {
    @Value("${raygun.key:pDA0JLDzDsiN8giw2ng}")
    private String raygunKey;
    private RaygunClient raygunClient;

    public RaygunServiceImpl() {
    }

    @Override
    public void sendError(Exception exception) {
        this.raygunClient = new RaygunClient(raygunKey);
        raygunClient.send(exception);
    }

    @Override
    public void sendInfo(String message){
        this.raygunClient = new RaygunClient(raygunKey);
        Map<String, String> customData = Maps.newHashMap();
        customData.put("Information",message);
        var raygunMessage = RaygunMessageBuilder.newMessageBuilder().build();
        RaygunMessageDetails details = raygunMessage.getDetails();
        RaygunErrorMessage error = new RaygunErrorMessage(new Exception());
        error.setMessage("Information: " + message);
        details.setError(error);
        details.setUserCustomData(customData);
        raygunClient.send(raygunMessage);
    }
}

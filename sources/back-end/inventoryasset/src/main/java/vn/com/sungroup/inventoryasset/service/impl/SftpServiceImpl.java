package vn.com.sungroup.inventoryasset.service.impl;

import io.sentry.Sentry;
import lombok.RequiredArgsConstructor;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.RemoteFile;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.config.SftpProperties;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SftpServiceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpServiceImpl.class);
    private final RaygunServiceImpl raygunService;
    private final SftpProperties sftpProperties;

    @Value("${move-folder:true}")
    private boolean isMoveFolder;

    public void test() {
        try {
            SSHClient client = new SSHClient();
            client.addHostKeyVerifier(new PromiscuousVerifier());
            client.connect(sftpProperties.getHost(), sftpProperties.getPort());
            client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
            SFTPClient sftp = client.newSFTPClient();
            sftp.get(sftpProperties.getRemotePath() +"Equipment/" + "EQUIPMENT_0000000000070546_20221201-144835-526.XML"  , sftpProperties.getLocalPath());
            sftp.close();
            client.disconnect();
        } catch (IOException e) {
            Sentry.captureException(e);
            raygunService.sendError(e);
            throw new RuntimeException(e);
        }
    }

    public List<RemoteResourceInfo> listFiles(String path) {
        try {
            SSHClient client = new SSHClient();
            client.addHostKeyVerifier(new PromiscuousVerifier());
            client.connect(sftpProperties.getHost(), sftpProperties.getPort());
            client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
            SFTPClient sftp = client.newSFTPClient();
            List<RemoteResourceInfo> files = sftp.ls(path);
            sftp.close();
            client.disconnect();
            return files;
        } catch (Exception e) {
            LOGGER.error("Error while listing files", e);
            Sentry.captureException(e);
            raygunService.sendError(e);
            return Collections.emptyList();
        }
    }

    public void downloadFile(String localPath, String remotePath) throws IOException {
        SSHClient client = new SSHClient();
        client.addHostKeyVerifier(new PromiscuousVerifier());
        client.connect(sftpProperties.getHost(), sftpProperties.getPort());
        client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
        SFTPClient sftp = client.newSFTPClient();
        sftp.get(localPath, remotePath);
        sftp.close();
        client.disconnect();
    }

    public byte[] openFile(String remotePath) {
        try {
            SSHClient client = new SSHClient();
            client.addHostKeyVerifier(new PromiscuousVerifier());
            client.connect(sftpProperties.getHost(), sftpProperties.getPort());
            client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
            SFTPClient sftp = client.newSFTPClient();
            RemoteFile remoteFile = sftp.open(remotePath);
            byte[] bytes = remoteFile.new ReadAheadRemoteFileInputStream(16).readAllBytes();
            sftp.close();
            client.disconnect();
            return bytes;
        } catch (Exception e) {
            LOGGER.error("Error while opening file", e);
            Sentry.captureException(e);
            raygunService.sendError(e);
            return null;
        }
    }

    public void moveFile(String sourceFolderName) {
        if (!isMoveFolder) {
            return;
        }
        try {
            LOGGER.info("Start move folder {}", sftpProperties.getBackupRemotePath());
            String remotePath = sftpProperties.getRemotePath();
            SSHClient client = new SSHClient();
            client.addHostKeyVerifier(new PromiscuousVerifier());
            client.connect(sftpProperties.getHost(), sftpProperties.getPort());
            client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
            SFTPClient sftp = client.newSFTPClient();
            String sourcePath = remotePath + sourceFolderName;
            String destPath = sftpProperties.getBackupRemotePath() + sourceFolderName;
            if (sftp.statExistence(destPath) == null) {
                sftp.mkdirs(destPath);
            }
            List<RemoteResourceInfo> resourceInfos = sftp.ls(sourcePath);
            for (RemoteResourceInfo resourceInfo : resourceInfos) {
                if (sftp.statExistence(destPath + "/" + resourceInfo.getName()) != null)
                {
                    sftp.rm(destPath + "/" + resourceInfo.getName());
                }
                sftp.rename(resourceInfo.getPath(), destPath + "/" + resourceInfo.getName());
            }
            sftp.close();
            client.disconnect();
            LOGGER.info("End move folder {}", sftpProperties.getBackupRemotePath());
        } catch (Exception e) {
            LOGGER.error("Error when move file to backup folder", e);
            Sentry.captureException(e);
            raygunService.sendError(e);
        }
    }


    public void moveFiles(String sourceFolderName,List<String> files) {
        if (!isMoveFolder) {
            return;
        }
        try {
            String remotePath = sftpProperties.getRemotePath();
            SSHClient client = new SSHClient();
            client.addHostKeyVerifier(new PromiscuousVerifier());
            client.connect(sftpProperties.getHost(), sftpProperties.getPort());
            client.authPassword(sftpProperties.getUsername(), sftpProperties.getPassword());
            SFTPClient sftp = client.newSFTPClient();
            String sourcePath = remotePath + sourceFolderName;
            String destPath =  sftpProperties.getBackupRemotePath() + "/" + sourceFolderName;
            if (sftp.statExistence(destPath) == null) {
                sftp.mkdirs(destPath);
            }
            List<RemoteResourceInfo> resourceInfos = sftp.ls(sourcePath);
            for (RemoteResourceInfo resourceInfo : resourceInfos) {
                if (files.contains(resourceInfo.getName())){
                    sftp.rename(resourceInfo.getPath(), destPath + "/" + resourceInfo.getName());
                }
            }
            sftp.close();
            client.disconnect();
        } catch (Exception e) {
            LOGGER.error("Error when move file to backup folder", e);
            Sentry.captureException(e);
            raygunService.sendError(e);
        }
    }
}

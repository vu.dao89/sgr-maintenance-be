package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.storagelocation.StorageLocationDto;
import vn.com.sungroup.inventoryasset.entity.StorageLocation;
import vn.com.sungroup.inventoryasset.repository.StorageLocationRepository;
import vn.com.sungroup.inventoryasset.service.StorageLocationService;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class StorageLocationServiceImpl implements StorageLocationService {
    private final Logger log = LoggerFactory.getLogger(StorageLocationServiceImpl.class);

    private final StorageLocationRepository storageLocationRepository;


    @Override
    public void save(StorageLocationDto storageLocation) {
        storageLocationRepository.save(StorageLocation.builder()
//                .id(storageLocation.getId())
                .code(storageLocation.getStorageLocationId())
                .description(storageLocation.getDescription())
                .maintenancePlantId(storageLocation.getPlantId())
                .build());
    }

    @Override
    public List<StorageLocation> findAll() {
        return storageLocationRepository.findAll();
    }
}

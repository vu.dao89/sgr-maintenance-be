package vn.com.sungroup.inventoryasset.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.config.SftpProperties;
import vn.com.sungroup.inventoryasset.constants.StringConstants;
import vn.com.sungroup.inventoryasset.dto.activitykey.ActivityKeyDto;
import vn.com.sungroup.inventoryasset.dto.activitytype.ActivityTypeDto;
import vn.com.sungroup.inventoryasset.dto.equipment.CharacteristicDto;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentCategoryDto;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDto;
import vn.com.sungroup.inventoryasset.dto.equipment.FunctionalLocationDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MTDto;
import vn.com.sungroup.inventoryasset.dto.equipment.MeasuringPointDto;
import vn.com.sungroup.inventoryasset.dto.equipment.PartnerDto;
import vn.com.sungroup.inventoryasset.dto.equipment.SystemStatusDto;
import vn.com.sungroup.inventoryasset.dto.groupcode.GroupCodeDto;
import vn.com.sungroup.inventoryasset.dto.notification.NotificationTypeDto;
import vn.com.sungroup.inventoryasset.dto.objecttype.ObjectTypeDto;
import vn.com.sungroup.inventoryasset.dto.personal.PersonalDto;
import vn.com.sungroup.inventoryasset.dto.plannerGroup.PlannerGroupDto;
import vn.com.sungroup.inventoryasset.dto.plant.MaintenancePlantDto;
import vn.com.sungroup.inventoryasset.dto.response.employee.EmployeeResponse;
import vn.com.sungroup.inventoryasset.dto.response.employee.EmployeeResponse.EmployeeDataResponse;
import vn.com.sungroup.inventoryasset.dto.storagelocation.StorageLocationDto;
import vn.com.sungroup.inventoryasset.dto.systemcondition.SystemConditionDto;
import vn.com.sungroup.inventoryasset.dto.uom.UnitOfMeasureDto;
import vn.com.sungroup.inventoryasset.dto.workcenter.WorkCenterDto;
import vn.com.sungroup.inventoryasset.dto.workordertype.WorkOrderTypeDto;
import vn.com.sungroup.inventoryasset.entity.Department;
import vn.com.sungroup.inventoryasset.entity.Employee;
import vn.com.sungroup.inventoryasset.mapper.ActivityKeyMgMapper;
import vn.com.sungroup.inventoryasset.mapper.DepartmentMapper;
import vn.com.sungroup.inventoryasset.mapper.EmployeeMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.GroupCodeMgMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.MaintenancePlantMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.NotificationTypeMgMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.ObjectTypeMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.PersonnelMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.SystemConditionMgMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.UnitOfMeasureMapper;
import vn.com.sungroup.inventoryasset.mapper.mongo.WorkCenterMapper;
import vn.com.sungroup.inventoryasset.mongo.ActivityTypeMg;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;
import vn.com.sungroup.inventoryasset.mongo.EquipmentCategoryMg;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;
import vn.com.sungroup.inventoryasset.mongo.ObjectTypeMg;
import vn.com.sungroup.inventoryasset.mongo.PersonnelMg;
import vn.com.sungroup.inventoryasset.mongo.PlannerGroupMg;
import vn.com.sungroup.inventoryasset.mongo.StorageLocationMg;
import vn.com.sungroup.inventoryasset.mongo.SystemConditionMg;
import vn.com.sungroup.inventoryasset.mongo.UnitOfMeasurementMg;
import vn.com.sungroup.inventoryasset.mongo.WorkCenterMg;
import vn.com.sungroup.inventoryasset.mongo.WorkOrderTypeMg;
import vn.com.sungroup.inventoryasset.repository.MeasuringPointMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.EquipmentMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.MaintenancePlantMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.ObjectTypeMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.PersonnelMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.PlannerGroupRepositoryMg;
import vn.com.sungroup.inventoryasset.repository.mongo.StorageLocationRepositoryMg;
import vn.com.sungroup.inventoryasset.repository.mongo.UnitOfMeasurementRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.SystemConditionMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.WorkCenterMgRepository;
import vn.com.sungroup.inventoryasset.sapclient.SapDevService;
import vn.com.sungroup.inventoryasset.service.ActivityKeyService;
import vn.com.sungroup.inventoryasset.service.ActivityTypeService;
import vn.com.sungroup.inventoryasset.service.CharacteristicService;
import vn.com.sungroup.inventoryasset.service.EmployeeService;
import vn.com.sungroup.inventoryasset.service.EquipmentCategoryService;
import vn.com.sungroup.inventoryasset.service.EquipmentService;
import vn.com.sungroup.inventoryasset.service.FuncLocationService;
import vn.com.sungroup.inventoryasset.service.GroupCodeService;
import vn.com.sungroup.inventoryasset.service.MaintenancePlantService;
import vn.com.sungroup.inventoryasset.service.MasterDataProcessedHistoryService;
import vn.com.sungroup.inventoryasset.service.mongo.CharacteristicMgService;
import vn.com.sungroup.inventoryasset.service.mongo.EquipmentMgService;
import vn.com.sungroup.inventoryasset.service.mongo.FuncLocationMgService;
import vn.com.sungroup.inventoryasset.service.mongo.MeasuringPointMgService;
import vn.com.sungroup.inventoryasset.service.MeasuringPointService;
import vn.com.sungroup.inventoryasset.service.NotificationTypeService;
import vn.com.sungroup.inventoryasset.service.ObjectTypeService;
import vn.com.sungroup.inventoryasset.service.PartnerService;
import vn.com.sungroup.inventoryasset.service.PersonalService;
import vn.com.sungroup.inventoryasset.service.PlannerGroupService;
import vn.com.sungroup.inventoryasset.service.StorageLocationService;
import vn.com.sungroup.inventoryasset.service.SyncMasterDataService;
import vn.com.sungroup.inventoryasset.service.SystemConditionService;
import vn.com.sungroup.inventoryasset.service.UnitOfMeasureService;
import vn.com.sungroup.inventoryasset.service.WorkCenterService;
import vn.com.sungroup.inventoryasset.service.WorkOrderTypeService;
import vn.com.sungroup.inventoryasset.service.mongo.ActivityKeyMgService;
import vn.com.sungroup.inventoryasset.service.mongo.GroupCodeMgService;
import vn.com.sungroup.inventoryasset.service.mongo.NotificationTypeMgService;
import vn.com.sungroup.inventoryasset.util.CSVParser;
import vn.com.sungroup.inventoryasset.util.XMLParser;

@Component
@RequiredArgsConstructor
@Slf4j
public class SyncMasterDataServiceImpl implements SyncMasterDataService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SyncMasterDataServiceImpl.class);
    private final SftpProperties sftpProperties;
    private final SftpServiceImpl sftpService;

    private final SapDevService sapDevService;

    private final EmployeeMapper employeeMapper;

    private final EmployeeService employeeService;

    private final ExecutorService executorService = Executors.newFixedThreadPool(30,
            new ThreadFactoryBuilder().setNameFormat("Executor-%d").build());

    private final EquipmentService equipmentService;
    private final MaintenancePlantService maintenancePlantService;
    private final MaintenancePlantMgRepository maintenancePlantMgRepository;
    private final WorkCenterService workCenterService;
    private final WorkCenterMgRepository workCenterMgRepository;
    private final ObjectTypeService objectTypeService;
    private final ObjectTypeMgRepository objectTypeMgRepository;
    private final EquipmentCategoryService equipmentCategoryService;
    private final WorkOrderTypeService workOrderTypeService;
    private final GroupCodeService groupCodeService;
    private final NotificationTypeService notificationTypeService;
    private final StorageLocationService storageLocationService;

    private final ActivityTypeService activityTypeService;

    private final ActivityKeyService activityKeyService;
    private final PersonalService personalService;

    private final PersonnelMgRepository personnelMgRepository;
    private final UnitOfMeasureService unitOfMeasureService;
    private final UnitOfMeasurementRepository unitOfMeasurementRepository;
    private final SystemConditionService systemConditionService;
    private final SystemConditionMgRepository systemConditionMgRepository;
    private final FuncLocationService funcLocationService;
    private final PlannerGroupService plannerGroupService;
    private final DepartmentMapper departmentMapper;
    private final MasterDataProcessedHistoryService masterDataProcessedHistoryService;
    private final CharacteristicService characteristicService;
    private final PartnerService partnerService;
    private final MeasuringPointService measuringPointService;

    private final MeasuringPointMgRepository measuringPointMgRepository;

    private final EquipmentMgRepository equipmentMgRepository;

    private final MongoTemplate template;

    private final NotificationTypeMgService notificationTypeMgService;

    private final NotificationTypeMgMapper notificationTypeMgMapper;

    private final GroupCodeMgService groupCodeMgService;

    private final GroupCodeMgMapper groupCodeMgMapper;

    private final ActivityKeyMgService activityKeyMgService;

    private final ActivityKeyMgMapper activityKeyMgMapper;

    private final MeasuringPointMgService measuringPointMgService;

    private final FuncLocationMgService funcLocationMgService;

    private final CharacteristicMgService characteristicMgService;

    private final EquipmentMgService equipmentMgService;

    private final StorageLocationRepositoryMg storageLocationRepository;
    private final PlannerGroupRepositoryMg plannerGroupRepository;

    @Override
    public void syncMasterData() {
        syncMaintenancePlant();
        syncWorkCenter();
        syncObjectType();
        syncActivityTypes();
        syncEquipmentCategory();
        syncWorkOrderType();
        syncNotificationType();
        syncGroupCode();
        syncActivityKeys();
        syncUnitOfMeasurement();
        syncPersonals();
        syncSystemConditions();
        syncStorageLocation();
        syncPlannerGroup();
        syncFuncLocation();
        syncEquipment();
    }

    @Override
    public void syncEmployee() {

        EmployeeResponse employeeResponse = sapDevService.getEmployee();

        List<EmployeeDataResponse> employeeResponses = employeeResponse.getData();

        List<Employee> employees = new ArrayList<>();

        for (EmployeeDataResponse employeeDataResponse : employeeResponses) {
            final Department department = departmentMapper.toEntity(employeeDataResponse.getDepartment());
            final Employee employee = employeeMapper.toEntity(employeeDataResponse);

            Optional<Employee> employeeFound = employeeService.getByPrimaryId(
                    employeeDataResponse.getPrimaryId());

            // new employee
            if (employeeFound.isEmpty()) {
                employee.setDepartments(List.of(department));
                employees.add(employee);
                continue;
            }

            employee.setId(employeeFound.get().getId());

            // update employee from department null -> not null
            if (employeeFound.get().getDepartments().isEmpty()) {
                employee.setDepartments(List.of(department));
                employees.add(employee);
                continue;
            }

            // update employee
            List<Department> departments = new ArrayList<>();
            for (Department departmentEntity : employeeFound.get().getDepartments()) {
                if (departmentEntity.getDepartmentId().equalsIgnoreCase(department.getDepartmentId())
                        && departmentEntity.getEmployeeCode().equalsIgnoreCase(department.getEmployeeCode())) {
                    department.setId(departmentEntity.getId());
                }
                departments.add(department);
            }
            employee.setDepartments(departments);
            employees.add(employee);
        }
        employeeService.saveAll(employees);

    }

    private void syncEquipment() {
        final String EQUIPMENT_FOLDER = "Equipment";
        List<RemoteResourceInfo> remoteResourceInfos = sftpService.listFiles(
                sftpProperties.getRemotePath() + EQUIPMENT_FOLDER);

        List<EquipmentDto> equipments = new ArrayList<>();

        List<CompletableFuture<MTDto<EquipmentDto>>> futures = remoteResourceInfos.stream()
                .map(remoteResourceInfo -> CompletableFuture.supplyAsync(() -> {
                    MTDto<EquipmentDto> mtEquipmentDto = XMLParser.parse(
                            new String(sftpService.openFile(remoteResourceInfo.getPath()),
                                    StandardCharsets.UTF_8), new TypeReference<>() {
                            });
                    if (Objects.nonNull(mtEquipmentDto) && Objects.nonNull(mtEquipmentDto.header)
                            && !mtEquipmentDto.header.isEmpty()) {
                        return mtEquipmentDto;
                    }
                    return null;
                }, executorService).exceptionally(throwable -> {
                    LOGGER.error("Error when parse file MT_EQUIPMENT: {}", remoteResourceInfo.getPath(),
                            throwable);
                    return null;
                })).filter(Objects::nonNull).collect(Collectors.toList());

        CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new))
                .thenApply(v -> futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList()))
                .thenAccept(lists ->
                        equipments.addAll(
                                lists.stream()
                                        .filter(Objects::nonNull)
                                        .map(mtEquipmentDto -> {
                                            mtEquipmentDto.header.forEach(equipment -> {
                                                List<CharacteristicDto> characteristics = getCharacteristicsById(
                                                        mtEquipmentDto.characteristics, equipment.getEquipmentId());
                                                List<PartnerDto> partners = getPartnersById(
                                                        mtEquipmentDto.partners, equipment.getEquipmentId());
                                                SystemStatusDto status = getSystemStatusById(
                                                        mtEquipmentDto.statuses, equipment.getEquipmentId());
                                                List<MeasuringPointDto> measurePoint = getMeasuringPointById(
                                                        mtEquipmentDto.measurePoint, equipment.getEquipmentId());

                                                equipment.setCharacteristics(characteristics);
                                                equipment.setPartners(partners);
                                                equipment.setStatus(status);
                                                equipment.setMeasurePoint(measurePoint);
                                            });
                                            return mtEquipmentDto.header;
                                        })
                                        .flatMap(List::stream)
                                        .filter(Objects::nonNull)
                                        .collect(Collectors.toList()))
                ).join();

        if (equipments.isEmpty()) {
            return;
        }

        var existingEquipments = equipmentMgRepository.findAll();
        var existingCharacteristics = characteristicMgService.findAll();
        var existingMeasuringPoints = measuringPointMgService.findAll();
        var existingPartners = partnerService.findAll();

        for (EquipmentDto equipment : equipments) {
            try {
                var existingEquipment =
                    existingEquipments.stream().filter(equipment1 -> equipment1.getEquipmentId()
                        .equals(equipment.getEquipmentId())).findFirst().orElse(null);
                if (existingEquipment != null) {
                    equipment.setId(existingEquipment.getId());
                }

                equipment.setCharacteristics(collectCharacteristics(equipment.getCharacteristics(),
                    existingCharacteristics));

                if (equipment.getPartners() != null) {
                    equipment.getPartners().forEach(partnerDto -> {
                        var existingPartner =
                            existingPartners.stream().filter(partner -> partner.getPartnerNumber()
                                .equals(partnerDto.getPartnerNumber())).findFirst().orElse(null);
                        if (existingPartner != null) {
                            partnerDto.set_id(existingPartner.getId());
                        }
                    });
                }

                equipment.setMeasurePoint(
                    collectMeasuringPoints(equipment.getMeasurePoint(), existingMeasuringPoints));

                equipmentMgService.processSave(equipment);
            } catch (DataIntegrityViolationException e) {
                // ignore
            }
        }

        saveProcessedFiles(EQUIPMENT_FOLDER);
        LOGGER.info("Sync master data completed with {} records", equipments.size());
    }

    private List<MeasuringPointDto> collectMeasuringPoints(List<MeasuringPointDto> measurePoints,
                                                           List<MeasuringPointMg> existingMeasuringPoints) {
        if (measurePoints != null) {
            measurePoints.forEach(measuringPointDto -> {
                var existingMeasurePoint =
                        existingMeasuringPoints.stream().filter(measuringPoint -> measuringPoint.getPoint().equals(measuringPointDto.getPoint())).findFirst().orElse(null);
                if (existingMeasurePoint != null) {
                    measuringPointDto.setIdMg(existingMeasurePoint.getId());
                }
            });
        }

        return measurePoints;
    }


    private void syncFuncLocation() {
        final String FUNC_LOCATION_FOLDER = "FuncLocation";
        List<RemoteResourceInfo> remoteResourceInfos = sftpService.listFiles(
            sftpProperties.getRemotePath() + FUNC_LOCATION_FOLDER);

        List<FunctionalLocationDto> functionalLocationDtos = new ArrayList<>();

        List<CompletableFuture<MTDto<FunctionalLocationDto>>> futures = remoteResourceInfos.stream()
            .map(remoteResourceInfo -> CompletableFuture.supplyAsync(() -> {
                MTDto<FunctionalLocationDto> mtFunctionalLocationDto = XMLParser.parse(
                    new String(sftpService.openFile(remoteResourceInfo.getPath()),
                        StandardCharsets.UTF_8), new TypeReference<>() {
                    });
                if (Objects.nonNull(mtFunctionalLocationDto) && Objects.nonNull(
                    mtFunctionalLocationDto.header) && !mtFunctionalLocationDto.header.isEmpty()) {
                    return mtFunctionalLocationDto;
                }
                return null;
            }, executorService).exceptionally(throwable -> {
                LOGGER.error("Error when parse file: {}", remoteResourceInfo.getPath(), throwable);
                return null;
            })).filter(Objects::nonNull).collect(Collectors.toList());

        CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new))
            .thenApply(
                v -> futures.stream().map(CompletableFuture::join).collect(Collectors.toList()))
            .thenAccept(lists -> functionalLocationDtos.addAll(lists.stream()
                .filter(Objects::nonNull)
                .map(mtFunctionalLocationDto -> {
                    mtFunctionalLocationDto.header.forEach(functionalLocation -> {
                        List<CharacteristicDto> characteristics = getCharacteristicsById(
                            mtFunctionalLocationDto.characteristics,
                            functionalLocation.getFunctionalLocationId());
                        List<MeasuringPointDto> measurePoint = getMeasuringPointById(
                            mtFunctionalLocationDto.measurePoint,
                            functionalLocation.getFunctionalLocationId());

                        functionalLocation.setCharacteristics(characteristics);
                        functionalLocation.setMeasurePoint(measurePoint);
                    });

                    return mtFunctionalLocationDto.header;
                })
                .flatMap(List::stream).filter(Objects::nonNull)
                .collect(Collectors.toList()))).join();

        if (functionalLocationDtos.isEmpty()) {
            return;
        }

        var existingFuncLocations = funcLocationMgService.findAll();
        var existingCharacteristics = characteristicMgService.findAll();
        var existingMeasuringPoints = measuringPointMgService.findAll();
        for (FunctionalLocationDto functionalLocationDto : functionalLocationDtos) {
            var existingFunctionalLocal =
                existingFuncLocations.stream().filter(
                        functionalLocation -> functionalLocation.getFunctionalLocationId()
                            .equals(functionalLocationDto.getFunctionalLocationId())).findFirst()
                    .orElse(null);
            if (existingFunctionalLocal != null) {
                functionalLocationDto.setIdMg(existingFunctionalLocal.getId());
            }

            functionalLocationDto.setCharacteristics(
                collectCharacteristics(functionalLocationDto.getCharacteristics(),
                    existingCharacteristics));

            functionalLocationDto.setMeasurePoint(
                collectMeasuringPoints(functionalLocationDto.getMeasurePoint(),
                    existingMeasuringPoints));

            funcLocationMgService.save(functionalLocationDto);
        }

        //sftpService.moveFile(FUNC_LOCATION_FOLDER);
        saveProcessedFiles(FUNC_LOCATION_FOLDER);
        LOGGER.info("Sync master data completed with {} records", functionalLocationDtos.size());
    }

    private List<CharacteristicDto> collectCharacteristics(List<CharacteristicDto> characteristics,
                                                           List<CharacteristicMg> existingCharacteristics) {
        if (characteristics != null) {
            characteristics.forEach(characteristicDto -> {
                var existingCharacteristic =
                        existingCharacteristics.stream().filter(characteristic -> characteristic.getCharId().equals(characteristicDto.getCharId()) &&
                                characteristic.getClassId().equals(characteristicDto.getClassId())).findFirst().orElse(null);
                if (existingCharacteristic != null) {
                    characteristicDto.setIdMg(existingCharacteristic.getId());
                }
            });
        }

        return characteristics;
    }


    private List<MeasuringPointDto> getMeasuringPointById(
            List<MeasuringPointDto> measuringPointDtos, String id) {
        if (measuringPointDtos == null) {
            return Collections.emptyList();
        }

        return measuringPointDtos.stream()
                .filter(measuringPointDto -> Objects.equals(measuringPointDto.getId(), id))
                .collect(Collectors.toList());
    }

    private SystemStatusDto getSystemStatusById(
            List<SystemStatusDto> systemStatusDtos, String id) {
        if (systemStatusDtos == null) {
            return null;
        }

        return systemStatusDtos.stream()
                .filter(systemStatusDto -> Objects.equals(systemStatusDto.getId(), id))
                .findFirst()
                .orElse(null);
    }

    private List<PartnerDto> getPartnersById(List<PartnerDto> partnerDtos, String id) {
        if (partnerDtos == null) {
            return Collections.emptyList();
        }

        return partnerDtos.stream()
                .filter(partnerDto -> Objects.equals(partnerDto.getId(), id))
                .collect(Collectors.toList());
    }

    private List<CharacteristicDto> getCharacteristicsById(
            List<CharacteristicDto> characteristicDtos, String id) {
        if (characteristicDtos == null) {
            return Collections.emptyList();
        }

        return characteristicDtos.stream()
                .filter(characteristicDto -> Objects.equals(characteristicDto.getId(), id))
                .collect(Collectors.toList());
    }

    private <T> List<T> CSVParse(String folderName, Class<T> clazz) {
        List<T> objects = new ArrayList<>();
        List<RemoteResourceInfo> remoteResourceInfos = sftpService
                .listFiles(sftpProperties.getRemotePath() + folderName);
        List<CompletableFuture<List<T>>> futures = remoteResourceInfos.stream()
                .map(remoteResourceInfo -> CompletableFuture
                        .supplyAsync(() -> {
                            List<T> plantsList = CSVParser.parse(new ByteArrayInputStream(
                                    sftpService.openFile(remoteResourceInfo.getPath())), clazz);
                            if (Objects.nonNull(plantsList) && !plantsList.isEmpty()) {
                                return plantsList;
                            }
                            return null;
                        }, executorService).exceptionally(throwable -> {
                            LOGGER.error("Error when parse file: {}", remoteResourceInfo.getPath(),
                                    throwable);
                            return null;
                        }))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new))
                .thenApply(v -> futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList()))
                .thenAccept(lists -> {
                    objects.addAll(lists.stream()
                            .filter(Objects::nonNull)
                            .flatMap(List::stream)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList()));
                }).join();

        return objects;
    }

    private void syncMaintenancePlant() {
        List<MaintenancePlantDto> rawMaintenancePlantDtos =
            CSVParse(StringConstants.MAINTENANCE_PLANT_FOLDER, MaintenancePlantDto.class);

        if (rawMaintenancePlantDtos.isEmpty()) {
            log.warn("Data maintenance plant is empty.");
            return;
        }

        List<MaintenancePlantDto> maintenancePlantDtos = rawMaintenancePlantDtos.stream()
            .collect(Collectors.groupingBy(MaintenancePlantDto::getCode))
            .entrySet().stream()
            .filter(maintenancePlantDtoEntry ->
                StringUtils.hasText(maintenancePlantDtoEntry.getKey())
                    && !maintenancePlantDtoEntry.getValue().isEmpty())
            .map(maintenancePlantDtoEntry ->
                maintenancePlantDtoEntry.getValue().stream().findFirst().get())
            .collect(Collectors.toList());

        var maintenancePlantDocuments = maintenancePlantMgRepository.findAll();
        List<MaintenancePlantMg> maintenancePlants = new ArrayList<>();

        for (MaintenancePlantDto maintenancePlantDto : maintenancePlantDtos) {
            MaintenancePlantMg maintenancePlantToSave = MaintenancePlantMapper.INSTANCE
                .maintenancePlantDtoToDocument(maintenancePlantDto);

            var maintenancePlantDocumentByCode = maintenancePlantDocuments.stream()
                .filter(maintenancePlant ->
                    maintenancePlant.getCode().equals(maintenancePlantDto.getCode()))
                .findFirst();
            if (maintenancePlantDocumentByCode.isPresent()) {
                maintenancePlantToSave.setId(maintenancePlantDocumentByCode.get().getId());
                maintenancePlantToSave.setCode(maintenancePlantDocumentByCode.get().getCode());
            }

            maintenancePlants.add(maintenancePlantToSave);
        }

        maintenancePlantMgRepository.saveAll(maintenancePlants);

        saveProcessedFiles(StringConstants.MAINTENANCE_PLANT_FOLDER);
    }

    private void syncWorkCenter() {
        List<WorkCenterDto> rawWorkCenterDtos = CSVParse(StringConstants.WORK_CENTER_FOLDER,
            WorkCenterDto.class);

        if (rawWorkCenterDtos.isEmpty()) {
            log.warn("Data work center is empty.");
            return;
        }

        List<WorkCenterDto> workCenterDtos = rawWorkCenterDtos.stream()
            .collect(Collectors.groupingBy(workCenterDto ->
                Arrays.asList(workCenterDto.getCode(), workCenterDto.getMaintenancePlantId(),
                    workCenterDto.getCostCenterCode())))
            .entrySet().stream()
            .filter(workCenterDtoEntry ->
                !workCenterDtoEntry.getKey().isEmpty() && !workCenterDtoEntry.getValue().isEmpty())
            .map(workCenterDtoEntry -> workCenterDtoEntry.getValue().stream().findFirst().get())
            .collect(Collectors.toList());

        var workCenterDocuments = workCenterMgRepository.findAll();
        List<WorkCenterMg> workCenters = new ArrayList<>();

        for (WorkCenterDto workCenterDto : workCenterDtos) {
            WorkCenterMg workCenterToSave = WorkCenterMapper.INSTANCE.workCenterDtoToDocument(workCenterDto);

            var workCenterDocument = workCenterDocuments.stream()
                .filter(workCenter ->
                    workCenter.getCode().equals(workCenterDto.getCode()) &&
                        workCenter.getMaintenancePlantId()
                            .equals(workCenterDto.getMaintenancePlantId()) &&
                        workCenter.getCostCenterCode()
                            .equals(workCenterDto.getCostCenterCode()))
                .findFirst();
            if (workCenterDocument.isPresent()) {
                workCenterToSave.setId(workCenterDocument.get().getId());
                workCenterToSave.setCode(workCenterDocument.get().getCode());
                workCenterToSave.setMaintenancePlantId(
                    workCenterDocument.get().getMaintenancePlantId());
                workCenterToSave.setCostCenterCode(workCenterDocument.get().getCostCenterCode());
            }

            workCenters.add(workCenterToSave);
        }

        workCenterMgRepository.saveAll(workCenters);

        saveProcessedFiles(StringConstants.WORK_CENTER_FOLDER);
    }

    private void syncObjectType() {
        List<ObjectTypeDto> rawObjectTypes = CSVParse(StringConstants.OBJ_TYPE_FOLDER,
            ObjectTypeDto.class);

        List<ObjectTypeDto> objectTypeDtos = rawObjectTypes.stream()
            .collect(Collectors.groupingBy(ObjectTypeDto::getObjectTypeId))
            .entrySet().stream()
            .filter(objectTypeDtoEntry -> StringUtils.hasText(objectTypeDtoEntry.getKey())
                && !objectTypeDtoEntry.getValue().isEmpty())
            .map(objectTypeDtoEntry -> objectTypeDtoEntry.getValue().stream().findFirst().get())
            .collect(Collectors.toList());

        var objectTypeDocuments = objectTypeMgRepository.findAll();
        List<ObjectTypeMg> objectTypes = new ArrayList<>();

        for (ObjectTypeDto objectTypeDto : objectTypeDtos) {
            ObjectTypeMg objectTypeToSave = ObjectTypeMapper.INSTANCE.maintenancePlantDtoToDocument(
                objectTypeDto);

            var objectTypeDocument = objectTypeDocuments.stream()
                .filter(objectType ->
                    objectType.getCode().equals(objectTypeDto.getObjectTypeId()))
                .findFirst();
            if (objectTypeDocument.isPresent()) {
                objectTypeToSave.setId(objectTypeDocument.get().getId());
                objectTypeToSave.setCode(objectTypeDocument.get().getCode());
            }
            objectTypes.add(objectTypeToSave);
        }

        objectTypeMgRepository.saveAll(objectTypes);

        saveProcessedFiles(StringConstants.OBJ_TYPE_FOLDER);
    }

    private void syncActivityTypes() {
        final String ACT_TYPE = "ActType";
        List<ActivityTypeDto> activityTypes = CSVParse(ACT_TYPE, ActivityTypeDto.class);
        if (activityTypes.isEmpty()) {
            return;
        }
        List<ActivityTypeMg> existingActivityTypes = activityTypeService.findAll();

        Map<List<String>, List<ActivityTypeDto>> activityTypeGroup = activityTypes.stream()
            .collect(Collectors.groupingBy(activityTypeDto-> Arrays.asList(activityTypeDto.getActType(),
                    activityTypeDto.getCostCenter())));

        activityTypes = activityTypeGroup.values().stream()
                .map(activityTypeDtos -> activityTypeDtos.stream()
                    .findFirst()
                    .orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        List<ActivityTypeMg> saveActivityTypeList = new ArrayList<>();
        for (ActivityTypeDto activityType : activityTypes) {
            ActivityTypeMg existingActivityType = existingActivityTypes.stream()
                .filter(activityType1 -> activityType1.getType().equals(activityType.getActType()) &&
                    activityType1.getCostCenter().equals(activityType.getCostCenter()))
                .findFirst()
                .orElse(null);

            if (existingActivityType != null) {
                saveActivityTypeList.add(ActivityTypeMg.builder()
                    .id(existingActivityType.getId())
                    .type(existingActivityType.getType())
                    .description(activityType.getActTypeDes())
                    .costCenter(existingActivityType.getCostCenter())
                    .costCenterShortText(activityType.getCostCenterDes())
                    .build());
            } else {
                saveActivityTypeList.add(ActivityTypeMg.builder()
                    .type(activityType.getActType())
                    .description(activityType.getActTypeDes())
                    .costCenter(activityType.getCostCenter())
                    .costCenterShortText(activityType.getCostCenterDes())
                    .build());
            }
        }
        activityTypeService.saveAll(saveActivityTypeList);
        //sftpService.moveFile(ACT_TYPE);
        saveProcessedFiles(ACT_TYPE);
    }

    private void syncStorageLocation() {
        final String SLGORT_FOLDER = "Slgort";
        List<StorageLocationDto> storageLocations = CSVParse(SLGORT_FOLDER, StorageLocationDto.class);
        if (storageLocations.isEmpty()) {
            return;
        }

        var existingStorageLocations = storageLocationRepository.findAll();
        var storageLocationsGroup =
                storageLocations.stream().collect(Collectors.groupingBy(storageLocationDto -> Arrays.asList(storageLocationDto.getStorageLocationId(), storageLocationDto.getPlantId())));
        storageLocations =
                storageLocationsGroup.entrySet().stream().map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());


        for (StorageLocationDto storageLocation : storageLocations) {
            try {
                var existingStorageLocation =
                        existingStorageLocations.stream().filter(storageLocation1 -> storageLocation1.getCode().equals(storageLocation.getStorageLocationId()) &&
                                storageLocation1.getMaintenancePlantId().equals(storageLocation.getPlantId())).findFirst().orElse(null);
                if (existingStorageLocation != null) {
                    storageLocation.setId(existingStorageLocation.getId());
                }
                storageLocationRepository.save(StorageLocationMg.builder()
                    .id(storageLocation.getId())
                    .code(storageLocation.getStorageLocationId())
                    .description(storageLocation.getDescription())
                    .maintenancePlantId(storageLocation.getPlantId())
                    .build());
            } catch (DataIntegrityViolationException e) {
                // ignore
            }
        }

        saveProcessedFiles(SLGORT_FOLDER);
    }

    private void syncEquipmentCategory() {
        final String EQUI_CAT_FOLDER = "EquiCat";
        List<EquipmentCategoryDto> equipmentCategories = CSVParse(EQUI_CAT_FOLDER, EquipmentCategoryDto.class);
        if (equipmentCategories.isEmpty()) {
            return;
        }

        List<EquipmentCategoryMg> existingEquipmentCategories = equipmentCategoryService.findAll();

        Map<List<String>, List<EquipmentCategoryDto>> equipmentCategoryGroup = equipmentCategories.stream()
            .collect(Collectors.groupingBy(equipmentCategoryDto -> Arrays.asList(equipmentCategoryDto.getEquipmentCategoryId())));

        equipmentCategories = equipmentCategoryGroup.entrySet().stream()
            .map(listListEntry -> listListEntry.getValue().stream()
                .findFirst().orElse(null))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        List<EquipmentCategoryMg> saveEquipmentCategoryList = new ArrayList<>();
        for (EquipmentCategoryDto equipmentCategory : equipmentCategories) {

            EquipmentCategoryMg existingEquipmentCategory = existingEquipmentCategories.stream()
                .filter(equipmentCategory1 -> equipmentCategory1.getCode()
                    .equals(equipmentCategory.getEquipmentCategoryId()))
                .findFirst()
                .orElse(null);

            if (existingEquipmentCategory != null) {
                saveEquipmentCategoryList.add(EquipmentCategoryMg.builder()
                    .id(existingEquipmentCategory.getId())
                    .code(existingEquipmentCategory.getCode())
                    .description(equipmentCategory.getEquipmentCategoryDes())
                    .build());
            } else {
                saveEquipmentCategoryList.add(EquipmentCategoryMg.builder()
                    .code(equipmentCategory.getEquipmentCategoryId())
                    .description(equipmentCategory.getEquipmentCategoryDes())
                    .build());
            }
        }
        equipmentCategoryService.saveAll(saveEquipmentCategoryList);
        //sftpService.moveFile(EQUI_CAT_FOLDER);
        saveProcessedFiles(EQUI_CAT_FOLDER);
    }

    private void syncWorkOrderType() {
        final String W_O_TYPE_FOLDER = "WOtype";
        List<WorkOrderTypeDto> workOrderTypes = CSVParse(W_O_TYPE_FOLDER, WorkOrderTypeDto.class);
        if (workOrderTypes.isEmpty()) {
            return;
        }

        List<WorkOrderTypeMg> existingWorkOrderTypes = workOrderTypeService.findAll();

        Map<List<String>, List<WorkOrderTypeDto>> workOrderTypeGroup = workOrderTypes.stream()
            .collect(Collectors.groupingBy(workOrderTypeDto -> Arrays.asList(workOrderTypeDto.getWoType(), workOrderTypeDto.getPlant())));

        workOrderTypes = workOrderTypeGroup.values().stream()
            .map(workOrderTypeDtos -> workOrderTypeDtos.stream()
                .findFirst()
                .orElse(null))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        List<WorkOrderTypeMg> saveWorkOrderTypeList = new ArrayList<>();
        for (WorkOrderTypeDto workOrderType : workOrderTypes) {
            WorkOrderTypeMg existingWorkOrderType = existingWorkOrderTypes.stream()
                .filter(workOrderType1 -> workOrderType1.getType().equals(workOrderType.getWoType()) &&
                    workOrderType1.getMaintenancePlantId().equals(workOrderType.getPlant()))
                .findFirst()
                .orElse(null);

            saveWorkOrderTypeList.add(WorkOrderTypeMg.builder()
                .idMg(existingWorkOrderType != null ? existingWorkOrderType.getIdMg() : null)
                .id(workOrderType.getId() != null ? workOrderType.getId() : UUID.randomUUID())
                .name(workOrderType.getWoTypeDes())
                .type(workOrderType.getWoType())
                .maintenancePlantId(workOrderType.getPlant())
                .priorityType(workOrderType.getPriorityType())
                .build());

        }
        workOrderTypeService.saveAll(saveWorkOrderTypeList);
        //sftpService.moveFile(W_O_TYPE_FOLDER);
        saveProcessedFiles(W_O_TYPE_FOLDER);
    }

    private void syncGroupCode() {
        final String GR_CODE_FOLDER = "Grcode";
        List<GroupCodeDto> groupCodesList = CSVParse(GR_CODE_FOLDER, GroupCodeDto.class);
        if (groupCodesList.isEmpty()) {
            return;
        }

        var existingGroupCodeMqs = groupCodeMgService.findAll();
        var groupCodeGroup =
                groupCodesList.stream().collect(Collectors.groupingBy(groupCodeDto -> Arrays.asList(groupCodeDto.getCatalogProfile(),
                        groupCodeDto.getCatalog(), groupCodeDto.getCodeGroup(), groupCodeDto.getCode())));
        groupCodesList =
                groupCodeGroup.entrySet().stream().map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());

        for (GroupCodeDto groupCode : groupCodesList) {
            var existingGroupCodeMq =
                existingGroupCodeMqs.stream().filter(groupCode1 ->
                    groupCode1.getCatalogProfile().equals(groupCode.getCatalogProfile()) &&
                        groupCode1.getCatalog().equals(groupCode.getCatalog()) &&
                        groupCode1.getCodeGroup().equals(groupCode.getCodeGroup()) &&
                        groupCode1.getCode().equals(groupCode.getCode())).findFirst().orElse(null);
            if (existingGroupCodeMq != null) {
                groupCode.setIdMg(existingGroupCodeMq.getId());
            }
        }
        groupCodeMgService.saveAll(groupCodeMgMapper.toListMgEntity(groupCodesList));
        //sftpService.moveFile(GR_CODE_FOLDER);

        saveProcessedFiles(GR_CODE_FOLDER);
    }

    private void syncNotificationType() {
        final String NOTI_TYPE_FOLDER = "NotiType";
        List<NotificationTypeDto> notificationTypes = CSVParse(NOTI_TYPE_FOLDER,
                NotificationTypeDto.class);
        if (notificationTypes.isEmpty()) {
            return;
        }

        var existingNotificationTypeMgs = notificationTypeMgService.findAll();

        var notificationTypeGroup =
                notificationTypes.stream().collect(Collectors.groupingBy(notificationTypeDto -> Arrays.asList(notificationTypeDto.getNotiType(), notificationTypeDto.getCatalogProfile(),
                        notificationTypeDto.getWorkOrderTypeId())));
        notificationTypes =
                notificationTypeGroup.entrySet().stream().map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());

        for (NotificationTypeDto notificationType : notificationTypes) {

            var existingNotificationTypeMq = existingNotificationTypeMgs.stream()
                .filter(notificationType1 ->
                    notificationType1.getNotiType().equals(notificationType.getNotiType()) &&
                        notificationType1.getCatalogProfile()
                            .equals(notificationType.getCatalogProfile()) &&
                        notificationType1.getWorkOrderTypeId()
                            .equals(notificationType.getWorkOrderTypeId())).findFirst()
                .orElse(null);
            if (existingNotificationTypeMq != null) {
                notificationType.setIdMg(existingNotificationTypeMq.getId());
            }
        }
        notificationTypeMgService.saveAll(
            notificationTypeMgMapper.toListMgEntity(notificationTypes));
        //sftpService.moveFile(NOTI_TYPE_FOLDER);

        saveProcessedFiles(NOTI_TYPE_FOLDER);
    }

    private void syncActivityKeys() {
        final String ACT_KEY_FOLDER = "ActKey";
        List<ActivityKeyDto> activityKeyDtos = CSVParse("ActKey", ActivityKeyDto.class);
        if (activityKeyDtos.isEmpty()) {
            return;
        }

        var existingActivityKeyMgs = activityKeyMgService.findAll();
        var activityKeyGroup =
                activityKeyDtos.stream().collect(Collectors.groupingBy(activityKeyDto -> Arrays.asList(activityKeyDto.getWorkOrderType(), activityKeyDto.getType())));
        activityKeyDtos =
                activityKeyGroup.entrySet().stream().map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());

        for (ActivityKeyDto activityKeyDto : activityKeyDtos) {
            var existingActivityKeyMg =
                existingActivityKeyMgs.stream().filter(activityKey ->
                        activityKey.getWorkOrderType().equals(activityKeyDto.getWorkOrderType()) &&
                            activityKey.getType().equals(activityKeyDto.getType())).findFirst()
                    .orElse(null);
            if (existingActivityKeyMg != null) {
                activityKeyDto.setIdMg(existingActivityKeyMg.getId());
            }
        }
        activityKeyMgService.saveAll(activityKeyMgMapper.toListMgEntity(activityKeyDtos));
        //sftpService.moveFile(ACT_KEY_FOLDER);

        saveProcessedFiles(ACT_KEY_FOLDER);
    }

    private void syncPersonals() {
        final String PERSONAL_FOLDER = "Personnel";
        List<PersonalDto> personnel = CSVParse(PERSONAL_FOLDER, PersonalDto.class);
        if (personnel.isEmpty()) {
            return;
        }

        var existingPersonnel = personnelMgRepository.findAll();
        var personnelGroup =
            personnel.stream().collect(
                Collectors.groupingBy(personalDto -> Arrays.asList(personalDto.getPersonelId(),
                    personalDto.getWorkCenterId(), personalDto.getPlantId())));
        personnel = personnelGroup.entrySet().stream()
            .map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null))
            .filter(Objects::nonNull).collect(Collectors.toList());

        List<PersonnelMg> personnelMgList = new ArrayList<>();
        for (PersonalDto personal : personnel) {
            try {
                var existingData =
                    existingPersonnel.stream().filter(personnel1 ->
                        personnel1.getCode().equals(personal.getPersonelId()) &&
                            personnel1.getWorkCenterId().equals(personal.getWorkCenterId()) &&
                            personnel1.getMaintenancePlantId().equals(personal.getPlantId())
                    ).findFirst().orElse(null);
                if (existingData != null) {
                    personal.setIdPer(existingData.getId());
                }
                PersonnelMg personnelMgSave = PersonnelMapper.INSTANCE.personnelDtoToToDocument(personal);
                personnelMgList.add(personnelMgSave);
            } catch (DataIntegrityViolationException duplicationPersonal) {
                //ignored
            }
        }
        personnelMgRepository.saveAll(personnelMgList);

        saveProcessedFiles(PERSONAL_FOLDER);
    }

    private void syncUnitOfMeasurement() {
        final String UOM_FOLDER = "Uom";
        List<UnitOfMeasureDto> unitOfMeasures = CSVParse(UOM_FOLDER, UnitOfMeasureDto.class);
        if (unitOfMeasures.isEmpty()) {
            return;
        }

        var existingUnitOfMeasures = unitOfMeasurementRepository.findAll();
        var unitOfMeasureGroup =
                unitOfMeasures.stream().collect(Collectors.groupingBy(
                    unitOfMeasureDto -> Arrays.asList(unitOfMeasureDto.getUom())));
        unitOfMeasures =
            unitOfMeasureGroup.entrySet().stream().map(
                    listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null))
                .filter(Objects::nonNull).collect(Collectors.toList());

        List<UnitOfMeasurementMg> unitOfMeasurements = new ArrayList<>();

        for (UnitOfMeasureDto unitOfMeasure : unitOfMeasures) {
            try {
                var existingUnitOfMeasure =
                    existingUnitOfMeasures.stream().filter(
                        unitOfMeasurement -> unitOfMeasurement.getCode().equals(unitOfMeasure.getUom())).findFirst();
                if (existingUnitOfMeasure.isPresent()) {
                    unitOfMeasure.setIdUnit(existingUnitOfMeasure.get().getId());
                }
                UnitOfMeasurementMg unitOfMeasurementSave = UnitOfMeasureMapper.INSTANCE.maintenancePlantDtoToDocument(
                    unitOfMeasure);

                unitOfMeasurements.add(unitOfMeasurementSave);
            } catch (DataIntegrityViolationException e) {
                //ignored
            }
        }
        unitOfMeasurementRepository.saveAll(unitOfMeasurements);
        saveProcessedFiles(UOM_FOLDER);
    }


    private void syncSystemConditions() {
        final String SYSCOND_FOLDER = "Syscond";
        List<SystemConditionDto> systemConditions = CSVParse(SYSCOND_FOLDER, SystemConditionDto.class);
        if (systemConditions.isEmpty()) {
            return;
        }

        var existingSystemConditions = systemConditionMgRepository.findAll();
        var systemConditionGroup =
            systemConditions.stream().collect(Collectors.groupingBy(
                systemConditionDto -> Arrays.asList(systemConditionDto.getSysCond())));
        systemConditions =
            systemConditionGroup.entrySet().stream()
                .map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null))
                .filter(Objects::nonNull).collect(Collectors.toList());
        List<SystemConditionMg> systemConditionMgList = new ArrayList<>();

        for (SystemConditionDto systemCondition : systemConditions) {
            try {
                var existingSystemCondition = existingSystemConditions.stream().filter(
                    systemCondition1 -> systemCondition1.getCode()
                        .equals(systemCondition.getSysCond())).findFirst().orElse(null);
                if (existingSystemCondition != null) {
                    systemCondition.setIdSys(existingSystemCondition.getId());
                }
                SystemConditionMg systemConditionMgSave = SystemConditionMgMapper.INSTANCE.systemConditionMgDtoToDocument(systemCondition);
                systemConditionMgList.add(systemConditionMgSave);
            } catch (DataIntegrityViolationException e) {
                // ignore
            }
        }
        systemConditionMgRepository.saveAll(systemConditionMgList);
        saveProcessedFiles(SYSCOND_FOLDER);
    }

    private void syncPlannerGroup() {
        final String PLANNER_GROUP_FOLDER = "PlannerGr";
        List<PlannerGroupDto> plannerGroups = CSVParse(PLANNER_GROUP_FOLDER, PlannerGroupDto.class);
        if (plannerGroups.isEmpty()) {
            return;
        }

        var existingPlannerGroups = plannerGroupRepository.findAll();
        var plannerGroupGroup =
                plannerGroups.stream().collect(Collectors.groupingBy(plannerGroupDto -> Arrays.asList(plannerGroupDto.getMaintenancePlantId(), plannerGroupDto.getPlannerGroupId())));
        plannerGroups =
                plannerGroupGroup.entrySet().stream().map(listListEntry -> listListEntry.getValue().stream().findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());


        plannerGroups.forEach(plannerGroupDto -> {
            try {
                var existingPlannerGroup =
                        existingPlannerGroups.stream().filter(plannerGroup -> plannerGroup.getPlannerGroupId().equals(plannerGroupDto.getPlannerGroupId()) &&
                                plannerGroup.getMaintenancePlantId().equals(plannerGroupDto.getMaintenancePlantId())).findFirst().orElse(null);
                if (existingPlannerGroup != null) {
                    plannerGroupDto.setId(existingPlannerGroup.getId());
                }

                var maintenancePlant = maintenancePlantMgRepository.findByCode(plannerGroupDto.getMaintenancePlantId()).orElse(null);
                plannerGroupRepository.save(PlannerGroupMg.builder()
                    .id(plannerGroupDto.getId())
                    .plannerGroupId(plannerGroupDto.getPlannerGroupId())
                    .maintenancePlant(maintenancePlant)
                    .name(plannerGroupDto.getName())
                    .build());
            } catch (DataIntegrityViolationException e) {
                // ignore
            }
        });
    }


    private void saveProcessedFiles(String folderName) {
        List<RemoteResourceInfo> remoteResourceInfos = sftpService.listFiles(
                sftpProperties.getRemotePath() + folderName);
        var fileInfos = remoteResourceInfos.stream().map(x -> x.getName()).collect(Collectors.toList());
        masterDataProcessedHistoryService.saveProcessedFiles(folderName, fileInfos);
    }
}

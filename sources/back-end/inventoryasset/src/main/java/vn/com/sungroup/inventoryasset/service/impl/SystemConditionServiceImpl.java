package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.SystemConditionRequest;
import vn.com.sungroup.inventoryasset.dto.systemcondition.SystemConditionDto;
import vn.com.sungroup.inventoryasset.entity.SystemCondition;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.repository.SystemConditionRepository;
import vn.com.sungroup.inventoryasset.service.SystemConditionService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class SystemConditionServiceImpl implements SystemConditionService {

    private final SystemConditionRepository systemConditionRepository;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();
    @Override
    public SystemCondition getByCode(String code) throws MasterDataNotFoundException {
        return systemConditionRepository.findByCode(code).orElseThrow(()->new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

    @Override
    public void save(SystemConditionDto systemCondition) {
        systemConditionRepository.save(SystemCondition.builder()
                .id(systemCondition.getId())
                .code(systemCondition.getSysCond())
                .text(systemCondition.getSysCondText())
                .build());
    }

    @Override
    public List<SystemCondition> findAll() {
        return systemConditionRepository.findAll();
    }

    @Override
    public Page<SystemCondition> getSystemConditions(Pageable pageable, SystemConditionRequest input) throws InvalidSortPropertyException {
        try {
            return systemConditionRepository.findAllByConditions(pageable, input);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }
}

package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.equipment.SystemStatusDto;
import vn.com.sungroup.inventoryasset.entity.SystemStatus;
import vn.com.sungroup.inventoryasset.repository.SystemStatusRepository;
import vn.com.sungroup.inventoryasset.service.SystemStatusService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SystemStatusServiceImpl implements SystemStatusService {
  private final SystemStatusRepository systemStatusRepository;


  @Override
  public void processSaveSystemStatus(SystemStatusDto systemStatusDto) {
    if (systemStatusDto == null) {
      return;
    }
    try {
      systemStatusRepository.save(
          SystemStatus.builder()
              .statusId(systemStatusDto.getStatusId())
              .code(systemStatusDto.getCode())
              .description(systemStatusDto.getDescription())
              .userStatusProfile(systemStatusDto.getProfile())
              .userStatus(systemStatusDto.getUserStatus())
              .build()
      );
    } catch (DataIntegrityViolationException e) {
      // ignore
    }
  }

  @Override
  public List<SystemStatus> findAll() {
    return systemStatusRepository.findAll();
  }
}

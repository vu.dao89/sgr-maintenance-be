package vn.com.sungroup.inventoryasset.service.impl;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.DOT;
import static vn.com.sungroup.inventoryasset.constants.StringConstants.THUMBNAIL_SUFFIXES;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import javax.imageio.ImageIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.InputStreamConvertedDto;
import vn.com.sungroup.inventoryasset.dto.globaldocument.GlobalDocumentUploadRequest;
import vn.com.sungroup.inventoryasset.exception.GenerateThumbnailException;
import vn.com.sungroup.inventoryasset.exception.CloudException;
import vn.com.sungroup.inventoryasset.service.ThumbnailService;
import vn.com.sungroup.inventoryasset.service.azurestorage.AzureStorageService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

@Service
@RequiredArgsConstructor
@Slf4j
public class ThumbnailServiceImpl implements ThumbnailService {

  private final AzureStorageService azureStorageService;

  private static final int WIDTH = 320;
  private static final int HEIGHT = 320;

  @Autowired
  private final MessageSource messageSource;
  private final Locale locale = LocaleContextHolder.getLocale();
  @Override
  public String generateThumbnailUrl(GlobalDocumentUploadRequest request)
      throws GenerateThumbnailException, CloudException {
    String fileExtension = request.fileName.split("[.]")[1];
    if(!fileExtension.isEmpty() && fileExtension.toUpperCase().equalsIgnoreCase("HEIC")){
      return azureStorageService.getFile(request.fileName).getURI();
    }
    BufferedImage bufferedImageThumbnail = resizeImage(request.fileBytes);
    InputStreamConvertedDto inputStreamConvertedDto = convertBufferImageToInputStream(
        bufferedImageThumbnail, fileExtension);

    String thumbnailName = request.fileName.replace(DOT + fileExtension,
        THUMBNAIL_SUFFIXES + DOT + fileExtension);
    azureStorageService.uploadFile(inputStreamConvertedDto.getInputStream(), thumbnailName,
        inputStreamConvertedDto.getSize());

    var blobStorageEntityThumbnail = azureStorageService.getFile(thumbnailName);

    return blobStorageEntityThumbnail.getURI();
  }

  private InputStreamConvertedDto convertBufferImageToInputStream(BufferedImage bufferedImage,
      String fileType) throws GenerateThumbnailException {
    try {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      ImageIO.write(bufferedImage, fileType, os);

      return InputStreamConvertedDto.builder()
          .inputStream(new ByteArrayInputStream(os.toByteArray()))
          .size(os.size())
          .build();
    } catch (IOException ex) {
      log.error("Cannot convert buffer image to input stream when generate thumbnail: ", ex);
      throw new GenerateThumbnailException(MessageUtil.getMessage(messageSource,locale,"PROCESS_GENERATED_THUMBNAIL_ERROR"));
    }
  }

  private BufferedImage resizeImage(byte[] fileBytes) throws GenerateThumbnailException {
    try {
      InputStream inputStreamOrigin = new ByteArrayInputStream(fileBytes);
      BufferedImage bufferedImageOrigin = ImageIO.read(inputStreamOrigin);

      return Thumbnails.of(bufferedImageOrigin)
          .size(WIDTH, HEIGHT)
          .asBufferedImage();
    } catch (IOException ex) {
      log.error("Cannot resize image when generate thumbnail: ", ex);
      throw new GenerateThumbnailException(MessageUtil.getMessage(messageSource,locale,"PROCESS_GENERATED_THUMBNAIL_ERROR"));
    }
  }
}

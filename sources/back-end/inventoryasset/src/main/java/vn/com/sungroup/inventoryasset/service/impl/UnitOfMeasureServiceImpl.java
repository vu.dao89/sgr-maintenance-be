package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.UnitOfMeasurementRequest;
import vn.com.sungroup.inventoryasset.dto.uom.UnitOfMeasureDto;
import vn.com.sungroup.inventoryasset.entity.UnitOfMeasurement;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.repository.UnitOfMeasureRepository;
import vn.com.sungroup.inventoryasset.service.UnitOfMeasureService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;

import java.util.List;
import java.util.Locale;

import static vn.com.sungroup.inventoryasset.constants.StringConstants.INVALID_SORT_PROPERTY_LOG_MESSAGE;

@Service
@Transactional
@RequiredArgsConstructor
public class UnitOfMeasureServiceImpl implements UnitOfMeasureService {
    private final Logger log = LoggerFactory.getLogger(UnitOfMeasureServiceImpl.class);
    private final UnitOfMeasureRepository unitOfMeasureRepository;

    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    @Override
    public UnitOfMeasurement getByCode(String code) throws MasterDataNotFoundException {
        return unitOfMeasureRepository.findByCode(code).orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
    }

    @Override
    public void save(UnitOfMeasureDto unitOfMeasure) {
        unitOfMeasureRepository.save(UnitOfMeasurement.builder()
                .id(unitOfMeasure.getId())
                .code(unitOfMeasure.getUom())
                .description(unitOfMeasure.getUomDes())
                .dimension(unitOfMeasure.getDimension())
                .rounding(unitOfMeasure.getRounding().trim())
                .build());
    }

    @Override
    public List<UnitOfMeasurement> findAll() {
        return unitOfMeasureRepository.findAll();
    }

    @Override
    public Page<UnitOfMeasurement> getUnitOfMeasures(Pageable pageable, UnitOfMeasurementRequest input) throws InvalidSortPropertyException {
        try {
            return unitOfMeasureRepository.findAllByConditions(pageable, input);
        } catch (PropertyReferenceException ex) {
            log.error(INVALID_SORT_PROPERTY_LOG_MESSAGE, ex.getPropertyName());
            throw new InvalidSortPropertyException(ex);
        }
    }
}

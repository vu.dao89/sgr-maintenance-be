package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.entity.common.VarianceReason;
import vn.com.sungroup.inventoryasset.repository.common.VarianceReasonRepository;
import vn.com.sungroup.inventoryasset.service.VarianceReasonService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class VarianceReasonServiceImpl implements VarianceReasonService {
    private final VarianceReasonRepository varianceReasonRepository;

    @Override
    public List<VarianceReason> findAll() {
        return varianceReasonRepository.findAll();
    }

    @Override
    public Page<VarianceReason> findAll(Pageable pageable) {
        return varianceReasonRepository.findAll(pageable);
    }

    @Override
    public Optional<VarianceReason> findOne(UUID id) {
        return varianceReasonRepository.findById(id);
    }

    @Override
    public void saveAll(List<VarianceReason> varianceReasons) {
        varianceReasonRepository.saveAll(varianceReasons);
    }

    @Override
    public void deleteAll(List<VarianceReason> varianceReasons) {
        varianceReasonRepository.deleteAll(varianceReasons);
    }

    @Override
    public void updateAll(List<VarianceReason> varianceReasons) {
        varianceReasonRepository.saveAll(varianceReasons);
    }
}

package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.WorkCenterRequest;
import vn.com.sungroup.inventoryasset.entity.WorkCenter;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.repository.MaintenancePlantRepository;
import vn.com.sungroup.inventoryasset.repository.WorkCenterRepository;
import vn.com.sungroup.inventoryasset.service.WorkCenterService;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class WorkCenterServiceImpl extends BaseServiceImpl implements WorkCenterService {

    private final Logger log = LoggerFactory.getLogger(WorkCenterServiceImpl.class);

    private final WorkCenterRepository workCenterRepository;
    private final MaintenancePlantRepository maintenancePlantRepository;
    private final PayloadUtil payloadUtil;
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();


    @Override
    public WorkCenter getByCode(String code) throws MasterDataNotFoundException {
        var workCenter = workCenterRepository.findByCode(code).orElseThrow(()
                -> new MasterDataNotFoundException(MessageUtil.getMessage(messageSource,locale,"DATA_NOT_FOUND")));
        var plant = maintenancePlantRepository.findByCode(workCenter.getMaintenancePlantId()).orElse(null);
        if(plant != null){
            workCenter.setMaintenancePlantName(plant.getPlantName());
        }

        return workCenter;
    }

    @Override
    public void save(WorkCenter workCenter) {
        workCenterRepository.save(workCenter);
    }

    @Override
    public List<WorkCenter> findAll() {
        return workCenterRepository.findAll();
    }


    @Override
    public Page<WorkCenter> getWorkCenterByFilter(WorkCenterRequest request, Pageable pageable)
            throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        payloadUtil.validateInput(request);
        if (request.getMaintenancePlantCodes().isEmpty()) {
            var mPlantIds = getMaintenancePlantIds();
            if (mPlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());

            request.setMaintenancePlantCodes(mPlantIds);
        }

//        payloadUtil.validateMaintenancePlantPermission(request.getMaintenancePlantCodes(), getMaintenancePlantIds());

        var workCenters = workCenterRepository.findByFilter(request, pageable);
        var plaintIds =
                workCenters.map(workCenter -> workCenter.getMaintenancePlantId()).stream().distinct().collect(Collectors.toList());
        var plants = maintenancePlantRepository.findAllByCodeIn(plaintIds);
        workCenters.forEach(workCenter -> {
            var plant =
                    plants.stream().filter(maintenancePlant -> maintenancePlant.getCode().equals(workCenter.getMaintenancePlantId())).findFirst().orElse(null);
            if(plant != null){
                workCenter.setMaintenancePlantName(plant.getPlantName());
            }
        });

        return workCenters;
    }
}

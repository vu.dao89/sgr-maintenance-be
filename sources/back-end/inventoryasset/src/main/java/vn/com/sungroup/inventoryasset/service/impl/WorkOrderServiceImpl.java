package vn.com.sungroup.inventoryasset.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.change.WorkOrderChangeStatusRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.metric.WorkOrderMetricRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.sap.workorder.post.WorkOrderCreateRequestSAP;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangePersonalRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.change.WorkOrderChangeRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.complete.WorkOrderCompleteRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.metric.WorkOrderMetricRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderOperation;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderOperationItem;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderChangeStatusResponseSAP;
import vn.com.sungroup.inventoryasset.dto.response.workorder.WorkOrderMetricResponse;
import vn.com.sungroup.inventoryasset.dto.sap.*;
import vn.com.sungroup.inventoryasset.entity.OperationDocument;
import vn.com.sungroup.inventoryasset.entity.PriorityType;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MasterDataNotFoundException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.exception.SAPApiException;
import vn.com.sungroup.inventoryasset.mapper.WorkOrderMapper;
import vn.com.sungroup.inventoryasset.repository.*;
import vn.com.sungroup.inventoryasset.repository.common.CommonStatusRepository;
import vn.com.sungroup.inventoryasset.repository.common.PriorityTypeRepository;
import vn.com.sungroup.inventoryasset.repository.common.VarianceReasonRepository;
import vn.com.sungroup.inventoryasset.sapclient.SAPService;
import vn.com.sungroup.inventoryasset.service.OperationService;
import vn.com.sungroup.inventoryasset.service.WorkOrderService;
import vn.com.sungroup.inventoryasset.util.DatetimeUtils;
import vn.com.sungroup.inventoryasset.util.MessageUtil;
import vn.com.sungroup.inventoryasset.util.ParseUtils;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.time.format.DateTimeFormatter;
import java.util.*;

import static vn.com.sungroup.inventoryasset.dto.common.status.CommonStatusCodeConsts.*;
import static vn.com.sungroup.inventoryasset.error.Errors.*;
import static vn.com.sungroup.inventoryasset.sapclient.SAPUtils.RESPONSE_SUCCESS_CODE;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class WorkOrderServiceImpl extends BaseServiceImpl implements WorkOrderService {
    @Autowired
    private WorkCenterRepository workCenterRepository;
    @Autowired
    private LockReasonRepository lockReasonRepository;
    private final OperationDocumentRepository operationDocumentRepository;
    private final GlobalDocumentRepository globalDocumentRepository;

    private final SAPService sapService;
    @Lazy
    private final OperationService operationService;

    private final WorkOrderMapper workOrderMapper;
    private final EquipmentRepository equipmentRepository;
    private final OperationHistoryRepository operationHistoryRepository;
    private final VarianceReasonRepository varianceReasonRepository;
    private final PayloadUtil payloadUtil;

    private final PriorityTypeRepository priorityTypeRepository;
    private final CommonStatusRepository commonStatusRepository;
    private final WorkOrderTypeRepository workOrderTypeRepository;
    private final ParseUtils parseUtils;
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    private static List<WorkOrderOperationItem> getWorkOrderOperationItems(WorkOrderOperation operation) {
        List<WorkOrderOperationItem> workOrderOperationItems = new ArrayList<>();
        if (operation != null && operation.getItems() != null && !operation.getItems().isEmpty()) {
            operation.getItems().forEach(workOrderOperationItem -> {
                var subOperations = workOrderOperationItem.getSubOperations();
                if (subOperations != null && !subOperations.isEmpty()) {
                    subOperations.forEach(subOperation -> {
                        workOrderOperationItems.add(subOperation);
                    });
                }
                workOrderOperationItems.add(workOrderOperationItem);
            });
        }
        return workOrderOperationItems;
    }

    @Override
    public WorkOrderDetailsSAP getDetailsWorkOrder(WorkOrderDetailsRequest request) throws SAPApiException {
        // validate request
        var workOrder = sapService.getWorkOrder(request);

        var status = commonStatusRepository.findByStatusAndCode(workOrder.getWorkOrder().getWorkOrderSystemStatus(), CommonStatusCodeConsts.WorkOrder).orElse(null);
        workOrder.getWorkOrder().setCommonStatus(status);

        var workCenter =
                workCenterRepository.findByCodeAndMaintenancePlantId(workOrder.getWorkOrder().getWorkCenter(),
                        workOrder.getWorkOrder().getMainPlant()).orElse(null);
        workOrder.getWorkOrder().setWorkCenterDetail(workCenter);

        var equipment = equipmentRepository.findByEquipmentId(workOrder.getWorkOrder().getEquipmentId()).orElse(null);
        if (equipment != null) {
            workOrder.getWorkOrder().setThumbnailUrl(equipment.getThumbnailUrl());
            workOrder.getWorkOrder().setOriginalUrl(equipment.getOriginalUrl());
            workOrder.getWorkOrder().setLatitude(equipment.getLatitude());
            workOrder.getWorkOrder().setLongitude(equipment.getLongitude());
        }

        for (var operation : workOrder.getOperation().getItem()) {
            var operationDocuments = operationDocumentRepository.findAllByWorkOrderIdAndOperationCodeAndSuperOperationCode(workOrder.getWorkOrder().getWorkOrderId(), operation.getOperationId(), operation.getSuperOperationId());
            operation.setDocuments(operationDocuments);

            var lastesOperationHistory =
                    operationHistoryRepository.findByWorkOrderIdAndOperationCodeAndSuperOperationCode(workOrder.getWorkOrder().getWorkOrderId(), operation.getOperationId(), operation.getSuperOperationId());
            if(lastesOperationHistory != null){
                operation.setOperationHistory(lastesOperationHistory.toOperationHistoryResponse());
            }
            var varianceReason = varianceReasonRepository.findByDescription(operation.getResult()).orElse(null);
            operation.setVarianceReason(varianceReason);

            var operationStatus = commonStatusRepository.findByStatusAndCode(operation.getOperationSystemStatus(),
                    CommonStatusCodeConsts.Operation).orElse(null);
            operation.setOperationStatus(operationStatus);

            var operationWorkCenter =
                    workCenterRepository.findByCodeAndMaintenancePlantId(operation.getWorkCenter(),
                            operation.getMainPlant()).orElse(null);
            operation.setWorkCenterDetail(operationWorkCenter);
        }

        Optional<WorkOrderType> workOrderTypeFound = workOrderTypeRepository.findByTypeAndMaintenancePlantId(workOrder.getWorkOrder().getWorkOrderType(), workOrder.getWorkOrder().getMainPlant());
        if (workOrderTypeFound.isPresent()) {
            final WorkOrderType workOrderType = workOrderTypeFound.get();
            Optional<PriorityType> priorityTypeFound = priorityTypeRepository.findByTypeAndPriority(workOrderType.getPriorityType(), workOrder.getWorkOrder().getPriority());
            priorityTypeFound.ifPresent(priorityType -> workOrder.getWorkOrder().setPriorityType(priorityType));
        }
        return workOrder;
    }

    @Override
    public WorkOrderSearchSAP workOrderSearch(Pageable pageable, WorkOrderSearchRequest input) throws MissingRequiredFieldException, SAPApiException, InvalidInputException {
        payloadUtil.validateInput(input);

        payloadUtil.validatePersonPermission(input.getMainPersonIds(), getPersonnel(), "mainPersonId");

        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());

        WorkOrderSearchRequestSAP requestToSAP = buildWorkOrderSearchRequestSAP(pageable, input);
        log.info("Work order search with input: {}", parseUtils.toJsonString(requestToSAP));
        var workOrders = sapService.getWorkOrders(requestToSAP);
        var priorities = priorityTypeRepository.findAll();
        var commonStatus = commonStatusRepository.findAll();
        workOrders.getData().forEach(workOrderSearchItemSAP -> {
            var equipment = equipmentRepository.findByEquipmentId(workOrderSearchItemSAP.getEquipmentId()).orElse(null);
            if (equipment != null) {
                workOrderSearchItemSAP.setThumbnailUrl(equipment.getThumbnailUrl());
                workOrderSearchItemSAP.setOriginalUrl(equipment.getOriginalUrl());
                workOrderSearchItemSAP.setLatitude(equipment.getLatitude());
                workOrderSearchItemSAP.setLongitude(equipment.getLongitude());
            }

            var priority =
                    priorities.stream().filter(x -> x.getPriority().equals(workOrderSearchItemSAP.getPriority())).findFirst().orElse(null);
            workOrderSearchItemSAP.setPriorityType(priority);
            var status =
                    commonStatus.stream().filter(commonStatus1 -> commonStatus1.getStatus().equals(workOrderSearchItemSAP.getWorkOrderSystemStatus())).findFirst().orElse(null);
            workOrderSearchItemSAP.setCommonStatus(status);
        });

        return workOrders;
    }

    private WorkOrderSearchRequestSAP buildWorkOrderSearchRequestSAP(Pageable pageable, WorkOrderSearchRequest input) {

        var request = WorkOrderSearchRequestSAP.builder();
        if (StringUtils.hasText(input.getFilterText())) {
            request.keyword(input.getFilterText());
        }

        if (!input.getMaintenancePlantCodes().isEmpty()) {
            request.plants(input.convertToPlantsSAP());
        }

        if (input.getWorkOrderTypeIds() != null && !input.getWorkCenterIds().isEmpty()) {
            request.workCenter(input.convertToWorkCenterSAP());
        }

        if (input.getWorkOrderTypeIds() != null && !input.getWorkOrderTypeIds().isEmpty()) {
            request.workOrderType(input.convertToWorkOrderTypeSAP());
        }

        if (input.getEquipmentIds() != null && !input.getEquipmentIds().isEmpty()) {
            request.equipment(input.convertToEquipmentSAP());
        }

        if (input.getFunctionalLocationIds() != null && !input.getFunctionalLocationIds().isEmpty()) {
            request.functionalLocation(input.convertToFunctionalLocationSAP());
        }

        if (input.getStatusIds() != null && !input.getStatusIds().isEmpty()) {
            request.statuses(input.convertToStatusesSAP());
        }

        if (input.getPriorityIds() != null && !input.getPriorityIds().isEmpty()) {
            request.priority(input.convertToPrioritySAP());
        }

        if (input.getMainPersonIds() != null && !input.getMainPersonIds().isEmpty()) {
            request.person(input.convertToMainPersonSAP());
        }

        request.workOrderDescription(input.getWorkOrderDescription());
        request.workOrderStartDateFrom(input.getWorkOrderStartDateFrom());
        request.workOrderStartDateTo(input.getWorkOrderStartDateTo());
        request.workOrderFinishDateFrom(input.getWorkOrderFinishDateFrom());
        request.workOrderFinishDateTo(input.getWorkOrderFinishDateTo());

        var requestSAP = request.build();
        requestSAP.setPage(String.valueOf(pageable.getPageNumber()));
        requestSAP.setPageSize(String.valueOf(pageable.getPageSize()));

        return requestSAP;
    }

    @Override
    public BaseResponseSAP createToSap(WorkOrderCreateRequest input) throws Exception {

        var operation = input.getOperation();
        final WorkOrderCreateRequestSAP request = workOrderMapper.toSapCreateRequest(input);
        request.getWorkOrder().setEmail(getEmail());
        request.getWorkOrder().updateDescription();

        List<WorkOrderOperationItem> workOrderOperationItems = getWorkOrderOperationItems(operation);

        if (!workOrderOperationItems.isEmpty()) {
            var workOrderOperationItemsSAP = workOrderMapper.toWorkOrderOperationItemSAP(workOrderOperationItems);
            workOrderOperationItemsSAP.forEach(workOrderOperationItemSAP -> {
                workOrderOperationItemSAP.updateDescription();
            });
            request.getOperation().setItems(workOrderOperationItemsSAP);
        }

        log.info("WorkOrderServiceImpl workOrderCreate() - START");
        var output = sapService.createWorkOrder(request);
        //Operation document save
        saveWorkOrderDocuments(workOrderOperationItems, output);
        if (!output.getDocument().isEmpty() && Objects.equals(output.getCode(), RESPONSE_SUCCESS_CODE)) {
            var message = MessageUtil.getMessage(messageSource, locale, "CREATE_WORK_ORDER_SUCCESS_MESSAGE");
            message = String.format(message, output.getDocument());
            output.setMessage(message);
        }
        return output;
    }

    private void saveWorkOrderDocuments(List<WorkOrderOperationItem> workOrderOperationItems, BaseResponseSAP output) {
        List<OperationDocument> operationDocuments = new ArrayList<>();
        if (!workOrderOperationItems.isEmpty()) {
            workOrderOperationItems.forEach(workOrderOperationItem -> {
                var documentIds = workOrderOperationItem.getDocumentIds();
                if (documentIds != null && !documentIds.isEmpty()) {
                    var documents = globalDocumentRepository.findAllById(documentIds);
                    documents.forEach(globalDocument -> {
                        var operationDocument = OperationDocument.builder()
                                .workOrderId(output.getDocument())
                                .operationCode(workOrderOperationItem.getOperationId())
                                .globalDocument(globalDocument)
                                .build();
                        var supperOperationId = workOrderOperationItem.getSuperOperationId();
                        if (supperOperationId != null && !supperOperationId.isBlank()) {
                            operationDocument.setSuperOperationCode(supperOperationId);
                        }

                        operationDocuments.add(operationDocument);

                    });
                }
            });
        }

        if (!operationDocuments.isEmpty()) {
            operationDocumentRepository.saveAll(operationDocuments);
        }
    }

    @Override
    public WorkOrderChangeResponseSAP changeToSap(WorkOrderChangeRequest input) throws SAPApiException {
        log.info("WorkOrderServiceImpl workOrderChange() - START");
        var request = workOrderMapper.toSapChangeRequest(input);
        request.setEmail(getEmail());
        request.updateDescription();
        return sapService.changeWorkOrder(request);
    }

    @Override
    public WorkOrderChangeStatusResponseSAP changeStatus(String workOrderId, String statusCode, Integer reason) throws SAPApiException, InvalidInputException, MasterDataNotFoundException {
        WorkOrderDetailsRequest requestWorkOrder = WorkOrderDetailsRequest.builder()
                .workOrderId(workOrderId)
                .build();
        var workOrder = sapService.getWorkOrder(requestWorkOrder);
        if (workOrder == null)
            throw new MasterDataNotFoundException(WORK_ORDER_NOT_FOUND, "Work order not found");

        var workOrderStatus = commonStatusRepository.findByStatusAndCode(workOrder.getWorkOrder().getWorkOrderSystemStatus(), CommonStatusCodeConsts.WorkOrder).orElse(null);
        var workOrderStatusDetail = workOrderStatus.getStatus() + " - " + workOrderStatus.getDescription();

        if (statusCode.equals(WorkOrder_Complete_Code)) {
            validateWorkOrderComplete(workOrder, workOrderStatusDetail);
        }

        if (statusCode.equals(WorkOrder_Release_Code)) {
            validateWorkOrderStart(workOrder, workOrderStatusDetail);
        }

        if (statusCode.equals(WorkOrder_Lock_Code)) {
            validateWorkOrderLock(workOrder, workOrderStatusDetail, reason);
        }

        var requestBuilder = WorkOrderChangeStatusRequestSAP.builder()
                .workOrderId(workOrderId).status(statusCode)
                .email(getEmail());
        if (statusCode.equals(WorkOrder_Lock_Code)) {
            requestBuilder.workOrderId(workOrderId).reason(reason.toString());
        }
        return sapService.changeWorkOrderStatus(requestBuilder.build());
    }

    private void validateWorkOrderLock(WorkOrderDetailsSAP workOrder, String workOrderStatusDetail,
                                       Integer reason) throws InvalidInputException {
        List<String> notAllowedStatuses = new ArrayList<>();
        notAllowedStatuses.add("I0001"); // New - Tạo mới
        notAllowedStatuses.add("I0002"); // Đã ban hành thực hiện

        if (!notAllowedStatuses.contains(workOrder.getWorkOrder().getWorkOrderSystemStatus())) {
            var message = MessageUtil.getMessage(messageSource, locale, "WORK_ORDER_NOT_LOCK_DETAIL");
            message = String.format(message, workOrderStatusDetail);
            throw new InvalidInputException(WORK_ORDER_NOT_LOCK, message);
        }

        //check operation ReservationWithdraw
        var messageReservation = MessageUtil.getMessage(messageSource, locale, "WORK_ORDER_NOT_LOCK_BY_RESERVATION_DETAIL");
        if (workOrder.getOperation().getItem().stream()
                .anyMatch(x -> !x.getReservationWithdraw().equalsIgnoreCase("0")
                        && !x.getReservationWithdraw().equalsIgnoreCase(""))) {
            throw new InvalidInputException(WORK_ORDER_NOT_LOCK, messageReservation);
        }

        if (reason == null){
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, "Reason is required");
        }
        else {
            var lockReason = lockReasonRepository.findByCode(reason).orElse(null);
            if(lockReason == null){
                throw new InvalidInputException(DATA_NOT_FOUND, "Lock Reason");
            }
        }
    }

    private void validateWorkOrderStart(WorkOrderDetailsSAP workOrder, String workOrderStatusDetail) throws InvalidInputException {
        if (!workOrder.getWorkOrder().getWorkOrderSystemStatus().equalsIgnoreCase(WorkOrder_New_Code)) {
            var message = MessageUtil.getMessage(messageSource, locale, "WORK_ORDER_NOT_START_DETAIL");
            message = String.format(message, workOrderStatusDetail);
            throw new InvalidInputException(WORK_ORDER_NOT_START, message);
        }
    }

    private void validateWorkOrderComplete(WorkOrderDetailsSAP workOrder, String workOrderStatusDetail) throws InvalidInputException {
        if (!workOrder.getWorkOrder().getStatus().equalsIgnoreCase(WorkOrder_Finish_Code)) {
            var message = MessageUtil.getMessage(messageSource, locale, "WORK_ORDER_NOT_COMPLETE_DETAIL");
            message = String.format(message, workOrderStatusDetail);
            throw new InvalidInputException(WORK_ORDER_NOT_COMPLETE, message);
        }

        if (workOrder.getOperation() != null &&
                workOrder.getOperation().getItem() != null && !workOrder.getOperation().getItem().isEmpty()) {
            for (OperationItemSAP operationItem : workOrder.getOperation().getItem()) {
                if (!operationService.isCompletedOperationStatus(operationItem.getOperationSystemStatus())) {
                    throw new InvalidInputException(OPERATION_NOT_COMPLETE, MessageUtil.getMessage(messageSource, locale, "OPERATION_NOT_COMPLETE_MESSAGE"));
                }
            }
        }
    }

    @Override
    public WorkOrderMetricResponse getWorkOrderMetricFromSAP(WorkOrderMetricRequest input) throws SAPApiException, InvalidInputException {
        log.info("WorkOrderServiceImpl getWorkOrderMetric() - START");

        payloadUtil.validatePersonPermission(Arrays.asList(input.getMainPerson()), getPersonnel(), "mainPerson");

        String finishFrom = input.getWorkOrderFinishFrom();
        String finishTo = input.getWorkOrderFinishTo();

        if (!StringUtils.hasText(finishFrom) && !StringUtils.hasText(finishTo)) {
            finishFrom = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getFirstDayOfMonth(), DateTimeFormatter.BASIC_ISO_DATE);
            finishTo = DatetimeUtils.convertLocalDateToString(DatetimeUtils.getLastDayOfMonth(), DateTimeFormatter.BASIC_ISO_DATE);
        }
        WorkOrderMetricRequestSAP request = WorkOrderMetricRequestSAP.builder()
                .mainPlant(input.convertPlantSAP())
                .mainPerson(input.getMainPerson())
                .personnelResponse(input.getPersonnelResponse())
                .workOrderFinishFrom(finishFrom)
                .workOrderFinishTo(finishTo)
                .build();
        return sapService.getWorkOrderMetric(request);
    }

    @Override
    public void changePersonal(WorkOrderChangePersonalRequest input) throws SAPApiException {
        final WorkOrderDetailsSAP workOrderDetail = sapService.getWorkOrder(
                WorkOrderDetailsRequest.builder()
                        .workOrderId(input.getWorkOrderId())
                        .build());
        final var workOrder = workOrderDetail.getWorkOrder();

        sapService.changeWorkOrder(WorkOrderChangeRequestSAP.builder()
                .woId(workOrder.getWorkOrderId())
                .woDes(workOrder.getWorkOrderDescription())
                .woLongtext(StringUtils.hasText(workOrder.getWorkOrderLongDescription()) ? workOrder.getWorkOrderLongDescription() : workOrder.getWorkOrderDescription())
                .mainActKey(workOrder.getMainActKey()).equiId(workOrder.getEquipmentId())
                .funcLocId(workOrder.getFunctionalLocationId())
                .prority(workOrder.getPriority())
                .mainPerson(input.getPersonnelId())
                .mainPlant(workOrder.getMainPlant())
                .workCenter(workOrder.getWorkCenter())
                .plannerGroup(workOrder.getPlannerGroup())
                .syscond(workOrder.getSystemCondition())
                .woStartDate(workOrder.getWoStartDate())
                .woFinishDate(workOrder.getWoFinishDate())
                .email(getEmail())
                .build());
    }

    @Override
    public void validateWorkOrderComplete(WorkOrderCompleteRequest input) throws SAPApiException, InvalidInputException {
        var workOrder = sapService.getWorkOrder(WorkOrderDetailsRequest.builder().workOrderId(input.getWorkOrderId()).build());

        var notificationId = workOrder.getWorkOrder().getNotificationId();
        if(notificationId != null && !notificationId.isBlank() && !notificationId.isEmpty()){
            if(input.getNotificationId() == null || !input.getNotificationId().equalsIgnoreCase(notificationId)){
                throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource, locale,
                         String.format("INVALID_NOTIFICATION_WORK_ORDER_COMPLETED_MESSAGE", notificationId)));
            }
        }
    }

}

package vn.com.sungroup.inventoryasset.service.impl;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.sungroup.inventoryasset.dto.request.WorkOrderTypeRequest;
import vn.com.sungroup.inventoryasset.entity.WorkOrderType;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.InvalidSortPropertyException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;
import vn.com.sungroup.inventoryasset.mongo.WorkOrderTypeMg;
import vn.com.sungroup.inventoryasset.repository.WorkOrderTypeRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.WorkOrderTypeRepositoryMg;
import vn.com.sungroup.inventoryasset.service.WorkOrderTypeService;
import vn.com.sungroup.inventoryasset.util.PayloadUtil;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class WorkOrderTypeServiceImpl extends BaseServiceImpl implements WorkOrderTypeService {

    private final Logger log = LoggerFactory.getLogger(WorkOrderTypeServiceImpl.class);
    private final WorkOrderTypeRepository workOrderTypeRepository;
    private final WorkOrderTypeRepositoryMg workOrderTypeRepositoryMg;
    private final PayloadUtil payloadUtil;

    @Override
    public void saveAll(List<WorkOrderTypeMg> workOrderTypeList) {
        workOrderTypeRepositoryMg.saveAll(workOrderTypeList);
    }

    @Override
    public List<WorkOrderTypeMg> findAll() {
        return workOrderTypeRepositoryMg.findAll();
    }

    @Override
    public Page<WorkOrderType> getWorkOrderTypes(Pageable pageable, WorkOrderTypeRequest input) throws InvalidSortPropertyException, MissingRequiredFieldException, InvalidInputException {
        payloadUtil.validateInput(input);
        if (input.getMaintenancePlantCodes().isEmpty()) {
            var mPlantIds = getMaintenancePlantIds();
            if (mPlantIds.isEmpty()) return new PageImpl<>(new ArrayList<>());

            input.setMaintenancePlantCodes(mPlantIds);
        }

        payloadUtil.validateMaintenancePlantPermission(input.getMaintenancePlantCodes(), getMaintenancePlantIds());

        return workOrderTypeRepository.findAllByConditions(pageable, input);
    }
}

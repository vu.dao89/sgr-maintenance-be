package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.ActivityKeyMg;

public interface ActivityKeyMgService {

  List<ActivityKeyMg> findAll();

  void saveAll(List<ActivityKeyMg> activityKeyMgs);
}

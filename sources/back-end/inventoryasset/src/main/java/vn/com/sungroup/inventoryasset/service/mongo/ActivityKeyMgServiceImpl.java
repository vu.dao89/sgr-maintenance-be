package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.mapper.mongo.ActivityKeyMgRepository;
import vn.com.sungroup.inventoryasset.mongo.ActivityKeyMg;

@Service
@RequiredArgsConstructor
public class ActivityKeyMgServiceImpl implements ActivityKeyMgService {

  private final ActivityKeyMgRepository activityKeyMgRepository;

  @Override
  public List<ActivityKeyMg> findAll() {
    return activityKeyMgRepository.findAll();
  }

  @Override
  public void saveAll(List<ActivityKeyMg> activityKeyMgs) {
    activityKeyMgRepository.saveAll(activityKeyMgs);
  }
}

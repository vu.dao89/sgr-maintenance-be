package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.entity.Characteristic;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;

public interface CharacteristicMgService {

  List<CharacteristicMg> findAll();

  List<CharacteristicMg> processSaveCharacteristics(List<CharacteristicMg> toCharacteristicEntityList);
}

package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;
import vn.com.sungroup.inventoryasset.repository.mongo.CharacteristicMgRepository;

@Service
@RequiredArgsConstructor
public class CharacteristicMgServiceImpl implements CharacteristicMgService {

  private final CharacteristicMgRepository characteristicMgRepository;

  @Override
  public List<CharacteristicMg> findAll() {
    return characteristicMgRepository.findAll();
  }

  @Override
  public List<CharacteristicMg> processSaveCharacteristics(
      List<CharacteristicMg> characteristicMgs) {
    return characteristicMgRepository.saveAll(characteristicMgs);
  }
}

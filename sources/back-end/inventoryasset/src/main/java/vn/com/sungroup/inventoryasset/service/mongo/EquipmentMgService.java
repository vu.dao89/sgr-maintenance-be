package vn.com.sungroup.inventoryasset.service.mongo;

import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDto;

public interface EquipmentMgService {


  void processSave(EquipmentDto equipmentDto);
}

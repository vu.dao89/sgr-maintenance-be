package vn.com.sungroup.inventoryasset.service.mongo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.equipment.EquipmentDto;
import vn.com.sungroup.inventoryasset.mapper.mongo.EquipmentMgMapper;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;
import vn.com.sungroup.inventoryasset.mongo.EquipmentMg;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;
import vn.com.sungroup.inventoryasset.mongo.ObjectTypeMg;
import vn.com.sungroup.inventoryasset.mongo.WorkCenterMg;
import vn.com.sungroup.inventoryasset.repository.mongo.EquipmentMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.MaintenancePlantMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.ObjectTypeMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.WorkCenterMgRepository;
import vn.com.sungroup.inventoryasset.util.DateUtils;

@Service
@RequiredArgsConstructor
public class EquipmentMgServiceImpl implements EquipmentMgService {

  private final EquipmentMgRepository equipmentMgRepository;
  private final CharacteristicMgService characteristicMgService;
  private final MeasuringPointMgService measuringPointMgService;
  private final MaintenancePlantMgRepository maintenancePlantMgRepository;
  private final ObjectTypeMgRepository objectTypeMgRepository;
  private final WorkCenterMgRepository workCenterRepository;

  @Override
  public void processSave(EquipmentDto equipmentDto) {

    LocalTime createTime = DateUtils.parseStringToLocalTime(equipmentDto.getCreateTime(),
        DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    LocalDate createDate = DateUtils.parseStringToLocalDate(equipmentDto.getCreateDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyDate = DateUtils.parseStringToLocalDate(equipmentDto.getCusWarrantyDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyEnd = DateUtils.parseStringToLocalDate(equipmentDto.getCusWarrantyEnd(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyDate = DateUtils.parseStringToLocalDate(equipmentDto.getVenWarrantyDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyEnd = DateUtils.parseStringToLocalDate(equipmentDto.getVenWarrantyEnd(),
        DateTimeFormatter.BASIC_ISO_DATE);

    List<CharacteristicMg> characteristics = characteristicMgService.processSaveCharacteristics(
        EquipmentMgMapper.INSTANCE.toCharacteristicEntityList(
            equipmentDto.getCharacteristics()));
    List<MeasuringPointMg> measuringPoints = measuringPointMgService.processSaveMeasuringPoints(
        EquipmentMgMapper.INSTANCE.toMeasuringPointEntityList(
            equipmentDto.getMeasurePoint()));

    MaintenancePlantMg maintenancePlant = maintenancePlantMgRepository.findByCode(
        equipmentDto.getMaintenancePlantId()).orElse(null);

    ObjectTypeMg equipmentType = objectTypeMgRepository.findByCode(
        equipmentDto.getEquipmentTypeId()).orElse(null);

    WorkCenterMg workCenter = workCenterRepository.findByCodeAndMaintenancePlantIdAndCostCenterCode(
        equipmentDto.getMaintenanceWorkCenterId(),
        equipmentDto.getMaintenancePlantId(),
        equipmentDto.getCostCenter()).orElse(null);

    equipmentMgRepository.save(
        EquipmentMg.builder()
            .id(equipmentDto.getId())
            .equipmentId(equipmentDto.getFunctionalLocationId())
            .parentEquipmentId(equipmentDto.getParentEquipmentId())
            .constYear(equipmentDto.getConstYear())
            .constMonth(equipmentDto.getConstMonth())
            .maintenancePlant(maintenancePlant)
            .equipmentType(equipmentType)
            .description(equipmentDto.getDescription())
            .longDesc(equipmentDto.getLongDesc())
            .location(equipmentDto.getLocation())
            .maintenanceWorkCenter(workCenter)
            .costCenter(equipmentDto.getCostCenter())
            .manuCountry(equipmentDto.getManuCountry())
            .manuPartNo(equipmentDto.getManuPartNo())
            .manuSerialNo(equipmentDto.getManuSerialNo())
            .manufacturer(equipmentDto.getManufacturer())
            .modelNumber(equipmentDto.getModelNumber())
            .plannerGroup(equipmentDto.getPlannerGroup())
            .planningPlantId(equipmentDto.getPlanningPlantId())
            .plantSection(equipmentDto.getPlantSection())
            .size(equipmentDto.getSize())
            .weight(equipmentDto.getWeight())
            .createTime(createTime)
            .createDate(createDate)
            .cusWarrantyDate(cusWarrantyDate)
            .cusWarrantyEnd(cusWarrantyEnd)
            .venWarrantyDate(venWarrantyDate)
            .venWarrantyEnd(venWarrantyEnd)
            .qrCode(equipmentDto.getQrCode())
            .startupDate(equipmentDto.getStartUpdate())
            .endOfUseDate(equipmentDto.getEndOfUseDate())
            .characteristics(characteristics)
            .measuringPoints(measuringPoints)
            .build());

  }

}

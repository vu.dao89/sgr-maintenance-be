package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.dto.equipment.FunctionalLocationDto;
import vn.com.sungroup.inventoryasset.mongo.FunctionalLocationMg;

public interface FuncLocationMgService {

  List<FunctionalLocationMg> findAll();

  void save(FunctionalLocationDto functionalLocationDto);
}

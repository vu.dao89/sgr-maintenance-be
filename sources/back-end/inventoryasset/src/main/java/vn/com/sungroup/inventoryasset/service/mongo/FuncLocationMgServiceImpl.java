package vn.com.sungroup.inventoryasset.service.mongo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.dto.equipment.FunctionalLocationDto;
import vn.com.sungroup.inventoryasset.mapper.mongo.EquipmentMgMapper;
import vn.com.sungroup.inventoryasset.mongo.CharacteristicMg;
import vn.com.sungroup.inventoryasset.mongo.FunctionalLocationMg;
import vn.com.sungroup.inventoryasset.mongo.MaintenancePlantMg;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;
import vn.com.sungroup.inventoryasset.mongo.ObjectTypeMg;
import vn.com.sungroup.inventoryasset.repository.mongo.FuncLocationMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.MaintenancePlantMgRepository;
import vn.com.sungroup.inventoryasset.repository.mongo.ObjectTypeMgRepository;
import vn.com.sungroup.inventoryasset.util.DateUtils;

@Service
@RequiredArgsConstructor
public class FuncLocationMgServiceImpl implements FuncLocationMgService {

  private final FuncLocationMgRepository funcLocationMgRepository;
  private final CharacteristicMgService characteristicMgService;
  private final MeasuringPointMgService measuringPointMgService;
  private final MaintenancePlantMgRepository maintenancePlantMgRepository;
  private final ObjectTypeMgRepository objectTypeMgRepository;

  @Override
  public List<FunctionalLocationMg> findAll() {
    return funcLocationMgRepository.findAll();
  }

  @Override
  public void save(FunctionalLocationDto functionalLocationDto) {

    LocalDate createDate = DateUtils.parseStringToLocalDate(functionalLocationDto.getCreateDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyDate = DateUtils.parseStringToLocalDate(
        functionalLocationDto.getCusWarrantyDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate cusWarrantyEnd = DateUtils.parseStringToLocalDate(
        functionalLocationDto.getCusWarrantyEnd(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyDate = DateUtils.parseStringToLocalDate(
        functionalLocationDto.getVenWarrantyDate(),
        DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate venWarrantyEnd = DateUtils.parseStringToLocalDate(
        functionalLocationDto.getVenWarrantyEnd(),
        DateTimeFormatter.BASIC_ISO_DATE);

    List<CharacteristicMg> characteristics = characteristicMgService.processSaveCharacteristics(
        EquipmentMgMapper.INSTANCE.toCharacteristicEntityList(
            functionalLocationDto.getCharacteristics()));
    List<MeasuringPointMg> measuringPoints = measuringPointMgService.processSaveMeasuringPoints(
        EquipmentMgMapper.INSTANCE.toMeasuringPointEntityList(
            functionalLocationDto.getMeasurePoint()));

    MaintenancePlantMg maintenancePlant = maintenancePlantMgRepository.findByCode(
        functionalLocationDto.getMaintenancePlantId()).orElse(null);

    ObjectTypeMg functionalLocationType = objectTypeMgRepository.findByCode(
        functionalLocationDto.getFunctionalLocationTypeId()).orElse(null);

    ObjectTypeMg objectType = objectTypeMgRepository.findByCode(
            functionalLocationDto.getObjectTypeId())
        .orElse(null);

    funcLocationMgRepository.save(
        FunctionalLocationMg.builder()
            .id(functionalLocationDto.getIdMg())
            .functionalLocationId(functionalLocationDto.getFunctionalLocationId())
            .parentFunctionalLocationId(functionalLocationDto.getParentFunctionalLocationId())
            .constYear(functionalLocationDto.getConstYear())
            .constMonth(functionalLocationDto.getConstMonth())
            .maintenancePlant(maintenancePlant)
            .functionalLocationType(functionalLocationType)
            // TODO field in xml does not exists
            .functionalLocationCategory(null)
            .description(functionalLocationDto.getDescription())
            .longDesc(functionalLocationDto.getLongDesc())
            .objectType(objectType)
            .location(functionalLocationDto.getLocation())
            .maintenanceWorkCenterId(functionalLocationDto.getMaintenanceWorkCenterId())
            .costCenter(functionalLocationDto.getCostCenter())
            .manuCountry(functionalLocationDto.getManuCountry())
            .manuPartNo(functionalLocationDto.getManuPartNo())
            .manuSerialNo(functionalLocationDto.getManuSerialNo())
            .manufacturer(functionalLocationDto.getManufacturer())
            .modelNumber(functionalLocationDto.getModelNumber())
            .plannerGroup(functionalLocationDto.getPlannerGroup())
            .planningPlantId(functionalLocationDto.getPlanningPlantId())
            .plantSection(functionalLocationDto.getPlantSection())
            .size(functionalLocationDto.getSize())
            .weight(functionalLocationDto.getWeight())
            .createDate(createDate)
            .cusWarrantyDate(cusWarrantyDate)
            .cusWarrantyEnd(cusWarrantyEnd)
            .venWarrantyDate(venWarrantyDate)
            .venWarrantyEnd(venWarrantyEnd)
            .qrCode(functionalLocationDto.getQrCode())
            .startupDate(functionalLocationDto.getStartupDate())
            .endOfUseDate(functionalLocationDto.getEndOfUseDate())
            .characteristics(characteristics)
            .measuringPoints(measuringPoints)
            .build());

  }

}

package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.GroupCodeMg;

public interface GroupCodeMgService {

  List<GroupCodeMg> findAll();

  void saveAll(List<GroupCodeMg> groupCodeMgs);
}

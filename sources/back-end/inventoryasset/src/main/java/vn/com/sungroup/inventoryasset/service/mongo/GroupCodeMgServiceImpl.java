package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.mongo.GroupCodeMg;
import vn.com.sungroup.inventoryasset.repository.mongo.GroupCodeMgRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class GroupCodeMgServiceImpl implements GroupCodeMgService {

  private final GroupCodeMgRepository groupCodeMgRepository;

  @Override
  public List<GroupCodeMg> findAll() {
    return groupCodeMgRepository.findAll();
  }

  @Override
  public void saveAll(List<GroupCodeMg> groupCodeMgs) {
    groupCodeMgRepository.saveAll(groupCodeMgs);
  }
}

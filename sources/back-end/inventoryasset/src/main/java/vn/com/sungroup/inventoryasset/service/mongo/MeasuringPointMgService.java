package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;

public interface MeasuringPointMgService {

  List<MeasuringPointMg> findAll();

  List<MeasuringPointMg> processSaveMeasuringPoints(List<MeasuringPointMg> toMeasuringPointEntityList);
}

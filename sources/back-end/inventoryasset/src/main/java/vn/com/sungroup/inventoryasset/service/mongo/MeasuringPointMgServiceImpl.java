package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.mongo.MeasuringPointMg;
import vn.com.sungroup.inventoryasset.repository.MeasuringPointMgRepository;

@Service
@RequiredArgsConstructor
public class MeasuringPointMgServiceImpl implements MeasuringPointMgService {

  private final MeasuringPointMgRepository measuringPointMgRepository;
  @Override
  public List<MeasuringPointMg> findAll() {
    return measuringPointMgRepository.findAll();
  }

  @Override
  public List<MeasuringPointMg> processSaveMeasuringPoints(
      List<MeasuringPointMg> measuringPointMgs) {
    return measuringPointMgRepository.saveAll(measuringPointMgs);
  }
}

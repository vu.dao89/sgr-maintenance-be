package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import vn.com.sungroup.inventoryasset.mongo.NotificationTypeMg;

public interface NotificationTypeMgService {

  List<NotificationTypeMg> findAll();

  void saveAll(List<NotificationTypeMg> notificationTypeMgs);
}

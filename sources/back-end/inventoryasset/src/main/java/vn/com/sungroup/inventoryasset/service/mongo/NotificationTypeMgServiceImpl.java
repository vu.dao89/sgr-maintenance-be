package vn.com.sungroup.inventoryasset.service.mongo;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.com.sungroup.inventoryasset.mongo.NotificationTypeMg;
import vn.com.sungroup.inventoryasset.repository.mongo.NotificationTypeMgRepository;

@Service
@RequiredArgsConstructor
public class NotificationTypeMgServiceImpl implements NotificationTypeMgService {

  private final NotificationTypeMgRepository notificationTypeMgRepository;

  @Override
  public List<NotificationTypeMg> findAll() {
    return notificationTypeMgRepository.findAll();
  }

  @Override
  public void saveAll(List<NotificationTypeMg> notificationTypeMgs) {
    notificationTypeMgRepository.saveAll(notificationTypeMgs);
  }
}

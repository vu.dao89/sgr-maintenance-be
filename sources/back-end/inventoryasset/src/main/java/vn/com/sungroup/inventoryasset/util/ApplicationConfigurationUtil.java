/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * ApplicationConfigurationUtil class.
 *
 * <p>Contains information about Application Configuration Util.
 */
@Configuration
@Getter
@Setter
@EnableConfigurationProperties
public class ApplicationConfigurationUtil {

    // server Application
    @Value("${server.url}")
    private String serverApp;

    // server port application
    @Value("${server.port}")
    private String serverPort;

    // context path application
    @Value("${server.servlet.contextPath}")
    private String contextPath;
}

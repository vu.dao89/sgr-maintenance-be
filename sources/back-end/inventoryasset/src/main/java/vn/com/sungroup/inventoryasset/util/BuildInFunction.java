/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * BuildInFunction class.
 *
 * <p>Contains information about Build-In functions.
 */
@Log4j2
public class BuildInFunction {
    private static final Gson gson = new Gson();


    /**
     * input is string then return length input is collection then return length input is number
     * then return value
     *
     * @throws IllegalArgumentException if input is a object or boolean
     */
    public static int length(Object input) throws IllegalArgumentException {
        if (input == null) {
            return 0;
        } else if (input instanceof Collection<?>) {
            return ((Collection<?>) input).size();
        } else if (input instanceof String) {
            return input.toString().length();
        } else if (NumberUtils.isCreatable(input.toString())) {
            return input.toString().length();
        } else if (input instanceof Boolean) {
            throw new IllegalArgumentException("Lenght(Boolean) is not a function");
        } else {
            throw new IllegalArgumentException("Lenght(Object) is not a function");
        }
    }


    public static <T> List<T> listAppend(List<T> lists, T obj) throws Exception {
        if (null == lists) {
            lists = new ArrayList<>();
        }
        lists.add(obj);
        return lists;
    }


    public static String replace(String content, String oldValue, String newValue) {
        content = content.replace(oldValue, newValue);
        return content;
    }


    public static String toString(Object input) throws Exception {
        if (input == null) {
            return null;
        }
        return gson.toJson(input);
    }


    @SuppressWarnings("rawtypes")
    public static Integer size(List l) {
        if (l == null) return 0;
        return l.size();
    }

    public static Object get(Map<Object, Object> m, Object key) {
        if (m == null) return null;
        return m.get(key);
    }


    @SuppressWarnings("rawtypes")
    public static List values(Map<Object, Object> m) {
        if (m == null) {
            return null;
        }
        return new ArrayList<>(m.values());
    }


    @SuppressWarnings("rawtypes")
    public static Boolean contains(List l, Object value) {
        if (CollectionUtils.isEmpty(l)) {
            return false;
        }
        if ((l.get(0) instanceof BigDecimal) && !(value instanceof BigDecimal)) {
            return l.contains(BigDecimal.valueOf((double) value));
        }
        return l.contains(value);
    }


}

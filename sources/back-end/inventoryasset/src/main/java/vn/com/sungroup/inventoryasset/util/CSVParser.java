package vn.com.sungroup.inventoryasset.util;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class CSVParser {

    public static <T> List<T> parse(ByteArrayInputStream arrayInputStream, Class<T> clazz)  {
        var reader = new InputStreamReader(arrayInputStream, StandardCharsets.UTF_8);
        var icsvParser = new CSVParserBuilder().withSeparator(';').build();
        var readerRfc = new CSVReaderBuilder(reader).withCSVParser(icsvParser).build();

        return new CsvToBeanBuilder(readerRfc)
                .withType(clazz)
                .withIgnoreLeadingWhiteSpace(true)
                .build()
                .parse();
    }
}

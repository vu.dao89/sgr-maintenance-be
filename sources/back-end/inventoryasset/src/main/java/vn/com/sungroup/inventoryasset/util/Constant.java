/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

/**
 * class.
 *
 * <p>Contains information about Constant.
 */
public class Constant {

    public static final String lastURL = "";
    public static final String userName = "";
    public static final String FORMAT_DATE = "yyyy-MM-dd";
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_TIME = "HH:mm:ss";
    public static final String CONFIG_CHANNEL_EMAIL = "email";
    public static final Integer REST_TIMEOUT = 12000;
}

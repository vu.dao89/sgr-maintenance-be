package vn.com.sungroup.inventoryasset.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {
    public static   String formatDate(LocalDateTime input){
        if (input != null){
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
            return dateFormat.format(input);
        }
        return "";
    }

    public static   String formatDate(LocalDate input){
        if (input != null){
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
            return dateFormat.format(input);
        }
        return "";
    }

    public static String formatTime(LocalDateTime input){
        if (input != null){
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HHmmss");
            return timeFormat.format(input);
        }
        return "";
    }

    public static String formatTime(LocalTime input){
        if (input != null){
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HHmmss");
            return timeFormat.format(input);
        }
        return "";
    }
}

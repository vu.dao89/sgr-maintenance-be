/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import java.time.format.DateTimeParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * DateUtils class.
 *
 * <p>Contains information about Date Utils.
 */
public class DateUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY = "day";
    public static final String HOUR = "hour";
    public static final String MINUTE = "minute";
    public static final String SECOND = "second";
    public static final String MILLISECOND = "millisecond";

    public static String dateToText(LocalDate date, String format) throws Exception {
        return DateTimeFormatter.ofPattern(format).format(date);
    }

    public static LocalDateTime dateToLocalDateTime(Object objDate) throws Exception {
        if (null == objDate) {
            return null;
        }

        Date date = (Date) objDate;
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDate dateToLocalDate(Object objDate) throws Exception {
        if (null == objDate) {
            return null;
        }
        if (objDate instanceof java.sql.Timestamp) {
            return ((java.sql.Timestamp) objDate).toLocalDateTime().toLocalDate();
        }
        java.sql.Date sqlDate = (java.sql.Date) objDate;
        return sqlDate.toLocalDate();
    }

    public static LocalTime dateToLocalTime(Object objDate) throws Exception {
        if (null == objDate) {
            return null;
        }

        java.sql.Time sqlTime = (java.sql.Time) objDate;
        return sqlTime.toLocalTime();
    }

    public static LocalTime dateTimeToLocalTime(Object objDate) throws Exception {
        if (null == objDate) {
            return null;
        }
        LocalDateTime dateTime = (LocalDateTime) objDate;
        LocalTime time = dateTime.toLocalTime();
        return time;
    }

    public static LocalDate dateUtilToLocalDate(Object objDate) throws Exception {
        if (null == objDate) {
            return null;
        }
        Date date = (Date) objDate;
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        return sqlDate.toLocalDate();
    }

    public static LocalDate parseStringToLocalDate(String value, DateTimeFormatter formatter) {
        try {
            return value != null ? LocalDate.parse(value, formatter) : null;
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    public static LocalTime parseStringToLocalTime(String value, DateTimeFormatter formatter) {
        try {
            return value != null ? LocalTime.parse(value, formatter) : null;
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}

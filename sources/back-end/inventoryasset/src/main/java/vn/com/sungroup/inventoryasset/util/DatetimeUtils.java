package vn.com.sungroup.inventoryasset.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DatetimeUtils {

  public static final int FIRST_DAY = 1;

  public static LocalDate getFirstDayOfMonth() {
    LocalDate localDateCurrent = LocalDate.now();
    return localDateCurrent.withDayOfMonth(FIRST_DAY);
  }

  public static LocalDate getLastDayOfMonth() {
    LocalDate localDateCurrent = LocalDate.now();
    return localDateCurrent.withDayOfMonth(
        localDateCurrent.getMonth().length(localDateCurrent.isLeapYear()));
  }

  public static String convertLocalDateToString(LocalDate date, DateTimeFormatter formatter) {
    return date.format(formatter);
  }
}

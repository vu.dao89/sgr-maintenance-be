package vn.com.sungroup.inventoryasset.util;

import org.springframework.context.MessageSource;
import org.springframework.lang.Nullable;

import java.util.Locale;

public class MessageUtil {

    public static String getMessage(final MessageSource messageSource, final Locale locale, String code){
        return   messageSource.getMessage(code,null,locale);
    }
    public static String getMessage(final MessageSource messageSource, final Locale locale, String code,@Nullable Object[] args){
        return   messageSource.getMessage(code,args,locale);
    }
}

package vn.com.sungroup.inventoryasset.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.jayway.jsonpath.InvalidJsonException;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
@RequiredArgsConstructor
@Slf4j
public class ParseUtils {

  private final ObjectMapper objectMapper;

  public <T> T parseJson(String data, Class<T> clazz) throws InvalidJsonException {
    try {
      return objectMapper.readValue(data, clazz);
    } catch (JsonProcessingException e) {
      throw new InvalidJsonException(e);
    }
  }

  public String convertObjectToJson(Object object) throws JsonProcessingException {
    return objectMapper.writeValueAsString(object);
  }

  public <T> List<T> parseJsonToList(String data, Class<T> tClass) throws InvalidJsonException {
    try {
      CollectionType listType =
          objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);
      return objectMapper.readValue(data, listType);
    } catch (JsonProcessingException e) {
      throw new InvalidJsonException(e);
    }
  }

  public String toJsonString(List<MultipartFile> files) {
    List<String> fileNames = new ArrayList<>();
    for(var file : files) {
      fileNames.add(toJsonString(file));
    }

    return fileNames.toString();
  }

  public String toJsonString(MultipartFile file) {
    try {
      return objectMapper.writeValueAsString(file.getOriginalFilename());
    } catch (JsonProcessingException e) {
      throw new InvalidJsonException(e);
    }
  }

  public String toJsonString(Object object) {
    try {
      return objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      throw new InvalidJsonException(e);
    }
  }

}

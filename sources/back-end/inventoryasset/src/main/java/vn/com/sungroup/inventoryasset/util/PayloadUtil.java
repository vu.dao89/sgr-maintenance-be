package vn.com.sungroup.inventoryasset.util;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.notification.detail.NotificationItemDto;
import vn.com.sungroup.inventoryasset.dto.request.notification.NotificationCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.operation.MaterialDocumentItem;
import vn.com.sungroup.inventoryasset.dto.request.operation.MaterialDocumentRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreate;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderCreateRequest;
import vn.com.sungroup.inventoryasset.dto.request.workorder.post.WorkOrderOperationItem;
import vn.com.sungroup.inventoryasset.dto.sap.OperationChangeRequest;
import vn.com.sungroup.inventoryasset.dto.sap.OperationCreateRequest;
import vn.com.sungroup.inventoryasset.entity.Equipment;
import vn.com.sungroup.inventoryasset.entity.FunctionalLocation;
import vn.com.sungroup.inventoryasset.exception.InvalidInputException;
import vn.com.sungroup.inventoryasset.exception.MissingRequiredFieldException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static vn.com.sungroup.inventoryasset.error.Errors.*;
import static vn.com.sungroup.inventoryasset.util.security.PermissionCode.ALL_PERSON;
import static vn.com.sungroup.inventoryasset.util.security.PermissionCode.APPLICATION_ID;

@RequiredArgsConstructor
@Service
@Slf4j
public class PayloadUtil {


    
    private final Validator validator;

    private static final String OPERATION_INVALID = "OPERATION_INVALID";
    private static final String OPERATION_TYPE_INVALID = "OPERATION_TYPE_INVALID";
    private static final String CHANGE_OPERATION_TYPE_INVALID = "OPERATION_TYPE_INVALID";
    private static final String OPERATION_ID_INVALID = "OPERATION_ID_INVALID";
    private static final String NOTIFICATION_ID_INVALID = "NOTIFICATION_ID_INVALID";
    private static final String NOTIFICATION_ITEM_ID_INVALID = "NOTIFICATION_ITEM_ID_INVALID";
    private static final String SUPER_OPERATION_ID_INVALID = "SUPER_OPERATION_ID_INVALID";
    private static final String OPERATION_QUANTITY_INVALID = "OPERATION_QUANTITY_INVALID";
    private static final String MAINTENANCE_PLAINT_EMPTY = "MAINTENANCE_PLAINT_EMPTY";
    private static final String MAINTENANCE_PLAINT_PERMISSION = "MAINTENANCE_PLAINT_PERMISSION";
    private static final String PERSON_PERMISSION = "PERSON_PERMISSION";

    //Post MeasPoint
    public static final String MEASPOINT_NOT_FOUND = "MEASPOINT_NOT_FOUND";
    public static final String MEASPOINT_DIFF_REQUIRE = "MEASPOINT_DIFF_REQUIRE";
    public static final String MEASPOINT_REQUIRE_COUNT_READ = "MEASPOINT_REQUIRE_COUNT_READ";
    public static final String MEASPOINT_REQUIRE_MEAS_READ = "MEASPOINT_REQUIRE_MEAS_READ";
    public static final String QUANTITY_MATERIAL_INVALID = "QUANTITY_MATERIAL_INVALID";


    
    private static final String EQUIPMENT_FUNCTIONAL_LOCATION_INVALID= "The equipment and functional location should " +
            "not be null or empty at the same time";
    private static final String OPERATION_DELETE_ERROR = "The work order should have more than one operation before " +
            "deleting operation";
    @Autowired
    private final MessageSource messageSource;
    private final Locale locale = LocaleContextHolder.getLocale();

    public <T> void validateInput(T requestPayload)
            throws MissingRequiredFieldException {
        Set<ConstraintViolation<T>> violations = validator.validate(requestPayload);
        if (!violations.isEmpty()) {
            final var violation = violations.iterator().next();
            final var errorPath = violation.getPropertyPath().toString();
            log.error(errorPath + " is required");
            throw new MissingRequiredFieldException(violation.getMessage());
        }
    }

    public void validateNotificationCreate(NotificationCreateRequest input) throws MissingRequiredFieldException {
        validateInput(input.getHeader());
    }

    public void validateNotificationItemCreate(NotificationItemDto input) throws InvalidInputException {
        if(input.getId() == null || input.getId().isBlank()){
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,NOTIFICATION_ID_INVALID));
        }
    }

    public void validateNotificationItemUpdate(NotificationItemDto input) throws InvalidInputException {
        if(input.getId() == null || input.getId().isBlank()){
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,NOTIFICATION_ID_INVALID));
        }

        if(input.getItem() == null || input.getItem().isBlank()){
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,NOTIFICATION_ITEM_ID_INVALID));
        }
    }

    public void validateSubOperationChange(OperationChangeRequest operationChangeRequest) throws MissingRequiredFieldException, InvalidInputException {
        if(operationChangeRequest.getType().equalsIgnoreCase("ADD")){
            // set OperationId = test to pass validation
            operationChangeRequest.setOperationId("create");
        }
        validateInput(operationChangeRequest);
        var subOperationId = operationChangeRequest.getSuperOperationId();
        if(subOperationId == null || subOperationId.isBlank() ){
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,SUPER_OPERATION_ID_INVALID));
        }
    }

    public void validateOperationCreate(OperationCreateRequest input) throws InvalidInputException, MissingRequiredFieldException {
        validateInput(input);
        if (!input.getType().equalsIgnoreCase("ADD")) {
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,OPERATION_TYPE_INVALID));
        }
        if (input.getSubOperations() != null && !input.getSubOperations().isEmpty()) {
            try {
                input.getSubOperations().forEach(request -> {
                    try {
                        request.setPersonnel(input.getPersonnel());
                        validateOperationCreate(request);
                    } catch (InvalidInputException | MissingRequiredFieldException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (RuntimeException e) {
                throw new InvalidInputException(MISSING_REQUIRED_FIELD, e.getMessage());
            }

        }
    }

    public void validateOperationUpdate(OperationCreateRequest input) throws InvalidInputException, MissingRequiredFieldException {
        validateInput(input);
        if (!input.getType().equalsIgnoreCase("CHANGE")) {
            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,OPERATION_TYPE_INVALID));
        }

        if (input.getSubOperations() != null && !input.getSubOperations().isEmpty()) {
            try {
                input.getSubOperations().forEach(request -> {
                    try {
                        request.setPersonnel(input.getPersonnel());
                        validateOperationUpdate(request);
                        if(request.getSuperOperationId() == null || request.getSuperOperationId().isEmpty() ||
                                !request.getSuperOperationId().equals(input.getOperationId())){
                            throw new InvalidInputException(MISSING_REQUIRED_FIELD, MessageUtil.getMessage(messageSource,locale,SUPER_OPERATION_ID_INVALID));
                        }
                    } catch (InvalidInputException | MissingRequiredFieldException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (RuntimeException e) {
                throw new InvalidInputException(MISSING_REQUIRED_FIELD, e.getMessage());
            }

        }
    }

    public void validateMaterialDocumentCreate(MaterialDocumentRequest input) throws MissingRequiredFieldException {
        validateInput(input);
        if (input.getItems() != null && !input.getItems().isEmpty()) {
            for (MaterialDocumentItem materialDocumentItem : input.getItems()) {
                validateInput(materialDocumentItem);
            }
        }
    }

    public void validateWorkOrderCreate(WorkOrderCreateRequest input)
            throws MissingRequiredFieldException, InvalidInputException {
        validateInput(input);
        validateOperation(input.getOperation().getItems());
    }

    public void validateWorkOrderCreateByEquipment(WorkOrderCreate workOrderCreate,
                                                   Equipment equipment) throws InvalidInputException {
        if (!inputSameEquipment(workOrderCreate, equipment)) {
            log.error("Input not same equipment table");
            throw new InvalidInputException(WORK_ORDER_NOT_SAME);
        }
    }

    public void validateWorkOrderCreateByFunctionalLocation(WorkOrderCreate workOrderCreate, FunctionalLocation functionalLocation)
            throws InvalidInputException {
        if (!inputSameFuncLocation(workOrderCreate, functionalLocation)) {
            log.error("Input not same functional location table");
            throw new InvalidInputException(WORK_ORDER_NOT_SAME);
        }
    }

    private void validateOperation(List<WorkOrderOperationItem> items)
            throws MissingRequiredFieldException, InvalidInputException {
        for (WorkOrderOperationItem item : items) {
            validateInput(item);
            var subOperations = item.getSubOperations();
            if (subOperations != null && !subOperations.isEmpty()) {
                for (WorkOrderOperationItem subOperation : subOperations) {
                    validateInput(subOperation);
                    if(!subOperation.getSuperOperationId().equals(item.getOperationId())){
                        throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource,locale,SUPER_OPERATION_ID_INVALID));
                    }
                }
            }
        }
    }

    private boolean inputSameEquipment(WorkOrderCreate workOrderCreate, Equipment equipment) {
        if (equipment.getMaintenancePlant() == null
                || equipment.getMaintenanceWorkCenter() == null
                || equipment.getFunctionalLocation() == null) {
            return false;
        }
        return
                workOrderCreate.getMaintenancePlant().equalsIgnoreCase(equipment.getMaintenancePlant().getCode())
                        && workOrderCreate.getWorkCenter()
                        .equalsIgnoreCase(equipment.getMaintenanceWorkCenter().getCode())
                        && workOrderCreate.getFunctionalLocation()
                        .equalsIgnoreCase(equipment.getFunctionalLocation().getFunctionalLocationId());
    }

    private boolean inputSameFuncLocation(WorkOrderCreate workOrderCreate,
                                          FunctionalLocation functionalLocation) {
        if (functionalLocation.getMaintenancePlant() == null) {
            return false;
        }
        return workOrderCreate.getMaintenancePlant()
                .equalsIgnoreCase(functionalLocation.getMaintenancePlant().getCode())
                && workOrderCreate.getWorkCenter()
                .equalsIgnoreCase(functionalLocation.getMaintenanceWorkCenterId());
    }

    public void validateMaintenancePlantPermission(List<String> plantIds, List<String> currentPlants) throws InvalidInputException {
        if (plantIds.stream().anyMatch(String::isEmpty)) {
            throw new InvalidInputException(INVALID_FIELD, MessageUtil.getMessage(messageSource,locale, MAINTENANCE_PLAINT_EMPTY));
        }

        for (String plantId : plantIds) {
            var hasPermission = hasPermissionPlant(plantId, currentPlants);
            if (!hasPermission) {
                throw new SecurityException(MessageUtil.getMessage(messageSource,locale,MAINTENANCE_PLAINT_PERMISSION) + plantId);
            }
        }

    }

    public void validatePersonPermission(List<String> permissionIds, String currentPermissionCode,
                                         String missingField) throws InvalidInputException {
        if (!isRoleAllPerson()) {
            if(permissionIds.isEmpty()){
                throw new InvalidInputException(MISSING_REQUIRED_FIELD,
                        String.format(MessageUtil.getMessage(messageSource,locale,"MISSING_REQUIRED_FIELD"), missingField));
            }

            for (String permissionId : permissionIds) {
                if(permissionId == null){
                    throw new InvalidInputException(MISSING_REQUIRED_FIELD,
                            String.format(MessageUtil.getMessage(messageSource,locale,"MISSING_REQUIRED_FIELD"), missingField));
                }
                if (!permissionId.equals(currentPermissionCode)) {
                    throw new SecurityException(MessageUtil.getMessage(messageSource,locale,PERSON_PERMISSION) + permissionId);
                }
            }
        }
    }

    public void validateEquipmentAndFunctionalLocation(String equipmentId, String functionalLocationId) throws InvalidInputException {
        if((equipmentId == null || equipmentId.isEmpty() || equipmentId.isBlank()) && (functionalLocationId == null ||
                functionalLocationId.isEmpty() || functionalLocationId.isBlank())){
            throw new InvalidInputException(INVALID_FIELD,EQUIPMENT_FUNCTIONAL_LOCATION_INVALID);
        }
    }

    public void validateDeletingOperation(Long totalOperations) throws InvalidInputException {
        if(totalOperations <= 1){
            throw new InvalidInputException(INVALID_FIELD, OPERATION_DELETE_ERROR);
        }
    }

    private boolean hasPermissionPlant(String permissionPlant, List<String> currentPlants) {
        return currentPlants.stream().anyMatch(s -> s.equals(permissionPlant));
    }

    private boolean isRoleAllPerson() {
        var departmentRoles = SecurityUtil.getCurrentUser().getDepartmentRoles();

        return departmentRoles.stream()
                .anyMatch(departmentRole -> departmentRole.getRoles().stream()
                        .anyMatch(role -> role.getPermissions().stream()
                                .anyMatch(permissionUser -> StringUtils.hasText(permissionUser.getCode())
                                        && permissionUser.getCode().equals(ALL_PERSON)
                                        && StringUtils.hasText(permissionUser.getAppCode())
                                        && permissionUser.getAppCode().equals(APPLICATION_ID))));
    }
}

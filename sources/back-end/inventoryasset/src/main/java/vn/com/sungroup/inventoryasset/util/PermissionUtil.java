/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import vn.com.sungroup.inventoryasset.config.interceptor.MultiTenantInterceptor;
import vn.com.sungroup.inventoryasset.dto.BaseResponse;

/**
 * PermissionUtil class.
 *
 * <p>Contains information about Permission Util.
 */
@Component
public class PermissionUtil {

    private static final String USER_NAME_PARAM = "?userName=";
    private static final String ROLE_PARAM = "&role=";
    private static final String TENANT_CONTEXT = "sgrpaperless";

    @Value("${security.permission.apiGrantRole}")
    private String apiGrantRole;

    @Value("${security.permission.apiCheckRole}")
    private String apiCheckRole;

    @Value("${security.permission.apiRevokeRole}")
    private String apiRevokeRole;

    public boolean check(String role, String username) throws Exception {
        String param = USER_NAME_PARAM + username + ROLE_PARAM + role;
        HttpMethod httpMethod = HttpMethod.GET;
        param = apiCheckRole + param;
        return callPermissionAPI(param, httpMethod);
    }

    public boolean revoke(String role, String username) throws Exception {
        String param = USER_NAME_PARAM + username + ROLE_PARAM + role;
        HttpMethod httpMethod = HttpMethod.PUT;
        param = apiRevokeRole + param;
        return callPermissionAPI(param, httpMethod);
    }

    public boolean grant(String role, String username) throws Exception {
        String param = USER_NAME_PARAM + username + ROLE_PARAM + role;
        HttpMethod httpMethod = HttpMethod.PUT;
        param = apiGrantRole + param;
        return callPermissionAPI(param, httpMethod);
    }

    private boolean callPermissionAPI(String param, HttpMethod httpMethod)
            throws IllegalArgumentException {
        boolean result = false;
        // Get token
        String token = SecurityUtil.getCurrentUser().getToken();
        if (StringUtils.isBlank(token)) {
            throw new IllegalArgumentException("PermissionUtil: Cannot get token");
        }
        // Set header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        headers.add(MultiTenantInterceptor.TENANT_HEADER_NAME, TENANT_CONTEXT);
        // Set Permission API's parameters
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Boolean> request = new HttpEntity<Boolean>(null, headers);
        ResponseEntity<BaseResponse<Boolean>> response;

        // Call Permission API
        response =
                restTemplate.exchange(
                        param,
                        httpMethod,
                        request,
                        new ParameterizedTypeReference<BaseResponse<Boolean>>() {});
        if (null != response) {
            if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null) {
                result = response.getBody().getData();
            } else {
                result = false;
            }
        }
        return result;
    }
}

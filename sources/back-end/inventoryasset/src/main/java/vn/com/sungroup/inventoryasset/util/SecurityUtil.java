/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.com.sungroup.inventoryasset.dto.User;

/**
 * SecurityUtil class.
 *
 * <p>Contains information about Security Util.
 */
public class SecurityUtil {
    public static User getCurrentUser(){
        User user = new User();
        OAuth2Authentication oauth2Authen = (OAuth2Authentication) SecurityContextHolder.getContext()
                .getAuthentication();
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) oauth2Authen.getDetails();
        UsernamePasswordAuthenticationToken userInfo = (UsernamePasswordAuthenticationToken) oauth2Authen
                .getUserAuthentication();
        String accessToken = details.getTokenValue();
//        UsernamePasswordAuthenticationToken userInfo = (UsernamePasswordAuthenticationToken) ;
        if (userInfo.getDetails() != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true);
      try {
        String json = objectMapper.writeValueAsString(userInfo.getDetails());
        user = objectMapper.readValue(json, User.class);
      } catch (JsonProcessingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
        }
        user.setToken(accessToken);
        return user;
    }
}

package vn.com.sungroup.inventoryasset.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringUtilsCustom {

  public static boolean isEmpty(String str) {
    return str == null || "".equals(StringUtils.trimWhitespace(str));
  }
}

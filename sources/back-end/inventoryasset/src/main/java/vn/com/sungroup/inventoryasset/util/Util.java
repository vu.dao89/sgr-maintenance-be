/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import lombok.extern.slf4j.Slf4j;
import vn.com.sungroup.inventoryasset.dto.FileResponseModel;
import vn.com.sungroup.inventoryasset.exception.ExternalApiException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Util class.
 *
 * <p>Contains information about Util.
 */
@Slf4j
public class Util {

    private static final String XSLX = ".xlsx";
    private static final String GET_METHOD = "GET";
    // User agent default system
    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)"
                + " Chrome/54.0.2840.99 Safari/537";
    private static final Pattern UUID_PATTERN =
            Pattern.compile(
                    "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$");

    /**
     * Convert a String object to object with parameter dateFormat
     *
     * @param <T>
     * @param jsonData
     * @param clazz
     * @param dateFormat
     * @return
     * @throws JsonMappingException
     * @throws JsonProcessingException
     */
    public static <T> T JSONDeserialize(Object jsonData, Class<T> clazz, String dateFormat)
        throws JsonProcessingException {
        if (null == jsonData) {
            return null;
        }
        if ((ClassUtils.isPrimitiveOrWrapper(clazz) || clazz == String.class)
                && ClassUtils.isPrimitiveOrWrapper(jsonData.getClass())) {
            return (T) jsonData;
        }
        ObjectMapper mapper =
                new ObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        if (StringUtils.isNotBlank(dateFormat)) {
            DateFormat df = new SimpleDateFormat(dateFormat);
            mapper.setDateFormat(df);
        }
        mapper.registerModule(new JavaTimeModule());
        return mapper.readValue(jsonData.toString(), clazz);
    }

    /**
     * Convert a object to Json String with parameter dateFormat
     *
     * @param object
     * @param dateFormat
     * @return
     */
    public static String JSONSerialize(Object object, String dateFormat) {
        try {
            if (null != dateFormat) {
                Gson gson =
                        new GsonBuilder()
                                .registerTypeAdapter(
                                        LocalDate.class,
                                        (JsonSerializer<LocalDate>)
                                                new JsonSerializer<LocalDate>() {
                                                    @Override
                                                    public JsonElement serialize(
                                                            LocalDate t,
                                                            java.lang.reflect.Type type,
                                                            JsonSerializationContext jsc) {
                                                        if (t != null) {
                                                            DateTimeFormatter dateTimeFormatter =
                                                                    DateTimeFormatter.ofPattern(
                                                                            dateFormat);
                                                            return new JsonPrimitive(
                                                                    t.format(dateTimeFormatter));
                                                        } else {
                                                            return null;
                                                        }
                                                    }
                                                })
                                .registerTypeAdapter(
                                        LocalDateTime.class,
                                        (JsonSerializer<LocalDateTime>)
                                                new JsonSerializer<LocalDateTime>() {
                                                    @Override
                                                    public JsonElement serialize(
                                                            LocalDateTime t,
                                                            java.lang.reflect.Type type,
                                                            JsonSerializationContext jsc) {
                                                        if (t != null) {
                                                            DateTimeFormatter dateTimeFormatter =
                                                                    DateTimeFormatter.ofPattern(
                                                                            dateFormat);
                                                            return new JsonPrimitive(
                                                                    t.format(dateTimeFormatter));
                                                        } else {
                                                            return null;
                                                        }
                                                    }
                                                })
                                .setDateFormat(dateFormat) // java.util.Date
                                .create();
                return gson.toJson(object);
            }
            return new Gson().toJson(object);
        } catch (Exception e) {
            log.error("JSONSerialize() ERR: {}", e);
            return null;
        }
    }

    /**
     * Call rest api for service
     *
     * @param url
     * @param requestParam
     * @param requestBody
     * @param token
     * @param method
     * @return data of service
     */
    public static String callRestApi(
            String url,
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            String token,
            String method)
            throws ExternalApiException {

        log.info(
                "START: CALLRESTAPI with url={}, requestParam={}, requestBody={}, token={},"
                    + " method={}",
                url,
                requestParam,
                requestBody,
                token,
                method);
        url = String.format(url);
        if (StringUtils.isNotBlank(token)) {
            token = token.replace("Bearer", "").trim();
        }
        RestTemplate restTemplate =
                new RestTemplateBuilder()
                        .setConnectTimeout(Duration.ofMillis(Constant.REST_TIMEOUT))
                        .setReadTimeout(Duration.ofMillis(Constant.REST_TIMEOUT))
                        .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (StringUtils.isNotBlank(token)) {
            headers.setBearerAuth(token);
        }
        headers.set("User-Agent", USER_AGENT);
        HttpEntity<Object> request;
        if (null != requestBody && requestBody.size() == 1) {
            Object requestBody1 = requestBody.get(requestBody.keySet().toArray()[0]);
            request = new HttpEntity<>(requestBody1, headers);
        } else {
            request = new HttpEntity<>(requestBody, headers);
        }

        if (GET_METHOD.equalsIgnoreCase(method)) {
            request = new HttpEntity<>(headers);
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
        uriBuilder.encode(StandardCharsets.US_ASCII);
        if (null != requestParam && !requestParam.isEmpty()) {
            for (Map.Entry<String, Object> map : requestParam.entrySet()) {
                if (map.getValue() instanceof List) {
                    for (Object o : (List<?>) map.getValue()) {
                        if (o != null) {
                            uriBuilder.queryParam(map.getKey(), o);
                        }
                    }
                } else {
                    uriBuilder.queryParam(map.getKey(), map.getValue());
                }
            }
        }

        try {
            ResponseEntity<String> response =
                    restTemplate.exchange(
                            uriBuilder.toUriString(),
                            HttpMethod.valueOf(method.toUpperCase()),
                            request,
                            String.class);
            if (HttpStatus.OK == response.getStatusCode()) {
                return response.getBody();
            }
        } catch (RuntimeException e) {
            throw new ExternalApiException(e.getMessage(), e);
        }

        log.info("END: CALLRESTAPI");
        return null;
    }

    /**
     * convert String value to Date with default system pattern
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date stringToDate(String date) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(date);
    }

    /**
     * Convert Excel data to List Object
     *
     * @param file
     * @param target
     * @param SHEET
     * @return List Object
     */
    public static <T> List<T> excelToList(
            MultipartFile file, Class<T> target, String SHEET, List<String> stFields)
            throws Exception {
        log.info(
                "START: EXCELTOLIST with file={}, target={}, SHEET={}, stFields={}",
                file,
                target,
                SHEET,
                stFields);
        List<T> result = new ArrayList<>();
        int numRow = 0;
        String cellValue = "";
        String columnValue = "";
        Workbook workbook = null;
        try {
            // Workbook workbook;
            InputStream is = file.getInputStream();
            // workbook = StreamingReader.builder().open(is);
            workbook = WorkbookFactory.create(is);
            // default Sheet
            Sheet sheet = workbook.getSheetAt(0);
            if (StringUtils.isNotBlank(SHEET)) {
                sheet = workbook.getSheet(SHEET);
            }
            Iterator<Row> rows = sheet.iterator();

            int rowNumber = 0;
            // list attribute in class
            Field[] objFields = target.getDeclaredFields();
            List<Field> fields = new ArrayList<Field>();
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    // map field with excel column
                    handleHeaderFileExcel(currentRow, objFields, fields, stFields);
                    rowNumber++;
                    continue;
                }
                List<Cell> listCell = new ArrayList<>();
                int fisrt = currentRow.getFirstCellNum();
                int lastColumn = Math.max(currentRow.getLastCellNum(), fields.size());
                for (int i = 0; i < lastColumn; i++) {
                    Cell cell = currentRow.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                    listCell.add(cell);
                }
                T obj = null;
                // column data
                obj = target.newInstance();
                numRow++;
                addDataToCellInExcelToList(listCell, fields, columnValue, obj, cellValue);
                result.add(obj);
            }

        } catch (Exception e) {
            if (file == null) {
                throw new RuntimeException("ERROR : EXCELTOLIST: Excel file not found", e);
            } else {
                throw new RuntimeException(
                        "ERROR : EXCELTOLIST: fail to parse Excel file: Row: "
                                + (numRow + 1)
                                + ", Column Value: "
                                + columnValue
                                + ", Cell Value: "
                                + cellValue,
                        e);
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            log.info("END: EXCELTOLIST");
        }

        return result;
    }

    /**
     * handle Header File Excel
     *
     * @param rowNumber
     * @param currentRow
     * @param objFields
     * @param fields
     */
    private static void handleHeaderFileExcel(
            Row currentRow, Field[] objFields, List<Field> fields, List<String> stFields) {
        // map field with excel column
        if (stFields != null) {
            for (String sField : stFields) {
                for (Field f : objFields) {
                    if (f.getName().equalsIgnoreCase(sField.trim())) {
                        f.setAccessible(true);
                        fields.add(f);
                    }
                }
            }
        } else {
            Iterator<Cell> cellsInRow = currentRow.iterator();
            while (cellsInRow.hasNext()) {
                Cell currentCell = cellsInRow.next();
                for (Field f : objFields) {
                    if (f.getName().equalsIgnoreCase(currentCell.getStringCellValue().trim())) {
                        f.setAccessible(true);
                        fields.add(f);
                    }
                }
            }
        }
    }

    /**
     * add Data To Cell In ExcelToList
     *
     * @param <T>
     * @param listCell
     * @param fields
     * @param columnValue
     * @param obj
     * @param cellValue
     * @throws Exception
     */
    private static <T> void addDataToCellInExcelToList(
            List<Cell> listCell, List<Field> fields, String columnValue, T obj, String cellValue)
            throws Exception {
        int cellIdx = 0;
        for (Cell currentCell : listCell) {
            Field f = fields.get(cellIdx);
            //            columnValue = f.getName();
            if (null != currentCell && "String".equalsIgnoreCase(f.getType().getSimpleName())) {
                //                cellValue = String.valueOf(currentCell);
                if (currentCell.getCellType() == CellType.NUMERIC
                        || currentCell.getCellType() == CellType.BOOLEAN) {
                    f.set(obj, String.valueOf(currentCell));
                } else {
                    f.set(obj, currentCell.getStringCellValue());
                }
            } else if (null != currentCell) {
                String dataType = f.getType().getSimpleName();
                //                cellValue = String.valueOf(currentCell);
                switchCaseDataTypeExcelToList(obj, currentCell, f, dataType);
            } else {
            }
            cellIdx++;
        }
    }

    /**
     * switch Case Data Type ExcelToList
     *
     * @param <T>
     * @param obj
     * @param currentCell
     * @param f
     * @param dataType
     * @throws IllegalAccessException
     */
    private static <T> void switchCaseDataTypeExcelToList(
            T obj, Cell currentCell, Field f, String dataType) throws Exception {
        switch (dataType) {
            case "Integer":
                f.set(
                        obj,
                        (currentCell != null && currentCell.getCellType() == CellType.NUMERIC)
                                ? (int) currentCell.getNumericCellValue()
                                : null);
                break;
            case "Long":
                f.set(
                        obj,
                        (currentCell != null && currentCell.getCellType() == CellType.NUMERIC)
                                ? (long) currentCell.getNumericCellValue()
                                : null);
                break;
            case "Double":
                f.set(
                        obj,
                        (currentCell != null && currentCell.getCellType() == CellType.NUMERIC)
                                ? currentCell.getNumericCellValue()
                                : null);
                break;
            case "BigDecimal":
                f.set(
                        obj,
                        (currentCell != null && currentCell.getCellType() == CellType.NUMERIC)
                                ? BigDecimal.valueOf(currentCell.getNumericCellValue())
                                : null);
                break;
            case "LocalDateTime":
                f.set(
                        obj,
                        currentCell != null
                                ? DateUtils.dateToLocalDateTime(currentCell.getDateCellValue())
                                : null);
                break;
            case "LocalDate":
                f.set(
                        obj,
                        currentCell != null
                                ? DateUtils.dateUtilToLocalDate(currentCell.getDateCellValue())
                                : null);
                break;
            case "LocalTime":
                f.set(
                        obj,
                        currentCell != null
                                ? DateUtils.dateTimeToLocalTime(
                                        currentCell.getLocalDateTimeCellValue())
                                : null);
                break;
            case "Boolean":
                f.set(
                        obj,
                        (currentCell != null && currentCell.getCellType() == CellType.BOOLEAN)
                                ? currentCell.getBooleanCellValue()
                                : null);
                break;
            case "UUID":
                f.set(
                        obj,
                        currentCell != null
                                        && UUID_PATTERN
                                                .matcher(currentCell.getStringCellValue())
                                                .matches()
                                ? java.util.UUID.fromString(currentCell.getStringCellValue())
                                : null);
                break;
            default:
                break;
        }
    }

    /**
     * Convert List Object to Excel data
     *
     * @param <T>
     * @param source
     * @param fileName
     * @param stFields
     * @param stDisplayFields
     * @param separator
     * @return
     * @throws Exception
     */
    public static <T> FileResponseModel listToExcel(
            List<T> source, String fileName, List<String> stFields, List<String> stDisplayFields)
            throws Exception {

        log.info(
                "START: LISTTOEXCELMODEL with source={}, fileName={}, stFields={},"
                    + " stDisplayFields={}",
                source,
                fileName,
                stFields,
                stDisplayFields);
        FileResponseModel excelModel = new FileResponseModel();
        try (Workbook workbook = new XSSFWorkbook()) {
            // init excel object
            Sheet sheet = workbook.createSheet(fileName);
            int rowNumber = 0;
            int cellNumber = 0;
            Row row;

            // set all field, column for excel file
            Class cls = source.get(0).getClass();
            List<Field> fields = new ArrayList<>();
            for (String field : stFields) {
                Field temp = cls.getDeclaredField(field.trim());
                fields.add(temp);
            }

            row = sheet.createRow(rowNumber);
            int i = 0;
            // create excel column header
            for (Field field : fields) {
                field.setAccessible(true);
                if (i < stDisplayFields.size()) {
                    row.createCell(cellNumber++).setCellValue(stDisplayFields.get(i));
                } else {
                    sheet.setColumnHidden(cellNumber, true);
                    row.createCell(cellNumber++).setCellValue("");
                }
                i++;
            }

            // create excel column data
            createExcelColumnData(source, workbook, sheet, rowNumber, fields);

            // write excel data to file
            Path tmpExtensionDir = Files.createTempDirectory(cls.getSimpleName());
            File file = new File(tmpExtensionDir.toString() + cls.getSimpleName() + XSLX);
            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                workbook.write(outputStream);
                excelModel.fullName = fileName + XSLX;
                excelModel.name = fileName;
                excelModel.path = file.getPath();
                excelModel.binary = FileUtils.readFileToByteArray(file);
                excelModel.extension = XSLX;
                excelModel.base64Code = Base64.getEncoder().encodeToString(excelModel.binary);
            }
            log.info("END: LISTTOEXCELMODEL");
        } catch (IOException | IllegalArgumentException | NoSuchFieldException e) {
            log.error("ERROR : process ListToExcel error: {}", e);
        }

        return excelModel;
    }

    /**
     * create Excel Column Data
     *
     * @param <T>
     * @param source
     * @param workbook
     * @param sheet
     * @param rowNumber
     * @param fields
     * @throws IllegalAccessException
     */
    private static <T> void createExcelColumnData(
            List<T> source, Workbook workbook, Sheet sheet, int rowNumber, List<Field> fields)
            throws IllegalAccessException {
        int cellNumber;
        Row row;
        Cell cell;
        for (Object obj : source) {
            row = sheet.createRow(++rowNumber);
            cellNumber = 0;
            for (Field field : fields) {
                cell = row.createCell(cellNumber++);

                if (field.get(obj) != null) {
                    switchCaseDataTypeListToExcel(workbook, cell, obj, field);
                } else {
                    cell.setCellValue("");
                }
            }
        }
    }

    /**
     * switch Case DataType ListToExcel
     *
     * @param workbook
     * @param cell
     * @param obj
     * @param field
     * @throws IllegalAccessException
     */
    private static void switchCaseDataTypeListToExcel(
            Workbook workbook, Cell cell, Object obj, Field field) throws IllegalAccessException {
        switch (field.getType().getName()) {
            case "java.lang.Integer":
                cell.setCellValue((Integer) field.get(obj));
                break;
            case "java.lang.Long":
                cell.setCellValue((Long) field.get(obj));
                break;
            case "java.lang.Boolean":
                cell.setCellValue((Boolean) field.get(obj));
                break;
            case "java.lang.Double":
                cell.setCellValue((Double) field.get(obj));
                break;
            case "java.time.LocalDate":
                cell.setCellValue((LocalDate) field.get(obj));
                CellUtil.setCellStyleProperty(
                        cell,
                        CellUtil.DATA_FORMAT,
                        workbook.getCreationHelper()
                                .createDataFormat()
                                .getFormat(Constant.FORMAT_DATE));
                break;
            case "java.time.LocalDateTime":
                cell.setCellValue((LocalDateTime) field.get(obj));
                CellUtil.setCellStyleProperty(
                        cell,
                        CellUtil.DATA_FORMAT,
                        workbook.getCreationHelper()
                                .createDataFormat()
                                .getFormat(Constant.FORMAT_DATE_TIME));
                break;
            case "java.time.LocalTime":
                LocalDateTime value =
                        LocalDateTime.of(LocalDate.of(1999, 1, 1), (LocalTime) field.get(obj));
                cell.setCellValue(value);
                CellUtil.setCellStyleProperty(
                        cell,
                        CellUtil.DATA_FORMAT,
                        workbook.getCreationHelper()
                                .createDataFormat()
                                .getFormat(Constant.FORMAT_TIME));
                break;
            default:
                cell.setCellValue(field.get(obj).toString());
                break;
        }
    }

    /**
     * Call expert api service other system
     *
     * @param url
     * @param requestParam
     * @param requestBody
     * @param pathVariable
     * @param mapHeaders
     * @param method
     * @return
     */
    public static String callExternalApi(
            String url,
            Map<String, Object> requestParam,
            Map<String, Object> requestBody,
            Map<String, Object> pathVariable,
            Map<String, String> mapHeaders,
            String method,
            long timeOut,
            int retryTimes)
            throws ExternalApiException {

        log.info(
                "START: CALLEXTERNALAPI with url={}, requestParam={}, requestBody={},"
                    + " pathVariable={}, mapHeaders={}, Method={}",
                url,
                requestParam,
                requestBody,
                pathVariable,
                mapHeaders,
                method);

        url = String.format(url);
        RestTemplate restTemplate =
                new RestTemplateBuilder()
                        .setConnectTimeout(Duration.ofMillis(timeOut))
                        .setReadTimeout(Duration.ofMillis(timeOut))
                        .build();
        HttpHeaders headers = new HttpHeaders();
        for (Map.Entry<String, String> map : mapHeaders.entrySet()) {
            headers.set(map.getKey(), map.getValue());
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (mapHeaders.get("Authorization") != null) {
            if (mapHeaders.get("Authorization").contains(" ")) {
                String authType = mapHeaders.get("Authorization").split(" ")[0];
                String credential = mapHeaders.get("Authorization").split(" ")[1];
                if (authType.equals("Bearer")) {
                    headers.setBearerAuth(credential);
                } else { 
                    headers.setBasicAuth(credential);
                }
            } else {
                headers.setBearerAuth(mapHeaders.get("Authorization"));
            }
        }
        headers.set("User-Agent", USER_AGENT);
        HttpEntity<Object> request;
        if (null != requestBody && requestBody.size() == 1) {
            Object requestBody1 = requestBody.get(requestBody.keySet().toArray()[0]);
            request = new HttpEntity<>(requestBody1, headers);
        } else {
            request = new HttpEntity<>(requestBody, headers);
        }

        if (GET_METHOD.equalsIgnoreCase(method)) {
            request = new HttpEntity<>(headers);
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
        uriBuilder.encode(StandardCharsets.UTF_8);
        if (null != requestParam && !requestParam.isEmpty()) {
            for (Map.Entry<String, Object> map : requestParam.entrySet()) {
                if (null != requestParam && !requestParam.isEmpty()) {
                    String key = map.getKey();
                    if (map.getValue() instanceof List) {
                        for (Object o : (List<?>) map.getValue()) {
                            if (o != null) {
                                uriBuilder.queryParam(key, o);
                            }
                        }
                    } else if (map.getValue() != null) {
                        uriBuilder.queryParam(key, map.getValue());
                    }
                }
            }
        }
        int retryCount = 0;
        boolean flgSuccess = false;
        while (!flgSuccess && retryCount <= retryTimes) {
            try {
                ResponseEntity<String> response =
                        restTemplate.exchange(
                                uriBuilder.buildAndExpand(pathVariable).toUri(),
                                HttpMethod.valueOf(method.toUpperCase()),
                                request,
                                String.class);
                if (HttpStatus.OK == response.getStatusCode()) {
                    flgSuccess = true;
                    return response.getBody();
                }
            } catch (RuntimeException e) {
                if (retryCount == retryTimes) {
                    throw new ExternalApiException("Error External Api Timeout", e);
                }
                log.info("RETRY NUMBER: {}", retryCount + 1);
            } finally {
                if (flgSuccess || retryCount == retryTimes) {
                    log.info("END: CALLEXTERNALAPI");
                }
                retryCount++;
            }
        }

        return null;
    }
}

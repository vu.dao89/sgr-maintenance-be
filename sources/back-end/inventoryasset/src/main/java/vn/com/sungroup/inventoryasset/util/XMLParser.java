package vn.com.sungroup.inventoryasset.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class XMLParser {

    private XMLParser() {
    }

    private static final XmlMapper xmlMapper = XmlMapper.xmlBuilder().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).build();

    public static <T> T parse(String xml, Class<T> clazz) {
        try {
            return xmlMapper.readValue(xml, clazz);
        } catch (Exception e) {
            log.error("Error when parse xml to object", e);
        }
        return null;
    }

    public static <T> T parse(String xml, TypeReference<T> typeReference) {
        try {
            return xmlMapper.readValue(xml, typeReference);
        } catch (Exception e) {
            log.error("Error when parse xml to object", e);
        }
        return null;
    }
}

/*
 * Copyright 2022 SpeedX Team.
 */

package vn.com.sungroup.inventoryasset.util.security;

import org.springframework.security.core.Authentication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigExpressionHandler {
    private String domainWorkflow;
    private String apiCheckPermission;
    private Authentication authentication;
}
package vn.com.sungroup.inventoryasset.util.security;

import static vn.com.sungroup.inventoryasset.util.security.PermissionCode.APPLICATION_ID;

import io.sentry.Sentry;
import io.sentry.SentryLevel;
import java.util.Arrays;
import java.util.List;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import vn.com.sungroup.inventoryasset.dto.permission.DepartmentRole;
import vn.com.sungroup.inventoryasset.util.SecurityUtil;

@Slf4j
public class CustomMethodSecurityExpressionRoot extends SecurityExpressionRoot
    implements MethodSecurityExpressionOperations {

  public CustomMethodSecurityExpressionRoot(ConfigExpressionHandler config) {
    super(config.getAuthentication());
  }

  public boolean checkPermission(String permission) {
    Sentry.captureMessage(permission, SentryLevel.INFO);
    Sentry.captureMessage(SecurityUtil.getCurrentUser().toString(), SentryLevel.INFO);
    List<DepartmentRole> departmentRoles = SecurityUtil.getCurrentUser().getDepartmentRoles();

    return departmentRoles.stream()
        .anyMatch(departmentRole -> departmentRole.getRoles().stream()
            .anyMatch(role -> role.getPermissions().stream()
                .anyMatch(permissionUser -> StringUtils.hasText(permissionUser.getCode())
                    && permissionUser.getCode().equals(permission)
                    && StringUtils.hasText(permissionUser.getAppCode())
                    && permissionUser.getAppCode().equals(APPLICATION_ID))));
  }

  public boolean checkPermissions(String... permissions) {
    Sentry.captureMessage(Arrays.toString(permissions), SentryLevel.INFO);
    Sentry.captureMessage(SecurityUtil.getCurrentUser().toString(), SentryLevel.INFO);
    List<DepartmentRole> departmentRoles = SecurityUtil.getCurrentUser().getDepartmentRoles();

    return departmentRoles.stream()
        .anyMatch(departmentRole -> departmentRole.getRoles().stream()
            .anyMatch(role -> role.getPermissions().stream()
                .anyMatch(permissionUser -> Arrays.stream(permissions)
                    .anyMatch(permission -> StringUtils.hasText(permissionUser.getCode())
                        && permissionUser.getCode().equals(permission)
                        && StringUtils.hasText(permissionUser.getAppCode())
                        && permissionUser.getAppCode().equals(APPLICATION_ID)))));

  }

  @Override
  public Object getFilterObject() {
    return null;
  }

  @Override
  public Object getReturnObject() {
    return null;
  }

  @Override
  public Object getThis() {
    return null;
  }

  @Override
  public void setFilterObject(Object arg0) {

  }

  @Override
  public void setReturnObject(Object arg0) {

  }

}
package vn.com.sungroup.inventoryasset.util.security;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PermissionCode {

  public static final String ASSET_APPLICATION = "ASSET";
  public static final String ASSET_SAP_DISPLAY = "ASSET_SAP_DISPLAY_69";
  public static final String ASSET_SAP_UPDATE = "ASSET_SAP_UPDATE_70";
  public static final String ASSET_SAP_INVENTORY = "ASSET_SAP_INVENTORY_71";

  public static final String WO_VIEW = "MTN_WO_VIEW";
  public static final String WO_SEARCH = "MTN_WO_SEARCH";
  public static final String WO_POST = "MTN_WO_POST";
  public static final String WO_CONFIRM_START = "MTN_WO_CONFIRM_START";
  public static final String MTN_WO_LOCK = "MTN_WO_LOCK";
  public static final String WO_CONFIRM_COMPLETE = "MTN_WO_CONFIRM_COMPLETE";
  public static final String WO_CHANGE_MAIN_PERSON = "MTN_WO_CHANGE_MAIN_PERSON";

  public static final String WO_OPS_VIEW = "MTN_WO_OPS_VIEW";
  public static final String WO_SUB_OPS_VIEW = "MTN_WO_SUB_OPS_VIEW";
  public static final String WO_OPS_SEARCH = "MTN_WO_OPS_SEARCH";
  public static final String WO_SUB_OPS_POST = "MTN_WO_SUB_OPS_POST";
  public static final String WO_OPS_POST = "MTN_WO_OPS_POST";
  public static final String WO_OPS_CONFIRM_COMPLETE = "MTN_WO_OPS_CONFIRM_COMPLETE";
  public static final String WO_OPS_CONFIRM_START = "MTN_WO_OPS_CONFIRM_START";
  public static final String WO_OPS_CHANGE_MAIN_PERSON = "MTN_WO_OPS_CHANGE_MAIN_PERSON";
  public static final String WO_SUB_OPS_CHANGE_MAIN_PERSON = "MTN_WO_SUB_OPS_CHANGE_MAIN_PERSON";
  public static final String WO_OPS_ATTACHMENT_POST = "MTN_WO_OPS_ATTACHMENT_POST";
  public static final String MTN_WO_OPS_MEASURING_DOC_POST = "MTN_WO_OPS_MEASURING_DOC_POST";

  public static final String NOTI_SEARCH = "MTN_NOTI_SEARCH";
  public static final String NOTI_VIEW = "MTN_NOTI_VIEW";
  public static final String NOTI_POST = "MTN_NOTI_POST";
  public static final String NOTI_CHANGE = "MTN_NOTI_CHANGE";
  public static final String NOTI_CONFIRM_COMPLETE = "MTN_NOTI_CONFIRM_COMPLETE";
  public static final String NOTI_ATTACHMENT_POST = "MTN_NOTI_ATTACHMENT_POST";

  public static final String EQUIP_MEAS_POST = "MTN_EQUIP_MEAS_POST";
  public static final String EQUIP_VIEW = "MTN_EQUIP_VIEW";
  public static final String EQUIP_MEAS_GET = "MTN_EQUIP_MEAS_GET";
  public static final String EQUIP_SEARCH = "MTN_EQUIP_SEARCH";
  public static final String EQUIP_ATTACHMENT_POST = "MTN_EQUIP_ATTACHMENT_POST";

  public static final String ALL_PERSON = "MTN_ALL_PERSON";

  public static final String PLANT_M003 = "MTN_PLANT_B003_M003";
  public static final String PLANT_M046 = "MTN_PLANT_B046_M046";

  public static final String APPLICATION_ID = "MAINTENANCE";
}
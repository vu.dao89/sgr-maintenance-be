DROP TABLE IF EXISTS ${schema}.notification_document;

CREATE TABLE IF NOT EXISTS ${schema}.notification_document
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    notification_code     varchar,
    document_id UUID,
    constraint notification_document_duplicate_key unique (notification_code, document_id)
    );

DROP TABLE IF EXISTS ${schema}.operation_document;

CREATE TABLE IF NOT EXISTS ${schema}.operation_document
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    operation_code     varchar,
    document_id UUID,
    constraint operation_document_duplicate_key unique (operation_code, document_id)
    );
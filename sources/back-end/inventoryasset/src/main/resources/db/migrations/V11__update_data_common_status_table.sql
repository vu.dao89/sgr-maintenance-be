-- common_status --
DROP TABLE IF EXISTS ${schema}.common_status;

CREATE TABLE IF NOT EXISTS ${schema}.common_status
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar,
    status varchar,
    description varchar,
    color_name varchar,
    color_code varchar,
    constraint code_status_duplicate_key unique(code, status)
    );

insert into ${schema}.common_status  (id, code, status, description, color_name, color_code)
values  (uuid_generate_v4(),'1 - Notification','I0068','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'1 - Notification','I0070','Đang thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'1 - Notification','I0071','Đã tạo lệnh bảo trì','Cam','FFCC66'),
        (uuid_generate_v4(),'1 - Notification','I0072','Hoàn thành','Xanh nước biển','1954C5'),
        (uuid_generate_v4(),'2 -Work order','I0001','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'2 -Work order','I0002','Đã ban hành thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'2 -Work order','I0010','Thực hiện 1 phần công việc','Vàng','FFFF00'),
        (uuid_generate_v4(),'2 -Work order','I0009','Thực hiện xong công việc','Cam','FFCC66'),
        (uuid_generate_v4(),'2 -Work order','I0045','Đã đóng','Xám','CCCCCC'),
        (uuid_generate_v4(),'2 -Work order','I0043','Đã khóa','Nâu','CC9900'),
        (uuid_generate_v4(),'2 -Work order','I0046','Đã tính giá thành','Xanh nước biển','1954C5'),
        (uuid_generate_v4(),'3 - Operation','I0001','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'3 - Operation','I0002','Đã ban hành thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'3 - Operation','I0010','Thực hiện 1 phần công việc','Vàng','FFFF00'),
        (uuid_generate_v4(),'3 - Operation','I0009','Thực hiện xong công việc','Cam','FFCC66'),
        (uuid_generate_v4(),'3 - Operation','I0045','Đã đóng','Xám','CCCCCC'),
        (uuid_generate_v4(),'3 - Operation','I0043','Đã khóa','Nâu','CC9900'),
        (uuid_generate_v4(),'3 - Operation','I0046','Đã tính giá thành','Xanh nước biển','1954C5');
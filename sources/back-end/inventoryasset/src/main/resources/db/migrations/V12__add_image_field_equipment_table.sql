ALTER TABLE ${schema}.equipment ADD COLUMN IF NOT EXISTS thumbnail_url VARCHAR;
ALTER TABLE ${schema}.equipment ADD COLUMN IF NOT EXISTS original_url VARCHAR;
ALTER TABLE ${schema}.equipment ADD COLUMN IF NOT EXISTS latitude VARCHAR;
ALTER TABLE ${schema}.equipment ADD COLUMN IF NOT EXISTS longitude VARCHAR;
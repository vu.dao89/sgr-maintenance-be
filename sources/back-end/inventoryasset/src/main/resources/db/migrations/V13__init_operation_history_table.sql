DROP TABLE IF EXISTS ${schema}.operation_history;
CREATE TABLE IF NOT EXISTS ${schema}.operation_history
(
    id    UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    operation_code          varchar unique,
    user_id         varchar,
    activity        varchar,
    start_at_date   date,
    start_at_time   time
    );
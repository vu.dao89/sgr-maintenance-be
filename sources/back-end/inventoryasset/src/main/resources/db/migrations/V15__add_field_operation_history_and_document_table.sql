ALTER TABLE ${schema}.operation_history ADD COLUMN IF NOT EXISTS workorder_id VARCHAR;
ALTER TABLE ${schema}.operation_document ADD COLUMN IF NOT EXISTS workorder_id VARCHAR;
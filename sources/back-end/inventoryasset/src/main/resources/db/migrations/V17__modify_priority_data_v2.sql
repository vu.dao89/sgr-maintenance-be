DROP TABLE IF EXISTS ${schema}.priority_type;

CREATE TABLE IF NOT EXISTS ${schema}.priority_type
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    type     varchar,
    priority varchar,
    description   varchar,
    color_name varchar,
    color_code varchar,
    constraint type_priority_duplicate_key unique (type, priority)
    );

-- priority_type --
insert into ${schema}.priority_type (id, type, priority, description, color_name, color_code)
values (uuid_generate_v4(),'PM','1','Rất quan trọng','Đỏ','FF6600'),
       (uuid_generate_v4(),'PM','2','Quan trọng','Cam','FFCC00'),
       (uuid_generate_v4(),'PM','3','Trung bình','Vàng','FFFF00'),
       (uuid_generate_v4(),'PM','4','Thấp','Xanh lá','CCFF00'),
       (uuid_generate_v4(),'Z1','1','1-Gấp','Đỏ','FF6600'),
       (uuid_generate_v4(),'Z1','2','2-Nghiêm trọng','Cam','FFCC00'),
       (uuid_generate_v4(),'Z1','3','3-Phức tạp','Vàng','FFFF00'),
       (uuid_generate_v4(),'Z1','4','4-Đơn giản','Xanh lá','CCFF00');



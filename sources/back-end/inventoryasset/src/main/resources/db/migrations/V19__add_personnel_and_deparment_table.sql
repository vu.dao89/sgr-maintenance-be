CREATE TABLE IF NOT EXISTS ${schema}.employee
(
    id                         UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    primary_id                 VARCHAR unique,
    name                       VARCHAR,
    employee_id                VARCHAR,
    employee_code              VARCHAR,
    parent_id                  VARCHAR,
    active                     VARCHAR,
    level                      VARCHAR,
    gender                     int,
    marital                    int,
    complete_name              VARCHAR,
    email                      VARCHAR,
    email_sending              VARCHAR,
    user_id                    VARCHAR
    );

CREATE TABLE IF NOT EXISTS ${schema}.department
(
    id                         UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    department_id              VARCHAR unique,
    department_code            VARCHAR,
    department_name            VARCHAR,
    complete_name              VARCHAR,
    name_short                 VARCHAR,
    employee_code              VARCHAR
    );

CREATE TABLE IF NOT EXISTS ${schema}.department_employee
(
    employee_id       UUID NOT NULL references employee (id),
    department_id     UUID NOT NULL references department (id)
    );

ALTER TABLE personal RENAME TO personnel;
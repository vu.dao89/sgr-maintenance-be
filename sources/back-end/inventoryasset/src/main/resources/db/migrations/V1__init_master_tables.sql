CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS ${schema}.measuring_point
(
    id                UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    point             varchar unique,
    position          varchar,
    description       varchar,
    characteristic    varchar,
    target_value      varchar,
    counter           varchar,
    lower_range_limit varchar,
    upper_range_limit varchar,
    range_unit        varchar,
    text              varchar,
    code_group        varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.characteristic
(
    id                UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    value_rel         varchar,
    char_value        varchar,
    object_class_flag varchar,
    internal_counter  varchar,
    char_id           varchar,
    class_type        varchar,
    char_val_counter  varchar,
    char_val_desc     varchar,
    class_id          varchar,
    constraint char_id_class_id_duplicate_key unique (char_id, class_id)
);

CREATE TABLE IF NOT EXISTS ${schema}.equipment_category
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar unique,
    description varchar(500)
);

CREATE TABLE IF NOT EXISTS ${schema}.system_status
(
    id                  UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    status_id           varchar unique,
    code                varchar,
    description         varchar,
    user_status_profile varchar,
    user_status         varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.partner
(
    id               UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    counter          varchar,
    partner_function varchar,
    partner_number   varchar unique,
    object_type_code varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.activity_key
(
    id              UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    work_order_type varchar,
    type            varchar,
    description     varchar,
    constraint work_order_type_type_duplicate_key unique(work_order_type, type)
);

CREATE TABLE IF NOT EXISTS ${schema}.activity_type
(
    id                     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    type                   varchar,
    description            varchar,
    cost_center            varchar,
    cost_center_short_text varchar,
    constraint type_cost_center_uq unique(type, cost_center)
);

CREATE TABLE IF NOT EXISTS ${schema}.priority_type
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    type     varchar,
    priority varchar,
    description   varchar,
    constraint type_priority_duplicate_key unique (type, priority)
);

CREATE TABLE IF NOT EXISTS ${schema}.maintenance_plant
(
    id             UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code           varchar unique,
    plant_name     varchar,
    planning_plant varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.system_condition
(
    id   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code varchar unique,
    text varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.object_type
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar unique,
    description varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.unit_of_measurement
(
    id        UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code      varchar unique,
    description    varchar,
    dimension varchar,
    rounding  varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.work_center
(
    id                   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code                 varchar,
    maintenance_plant_id varchar,
    cost_center_code     varchar,
    category             varchar,
    description          varchar,
    constraint code_maintenance_plant_id_duplicate_id unique (code, maintenance_plant_id)
);

CREATE TABLE IF NOT EXISTS ${schema}.control_key
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar unique,
    description varchar
);

CREATE TABLE IF NOT EXISTS ${schema}.personal
(
    id                   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code                 varchar,
    name                 varchar,
    start_date           DATE,
    end_date             DATE,
    work_center_id       varchar,
    maintenance_plant_id varchar,
    constraint code_start_date_end_date unique (code, start_date, end_date)
);

CREATE TABLE IF NOT EXISTS ${schema}.work_order_type
(
    id                   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    maintenance_plant_id varchar,
    type                 varchar,
    name                 varchar,
    constraint maintenance_plant_id_type_duplicate_key unique (maintenance_plant_id, type)
);

CREATE TABLE IF NOT EXISTS ${schema}.common_status
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar,
    status varchar,
    description varchar,
    constraint code_status_duplicate_key unique(code, status)
);

CREATE TABLE IF NOT EXISTS ${schema}.storage_location
(
    id                   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    maintenance_plant_id varchar,
    code                 varchar,
    description          varchar,
    constraint maintenance_plant_id_code_duplicate_key unique (maintenance_plant_id, code)
);

CREATE TABLE IF NOT EXISTS ${schema}.group_code
(
    id                   UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    catalog_profile      varchar,
    catalog_profile_text varchar,
    catalog              varchar,
    catalog_desc         varchar,
    code_group           varchar,
    code_group_desc      varchar,
    code                 varchar,
    code_desc            varchar,
    constraint catalog_profile_catalog_code_group_code_duplicate_key UNIQUE (catalog_profile, catalog, code_group, code)
);

CREATE TABLE IF NOT EXISTS ${schema}.notification_type
(
    id                 UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    noti_type          varchar unique,
    description        varchar,
    catalog_profile    varchar,
    problems           varchar,
    causes             varchar,
    activities         varchar,
    object_parts       varchar,
    work_order_type_id varchar,
    priority_type_id   varchar,
    person_responsible   varchar
);
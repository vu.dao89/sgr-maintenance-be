DROP TABLE IF EXISTS ${schema}.variance_reason;

CREATE TABLE IF NOT EXISTS ${schema}.variance_reason
(
    id                  UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code                varchar unique,
    description         varchar,
    require BOOLEAN,
    color_name varchar,
    color_code varchar,
    constraint code_duplicate_key unique (code)
    );

insert into ${schema}.variance_reason (id, code, description, require, color_name,color_code)
VALUES (uuid_generate_v4(),'F','KHÔNG ĐẠT [không vận hành]',true,'Đỏ','FF0000'),
       (uuid_generate_v4(),'N/A','KHÔNG KIỂM TRA',true,'Trắng','FFF'),
       (uuid_generate_v4(),'P','ĐẠT',false,'Xanh lá','CCFF00'),
       (uuid_generate_v4(),'S/M','ĐẠT, NHƯNG CẦN GIÁM SÁT',true,'Cam','FFCC66'),
       (uuid_generate_v4(),'X','TẠM DỪNG CÔNG VIỆC',false,'Xanh nước biển','4C8BFF');



alter table ${schema}.department drop constraint department_department_id_key;

alter table ${schema}.department add constraint department_unique unique (department_id, employee_code);
DROP TABLE IF EXISTS ${schema}.equipment_document;

CREATE TABLE IF NOT EXISTS ${schema}.equipment_document
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    equipment_code     varchar,
    document_id UUID,
    constraint equipment_document_duplicate_key unique (equipment_code, document_id)
    );

-- add column cost_center_name to table equipment
ALTER TABLE ${schema}.equipment ADD COLUMN IF NOT EXISTS cost_center_name VARCHAR;

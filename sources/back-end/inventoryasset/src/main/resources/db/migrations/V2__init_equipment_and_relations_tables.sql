CREATE TABLE IF NOT EXISTS ${schema}.equipment
(
    id                         UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    equipment_id               VARCHAR unique,
    parent_equipment_id        VARCHAR,
    const_year                 VARCHAR,
    const_month                VARCHAR,
    maintenance_plant_id       VARCHAR,
    functional_location_id     VARCHAR,
    equipment_category_id      VARCHAR,
    system_status_id           VARCHAR,
    description                VARCHAR,
    long_desc                  VARCHAR,
    equipment_type_id          VARCHAR,
    location                   VARCHAR,
    maintenance_work_center_id VARCHAR,
    cost_center                VARCHAR,
    manu_country               VARCHAR,
    manu_part_no               VARCHAR,
    manu_serial_no             VARCHAR,
    manufacturer               VARCHAR,
    model_number               VARCHAR,
    planner_group              VARCHAR,
    planning_plant_id          VARCHAR,
    plant_section              VARCHAR,
    size                       VARCHAR,
    weight                     VARCHAR,
    create_date                DATE,
    create_time                TIME,
    cus_warranty_end           DATE,
    cus_warranty_date          DATE,
    ven_warranty_end           DATE,
    ven_warranty_date          DATE,
    qr_code                    VARCHAR,
    startup_date               VARCHAR,
    end_of_use_date            VARCHAR
);

CREATE TABLE IF NOT EXISTS ${schema}.equipment_measuring_point
(
    equipment_id       UUID NOT NULL references equipment (id),
    measuring_point_id UUID NOT NULL references measuring_point (id)
);

CREATE TABLE IF NOT EXISTS ${schema}.equipment_characteristic
(
    equipment_id      UUID NOT NULL references equipment (id),
    characteristic_id UUID NOT NULL references characteristic (id)
);

CREATE TABLE IF NOT EXISTS ${schema}.equipment_partner
(
    equipment_id UUID NOT NULL references equipment (id),
    partner_id   UUID NOT NULL references partner (id)
);
ALTER TABLE ${schema}.system_status
DROP CONSTRAINT IF EXISTS system_status_status_id_key;

alter table ${schema}.system_status add constraint status_id_key unique (status_id);
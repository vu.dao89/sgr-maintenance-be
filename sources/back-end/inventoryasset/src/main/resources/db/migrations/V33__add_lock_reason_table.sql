DROP TABLE IF EXISTS ${schema}.lock_reason;

CREATE TABLE IF NOT EXISTS ${schema}.lock_reason
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    reason varchar,
    description   varchar,
    constraint reason_duplicate_key unique (reason)
    );

-- priority_type --
insert into ${schema}.lock_reason (id, reason, description)
values (uuid_generate_v4(),'Thiếu nhân sự vận hành','Thiếu nhân sự vận hành'),
       (uuid_generate_v4(),'Dừng theo kế hoạch vận hành','Dừng theo kế hoạch vận hành'),
       (uuid_generate_v4(),'Thời tiết','Thời tiết'),
       (uuid_generate_v4(),'Khắc phục thông báo','Khắc phục thông báo'),
       (uuid_generate_v4(),'Theo kế hoạch cần bảo trì','Theo kế hoạch cần bảo trì');
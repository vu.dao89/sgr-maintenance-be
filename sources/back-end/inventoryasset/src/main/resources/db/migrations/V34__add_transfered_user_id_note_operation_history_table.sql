ALTER TABLE ${schema}.operation_history ADD COLUMN IF NOT EXISTS transferred_user_id VARCHAR;
ALTER TABLE ${schema}.operation_history ADD COLUMN IF NOT EXISTS note VARCHAR;
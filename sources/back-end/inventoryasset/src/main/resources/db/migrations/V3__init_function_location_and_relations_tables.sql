CREATE TABLE IF NOT EXISTS ${schema}.functional_location
(
    id                            UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    functional_location_id        varchar unique,
    parent_functional_location_id varchar,
    const_year                    varchar,
    const_month                   varchar,
    maintenance_plant_id          varchar,
    functional_location_type_id   varchar,
    functional_location_category  varchar,
    description                   varchar,
    long_desc                     varchar,
    object_type_id                varchar,
    location                      varchar,
    maintenance_work_center_id    varchar,
    cost_center                   varchar,
    manu_country                  varchar,
    manu_part_no                  varchar,
    manu_serial_no                varchar,
    manufacturer                  varchar,
    model_number                  varchar,
    planner_group                 varchar,
    planning_plant_id             varchar,
    plant_section                 varchar,
    size                          varchar,
    weight                        VARCHAr,
    create_date                   DATE,
    cus_warranty_end              date,
    cus_warranty_date             date,
    ven_warranty_end              date,
    ven_warranty_date             date,
    qr_code                       varchar,
    startup_date                  varchar,
    end_of_use_date               varchar
    );

CREATE TABLE IF NOT EXISTS ${schema}.functional_location_measuring_point
(
    functional_location_id  UUID NOT NULL references functional_location (id),
    measuring_point_id      UUID NOT NULL references measuring_point (id)
    );

CREATE TABLE IF NOT EXISTS ${schema}.functional_location_characteristic
(
    functional_location_id UUID NOT NULL references functional_location (id),
    characteristic_id      UUID NOT NULL references characteristic (id)
    );


CREATE TABLE IF NOT EXISTS ${schema}.master_data_processed_history
(
    id    UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    foldername varchar,
    filename        varchar,
    status        varchar,
    processed_at       date,
    processed_at_time  time,
    backedup_at      date,
    backedup_at_time      time
    );
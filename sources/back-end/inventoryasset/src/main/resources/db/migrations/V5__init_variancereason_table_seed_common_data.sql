CREATE TABLE IF NOT EXISTS ${schema}.variance_reason
(
    id                  UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code                varchar unique,
    description         varchar
    );

insert into ${schema}.variance_reason (id, code, description)
VALUES (uuid_generate_v4(),'F','KHÔNG ĐẠT [không vận hành]'),
       (uuid_generate_v4(),'N/A','KHÔNG KIỂM TRA'),
       (uuid_generate_v4(),'P','ĐẠT'),
       (uuid_generate_v4(),'S/M','ĐẠT, NHƯNG CẦN GIÁM SÁT');
--- Seed common data ---
-- common_status --
insert into ${schema}.common_status  (id, code, status, description)
values  (uuid_generate_v4(),'1 - Notification','I068','Tạo mới'),
        (uuid_generate_v4(),'1 - Notification','I070','Đang thực hiện'),
        (uuid_generate_v4(),'1 - Notification','I071','Đã tạo lệnh bảo trì'),
        (uuid_generate_v4(),'1 - Notification','I072','Hoàn thành'),
        (uuid_generate_v4(),'2 -Work order','I0001','Tạo mới'),
        (uuid_generate_v4(),'2 -Work order','I0002','Đã ban hành thực hiện'),
        (uuid_generate_v4(),'2 -Work order','I0010','Thực hiện 1 phần công việc'),
        (uuid_generate_v4(),'2 -Work order','I0009','Thực hiện xong công việc'),
        (uuid_generate_v4(),'2 -Work order','I0045','Đã đóng'),
        (uuid_generate_v4(),'2 -Work order','I0043','Đã khóa'),
        (uuid_generate_v4(),'2 -Work order','I0046','Đã tính giá thành'),
        (uuid_generate_v4(),'3 - Operation','I0001','Tạo mới'),
        (uuid_generate_v4(),'3 - Operation','I0002','Đã ban hành thực hiện'),
        (uuid_generate_v4(),'3 - Operation','I0010','Thực hiện 1 phần công việc'),
        (uuid_generate_v4(),'3 - Operation','I0009','Thực hiện xong công việc'),
        (uuid_generate_v4(),'3 - Operation','I0045','Đã đóng'),
        (uuid_generate_v4(),'3 - Operation','I0043','Đã khóa'),
        (uuid_generate_v4(),'3 - Operation','I0046','Đã tính giá thành');

-- control_key --
insert into ${schema}.control_key (id, code, description)
values (uuid_generate_v4(),'ZM01','Sun group - internal'),
       (uuid_generate_v4(),'ZM02','Sun group - internal, external'),
       (uuid_generate_v4(),'ZM03','Sun Group Operation - no costs'),
       (uuid_generate_v4(),'ZM04','Sun Group Operation - Cost'),
       (uuid_generate_v4(),'PM01','Plant maintenance - internal'),
       (uuid_generate_v4(),'PM02','Plant maintenance - external');

-- priority_type --
insert into ${schema}.priority_type (id, type, priority, description)
values (uuid_generate_v4(),'PM','1','Rất quan trọng'),
       (uuid_generate_v4(),'PM','3','Quan trọng'),
       (uuid_generate_v4(),'PM','4','Trung bình'),
       (uuid_generate_v4(),'PM','5','Thấp'),
       (uuid_generate_v4(),'Z1','1','Cao'),
       (uuid_generate_v4(),'Z1','2','Thấp');

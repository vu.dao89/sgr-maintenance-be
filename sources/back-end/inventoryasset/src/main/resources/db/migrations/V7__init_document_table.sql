CREATE TABLE IF NOT EXISTS ${schema}.document
(
    id    UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    filename          varchar,
    file_type         varchar,
    original_url      varchar,
    thumbnail_url     varchar,
    uploaded_at       date,
    uploaded_at_time  time
    );
CREATE TABLE IF NOT EXISTS ${schema}.planner_group
(
    id    UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    maintenance_plant_id varchar,
    planner_group_id        varchar,
    name        varchar,
    constraint planner_group_id_maintenance_plant_id_duplicate_id unique (planner_group_id, maintenance_plant_id)
);
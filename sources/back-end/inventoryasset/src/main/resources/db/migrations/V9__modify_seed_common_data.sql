DROP TABLE IF EXISTS ${schema}.priority_type;

CREATE TABLE IF NOT EXISTS ${schema}.priority_type
(
    id       UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    type     varchar,
    priority varchar,
    description   varchar,
    color_name varchar,
    color_code varchar,
    constraint type_priority_duplicate_key unique (type, priority)
    );

-- priority_type --
insert into ${schema}.priority_type (id, type, priority, description, color_name, color_code)
values (uuid_generate_v4(),'PM','1','Rất quan trọng','Đỏ','FF6600'),
       (uuid_generate_v4(),'PM','3','Quan trọng','Cam','FFCC00'),
       (uuid_generate_v4(),'PM','4','Trung bình','Vàng','FFFF00'),
       (uuid_generate_v4(),'PM','5','Thấp','Xanh lá','CCFF00'),
       (uuid_generate_v4(),'Z1','1','Cao','Cam','FFCC66'),
       (uuid_generate_v4(),'Z1','2','Thấp','Xanh lá','CCFF00');

-- common_status --
DROP TABLE IF EXISTS ${schema}.common_status;

CREATE TABLE IF NOT EXISTS ${schema}.common_status
(
    id     UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    code   varchar,
    status varchar,
    description varchar,
    color_name varchar,
    color_code varchar,
    constraint code_status_duplicate_key unique(code, status)
    );

insert into ${schema}.common_status  (id, code, status, description, color_name, color_code)
values  (uuid_generate_v4(),'1 - Notification','I068','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'1 - Notification','I070','Đang thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'1 - Notification','I071','Đã tạo lệnh bảo trì','Cam','FFCC66'),
        (uuid_generate_v4(),'1 - Notification','I072','Hoàn thành','Xanh nước biển','1954C5'),
        (uuid_generate_v4(),'2 -Work order','I0001','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'2 -Work order','I0002','Đã ban hành thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'2 -Work order','I0010','Thực hiện 1 phần công việc','Vàng','FFFF00'),
        (uuid_generate_v4(),'2 -Work order','I0009','Thực hiện xong công việc','Cam','FFCC66'),
        (uuid_generate_v4(),'2 -Work order','I0045','Đã đóng','Xám','CCCCCC'),
        (uuid_generate_v4(),'2 -Work order','I0043','Đã khóa','Nâu','CC9900'),
        (uuid_generate_v4(),'2 -Work order','I0046','Đã tính giá thành','Xanh nước biển','1954C5'),
        (uuid_generate_v4(),'3 - Operation','I0001','Tạo mới','Trắng','FFF'),
        (uuid_generate_v4(),'3 - Operation','I0002','Đã ban hành thực hiện','Xanh lá','CCFF00'),
        (uuid_generate_v4(),'3 - Operation','I0010','Thực hiện 1 phần công việc','Vàng','FFFF00'),
        (uuid_generate_v4(),'3 - Operation','I0009','Thực hiện xong công việc','Cam','FFCC66'),
        (uuid_generate_v4(),'3 - Operation','I0045','Đã đóng','Xám','CCCCCC'),
        (uuid_generate_v4(),'3 - Operation','I0043','Đã khóa','Nâu','CC9900'),
        (uuid_generate_v4(),'3 - Operation','I0046','Đã tính giá thành','Xanh nước biển','1954C5');


module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'plugin:prettier/recommended'],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/no-shadow': ['error'],
        'no-shadow': 'off',
        'no-undef': 'off',
        'comma-dangle': ['error', 'never'],
        'react-native/no-inline-styles': 0,
        'prettier/prettier': ['error', {}, { usePrettierrc: true }],
      },
    },
  ],
};

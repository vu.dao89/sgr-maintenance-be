// In App.js in a new project

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import { Button, Text, View } from "react-native";

function HomeScreen({ navigation }: any) {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Home Screen</Text>
      <Button
        title="Navigate"
        onPress={() => {
          navigation?.navigate("Template");
        }}
      />
    </View>
  );
}

function TemplateScreen({ navigation }: any) {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Template Screen</Text>
      <Button
        title="Back"
        onPress={() => {
          navigation?.goBack();
        }}
      />
    </View>
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Template" component={TemplateScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

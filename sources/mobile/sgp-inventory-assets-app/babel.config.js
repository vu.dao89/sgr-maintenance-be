module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    '@babel/plugin-proposal-export-namespace-from',
    [
      'module-resolver',
      {
        root: ['./'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@Components': './src/app/components',
          '@Screens': './src/app/screens',
          '@I18n': './src/i18n',
          '@Assets': './src/assets',
          '@Actions': './src/redux/actions',
          '@Reducers': './src/redux/reducers',
          '@Sagas': './src/redux/sagas',
          '@Services': './src/services',
          '@Constants': './src/constants',
          '@Utils': './src/utils',
        },
      },
    ],
    [
      'module:react-native-dotenv',
      {
        envName: 'APP_ENV',
        moduleName: '@env',
        path: '.env',
        blocklist: null,
        allowlist: null,
        blacklist: null, // DEPRECATED
        whitelist: null, // DEPRECATED
        safe: false,
        allowUndefined: true,
        verbose: false,
      },
    ],
    'react-native-reanimated/plugin',
  ],
};

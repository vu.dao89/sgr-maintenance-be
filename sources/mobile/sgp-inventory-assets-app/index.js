import React from "react";
import { AppRegistry } from "react-native";
import XFramework from "react-native-x-framework";

import { Provider } from "react-redux";
import RootNavigator from "src/navigations/RootNavigator";
import { name as appName } from "./app.json";
import store from "./src/redux";

import { enableLatestRenderer } from "react-native-maps";

import { ToastProvider } from "react-native-toast-notifications";

enableLatestRenderer();

let isInitXFramwork = false;
if (!isInitXFramwork) {
  isInitXFramwork = true;
  XFramework.initMiniApp(
    {
      bundleName: appName,
      debug: true,
    },
    {}
  );
}
// eslint-disable-next-line react/prop-types
function HeadlessCheck({ isHeadless }) {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    return null;
  }
  return (
    <Provider store={store}>
      <ToastProvider>
        <RootNavigator />
      </ToastProvider>
    </Provider>
  );
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);

// ReactNativeBrotherPrintersModule.java

package com.rnbrotherprinters;

import android.util.Log;

import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.brother.sdk.lmprinter.Channel;
import com.brother.sdk.lmprinter.OpenChannelError;
import com.brother.sdk.lmprinter.PrintError;
import com.brother.sdk.lmprinter.PrinterDriver;
import com.brother.sdk.lmprinter.PrinterDriverGenerateResult;
import com.brother.sdk.lmprinter.PrinterDriverGenerator;
import com.brother.sdk.lmprinter.PrinterModel;
import com.brother.sdk.lmprinter.setting.PTPrintSettings;
import com.brother.sdk.lmprinter.setting.PrintImageSettings;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import java.io.File;
import java.util.ArrayList;

public class ReactNativeBrotherPrintersModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public ReactNativeBrotherPrintersModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "ReactNativeBrotherPrinters";
  }

  @ReactMethod
  public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
    // TODO: Implement some actually useful functionality
    callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);
  }

  @ReactMethod
  public void printImage(String uri, String ip, Boolean halfCut, Boolean autoCut, Callback callbackSuccess, Callback callbackFailure) {
    new Thread(createPrintActionRunnable(ip, uri,halfCut, autoCut, callbackSuccess, callbackFailure)).start();
  }

  @ReactMethod
  void searchWiFiPrinter(Callback callback) {
    new Thread(createSearchPrinterRunnable(callback)).start();

}

  private Runnable createSearchPrinterRunnable(final Callback callback){
    Runnable aRunnable = new Runnable(){
      public void run(){
        Printer printer = new Printer();
        NetPrinter[] printerList = printer.getNetPrinters("PT-P950NW");
        if (callback != null){
          WritableNativeArray array = new WritableNativeArray();
          for (NetPrinter netPrinter : printerList) {
            WritableNativeMap map = new WritableNativeMap();
            map.putString("modelName", netPrinter.nodeName);
            map.putString("serNo", netPrinter.serNo);
            map.putString("ipAddress", netPrinter.ipAddress);
            map.putString("macAddress", netPrinter.macAddress);
            map.putString("nodeName", netPrinter.nodeName);
            map.putString("location", netPrinter.location);
            array.pushMap(map);

          }

          WritableNativeMap re = new WritableNativeMap();
          re.putArray("devices", array);
          callback.invoke(re);
        }


      }
    };

    return aRunnable;

  }

  private Runnable createPrintActionRunnable(final String ip,final String uri, final Boolean halfCut, final Boolean autoCut, final Callback callbackSuccess, final Callback callbackFailure){
    Runnable aRunnable = new Runnable(){
      public void run(){
        Channel channel = Channel.newWifiChannel(ip);

        PrinterDriverGenerateResult result = PrinterDriverGenerator.openChannel(channel);
        if (result.getError().getCode() != OpenChannelError.ErrorCode.NoError) {
          callbackFailure.invoke();
          return;
        }

        File dir = reactContext.getExternalFilesDir(null);
        File file = new File(uri);

        PrinterDriver printerDriver = result.getDriver();
        PTPrintSettings printSettings = new PTPrintSettings(PrinterModel.PT_P950NW);

        printSettings.setLabelSize(PTPrintSettings.LabelSize.Width36mm);
        printSettings.setAutoCut(halfCut);
        printSettings.setHalfCut(autoCut);
        printSettings.setPrintOrientation(PrintImageSettings.Orientation.Landscape);
        printSettings.setWorkPath(dir.toString());

        PrintError printError = printerDriver.printImage(file.toString().replace("file:", ""), printSettings);
        if (printError.getCode() != PrintError.ErrorCode.NoError) {
          WritableNativeMap dataFailure = new WritableNativeMap();
          dataFailure.putString("code", printError.getCode().toString());
          dataFailure.putString("message", printError.getErrorRecoverySuggestion());
          callbackFailure.invoke(dataFailure);
        } else {
          callbackSuccess.invoke("SUCCESS");
        }
        printerDriver.closeChannel();
      }
    };

    return aRunnable;

  }
}

interface Config {
  bundleName: string;
  domain?: string;
  debug?: boolean;
  devLoad?: boolean;
}

interface ConfigAlo {
  debug: boolean;
}

declare module 'react-native-brother-printers' {


   /**
  * Prints an image
  *
  * @param device.ip               IP of printer of wifi
  * @param uri                     URI of image wanting to be printed
  * @param params
  * @param params.autoCut            Boolean if the printer should auto cut the receipt/label
  * @param params.onSuccess        Callback when success
  * @param params.onFailure        Callback when failure
  *
  * @return {Promise<*>}
 **/
 
  export function printImage(device: {ip: string}, uri: string, params ?: {
   onSuccess?: (...args: any[]) => void,
   onFailure?: (...args: any[]) => void,
   autoCut?: boolean,
   halfCut?: boolean,
 }):Promise<void>

 export function searchWiFiPrinter(printer: (printers: {devices: any[]}) => void): void

 export function discoverPrinters(params: any): void

 export function registerBrotherListener(key: string, method: (...args: any[]) => void): void
 
}


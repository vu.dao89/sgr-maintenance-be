// main index.js

import {NativeModules, NativeEventEmitter, Platform} from "react-native";

const {ReactNativeBrotherPrinters} = NativeModules;

export const Width3_5mm = 1;
export const Width6mm = 2;
export const Width9mm = 3;
export const Width12mm = 4;
export const Width18mm = 5;
export const Width24mm = 6;
export const Width36mm = 7;
export const WidthHS_5_8mm = 8;
export const WidthHS_8_8mm = 9;
export const WidthHS_11_7mm = 10;
export const WidthHS_17_7mm = 11;
export const WidthHS_23_6mm = 12;
export const WidthFL_21x45mm = 13;

export const LabelSize = {
  Width3_5mm,
  Width6mm,
  Width9mm,
  Width12mm,
  Width18mm,
  Width24mm,
  Width36mm,
  WidthHS_5_8mm,
  WidthHS_8_8mm,
  WidthHS_11_7mm,
  WidthHS_17_7mm,
  WidthHS_23_6mm,
  WidthFL_21x45mm,
}

export const LabelNames = [
  "3.5mm",
  "6mm",
  "9mm",
  "12mm",
  "18mm",
  "24mm",
  "36mm",
  "HS 5.8mm",
  "HS 8.8mm",
  "HS 11.7mm",
  "HS 17.7mm",
  "HS 23.6mm",
  "FL 21x45mm",
];

const {
  discoverPrinters: _discoverPrinters,
  pingPrinter: _pingPrinter,
  printImage: _printImage,
  // printPDF: _printPDF,
  searchWiFiPrinter: _searchWiFiPrinter,
} = ReactNativeBrotherPrinters;

/**
 * Starts the discovery process for brother printers
 *
 * @param params
 * @param params.V6             If we should searching using IP v6.
 * @param params.printerName    If we should name the printer something specific.
 *
 * @return {Promise<void>}
 */
export async function discoverPrinters(params = {}) {
  return _discoverPrinters(params);
}

/**
 * Checks if a reader is discoverable
 *
 * @param ip
 *
 * @return {Promise<void>}
 */
export async function pingPrinter(ip) {
  return _pingPrinter(ip);
}

export const printImage = async (device, uri, params = {
  onSuccess: () => {//
  },
  onFailure: () => {//
  },
  autoCut: true,
  halfCut: true,
}) => {
  if (!device.ip) {
    params?.onFailure({code: 'ERROR', message: 'The printer is not connected'});
    return new Error("No found printer");
  }
  if(Platform.OS === 'android') {
    return _printImage(uri, device.ip, params.halfCut || true, params.autoCut || true , params.onSuccess, params.onFailure);
  } else {
    try {
      await _printImage(uri, device.ip, params.halfCut || true, params.autoCut || true)
      params?.onSuccess()
    } catch(error) {
      params?.onFailure(error)
    }
  }
}

export const searchWiFiPrinter = (callback) => {
  return _searchWiFiPrinter(callback);
}

// export async function printPDF(device, uri, params = {}) {
//   return _printPDF(device, uri, params);
// }

const listeners = new NativeEventEmitter(ReactNativeBrotherPrinters);

export const registerBrotherListener = (key, method) => {
  return listeners.addListener(key, method);
}

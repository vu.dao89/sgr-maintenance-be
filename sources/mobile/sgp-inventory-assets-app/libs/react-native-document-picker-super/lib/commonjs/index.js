'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.pickDirectory = pickDirectory;
exports.pickMultiple = pickMultiple;
exports.pickSingle = pickSingle;
exports.pick = pick;
exports.releaseSecureAccess = releaseSecureAccess;
exports.isCancel = isCancel;
exports.default = exports.types = void 0;

var _reactNative = require('react-native');

var _invariant = _interopRequireDefault(require('invariant'));

var _fileTypes = require('./fileTypes');

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

const types = _fileTypes.perPlatformTypes[_reactNative.Platform.OS];
exports.types = types;
const RNDocumentPickerSuper = _reactNative.NativeModules.RNDocumentPickerSuper;

if (!RNDocumentPickerSuper) {
  // Use a timeout to ensure the warning is displayed in the YellowBox
  setTimeout(() => {
    console.warn(
      'RNDocumentPickerSuper: Native module is not available: Either the native module was not properly installed (please follow readme installation instructions)' +
        "or you're running in a environment without native modules (eg. JS tests in Node). A module mock is not available at this point, contributions are welcome!"
    );
  }, 0);
}

function pickDirectory() {
  if (_reactNative.Platform.OS === 'android' || _reactNative.Platform.OS === 'windows') {
    return RNDocumentPickerSuper.pickDirectory();
  } else {
    // TODO iOS impl
    return Promise.resolve(null);
  }
}

function pickMultiple(opts) {
  const options = { ...opts, allowMultiSelection: true };
  return pick(options);
}

function pickSingle(opts) {
  const options = { ...opts, allowMultiSelection: false };
  return pick(options).then(results => results[0]);
}

function pick(opts) {
  const options = {
    // must be false to maintain old (v5) behavior
    allowMultiSelection: false,
    type: [types.allFiles],
    ...opts
  };

  if (!Array.isArray(options.type)) {
    options.type = [options.type];
  } // @ts-ignore give me a break...

  return doPick(options);
}

function doPick(options) {
  var _options$mode, _options$copyTo;

  (0, _invariant.default)(
    !('filetype' in options),
    'A `filetype` option was passed to DocumentPicker.pick, the correct option is `type`'
  );
  (0, _invariant.default)(
    !('types' in options),
    'A `types` option was passed to DocumentPicker.pick, the correct option is `type`'
  );
  (0, _invariant.default)(
    options.type.every(type => typeof type === 'string'),
    `Unexpected type option in ${options.type}, did you try using a DocumentPicker.types.* that does not exist?`
  );
  (0, _invariant.default)(
    options.type.length > 0,
    '`type` option should not be an empty array, at least one type must be passed if the `type` option is not omitted'
  );
  (0, _invariant.default)(
    // @ts-ignore TS2345: Argument of type 'string' is not assignable to parameter of type 'PlatformTypes[OS][keyof PlatformTypes[OS]]'.
    !options.type.includes('folder'),
    'RN document picker: "folder" option was removed, use "pickDirectory()"'
  );

  if (
    'mode' in options &&
    !['import', 'open'].includes(
      (_options$mode = options.mode) !== null && _options$mode !== void 0 ? _options$mode : ''
    )
  ) {
    throw new TypeError('Invalid mode option: ' + options.mode);
  }

  if (
    'copyTo' in options &&
    !['cachesDirectory', 'documentDirectory'].includes(
      (_options$copyTo = options.copyTo) !== null && _options$copyTo !== void 0
        ? _options$copyTo
        : ''
    )
  ) {
    throw new TypeError('Invalid copyTo option: ' + options.copyTo);
  }

  return RNDocumentPickerSuper.pick(options);
}

function releaseSecureAccess(uris) {
  if (_reactNative.Platform.OS !== 'ios') {
    return Promise.resolve();
  }

  (0, _invariant.default)(
    Array.isArray(uris) && uris.every(uri => typeof uri === 'string'),
    `"uris" should be an array of strings, was ${uris}`
  );
  return RNDocumentPickerSuper.releaseSecureAccess(uris);
}

const E_DOCUMENT_PICKER_CANCELED = 'DOCUMENT_PICKER_CANCELED';

function isCancel(err) {
  return (err === null || err === void 0 ? void 0 : err.code) === E_DOCUMENT_PICKER_CANCELED;
}

var _default = {
  isCancel,
  releaseSecureAccess,
  pickDirectory,
  pick,
  pickMultiple,
  pickSingle,
  types,
  perPlatformTypes: _fileTypes.perPlatformTypes
};
exports.default = _default;
//# sourceMappingURL=index.js.map

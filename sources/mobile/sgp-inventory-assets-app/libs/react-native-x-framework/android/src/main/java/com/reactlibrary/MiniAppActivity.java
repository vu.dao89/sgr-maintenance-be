package com.reactlibrary;

import android.os.Bundle;

import com.airbnb.android.react.maps.MapsPackage;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactPackage;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.shell.MainReactPackage;

import java.util.ArrayList;
import java.util.Arrays;

import com.lewin.qrcode.QRScanReaderPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.rnbrotherprinters.ReactNativeBrotherPrintersPackage;
import com.reactnativecommunity.cameraroll.CameraRollPackage;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import com.rnziparchive.RNZipArchivePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
// import com.swmansion.reanimated.ReanimatedPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.swmansion.rnscreens.RNScreensPackage;
import com.rnfs.RNFSPackage;
import com.horcrux.svg.SvgPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.zoontek.rnpermissions.RNPermissionsPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.imagepicker.ImagePickerPackage;
import com.rnimagepicker.RNImagePickerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import org.reactnative.camera.RNCameraPackage;
import cl.json.RNSharePackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rnappauthsuper.RNAppAuthSuperPackage;
import com.henninghall.date_picker.DatePickerPackage;
import com.reactnativedocumentpickersuper.DocumentPickerSuperPackage;
import com.reactnativepagerview.PagerViewPackage;
import org.wonday.pdf.RCTPdfView;
import com.swmansion.reanimated.ReanimatedPackage;
import com.reactlibrary.XFrameworkPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import org.reactnative.maskedview.RNCMaskedViewPackage;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import com.toyberman.RNSslPinningPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.emeraldsanto.encryptedstorage.RNEncryptedStoragePackage;

public class MiniAppActivity extends ReactActivity {
    private static MiniAppActivity mInstance;
    private String mMainComponentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInstance = this;
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        mMainComponentName = bundle.getString("bundleName", "");
        boolean devLoad = bundle.getBoolean("devLoad");
        Bundle initProps = bundle.getBundle("initProps");

        ReactRootView mReactRootView = new ReactRootView(this);
        ReactInstanceManager mReactInstanceManager;
        if (false) {
            // mReactInstanceManager = ReactInstanceManager.builder()
            //         .setApplication(getApplication())
            //         .setCurrentActivity(this)
            //         .setBundleAssetName("index.android.bundle")
            //         .setJSMainModulePath("index")
            //         .addPackages(getPackages())
            //         .setUseDeveloperSupport(BuildConfig.DEBUG)
            //         .setInitialLifecycleState(LifecycleState.RESUMED)
            //         .build();
        } else {
            String appPath = bundle.getString("appPath", "");
            mReactInstanceManager = ReactInstanceManager.builder()
                    .setApplication(getApplication())
                    .setCurrentActivity(this)
                    .setBundleAssetName(mMainComponentName)
                    .setJSBundleFile(appPath)
                    .addPackages(getPackages())
                    .setInitialLifecycleState(LifecycleState.RESUMED)
                    .build();
        }

        mReactRootView.startReactApplication(mReactInstanceManager, mMainComponentName, initProps);
        setContentView(mReactRootView);
    }

    public ArrayList<ReactPackage> getPackages() {
        return new ArrayList<>(Arrays.<ReactPackage>asList(
                new MainReactPackage(),
                new XFrameworkPackage(),
                new AsyncStoragePackage(),
                new RNGestureHandlerPackage(),
                // new ReanimatedPackage(),
                new SafeAreaContextPackage(),
                new RNScreensPackage(),
                new RNFSPackage(),
                new LinearGradientPackage(),
                new SvgPackage(),
                new RNPermissionsPackage(),
                new FastImageViewPackage(),
                new ImagePickerPackage(),
                new RNImagePickerPackage(),
                new PickerPackage(),
                new RNSharePackage(),
                new ReactNativeBrotherPrintersPackage(),
                new RNCameraPackage(),
                new QRScanReaderPackage(),
                new RNViewShotPackage(),
                new CameraRollPackage(),
                new RNDateTimePickerPackage(),
                new RNAppAuthSuperPackage(),
                new DatePickerPackage(),
                new RNDeviceInfo(),
                new DocumentPickerSuperPackage(),
                new PagerViewPackage(),
                new RCTPdfView(),
                new ReanimatedPackage(),
                new RNZipArchivePackage(),
                new RNFetchBlobPackage(),
                new RNCMaskedViewPackage(),
                new NetInfoPackage(),
                new RNFileViewerPackage(),
                new RNSslPinningPackage(),
                new MapsPackage(),
                new RNFusedLocationPackage(),
                new RNEncryptedStoragePackage()
        ));
    }

    @Override
    protected String getMainComponentName() {
        return mMainComponentName;
    }

    public static void close() {
        if (mInstance != null) mInstance.finish();
        mInstance = null;
    }
}

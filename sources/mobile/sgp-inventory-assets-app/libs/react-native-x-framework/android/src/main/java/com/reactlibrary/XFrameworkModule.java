package com.reactlibrary;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class XFrameworkModule extends ReactContextBaseJavaModule {

    private static final Map<String, ReactContext> _reactContexts = new HashMap<>();
    private static final Map<String, Promise> _promises = new HashMap<>();
    private static final Map<String, String[]> _whiteList = new HashMap<>();
    private static Callback _closeCallback = null;

    private final ReactApplicationContext reactContext;

    public XFrameworkModule(ReactApplicationContext reactContext) {
        super(reactContext);
        // TODO: Initialize permissions
        _whiteList.put("123abcdo-d1c9-4db5-b36a-92000a19237c",
                new String[]{"flow", "zzzTestzzz", "voucher"});
        this.reactContext = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return "XFramework";
    }

    @ReactMethod
    public void openApp(String bundleName, String appPath, ReadableMap initProps,
                        boolean devLoad, Callback callback) {
        Intent intent = new Intent(reactContext, MiniAppActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString("bundleName", bundleName);
        bundle.putString("appPath", appPath);
        bundle.putBoolean("devLoad", devLoad);
        bundle.putBundle("initProps", Arguments.toBundle(initProps));
        intent.putExtras(bundle);
        reactContext.startActivity(intent);
        _closeCallback = callback;
    }

    @ReactMethod
    public void closeApp(String bundleName, ReadableMap result) {
        if(_closeCallback == null) {
            return;
        }
        if ( result != null){
            WritableMap map = Arguments.createMap();
            map.merge((result));
            _closeCallback.invoke(map);
        }

        MiniAppActivity.close();
        _closeCallback = null;
        _reactContexts.remove(bundleName);
    }

    @ReactMethod
    public void addBridge(String bundleName) {
        _reactContexts.put(bundleName, reactContext);
    }

    @ReactMethod
    public void sendMessage(String bundleName, ReadableMap msg, Callback callback) {
        ReactContext reactContext = _reactContexts.get(bundleName);
        WritableMap result = Arguments.createMap();
        if (reactContext != null) {
            WritableMap map = Arguments.createMap();
            map.merge(msg);
            pushEvent(reactContext, "EventMessage", map);
            result.putString("msg", "Send message ok!");
        } else {
            result.putString("msg", "Cannot find this bundle name " + bundleName);
        }
        callback.invoke(result);
    }

    @ReactMethod
    public void sendRequest(String bundleName, ReadableMap request, Promise promise) {
        ReactContext reactContext = _reactContexts.get(bundleName);

        String appId = request.getString("appId");
        String type = request.getString("type");
        if (appId == null || type == null) {
            promise.reject(new Throwable("Invalid request, missing appId or type"));
            return;
        }

        String[] permissions = _whiteList.get(appId);
        if (permissions == null || !Arrays.asList(permissions).contains(type)) {
            promise.reject(new Throwable("The request isn't allowed"));
            return;
        }

        if (reactContext != null) {
            String requestId = UUID.randomUUID().toString();
            _promises.put(requestId, promise);
            WritableMap map = Arguments.createMap();
            map.putString("id", requestId);
            map.merge(request);
            pushEvent(reactContext, "EventRequest", map);
        } else {
            promise.reject(new Throwable("Cannot find this bundle name " + bundleName));
        }
    }

    @ReactMethod
    public void replyResponse(String requestId, ReadableMap response, Callback callback) {
        Promise promise = _promises.get(requestId);
        WritableMap result = Arguments.createMap();
        if (promise != null) {
            WritableMap map = Arguments.createMap();
            map.merge(response);
            promise.resolve(map);
            // Clean promise container
            _promises.remove(requestId);
            result.putString("msg", "Reply response ok!");
        } else {
            result.putString("msg", "Cannot find promise with id " + requestId);
        }
        callback.invoke(result);
    }

    @ReactMethod
    public void replyError(String requestId, ReadableMap error, Callback callback) {
        Promise promise = _promises.get(requestId);
        WritableMap result = Arguments.createMap();
        if (promise != null) {
            WritableMap map = Arguments.createMap();
            map.merge(error);
            promise.reject(new Throwable("Error happens at request handler"), map);
            // Clean promise container
            _promises.remove(requestId);
            result.putString("msg", "Reply error ok!");
        } else {
            result.putString("msg", "Cannot find promise with id " + requestId);
        }
        callback.invoke(result);
    }

    @ReactMethod
    public void bringSuperToFront(String superappPackage, Callback callback) {
        WritableMap result = Arguments.createMap();
        try {
            Class<?> aClass = Class.forName(superappPackage + ".MainActivity");
            Intent intent = new Intent(reactContext, aClass);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            reactContext.startActivity(intent);
            result.putString("msg", "Ok");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            result.putString("error", e.getMessage());
        }
        callback.invoke(result);
    }

    @ReactMethod
    public void bringMiniAppToFront(Callback callback) {
        WritableMap result = Arguments.createMap();
        if (_closeCallback != null) {
            result.putString("msg", "Ok");
            Intent intent = new Intent(reactContext, MiniAppActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            reactContext.startActivity(intent);
        } else {
            result.putString("msg", "Cannot find mini app");
        }
        callback.invoke(result);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @ReactMethod
    public void getBundleNames(Promise promise) {
        if (_reactContexts.keySet().toArray() != null) {
            String[] bundleNames = Objects.requireNonNull(_reactContexts.keySet().toArray(new String[0]));
            WritableArray arrays = Arguments.fromArray(bundleNames);
            promise.resolve(arrays);
        } else {
            promise.reject(new Throwable("No listeners"));
        }
    }

    private void pushEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}

interface Config {
  bundleName: string;
  domain?: string;
  debug?: boolean;
  devLoad?: boolean;
}

interface ConfigAlo {
  debug: boolean;
}

declare module 'react-native-x-framework' {
  const XFramework: {
    AppList: any;
    LoadingBar: any;
    init(config: Config): void;
    initMiniApp(config: Config, initProps?: object): void;
    getApps(): Promise<object>;
    openApp(id: string, initProps?: object, progressCB?: void): Promise<string>;
    closeApp(): void;
    cleanApp(id: string): Promise<void>;
    clearApps(): Promise<void>;
    cleanOldAppList(): Promise<void>;
    getIconUrl(id: string): string;
    bringSuperToFront(callback?: () => void): void;
    bringMiniAppToFront(callback?: () => void): void;
    setCommonLogInfo(info: object): void;
    setRemoteLoggers(
      log: (commonInfo: object, message: string) => void,
      warn: (commonInfo: object, warnMessage: string) => void,
      error: (commonInfo: object, errorMessage: string) => void,
    ): void;
    Res: {
      init(appJson: object, imgInfos: object): void;
      getImage(
        staticImage: object,
        imgPath: string,
        forceInteralStorage: boolean,
      ): object;
    };
    Alo: {
      subscribe(eventName: string, listener: (data: object) => void): string;
      unsubscribe(token: string): void;
      publish(eventName: string, data: object): Promise<void>;
      register(
        requestType: string,
        handler: (
          req: object,
          sendRes: (res?: object) => void,
          sendErr: (res?: object) => void,
        ) => void,
      ): void;
      request(type: string, data: object): Promise<object>;
    };
  };
  export default XFramework;
}

import logger, {init as initLogger, log, warn, error} from './js/logger';
import AppList from './js/ui/AppList';
import LoadingBar from './js/ui/loading/LoadingBar';
import appManager, {init as initAppManager} from './js/app-manager';
import miniAppManager, {
  init as initMiniAppManager,
} from './js/mini-app-manager';

import {init as intInteract} from './js/interact';
import imageDensity from './js/image-density';
import zEvent, {init as initEvent} from './js/z-event';
import zRequest, {
  init as initRequest,
  initMiniApp as initMiniAppRequest,
} from './js/z-request';

// Default config
let _config = {
  bundleName: '',
  domain: '',
  debug: false,
  devLoad: false,
  localAppList: null,
  package: '',
};

let _miniAppConfig = {
  bundleName: '',
  debug: false,
};

const init = config => {
  initLogger(config);
  log('init', 'Init with', config);
  Object.assign(_config, config);
  initAppManager(_config);
  intInteract(_config);
  initEvent(_config);
  initRequest(_config);
};

const initMiniApp = (config, initProps) => {
  initLogger(config);
  log('init', 'Init with', config, initProps);
  Object.assign(_miniAppConfig, config);
  initMiniAppManager(config);
  intInteract(_miniAppConfig);
  initEvent(_miniAppConfig);
  initMiniAppRequest(_miniAppConfig, initProps);
};

const resInit = (appJson, imgInfos) => {
  if (appJson) {
    if (!appJson.id || !appJson.version) {
      warn('resInit', 'Missing id or version in app.json');
    } else {
      imageDensity.init(appJson, imgInfos);
    }
  }
};

// TODO: Lifecycle => pause, update, download

//--------------------------------------------
const {getImage} = imageDensity;
export default {
  AppList,
  LoadingBar,
  init,
  initMiniApp,
  ...appManager,
  ...miniAppManager,
  ...logger,
  Res: {init: resInit, get: getImage},
  Alo: {...zEvent, ...zRequest},
};

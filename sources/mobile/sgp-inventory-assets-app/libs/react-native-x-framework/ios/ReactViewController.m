#import "ReactViewController.h"
#import "RCTRootView.h"

@implementation ReactViewController

- (instancetype)initWithBridge:(RCTBridge *)bridge moduleName:(NSString*)moduleName initialProps:(NSDictionary*)initialProps
{
  self = [super init];
  if(self)
  {
    self.view = [[RCTRootView alloc] initWithBridge:bridge moduleName:moduleName initialProperties:initialProps];
  }
  return self;
}

@end

import {NativeModules, Alert, Platform} from 'react-native';
const {XFramework} = NativeModules;
// Internal
import {log, logError, warn} from './logger';
import util from './util';
import cache from './cache';

let _apps = [];
let _remoteApps;
let _localApps;

let _domain;
let _debug;
let _bundleName;
let _devLoad;
let _openningApp;

// For Android
let _package;

// Init
export async function init(config) {
  log('init', 'Init app manager', config);
  _domain = config.domain;
  _debug = config.debug;
  _bundleName = config.bundleName;
  _localApps = config.localAppList;
  _package = config.package;
  enableDevLoad(config.devLoad);

  util.init(_debug);

  _apps = await cache.AppList;
  _apps = _mergeApps(_apps, _localApps, null);
  cache.AppList = _apps;

  log('init', 'Cached app list', _apps);
}

// Public
function getApps() {
  return new Promise(async (resolve, reject) => {
    if (!_checkDomain()) {
      reject(new Error('[XFramework/getApps] Invalid domain'));
    }

    const appsUrl = util.getAppsUrl(_domain);
    log('getApps', 'appsUrl', _domain);

    try {
      const response = await util.getAppList(appsUrl);
      log('getAppsresponse', 'response', response);
      if (response) {
        _remoteApps = await response.json();
        _apps = _mergeApps(_apps, _localApps, _remoteApps);
        // Keep for checking and validating later
        cache.AppList = _apps;
        resolve(_apps);
      } else {
        if (_localApps && _apps && _apps.length) {
          _apps = _mergeApps(_apps, _localApps, null);
          resolve(_apps);
          // Keep for checking and validating later
          cache.AppList = _apps;
        } else {
          reject(
            new Error(
              '[XFramework/getApps] Error when getting app list',
              response,
            ),
          );
        }
      }
    } catch (error) {
      warn('getApps', '', error);
      reject(error);
    }
  });
}

function openApp(id, initProps, progressCB, closeCB = () => {}) {
  if (_openningApp) {
    log('openApp', 'Openning app', _openningApp);
    return;
  }
  const app = getAppById(id);
  _openningApp = app;
  log('openApp', 'Openning app', _openningApp);
  return new Promise(async (resolve, reject) => {
    // let VERSION_SUPER = require('../../../package.json').version;
    // if (!app) {
    //   reject(new Error('[XFramework/openApp] Cannot find this app'));
    //   return;
    // }

    // if (app.disabled) {
    //   Alert.alert(
    //     'Administrator',
    //     'This functionality is disabled temporarily, please try again later',
    //   );
    //   _openningApp = null;
    //   return;
    // }

    // if (VERSION_SUPER < app.versionSuperApp) {
    //   Alert.alert(
    //     'Administrator',
    //     `This app is only compatible with version equal or higher than ${app.versionSuperApp
    //     }, please upgrade!!!`,
    //   );
    //   _openningApp = null;
    //   return;
    // }

    log('openApp', 'Opening', app, initProps);

    // Open app with current version
    try {
      const result = await openVersionApp(
        app?.bundleName,
        app?.id,
        app?.version,
        app?.remote,
        initProps,
        progressCB,
        closeCB,
        app?.size,
        app?.checksum,
        app?.security,
        app?.forced,
      );
      resolve(result);
      if (progressCB) {
        progressCB('end', result);
      }
      _openningApp = null;
    } catch (error) {
      _openningApp = null;
      if (progressCB) {
        progressCB('end', error);
      }
      warn('openApp', '', error);
      reject(error);
    }
  });
}

async function cleanApp(id) {
  try {
    const appFolder = util.getAppFolder(id);
    await util.deleteFolder(appFolder);
    log('cleanApp', 'successfully');
  } catch (error) {
    warn('cleanApp', '', error);
  }
}

function clearApps() {
  return new Promise(async (resolve, reject) => {
    try {
      const appsFolder = util.getAppsFolder();
      await util.deleteFolder(appsFolder);
      cache.AppList = [];
      log('clearApps', 'successfully');
      resolve();
    } catch (error) {
      warn('clearApps', '', error);
      reject(error);
    }
  });
}

function cleanOldAppList() {
  return new Promise(async (resolve, reject) => {
    try {
      _apps = [];
      log('cleanOldAppList', 'successfully');
      resolve();
    } catch (error) {
      warn('cleanOldAppList', '', error);
      reject(error);
    }
  });
}

const getIconUrl = id => {
  return util.getIconUrl(_domain, id);
};

function bringSuperToFront(callback = () => {}) {
  if (Platform.OS === 'android') {
    if (!_package) {
      Alert.alert(
        'Error',
        'We need to init XFramework with package on Android',
      );
    } else {
      XFramework.bringSuperToFront(_package, callback);
    }
  } else {
    XFramework.bringSuperToFront(callback);
  }
}

function bringMiniAppToFront(callback = () => {}) {
  XFramework.bringMiniAppToFront(callback);
}

export default {
  getApps,
  openApp,
  cleanApp,
  clearApps,
  getIconUrl,
  bringSuperToFront,
  bringMiniAppToFront,
  cleanOldAppList,
};

// Private
function enableDevLoad(enable) {
  if (__DEV__) {
    _devLoad = enable;
  } else {
    _devLoad = false;
  }
}

// Support methods
// Flag: These flags is used to control app version from server by administrator
// 1. No any flag: The highest version is choosed
// 2. disabled: Throw error "The app is disabled by administrator"
// 3. forced: Must use this remote app version
// 4. useLocal: If disabled & forced are false, must use local version if existing,
const _mergeApps = (cachedApps = [], localApps, remoteApps) => {
  if (localApps) {
    localApps = localApps.map(el => {
      el.remote = false;
      return el;
    });
  }

  if (remoteApps) {
    remoteApps = remoteApps.map(el => {
      el.remote = true;
      return el;
    });
  }
  //==========================
  let mergedApps = cachedApps.length
    ? cachedApps
    : localApps || remoteApps || [];

  if (cachedApps.length && localApps) {
    let index;
    for (index in localApps) {
      const localApp = localApps[index];
      const app = mergedApps.find(el => el.id === localApp.id);
      if (app) {
        if (localApp.version > app.version) {
          Object.assign(app, localApp);
        } else {
          // TODO: Think about this case
        }
      } else {
        mergedApps.push(localApp);
      }
    }
  }

  if (remoteApps) {
    let index;
    for (index in remoteApps) {
      const remoteApp = remoteApps[index];
      const app = mergedApps.find(el => el.id === remoteApp.id);
      if (app) {
        if (remoteApp.disabled) {
          app.disabled = true;
        } else {
          app.disabled = false;
          if (remoteApp.forced) {
            Object.assign(app, remoteApp);
          } else if (remoteApp.useLocal) {
            app.forced = false;
            const localApp = localApps.find(el => el.id === app.id);
            if (localApp) {
              Object.assign(app, localApp);
            } else {
              warn(
                '_mergeApps',
                'useLocal is allowed but cannot find the app',
                app,
                ' in local app list',
              );
            }
          } else {
            app.forced = false;
            // TODO: Think about this case
          }
        }
      } else {
        mergedApps.push(remoteApp);
      }
    }
  }

  log('_mergeApps', 'Remote app list', remoteApps);
  log('_mergeApps', 'Local app list', localApps);
  log('_mergeApps', 'Merged app list', mergedApps);

  return mergedApps.map(e => {
    return {
      ...e,
      id: String(e.id).trim()
    }
  });
};

export const getAppById = id => {
  return _apps.find(app => {
    console.log('123123123123', `__${app.id}${id}__`);
    return app.id === id
  });
};

export const getAppByBundleName = bundleName => {
  return _apps.find(app => app.bundleName === bundleName);
};

const _checkDomain = () => {
  const msg = util.validateDomain(_domain);
  if (msg && _debug) {
    log('checkDomain', '', msg);
  }
  return !msg;
};

// For remote apps
function fetchApp(id, version, progressCB, zipSize, security) {
  return new Promise(async (resolve, reject) => {
    if (!_checkDomain()) {
      reject(new Error('[XFramework/fetchApp', 'Invalid domain'));
    }

    try {
      const appUrl = util.getAppUrl(_domain, id, version);
      const appFolder = util.getAppFolder(id, version);
      const zipPath = util.getZipPath(id, version);

      log('fetchApp', 'Download app at', appUrl, ' and save to ', zipPath);

      const result = await util.downloadApp(
        appUrl,
        appFolder,
        zipPath,
        progressCB,
        zipSize,
      );

      if (result) {
        const bundlePath = await util.decompress(
          id,
          version,
          zipPath,
          progressCB,
          security,
        );
        resolve(bundlePath);
      } else {
        reject(
          new Error(
            '[XFramework/fetchApp] Error when downloading app ' +
              JSON.stringify(result),
          ),
        );
      }
    } catch (error) {
      logError('fetchApp', '', error);
      reject(error);
    }
  });
}

// For local app
function copyAndDecompress(bundleName, id, version, progressCB, security) {
  return new Promise(async (resolve, reject) => {
    try {
      const filePath = util.getLocalAppPath(bundleName, version);
      const appFolder = util.getAppFolder(id, version);
      const zipPath = util.getZipPath(id, version);

      log(
        'copyAndDecompress',
        'Clone app at',
        filePath,
        ' and save to ',
        zipPath,
      );

      await util.clone(filePath, appFolder, zipPath, progressCB);
      const bundlePath = await util.decompress(
        id,
        version,
        zipPath,
        progressCB,
        security,
      );
      resolve(bundlePath);
    } catch (error) {
      warn('copyAndDecompress', '', error);
      reject(error);
    }
  });
}

function openVersionApp(
  bundleName,
  id,
  version,
  remote,
  initProps,
  progressCB,
  closeCB,
  zipSize,
  checksum,
  security,
  forced,
) {
  if (!initProps) {
    initProps = {
      id,
      version,
      superAppBundleName: _bundleName,
      devLoad: _devLoad,
    };
  } else {
    Object.assign(initProps, {
      id,
      version,
      superAppBundleName: _bundleName,
      devLoad: _devLoad,
    });
  }

  return new Promise(async (resolve, reject) => {
    let exist = await util.isAppDownloaded(id, version);

    const zipPath = util.getZipPath(id, version);
    let bundlePath;
    let zipChecksum;
    try {
      if (exist) {
        if (progressCB) {
          progressCB('begin', {msg: 'app-exist'});
        }
        bundlePath = util.getBundlePath(id, version);
        zipChecksum = await util.getChecksum(zipPath);
        if (checksum === zipChecksum) {
          log(
            'openVersionApp',
            '',
            exist,
            bundleName,
            bundlePath,
            initProps,
            _devLoad,
            closeCB,
          );

          progressCB('appopening', {msg: 'sss'});
          await sleep(Platform.OS === 'ios' ? 700 : 200);

          XFramework.openApp(
            bundleName,
            bundlePath,
            initProps,
            _devLoad,
            closeCB,
          );
        } else {
          Alert.alert(
            'Administrator',
            'Incorrect checksum, the app maybe modified without changing version',
          );
        }
      } else {
        if (remote) {
          if (progressCB) {
            progressCB('begin', {msg: 'remote'});
          }
          bundlePath = await fetchApp(
            id,
            version,
            progressCB,
            zipSize,
            security,
          );
        } else {
          if (progressCB) {
            progressCB('begin', {msg: 'local'});
          }
          bundlePath = await copyAndDecompress(
            bundleName,
            id,
            version,
            progressCB,
            security,
          );
        }
        zipChecksum = await util.getChecksum(zipPath);
        log('openVersionApp', 'checksum from download file', zipChecksum);
        log('openVersionApp', 'checksum from server', checksum);
        progressCB('appopening', {msg: 'sss'});
        await sleep(Platform.OS === 'ios' ? 700 : 200);

        if (checksum === zipChecksum) {
          XFramework.openApp(
            bundleName,
            bundlePath,
            initProps,
            _devLoad,
            closeCB,
          );
        } else {
          Alert.alert(
            'Administrator',
            'Incorrect checksum, the app maybe modified without changing version',
          );
        }
      }
      resolve('Opened app ' + bundleName + ' dev load ' + _devLoad);
    } catch (error) {
      logError('openVersionApp', '', error);
      reject(error);
    }
  });
}

const sleep = time => {
  return new Promise(resolve => setTimeout(resolve, time));
};

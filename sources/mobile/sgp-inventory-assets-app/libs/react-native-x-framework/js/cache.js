// import AsyncStorage from '@react-native-async-storage/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage';

const Key = {
  AppList: 'zzzzz-x-framework-app-list-zzzz',
};

export default class Cache {
  static set AppList(data) {
    EncryptedStorage.setItem(Key.AppList, data ? JSON.stringify(data) : '');
  }

  static get AppList() {
    return new Promise(async (resolve, reject) => {
      const json = await EncryptedStorage.getItem(Key.AppList);
      if (json) {
        resolve(JSON.parse(json));
      } else {
        resolve([]);
      }
    });
  }

  static async clearAll() {
    await EncryptedStorage.removeItem(Key.AppList);
  }
}

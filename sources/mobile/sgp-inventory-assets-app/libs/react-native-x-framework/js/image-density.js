import {Platform, PixelRatio} from 'react-native';
// Internal
import {log, warn} from './logger';
import util from './util';

let _assetPath;
let _imgInfos = {};

async function init({id, version}, imgInfos) {
  _assetPath = util.getAssetsFolder(id, version);
  if (imgInfos) {
    Object.assign(_imgInfos, imgInfos);
  } else {
    warn('imageInit', 'We need a img infomation list');
  }
  const density = pickScale([1, 1.5, 2, 3, 3.5], PixelRatio.get());
  log('imageInit', 'Density', density);
}

function getImage(staticImage, imgPath, forceInteralStorage = false) {
  if (__DEV__ && !forceInteralStorage) {
    return staticImage;
  } else {
    if (!_assetPath) {
      warn('getImage', 'Must initialize resource info firstly!!!');
    }

    const fullPath =
      (Platform.OS === 'android' ? 'file://' : '') +
      _assetPath +
      addDensity(imgPath);
    let size = {};
    const imgInfo = _imgInfos[imgPath];
    if (imgInfo && imgInfo.height && imgInfo.width) {
      Object.assign(size, imgInfo);
    } else {
      warn('getImage', 'Cannot find image info of', imgPath);
    }
    const source = {
      uri: fullPath,
      ...size,
    };
    return source;
  }
}

function addDensity(imgPath) {
  const arr = imgPath.split('.');
  const density = pickScale([1, 1.5, 2, 3, 3.5], PixelRatio.get());
  return arr[0] + '@' + density + 'x.' + arr[1];
}

function pickScale(scales, deviceScale) {
  // Packager guarantees that `scales` array is sorted
  for (var i = 0; i < scales.length; i++) {
    if (scales[i] >= deviceScale) {
      return scales[i];
    }
  }

  // If nothing matches, device scale is larger than any available
  // scales, so we return the biggest one. Unless the array is empty,
  // in which case we default to 1
  return scales[scales.length - 1] || 1;
}

export default {
  init,
  getImage,
};

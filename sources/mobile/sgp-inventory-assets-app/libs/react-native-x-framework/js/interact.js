import {NativeEventEmitter, NativeModules, Alert} from 'react-native';
// Internal
import {log} from './logger';
import util from './util';
import {getEvents} from './z-event-manager';
import {getHandler} from './z-request-manager';
const {XFramework} = NativeModules;

let _bundleName;
// let _debug;
let _eventMessageSub;
let _eventRequestSub;

export async function init(config) {
  _bundleName = config.bundleName;
  // _debug = config.debug;
  const eventEmitter = new NativeEventEmitter(XFramework);

  eventEmitter.removeAllListeners('EventMessage');
  eventEmitter.removeAllListeners('EventRequest');

  if (_eventMessageSub) {
    _eventMessageSub.remove();
  }

  _eventMessageSub = eventEmitter.addListener('EventMessage', msg => {
    log('onMessage', '', msg);

    const events = getEvents(msg.eventName);
    if (events && events.length) {
      events.forEach(event => {
        event.listener(msg.data);
      });
    } else {
      log('onMessage', 'Cannot find event', msg.eventName);
    }
  });

  if (_eventRequestSub) {
    _eventRequestSub.remove();
  }

  _eventRequestSub = eventEmitter.addListener('EventRequest', req => {
    log('onRequest', '', req);

    const handler = getHandler(req.type);
    if (handler) {
      handler(
        req,
        res => replyResponse(req.id, res),
        err => replyError(req.id, err),
      );
    } else {
      replyError(req.id, {msg: 'Cannot find the handler for this request'});
    }
  });

  if (XFramework) {
    XFramework.addBridge(_bundleName);
    const bundleNames = await XFramework.getBundleNames();
    log('onRequest', 'All bundleNames', bundleNames);
  } else {
    Alert.alert(
      'Warning',
      '[onRequest] Mini app must be executed on Super app',
    );
  }
}

export function sendMessage(to, msg, callback) {
  log('sendMessage', to, msg);
  if (XFramework) {
    XFramework.sendMessage(to, msg, callback);
  } else {
    Alert.alert(
      'Warning',
      '[sendMessage] Mini app must be executed on Super app',
    );
  }
}

export function sendRequest(to, req) {
  log('sendRequest', to, req);
  if (XFramework) {
    return XFramework.sendRequest(to, req);
  } else {
    Alert.alert(
      'Warning',
      '[sendRequest] Mini app must be executed on Super app',
    );
    return util.sleep(1000);
  }
}

function replyResponse(reqId, res) {
  log('replyResponse', reqId, res);
  if (XFramework) {
    XFramework.replyResponse(reqId, res, msg => {
      log('replyResponse', 'Result', msg);
    });
  } else {
    Alert.alert(
      'Warning',
      '[replyResponse] Mini app must be executed on Super app',
    );
  }
}

function replyError(reqId, err) {
  log('replyError', reqId, err);

  if (XFramework) {
    XFramework.replyError(reqId, err, msg => {
      log('replyError', 'Result', msg);
    });
  } else {
    Alert.alert(
      'Warning',
      '[replyError] Mini app must be executed on Super app',
    );
  }
}

let _debug;
let _commonInfo;
// Remote logger
let _remoteLog;
let _remoteLogWarn;
let _remoteLogError;

export function init(config) {
  _debug = config.debug;
}

function setCommonLogInfo(commonInfo) {
  _commonInfo = commonInfo;
}

function setRemoteLoggers(remoteLog, remoteLogWarn, remoteLogError) {
  _remoteLog = remoteLog;
  _remoteLogWarn = remoteLogWarn;
  _remoteLogError = remoteLogError;
}

export const log = (where, message, ...params) => {
  if (typeof message !== 'string') {
    message = JSON.stringify(message);
  }

  const logMessage = `[XFramework${where ? `/${where}` : ''}] ${message}`;
  if (_debug) {
    console.log(logMessage, ...params);
  }

  if (_remoteLog) {
    _remoteLog(_commonInfo, logMessage);
  }
};

export const warn = (where, message, ...params) => {
  if (typeof message !== 'string') {
    message = JSON.stringify(message);
  }

  const warnMessage = `[XFramework${where ? `/${where}` : ''}] ${message}`;
  if (_debug) {
    console.warn(warnMessage, ...params);
  }

  if (_remoteLogWarn) {
    _remoteLogWarn(_commonInfo, warnMessage);
  }
};

export const logError = (where, message, ...params) => {
  if (typeof message !== 'string') {
    message = JSON.stringify(message);
  }

  const errorMessage = `[XFramework${where ? `/${where}` : ''}] ${message}`;
  if (_debug) {
    console.error(errorMessage, ...params);
  }

  if (_remoteLogError) {
    _remoteLogError(_commonInfo, errorMessage);
  }
};

export default {
  setCommonLogInfo,
  setRemoteLoggers,
};

import {NativeModules} from 'react-native';
// Internal
import {log} from './logger';
const {XFramework} = NativeModules;

// let _debug;
let _bundleName;

export function init(config) {
  // _debug = config.debug;
  _bundleName = config.bundleName;
}

async function closeApp(result) {
  XFramework.closeApp(_bundleName, result);
  const bundleNames = await XFramework.getBundleNames();
  log('closeApp', 'app closed with', result, ' all bundleNames ', bundleNames);
}

export default {closeApp};

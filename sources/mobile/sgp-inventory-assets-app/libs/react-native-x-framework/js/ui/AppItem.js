import * as React from 'react';
import {StyleSheet, TouchableOpacity, Image, Text, Alert} from 'react-native';
// Internal
import {log, warn, logError} from '../logger';
import appManager from '../app-manager';

 const AppItem = ({app, style, ...more}) =>{
   const [percent, setPercent] = React.useState(1);

  const openApp = async () => {
    try {
      const result = await appManager.openApp(
        app.id,
        {
          accessToken: 'abc-xyz',
          user: { firstName: 'Phong', lastName: 'Le' },
          theme: { Color: { Main: '#ff2233' } },
        },
        // state: begin, begin<Downloading/Cloning>, downloading/cloning, end<Downloading/Cloning>, decompressing, end
        (state, progressData) => {
          switch (state) {
            case 'downloading':
              const { jobId, contentLength, bytesWritten } = progressData;
              log('AppItem', 'Downloading', jobId, contentLength, bytesWritten);
              setPercent(bytesWritten / contentLength)
              break;
            case 'decompressing':
              log('AppItem', 'Decompressing', progressData);
              break;
            default:
              log('AppItem', state, progressData);
          }
        },
        data => {
          setTimeout(() => {
            Alert.alert('Mini app close', JSON.stringify(data));
            log('AppItem', 'Mini app close', data);
          }, 100);
        },
      );
      log('AppItem', 'openApp', result);
    } catch (error) {
      logError('AppItem', 'openApp', error);
    }
  };

   const cleanApp = React.useCallback(async () => {
     try {
       appManager.cleanApp(app.id);
     } catch (error) {
       warn('AppItem', 'cleanApp', error);
     }
   }, [app]);

   const pce = percent >= 1
     ? `${app.name}\n${app.version}`
     : `${Math.floor(percent * 100)}%`
  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={openApp}
      onLongPress={cleanApp}
      {...more}>
      {/* <Image
        style={styles.icon}
        source={{
          uri: appManager.getIconUrl(app.id),
        }}
      /> */}
      <Text style={styles.text}>
        {pce}
      </Text>
    </TouchableOpacity>
  );
}

export default AppItem;

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  icon: {
    height: 80,
    width: 80,
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 12,
    margin: 10,
    backgroundColor: '#0002',
  },
  text: {color: 'black', textAlign: 'center'},
});

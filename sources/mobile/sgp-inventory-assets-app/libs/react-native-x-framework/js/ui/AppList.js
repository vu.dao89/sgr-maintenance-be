import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import AppItem from './AppItem';

export default function AppList(props) {
  const {apps, style, appStyle, ...more} = props;
  return (
    <ScrollView
      style={[styles.container, style]}
      horizontal={true}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      {...more}>
      {apps.map(app => (
        <AppItem key={app.id} style={appStyle} app={app} />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor:"red"
  },
});

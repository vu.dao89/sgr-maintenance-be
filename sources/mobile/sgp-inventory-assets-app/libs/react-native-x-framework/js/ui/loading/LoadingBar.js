import React from 'react';
import {Modal, View, Text} from 'react-native';
import loadingStyles from './styles';

const LoadingBar = props => {
  const {isLoading, percent, appLoadingState} = props;
  return (
    <Modal
      visible={isLoading}
      // animationType="fade"
      transparent
      statusBarTranslucent>
      <View style={loadingStyles.ctnModal}>
        <View
          style={[
            loadingStyles.ctnProgress,
            {
              width: `80%`,
            },
          ]}>
          <View
            style={[
              loadingStyles.ctnFillProgress,
              {
                width: `${Math.round(Math.min((percent || 0) * 100, 100))}%`,
              },
            ]}
          />
        </View>
        <Text style={{color: 'white', marginTop: 10}}>{`${appLoadingState} ${
          percent * 100 < 100 ? Math.round(Math.min(percent * 100, 100)) : ''
        }%`}</Text>
      </View>
    </Modal>
  );
};
export default LoadingBar;

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  activityIndicatorWrapper: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    display: 'flex',
    height: 80,
    justifyContent: 'space-around',
    width: 80,
  },
  modalBackground: {
    alignItems: 'center',
    backgroundColor: '#00000040',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },

  // loading bar
  ctnModal: {
    backgroundColor: '#000000',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ctnProgress: {
    height: 4,
    backgroundColor: '#CCCCCC',
  },
  ctnFillProgress: {
    height: 4,
    backgroundColor: '#F7BC13', //'#00ff00',
  },
});
export default styles;

import * as React from 'react';
import {
    Text,
    Dimensions,
    SafeAreaView,
    TouchableOpacity,
    StyleSheet,
    View,
    Platform,
    Alert,
    Switch,
    BackHandler,
    Button,
} from 'react-native';
import { Camera, useCameraDevices, useFrameProcessor } from 'react-native-vision-camera';
import * as REA from 'react-native-reanimated';

import { Svg, Rect } from 'react-native-svg';
import { BarcodeFormat, scanBarcodes } from 'vision-camera-code-scanner';

export default function App() {
    const mounted = REA.useSharedValue(true);
    const rotated = REA.useSharedValue(false);
    const regionEnabledShared = REA.useSharedValue(false);
    const [hasPermission, setHasPermission] = React.useState(false);

    const [buttonText, setButtonText] = React.useState('Pause');
    const [isActive, setIsActive] = React.useState(true);
    const [frameWidth, setFrameWidth] = React.useState(1280);
    const [frameHeight, setFrameHeight] = React.useState(720);
    const [regionEnabled, setRegionEnabled] = React.useState(false);
    const [torchEnabled, setTorchEnabled] = React.useState(false);
    const [useFront, setUseFront] = React.useState(false);
    const useFrontShared = REA.useSharedValue(false);

    const devices = useCameraDevices();
    const backCam = devices.back;

    let scanned = false;

    React.useEffect(() => {
        mounted.value = true; // to avoid the error: Can’t perform a React state update on an unmounted component.
        return () => {
            mounted.value = false;
        };
    });

    React.useEffect(() => {
        (async () => {
            const status = await Camera.requestCameraPermission();
            setHasPermission(status === 'authorized');
        })();

        const backAction = () => {
            setIsActive(false);
            navigation.goBack();
            return true;
        };

        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );

        return () => backHandler.remove();
    }, []);

    const toggleCameraStatus = () => {
        if (buttonText == 'Pause') {
            setButtonText('Resume');
            setIsActive(false);
        } else {
            setButtonText('Pause');
            setIsActive(true);
        }
    };

    const getViewBox = () => {
        const frameSize = getFrameSize();
        const viewBox = '0 0 ' + frameSize[0] + ' ' + frameSize[1];
        console.log('viewBox' + viewBox);
        updateRotated();
        return viewBox;
    };

    const getFrameSize = () => {
        let width, height;
        if (HasRotation()) {
            width = frameHeight;
            height = frameWidth;
        } else {
            width = frameWidth;
            height = frameHeight;
        }
        return [width, height];
    };

    const HasRotation = () => {
        let value = false;
        if (Platform.OS === 'android') {
            if (
                !(
                    frameWidth > frameHeight &&
                    Dimensions.get('window').width > Dimensions.get('window').height
                )
            ) {
                value = true;
            }
        }
        return value;
    };

    const updateRotated = () => {
        rotated.value = HasRotation();
    };

    const updateFrameSize = (width) => {
        setFrameWidth(width);
        setFrameHeight(width);
    };

    const onBarcodeScanned = results => {
        if (!scanned && Array.isArray(results) && results.length > 0) {
            Alert.alert('', JSON.stringify(results, null, 2));
            scanned = true;
        }
    };

    const sharedBarcodes = REA.useSharedValue([]);

    REA.useDerivedValue(() => {
        REA.runOnJS(onBarcodeScanned)(sharedBarcodes.value.detectedBarcodes);

        REA.runOnJS(updateFrameSize)(
            sharedBarcodes.value?.frame?.width || 100,
            sharedBarcodes.value?.frame?.height || 100,
        );
    });

    const frameProcessor = useFrameProcessor(frame => {
        'worklet';
        const detectedBarcodes = scanBarcodes(frame, [BarcodeFormat.ALL_FORMATS]);
        sharedBarcodes.value = {
            detectedBarcodes,
            frame,
        };
    }, []);

    const format = React.useMemo(() => {
        const desiredWidth = 1280;
        const desiredHeight = 720;
        let selectedCam;
        if (useFront) {
            selectedCam = frontCam;
        } else {
            selectedCam = backCam;
        }
        if (selectedCam) {
            for (let index = 0; index < selectedCam.formats.length; index++) {
                const format = selectedCam.formats[index];
                console.log('h: ' + format.videoHeight);
                console.log('w: ' + format.videoWidth);
                if (
                    format.videoWidth == desiredWidth &&
                    format.videoHeight == desiredHeight
                ) {
                    console.log('select format: ' + format);
                    return format;
                }
            }
        }
        return undefined;
    }, [useFront]);

    const [isShowCam, setshowCam] = React.useState()
    return (
        <SafeAreaView style={styles.container}>
            <Button title='okkkk' onPress={() => {
                setshowCam(true)
            }} />
            {isShowCam && backCam != null && hasPermission && (
                <>
                    <Camera
                        style={StyleSheet.absoluteFill}
                        device={useFront ? frontCam : backCam}
                        isActive={isActive}
                        format={format}
                        frameProcessor={frameProcessor}
                        frameProcessorFps={2}
                    />
                </>
            )}
            <Svg style={StyleSheet.absoluteFill} viewBox={getViewBox()}>
                {regionEnabled && (
                    <Rect
                        x={0.1 * getFrameSize()[0]}
                        y={0.2 * getFrameSize()[1]}
                        width={0.8 * getFrameSize()[0]}
                        height={0.45 * getFrameSize()[1]}
                        strokeWidth="2"
                        stroke="red"
                    />
                )}
            </Svg>
            <View style={styles.control}>
                <View style={{ flex: 0.8 }}>
                    <View style={styles.switchContainer}>
                        <Text style={{ alignSelf: 'center', color: 'black' }}>
                            Scan Region
                        </Text>
                        <Switch
                            style={{ alignSelf: 'center' }}
                            trackColor={{ false: '#767577', true: 'black' }}
                            thumbColor={regionEnabled ? 'white' : '#f4f3f4'}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={newValue => {
                                regionEnabledShared.value = newValue;
                                setRegionEnabled(newValue);
                            }}
                            value={regionEnabled}
                        />
                    </View>
                    <View style={styles.switchContainer}>
                        <Text style={{ alignSelf: 'center', color: 'black' }}>Front</Text>
                        <Switch
                            style={{ alignSelf: 'center' }}
                            trackColor={{ false: '#767577', true: 'black' }}
                            thumbColor={useFront ? 'white' : '#f4f3f4'}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={newValue => {
                                useFrontShared.value = newValue;
                                setUseFront(newValue);
                            }}
                            value={useFront}
                        />
                        <Text style={{ alignSelf: 'center', color: 'black' }}>Torch</Text>
                        <Switch
                            style={{ alignSelf: 'center' }}
                            trackColor={{ false: '#767577', true: 'black' }}
                            thumbColor={torchEnabled ? 'white' : '#f4f3f4'}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={newValue => {
                                setTorchEnabled(newValue);
                            }}
                            value={torchEnabled}
                        />
                    </View>
                </View>
                <TouchableOpacity
                    onPress={toggleCameraStatus}
                    style={styles.toggleCameraStatusButton}>
                    <Text style={{ fontSize: 15, color: 'black', alignSelf: 'center' }}>
                        {buttonText}
                    </Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    barcodeText: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    },
    toggleCameraStatusButton: {
        flex: 0.2,
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 2,
        borderRadius: 5,
        padding: 8,
        margin: 5,
    },
    control: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        height: '15%',
        width: '100%',
        alignSelf: 'flex-start',
        borderColor: 'white',
        borderWidth: 0.1,
        backgroundColor: 'rgba(255,255,255,0.2)',
        alignItems: 'center',
    },
    switchContainer: {
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
});

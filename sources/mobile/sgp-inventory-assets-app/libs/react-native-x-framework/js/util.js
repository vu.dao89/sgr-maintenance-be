import {Platform, PixelRatio} from 'react-native';
// import EncryptedStorage from '@react-native-community/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage';
import {Buffer} from 'buffer';
import axios from 'axios';
import RNFS from 'react-native-fs';
import {subscribe, unzip} from 'react-native-zip-archive';
// Internal
import {log} from './logger';
const _appsFolder = `${RNFS.DocumentDirectoryPath}/apps`;

// let _debug;
let _zipProgressCB;
let _zipUnsubscribe;

const initZipSubcribe = () => {
  if (_zipUnsubscribe) {
    _zipUnsubscribe.remove();
    _zipUnsubscribe = null;
  }

  _zipUnsubscribe = subscribe(progressData => {
    if (_zipProgressCB) {
      _zipProgressCB('decompressing', {
        ...progressData,
        msg: 'Đang giải nén...',
      });
    }
  });
};

const validateDomain = domain => {
  if (!domain) {
    return 'Please setup a domain!';
  }

  const re = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i',
  );

  if (!domain.match(re)) {
    return 'Invalid domain';
  }

  return '';
};

// APIs
const getAppList = async appsUrl => {
  const accessToken = await EncryptedStorage.getItem('ACCESS_TOKEN_KEY');
  const trimQouteAccessToken = accessToken.replace(/['"]+/g, '');

  return await axios.get(appsUrl, {
    headers: {
      'Cache-Control': 'no-store',
      Authorization: `Bearer ${trimQouteAccessToken}`,
    },
  });
};

const downloadApp = (
  appUrl,
  appFolder,
  zipPath,
  progressCB,
  contentLength = 10000000,
) => {
  return new Promise(async (resolve, reject) => {
    const accessToken = await EncryptedStorage.getItem('ACCESS_TOKEN_KEY');
    const trimQouteAccessToken = accessToken.replace(/['"]+/g, '');

    try {
      await RNFS.mkdir(appFolder);
      log('util-download', 'Created folder', appFolder);
      const exist = await RNFS.exists(zipPath);
      if (exist) {
        await RNFS.unlink(zipPath);
      }

      const data = await axios
        .request({
          method: 'GET',
          url: appUrl,
          headers: {
            Accept: '*/*',
            Authorization: `Bearer ${trimQouteAccessToken}`,
          },
          responseType: 'arraybuffer',
          onDownloadProgress: progressEvent => {
            if (progressCB) {
              if (progressEvent.contentLength === -1) {
                progressEvent.contentLength = contentLength;
              }
              progressCB('downloading', {
                msg: 'Đang tải về...',
                contentLength: progressEvent.total,
                bytesWritten: progressEvent.loaded,
              });
            }
          },
        })
        .then(response =>
          Buffer.from(response.data, 'binary').toString('base64'),
        );

      await RNFS.writeFile(zipPath, data, 'base64');
      if (progressCB) {
        progressCB('downloading', {msg: 'Đã tải xong'});
      }

      resolve(data);
    } catch (error) {
      reject(error);
    }
  });
};
//======================

// Unzip
const decompress = (id, version, zipPath, progressCB, security) => {
  _zipProgressCB = progressCB;
  return new Promise(async (resolve, reject) => {
    try {
      const versionFoler = getVersionFolder(id, version);
      log(
        'util-decompress',
        'Unzipping from file',
        zipPath,
        ' to ',
        versionFoler,
      );
      await unzip(zipPath, versionFoler);

      _zipProgressCB = null;

      // Verify after unzipping
      const bundlePath = getBundlePath(id, version);
      await RNFS.stat(bundlePath);
      resolve(bundlePath);
    } catch (error) {
      _zipProgressCB = null;
      reject(error);
    }
  });
};
//======================

// Storage
const clone = (filePath, appFolder, zipPath) => {
  return new Promise(async (resolve, reject) => {
    try {
      await RNFS.mkdir(appFolder);
      log('util-clone', 'Created folder', appFolder);

      const exist = await RNFS.exists(zipPath);
      if (exist) {
        await RNFS.unlink(zipPath);
      }

      const result =
        Platform.OS === 'android'
          ? await RNFS.copyFileAssets(filePath, zipPath)
          : await RNFS.copyFile(filePath, zipPath);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
};

const deleteFolder = folderPath => {
  return RNFS.unlink(folderPath);
};

const isAppDownloaded = (id, version) => {
  return RNFS.exists(getBundlePath(id, version));
};
//======================

// Urls and storage paths
const getAppsUrl = domain => `${domain}/${Platform.OS}/app-list`;

const getAppUrl = (domain, id, version) =>
  `${domain}/${Platform.OS}/${id}/${version}`;

const getIconUrl = (domain, id) => {
  return `${domain}/${Platform.OS}/${id}/icon/app@${PixelRatio.get()}x.png`;
};

const getAppsFolder = () => _appsFolder;

const getAppFolder = id => `${_appsFolder}/${id}`;

const getVersionFolder = (id, version) => `${getAppFolder(id)}/${version}`;

const getAssetsFolder = (id, version) =>
  `${getVersionFolder(id, version)}/assets`;

const getLocalAppPath = (bundleName, version) => {
  return Platform.OS === 'ios'
    ? `${RNFS.MainBundlePath}/${bundleName}_${version}.zip`
    : `${bundleName}_${version}.zip`;
};

const getZipPath = (id, version) => `${_appsFolder}/${id}/${version}.zip`;

const getBundlePath = (id, version) =>
  `${_appsFolder}/${id}/${version}/app.bundle`;

// Support methods
const iOS = () => {
  return Platform.OS === 'ios';
};

const android = () => {
  return Platform.OS === 'android';
};

const sleep = time => {
  return new Promise(resolve =>
    setTimeout(() => resolve('Sleep in  ' + time), time),
  );
};

// Checksum
const getChecksum = zipPath => {
  return new Promise(async (resolve, reject) => {
    const checksum = await RNFS.hash(zipPath, 'sha1');
    resolve(checksum);
  });
};
//======================

export default {
  initZipSubcribe,
  validateDomain,
  // APIs
  getAppList,
  downloadApp,
  // Storage
  clone,
  deleteFolder,
  isAppDownloaded,
  // Urls and storage paths
  getAppsUrl,
  getAppUrl,
  getIconUrl,
  getAppsFolder,
  getAppFolder,
  getVersionFolder,
  getAssetsFolder,
  getLocalAppPath,
  getZipPath,
  getBundlePath,
  // Unzip
  decompress,
  // Support methods
  iOS,
  android,
  sleep,
  // Checksum
  getChecksum,
};

const _events = {};

export function set(token, eventName, listener) {
  _events[token] = {eventName, listener};
}

export function unset(token) {
  if (_events.hasOwnProperty(token)) {
    delete _events[token];
  }
}

export function getEvents(eventName) {
  return Object.values(_events).filter(event => event.eventName === eventName);
}

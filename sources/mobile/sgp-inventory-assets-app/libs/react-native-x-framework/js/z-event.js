import {NativeModules} from 'react-native';
import uuid from 'uuid';
// Internal
import {log} from './logger';
import {sendMessage} from './interact';
import {set, unset} from './z-event-manager';

const {XFramework} = NativeModules;

// let _debug;

export function init(config) {
  // _debug = config.debug;
}

//---------------Common--------------------
function subscribe(eventName, listener) {
  const token = uuid.v4();

  log('event', 'Subscribe to', eventName, token);
  set(token, eventName, listener);
  return token;
}

function unsubscribe(token) {
  log('event', 'Unsubscribe to', token);
  unset(token);
}

async function publish(eventName, data) {
  const bundleNames = await XFramework.getBundleNames();
  bundleNames.forEach(name => {
    sendMessage(name, {eventName, data}, result => {
      log('publish', 'Send to', name, data, result);
    });
  });
}

export default {subscribe, unsubscribe, publish};

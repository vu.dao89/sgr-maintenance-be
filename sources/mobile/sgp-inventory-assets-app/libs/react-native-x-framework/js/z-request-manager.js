// TODO: will be removed
// import {log, warn} from './logger';
// import {getAppByBundleName} from './app-manager';
// Define default permission
// const _permissions = {whitelist: {}, blacklist: {}};
//-------------------------------------------------
const _requestHandlers = {};

export function set(requestType, handler) {
  _requestHandlers[requestType] = handler;
}

export function getHandler(requestType) {
  return _requestHandlers[requestType];
}

// TODO: will be removed
// export function setPermissions(permissions = {}) {
//   const {whitelist, blacklist} = _permissions;
//   if (permissions.whitelist) {
//     Object.assign(whitelist, permissions.whitelist);
//   }
//   if (permissions.blacklist) {
//     Object.assign(blacklist, permissions.blacklist);
//   }

//   log('setPermissions', JSON.stringify(_permissions));
// }

// // whitelist is higher priority than blacklist then whitelist is checked firstly
// // whitelist: Allows requests if their type in whitelist, blocks the rest
// // if you don't define whitelist, all of request are allowed
// // blacklist: After passing whitelist, blocks requests if their type is in blacklist
// export function validateRequest(request = {}) {
//   log('validateRequest', 'validate', request.from, request.type);
//   const {from, type} = request;

//   if (!type || !from) {
//     const msg = 'Cannot find type or from in request';
//     warn('validateRequest', msg);
//     return msg;
//   }

//   const app = getAppByBundleName(from);

//   if (app) {
//     const {id: appId} = app;
//     const whitelistPermission = _permissions.whitelist[appId];

//     if (whitelistPermission) {
//       if (
//         !whitelistPermission.includes(type) &&
//         !whitelistPermission.includes('*')
//       ) {
//         const msg = `The request ${type} from ${from} isn't allowed by whitelist in Super app`;
//         warn('validateRequest', msg);
//         return msg;
//       }
//     }

//     const blacklistPermission = _permissions.blacklist[appId];
//     if (blacklistPermission && blacklistPermission.includes(type)) {
//       const msg = `The request ${type} from ${from} is blocked by blacklist in Super app`;
//       warn('validateRequest', msg);
//       return msg;
//     }

//     return '';
//   } else {
//     const msg = 'Cannot find app with bundle name ' + from;
//     warn('validateRequest', msg);
//     return msg;
//   }
// }
//-------------------------------------------------

import {log} from './logger';
import {sendRequest} from './interact';
import {set, setPermissions} from './z-request-manager';
// let _debug;

//----------------Mini app-----------------
let _bundleName;
let _superAppBundleName;

export function initMiniApp(config, initProps) {
  // _debug = config.debug;
  _bundleName = config.bundleName;
  _superAppBundleName = initProps.superAppBundleName;
}

function request(type, data) {
  log('request', 'Request to', _superAppBundleName, data);
  return sendRequest(_superAppBundleName, {
    type: type,
    data,
    from: _bundleName,
  });
}

//----------------Super app-----------------
export function init(config) {
  // _debug = config.debug;
}

function register(requestType, handler) {
  log('request', 'Register', requestType);
  set(requestType, handler);
}

export default {register, request, setPermissions};

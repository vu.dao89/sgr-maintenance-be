// external
import React, { forwardRef, useImperativeHandle, useState } from "react";
import { TouchableOpacity, View } from "react-native";
import Modal from "react-native-modal";

// icon
import { ICClose } from "@Assets/image";

// internal
import TextCM from "../TextCM";
import { heightResponsive } from "@Constants";
import styles from "./styles";

interface IProp {
  header?: string;
  onClosePress?: any;
  children?: any;
  handleAfterSelect?: () => void;
}
const BaseModal = forwardRef(({ header, onClosePress, children, handleAfterSelect }: IProp, ref) => {
  const [isVisible, setVisible] = useState(false);

  const onOpenModal = () => setVisible(true);
  const onCloseModal = () => {
    if (onClosePress) onClosePress();
    setVisible(false);
    if (handleAfterSelect) handleAfterSelect();
  };

  useImperativeHandle(ref, () => ({
    onOpenModal,
    onCloseModal,
  }));

  return (
    <Modal
      statusBarTranslucent={true}
      useNativeDriver={true}
      isVisible={isVisible}
      hideModalContentWhileAnimating={true}
      backdropOpacity={0.75}
      avoidKeyboard={false}
      onBackdropPress={onCloseModal}
      style={styles.ctnModal}>
      <View style={styles.container}>
        <View style={styles.viewHeader}>
          <View />
          <TextCM style={styles.txtContent}>{header}</TextCM>
          <TouchableOpacity onPress={onCloseModal} hitSlop={{ top: 5, right: 5, left: 5, bottom: 5 }}>
            <ICClose width={heightResponsive(24)} height={heightResponsive(24)} />
          </TouchableOpacity>
        </View>

        <View style={styles.viewBody}>{children}</View>
      </View>
    </Modal>
  );
});
BaseModal.displayName = "BaseModal";
export default BaseModal;

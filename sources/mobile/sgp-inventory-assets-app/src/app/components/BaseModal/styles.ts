import { Color, FontSize, heightResponsive, widthResponsive } from "@Constants";
import { StyleSheet, Dimensions } from "react-native";
const windowWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  container: {
    //  flex: 1,
    borderTopLeftRadius: heightResponsive(20),
    borderTopRightRadius: heightResponsive(20),
    marginLeft: 0,
    marginBottom: 0,
    bottom: 0,
    width: windowWidth,
    position: "absolute",
    // paddingHorizontal: (16),
  },
  ctnModal: {
    // flex: 1,
    width: windowWidth,
    // backgroundColor: 'blue',
    margin: 0,
  },
  viewHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: widthResponsive(16),
    backgroundColor: Color.BackgroundTextInput,
    height: heightResponsive(54),
    alignItems: "center",
    borderTopLeftRadius: heightResponsive(20),
    borderTopRightRadius: heightResponsive(20),
  },
  viewBody: {
    backgroundColor: Color.CardBackground,
  },
  txtContent: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
    fontWeight: "500",
  },
});

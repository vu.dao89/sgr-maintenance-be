// External
import React, { FC, forwardRef, ReactElement, useState } from "react";
import { View, ViewStyle, StyleProp, TouchableOpacity, TouchableOpacityProps } from "react-native";
import { isEmpty } from "lodash";
import DateTimePickerModal from "react-native-modal-datetime-picker";

// Internal
import { ICCalendar, IcWarningRed } from "@Assets/image";
import { heightResponsive as H, widthResponsive as W } from "@Constants";
import i18n from "@I18n";
import TextCM from "../TextCM";
import styles from "./styles";
import { DateTimeUtil } from "@Utils";

type IMode = "date" | "time" | "datetime" | undefined;

export type IProp = {
  /**
   *  Title of date picker
   */
  title?: string;
  /**
   *  Show the letter * beside title
   */
  isRequired?: boolean;
  /**
   *  Style of title container
   */
  styleCtnTitle?: StyleProp<ViewStyle>;
  /**
   *  Style of title date picker
   */
  styleCtnTextInput?: StyleProp<ViewStyle>;
  /**
   *  Set disable the date picker
   */
  disable?: boolean;
  /**
   *  If have message error. It will show the message
   */
  messageError?: string;
  /**
   *  Mode display of date picker
   */
  mode?: IMode;
  /**
   *  Get value from date picker
   */
  onSelectDate?: (data: any) => void;
  /**
   *  Value of date picker
   */
  value?: Date | string | number;
  /**
   *  Place holder of text field
   */
  placeholder?: string;
};

const DatePickerCustom: FC<TouchableOpacityProps & IProp> = forwardRef(
  (
    {
      style,
      title,
      styleCtnTextInput,
      isRequired,
      styleCtnTitle,
      value,
      disable = false,
      messageError = "",
      mode = "date",
      onSelectDate,
      placeholder,
      ...more
    }: TouchableOpacityProps & IProp,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isFill = !isEmpty(String(value));
    const isError = !isEmpty(messageError);

    const [isVisible, setVisible] = useState<boolean>(false);
    const toggleVisible = () => setVisible(!isVisible);

    const onConfirm = (date: Date): void => {
      if (onSelectDate) {
        onSelectDate(date);
      }
      toggleVisible();
    };

    return (
      <View style={style}>
        {isUseTitle && (
          <View style={[{ flexDirection: "row" }, styleCtnTitle]}>
            <TextCM style={[styles.txtDefaultTitle, styles.txtTitle, disable && styles.txtDisable]}>{title}</TextCM>
            {isRequired && (
              <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired, disable && styles.txtDisable]}>*</TextCM>
            )}
          </View>
        )}

        <View>
          <TouchableOpacity style={styles.styleDefault} disabled={disable} {...more} onPress={toggleVisible}>
            <TextCM
              style={[
                styles.styleTextDefault,
                styleCtnTextInput,
                disable && styles.txtDisable,
                !isFill && styles.txtDisable,
              ]}>
              {(value && DateTimeUtil.formatDate(value, "DD/MM/YYYY")) ||
                placeholder ||
                i18n.t("CM.TextField.txtInputData").toString()}
            </TextCM>
          </TouchableOpacity>

          <TouchableOpacity
            disabled={disable}
            hitSlop={{ right: W(12), left: W(12), top: H(12), bottom: H(12) }}
            onPress={toggleVisible}
            style={styles.ctnRightComponent}>
            <ICCalendar width={H(20)} height={H(20)} />
          </TouchableOpacity>
        </View>

        {isError && (
          <View style={styles.ctnError}>
            <IcWarningRed />
            <TextCM style={styles.txtError}>{messageError}</TextCM>
          </View>
        )}

        <DateTimePickerModal isVisible={isVisible} mode={mode} onConfirm={onConfirm} onCancel={toggleVisible} />
      </View>
    );
  }
);

DatePickerCustom.displayName = "DatePicker";

export default DatePickerCustom;

import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TouchableOpacityProps, View } from "react-native";
import DatePickerCustom, { IProp } from "./DatePickerCustom";

type IDatePicker = {
  name: string;
  useHookForm?: boolean;
  onChangeText?: (value: string) => void;
};

const DatePicker: FC<TouchableOpacityProps & IProp & IDatePicker> = ({
  name,
  useHookForm = true,
  onChangeText,
  ...more
}) => {
  const {
    control,
    setValue,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return String(field?.message) || "";
    }
    return "";
  };

  const _onChangeText = (value: string, onChangeForm: (...event: any[]) => void): void => {
    if (onChangeText) onChangeText(value);
    onChangeForm(value);
    setValue(name, String(value));
  };
  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { onChange, value, ...moreField } }) => {
            return (
              <DatePickerCustom
                {...moreField}
                {...more}
                value={value}
                onSelectDate={(e: string) => _onChangeText(e, onChange)}
                messageError={getMsgError()}
              />
            );
          }}
        />
      ) : (
        <DatePickerCustom {...more} />
      )}
    </View>
  );
};

export default DatePicker;

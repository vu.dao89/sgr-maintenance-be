import { Platform, StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  styleDefault: {
    backgroundColor: Color.BodyTable,
    borderRadius: H(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  styleTextDefault: {
    paddingLeft: W(12),
    paddingRight: W(36),
    paddingVertical: Platform.select({ ios: H(10), android: H(12) }),
    color: Color.White,
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
    lineHeight: H(24),
  },
  txtDefaultTitle: {
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  txtTitle: {
    color: Color.LightGray,
    marginBottom: H(8),
  },
  txtTitleRequired: {
    marginLeft: W(2),
    color: Color.RedText,
  },
  ctnRightComponent: {
    position: "absolute",
    zIndex: 1,
    right: W(12),
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  ctnError: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: H(8),
  },
  txtError: {
    marginLeft: W(4),
    color: Color.RedText,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  //disable
  txtDisable: {
    color: Color.TxtDisable,
  },
  textInput: {
    marginLeft: W(8),
    color: Color.White,
    fontSize: FontSize.FontMedium,
    width: W(280),
  },
});

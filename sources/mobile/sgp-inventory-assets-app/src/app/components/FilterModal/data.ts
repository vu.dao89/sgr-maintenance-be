import i18n from "@I18n";
import { month, quarter } from "@Constants";
import moment from "moment";

export type FilterOptionType = {
  id: string;
  value: string;
  label: string;
};

export type DateObjectType = {
  from_date: string;
  to_date: string;
};

export const MonthData: FilterOptionType[] = [
  {
    id: `M${month.jan}`,
    value: month.jan,
    label: i18n.t("HS.Month", { month: 1 }),
  },
  {
    id: `M${month.feb}`,
    value: month.feb,
    label: i18n.t("HS.Month", { month: 2 }),
  },
  {
    id: `M${month.mar}`,
    value: month.mar,
    label: i18n.t("HS.Month", { month: 3 }),
  },
  {
    id: `M${month.apr}`,
    value: month.apr,
    label: i18n.t("HS.Month", { month: 4 }),
  },
  {
    id: `M${month.may}`,
    value: month.may,
    label: i18n.t("HS.Month", { month: 5 }),
  },
  {
    id: `M${month.jun}`,
    value: month.jun,
    label: i18n.t("HS.Month", { month: 6 }),
  },
  {
    id: `M${month.jul}`,
    value: month.jul,
    label: i18n.t("HS.Month", { month: 7 }),
  },
  {
    id: `M${month.aug}`,
    value: month.aug,
    label: i18n.t("HS.Month", { month: 8 }),
  },
  {
    id: `M${month.sep}`,
    value: month.sep,
    label: i18n.t("HS.Month", { month: 9 }),
  },
  {
    id: `M${month.oct}`,
    value: month.oct,
    label: i18n.t("HS.Month", { month: 10 }),
  },
  {
    id: `M${month.nov}`,
    value: month.nov,
    label: i18n.t("HS.Month", { month: 11 }),
  },
  {
    id: `M${month.dec}`,
    value: month.dec,
    label: i18n.t("HS.Month", { month: 12 }),
  },
];

export const QuarterData: FilterOptionType[] = [
  {
    id: `Q${quarter.q1}`,
    value: quarter.q1,
    label: i18n.t("HS.Quarter", { quarter: 1 }),
  },
  {
    id: `Q${quarter.q2}`,
    value: quarter.q2,
    label: i18n.t("HS.Quarter", { quarter: 2 }),
  },
  {
    id: `Q${quarter.q3}`,
    value: quarter.q3,
    label: i18n.t("HS.Quarter", { quarter: 3 }),
  },
  {
    id: `Q${quarter.q4}`,
    value: quarter.q4,
    label: i18n.t("HS.Quarter", { quarter: 4 }),
  },
];

export const getYearData: (yearRangeEnd: number) => FilterOptionType[] = (yearRangeEnd: number) => [
  {
    id: `Y${yearRangeEnd - 3}`,
    value: `${yearRangeEnd - 3}`,
    label: `${yearRangeEnd - 3}`,
  },
  {
    id: `Y${yearRangeEnd - 2}`,
    value: `${yearRangeEnd - 2}`,
    label: `${yearRangeEnd - 2}`,
  },
  {
    id: `Y${yearRangeEnd - 1}`,
    value: `${yearRangeEnd - 1}`,
    label: `${yearRangeEnd - 1}`,
  },
  {
    id: `Y${yearRangeEnd - 0}`,
    value: `${yearRangeEnd - 0}`,
    label: `${yearRangeEnd - 0}`,
  },
];

export enum FilterTypes {
  QUARTER = 1,
  MONTH = 2,
  YEAR = 3,
  ALL = -1,
}

export const CURRENT_YEAR = moment().year();
export const CURRENT_MONTH = moment().month();
export const CURRENT_QUARTER = moment().quarter();

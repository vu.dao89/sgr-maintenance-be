import React, { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { StyleProp, TouchableOpacity, View, ViewStyle } from "react-native";
import moment from "moment";
import i18n from "@I18n";

import { heightResponsive as H } from "@Constants";
import { ICCaret2Left, ICCaret2Right } from "@Assets/image";
import Text from "../TextCM";
import BaseModal from "../BaseModal";
import { CURRENT_YEAR, FilterTypes, getYearData, MonthData, QuarterData } from "./data";
import styles from "./styles";

type Props = {
  style?: StyleProp<ViewStyle>;
  onSelectOption: (dateObject: any, filterType: FilterTypes) => void;
  ref?: any;
};

const ModalFilter = forwardRef((props: Props, ref) => {
  const { onSelectOption } = props;
  const [yearRangeEnd, setYearRangeEnd] = useState(CURRENT_YEAR);
  const baseModalRef = useRef<any>();
  const [selectedFilterType, setSelectedFilterType] = useState<FilterTypes>(FilterTypes.MONTH);

  const [selectedFilterID, setSelectedFilterID] = useState("");
  const [selectedYear, setSelectedYear] = useState(CURRENT_YEAR);
  type IFilterOption = {
    id: string;
    value: string;
    label: string;
  };

  const onCloseModal = () => {
    baseModalRef.current.onCloseModal();
  };
  const onOpenModal = () => {
    baseModalRef.current.onOpenModal();
  };
  useImperativeHandle(ref, () => ({
    showFilter: () => onOpenModal(),
    hideFilter: () => onCloseModal(),
    resetFilter: () => onResetFilter(),
  }));

  const getFilterOptions = (type: FilterTypes) => {
    switch (type) {
      case FilterTypes.MONTH:
        return MonthData;
      case FilterTypes.QUARTER:
        return QuarterData;
      case FilterTypes.YEAR:
        return getYearData(yearRangeEnd);
      default:
        return [];
    }
  };

  const onYearRangeLeft = () => {
    const step = selectedFilterType === FilterTypes.YEAR ? 4 : 1;
    setYearRangeEnd(y => y - step);
  };
  const onYearRangeRight = () => {
    const step = selectedFilterType === FilterTypes.YEAR ? 4 : 1;
    setYearRangeEnd(y => Math.min(y + step, CURRENT_YEAR));
  };

  const selectOption = (opt: any) => {
    setSelectedFilterID(opt.id);

    selectedFilterType === FilterTypes.YEAR ? setSelectedYear(opt.value) : setSelectedYear(yearRangeEnd);

    const dateObject = getDateObject(opt.value);
    onSelectOption(dateObject, selectedFilterType);
    onCloseModal();
    // console.log(getDateObject(opt.value));
    // onSelectOption(
    //   _queryString,
    //   selectedFilterType === DTU.SELECT_YEAR ? opt.value : yearRangeEnd,
    //   opt.value,
    //   selectedFilterType,
    // );
  };
  const onResetFilter = () => {
    onCloseModal();
    setSelectedFilterID("");
    onSelectOption(
      {
        from_date: "",
        to_date: "",
      },
      FilterTypes.ALL
    );
  };
  const onSelectCurrentPeriod = async () => {
    let dateObject = {
      from_date: "",
      to_date: "",
    };
    const currentPeriod =
      selectedFilterType === FilterTypes.MONTH
        ? moment().month()
        : selectedFilterType === FilterTypes.QUARTER
        ? moment().quarter()
        : moment().year();

    const currentPeriodID =
      getFilterOptions(selectedFilterType).find(o => Number(o.value) === currentPeriod)?.id || `${CURRENT_YEAR}`;
    switch (selectedFilterType) {
      case FilterTypes.MONTH:
        dateObject = {
          from_date: moment().startOf("month").format("YYYYMMDD"),
          to_date: moment().endOf("month").format("YYYYMMDD"),
        };
        break;
      case FilterTypes.QUARTER:
        dateObject = {
          from_date: moment().startOf("quarter").format("YYYYMMDD"),
          to_date: moment().endOf("quarter").format("YYYYMMDD"),
        };
        break;
      case FilterTypes.YEAR:
        dateObject = {
          from_date: moment().startOf("year").format("YYYYMMDD"),
          to_date: moment().endOf("year").format("YYYYMMDD"),
        };
        break;
      default:
        break;
    }
    setYearRangeEnd(CURRENT_YEAR);
    setSelectedFilterID(currentPeriodID);
    onSelectOption(dateObject, selectedFilterType);
    onCloseModal();
  };
  const getDateObject = (selectedValue: string) => {
    switch (selectedFilterType) {
      case FilterTypes.MONTH:
        return {
          from_date: moment(`${selectedValue}${yearRangeEnd}`, "MMYYYY").startOf("month").format("YYYYMMDD"),
          to_date: moment(`${selectedValue}${yearRangeEnd}`, "MMYYYY").endOf("month").format("YYYYMMDD"),
        };
      case FilterTypes.QUARTER:
        return {
          from_date: moment(`${yearRangeEnd}`).quarter(Number(selectedValue)).startOf("quarter").format("YYYYMMDD"),
          to_date: moment(`${yearRangeEnd}`).quarter(Number(selectedValue)).endOf("quarter").format("YYYYMMDD"),
        };
      case FilterTypes.YEAR:
        return {
          from_date: moment(`${selectedValue}`).startOf("year").format("YYYYMMDD"),
          to_date: moment(`${selectedValue}`).endOf("year").format("YYYYMMDD"),
        };
      default:
        return "";
    }
  };

  const FilterType = () => {
    return (
      <View style={styles.ctnFilterType}>
        <FilterTypeOption key={0} id={FilterTypes.MONTH} />
        <FilterTypeOption key={1} id={FilterTypes.QUARTER} />
        <FilterTypeOption key={2} id={FilterTypes.YEAR} />
      </View>
    );
  };

  const FilterTypeOption = ({ id }: { id: FilterTypes }) => {
    const handleFilterTypePress = () => {
      setSelectedFilterType(id);
    };
    return (
      <TouchableOpacity
        onPress={handleFilterTypePress}
        style={id === selectedFilterType ? styles.ctnFilterTypeSelected : styles.ctnFilterTypeOption}>
        <Text style={id === selectedFilterType ? styles.txtFilterTypeSelected : styles.txtFilterType}>{`${i18n.t(
          id == FilterTypes.MONTH ? "HS.SelectMonth" : id === FilterTypes.QUARTER ? "HS.SelectQuarter" : "HS.SelectYear"
        )}`}</Text>
      </TouchableOpacity>
    );
  };
  const FilterOption = ({ opt }: any) => {
    const _onSelectOption = () => {
      selectOption(opt);
    };
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={_onSelectOption}
        style={[
          styles.ctnFilterOption,
          opt.id === selectedFilterID && (selectedFilterType === FilterTypes.YEAR || selectedYear === yearRangeEnd)
            ? styles.ctnFilterOptionSelected
            : null,
        ]}>
        <Text
          style={[
            styles.txtFilterOption,
            opt.id === selectedFilterID && (selectedFilterType === FilterTypes.YEAR || selectedYear === yearRangeEnd)
              ? styles.txtFilterOptionSelected
              : null,
          ]}>
          {opt.label}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <BaseModal header={i18n.t("HS.SelectTime")} ref={baseModalRef}>
      <View style={styles.ctnModalView}>
        <FilterType />
        <View style={styles.ctnYearRange}>
          <TouchableOpacity onPress={onYearRangeLeft}>
            <ICCaret2Left width={H(24)} height={H(24)} />
          </TouchableOpacity>
          <Text style={styles.txtYearRange}>
            {selectedFilterType === FilterTypes.YEAR ? `${yearRangeEnd - 3}-${yearRangeEnd}` : `${yearRangeEnd}`}
          </Text>
          <TouchableOpacity
            onPress={onYearRangeRight}
            disabled={yearRangeEnd === CURRENT_YEAR}
            style={{
              opacity: yearRangeEnd === CURRENT_YEAR ? 0 : 1,
            }}>
            <ICCaret2Right width={H(24)} height={H(24)} />
          </TouchableOpacity>
        </View>
        <View style={styles.ctnFilterOptions}>
          {getFilterOptions(selectedFilterType)?.map((item: IFilterOption, index: number) => {
            return <FilterOption key={`${item.id}` + `${index}`} opt={item} />;
          })}
        </View>
        <View style={styles.ctnSelectCurrent}>
          <TouchableOpacity onPress={onResetFilter}>
            <Text style={styles.txtSelectCurrent}>{`${i18n.t("HS.ResetFilter")}`}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onSelectCurrentPeriod}>
            <Text style={styles.txtSelectCurrent}>{`${
              selectedFilterType === FilterTypes.MONTH
                ? i18n.t("HS.CurrentMonth")
                : selectedFilterType === FilterTypes.QUARTER
                ? i18n.t("HS.CurrentQuarter")
                : i18n.t("HS.CurrentYear")
            }`}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </BaseModal>
  );
});
ModalFilter.displayName = "ModalFilter";
export default ModalFilter;

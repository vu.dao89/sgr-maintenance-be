import { StyleSheet } from "react-native";

import { Color, heightResponsive as H, widthResponsive as W, FontSize as FS } from "@Constants";

const styles = StyleSheet.create({
  ctnModalView: {
    height: 400,
    backgroundColor: Color.CardBackground,
    borderTopLeftRadius: H(20),
    borderTopRightRadius: H(20),
  },
  ctnModalViewOvl: {
    height: "50%",
    opacity: 0.85,
    backgroundColor: Color.Background,
  },
  ctnModalHeader: {
    flexDirection: "row",
    backgroundColor: Color.TableHeaderBackground,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 12,
    borderTopLeftRadius: H(20),
    borderTopRightRadius: H(20),
  },
  txtModalHeader: {
    color: Color.White,
    fontWeight: "600",
    fontSize: FS.FontBig,
    lineHeight: H(28),
    textAlign: "center",
  },
  txtFilterType: {
    color: Color.TextGray,
    fontWeight: "500",
    fontSize: FS.FontSmaller,
    lineHeight: 20,
  },
  txtFilterTypeSelected: {
    color: Color.Yellow,
    fontWeight: "500",
    fontSize: FS.FontSmaller,
    lineHeight: 20,
  },
  ctnFilterType: {
    flexDirection: "row",
    height: H(46),
    alignItems: "stretch",
    marginTop: H(16),
  },
  ctnFilterTypeOption: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  ctnFilterTypeSelected: {
    borderBottomColor: Color.Yellow,
    borderBottomWidth: H(2),
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  ctnButtonClose: {
    position: "absolute",
    right: 0,
    padding: H(16),
  },
  ctnYearRange: {
    height: H(48),
    marginTop: H(12),
    paddingHorizontal: W(16),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  txtYearRange: {
    color: Color.Yellow,
    fontWeight: "700",
    fontSize: FS.FontMedium,
    lineHeight: H(24),
  },
  ctnFilterOptions: {
    // paddingHorizontal: W(16),
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  ctnFilterOption: {
    width: W(78),
    height: H(48),
    borderRadius: H(8),
    borderColor: Color.FilterBackground,
    borderWidth: H(1),
    justifyContent: "center",
    alignItems: "center",
    marginTop: H(12),
  },
  txtFilterOption: {
    color: Color.White,
    fontWeight: "400",
    fontSize: FS.FontMedium,
    lineHeight: H(24),
  },
  ctnFilterOptionSelected: {
    // additional to ctnFilterOption
    backgroundColor: Color.Yellow,
  },
  txtFilterOptionSelected: {
    // additional to txtFilterOption
    color: Color.TextButton,
  },
  ctnSelectCurrent: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: W(16),
    marginVertical: H(20),
  },
  txtSelectCurrent: {
    color: Color.Yellow,
    fontWeight: "500",
    fontSize: 14,
    lineHeight: 20,
  },
  flexRow: {
    flexDirection: "row",
  },
});
export default styles;

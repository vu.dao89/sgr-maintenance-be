import { isEmpty } from "lodash";
import React, { FC } from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Platform, StyleProp, TouchableOpacity, View, ViewStyle } from "react-native";
import { useNavigation } from "@react-navigation/native";

import TextCM from "../TextCM";
import styles from "./styles";

import { ICBackSVG } from "@Assets/image";

import { heightResponsive } from "@Constants";
type Props = {
  style?: StyleProp<ViewStyle>;
  title?: string;
  titleComp?: any;
  componentLeft?: any;
  componentRight?: any;
  actionLeft?: () => void;
};
const Header: FC<Props> = ({ style, title, titleComp, componentLeft, componentRight, actionLeft }) => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const onBackNavigate = () => {
    actionLeft ? actionLeft() : navigation?.goBack();
  };
  return (
    <View
      style={[
        styles.container,
        {
          paddingTop: insets.top + heightResponsive(Platform.OS === "ios" ? 0 : 2),
        },
        style,
      ]}>
      <View style={styles.ctnTitle}>
        {isEmpty(componentLeft) ? (
          <TouchableOpacity
            style={styles.icBack}
            onPress={onBackNavigate}
            hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}>
            <ICBackSVG width={heightResponsive(9)} height={heightResponsive(18)} />
          </TouchableOpacity>
        ) : (
          componentLeft
        )}

        {!isEmpty(title) && <TextCM style={styles.txtTitle}>{title}</TextCM>}
        {!!titleComp && titleComp}
        <View style={styles.ctnRight}>{!isEmpty(componentRight) && componentRight}</View>
      </View>
    </View>
  );
};

export default Header;

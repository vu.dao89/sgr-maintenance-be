import { Color, FontSize, heightResponsive, widthResponsive } from "@Constants";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    width: "100%",
    position: "relative",
    paddingHorizontal: widthResponsive(16),
  },
  ctnTitle: {
    height: heightResponsive(30),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginVertical: heightResponsive(13),
  },
  txtTitle: {
    position: "absolute",
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
    color: Color.Yellow,
    lineHeight: 30,
  },
  icBack: {
    position: "absolute",
    left: widthResponsive(8),
    width: widthResponsive(30),
  },
  ctnRight: {
    position: "absolute",
    right: 0,
  },
});

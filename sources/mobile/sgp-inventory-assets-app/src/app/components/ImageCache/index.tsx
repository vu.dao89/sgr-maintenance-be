import {isEmpty} from 'lodash';
import React, {FC, useEffect, useState} from 'react';
import {StyleProp, View, ActivityIndicator} from 'react-native';
import FastImage, {ResizeMode, ImageStyle} from 'react-native-fast-image';

import {AXIOS_AUTH_TOKEN_URL} from '@Services/httpClient';
import {useSelector} from 'react-redux';
import {AppState} from '@Reducers';
import {Color} from '@Constants';

type Props = {
  imageDefault?: any;
  isCache?: boolean;
  uri?: string;
  resizeMode?: ResizeMode;
  style?: StyleProp<ImageStyle>;
  id?: any;
  isDownLoad?: boolean;
};
const ImageCache: FC<Props> = ({
  uri,
  imageDefault,
  resizeMode,
  style,
  id = '',
  isDownLoad = true,
  ...more
}) => {
  const accessToken = useSelector((state: AppState) => state.common.token);
  const [isImageError, setImageError] = useState(false);
  const [loadingImage, setLoadingImage] = useState(false);

  // Set image error when render
  useEffect(() => {
    if (isDownLoad) {
      setImageError(isEmpty(id));
    } else {
      setImageError(isEmpty(uri));
    }
  }, [uri, id]);

  //Handle Image error
  const onHandleImageError = () => {
    setImageError(true);
  };

  if (!isDownLoad) {
    return (
      <FastImage
        style={style}
        source={
          isImageError
            ? imageDefault
            : {
                uri: uri,
                headers: {},
                priority: FastImage.priority.normal,
              }
        }
        onError={onHandleImageError}
        resizeMode={resizeMode}
        {...more}
      />
    );
  }

  return (
    <>
      <FastImage
        style={style}
        onLoadEnd={() => setLoadingImage(false)}
        onLoadStart={() => setLoadingImage(true)}
        source={
          isImageError
            ? imageDefault
            : {
                uri: `${AXIOS_AUTH_TOKEN_URL}/cos/content/download/${id}`,
                headers: {
                  Authorization: 'someAuthToken',
                  'X-Authorization': `Bearer ${accessToken}`,
                  'x-ms-blob-type': 'BlockBlob',
                },
                priority: FastImage.priority.normal,
              }
        }
        onError={onHandleImageError}
        resizeMode={resizeMode}
        {...more}
      />
      {loadingImage && (
        <View
          style={[
            style,
            {
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
          <ActivityIndicator size="large" color={Color.Yellow} />
        </View>
      )}
    </>
  );
};

export default React.memo(ImageCache);

import React, { FC, useEffect } from "react";
import { View, Modal, Platform, LayoutAnimation } from "react-native";
import { BarIndicator } from "react-native-indicators";
import { useSelector } from "react-redux";
import { AppState } from "@Reducers";

import styles from "./styles";
import { Color } from "@Constants";

const LoadingGlobal = () => {
  const isLoading = useSelector((state: AppState) => state.common.loading);

  // check loading ios error
  // useEffect(() => {
  //   if (Platform.OS === 'ios') {
  //     LayoutAnimation.easeInEaseOut();
  //   }
  // });

  return (
    <>
      <Modal
        visible={isLoading}
        // animationType="fade"
        transparent
        statusBarTranslucent>
        <View style={[styles.container]}>
          <BarIndicator color={Color.Yellow} size={40} />
        </View>
      </Modal>
    </>
  );
};

export default LoadingGlobal;

import { Color } from "@Constants";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.BackgroundDialog,
    alignItems: "center",
    justifyContent: "center",
  },
});

import React, { FC } from "react";
import { StyleProp, View, ViewStyle } from "react-native";
import { default as MapViewComponent, Marker, PROVIDER_GOOGLE } from "react-native-maps";

import { Region } from "./MapViewCustom";

type IMapView = {
  region: Region;
  style?: StyleProp<ViewStyle>;
};

const MapOnlyView: FC<IMapView> = ({ region, style }) => {
  return (
    <MapViewComponent
      style={style}
      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
      region={{ ...region, latitudeDelta: 0.015, longitudeDelta: 0.0121 }}>
      <Marker coordinate={region} />
    </MapViewComponent>
  );
};

export default MapOnlyView;

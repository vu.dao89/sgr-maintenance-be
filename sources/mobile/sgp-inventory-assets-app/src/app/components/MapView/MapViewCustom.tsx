import { isEmpty } from "lodash";
import React, { FC, forwardRef, ReactNode } from "react";
import {
  Platform,
  StyleProp,
  StyleSheet,
  TextInput,
  TextInputProps,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import { default as MapViewComponent, Marker, PROVIDER_GOOGLE } from "react-native-maps";

import { ICRefresh, IMNotLocation } from "@Assets/image";
import { Color, FontSize, heightResponsive as H, widthResponsive as W } from "@Constants";
import i18n from "@I18n";
import ImageCache from "../ImageCache";
import TextCM from "../TextCM";
import TextError from "../TextError";
import { LocationUtils } from "@Utils";

export type Region = {
  latitude: number;
  longitude: number;
};

export type IProps = {
  isRequired?: boolean;
  title?: string;
  style?: StyleProp<ViewStyle>;
  styleTitle?: StyleProp<ViewStyle>;
  styleTextInput?: StyleProp<ViewStyle>;
  styleMap?: StyleProp<ViewStyle>;
  region?: Region;
  onRefreshRegion?: () => void;
  messageError?: string;
  disabled?: boolean;
  rightComponent?: ReactNode;
  hasPermission?: boolean;
};

const MapViewCustom: FC<TextInputProps & IProps> = forwardRef(
  (
    {
      isRequired = false,
      title = "",
      style = {},
      styleTitle = {},
      styleTextInput = {},
      styleMap = {},
      region = { latitude: 15.9783846, longitude: 108.2598785 },
      onRefreshRegion,
      messageError,
      disabled = false,
      value,
      rightComponent,
      editable = false,
      hasPermission = false,
      ...more
    }: TextInputProps & IProps,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isError = !isEmpty(messageError);

    return (
      <View style={style}>
        {isUseTitle && (
          <View style={[{ flexDirection: "row" }, styleTitle]}>
            <TextCM style={[styles.txtDefaultTitle, styles.txtTitle, disabled && styles.txtDisable]}>{title}</TextCM>
            {isRequired && (
              <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired, disabled && styles.txtDisable]}>
                *
              </TextCM>
            )}
          </View>
        )}

        <View>
          <TextInput
            editable={editable}
            style={[styles.styleDefault, styleTextInput, disabled && styles.txtDisable]}
            placeholder={i18n.t("CM.TextField.txtInputData")}
            placeholderTextColor={!disabled ? Color.TextGray : styles.txtDisable.color}
            selectionColor={Color.White}
            value={value}
            underlineColorAndroid="transparent"
            {...more}
          />

          <TouchableOpacity
            hitSlop={{ right: W(12), left: W(12), top: H(12), bottom: H(12) }}
            onPress={() => onRefreshRegion && onRefreshRegion()}
            style={styles.ctnRightComponent}>
            <ICRefresh />
          </TouchableOpacity>

          {rightComponent && <View style={styles.ctnRightComponent}>{rightComponent}</View>}
        </View>

        {hasPermission ? (
          <MapViewComponent
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={[{ marginTop: H(16), marginBottom: H(8) }, styleMap]}
            region={{ ...region, latitudeDelta: 0.015, longitudeDelta: 0.0121 }}>
            {!isEmpty(value) && <Marker coordinate={region} />}
          </MapViewComponent>
        ) : (
          <View style={styles.ctnMap}>
            <ImageCache
              imageDefault={IMNotLocation}
              style={{
                width: "100%",
                height: H(150),
                paddingHorizontal: W(16),
              }}
            />
            <TextCM style={styles.txtNotData}>{String(i18n.t("IA.txtNotDataMap"))}</TextCM>
            <TextCM style={styles.txtRecomment}>{String(i18n.t("IA.txtRecommentPermissionLocation"))}</TextCM>
            <TouchableOpacity style={styles.btnSetup} onPress={() => LocationUtils.openSettingLocation()}>
              <TextCM style={styles.txtSetup}>{String(i18n.t("IA.btnSetup"))}</TextCM>
            </TouchableOpacity>
          </View>
        )}

        {isError && <TextError styleWrapper={{ marginTop: 0 }}>{messageError}</TextError>}
      </View>
    );
  }
);

MapViewCustom.displayName = "MapViewCustom";
export default MapViewCustom;

export const styles = StyleSheet.create({
  styleDefault: {
    backgroundColor: Color.BodyTable,
    borderRadius: H(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: W(12),
    paddingRight: W(36),
    paddingVertical: Platform.select({ ios: H(12), android: H(10) }),
    color: Color.White,
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
    lineHeight: H(20),
  },
  txtDefaultTitle: {
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  txtTitle: {
    color: Color.LightGray,
    marginBottom: H(8),
  },
  txtTitleRequired: {
    marginLeft: W(2),
    color: Color.RedText,
  },
  ctnRightComponent: {
    position: "absolute",
    zIndex: 1,
    right: W(12),
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  ctnError: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginTop: H(8),
  },
  txtError: {
    marginLeft: W(4),
    marginRight: W(8),
    color: Color.RedText,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  //disable
  txtDisable: {
    color: Color.TxtDisable,
  },
  icError: {
    marginTop: H(2),
  },
  ctnMap: {
    width: "100%",
    borderRadius: H(16),
    overflow: "hidden",
    marginBottom: H(16),
    height: H(300),
    backgroundColor: Color.BodyTable,
    alignItems: "center",
    marginTop: H(16),
  },
  txtNotData: {
    marginTop: H(4),
    lineHeight: 24,
    fontSize: FontSize.FontMedium,
    fontWeight: "700",
    textAlign: "center",
    color: Color.White,
  },
  txtRecomment: {
    marginTop: H(4),
    lineHeight: 20,
    fontSize: FontSize.FontSmaller,
    fontWeight: "400",
    textAlign: "center",
    color: Color.lightWhite,
    paddingHorizontal: W(42),
  },
  btnSetup: {
    marginTop: H(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: Color.Yellow,
    borderRadius: H(10),
  },
  txtSetup: {
    lineHeight: 20,
    fontSize: FontSize.FontSmaller,
    fontWeight: "500",
    textAlign: "center",
    color: Color.TextButton,
    paddingHorizontal: W(32),
    paddingVertical: H(8),
  },
});

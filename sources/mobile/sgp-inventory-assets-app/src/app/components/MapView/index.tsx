import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TextInputProps, View } from "react-native";

import i18n from "@I18n";
import { LocationUtils } from "@Utils";
import MapViewCustom, { IProps } from "./MapViewCustom";

type IMapView = {
  name: string;
  useHookForm?: boolean;
};

const MapView: FC<TextInputProps & IProps & IMapView> = ({
  name,
  useHookForm = true,
  onRefreshRegion,
  hasPermission = false,
  ...more
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return field?.message ? String(field?.message) : i18n.t("CA.MsgRequired");
    }
    return "";
  };

  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { value, ...moreField } }) => {
            const { latitude, longitude } = value;

            return (
              <MapViewCustom
                {...moreField}
                {...more}
                region={{
                  latitude: latitude ? Number(latitude) : 15.9783846,
                  longitude: longitude ? Number(longitude) : 108.2598785,
                }}
                value={`${LocationUtils.getFullDMS(latitude, longitude)}`}
                onRefreshRegion={onRefreshRegion}
                messageError={getMsgError()}
                hasPermission={hasPermission}
              />
            );
          }}
        />
      ) : (
        <MapViewCustom {...more} />
      )}
    </View>
  );
};

export default MapView;

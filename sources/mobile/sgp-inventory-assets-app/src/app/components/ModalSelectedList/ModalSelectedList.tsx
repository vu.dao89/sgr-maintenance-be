// external
import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import Modal from "react-native-modal";
// internal
import { heightResponsive } from "@Constants";
import styles from "./styles";
// icon
import { ICCheck, ICClose } from "@Assets/image";
import TextCM from "../TextCM";

interface IProp {
  data: any;
  onItemPress: any;
  header: string;
  onClosePress: any;
  itemSelected: any;
}
const ModalSelectedList: FC<IProp> = ({ data, onItemPress, header, onClosePress, itemSelected }) => {
  const onPressItem = (item: any, index: number) => () => {
    onItemPress(item);
    onClosePress();
  };
  return (
    <Modal isVisible={true} animationOutTiming={1} style={styles.ctnModal}>
      <View style={styles.container}>
        <View style={styles.viewHeader}>
          <View />
          <TextCM style={styles.txtContent}>{header}</TextCM>
          <TouchableOpacity onPress={onClosePress}>
            <ICClose width={heightResponsive(24)} height={heightResponsive(24)} />
          </TouchableOpacity>
        </View>
        {data &&
          data?.map((item: any, index: number) => (
            <TouchableOpacity key={index} style={styles.viewItem} onPress={onPressItem(item, index)}>
              <TextCM style={item?.id === itemSelected?.id ? styles.txtItemSelect : styles.txtItem}>
                {item?.title}
              </TextCM>
              {item?.id === itemSelected?.id ? (
                <ICCheck width={heightResponsive(24)} height={heightResponsive(24)} />
              ) : null}
            </TouchableOpacity>
          ))}
      </View>
    </Modal>
  );
};
export default ModalSelectedList;

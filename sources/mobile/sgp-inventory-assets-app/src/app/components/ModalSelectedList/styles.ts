import { Color, FontSize, heightResponsive, widthResponsive } from "@Constants";
import { StyleSheet, Dimensions } from "react-native";
const windowWidth = Dimensions.get("window").width;

export default StyleSheet.create({
  container: {
    minHeight: heightResponsive(212),
    width: "100%",
    backgroundColor: Color.BackgroundSelect,
    borderTopLeftRadius: heightResponsive(20),
    borderTopRightRadius: heightResponsive(20),
    position: "absolute",
    bottom: 0,
  },
  txtContent: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
    fontWeight: "500",
  },
  txtItem: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  txtItemSelect: {
    color: Color.Yellow,
    fontSize: FontSize.FontMedium,
  },
  viewHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: widthResponsive(16),
    backgroundColor: Color.BackgroundTextInput,
    height: heightResponsive(54),
    alignItems: "center",
    borderTopLeftRadius: heightResponsive(20),
    borderTopRightRadius: heightResponsive(20),
  },
  viewItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: widthResponsive(18),
    height: heightResponsive(48),
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: Color.BackgroundItemDropdown,
  },
  ctnModal: {
    width: windowWidth,
    justifyContent: "center",
    alignSelf: "center",
    margin: 0,
  },
});

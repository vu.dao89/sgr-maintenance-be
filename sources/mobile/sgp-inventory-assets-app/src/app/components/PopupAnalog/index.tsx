// External
import React, { FC, forwardRef, ReactElement, useImperativeHandle, useRef } from "react";
import { View, TouchableOpacity, Modal, TouchableWithoutFeedback, StyleProp, ViewStyle } from "react-native";
import { isEmpty } from "lodash";

// Internal
import { IcAnalogConfirmDelete, IcAnalogError, IcAnalogSuccess, IcAnalogWarn, ICClose } from "@Assets/image";
import { heightResponsive as H, widthResponsive as W } from "@Constants";
import TextCM from "../TextCM";
import styles from "./styles";

type IProp = {
  visible?: boolean;
  /**
   *  Title of button cancel. If empty -> Hide the button
   */
  titleCancel?: string;
  /**
   *  Title of button confirm
   */
  titleButton?: string;
  /**
   *  Title of popup
   */
  titlePopup?: string;
  /**
   *  Body content of popup
   */
  content?: string;
  /**
   *  The Icon in middle of popup
   */
  iconShow?: ReactElement;
  /**
   *  Run when press the X icon and press back (Android & IOS)
   */
  onClose?: () => void;
  /**
   *  Press outside the popup
   */
  onPressOutside?: () => void;
  /**
   *  Press button confirm
   */
  onPressConfirm?: () => void;
  /**
   *  Press button cancel
   */
  onPressCancel?: () => void;
  /**
   *  Auto set icon image of popup by type name
   */
  type?: "success" | "error" | "warning" | "delete" | "custom";
  /**
   *  Style of button cancel
   */
  styleButtonCancel?: StyleProp<ViewStyle>;
  /**
   *  Style of button confirm
   */
  styleButtonConfirm?: StyleProp<ViewStyle>;
  /**
   *  use to show X icon
   */
  isShowClose?: boolean;
  /**
   *  call when modal hide
   */
  onDismiss?: () => void;
};

const PopupAnalog: FC<IProp> = forwardRef(
  (
    {
      visible = false,
      titleCancel = "",
      titleButton = "",
      titlePopup = "",
      content = "",
      iconShow,
      onClose,
      onPressConfirm,
      onPressCancel,
      onPressOutside,
      type = "success",
      styleButtonCancel,
      styleButtonConfirm,
      isShowClose = true,
      onDismiss,
    }: IProp,
    $ref
  ) => {
    const $refModal = useRef<any>(null);

    const onHide = () => {
      onClose && onClose();
    };

    const onPressConfirmButton = () => {
      onPressConfirm && onPressConfirm();
    };

    const onPressCancelButton = () => {
      onPressCancel && onPressCancel();
    };

    const onCancel = () => {
      onPressOutside && onPressOutside();
    };

    const onDismissModal = () => {
      onDismiss && onDismiss();
    };

    useImperativeHandle($ref, () => ({
      ...$refModal?.current,
      hide: onHide,
    }));

    const showIcon = () => {
      switch (type) {
        case "success":
          return <IcAnalogSuccess />;
        case "error":
          return <IcAnalogError />;
        case "warning":
          return <IcAnalogWarn />;
        case "delete":
          return <IcAnalogConfirmDelete />;
        case "custom":
        default:
          return iconShow;
      }
    };

    return (
      <Modal
        focusable
        transparent
        ref={$refModal}
        onRequestClose={onHide}
        statusBarTranslucent
        // animationType="fade"
        onDismiss={onDismissModal}
        visible={visible}>
        <View style={styles.container}>
          <TouchableWithoutFeedback onPress={onCancel}>
            <View style={styles.ctnModal}></View>
          </TouchableWithoutFeedback>

          <View style={styles.ctnContainerPopup}>
            <View style={styles.ctnPopup}>
              {isShowClose && (
                <TouchableOpacity
                  onPress={onHide}
                  style={styles.ctnCloseIcon}
                  hitSlop={{
                    top: H(12),
                    bottom: H(12),
                    left: W(16),
                    right: W(16),
                  }}>
                  <ICClose height={H(20)} width={H(20)} />
                </TouchableOpacity>
              )}
              {showIcon()}
              <TextCM style={styles.txtTitlePopup}>{titlePopup}</TextCM>
              <TextCM style={styles.txtContent}>{content}</TextCM>
              <View style={styles.ctnActions}>
                {!isEmpty(titleCancel) ? (
                  <>
                    <TouchableOpacity
                      style={[styles.btnDefault, styles.btnCancel, styleButtonCancel]}
                      onPress={onPressCancelButton}>
                      <TextCM style={styles.txtBtnActions}>{titleCancel}</TextCM>
                    </TouchableOpacity>
                    <View style={{ width: W(16) }}></View>
                  </>
                ) : null}
                {!isEmpty(titleButton) && (
                  <TouchableOpacity
                    style={[styles.btnDefault, styles.btnConfirm, styleButtonConfirm]}
                    onPress={onPressConfirmButton}>
                    <TextCM style={styles.txtBtnActions}>{titleButton}</TextCM>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
);

PopupAnalog.displayName = "PopupAnalog";

export default PopupAnalog;

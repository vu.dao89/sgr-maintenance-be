import { StyleSheet } from "react-native";

import { Color, heightResponsive as H, widthResponsive as W } from "@Constants";

export default StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  ctnModal: {
    backgroundColor: "rgba(0,0,0,0.75)",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 0,
  },
  ctnContainerPopup: {
    justifyContent: "center",
    margin: "5%",
  },
  ctnPopup: {
    width: W(311),
    backgroundColor: Color.CardBackground,
    borderRadius: H(16),
    paddingTop: H(12),
    paddingHorizontal: W(16),
    paddingBottom: H(16),
    alignItems: "center",
    alignSelf: "center",
  },
  ctnCloseIcon: { zIndex: 1, alignSelf: "flex-end" },
  txtTitlePopup: {
    textAlign: "center",
    color: Color.White,
    lineHeight: H(24),
    fontSize: H(16),
    fontWeight: "700",
    alignSelf: "stretch",
  },
  txtContent: {
    textAlign: "center",
    color: Color.lightWhite,
    lineHeight: H(20),
    fontSize: H(14),
    fontWeight: "400",
    alignSelf: "stretch",
  },
  ctnActions: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: H(24),
    width: "100%",
  },
  btnDefault: {
    minHeight: H(48),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: H(10),
    paddingHorizontal: W(16),
    paddingVertical: H(10),
    flexGrow: 1,
    flex: 1,
  },
  btnCancel: {
    backgroundColor: Color.White,
  },
  btnConfirm: {
    backgroundColor: Color.Yellow,
  },
  txtBtnActions: {
    color: Color.TextButton,
    lineHeight: H(24),
    fontSize: H(16),
    fontWeight: "500",
  },
});

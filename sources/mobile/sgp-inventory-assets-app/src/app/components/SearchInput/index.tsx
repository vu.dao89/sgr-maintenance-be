// external
import React, { FC, useState, forwardRef, useImperativeHandle, useRef } from "react";
import { TextInput, TouchableOpacity, View } from "react-native";
// internal
import { Color, heightResponsive } from "@Constants";
import styles from "./styles";
// icon
import { ICSearchGray, ICCloseSearch } from "@Assets/image";

interface IProp {
  onTextSearch: any;
  placeholder: string;
  value?: any;
  handleOnBlur?: any;
  ref?: any;
  wrapperStyle?: any;
}
const SearchInput: FC<IProp> = forwardRef((props: IProp, ref: any) => {
  const { onTextSearch, placeholder, value, handleOnBlur, wrapperStyle } = props;
  const [keyword, setKeyword] = useState(value || "");
  const inputRef = useRef<any>(null);

  useImperativeHandle(ref, () => ({
    focus: () => inputRef?.current.focus(),
  }));
  const onChangeText = (_value: any) => {
    setKeyword(_value);
  };
  const handleClearInput = () => {
    setKeyword("");
    if (inputRef?.current) {
      inputRef?.current.focus();
    }
  };
  const onBlur = () => {
    if (keyword === "" && handleOnBlur) handleOnBlur();
  };
  const onSubmitEditing = () => {
    onTextSearch(keyword);
    if (keyword === "" && handleOnBlur) handleOnBlur();
  };
  return (
    <View style={[styles.ctnSearch, wrapperStyle]}>
      <TouchableOpacity onPress={onSubmitEditing} hitSlop={{ top: 5, left: 5, right: 5, bottom: 5 }}>
        <ICSearchGray width={heightResponsive(20)} height={heightResponsive(20)} />
      </TouchableOpacity>
      <TextInput
        ref={inputRef}
        style={styles.textInput}
        onBlur={onBlur}
        value={keyword}
        multiline={false}
        autoCorrect={false}
        onChangeText={onChangeText}
        returnKeyType="go"
        onSubmitEditing={onSubmitEditing}
        placeholder={placeholder || ""}
        placeholderTextColor={Color.MedalDescription}
      />
      {keyword.length > 0 ? (
        <TouchableOpacity
          style={styles.icClear}
          onPress={handleClearInput}
          hitSlop={{ bottom: 5, top: 5, left: 5, right: 5 }}>
          <ICCloseSearch width={heightResponsive(20)} height={heightResponsive(20)} />
        </TouchableOpacity>
      ) : null}
    </View>
  );
});
SearchInput.displayName = "SearchInput";
export default SearchInput;

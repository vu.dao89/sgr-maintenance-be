import { Color, FontSize, heightResponsive, widthResponsive } from "@Constants";
import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  ctnSearch: {
    backgroundColor: Color.BackgroundTextInput,
    borderRadius: heightResponsive(40),
    height: heightResponsive(44),
    flexDirection: "row",
    marginVertical: heightResponsive(16),
    alignItems: "center",
    paddingHorizontal: widthResponsive(16),
  },
  textInput: {
    flex: 1,
    marginLeft: widthResponsive(8),
    marginRight: widthResponsive(20 + 8),
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  icClear: {
    position: "absolute",
    right: widthResponsive(12),
  },
});

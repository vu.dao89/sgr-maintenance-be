import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TouchableOpacityProps, View } from "react-native";
import SelectBoxCustom, { IProp } from "./SelectBoxCustom";

type ISelectBox = {
  name: string;
  useHookForm?: boolean;
  onChangeText?: (value: string) => void;
};

const SelectBox: FC<TouchableOpacityProps & IProp & ISelectBox> = ({
  name,
  useHookForm = true,
  onChangeText,
  ...more
}) => {
  const {
    control,
    setValue,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return String(field?.message) || "";
    }
    return "";
  };

  const _onChangeValue = (value: any): void => {
    setValue(name, value);
  };

  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { value, ...moreField } }) => {
            const { label } = value;
            return (
              <SelectBoxCustom
                {...moreField}
                {...more}
                value={label}
                onSelectData={_onChangeValue}
                messageError={getMsgError()}
              />
            );
          }}
        />
      ) : (
        <SelectBoxCustom {...more} />
      )}
    </View>
  );
};

export default SelectBox;

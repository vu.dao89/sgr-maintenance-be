import { ICArrowDown, ICArrowDownDisable, IcWarningRed } from "@Assets/image";
import { Color, FontSize, heightResponsive as H, widthResponsive as W } from "@Constants";
import { isEmpty } from "lodash";
import React, { FC, forwardRef, ReactNode } from "react";
import {
  Platform,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
  ViewStyle,
} from "react-native";
import TextCM from "../TextCM";

export type IObjModal = {
  header: string;
  data?: any;
  itemSelected?: any;
};

export type IProp = {
  /**
   *  Title of select field
   */
  title?: string;
  /**
   *  Show the letter * beside title
   */
  isRequired?: boolean;
  /**
   *  Style of title container
   */
  styleCtnTitle?: StyleProp<ViewStyle>;
  /**
   *  Style of title select field
   */
  styleCtnTextInput?: StyleProp<ViewStyle>;
  /**
   *  Set disable the select field
   */
  disable?: boolean;
  /**
   *  If have message error. It will show the message
   */
  messageError?: string;
  /**
   *  Value of select field
   */
  value?: string;
  /**
   *  Place holder of select field
   */
  placeholder?: string;
  /**
   *  toggleModal from select field
   */
  toggleModal?: () => void;
  /**
   *  renderModal from select field
   */
  renderModal?: ReactNode;
};

export const SelectField: FC<TouchableOpacityProps & IProp> = forwardRef(
  (
    {
      style,
      title,
      styleCtnTextInput,
      isRequired,
      styleCtnTitle,
      value,
      disable = false,
      messageError = "",
      placeholder,
      toggleModal,
      renderModal = <></>,
      ...more
    }: TouchableOpacityProps & IProp,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isFill = !isEmpty(value);
    const isError = !isEmpty(messageError);

    return (
      <View style={style}>
        {isUseTitle ? (
          <View style={[{ flexDirection: "row" }, styleCtnTitle]}>
            <TextCM style={[styles.txtDefaultTitle, styles.txtTitle, disable && styles.txtDisable]}>{title}</TextCM>
            {isRequired ? (
              <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired, disable && styles.txtDisable]}>*</TextCM>
            ) : null}
          </View>
        ) : null}

        <View>
          <TouchableOpacity style={styles.styleDefault} disabled={disable} {...more} onPress={toggleModal}>
            <TextCM
              numberOfLines={1}
              style={[
                styles.styleTextDefault,
                styleCtnTextInput,
                disable && styles.txtDisable,
                !value && styles.textDisable,
              ]}>
              {value || placeholder}
            </TextCM>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={disable}
            hitSlop={{ right: W(12), left: W(12), top: H(12), bottom: H(12) }}
            onPress={toggleModal}
            style={styles.ctnRightComponent}>
            {disable ? <ICArrowDownDisable /> : <ICArrowDown />}
          </TouchableOpacity>
        </View>

        {isError && (
          <View style={styles.ctnError}>
            <IcWarningRed />
            <TextCM style={styles.txtError}>{messageError}</TextCM>
          </View>
        )}

        {renderModal}
      </View>
    );
  }
);

export default SelectField;

export const styles = StyleSheet.create({
  textFieldLable: {
    paddingBottom: W(8),
    fontSize: FontSize.FontSmaller,
    color: Color.MedalDescription,
  },
  textRequired: {
    marginLeft: W(2),
    fontSize: FontSize.FontSmaller,
    color: Color.Red,
  },
  // TEXTFIELD
  textWrapper: {
    backgroundColor: Color.BodyTable,
    borderRadius: H(10),
    height: H(44),
    width: W(343),
    flexDirection: "row",
    marginBottom: H(16),
    alignItems: "center",
    paddingHorizontal: W(8),
  },
  textInput: {
    marginLeft: W(8),
    color: Color.White,
    fontSize: FontSize.FontMedium,
    width: W(295),
  },
  textDisable: {
    color: Color.TxtDisable,
  },
  styleDefault: {
    backgroundColor: Color.BodyTable,
    borderRadius: H(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  styleTextDefault: {
    paddingLeft: W(12),
    paddingRight: W(36),
    paddingVertical: Platform.select({ ios: H(10), android: H(12) }),
    color: Color.White,
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
    lineHeight: H(24),
  },
  txtDefaultTitle: {
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  txtTitle: {
    color: Color.LightGray,
    marginBottom: H(8),
  },
  txtTitleRequired: {
    marginLeft: W(2),
    color: Color.RedText,
  },
  ctnRightComponent: {
    position: "absolute",
    zIndex: 1,
    right: W(12),
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  ctnError: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: H(8),
  },
  txtError: {
    marginLeft: W(4),
    color: Color.RedText,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  //disable
  txtDisable: {
    color: Color.TxtDisable,
  },
});

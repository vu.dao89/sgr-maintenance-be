import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TouchableOpacityProps, View } from "react-native";
import SelectFieldCustom, { IProp } from "./SelectFieldCustom";

type ISelectBox = {
  name: string;
  useHookForm?: boolean;
  onChangeText?: (value: string) => void;
};

const SelectField: FC<TouchableOpacityProps & IProp & ISelectBox> = ({
  name,
  useHookForm = true,
  onChangeText,
  ...more
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return String(field?.message) || "";
    }
    return "";
  };

  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { value, ...moreField } }) => {
            const { label } = value;
            return <SelectFieldCustom {...moreField} {...more} value={label || ""} messageError={getMsgError()} />;
          }}
        />
      ) : (
        <SelectFieldCustom {...more} />
      )}
    </View>
  );
};

export default SelectField;

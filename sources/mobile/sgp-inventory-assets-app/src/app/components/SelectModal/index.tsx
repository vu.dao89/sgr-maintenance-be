// External
import React, { FC, forwardRef, useImperativeHandle, useRef, useState } from "react";
import { ActivityIndicator, FlatList, RefreshControl, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { get } from "lodash";
import { useSelector } from "react-redux";

// Internal
import TextCM from "../TextCM";
import { Color, heightResponsive as H, widthResponsive as W } from "@Constants";
import { ICCheck } from "@Assets/image";
import styles from "./styles";
import { AppState } from "@Reducers";
import BaseModal from "../BaseModal";
import SearchInput from "../SearchInput";

type IProps = {
  ref: any;
  onClose?: any;
  onSelect?: any;
  value?: any;
  isSearch?: boolean;
  placeholderSearch?: string;
  onSearch?: (keyword: string) => void;
  data?: any[];
  renderItem?: any;
  title?: string;
  onLoadMore?: any;
  onRefresh?: any;
  handleAfterSelect?: () => void;
};

const SelectModal: FC<IProps> = forwardRef((props: IProps, ref) => {
  const {
    title,
    onSelect,
    value,
    data,
    isSearch,
    renderItem,
    onSearch,
    placeholderSearch,
    onClose,
    onLoadMore,
    onRefresh,
  } = props;
  const myListRef = useRef<any>(null);
  const baseRef = useRef<any>(null);
  const insets = useSafeAreaInsets();
  const [hasScrolled, setHasScrolled] = useState(false);
  const HomeData = useSelector((state: AppState) => state.home);
  const refreshing = get(HomeData.filterData, "loading", false);
  const [isLoadingLoadMore, setLoadingLoadMore] = useState(false);

  const onScroll = () => {
    setHasScrolled(true);
  };
  const onSelectItem = (item: any) => {
    if (onSelect) {
      onSelect(item);
    }
  };
  const onSearchData = (keyword: string) => {
    if (onSearch) {
      onSearch(keyword);
    }
  };

  const renderItemDefault = ({ item }: any) => {
    const isSelected = value === item.id;
    return (
      <TouchableOpacity style={styles.ctnItem} onPress={() => onSelectItem(item)}>
        <TextCM style={isSelected ? styles.txtItemNameSelected : styles.txtItemName}>{item.label}</TextCM>
        {isSelected ? <ICCheck style={{ marginRight: W(8) }} width={H(24)} height={H(24)} /> : null}
      </TouchableOpacity>
    );
  };

  useImperativeHandle(ref, () => ({
    ...baseRef.current,
    scrollIndex: (index: any) => {
      setTimeout(() => {
        myListRef.current?.scrollToIndex &&
          myListRef.current?.scrollToIndex({
            index: index,
            animated: true,
            viewPosition: 0.2,
          });
      }, 500);
    },
  }));

  const handleIsLoadMore = () => {
    setLoadingLoadMore(false);
    setHasScrolled(false);
  };
  const onEndReached = (info: any) => {
    if (!hasScrolled || isLoadingLoadMore) {
      return null;
    }
    if (onLoadMore) {
      setLoadingLoadMore(true);
      onLoadMore(handleIsLoadMore);
    }
  };

  const SeparatorComponent = () => <View style={styles.ctnSeparator} />;
  return (
    <BaseModal header={title} ref={baseRef} onClosePress={onClose}>
      <View style={{ height: H(667) }}>
        {isSearch ? (
          <View style={{ marginHorizontal: 16 }}>
            <SearchInput placeholder={placeholderSearch || ""} onTextSearch={onSearchData} />
          </View>
        ) : (
          <></>
        )}

        <FlatList
          style={{ flex: 1, marginBottom: insets.bottom + H(16) }}
          ItemSeparatorComponent={SeparatorComponent}
          data={data || []}
          keyExtractor={item => item?.id}
          ref={myListRef}
          onScroll={onScroll}
          onEndReachedThreshold={0.1}
          ListFooterComponent={
            <View>
              {isLoadingLoadMore && (
                <ActivityIndicator
                  animating={isLoadingLoadMore}
                  color={Color.Yellow}
                  size="large"
                  style={styles.mt16}
                />
              )}
              <View style={styles.ctnFooter}></View>
            </View>
          }
          onEndReached={onEndReached}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} tintColor={Color.White} />}
          renderItem={renderItem ? renderItem : renderItemDefault}
          onScrollToIndexFailed={info => {
            const wait = new Promise(resolve => setTimeout(resolve, 500));
            wait.then(() => {
              myListRef.current?.scrollToIndex({
                index: info.index,
                animated: true,
              });
            });
          }}
        />
      </View>
    </BaseModal>
  );
});

SelectModal.displayName = "SelectModal";
export default React.memo(SelectModal);

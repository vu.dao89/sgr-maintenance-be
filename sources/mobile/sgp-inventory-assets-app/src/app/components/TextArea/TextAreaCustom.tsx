// External
import React, { FC, forwardRef, useImperativeHandle, useRef } from "react";
import { TextInput, TextInputProps, View, ViewStyle, StyleProp } from "react-native";
import { isEmpty } from "lodash";

// Internal
import { IcWarningRed } from "@Assets/image";
import { Color } from "@Constants";
import i18n from "@I18n";
import TextCM from "../TextCM";
import styles from "./styles";

export type IProp = {
  /**
   *  Title of text area
   */
  title?: string;
  /**
   *  Show the letter * beside title
   */
  isRequired?: boolean;
  /**
   *  Style of title container
   */
  styleCtnTitle?: StyleProp<ViewStyle>;
  /**
   *  Style of title textarea
   */
  styleCtnTextInput?: StyleProp<ViewStyle>;
  /**
   *  Set disable the textarea
   */
  disable?: boolean;
  /**
   *  If have message error. It will show the message
   */
  messageError?: string;
};

const TextAreaCustom: FC<TextInputProps & IProp> = forwardRef(
  (
    {
      style,
      title,
      styleCtnTextInput,
      isRequired,
      styleCtnTitle,
      value,
      disable = false,
      messageError = "",
      onChangeText,
      ...more
    }: TextInputProps & IProp,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isError = !isEmpty(messageError);
    const $refTextInput = useRef<TextInput>(null);
    useImperativeHandle($ref, () => ({
      ...$refTextInput?.current,
      clear: () => {
        onChangeText && onChangeText("");
      },
    }));

    return (
      <View style={style}>
        {isUseTitle ? (
          <View style={[{ flexDirection: "row" }, styleCtnTitle]}>
            <TextCM style={[styles.txtDefaultTitle, styles.txtTitle, disable && styles.txtDisable]}>{title}</TextCM>
            {isRequired ? (
              <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired, disable && styles.txtDisable]}>*</TextCM>
            ) : null}
          </View>
        ) : null}

        <View>
          <TextInput
            multiline
            editable={!disable}
            ref={$refTextInput}
            style={[styles.styleDefault, styleCtnTextInput, disable && styles.txtDisable]}
            placeholder={i18n.t("CM.TextField.txtInputData")}
            placeholderTextColor={!disable ? Color.TextGray : styles.txtDisable.color}
            value={value}
            onChangeText={onChangeText}
            underlineColorAndroid="transparent"
            {...more}
          />
        </View>
        {isError ? (
          <View style={styles.ctnError}>
            <IcWarningRed />
            <TextCM style={styles.txtError}>{messageError}</TextCM>
          </View>
        ) : null}
      </View>
    );
  }
);

TextAreaCustom.displayName = "TextAreaCustom";

export default TextAreaCustom;

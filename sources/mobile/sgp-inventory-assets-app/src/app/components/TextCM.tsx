import { Color as Colors, Font, FontSize } from "@Constants";
import React, { FC } from "react";
import { Text, TextProps } from "react-native";

type CMProps = {
  children?: any;
  color?: any;
  size?: any;
};

const TextCM: FC<TextProps & CMProps> = ({ style, color, size = "FontSmaller", ...more }) => {
  return (
    <Text
      style={[
        {
          fontWeight: "400",
          fontFamily: Font.Roboto,
          // @ts-ignore
          fontSize: FontSize[size] || size,
          ...(!!color && {
            // @ts-ignore
            color: Colors[color] || color,
          }),
        },
        style,
      ]}
      {...more}
    />
  );
};

export default TextCM;

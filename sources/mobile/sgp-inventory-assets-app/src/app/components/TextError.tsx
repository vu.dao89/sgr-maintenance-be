import { ICErrorField } from "@Assets/image";
import { Color, Font, FontSize, heightResponsive, widthResponsive } from "@Constants";
import React, { FC } from "react";
import { Text, TextProps, StyleSheet, View, StyleProp, ViewStyle } from "react-native";

type IStyleTextField = {
  styleWrapper?: StyleProp<ViewStyle>;
};

const TextError: FC<TextProps & IStyleTextField> = ({ style, styleWrapper = {}, ...more }) => {
  return (
    <View style={[styles.fieldWrapper, styleWrapper]}>
      <ICErrorField style={styles.styleIc} width={widthResponsive(16)} height={heightResponsive(16)} />
      <Text style={[styles.styleDefault, style]} {...more} />
    </View>
  );
};

const styles = StyleSheet.create({
  fieldWrapper: {
    flexDirection: "row",
    marginTop: heightResponsive(8),
  },
  styleIc: {
    marginTop: heightResponsive(2),
  },
  styleDefault: {
    fontFamily: Font.Roboto,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    color: Color.RedText,
    lineHeight: heightResponsive(20),
    marginLeft: widthResponsive(5.5),
  },
});

export default TextError;

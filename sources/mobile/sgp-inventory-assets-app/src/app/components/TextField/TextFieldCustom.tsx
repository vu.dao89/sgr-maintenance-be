// External
import React, { FC, forwardRef, ReactElement, useImperativeHandle, useRef } from "react";
import { TextInput, TextInputProps, View, ViewStyle, StyleProp, TouchableOpacity } from "react-native";
import { isEmpty } from "lodash";

// Internal
import { ICCloseCircle, IcWarningRed } from "@Assets/image";
import { Color, heightResponsive as H, widthResponsive as W } from "@Constants";
import i18n from "@I18n";
import TextCM from "../TextCM";
import styles from "./styles";

export type IProp = {
  /**
   *  Title of text field
   */
  title?: string;
  /**
   *  Show the letter * beside title
   */
  isRequired?: boolean;
  /**
   *  Style of title container
   */
  styleCtnTitle?: StyleProp<ViewStyle>;
  /**
   *  Style of title textfield
   */
  styleCtnTextInput?: StyleProp<ViewStyle>;
  /**
   *  Right Component . if not it will have x icon when fill.
   *  Set <></> to hide it
   */
  rightComponent?: ReactElement;
  /**
   *  Set disable the textfield
   */
  disable?: boolean;
  /**
   *  If have message error. It will show the message
   */
  messageError?: string;
};

const TextFieldCustom: FC<TextInputProps & IProp> = forwardRef(
  (
    {
      style,
      title,
      styleCtnTextInput,
      isRequired,
      styleCtnTitle,
      rightComponent,
      value,
      disable = false,
      messageError = "",
      onChangeText,
      ...more
    }: TextInputProps & IProp,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isFill = !isEmpty(value);
    const isError = !isEmpty(messageError);
    const $refTextInput = useRef<TextInput>(null);
    useImperativeHandle($ref, () => ({
      ...$refTextInput?.current,
      clear: () => {
        onChangeText && onChangeText("");
      },
    }));

    return (
      <View style={style}>
        {isUseTitle ? (
          <View style={[{ flexDirection: "row" }, styleCtnTitle]}>
            <TextCM style={[styles.txtDefaultTitle, styles.txtTitle, disable && styles.txtDisable]}>{title}</TextCM>
            {isRequired ? (
              <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired, disable && styles.txtDisable]}>*</TextCM>
            ) : null}
          </View>
        ) : null}

        <View>
          <TextInput
            editable={!disable}
            ref={$refTextInput}
            style={[styles.styleDefault, styleCtnTextInput, disable && styles.txtDisable]}
            placeholder={i18n.t("CM.TextField.txtInputData")}
            placeholderTextColor={!disable ? Color.TextGray : styles.txtDisable.color}
            selectionColor={Color.White}
            value={value}
            onChangeText={onChangeText}
            underlineColorAndroid="transparent"
            {...more}
          />
          {!rightComponent && isFill && !disable && (
            <TouchableOpacity
              hitSlop={{ right: W(12), left: W(12), top: H(12), bottom: H(12) }}
              onPress={() => {
                onChangeText && onChangeText("");
              }}
              style={styles.ctnRightComponent}>
              <ICCloseCircle />
            </TouchableOpacity>
          )}
          {rightComponent ? <View style={styles.ctnRightComponent}>{rightComponent}</View> : null}
        </View>
        {isError ? (
          <View style={styles.ctnError}>
            <IcWarningRed style={styles.icError} />
            <TextCM style={styles.txtError}>{messageError}</TextCM>
          </View>
        ) : null}
      </View>
    );
  }
);

TextFieldCustom.displayName = "TextField";

export default TextFieldCustom;

import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TextInputProps, View } from "react-native";
import TextFieldCustom, { IProp } from "./TextFieldCustom";

type ITextField = {
  name: string;
  useHookForm?: boolean;
  onChangeText?: (value: string) => void;
};

const TextField: FC<TextInputProps & IProp & ITextField> = ({ name, useHookForm = true, onChangeText, ...more }) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return String(field?.message) || "";
    }
    return "";
  };

  const _onChangeText = (value: string, onChangeForm: (...event: any[]) => void): void => {
    if (onChangeText) onChangeText(value);
    onChangeForm(value);
  };

  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { onChange, value, ...moreField } }) => {
            return (
              <TextFieldCustom
                {...moreField}
                {...more}
                value={String(value)}
                onChangeText={(e: string) => _onChangeText(e, onChange)}
                messageError={getMsgError()}
              />
            );
          }}
        />
      ) : (
        <TextFieldCustom {...more} />
      )}
    </View>
  );
};

export default TextField;

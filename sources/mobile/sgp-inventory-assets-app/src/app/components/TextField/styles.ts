import { Platform, StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  styleDefault: {
    backgroundColor: Color.BodyTable,
    borderRadius: H(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: W(12),
    paddingRight: W(36),
    paddingVertical: Platform.select({ ios: H(12), android: H(10) }),
    color: Color.White,
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
    lineHeight: H(20),
  },
  txtDefaultTitle: {
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  txtTitle: {
    color: Color.LightGray,
    marginBottom: H(8),
  },
  txtTitleRequired: {
    marginLeft: W(2),
    color: Color.RedText,
  },
  ctnRightComponent: {
    position: "absolute",
    zIndex: 1,
    right: W(12),
    top: 0,
    bottom: 0,
    justifyContent: "center",
  },
  ctnError: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginTop: H(8),
  },
  txtError: {
    marginLeft: W(4),
    marginRight: W(8),
    color: Color.RedText,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  //disable
  txtDisable: {
    color: Color.TxtDisable,
  },
  icError: {
    marginTop: H(2),
  },
});

import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";

import styles from "./styles";

import { CModalViewer, CTAButtons, TextCM } from "@Components";

import { ICClose } from "@Assets/image";

const CAlertModal: FC<any> = ({
  open,
  title,
  icon,
  subButton,
  buttons = [
    {
      id: "confirm",
      text: "Xác nhận",
    },
  ],
  onCta = () => null,
  onClose = () => null,
  onModalHide = () => null,
}) => {
  return (
    <CModalViewer open={open} onModalHide={onModalHide} onBackdropPress={onClose}>
      <View style={styles.container}>
        <View style={styles.btnCloseWrapper}>
          <TouchableOpacity style={styles.btnClose} onPress={onClose}>
            <ICClose />
          </TouchableOpacity>
        </View>
        {!!icon && <View style={styles.icon}>{icon}</View>}
        {!!title && <TextCM style={styles.title}>{title}</TextCM>}
        <CTAButtons {...{ buttons, onCta }} style={{ paddingHorizontal: 0 }} />
        {subButton}
      </View>
    </CModalViewer>
  );
};

export default CAlertModal;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    margin: 0,
  },
  container: {
    margin: WR(24),
    padding: WR(16),
    paddingBottom: WR(24),
    borderRadius: WR(20),
    backgroundColor: "#262745",
  },
  btnCloseWrapper: {
    alignItems: "flex-end",
  },
  btnClose: {
    width: WR(24),
    height: WR(24),
  },
  icon: {
    width: WR(48),
    height: WR(48),
    marginBottom: WR(16),
    alignSelf: "center",
  },
  title: {
    textAlign: "center",
    paddingHorizontal: WR(24),
    marginBottom: WR(16),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
  },
});

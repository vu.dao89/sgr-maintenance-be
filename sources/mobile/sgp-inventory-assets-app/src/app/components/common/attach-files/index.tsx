import React, { FC, Fragment, useState } from "react";
import { PermissionsAndroid, Platform, TouchableOpacity, View } from "react-native";
import { Image as ImageCompressor } from "react-native-compressor";
import DocumentPicker from "react-native-document-picker-super";
import RNFS from "react-native-fs";
import ImagePicker from "react-native-image-crop-picker";
import { Rect, Svg } from "react-native-svg";

import { log } from "libs/react-native-x-framework/js/logger";

import { genRandomString, isArray } from "src/utils/Basic";
import { useIsMounted } from "src/utils/Core";
import { dayjsFormatDate } from "src/utils/DateTime";
import { formatBytes } from "src/utils/Format";

import styles from "./styles";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

import { CFileViewer, CSelect, TextCM } from "@Components";

import { ICUploadYellow } from "@Assets/image";
import { CameraOptions, launchCamera } from "react-native-image-picker-supper";

// const getImageSizes = (imagePath: any) => new Promise((resolve) => Image.getSize(imagePath, (width, height) => resolve({ width, height }))); // keep this to know, this method can get image's sizes :v

const getFileStat = (filePath: any): any =>
  new Promise(resolve =>
    RNFS.stat(filePath)
      .then(resolve)
      .catch(error => resolve({ error }))
  );

const getImageCompressed = (imagePath: any) =>
  new Promise(async resolve => {
    let resultPath = "";
    try {
      resultPath = await ImageCompressor.compress(imagePath, { compressionMethod: "auto" });
    } catch (error) {
      console.log(`🚀 Kds: getImageCompressed -> "${imagePath}" -> error:`, error);
    }
    resolve(resultPath);
  });

const image2asset = (i: any) => {
  let uri = i.path || i.uri;
  return {
    uri,
    type: i.mime || i.type,
    width: i.width,
    height: i.height,
    fileName: i.filename || uri.split("/").pop() || "", // ios only, using file path to get file name
    fileSize: i.fileSize || i.size,
  };
};

type Props = {
  files?: any;
  style?: any;
  simpleAddBtn?: any;
  simpleView?: any;
  viewerMaxWidth?: any;
  borderFile?: any;
  canRemove?: any;
  onChange?: any;
};

const SvgComponent = (props: any) => (
  <Svg width={"100%"} height={WR(42)} {...props}>
    <Rect
      x={0}
      y={0}
      rx={WR(10)}
      width={"100%"}
      height={"100%"}
      strokeWidth={"2"}
      stroke={Color.Yellow}
      strokeDasharray={"6,4"}
    />
  </Svg>
);

export interface CFile {
  type: string;
  uuid: string;
  date: string;
  size: string;
  name: string;
  source: string;
  file: {
    name: string;
    type: string;
    size: string;
    uri: string;
  };
  isRemote?: boolean;
  border?: boolean;
  width?: any;
  height?: any;
}

const CAttachFiles: FC<Props> = ({
  files = [],
  style,
  simpleAddBtn,
  viewerMaxWidth,
  simpleView,
  borderFile,
  canRemove,
  onChange,
}) => {
  const isMounted = useIsMounted();
  const [isSelectMethod, setSelectMethod] = useState(false);

  const canPickFile = !!onChange;

  const handleSelectedFiles = ({ assets, error, errorCode, errorMessage }: any) => {
    if (error || errorCode) {
      log(`🚀 Kds: handleSelectedFiles -> error:`, error || { errorCode, errorMessage });
    } else {
      log(`🚀 Kds: handleSelectedFiles -> assets`, assets);
      if (isArray(assets, true)) {
        let _files: any = [];

        assets.forEach((asset: any) => {
          try {
            let { type, uri, width, height, fileName, name = fileName, fileSize, size = fileSize } = asset;
            name = name.replace("rn_image_picker_lib_temp_", "");

            let source = Platform.OS === "ios" ? uri.replace("file://", "") : uri;
            let file: CFile = {
              type: type.startsWith("image") ? "img" : "file",
              uuid: genRandomString(),
              date: dayjsFormatDate(new Date()),
              size: formatBytes(size),
              name,
              source,
              file: {
                name,
                type,
                size,
                uri: source,
              },
              border: false,
            };

            if (borderFile) file.border = true;
            if (width !== undefined) Object.assign(file, { width, height });

            // if (file.type === "img") {
            //   let firstImgIdx = files.findIndex((i: any) => i.type === "img");
            //   if (firstImgIdx < 0) firstImgIdx = files.length - +!files.length;
            //   files.splice(firstImgIdx, 0, file);
            // } else {
            //   files.unshift(file);
            // }
            _files.push(file);
          } catch (error) {
            console.log(`🚀 Kds: handleSelectedFiles ->  error`, error);
          }
        });

        onChange(_files);
      }
    }
  };

  const handleCompressImages = async (images: any) => {
    if (!isMounted()) return;
    if (!isArray(images)) images = [images];
    let assets: any = [];
    for (const image of images) {
      console.log(`🚀 Kds: handleCompressImages -> image`, image);

      try {
        let imagePath = image.path || image.uri;
        let asset: any = image2asset({
          ...image,
          filename: image.filename || image.name || imagePath.split("/").pop() || "",
        });
        let resizedImagePath = await getImageCompressed(imagePath);

        console.log(`🚀 Kds: handleCompressImages -> resizedImagePath`, resizedImagePath);

        if (!isMounted()) return;

        if (resizedImagePath) {
          let resizedImageSize = ((await getFileStat(resizedImagePath)) || {}).size || 0;
          if (!isMounted()) return;
          Object.assign(asset, {
            path: resizedImagePath,
            size: resizedImageSize,
          });
        }
        assets.push(asset);
      } catch (error) {
        console.log(`🚀 Kds: handleCompressImages -> image -> error `, image, error);
      }
    }
    handleSelectedFiles({ assets });
  };

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
        title: "Camera Permission",
        message: "Inventory Assets needs access to your camera to upload attactment",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
        chooseFromCamera();
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const chooseFromCamera = () => {
    const options: CameraOptions = {
      mediaType: "photo",
      cameraType: "back",
    };

    launchCamera(options, response => {
      handleCompressImages(response.assets || []);
    });
  };

  const handleSelectFileMethod = async ({ value: fileMethod }: any) => {
    switch (fileMethod) {
      case "select-image":
        {
          if (Platform.OS === "ios") {
            let pickerConfig: any = {
              multiple: true,
              mediaType: "photo",
              forceJpg: true,
            };
            let images = await ImagePicker.openPicker(pickerConfig);
            handleCompressImages(images);
          } else {
            DocumentPicker.pickMultiple({
              type: [DocumentPicker.types.images],
            })
              .then((images: any) => handleCompressImages(images))
              .catch((error: any) => handleSelectedFiles({ error }));
          }
        }
        break;
      case "take-picture":
        {
          if (Platform.OS === "ios") {
            let pickerConfig: any = {
              cropping: true,
              avoidEmptySpaceAroundImage: false,
              freeStyleCropEnabled: true,
              forceJpg: true,
            };
            let images = await ImagePicker.openCamera(pickerConfig);
            handleCompressImages(images);
          } else {
            requestCameraPermission();
          }
        }
        break;
      case "select-file":
        {
          DocumentPicker.pickMultiple({
            type: [
              DocumentPicker.types.images,
              DocumentPicker.types.doc,
              DocumentPicker.types.docx,
              DocumentPicker.types.pdf,
              DocumentPicker.types.ppt,
              DocumentPicker.types.pptx,
              DocumentPicker.types.xls,
              DocumentPicker.types.xlsx,
            ],
          })
            .then((assets: any) => handleSelectedFiles({ assets }))
            .catch((error: any) => handleSelectedFiles({ error }));
        }
        break;
      default:
        break;
    }
  };

  const handleRemoveAttach = (index: any) => () => {
    files.splice(index, 1);
    onChange(files);
  };

  return (
    <View style={[styles.container, style]}>
      {canPickFile && (
        <TouchableOpacity
          style={[styles.selectFileBtnWrapper, { height: WR(simpleAddBtn ? 20 : 44) }]}
          onPress={() => setSelectMethod(true)}>
          {simpleAddBtn ? (
            <TextCM style={[styles.selectFileBtnTxt, { fontSize: FontSize.FontSmaller }]}>+ Đính kèm tài liệu</TextCM>
          ) : (
            <Fragment>
              <SvgComponent />
              <View style={styles.selectFileBtnContainer}>
                <ICUploadYellow />
                <TextCM style={[styles.selectFileBtnTxt, { marginLeft: WR(8), fontSize: FontSize.FontMedium }]}>
                  Đính kèm tài liệu
                </TextCM>
              </View>
            </Fragment>
          )}
        </TouchableOpacity>
      )}

      <CFileViewer
        {...{ files, simpleView }}
        maxWidth={viewerMaxWidth}
        style={{ marginTop: WR(24) }}
        onRemove={canRemove ? handleRemoveAttach : null}
      />

      <CSelect
        noInput
        isOpen={isSelectMethod}
        menus={[
          {
            value: "select-image",
            label: "Chọn hình ảnh",
          },
          {
            value: "take-picture",
            label: "Chụp hình ảnh",
          },
          {
            value: "select-file",
            label: "Chọn tập tin",
          },
        ]}
        onSelect={handleSelectFileMethod}
        onClose={() => setSelectMethod(false)}
      />
    </View>
  );
};

export default CAttachFiles;

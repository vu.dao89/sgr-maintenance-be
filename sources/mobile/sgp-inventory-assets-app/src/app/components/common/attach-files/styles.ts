import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    width: "100%",
  },
  selectFileBtnWrapper: {
    width: "100%",
  },
  selectFileBtnContainer: {
    position: "absolute",
    zIndex: 1,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  selectFileBtnTxt: {
    color: Color.Yellow,
  },
});

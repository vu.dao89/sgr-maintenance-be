import React, { FC } from "react";

import { TouchableOpacity, Keyboard } from "react-native";

import styles from "./styles";

import TextCM from "../../TextCM";

import { ICChecked, ICUncheck } from "@Assets/image";

type Props = {
  label?: any;
  checked?: any;
  styles?: any;
  onChange?: any;
};

const CCheckBox: FC<Props> = ({ label, checked, styles: _styles = {}, onChange = () => null }) => {
  return (
    <TouchableOpacity
      style={[styles.wrapper, _styles.wrapper]}
      onPress={() => {
        Keyboard.dismiss();
        onChange(!checked);
      }}>
      {checked ? <ICChecked /> : <ICUncheck />}
      <TextCM style={[styles.label, _styles.label]}>{label || ""}</TextCM>
    </TouchableOpacity>
  );
};

export default CCheckBox;

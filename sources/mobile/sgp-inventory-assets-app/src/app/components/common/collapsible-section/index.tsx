import React, { FC, useState } from "react";
import Collapsible from "react-native-collapsible";

import { TouchableOpacity, View } from "react-native";

import styles from "./styles";

import { CInfoViewer, CSelect, TextCM } from "@Components";

import { ICCollapseOff, ICCollapseOn, ICMenu } from "@Assets/image";

import { widthResponsive as WR } from "@Constants";
import { isEmpty } from "lodash";

type Props = {
  children?: any;
  title?: any;
  subtitle?: any;
  collapsible?: any;
  borderBg?: any;
  styles?: any;
  menus?: any;
  infos?: any;
  infoRow?: any;
  onMenu?: any;
  onMenuHide?: any;
};

const CCollapsibleSection: FC<Props> = ({
  children,
  title,
  subtitle = "",
  menus,
  infos = [],
  infoRow,
  collapsible = true,
  borderBg,
  styles: _styles = {},
  onMenu = () => null,
}) => {
  const [isCollapsed, setCollapsed] = useState(false);
  const [isShowMenu, setShowMenu] = useState(false);

  let isShowHeader = !!(collapsible || title);
  let headerNode;
  if (isShowHeader) {
    let HeaderRenderer = collapsible ? TouchableOpacity : View;
    headerNode = (
      // @ts-ignore
      <HeaderRenderer
        style={styles.headerWrapper}
        {...(collapsible && {
          onPress: () => setCollapsed(!isCollapsed),
        })}>
        <View style={[styles.headerContent, !!menus && { marginRight: WR(24) }]}>
          {!!collapsible && (isCollapsed ? <ICCollapseOn /> : <ICCollapseOff />)}
          <TextCM style={[styles.headerTitle, collapsible && styles.headerAlign]} numberOfLines={1}>
            {title || ""}
          </TextCM>
          {!isEmpty(subtitle) && <TextCM style={styles.headerSubTitle}>{subtitle}</TextCM>}
        </View>
        {!!menus && (
          <CSelect noInput isOpen={isShowMenu} menus={menus} onSelect={onMenu} onClose={() => setShowMenu(false)}>
            <TouchableOpacity style={styles.headerOptionBtn} onPress={() => setShowMenu(true)}>
              <ICMenu />
            </TouchableOpacity>
          </CSelect>
        )}
      </HeaderRenderer>
    );
  }

  return (
    <View style={[styles.wrapper, _styles.wrapper, borderBg && styles.borderWrapper]}>
      {headerNode}
      {/* @ts-ignore */}
      <Collapsible
        collapsed={isCollapsed}
        style={[styles.container, !isShowHeader && styles.containerAlign, _styles.container]}>
        <CInfoViewer {...{ infos, infoRow }} />
        {children}
      </Collapsible>
    </View>
  );
};

export default CCollapsibleSection;

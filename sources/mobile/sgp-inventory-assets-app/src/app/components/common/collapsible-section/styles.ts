import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    marginTop: WR(20),
    borderRadius: WR(20),
    backgroundColor: "#262745",
  },
  borderWrapper: {
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "#ffffff33",
  },
  container: {
    paddingHorizontal: WR(20),
    paddingBottom: WR(8),
  },
  containerAlign: {
    paddingTop: WR(28),
  },
  headerWrapper: {
    marginVertical: -WR(5),
    padding: WR(20),
    flexDirection: "row",
  },
  headerContent: {
    flex: 1,
    marginHorizontal: -WR(2),
    marginRight: WR(10),
    flexDirection: "row",
    alignItems: "center",
  },
  headerTitle: {
    flex: 1,
    color: Color.White,
    fontWeight: "600",
  },
  headerSubTitle: {
    color: Color.White,
    fontWeight: "600",
  },
  headerAlign: {
    marginLeft: WR(8),
  },
  headerOptionBtn: {
    position: "absolute",
    top: WR(16),
    right: WR(12),
    width: WR(32),
    height: WR(32),
    alignItems: "center",
    justifyContent: "center",
  },
});

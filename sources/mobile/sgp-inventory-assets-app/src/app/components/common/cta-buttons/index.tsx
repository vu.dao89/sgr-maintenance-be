import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Shadow } from "react-native-shadow-2";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import TextCM from "../../TextCM";

// plus WR(44 + 24) to padding bottom of above content when using isSticky

const CTAButtons: FC<any> = ({ style, isSticky, buttons, onCta }) => {
  const saInsets = useSafeAreaInsets();
  if (!buttons) return null;
  let btnNodes = (
    <View
      style={[
        styles.wrapper,
        !!isSticky && [styles.stickyContainer, { paddingBottom: saInsets.bottom || WR(12) }],
        style,
      ]}>
      {buttons.map((button: any, index: any) => {
        let { text, disabled, onPress, isSub, isBlueStyle } = button;
        return (
          <TouchableOpacity
            key={index}
            disabled={disabled}
            style={[
              styles.ctaButton,
              isSub && styles.subButton,
              !!index && { marginLeft: WR(12) },
              isBlueStyle && { backgroundColor: "#4C8BFF" },
            ]}
            onPress={onPress || (() => onCta(button))}>
            <TextCM
              style={[
                styles.ctaButtonTxt,
                isSub && styles.subButtonTxt,
                !isSub && styles.buttonTxt,
                isBlueStyle && { color: Color.White },
              ]}>
              {text}
            </TextCM>
          </TouchableOpacity>
        );
      })}
    </View>
  );
  if (isSticky) {
    return (
      <View style={styles.stickyWrapper}>
        <Shadow distance={16} startColor={"#00000040"} offset={[3, 4]}>
          {btnNodes}
        </Shadow>
      </View>
    );
  }
  return btnNodes;
};

export default CTAButtons;

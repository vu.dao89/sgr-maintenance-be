import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    paddingHorizontal: WR(24),
  },
  stickyWrapper: {
    position: "absolute",
    zIndex: 1,
    bottom: 0,
    left: 0,
    right: 0,
  },
  stickyContainer: {
    width: "100%",
    backgroundColor: "#262745",
    paddingVertical: WR(12),
    borderTopLeftRadius: WR(20),
    borderTopRightRadius: WR(20),
  },
  ctaButton: {
    flex: 1,
    height: WR(44),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(12),
    backgroundColor: Color.Yellow,
  },
  subButton: {
    backgroundColor: "#585858",
  },
  ctaButtonTxt: {
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
  },
  subButtonTxt: {
    color: "#C6C6C6",
  },
  buttonTxt: {
    color: "#222222",
  },
});

import React, { FC, forwardRef, Fragment, useImperativeHandle, useRef, useState } from "react";

import { Keyboard, TouchableOpacity, View } from "react-native";

import DateTimePickerModal from "react-native-modal-datetime-picker";

import i18n from "@I18n";

import { DateTimeUtil } from "src/utils";

import { Color } from "@Constants";

import styles from "./styles";

import TextCM from "../../TextCM";

import { ICCalendar } from "@Assets/image";

type Props = {
  isRequired?: any;
  isError?: any;
  isNow?: any;
  isRange?: any;
  isFilled?: any;
  noIndicator?: any;
  label?: any;
  value?: any;
  values?: any;
  plh?: any;
  format?: any;
  styles?: any;
  mode?: "date" | "time" | "datetime" | any;
  maximumDate?: any;
  minimumDate?: any;
  onChange?: any;
  onOpen?: any;
  onClose?: any;
};

const formats: any = {
  date: "dmy",
  datetime: "DD/MM/YYYY - HH:mm:ss",
};

const CDatePicker: FC<Props> = forwardRef(
  (
    {
      isRequired,
      isError,
      isNow,
      isRange,
      isFilled,
      noIndicator,
      label,
      plh,
      value,
      values,
      mode = "date",
      maximumDate,
      minimumDate,
      styles: _styles = {},
      onChange = () => null,
    },
    ref: any
  ) => {
    const [isOpen, setOpen] = useState(false);

    const pickingRangeType: any = useRef("");
    const [tempDate, setTempDate]: any = useState(null);

    useImperativeHandle(ref, () => ({ open: (status: any) => setOpen(status) }));

    let format = formats[mode];

    let labelNode = !!label && (
      <View style={styles.labelWrapper}>
        <TextCM style={[styles.labelTxt, isError && { color: Color.Red }]} numberOfLines={1}>
          {label}
        </TextCM>
        {!!isRequired && <TextCM style={styles.labelRequired}>*</TextCM>}
      </View>
    );

    let arrowNode = (
      <View style={styles.arrow}>
        <ICCalendar />
      </View>
    );

    let indicatorNode = !noIndicator && !isFilled && (
      <View style={[styles.indicator, { backgroundColor: isOpen ? Color.White : "#414042" }]} />
    );

    let pickerNode;
    let pickerContainerStyles = [styles.container, isFilled && styles.containerFilled, _styles.container];
    if (isRange) {
      const [fromDate, toDate] = values || [];
      pickerNode = (
        <View style={pickerContainerStyles}>
          <View style={styles.rangeContent}>
            <TouchableOpacity
              style={styles.rangeDate}
              onPress={() => {
                Keyboard.dismiss();
                pickingRangeType.current = "from";
                setTempDate(fromDate || new Date());
                setOpen(true);
              }}>
              <TextCM style={[{ color: fromDate ? Color.White : "#BDBDBD" }]} numberOfLines={1}>
                {fromDate ? DateTimeUtil.formatDate(fromDate, format) : plh || "Từ ngày"}
              </TextCM>
            </TouchableOpacity>
            <TextCM style={styles.rangeMiddle}>-</TextCM>
            <TouchableOpacity
              style={styles.rangeDate}
              onPress={() => {
                Keyboard.dismiss();
                pickingRangeType.current = "to";
                setTempDate(toDate || new Date());
                setOpen(true);
              }}>
              <TextCM style={[{ color: toDate ? Color.White : "#BDBDBD" }]} numberOfLines={1}>
                {toDate ? DateTimeUtil.formatDate(toDate, format) : plh || "Đến ngày"}
              </TextCM>
            </TouchableOpacity>
          </View>
          {arrowNode}
        </View>
      );
    } else {
      pickerNode = (
        <TouchableOpacity
          style={pickerContainerStyles}
          onPress={() => {
            Keyboard.dismiss();
            setTempDate(value || new Date());
            setOpen(true);
          }}>
          {isNow ? (
            <TextCM style={[styles.input, { color: value ? Color.White : "#BDBDBD" }]} numberOfLines={1}>
              {DateTimeUtil.formatDate(new Date(), format)}
            </TextCM>
          ) : (
            <TextCM style={[styles.input, { color: value ? Color.White : "#BDBDBD" }]} numberOfLines={1}>
              {value ? DateTimeUtil.formatDate(value, format) : plh || i18n.t("CM.select")}
            </TextCM>
          )}
          {arrowNode}
        </TouchableOpacity>
      );
    }

    const onConfirm = (date: any) => {
      if (isRange) {
        let nextDates = [...values];
        let isPickingTo = pickingRangeType.current === "to";
        nextDates[+isPickingTo] = date;
        let fromTs = +(nextDates[0] || null);
        let toTs = +(nextDates[1] || null);
        if (isPickingTo) {
          if (toTs < fromTs) {
            nextDates[0] = new Date(toTs);
          }
        } else {
          if (fromTs > toTs) {
            nextDates[1] = new Date(fromTs);
          }
        }
        console.log(nextDates, "nextDates");
        onChange(nextDates);
      } else {
        onChange(date);
      }
      setOpen(false);
    };

    return (
      <Fragment>
        <View style={[styles.wrapper, _styles.wrapper]}>
          {labelNode}
          {pickerNode}
          {indicatorNode}
        </View>
        <DateTimePickerModal
          date={tempDate || new Date()}
          isVisible={isOpen}
          mode={mode}
          onConfirm={onConfirm}
          onCancel={() => setOpen(false)}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
        />
      </Fragment>
    );
  }
);

export default CDatePicker;

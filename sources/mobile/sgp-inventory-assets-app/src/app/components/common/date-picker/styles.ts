import { StyleSheet } from "react-native";

import { Font, Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    marginBottom: WR(20),
  },
  container: {
    height: WR(36),
    flexDirection: "row",
    alignItems: "center",
  },
  containerFilled: {
    height: WR(44),
    backgroundColor: "#33345F",
    paddingHorizontal: WR(12),
    borderRadius: WR(12),
  },
  rangeContent: {
    flex: 1,
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
  },
  rangeDate: {
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
  rangeMiddle: {
    color: "#BDBDBD",
    marginHorizontal: WR(4),
  },
  labelWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  labelTxt: {
    color: "#8D9298",
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontTiniest,
  },
  labelRequired: {
    marginLeft: 4,
    color: Color.Red,
    fontSize: FontSize.FontTiniest,
  },
  input: {
    flex: 1,
  },
  arrow: {
    marginLeft: WR(8),
  },
  indicator: {
    width: "100%",
    height: WR(1),
  },
  pickerContainer: {
    margin: WR(24),
    padding: WR(16),
    borderRadius: WR(20),
    backgroundColor: Color.White,
  },
});

import React, { FC } from "react";
import { View } from "react-native";
import { ICAssetsEmpty } from "../../../../assets/image";
import i18n from "../../../../i18n";
import TextCM from "../../TextCM";

import styles from "./styles";

type Props = {
  noPermission?: boolean;
  style?: any;
};

const CEmptyState: FC<Props> = ({ noPermission = false, style }) => {
  return (
    <View style={[styles.ctnNotFound, style]}>
      <ICAssetsEmpty />
      <TextCM style={styles.txtNotFound}>{noPermission ? i18n.t("CM.noPermission") : i18n.t("CM.noData")}</TextCM>
    </View>
  );
};

export default CEmptyState;

import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR } from "@Constants";

export default StyleSheet.create({
  ctnNotFound: {
    marginTop: HR(80),
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  txtNotFound: {
    marginTop: HR(12),
    textAlign: "center",
    color: Color.TextGray,
    alignSelf: "center",
    fontSize: FontSize.FontSmaller,
  },
});

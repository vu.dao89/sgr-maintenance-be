import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { Image, Linking, TouchableOpacity, View, SafeAreaView, Platform } from "react-native";
import ImageViewer from 'react-native-image-zoom-viewer';
import RNFetchBlob from "rn-fetch-blob";
import CameraRoll from '@react-native-community/cameraroll';
import Toast, { useToast } from "react-native-toast-notifications";

import { useIsMounted } from "src/utils/Core";
import { genRandomString, isArray } from "src/utils/Basic";

import styles from "./styles";

import { ScreenWidth as SW, widthResponsive as WR } from "@Constants";

import { ImageCache, TextCM, CModalViewer, CSpinkit } from "@Components";

import { FICpdf, ICBlankImage, ICClose, ICCloseY, ICCloudArrowDown } from "@Assets/image";

const defaultMaxWidth = SW - WR(48);
const HIT_SLOP = { top: 16, left: 16, bottom: 16, right: 16 };

const rnfbDownloadFile = (fileUrl: any, toBase64?: any) => new Promise(async (resolve) => {
  try {
    let data = await RNFetchBlob.config({ fileCache: true }).fetch('GET', fileUrl);
    resolve({ data });
  } catch (error) {
    resolve({ error });
  }
});
const rnfbIsExists = (filePath: any) => new Promise((resolve) => {
  RNFetchBlob.fs.exists(filePath).then((status) => resolve(status)).catch(() => resolve(false));
});
const rnfbWriteFile = (filePath: any, contents: any, options?: any) => new Promise(async (resolve) => {
  let isExist = await rnfbIsExists(filePath);
  if (isExist) {
    let filePaths = filePath.split('.');
    filePaths[filePaths.length - 2] += `_${+new Date()}`;
    filePath = filePaths.join('.');
  }
  RNFetchBlob.fs.writeFile(filePath, contents, options).then(() => resolve(true)).catch(() => resolve(false));
});

const ImageFileViewer = ({ viewWidth, source, children, onView }: any) => {
  return (
    <TouchableOpacity
      key={genRandomString()}
      style={[styles.imageWrapper, { width: viewWidth, height: viewWidth + WR(56) }]}
      onPress={onView}
    >
      <View key={genRandomString()} style={[styles.imageContainer, { width: viewWidth, height: viewWidth }]}>
        <ImageCache
          key={genRandomString()}
          imageDefault={ICBlankImage}
          uri={source}
          style={styles.imageContent}
          isDownLoad={false}
        />
      </View>
      {children}
    </TouchableOpacity>
  );
};

const CommonFileViewer = ({ viewWidth, index, children, previewNode, simpleView, border, removeBtnNode, isDownloading, onDownload }: any) => {
  return (
    <View>
      <TouchableOpacity
        style={[
          { width: viewWidth },
          border && { borderWidth: 1, borderColor: "gray" },
          simpleView ? { marginTop: index ? WR(20) : 0 } : styles.fileWrapper,
        ]}
        onPress={onDownload}
      >
        <View style={styles.fileContainer}>
          {previewNode || <FICpdf />}
          {children}
        </View>
        {removeBtnNode}
      </TouchableOpacity>
      {isDownloading === "file" &&
        <View style={styles.imgDownloadOverlay}>
          <CSpinkit />
        </View>
      }
    </View>
  );
};

const ImageViewHeader = ({ onClose, onDownload }: any) => {
  return (
    <SafeAreaView style={styles.imgViewHeaderRoot}>
      <TouchableOpacity style={styles.imgViewHeaderBtn} onPress={onClose} hitSlop={HIT_SLOP}>
        <ICCloseY />
      </TouchableOpacity>
      <TouchableOpacity style={styles.imgViewHeaderBtn} onPress={onDownload} hitSlop={HIT_SLOP}>
        <ICCloudArrowDown />
      </TouchableOpacity>
    </SafeAreaView>
  )
};

const CFileViewer: FC<any> = ({ maxWidth = defaultMaxWidth, style, simpleView, files, onRemove }) => {

  const fileWidths: any = useState({
    img: (maxWidth - WR(8 * 2)) / 2,
    file: maxWidth,
  })[0];

  const isMounted = useIsMounted();
  const toast: any = useToast();
  const imgToastRef = useRef({});

  const [isViewImg, setViewImgStatus] = useState(false);
  const [viewImgIdx, setViewImgIdx] = useState(0);
  const [viewImages, setViewImages] = useState([]);

  const [isDownloading, setDownloading] = useState('');

  if (!isArray(files, true)) return null;

  const handleViewImages = (index: any) => () => {
    let image = files[index];
    let images = files.filter((i: any) => i.type === "img").map((i: any) => ({ ...i, url: i.source }));
    setViewImages(images);
    setViewImgIdx(images.findIndex((i: any) => i.uuid === image.uuid));
    setViewImgStatus(true);
  };

  const handleDownloadFile = ({ isRemote, type, source, url, name }: any) => async () => {
    if (!isRemote || isDownloading) return;
    let tempPath;
    let isImg = type === "img";
    let fileUrl = source || url;
    // console.log(`🚀 Kds: handleDownloadFile -> fileUrl`, fileUrl);
    let fileType = isImg ? "hình ảnh" : "tập tin";
    let _toast = isImg ? imgToastRef.current : toast;
    setDownloading(type);
    try {
      let { data: imageContent, error: downloadErr }: any = await rnfbDownloadFile(fileUrl, !isImg);
      // console.log(`🚀 Kds: handleDownloadFile -> imageContent`, imageContent);
      if (!isMounted()) return;
      if (downloadErr) throw downloadErr;
      tempPath = imageContent.path();
      if (isImg) {
        await CameraRoll.save(imageContent.data, { type: "photo" });
      } else {
        let filePath = RNFetchBlob.fs.dirs[Platform.OS === "ios" ? "DocumentDir" : "DownloadDir"] + '/' + name;
        // console.log(`🚀 Kds: handleDownloadFile -> filePath`, filePath);
        let imgBase64 = await imageContent.readFile("base64");
        // console.log(`🚀 Kds: handleDownloadFile -> imgBase64`, imgBase64);
        await rnfbWriteFile(filePath, imgBase64, "base64");
      }
      if (!isMounted()) return;
      _toast.show(`Lưu ${fileType} thành công!`);
    } catch (error) {
      console.log(`🚀 Kds: handleDownloadFile -> error`, error);
      _toast.show(`Lưu ${fileType} thất bại!`);
    }
    if (tempPath) RNFetchBlob.fs.unlink(tempPath);
    setDownloading("");
  };

  return (
    <View style={[styles.wrapper, { marginLeft: simpleView ? 0 : -WR(8) }, style]}>
      {files.map((file: any, index: any) => {
        const { uuid, type, name, size, date, source } = file;
        let attachInfoNode = (
          <View
            key={`info_${uuid}`}
            style={[styles.attachInfo, { marginLeft: type === "img" && !simpleView ? 0 : WR(12) }]}>
            <TextCM key={`name_${uuid}`} style={styles.attachName} numberOfLines={1}>
              {name}
            </TextCM>
            <TextCM key={`date_${uuid}`} style={styles.attachDesc} numberOfLines={1}>
              {size} - {date}
            </TextCM>
          </View>
        );
        let removeBtnNode = !!onRemove && (
          <TouchableOpacity
            key={`remove_${uuid}`}
            style={[styles.btnRemove, type === "file" && styles.btnBlank]}
            onPress={onRemove(index)}>
            <ICClose key={`close_${uuid}`} width={WR(16)} />
          </TouchableOpacity>
        );
        let viewerProps = {
          ...file,
          isDownloading,
          key: uuid,
          index,
          simpleView,
          removeBtnNode,
          viewWidth: fileWidths[type],
          onView: handleViewImages(index),
          onDownload: handleDownloadFile(file),
        };
        switch (type) {
          case "img": {
            if (simpleView) {
              return (
                <CommonFileViewer
                  {...viewerProps}
                  previewNode={
                    <View style={styles.imagePreview}>
                      <Image source={{ uri: source }} style={styles.imageContent} />
                    </View>
                  }>
                  {attachInfoNode}
                </CommonFileViewer>
              );
            }
            return (
              <ImageFileViewer {...viewerProps}>
                {attachInfoNode}
                {removeBtnNode}
              </ImageFileViewer>
            );
          }
          case "file": {
            return (
              <CommonFileViewer {...viewerProps}>
                {attachInfoNode}
              </CommonFileViewer>
            );
          }
          default:
            return;
        }
      })}
      <CModalViewer
        open={isViewImg}
        placement={'fullscr'}
      >
        <ImageViewer
          index={viewImgIdx}
          imageUrls={viewImages}
          // @ts-ignore
          renderHeader={(currentIndex: any) => {
            let curImage: any = viewImages[currentIndex];
            if (!curImage.isRemote) return null;
            return (
              <ImageViewHeader
                onClose={() => setViewImgStatus(false)}
                onDownload={handleDownloadFile(curImage)}
              />
            )
          }}
          renderIndicator={() => <Fragment></Fragment>}
          loadingRender={() => <CSpinkit />}
        />
        {isDownloading === "img" &&
          <View style={[styles.imgDownloadOverlay, { backgroundColor: "#00000077" }]}>
            <CSpinkit />
          </View>
        }
        <Toast ref={(ref: any) => (imgToastRef.current = ref)} />
      </CModalViewer>
    </View>
  );
};

export default CFileViewer;
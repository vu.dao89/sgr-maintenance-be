import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  imageWrapper: {
    marginLeft: WR(8),
    marginBottom: WR(8),
  },
  imageContainer: {
    marginBottom: WR(8),
  },
  imagePreview: {
    width: WR(56),
    height: WR(56),
  },
  imageContent: {
    width: "100%",
    height: "100%",
    borderRadius: WR(16),
  },
  attachInfo: {
    flex: 1,
    // overflow: "hidden",
  },
  attachName: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
    height: WR(20),
  },
  attachDesc: {
    color: "#A5A5A5",
    fontSize: FontSize.FontTiny,
    height: WR(20),
  },
  fileWrapper: {
    marginLeft: WR(8),
    marginBottom: WR(20),
    padding: WR(20),
    borderRadius: WR(16),
    backgroundColor: "#262745",
  },
  fileContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnRemove: {
    width: WR(24),
    height: WR(24),
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    position: "absolute",
    top: WR(8),
    right: WR(8),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(6),
  },
  btnBlank: {
    top: WR(6),
    right: WR(6),
    backgroundColor: "transparent",
  },
  imgViewHeaderRoot: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  imgViewHeaderBtn: {
    marginTop: WR(4),
    marginHorizontal: WR(12),
    width: WR(40),
    height: WR(40),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(22),
    backgroundColor: "#00000077",
  },
  imgDownloadOverlay: {
    zIndex: 2,
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems: "center",
    justifyContent: "center",
  },
});

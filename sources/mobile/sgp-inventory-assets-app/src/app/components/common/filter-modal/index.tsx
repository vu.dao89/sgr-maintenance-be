import { cloneDeep, isFunction, throttle } from "lodash";
import React, { FC, forwardRef, Fragment, useEffect, useRef, useState, useImperativeHandle } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import i18n from "@I18n";

import { getPagination, PAGE_SIZE } from "@Services/httpClient";

import { genRandomString, isArray, removeViChar } from "src/utils/Basic";
import { isCloseToBottom, useStateLazy } from "src/utils/Core";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import {
  CCollapsibleSection,
  CDatePicker,
  CModalViewer,
  CRadioSelect,
  CRefreshControl,
  CSearchBar,
  CSpinkit,
  CTAButtons,
  CNavbar,
  TextCM,
} from "@Components";

import { ICArrowRight, ICChecked, ICCloseY, ICFilterDefault, ICUncheck } from "@Assets/image";

type PropsType = {
  ref?: any;
  title: string;
  open?: any;
  options: any[];
  selecteds: any;
  showToggle?: any;
  onClose?: any;
  onChange?: any;
};

const mappingFilterSelections = (selections: any = []) =>
  selections.map((i: any) => ({ ...i, labelUnsigned: removeViChar(i.label) }));

const CFilterModal: FC<PropsType> = forwardRef(({ title, open, options, selecteds, showToggle, onClose, onChange }, ref) => {
  const saInsets = useSafeAreaInsets();
  const getSelectionReqIds: any = useRef({});
  const isInitFilter: any = useRef(false);

  const [toggleOpen, setToggleOpen]: any = useState(false);

  const [_selecteds, setSelecteds]: any = useState({ ...selecteds });
  const [_selections, setSelections]: any = useStateLazy({});
  const [fetchings, setFetchings]: any = useStateLazy({});
  const [paginations, setPaginations]: any = useStateLazy({});

  const [showingMultiSelector, setShowingMultiSelector]: any = useStateLazy(null);
  const [keyword, setKeyword]: any = useState("");
  const [searchResults, setSearchResults]: any = useState([]);

  useImperativeHandle(ref, () => ({
    setSelected: (nextSelecteds: any) => setSelecteds(nextSelecteds),
  }));

  const filterLength = Object.keys(_selecteds).length;
  const isApplyFilter = !!filterLength;

  const {
    id: showingSelectorId,
    type: showingSelectorType,
    onSearch: showingSelectorOnSearch,
  } = showingMultiSelector || {};
  let isShowingSelectorFetching = !!fetchings[showingSelectorId];
  let showingSelectorSelections =
    (keyword && searchResults) || _selections[showingSelectorId] || showingMultiSelector?.selections || [];
  let showingSelectorPagination = paginations[showingSelectorId + (keyword ? "_search" : "")] || {};

  useEffect(() => {
    if (!isInitFilter.current) {
      isInitFilter.current = true;
      let initialSelections: any = {};
      options.forEach(i => {
        if (!i.onSearch) initialSelections[i.id] = mappingFilterSelections(i.selections);
      });
      setSelections(initialSelections);
    }
  }, [open, toggleOpen]);

  const handleSearchSelections = (_keyword: any = "", _page: any = 0) => {
    try {
      if (!_keyword && isArray(showingSelectorSelections, true)) return;
      let reqId = genRandomString();
      let searchId = showingSelectorId + (_keyword ? "_search" : "");
      getSelectionReqIds.current[searchId] = reqId;
      setFetchings((prevState: any) => ({ ...prevState, [searchId]: true }));
      if (_page === 0) {
        setPaginations((prevState: any) => ({ ...prevState, [searchId]: getPagination() }));
        if (_keyword) {
          setSearchResults([]);
        } else {
          setSelections((prevState: any) => ({ ...prevState, [showingSelectorId]: [] }));
        }
      }
      throttle(
        (params: any) => {
          showingSelectorOnSearch(params, (pagination: any, selections: any) => {
            if (getSelectionReqIds.current[searchId] !== reqId) return;
            setFetchings((prevState: any) => ({ ...prevState, [searchId]: false }));
            setPaginations((prevState: any) => ({ ...prevState, [searchId]: getPagination(pagination) }));
            if (params.keyword) {
              setSearchResults((prevState: any) => [...prevState, ...selections]);
            } else {
              setSelections((prevState: any) => ({
                ...prevState,
                [showingSelectorId]: [...(prevState[showingSelectorId] || []), ...selections],
              }));
            }
          });
        },
        500,
        { trailing: false }
      )({ keyword: _keyword, page: _page, size: PAGE_SIZE });
    } catch (error) {
      console.log(`🚀 Kds: handleSearchSelections -> error`, error);
    }
  };

  const handleOnSearchChange = (_keyword: any) => {
    try {
      setKeyword(_keyword);
      let keywordUnsigned = removeViChar(_keyword);
      if (isFunction(showingSelectorOnSearch)) {
        handleSearchSelections(_keyword);
      } else {
        setSearchResults(
          _selections[showingSelectorId].filter(
            (i: any) =>
              (i.labelUnsigned || "").includes(keywordUnsigned) || String(i.value || "").includes(keywordUnsigned)
          )
        );
      }
    } catch (error) {
      console.log(`🚀 Kds: handleOnSearchChange -> error`, error);
    }
  };

  const handleSelectSelection = (_type: any, _id: any) => (_selection: any, _isSelected?: any) => {
    switch (_type) {
      case "single":
        if (_isSelected) {
          delete _selecteds[_id];
        } else {
          _selecteds[_id] = _selection.value;
        }
        break;
      case "datetime":
        _selecteds[_id] = _selection;
        break;
      default:
        if (_isSelected) {
          _selecteds[_id] = _selecteds[_id].filter((i: any) => i.value !== _selection.value);
          if (!_selecteds[_id].length) delete _selecteds[_id];
        } else {
          if (!_selecteds[_id]) _selecteds[_id] = [];
          _selecteds[_id].push(_selection);
        }
        break;
    }
    setSelecteds({ ..._selecteds });
  };

  const handleSelectFilterOption = async (option: any) => {
    setKeyword("");
    setSearchResults([]);
    setShowingMultiSelector(option);
    let reqId = genRandomString();
    let searchId = option.id;
    getSelectionReqIds.current[searchId] = reqId;
    let optionSelections = _selections[searchId];
    if (isFunction(option.onSearch) && !isArray(optionSelections, true)) {
      setFetchings((prevState: any) => ({ ...prevState, [searchId]: true }));
      if (!showingSelectorPagination)
        setPaginations((prevState: any) => ({ ...prevState, [searchId]: getPagination() }));
      throttle(
        (params: any) => {
          option.onSearch(params, (pagination: any, selections: any) => {
            if (getSelectionReqIds.current[searchId] !== reqId) return;
            setFetchings((prevState: any) => ({ ...prevState, [searchId]: false }));
            setPaginations((prevState: any) => ({ ...prevState, [searchId]: getPagination(pagination) }));
            setSelections((prevState: any) => ({
              ...prevState,
              [searchId]: [...(prevState[searchId] || []), ...selections],
            }));
          });
        },
        500,
        { trailing: false }
      )({ keyword: "", page: 0, size: PAGE_SIZE });
    }
  };

  const handleOnReachedEnd = (e: any) => {
    if (
      isCloseToBottom(e) &&
      !isShowingSelectorFetching &&
      showingMultiSelector &&
      showingSelectorOnSearch &&
      showingSelectorPagination.hasNext
    ) {
      handleSearchSelections(keyword, showingSelectorPagination.nextPage);
    }
  };

  const handleResetFilter = () => {
    let nextSelecteds: any = {};
    options.forEach(i => {
      if (i.isRequired && _selecteds[i.id]) {
        nextSelecteds[i.id] = _selecteds[i.id];
      }
    });
    setSelecteds(nextSelecteds);
  };

  /* START: main filter component */
  const _renderFilterHeader = () => {
    return (
      <CNavbar
        title={title}
        leftIcon={<ICCloseY />}
        onLeftPress={() => (showToggle ? setToggleOpen(false) : onClose(false))}
        rightBtnNode={
          isApplyFilter && (
            <TouchableOpacity style={[styles.navBtnRight, styles.navBtnResetBtn]} onPress={handleResetFilter}>
              <TextCM style={styles.navBtnResetTxt}>
                Đặt lại
              </TextCM>
            </TouchableOpacity>
          )
        }
      />
    );
  };

  const _renderFilterContainer = () => {
    return (
      <ScrollView
        contentContainerStyle={[styles.filterContainer, { paddingBottom: (saInsets.bottom || WR(18)) + WR(44 + 30) }]}>
        {options.map((option: any, index: any) => {
          const { id, type, label, selections, collapsible = false } = option;

          let optionNode, optionSelected = _selecteds[id];
          switch (type) {
            case "single":
              {
                optionNode = (
                  <CRadioSelect
                    options={selections}
                    selected={optionSelected}
                    onSelect={handleSelectSelection(type, id)}
                  />
                );
              }
              break;
            case "datetime":
              {
                optionNode = (
                  <CDatePicker
                    noIndicator
                    value={_selecteds[id]}
                    styles={{
                      container: styles.filterField,
                    }}
                    onChange={handleSelectSelection(type, id)}
                  />
                );
              }
              break;
            default:
              {
                let totalSelected = (optionSelected || []).length;
                optionNode = (
                  <TouchableOpacity
                    style={[styles.filterField, { marginBottom: WR(20) }]}
                    onPress={() => {
                      handleSelectFilterOption(option);
                    }}>
                    <View style={styles.filterSelection}>
                      {!!totalSelected ? (
                        <TextCM style={styles.selectorSelectedTxt}>
                          {optionSelected[0].label}
                          {totalSelected > 1 && ` (+${totalSelected - 1})`}
                        </TextCM>
                      ) : (
                        <TextCM style={styles.selectorPlhTxt}>Chọn</TextCM>
                      )}
                    </View>
                    <ICArrowRight />
                  </TouchableOpacity>
                );
              }
              break;
          }
          return (
            <CCollapsibleSection
              key={id}
              title={label}
              styles={!index && { wrapper: { marginTop: 0 } }}
              {...{ collapsible }}>
              {optionNode}
            </CCollapsibleSection>
          );
        })}
      </ScrollView>
    );
  };

  const _renderFilterFooter = () => {
    return (
      <CTAButtons
        isSticky
        buttons={[
          {
            text: "Áp dụng",
            onPress: () => {
              showToggle && setToggleOpen(false);
              onChange(_selecteds);
            },
          },
        ]}
      />
    );
  };
  /* END: main filter component */

  /* START: multi selector component */
  const _renderMultiSelectorHeader = () => {
    return (
      <CNavbar
        onLeftPress={() => setShowingMultiSelector(null)}
        titleNode={
          <CSearchBar
            style={styles.searchBar}
            keyword={keyword}
            onChange={handleOnSearchChange}
            onSearch={() => null}
          />
        }
      />
    );
  };

  const _renderMultiSelectorContainer = () => {
    let showingSelecteds = _selecteds[showingSelectorId] || [];
    let haveSelection = isArray(showingSelectorSelections, 1);
    return (
      <ScrollView
        scrollEventThrottle={50}
        refreshControl={<CRefreshControl onRefresh={() => handleSearchSelections(keyword)} />}
        contentContainerStyle={[
          styles.filterContainer,
          { paddingBottom: saInsets.bottom + WR(44 + 24 + 18) },
        ]}
        onScroll={handleOnReachedEnd}>
        {haveSelection
          ? showingSelectorSelections.map((selection: any, index: any) => {
            const { value, label } = selection;
            let isSelected = showingSelecteds.some((i: any) => i.value === value);
            return (
              <TouchableOpacity
                key={`${value}-${index}`}
                style={styles.selectorMultiItem}
                onPress={() => handleSelectSelection(showingSelectorType, showingSelectorId)(selection, isSelected)}>
                {isSelected ? <ICChecked /> : <ICUncheck />}
                <View
                  style={[
                    styles.selectorMultiContent,
                    showingSelectorSelections.length - 1 === index && { borderBottomWidth: 0 },
                  ]}>
                  <TextCM color={"White"}>{label}</TextCM>
                </View>
              </TouchableOpacity>
            );
          })
          : !isShowingSelectorFetching && <TextCM style={styles.selectorEmptyTxt}>{i18n.t("CM.noData")}</TextCM>}
        {isShowingSelectorFetching && (
          <View style={styles.selectionFetching}>
            <CSpinkit />
          </View>
        )}
      </ScrollView>
    );
  };

  const _renderMultiSelectorFooter = () => {
    return (
      <CTAButtons
        isSticky
        buttons={[
          {
            isSub: true,
            text: "Reset",
            onPress: () => {
              delete _selecteds[showingSelectorId];
              setSelecteds({ ..._selecteds });
            },
          },
          {
            text: "Chọn",
            onPress: () => setShowingMultiSelector(null),
          },
        ]}
      />
    );
  };

  const _renderMultiSelectorModal = () => {
    return (
      <CModalViewer open={showingMultiSelector} style={styles.filterWrapper}>
        {_renderMultiSelectorHeader()}
        {_renderMultiSelectorContainer()}
        {_renderMultiSelectorFooter()}
      </CModalViewer>
    );
  };
  /* END: multi selector component */

  let filterModalNode = (
    <CModalViewer open={open || toggleOpen} style={styles.filterWrapper}>
      {_renderFilterHeader()}
      {_renderFilterContainer()}
      {_renderFilterFooter()}
      {_renderMultiSelectorModal()}
    </CModalViewer>
  );
  if (showToggle) {
    return (
      <Fragment>
        <TouchableOpacity style={styles.navBtnRight} onPress={() => setToggleOpen(true)}>
          <ICFilterDefault />
          {isApplyFilter && (
            <View style={styles.applyFilterStatus}>
              <Text style={styles.searchLength}>{filterLength < 10 ? filterLength : "9+"}</Text>
            </View>
          )}
        </TouchableOpacity>
        {filterModalNode}
      </Fragment>
    );
  }
  return filterModalNode;
});

export default CFilterModal;

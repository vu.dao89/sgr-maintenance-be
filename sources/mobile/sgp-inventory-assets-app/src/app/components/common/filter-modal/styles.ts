import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  navBtnRight: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  navBtnResetBtn: {
    width: "auto",
    minWidth: "100%",
    marginRight: WR(20),
  },
  navBtnResetTxt: {
    color: Color.White,
    fontWeight: "500",
    fontSize: FontSize.FontMedium,
  },
  applyFilterStatus: {
    top: HR(10),
    right: HR(8),
    position: "absolute",
    paddingLeft: WR(3),
    paddingRight: WR(3),
    borderRadius: WR(4),
    backgroundColor: Color.Red,
  },
  searchLength: {
    color: Color.Yellow,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  searchBar: {
    flex: 1,
    height: "100%",
  },
  filterWrapper: {
    backgroundColor: Color.Background,
  },
  filterContainer: {
    paddingTop: WR(12),
    padding: WR(24),
  },
  filterField: {
    height: WR(44),
    paddingHorizontal: WR(12),
    flexDirection: "row",
    alignItems: "center",
    borderRadius: WR(12),
    backgroundColor: "#33345F",
  },
  filterSelection: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  selectorSelectedTxt: {
    color: Color.Yellow,
  },
  selectorPlhTxt: {
    color: "#BDBDBD",
  },
  selectorEmptyTxt: {
    margin: WR(12),
    color: "#BDBDBD",
    textAlign: "center",
    fontSize: FontSize.FontMedium,
  },
  selectorMultiItem: {
    minHeight: WR(44),
    flexDirection: "row",
    alignItems: "center",
  },
  selectorMultiContent: {
    flex: 1,
    height: "100%",
    marginLeft: WR(12),
    paddingVertical: WR(8),
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#414042",
  },
  selectionFetching: {
    paddingVertical: WR(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

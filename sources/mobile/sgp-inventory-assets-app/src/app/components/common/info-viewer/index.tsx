import React, { FC, Fragment } from "react";
import { View } from "react-native";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { TextCM } from "@Components";
import { isEmpty } from "lodash";

const CInfoViewer: FC<any> = ({ infos, infoRow, noLastItemMargin }) => {
  const isInfoRow = !!infoRow;
  const { label: rowLabel, unit: rowUnit } = infoRow || {};
  const data = infos.filter(({ label: infoLabel, value: infoValue, isGrouped }: any) => !isEmpty(infoValue));

  return (
    <Fragment>
      {isInfoRow && (
        <View style={styles.infoRowHeader}>
          <TextCM style={styles.infoRowHeaderTxt}>{rowLabel || ""}</TextCM>
          <TextCM style={styles.infoRowHeaderTxt}>{rowUnit || ""}</TextCM>
        </View>
      )}
      <View style={styles.infoWrapper}>
        {data.map(({ label: infoLabel, value: infoValue, isGrouped, labelVisible = true }: any, index: any) => {
          if (isEmpty(infoValue)) return null;
          return (
            <View
              key={index}
              style={[
                { width: isGrouped ? "50%" : "100%" },
                isGrouped && { paddingRight: WR(8) },
                isInfoRow ? { ...styles.infoRow, borderTopWidth: +!!index } : { marginTop: WR(index ? 16 : 0) },
                data.length - 1 === index && { marginBottom: noLastItemMargin ? 0 : WR(isInfoRow ? 16 : 20) },
              ]}>
              {labelVisible && <TextCM style={isInfoRow ? styles.infoRowLabel : styles.infoLabel}>{infoLabel}</TextCM>}
              <TextCM style={styles.infoValue}>{infoValue}</TextCM>
            </View>
          );
        })}
      </View>
    </Fragment>
  );
};

export default CInfoViewer;

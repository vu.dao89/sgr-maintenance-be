import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  infoWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  infoGrouped: {
    width: "50%",
  },
  infoLabel: {
    marginBottom: WR(8),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  infoValue: {
    color: Color.White,
  },
  infoRow: {
    height: "auto",
    paddingVertical: WR(12),
    flexDirection: "row",
    justifyContent: "space-between",
    borderTopColor: "#414042",
  },
  infoRowHeader: {
    marginBottom: WR(8),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  infoRowHeaderTxt: {
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  infoRowLabel: {
    color: Color.White,
    fontSize: FontSize.FontSmaller,
  },
});

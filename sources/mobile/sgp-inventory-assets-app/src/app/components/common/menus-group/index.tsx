import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";

import { isArray } from "src/utils/Basic";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { TextCM } from "@Components";

type PropsType = {
  column?: 2 | 3 | any;
  title?: string;
  menus: any[];
};

const CMenusGroup: FC<PropsType> = ({ column = 2, title = "", menus = [] }) => {
  if (!title && !isArray(menus, 1)) return null;
  return (
    <View style={styles.wrapper}>
      {!!title && (
        <View style={styles.header}>
          <TextCM style={styles.title}>{title}</TextCM>
        </View>
      )}
      <View style={[styles.container, column === 2 && { paddingHorizontal: WR(24) }]}>
        {menus.map((menu: any) => {
          const { id, label, icon, onPress } = menu;
          return (
            <TouchableOpacity
              key={id}
              style={[styles.menuItem, { width: `${Math.round((1 / column) * 100)}%` }]}
              onPress={onPress}>
              <View style={styles.menuIcon}>{icon}</View>
              <TextCM style={styles.menuTxt}>{label}</TextCM>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default CMenusGroup;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    borderRadius: WR(24),
    backgroundColor: "#262745",
  },
  header: {
    width: "100%",
    height: WR(56),
    paddingVertical: WR(16),
    paddingHorizontal: WR(16),
    borderBottomWidth: 1,
    borderBottomColor: "#414042",
  },
  title: {
    color: Color.White,
  },
  container: {
    width: "100%",
    paddingBottom: WR(5),
    flexDirection: "row",
    flexWrap: "wrap",
  },
  menuItem: {
    padding: WR(20),
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
  },
  menuIcon: {
    width: WR(24),
    height: WR(24),
    minHeight: WR(24),
    marginBottom: WR(4),
  },
  menuTxt: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
});

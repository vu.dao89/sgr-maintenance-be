import React, { FC, useEffect, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";

import { isArray } from "src/utils/Basic";

import styles, { sectionCardWidth } from "./styles";

import { Color, FontSize, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

import { CDatePicker, CSpinkit, CTags, TextCM } from "@Components";
import { formatObjectLabel } from "../../../../utils/Format";

const SectionInfoBox = ({ value, label, onPress }: any) => (
  <TouchableOpacity style={styles.sectionInfoBox} onPress={onPress}>
    <TextCM style={styles.sectionInfoTxt} numberOfLines={1}>
      {value} <Text style={{ opacity: 0.56, fontSize: FontSize.FontSmaller }}>{label}</Text>
    </TextCM>
  </TouchableOpacity>
);

const MetricCard = ({ title, total, filterSelection, outOfDates, mainPriorities, otherPriorities, onFilter }: any) => {
  const handleOnFilter = (i: any) =>
    onFilter({
      status: [filterSelection],
      priority: [i.filterSelection],
    });
  return (
    <View style={styles.sectionCard}>
      <View style={styles.sectionHeader}>
        <View style={styles.sectionMain}>
          <TextCM style={styles.sectionTitle}>{title}</TextCM>
          <TextCM style={styles.sectionNumber} numberOfLines={1}>
            {total}
          </TextCM>

          <CTags data={outOfDates} />
        </View>

        <View>
          {mainPriorities.map((i: any, idx: any) => (
            <SectionInfoBox key={idx} {...i} onPress={() => handleOnFilter(i)} />
          ))}
        </View>
      </View>

      {!!otherPriorities.length && (
        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
          {otherPriorities.map((i: any, idx: any) => (
            <SectionInfoBox key={idx} {...i} onPress={() => handleOnFilter(i)} />
          ))}
        </View>
      )}
    </View>
  );
};

const CMetricSection: FC<any> = ({ metric, dates, fetching, countStatusKey, countPriorityKey, onFilter }) => {
  const [datas, setDatas] = useState([]);

  const [swipeIdx, setSwipeIdx] = useState(0);

  useEffect(() => {
    if (isArray(metric, true)) {
      let emptyPriority: any;
      setDatas(
        metric.map((m: any) => {
          const { statId, statIdDescription, outDateItem, priorityItem } = m;
          const countStatus = m[countStatusKey];

          let priorities: any = [];
          priorityItem.forEach((p: any) => {
            const { priority, priorityText } = p;
            const countPriority = p[countPriorityKey];
            let label = priorityText || "Chưa có ưu tiên";
            let value = priority || "NONE";
            let item = {
              label,
              value: countPriority || 0,
              filterSelection: {
                label: formatObjectLabel(priority, label),
                value: value,
              },
            };

            if (value === "NONE") {
              emptyPriority = item;
            } else {
              priorities.push(item);
            }
          });

          priorities.sort((a: any, b: any) =>
            a.filterSelection.value > b.filterSelection.value
              ? 1
              : b.filterSelection.value > a.filterSelection.value
              ? -1
              : 0
          );

          return {
            title: statIdDescription,
            total: countStatus,
            filterSelection: {
              label: `${statIdDescription} (${statId})`,
              value: statId,
            },
            outOfDates:
              outDateItem?.map(({ countOutOfDate, outOfDateText }: any) => ({
                label: Number(countOutOfDate) + " " + outOfDateText,
                color: Color.Red,
              })) || [],
            mainPriorities: priorities.slice(0, 3),
            otherPriorities: [emptyPriority, ...priorities.slice(3)],
          };
        })
      );
    } else {
      setDatas([]);
    }
  }, [metric]);

  let totalData = datas.length;
  let isRefreshData = !!totalData && fetching;

  return (
    <View style={styles.wrapper}>
      <CDatePicker
        isRange
        isFilled
        values={dates}
        styles={{ wrapper: styles.rangePicker }}
        onChange={(nextDates: any) => onFilter({ fromDate: nextDates[0], toDate: nextDates[1] })}
      />

      <View style={[styles.container, { marginBottom: totalData ? WR(20) : 0 }]}>
        <View style={{ opacity: isRefreshData ? 0.5 : 1 }}>
          <Carousel
            data={datas}
            renderItem={({ item }: any) => <MetricCard {...item} onFilter={onFilter} />}
            sliderWidth={SW}
            itemWidth={sectionCardWidth}
            onSnapToItem={(index: any) => setSwipeIdx(index)}
          />

          <Pagination
            dotsLength={totalData}
            activeDotIndex={swipeIdx}
            containerStyle={styles.carouselPaginationContainer}
            dotStyle={styles.carouselPaginationDot}
            inactiveDotStyle={styles.carouselPaginationInactiveDot}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        </View>
        {isRefreshData && (
          <View style={styles.fetching}>
            <CSpinkit />
          </View>
        )}
      </View>
    </View>
  );
};

export default CMetricSection;

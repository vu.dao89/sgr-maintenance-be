import { StyleSheet } from "react-native";

import { FontSize, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export const sectionCardWidth = SW - WR(24 * 2);
const infoWidth = (sectionCardWidth - WR(20 * 2) - WR(14)) / 2;

export default StyleSheet.create({
  wrapper: {
    //backgroundColor: "blue",
  },
  rangePicker: {
    marginHorizontal: WR(24),
  },
  container: {
    marginTop: WR(-4),
    position: "relative",
  },
  fetching: {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  sectionCard: {
    width: sectionCardWidth,
    paddingBottom: WR(20),
    paddingRight: WR(20),
    paddingTop: WR(12),
    paddingLeft: WR(8),
    overflow: "hidden",
    borderRadius: WR(20),
    backgroundColor: "#262745",
  },
  sectionHeader: {
    width: sectionCardWidth - WR(16),
    flexDirection: "row",
    flexWrap: "wrap",
  },
  sectionMain: {
    width: infoWidth,
    height: WR(36 * 2 + 8),
    marginLeft: WR(12),
    marginTop: WR(8),
    overflow: "hidden",
  },
  sectionTitle: {
    marginBottom: WR(8),
    color: "#ffffff8f",
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
    lineHeight: WR(19),
  },
  sectionNumber: {
    color: "#ffffff",
    fontWeight: "600",
    fontSize: FontSize.FontHugest,
    lineHeight: WR(32),
  },
  sectionInfoBox: {
    marginTop: WR(8),
    marginLeft: WR(12),
    width: infoWidth,
    height: WR(36),
    paddingHorizontal: WR(12),
    flexDirection: "row",
    alignItems: "center",
    borderRadius: WR(16),
    backgroundColor: "#33345F",
  },
  sectionInfoTxt: {
    color: "#ffffff",
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
  },
  carouselPaginationContainer: {
    paddingTop: WR(16),
    paddingBottom: 0,
  },
  carouselPaginationDot: {
    width: WR(20),
    height: WR(6),
    borderRadius: WR(3),
    marginHorizontal: -WR(4),
    backgroundColor: "#8D9298",
  },
  carouselPaginationInactiveDot: {
    width: WR(6),
    height: WR(6),
    backgroundColor: "#8D9298",
  },
});

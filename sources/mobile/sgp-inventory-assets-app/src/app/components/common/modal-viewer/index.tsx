// @ts-nocheck
import React, { FC } from "react";
import { View } from "react-native";
import Modal from "react-native-modal";

import { widthResponsive as WR } from "@Constants";

import { CSpinkit, TextCM } from "@Components";

import styles from "./styles";

type Props = {
  open: any;
  title?: any;
  desc?: any;
  style?: any;
  placement?: "bottom" | "center" | "fullscr" | any;
  children?: any;
  isFetching?: boolean;
  onModalHide?: any;
  onModalShow?: any;
  onBackdropPress?: any;
};

const CModalViewer: FC<Props | any> = ({
  open,
  title,
  desc,
  style,
  children,
  isFetching,
  onModalHide,
  onModalShow,
  placement = "",
  ...props
}) => {
  let childrenNode = children;
  if (placement === "bottom") {
    childrenNode = (
      <View style={styles.container}>
        {!!title && <TextCM style={[styles.title, { marginBottom: WR(desc ? 8 : 24) }]}>{title}</TextCM>}
        {!!desc && <TextCM style={styles.desc}>{desc}</TextCM>}
        {children}
      </View>
    );
  }
  return (
    <Modal
      isVisible={!!open}
      animationIn={"fadeIn"}
      animationInTiming={100}
      animationOut={"fadeOut"}
      animationOutTiming={100}
      backdropColor={"#000000bf"}
      onModalHide={onModalHide}
      onModalShow={onModalShow}
      style={[styles.wrapper, styles[placement], style]}
      {...props}>
      {childrenNode}
      {isFetching && (
        <View style={styles.selectionFetching}>
          <CSpinkit />
        </View>
      )}
    </Modal>
  );
};

export default CModalViewer;

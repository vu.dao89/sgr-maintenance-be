import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    margin: 0,
  },
  bottom: {
    justifyContent: "flex-end",
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  fullscr: {
    backgroundColor: "#262745",
  },
  container: {
    maxHeight: "85%",
    width: "100%",
    backgroundColor: "#262745",
    borderTopLeftRadius: WR(20),
    borderTopRightRadius: WR(20),
  },
  title: {
    marginTop: WR(24),
    paddingHorizontal: WR(24),
    color: Color.White,
    fontWeight: "700",
    fontSize: FontSize.FontBigger,
  },
  desc: {
    marginBottom: WR(24),
    paddingHorizontal: WR(24),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  selectionFetching: {
    width: "100%",
    height: "100%",
    position: "absolute",
    paddingVertical: WR(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

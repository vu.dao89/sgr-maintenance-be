import React, { FC, useState } from "react";
import { Platform, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import styles from "./styles";

import { TextCM } from "@Components";

import { ICBackSVG, ICFilterDefault } from "@Assets/image";

interface CProps {
  title?: any;
  titleNode?: any;
  leftIcon?: any;
  rightIcon?: any;
  leftBtnNode?: any;
  rightBtnNode?: any;
  onLeftPress?: any;
  onRightPress?: any;
}

const CNavbar: FC<CProps> = ({
  title,
  titleNode,
  leftIcon,
  rightIcon,
  leftBtnNode,
  rightBtnNode,
  onLeftPress,
  onRightPress,
}) => {
  const insets = useSafeAreaInsets();
  const paddingTop = useState(insets.top + (Platform.OS === "ios" ? 0 : 24))[0];
  return (
    <View style={[styles.wrapper, { paddingTop }]}>
      <View style={styles.container}>

        {!!leftBtnNode &&
          <View style={styles.button}>
            {leftBtnNode}
          </View>
        }
        {!!onLeftPress &&
          <TouchableOpacity style={styles.button} onPress={onLeftPress}>
            {leftIcon || <ICBackSVG />}
          </TouchableOpacity>
        }


        <View style={styles.titleView}>
          {titleNode || (
            !!title &&
            <TextCM style={styles.titleTxt} numberOfLines={1}>
              {title}
            </TextCM>
          )}
        </View>

        {!!rightBtnNode &&
          <View style={styles.button}>
            {rightBtnNode}
          </View>
        }
        {!rightBtnNode && !!onRightPress &&
          <TouchableOpacity style={styles.button} onPress={onRightPress}>
            {rightIcon || <ICFilterDefault />}
          </TouchableOpacity>
        }

      </View>
    </View>
  );
};

export default CNavbar;

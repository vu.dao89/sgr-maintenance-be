import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

const navbarSize = HR(48);

export default StyleSheet.create({
  wrapper: {
    width: "100%",
    position: "relative",
    // backgroundColor: "gray",
  },
  container: {
    height: navbarSize,
    paddingHorizontal: WR(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    // backgroundColor: "green",
  },
  titleView: {
    height: navbarSize,
    position: "absolute",
    left: navbarSize + WR(8) * 2,
    right: navbarSize + WR(8) * 2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "red",
  },
  titleTxt: {
    textAlign: "center",
    color: Color.Yellow,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  button: {
    width: navbarSize,
    height: navbarSize,
    alignItems: "center",
    justifyContent: "center",
  },
});

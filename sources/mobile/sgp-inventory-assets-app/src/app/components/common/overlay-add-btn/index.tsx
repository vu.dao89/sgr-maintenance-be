import React, { FC } from "react";
import { TouchableOpacity } from "react-native";

import { ICPlus } from "@Assets/image";

import styles from "./styles";

const COverlayAddBtn: FC<any> = ({ style, onPress }) => (
  <TouchableOpacity style={[styles.button, style]} onPress={onPress}>
    <ICPlus />
  </TouchableOpacity>
);

export default COverlayAddBtn;

import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  button: {
    width: WR(56),
    height: WR(56),
    position: "absolute",
    bottom: WR(24),
    right: WR(24),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(56 / 2),
    backgroundColor: Color.Yellow,
  },
});

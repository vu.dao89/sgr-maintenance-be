import React, { FC } from "react";
import { Image, View } from "react-native";

import i18n from "@I18n";

import TextCM from "../../TextCM";

import { IMGDefault } from "@Assets/image";

import styles from "./styles";

const CPersonInfo: FC<any> = ({ style, label, avatar, name, fullName = name }) => (
  <View style={[styles.wrapper, style]}>
    <Image source={avatar ? { uri: avatar } : IMGDefault} style={styles.avatar} />
    <View style={styles.container}>
      <TextCM style={styles.label} numberOfLines={1}>
        {label}
      </TextCM>
      <TextCM style={styles.name} numberOfLines={1}>
        {fullName || i18n.t("CM.unknown")}
      </TextCM>
    </View>
  </View>
);

export default CPersonInfo;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
  },
  container: {
    flex: 1,
  },
  avatar: {
    width: WR(36),
    height: WR(36),
    marginRight: WR(8),
    borderRadius: WR(36 / 2),
  },
  label: {
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  name: {
    marginTop: WR(2),
    color: Color.White,
  },
});

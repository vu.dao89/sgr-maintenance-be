import React, { FC, Fragment, useEffect, useState } from "react";
import { Image, ScrollView, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import i18n from "@I18n";

import { isArray } from "src/utils/Basic";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CModalViewer, CSearchBar, CSpinkit, CTAButtons, TextCM } from "@Components";

import { ICCheck, IMGDefault } from "@Assets/image";
import { get, isEmpty } from "lodash";
import { MasterDataServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { PersonnelParams } from "../../../../services/parameters";
import { useIsMounted } from "../../../../utils/Core";
import { formatObjectLabel } from "../../../../utils/Format";

type PropsType = {
  open: any;
  title: any;
  mainPlantCode: any;
  workCenterCode: any;
  selected: any;
  onClose: any;
  onSelect: any;
};

const CPersonSelector: FC<PropsType> = ({
  open,
  title,
  mainPlantCode,
  workCenterCode,
  selected,
  onClose = () => null,
  onSelect = () => null,
}) => {
  const saInsets = useSafeAreaInsets();
  const isMounted = useIsMounted();

  const [keyword, setKeyword] = useState("");
  const [persons, setPersonnel] = useState([]);
  const [isFetching, setFetching] = useState(true);

  const [preAssignee, setPreAssignee]: any = useState(null);
  const [assignee, setAssignee]: any = useState(null);

  let haveData = isArray(persons, true);
  let unknownTxt = i18n.t("CM.unknown");

  useEffect(() => {
    setPreAssignee({ ...selected });
  }, []);

  const handleGetPersonnel = async () => {
    if (workCenterCode) {
      const param: PersonnelParams = {
        maintenancePlantCodes: [mainPlantCode],
        workCenterIds: [workCenterCode],
        keyword: keyword,
      };
      const { response } = await axiosHandler(() => MasterDataServices.getPersonnel(param));

      if (!isMounted()) return;

      const _personnel: any = [];
      get(response, "data.items", []).forEach((item: any) => {
        const { id, code, name } = item;
        _personnel.push({
          id: id,
          code: code,
          fullName: formatObjectLabel(code, name),
          department: "",
          email: "",
          avatar: "",
        });
      });
      setPersonnel(_personnel);
    }

    setFetching(false);
  };

  const handleOnCTA = () => {
    setAssignee({ ...preAssignee });
    onClose();
  };

  return (
    <CModalViewer
      {...{ open, title, onClose }}
      placement={"bottom"}
      onBackdropPress={onClose}
      onModalShow={() => {
        handleGetPersonnel();
      }}
      onModalHide={() => {
        if (keyword) {
          setKeyword("");
          setPersonnel([]);
        }
        const assigneeCode = get(assignee, "code");

        if (assigneeCode) {
          const preAssigneeCode = get(selected, "code");
          if (assigneeCode !== preAssigneeCode) {
            onSelect(assignee);
          }
        }
      }}>
      <CSearchBar
        {...{ keyword }}
        plh={"Nhập tên nhân viên"}
        style={styles.searchBar}
        onChange={setKeyword}
        onSearch={() => handleGetPersonnel()}
      />

      <ScrollView
        scrollEnabled={haveData}
        style={{ height: "100%", marginBottom: WR(56) }}
        contentContainerStyle={[styles.wrapper, { paddingBottom: saInsets.bottom || WR(16) }]}>
        {haveData &&
          persons.map((person: any, index: any) => {
            const { id, code, avatar, fullName, department, email } = person;
            let isSelected = preAssignee?.code === code;
            return (
              <Fragment key={id}>
                {!!index && <View style={styles.itemLineBreak} />}
                <TouchableOpacity
                  style={styles.personWrapper}
                  onPress={() => {
                    setPreAssignee({ ...person });
                  }}>
                  <View style={styles.personContainer}>
                    <Image source={avatar ? { uri: avatar } : IMGDefault} style={styles.personAvatar} />
                    <View style={styles.personContent}>
                      <TextCM style={styles.personFullName}>{fullName || unknownTxt}</TextCM>
                      {[department, email]
                        .filter((value: any) => !isEmpty(value))
                        .map((value: any) => {
                          return <TextCM style={styles.personInfo}>{value}</TextCM>;
                        })}
                    </View>
                  </View>
                  <View style={styles.selectedStatus}>{!!isSelected && <ICCheck />}</View>
                </TouchableOpacity>
              </Fragment>
            );
          })}

        {!isFetching && !haveData && <TextCM style={styles.emptyDataTxt}>{i18n.t("CM.noData")}</TextCM>}

        {isFetching && (
          <View style={styles.dataFetching}>
            <CSpinkit />
          </View>
        )}
      </ScrollView>

      {!isFetching && haveData && (
        <CTAButtons
          isSticky
          buttons={[
            {
              text: "Xác nhận",
              onPress: handleOnCTA,
            },
          ]}
        />
      )}
    </CModalViewer>
  );
};

export default CPersonSelector;

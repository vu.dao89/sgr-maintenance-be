import { StyleSheet } from "react-native";

import { Color, FontSize, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    paddingHorizontal: WR(24),
  },
  searchBar: {
    marginHorizontal: WR(24),
    marginBottom: WR(12),
  },
  dataFetching: {
    paddingVertical: WR(24),
  },
  itemLineBreak: {
    width: SW - WR(48),
    height: 1,
    backgroundColor: "#3F4166",
  },
  emptyDataTxt: {
    paddingHorizontal: WR(16),
    color: Color.White,
    fontSize: FontSize.FontMedium,
    textAlign: "center",
    paddingTop: WR(16),
    paddingBottom: WR(24),
  },
  personWrapper: {
    paddingVertical: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  personContainer: {
    flex: 1,
    flexDirection: "row",
  },
  personContent: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  personAvatar: {
    width: WR(36),
    height: WR(36),
    marginRight: WR(8),
    borderRadius: WR(36 / 2),
  },
  personFullName: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  personInfo: {
    marginTop: WR(4),
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  selectedStatus: {
    width: WR(20),
    height: WR(20),
  },
});

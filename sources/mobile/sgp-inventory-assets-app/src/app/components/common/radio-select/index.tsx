import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";

import { genRandomString, isArray } from "src/utils/Basic";

import styles from "./styles";

import TextCM from "../../TextCM";

import { ICRadioChecked, ICRadioUncheck } from "@Assets/image";

type Props = {
  styles?: any;
  selected: any;
  options: any;
  onSelect: any;
};

const CRadioSelect: FC<Props> = ({ styles: _styles = {}, selected, options, onSelect }) => {
  return (
    <View style={[styles.wrapper, _styles.wrapper]}>
      {isArray(options) &&
        options.map((option: any) => {
          const { value, label } = option;
          let isSelected = selected === value;
          return (
            <TouchableOpacity
              key={value}
              style={[styles.item, _styles.item]}
              onPress={() => onSelect(option, isSelected)}>
              <TextCM key={genRandomString()} style={styles.label}>
                {label}
              </TextCM>
              {isSelected ? <ICRadioChecked /> : <ICRadioUncheck />}
            </TouchableOpacity>
          );
        })}
    </View>
  );
};

export default CRadioSelect;

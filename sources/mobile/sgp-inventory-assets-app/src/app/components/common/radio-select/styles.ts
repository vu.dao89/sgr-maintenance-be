import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    marginTop: -WR(12),
    marginLeft: -WR(8),
    paddingBottom: WR(24),
    flexDirection: "row",
    flexWrap: "wrap",
  },
  item: {
    height: WR(44),
    paddingHorizontal: WR(12),
    marginLeft: WR(8),
    marginTop: WR(12),
    flexDirection: "row",
    alignItems: "center",
    borderRadius: WR(12),
    backgroundColor: "#33345F",
  },
  label: {
    marginRight: WR(8),
    color: Color.White,
  },
});

import React, { FC } from "react";
import { RefreshControl } from "react-native";

import { Color } from "@Constants";

const CRefreshControl: FC<any> = props => (
  <RefreshControl refreshing={false} colors={[Color.Yellow]} tintColor={Color.TextWhite} {...props} />
);

export default CRefreshControl;

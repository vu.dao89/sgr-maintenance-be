import React, { FC, useEffect, useState } from "react";
import { Platform, View } from "react-native";
import { PERMISSIONS, request as requestPermission, RESULTS } from "react-native-permissions";
import { QRscanner } from "react-native-qr-decode-image-camera";

import { useStateLazy } from "src/utils/Core";

import styles from "./styles";

import {
  Color,
  ScreenWidth as SW,
  ScreenHeight as SH,
  widthResponsive as WR,
  heightResponsive as HR,
} from "@Constants";

import { CNavbar, CModalViewer, TextCM, CSpinkit } from "@Components";

const rectSize = SW - WR(100);
const descPosition = SH / 2 + WR(48);

const CScanQRCodeModal: FC<any> = ({ open, dataName = "", onClose = () => null, onResult = () => null }) => {
  const [scanReady, setScanReady] = useState(false);
  const [canScan, setCanScan] = useState(true);

  const [isRepeatScan, setRepeatScan] = useStateLazy(true);

  useEffect(() => {
    if (open) {
      setRepeatScan(true);
      handleOnOpen();
    }
  }, [open]);

  const handleOnOpen = async () => {
    // @ts-ignore
    const permissionStatus = await requestPermission(PERMISSIONS[Platform.OS.toUpperCase()].CAMERA);
    setScanReady(true);
    setCanScan(permissionStatus === RESULTS.GRANTED);
  };

  const handleOnRead = async (result: any) => {
    if (!result?.data) return;
    setRepeatScan(false, () => onResult(result.data));
  };

  const _renderHeader = () => {
    return (
      <CNavbar
        title={"Quét mã QR"}
        onLeftPress={onClose}
      />
    );
  };

  const _renderBody = () => {
    let conentNode;
    switch (true) {
      case !scanReady:
        {
          conentNode = <CSpinkit />;
        }
        break;
      case !canScan:
        {
          conentNode = <TextCM style={styles.noPermissonTxt}>Chưa cấp quyền truy cập camera</TextCM>;
        }
        break;
      default:
        {
          conentNode = open && (
            <QRscanner
              {...{ isRepeatScan }}
              zoom={0}
              isShowScanBar={false}
              rectHeight={rectSize}
              rectWidth={rectSize}
              cornerColor={Color.Yellow}
              hintTextPosition={descPosition}
              hintText={`Đưa mã QR code${dataName ? " của " + dataName : ""} vào khung camera này`}
              hintTextStyle={styles.desc}
              onRead={handleOnRead}
            />
          );
        }
        break;
    }

    return <View style={[styles.container, (!scanReady || !canScan) && styles.center]}>{conentNode}</View>;
  };

  return (
    <CModalViewer {...{ open }} style={styles.wrapper}>
      {_renderHeader()}
      {_renderBody()}
    </CModalViewer>
  );
};

export default CScanQRCodeModal;

import { StyleSheet } from "react-native";

import { Color, FontSize, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  container: {
    flex: 1,
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  noPermissonTxt: {
    paddingHorizontal: WR(24),
    color: Color.White,
    textAlign: "center",
    fontSize: FontSize.FontMedium,
  },
  desc: {
    paddingHorizontal: WR(24),
    color: "#8D9298",
    textAlign: "center",
  },
});

import React, { FC } from "react";
import { TextInput, TouchableOpacity, View } from "react-native";

import { widthResponsive as WR } from "@Constants";

import styles from "./styles";

import { ICClose, ICSearchDark } from "@Assets/image";

const CSearchBar: FC<any> = ({ plh, style, keyword = "", onChange = () => null, onSearch = () => null }) => {
  return (
    <View style={[styles.wrapper, style]}>
      <TextInput
        placeholder={plh || "Tìm kiếm"}
        placeholderTextColor={"#BDBDBD"}
        style={styles.inputField}
        value={keyword}
        onChangeText={onChange}
        onSubmitEditing={onSearch}
      />
      {!!keyword && (
        <TouchableOpacity style={styles.btnRemove} onPress={() => onChange("")}>
          <ICClose width={WR(16)} />
        </TouchableOpacity>
      )}
      <TouchableOpacity style={styles.btnSearch} onPress={onSearch}>
        <ICSearchDark />
      </TouchableOpacity>
    </View>
  );
};

export default CSearchBar;

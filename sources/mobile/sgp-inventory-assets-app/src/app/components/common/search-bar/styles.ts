import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    height: WR(44),
    flexDirection: "row",
    alignItems: "center",
    borderRadius: WR(24),
    backgroundColor: "#394043",
  },
  inputField: {
    flex: 1,
    color: "white",
    paddingLeft: WR(16),
    fontSize: FontSize.FontSmaller,
  },
  btnRemove: {
    width: WR(36),
    height: WR(36),
    alignItems: "center",
    justifyContent: "center",
  },
  btnSearch: {
    width: WR(44),
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderTopRightRadius: WR(24),
    borderBottomRightRadius: WR(24),
    backgroundColor: Color.Yellow,
  },
});

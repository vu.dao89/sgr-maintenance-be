import { throttle } from "lodash";
import React, { FC, Fragment, useRef, useState } from "react";
import { Keyboard, ScrollView, TextInputProps, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import i18n from "@I18n";

import { isArray } from "src/utils/Basic";
import { isCloseToBottom } from "src/utils/Core";

import { Color, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

import styles from "./styles";

import { CModalViewer, CNavbar, CSearchBar, CSpinkit, TextCM } from "@Components";

import { ICCloseSearch, ICCloseY, ICSelectArrow } from "@Assets/image";

type Props = {
  isRequired?: any;
  isDisabled?: any;
  isFullScreen?: any;
  isError?: any;
  isOpen?: any;
  isFetching?: any;
  pagination?: any;
  highlightSelected?: any;
  fullWidth?: any;
  removeable?: any;
  children?: any;
  noInput?: any;
  opener?: any;
  label?: any;
  selected?: any;
  menus?: any;
  plh?: any;
  styles?: any;
  onSelect?: any;
  onChangeKeyword?: any;
  canOpen?: any;
  onOpen?: any;
  onClose?: any;
  onLoadMore?: any;
  onSearch?: any;
};

const CSelect: FC<TextInputProps & Props> = ({
  isRequired,
  isDisabled,
  isFullScreen,
  isError,
  isOpen: _isOpen,
  isFetching,
  pagination,
  highlightSelected,
  fullWidth,
  removeable = true,
  children,
  noInput,
  label,
  plh,
  selected,
  menus = [],
  styles: _styles = {},
  onSelect = () => null,
  canOpen = () => true,
  onOpen = () => null,
  onClose = () => null,
  onLoadMore = () => null,
  onChangeKeyword = () => null,
  onSearch = () => null,
}) => {
  const saInsets = useSafeAreaInsets();

  const menuLineBreakWidth = useState(SW - WR(isFullScreen ? 48 : 32))[0];

  const [isOpen, setOpen] = useState(false);
  const selectedMenu: any = useRef();

  const [keyword, setKeyword] = useState("");

  const handleClose = () => {
    setOpen(false);
    onClose();
  };

  const handleOnReachedEnd = (e: any) => {
    const { hasNext, nextPage } = pagination || {};
    if (isCloseToBottom(e) && !isFetching && hasNext) {
      onLoadMore(nextPage);
    }
  };

  let isHaveMenu = isArray(menus, true);
  let isSelected = !!selected?.value;
  let selectedColor = (isSelected && highlightSelected && Color.Yellow) || Color.White;

  return (
    <Fragment>
      {noInput ? (
        children
      ) : (
        <View style={[styles.wrapper, _styles.wrapper, fullWidth && { flex: 1 }, isDisabled && { opacity: 0.5 }]}>
          {!!label && (
            <View style={styles.labelWrapper}>
              <TextCM numberOfLines={1} style={[styles.labelTxt, isError && { color: Color.Red }]}>
                {label}
              </TextCM>
              {!!isRequired && <TextCM style={styles.labelRequired}>*</TextCM>}
            </View>
          )}
          <TouchableOpacity
            style={styles.container}
            onPress={() => {
              if (isDisabled || !canOpen()) return;
              Keyboard.dismiss();
              setOpen(true);
              onOpen();
            }}
            {...(isDisabled && {
              activeOpacity: 1,
            })}>
            <TextCM numberOfLines={1} style={[styles.input, { color: isSelected ? selectedColor : "#BDBDBD" }]}>
              {(isSelected && selected.label) || plh || i18n.t("CM.select")}
            </TextCM>
            {isSelected && !!removeable && !isDisabled && (
              <TouchableOpacity style={styles.btnRemove} onPress={() => onSelect(null)}>
                <ICCloseSearch />
              </TouchableOpacity>
            )}
            <View style={styles.arrow}>
              <ICSelectArrow fill={selectedColor} />
            </View>
          </TouchableOpacity>

          <View style={[styles.indicator, { backgroundColor: (isOpen && Color.White) || "#414042" }]} />
        </View>
      )}

      <CModalViewer
        open={isOpen || _isOpen}
        placement={isFullScreen ? "fullscr" : "bottom"}
        onModalHide={() => {
          if (selectedMenu.current) {
            setTimeout(() => {
              onSelect(selectedMenu.current);
              selectedMenu.current = null;
            }, 10);
          }
          setKeyword("");
        }}
        {...(!isFullScreen && {
          onBackdropPress: handleClose,
        })}>
        {!!isFullScreen && (
          <CNavbar
            leftIcon={<ICCloseY />}
            onLeftPress={handleClose}
            titleNode={
              <CSearchBar
                keyword={keyword}
                style={styles.searchBar}
                onChange={(k: any) => {
                  setKeyword(k);
                  throttle(
                    (kw: string) => {
                      onChangeKeyword(kw);
                    },
                    500,
                    { trailing: false }
                  )(k);
                }}
                onSearch={() => onSearch(keyword)}
              />
            }
          />
        )}
        <ScrollView
          scrollEventThrottle={50}
          scrollEnabled={isHaveMenu}
          style={{ marginTop: isFullScreen ? WR(8) : 0 }}
          contentContainerStyle={{
            paddingTop: isFullScreen ? 0 : WR(8),
            paddingBottom: saInsets.bottom || WR(8),
            ...(!isFullScreen && { maxHeight: SW }),
          }}
          onScroll={handleOnReachedEnd}>
          {isHaveMenu
            ? menus.map((menu: any, index: any) => {
                const { label: miLabel, value: miValue, color: miColor, style: miStyle } = menu;
                let isActive = selected?.value === miValue;
                return (
                  <Fragment key={`${miValue}-${index}`}>
                    {!!index && (
                      <View
                        style={[
                          styles.menuLineBreak,
                          {
                            width: menuLineBreakWidth,
                            marginHorizontal: WR(isFullScreen ? 24 : 16),
                          },
                        ]}
                      />
                    )}
                    <TouchableOpacity
                      style={styles.menuItem}
                      onPress={() => {
                        selectedMenu.current = menu;
                        handleClose();
                      }}>
                      <TextCM
                        style={[
                          styles.menuItemTxt,
                          { paddingHorizontal: WR(isFullScreen ? 24 : 16) },
                          !!miColor && { color: miColor },
                          !!isActive && { color: Color.Yellow },
                          miStyle,
                        ]}>
                        {miLabel}
                      </TextCM>
                    </TouchableOpacity>
                  </Fragment>
                );
              })
            : !isFetching && <TextCM style={[styles.menuItemTxt, styles.menuEmptyTxt]}>{i18n.t("CM.noData")}</TextCM>}
          {isFetching && (
            <View style={styles.selectionFetching}>
              <CSpinkit />
            </View>
          )}
        </ScrollView>
      </CModalViewer>
    </Fragment>
  );
};

export default CSelect;

import { StyleSheet } from "react-native";

import { Font, Color, FontSize, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    marginBottom: WR(20),
  },
  container: {
    height: WR(36),
    flexDirection: "row",
    alignItems: "center",
  },
  labelWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  labelTxt: {
    color: "#8D9298",
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontTiniest,
  },
  labelRequired: {
    marginLeft: 4,
    color: Color.Red,
    fontSize: FontSize.FontTiniest,
  },
  input: {
    flex: 1,
  },
  btnRemove: {
    width: WR(36),
    height: WR(36),
    marginLeft: WR(8),
    marginRight: -WR(8),
    alignItems: "center",
    justifyContent: "center",
  },
  arrow: {
    marginLeft: WR(8),
  },
  indicator: {
    width: "100%",
    height: WR(1),
  },
  menuEmptyTxt: {
    textAlign: "center",
    paddingTop: WR(16),
    paddingBottom: WR(24),
  },
  menuLineBreak: {
    height: 1,
    backgroundColor: "#3F4166",
  },
  menuItem: {
    minHeight: WR(48),
    flexDirection: "row",
    alignItems: "center",
  },
  menuItemTxt: {
    paddingVertical: WR(8),
    paddingHorizontal: WR(16),
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  navBtnLeft: {
    position: "absolute",
    left: WR(8),
  },
  searchBar: {
    flex: 1,
    height: "100%",
  },
  selectionFetching: {
    paddingVertical: WR(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

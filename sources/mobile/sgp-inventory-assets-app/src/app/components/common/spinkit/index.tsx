import React, { FC } from "react";
import { BarIndicator } from "react-native-indicators";

import { Color } from "@Constants";

const CSpinkit: FC<any> = ({ name = "default", color = Color.Yellow, size = 40, ...props }) => {
  let SpinkitComp;
  switch (name) {
    case "default":
      SpinkitComp = BarIndicator;
      break;
    case "":
    default:
      break;
  }
  // @ts-ignore
  return SpinkitComp ? <SpinkitComp {...{ color, size, ...props }} /> : null;
};

export default CSpinkit;

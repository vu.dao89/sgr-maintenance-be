import React, { FC } from "react";
import { Keyboard, TouchableOpacity } from "react-native";
import { Switch } from "react-native-switch";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import TextCM from "../../TextCM";

type Props = {
  label?: any;
  value?: any;
  styles?: any;
  onChange?: any;
};

const CSwitch: FC<Props> = ({ label, value, styles: _styles = {}, onChange = () => null }) => {
  return (
    <TouchableOpacity
      style={[styles.wrapper, _styles.wrapper]}
      onPress={() => {
        Keyboard.dismiss();
        onChange(!value);
      }}>
      <Switch
        value={value}
        activeText={""}
        inActiveText={""}
        barHeight={WR(16)}
        circleSize={WR(24)}
        circleBorderWidth={0}
        switchLeftPx={WR(3)}
        switchRightPx={WR(3)}
        switchWidthMultiplier={1.75}
        circleActiveColor={Color.Yellow}
        circleInActiveColor={"#394043"}
        backgroundActive={Color.White}
        backgroundInactive={"#33345F"}
        onValueChange={_value => onChange(_value)}
      />
      <TextCM style={[styles.label, _styles.label]}>{label || ""}</TextCM>
    </TouchableOpacity>
  );
};

export default CSwitch;

import React, { FC, Fragment } from "react";
import { View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { TabView } from "react-native-tab-view";

import { Color } from "@Constants";

import styles from "./styles";

const CTabViewCreate: FC<any> = ({
  index,
  routes,
  validate,
  onChange = () => null,
  onSearch = () => null,
  ...sceneProps
}) => {
  const saInsets = useSafeAreaInsets();
  return (
    <Fragment>
      <View style={styles.tabViewIndicatorWrapper}>
        {routes.map((_: any, idx: any) => (
          <View
            key={idx}
            style={[
              styles.tabViewIndicatorItem,
              {
                backgroundColor: Color[index === idx ? "Yellow" : "White"],
                ...(!idx && {
                  marginLeft: 0,
                  borderTopLeftRadius: 0,
                  borderBottomLeftRadius: 0,
                }),
                ...(routes.length - 1 === idx && {
                  marginRight: 0,
                  borderTopRightRadius: 0,
                  borderBottomRightRadius: 0,
                }),
              },
            ]}
          />
        ))}
      </View>

      <TabView
        swipeEnabled={false}
        navigationState={{ index, routes }}
        renderScene={({ route }: any) => {
          const { key, title } = route;
          const { Scene } = routes[key];
          return <Scene showTitle {...{ key, saInsets, title, validate, onChange, onSearch }} {...sceneProps} />;
        }}
        renderTabBar={() => null}
        onIndexChange={() => null}
      />
    </Fragment>
  );
};

export default CTabViewCreate;

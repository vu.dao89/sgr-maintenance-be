import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  tabViewIndicatorWrapper: {
    flexDirection: "row",
  },
  tabViewIndicatorItem: {
    flex: 1,
    height: WR(2),
    marginHorizontal: WR(1),
    borderRadius: WR(4),
  },
});

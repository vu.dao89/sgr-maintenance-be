import { debounce } from "lodash";
import React, { FC, useRef, useState } from "react";
import { View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { TabBar, TabView } from "react-native-tab-view";

import { Color, widthResponsive as WR } from "@Constants";

import { TextCM } from "@Components";

const CTabViewDetail: FC<any> = ({
  routes,
  index = 0,
  onLayout = () => null,
  onChange = () => null,
  onNavigate = () => null,
  ...sceneProps
}) => {
  const saInsets = useSafeAreaInsets();

  const [tabIndex, setTabIndex] = useState(index);
  const sceneHeights: any = useRef({});
  const lastSceneHeights: any = useRef(0);

  const updateParentLayout = (_index: any) => {
    let layoutHeihgt = (sceneHeights.current[_index] || 0) + WR((routes[_index].padding || 0) * 2);
    if (lastSceneHeights.current !== layoutHeihgt) {
      lastSceneHeights.current = layoutHeihgt;
      onLayout(layoutHeihgt);
    }
  };

  const handleOnLayout = (key: any) => (e: any) => {
    let sceneHeight = e.nativeEvent.layout.height;
    sceneHeights.current[key] = sceneHeight;
    if ([routes.length - 1, tabIndex].includes(parseInt(key))) {
      debounce(() => {
        updateParentLayout(tabIndex);
      }, 300)();
    }
  };

  const handleIndexChange = (nextIndex: any) => {
    setTabIndex(nextIndex);
    updateParentLayout(nextIndex);
  };

  const renderScene = ({ route }: any) => {
    const { key, title } = route;
    const { Scene } = routes[key];
    return (
      <View onLayout={handleOnLayout(key)}>
        <Scene {...{ key, saInsets, title, onChange, onNavigate }} {...sceneProps} />
      </View>
    );
  };

  const renderTabBarLabel = ({ route, focused }: any) => (
    <TextCM style={{ color: focused ? Color.Yellow : "#8D9298", fontWeight: "600" }}>{route.title}</TextCM>
  );

  const renderTabBar = (tabBarprops: any) => {
    return (
      <TabBar
        {...tabBarprops}
        indicatorStyle={{ backgroundColor: Color.Yellow }}
        style={{ backgroundColor: "transparent", borderBottomWidth: 1, borderBottomColor: "#414042", zIndex: 1 }}
        renderLabel={renderTabBarLabel}
      />
    );
  };

  return (
    <TabView
      swipeEnabled={false}
      navigationState={{ index: tabIndex, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={handleIndexChange}
    />
  );
};

export default CTabViewDetail;

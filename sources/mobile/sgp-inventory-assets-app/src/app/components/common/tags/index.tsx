import React, { FC, useEffect, useState } from "react";
import { View } from "react-native";

import { genRandomString, isArray } from "src/utils/Basic";
import { getInvertColor } from "src/utils/Style";

import { Color } from "@Constants";

import styles from "./styles";

import { TextCM } from "@Components";

const CTags: FC<any> = ({ style, data }) => {
  const [tags, setTags] = useState([]);

  useEffect(() => {
    setTags(
      (data || []).reduce((arr: any, i: any) => {
        let { color, label } = i;
        if (color && ["#", "rbg"].every(j => !color.startsWith(j))) color = "#" + color;
        color = String(color).toLowerCase();
        if (label) {
          arr.push({
            ...i,
            color,
            cntColor: getInvertColor(color, { black: Color.TextBlack, white: Color.White, threshold: 0.5 }),
          });
        }
        return arr;
      }, [])
    );
  }, [data]);

  if (!isArray(tags, true)) return null;
  return (
    <View style={[styles.wrapper, style]}>
      {tags.map(({ label, color, cntColor }: any, index: any) => {
        if (!label) return null;
        const rnd = genRandomString();
        return (
          <View key={`${rnd}_${index}`} style={[styles.item, { backgroundColor: color }]}>
            <TextCM style={[styles.text, { color: cntColor }]}>{label}</TextCM>
          </View>
        );
      })}
    </View>
  );
};

export default CTags;

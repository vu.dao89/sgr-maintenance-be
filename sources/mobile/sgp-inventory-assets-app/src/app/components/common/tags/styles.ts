import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR, heightResponsive as HR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    marginLeft: -WR(4),
    flexDirection: "row",
    flexWrap: "wrap",
  },
  item: {
    marginLeft: WR(4),
    marginTop: WR(4),
    height: WR(16),
    paddingVertical: WR(2),
    paddingHorizontal: WR(4),
    borderRadius: WR(12),
  },
  text: {
    fontWeight: "600",
    fontSize: HR(8),
  },
});

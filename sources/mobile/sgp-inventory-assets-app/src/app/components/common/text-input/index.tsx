import React, { FC, forwardRef, useState } from "react";
import { TextInput, View } from "react-native";

import i18n from "@I18n";

import { isNumber } from "src/utils/Basic";

import { Color, widthResponsive as WR } from "@Constants";

import TextCM from "../../TextCM";

import styles from "./styles";

const CTextInput: FC<any> = forwardRef(
  (
    {
      isRequired,
      isError,
      fullWidth,
      label,
      plh,
      value,
      multiline = 1,
      maxLength,
      styles: _styles = {},
      onChange = () => null,
      onFocus = () => null,
      onBlur = () => null,
      ...inputProps
    },
    ref: any
  ) => {
    const [isFocus, setFocus] = useState(false);
    const isMultiLine = multiline > 1;

    return (
      <View style={[styles.wrapper, _styles.wrapper, fullWidth && { flex: 1 }]}>
        {!!label && (
          <View style={styles.labelWrapper}>
            <TextCM style={[styles.labelTxt, isError && { color: Color.Red }]} numberOfLines={1}>
              {label}
            </TextCM>
            {!!isRequired && <TextCM style={styles.labelRequired}>*</TextCM>}
          </View>
        )}

        <View
          style={{
            height: WR(36 * multiline),
            ...(isMultiLine && {
              marginTop: WR(4),
              borderRadius: WR(8),
              backgroundColor: "#33345F",
            }),
          }}>
          <TextInput
            multiline={isMultiLine}
            numberOfLines={multiline}
            {...inputProps}
            ref={ref}
            value={value || ""}
            placeholder={plh || i18n.t("CM.enter")}
            placeholderTextColor={"#BDBDBD"}
            allowFontScaling={false}
            underlineColorAndroid={"transparent"}
            style={[styles.input, _styles.input, isMultiLine && styles.multiline]}
            onChangeText={(text: any) => onChange(isNumber(maxLength) ? text.substring(0, maxLength) : text)}
            onFocus={() => {
              setFocus(true);
              onFocus();
            }}
            onBlur={() => {
              setFocus(false);
              onBlur();
            }}
          />
        </View>

        {!isMultiLine && (
          <View style={[styles.indicator, _styles.indicator, { backgroundColor: isFocus ? Color.White : "#414042" }]} />
        )}
      </View>
    );
  }
);

export default CTextInput;

import { StyleSheet } from "react-native";

import { Color, Font, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  wrapper: {
    marginBottom: WR(20),
    textAlign: "justify",
  },
  labelWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  labelTxt: {
    color: "#8D9298",
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontTiniest,
  },
  labelRequired: {
    marginLeft: 4,
    color: Color.Red,
    fontSize: FontSize.FontTiniest,
  },
  input: {
    height: "100%",
    color: Color.White,
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontSmaller,
  },
  multiline: {
    flex: 1,
    marginVertical: WR(10),
    paddingHorizontal: WR(16),
  },
  indicator: {
    width: "100%",
    height: WR(1),
  },
});

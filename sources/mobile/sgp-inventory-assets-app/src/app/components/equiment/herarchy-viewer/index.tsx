import React, { FC, useState } from "react";
import { TouchableOpacity, View } from "react-native";
import Collapsible from "react-native-collapsible";

import { isArray } from "src/utils/Basic";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import { TextCM } from "@Components";

import { ICCollapseOff, ICCollapseOn } from "@Assets/image";
import { get } from "lodash";

const HerarchyLevel = ({ level = 0, space = 0, element, children, isLast, activeId }: any) => {
  const [isCollapsed, setCollapsed] = useState(false);

  let hasChild = isArray(children, true);
  let elementId = get(element, "id", "");
  let label = get(element, "description");
  let isActive = elementId === activeId;

  return (
    <View style={styles.treeLevel}>
      {!!level && (
        <View>
          <View
            style={[
              styles.treeBranch,
              {
                marginLeft: space - 10,
                height: isLast ? WR(14) : "100%",
              },
            ]}
          />
          <View style={[styles.treeLeaf, { left: space - WR(20) + WR(9) }]} />
          {!hasChild && <View style={[styles.treeNode, { left: space - WR(4) }]} />}
        </View>
      )}
      <View>
        <TouchableOpacity
          style={[styles.treeRow, { marginLeft: space - (hasChild ? 0 : WR(8)) }]}
          onPress={() => setCollapsed(!isCollapsed)}>
          {hasChild && (isCollapsed ? <ICCollapseOn /> : <ICCollapseOff />)}
          <TextCM
            style={[styles.treeLabel, { marginLeft: WR(hasChild ? 8 : 12) }, isActive && { color: Color.Yellow }]}>
            {label}
          </TextCM>
        </TouchableOpacity>
        <View style={styles.treeContainer}>
          {/* {hasChild && (
            // @ts-ignore
            <View style={[styles.childContainer, isCollapsed && { height: 0, overflow: "hidden" }]}>
              {childs.map((childItem: any, childIdx: any) => (
                <HerarchyLevel
                  key={childItem.id}
                  space={(level + 1) * WR(20)}
                  level={level + 1}
                  isLast={childIdx === childs.length - 1}
                  {...childItem}
                  {...{ activeId }}
                />
              ))}
            </View>
          )} */}
          {hasChild && (
            // @ts-ignore
            <Collapsible collapsed={isCollapsed} style={styles.childContainer}>
              {children.map((childItem: any, childIdx: any) => (
                <HerarchyLevel
                  key={get(childItem, ["element", "id"], `Herarchy_${elementId}_${childIdx}`)}
                  space={(level + 1) * WR(20)}
                  level={level + 1}
                  isLast={childIdx === children.length - 1}
                  {...childItem}
                  {...{ activeId }}
                />
              ))}
            </Collapsible>
          )}
        </View>
      </View>
    </View>
  );
};

const EHerarchyViewer: FC<any> = ({ datas, activeId }) => {
  if (!isArray(datas, true)) return null;

  return (
    <View style={styles.treeWrapper}>
      {datas.map((item: any, index: any) => (
        <HerarchyLevel
          key={get(item, ["element", "id"], `Herarchy_${index}`)}
          isLast={index === datas.length - 1}
          {...item}
          {...{ activeId }}
        />
      ))}
    </View>
  );
};

export default EHerarchyViewer;

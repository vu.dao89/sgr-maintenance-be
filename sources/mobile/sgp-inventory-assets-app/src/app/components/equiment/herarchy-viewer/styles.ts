import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  treeWrapper: {
    paddingHorizontal: WR(24),
  },
  treeRow: {
    paddingVertical: WR(6),
    flexDirection: "row",
    alignItems: "center",
  },
  treeLabel: {
    color: Color.White,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
  },
  treeLevel: {
    flexDirection: "row",
  },
  treeContainer: {
    flexDirection: "row",
    overflow: "hidden",
  },
  treeBranch: {
    width: 1,
    position: "absolute",
    left: -WR(3),
    top: 0,
    backgroundColor: "#8D9298",
  },
  treeLeaf: {
    position: "absolute",
    top: WR(14),
    width: WR(7),
    height: 1,
    backgroundColor: "#8D9298",
  },
  treeNode: {
    position: "absolute",
    top: WR(13),
    width: WR(3),
    height: WR(3),
    borderRadius: WR(1.5),
    backgroundColor: "#8D9298",
  },
});

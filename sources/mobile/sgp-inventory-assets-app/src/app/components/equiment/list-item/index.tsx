import React, { FC, Fragment, memo } from "react";
import { TouchableOpacity, View } from "react-native";
import { isEqual } from "lodash";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CTags, ImageCache, TextCM } from "@Components";

import { ICListPlus, IMEQDefault } from "@Assets/image";

const EListItem: FC<any> = ({ item, index, onViewDetail, onViewHierarchy }) => {
  const { id, name, functionalLocation, tags, status } = item;

  return (
    <Fragment key={id}>
      {!!index && <View style={styles.listSeparator} />}
      <TouchableOpacity style={styles.listItemWrapper} onPress={onViewDetail}>
        <View style={styles.listItemContainer}>
          <View style={styles.listItemContent}>
            <TextCM style={styles.listItemTitle}>{name}</TextCM>
            <TextCM style={styles.listItemDesc}>{functionalLocation}</TextCM>
            <CTags data={tags} style={styles.listItemTags} />
          </View>
          <View style={styles.listItemThumbnailWrapper}>
            <ImageCache imageDefault={IMEQDefault} style={styles.listItemThumbnailImg} />
          </View>
        </View>
        <View style={styles.listItemFooter}>
          <TouchableOpacity style={styles.listItemBtn} onPress={onViewHierarchy}>
            <ICListPlus width={WR(16)} height={WR(16)} />
            <TextCM style={styles.listItemBtnTxt}>Xem phân cấp</TextCM>
          </TouchableOpacity>
          {!!status && <TextCM style={styles.listItemStatus}>{status}</TextCM>}
        </View>
      </TouchableOpacity>
    </Fragment>
  );
};

const propsAreEqual = (prevProps: any, nextProps: any) => ['item', 'index'].every(i => isEqual(prevProps[i], nextProps[i]));

export default memo(EListItem, propsAreEqual);

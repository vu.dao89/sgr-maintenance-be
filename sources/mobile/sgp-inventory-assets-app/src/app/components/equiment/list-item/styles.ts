import { StyleSheet } from "react-native";

import { Color, FontSize, ScreenWidth, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  listSeparator: {
    width: ScreenWidth - WR(24) * 2,
    height: 1,
    marginHorizontal: WR(24),
    backgroundColor: "#414042",
  },
  listItemWrapper: {
    paddingTop: WR(16),
    paddingBottom: WR(4),
    paddingHorizontal: WR(24),
  },
  listItemContainer: {
    flexDirection: "row",
  },
  listItemContent: {
    flex: 1,
    marginRight: WR(16),
  },
  listItemTitle: {
    color: Color.White,
    fontWeight: "700",
  },
  listItemDesc: {
    marginTop: WR(8),
    color: "#8D9298",
    fontWeight: "400",
  },
  listItemTags: {
    marginTop: WR(8),
  },
  listItemThumbnailWrapper: {
    width: WR(56),
    height: WR(56),
  },
  listItemThumbnailImg: {
    width: WR(56),
    height: WR(56),
    borderRadius: WR(16),
  },
  listItemFooter: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  listItemBtn: {
    paddingVertical: WR(12),
    flexDirection: "row",
    alignItems: "center",
  },
  listItemBtnTxt: {
    marginLeft: WR(4),
    color: Color.Yellow,
    fontSize: FontSize.FontTiny,
    fontWeight: "600",
  },
  listItemStatus: {
    color: "#ACACAC",
    fontSize: FontSize.FontTiny,
    fontWeight: "400",
  },
});

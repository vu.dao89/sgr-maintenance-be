import React, { FC, useEffect, useRef, useState } from "react";
import { ScrollView, TouchableOpacity } from "react-native";
import Toast from "react-native-toast-notifications";

import { widthResponsive as WR } from "@Constants";
import { useStateLazy } from "../../../../utils/Core";
import { CCollapsibleSection, CModalViewer, CTAButtons } from "../../common";

import i18n from "../../../../i18n";
import { isArray } from "../../../../utils/Basic";
import TextCM from "../../TextCM";
import NotificationActivityCreation from "../create-activity";
import styles from "./styles";

type PropsType = {
  title: string;
  saInsets: any;
  isOpen: boolean;
  notificationType: any;
  onClose: any;
  onChange: any;
};

const NotificationActivitiesCreation: FC<PropsType> = ({
  title,
  saInsets,
  isOpen,
  notificationType,
  onClose,
  onChange,
}) => {
  const toastRef: any = useRef();

  const [isOpenActivites, setisOpenActivites] = useState(false);
  const [isOpenCreateActivities, setIsOpenCreateActivities] = useState(false);
  const [addMoreActivitiy, setAddMoreActivitiy] = useState(false);

  const [activitiesGroup, setActivitiesGroup] = useStateLazy([]);

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  useEffect(() => {
    if (isOpen) {
      setAddMoreActivitiy(false);
      setActivitiesGroup([]);
      setIsOpenCreateActivities(true);
    } else {
      setisOpenActivites(false);
      setIsOpenCreateActivities(false);
    }
  }, [isOpen]);

  return (
    <>
      <CModalViewer
        open={isOpenActivites}
        title={title}
        placement="bottom"
        onModalHide={() => {
          if (addMoreActivitiy) {
            setAddMoreActivitiy(false);
            setIsOpenCreateActivities(true);
          }
        }}
        onBackdropPress={() => onClose()}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />
        <ScrollView
          contentContainerStyle={[{ paddingBottom: modalPaddingBottom + WR(14) }]}
          style={{ paddingStart: WR(24), paddingEnd: WR(24) }}>
          <TouchableOpacity
            style={styles.btnAddItem}
            onPress={() => {
              setAddMoreActivitiy(true);
              setisOpenActivites(false);
            }}>
            <TextCM style={styles.btnAddTxt}>+ Thêm hành động khắc phục</TextCM>
          </TouchableOpacity>

          {isArray(activitiesGroup, 1) &&
            activitiesGroup.map((group: any, index: any) => {
              return (
                <CCollapsibleSection
                  borderBg
                  key={`${group.workOrderId}_${index}`}
                  title={`Hành Động ${index + 1}`}
                  infos={[
                    {
                      label: i18n.t("N.activity"),
                      value: group.activity?.label,
                    },
                    {
                      label: i18n.t("N.activityCode"),
                      value: group.activityCode?.label,
                    },
                    {
                      label: i18n.t("N.activityText"),
                      value: group.activityText,
                    },
                  ]}></CCollapsibleSection>
              );
            })}
        </ScrollView>
        <CTAButtons
          isSticky
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              text: "Hoàn thành",
              onPress: () => {
                onChange(activitiesGroup);
              },
            },
          ]}
        />
      </CModalViewer>
      <NotificationActivityCreation
        saInsets={saInsets}
        isOpen={isOpenCreateActivities}
        onModalHide={() => {
          if (isArray(activitiesGroup, 1)) setisOpenActivites(true);
        }}
        notificationType={notificationType}
        onClose={() => {
          if (isArray(activitiesGroup, 1)) setIsOpenCreateActivities(false);
          else onClose();
        }}
        onChange={(activity: any) => {
          activitiesGroup.push({ ...activity });
          setActivitiesGroup([...activitiesGroup]);

          setIsOpenCreateActivities(false);
        }}
      />
    </>
  );
};
export default NotificationActivitiesCreation;

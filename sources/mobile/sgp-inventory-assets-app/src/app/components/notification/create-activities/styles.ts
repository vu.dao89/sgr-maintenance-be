import { StyleSheet } from "react-native";

import { Color } from "@Constants";

export default StyleSheet.create({
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
});

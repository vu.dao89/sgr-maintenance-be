import { widthResponsive as WR } from "@Constants";
import React, { FC, useEffect, useRef } from "react";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-toast-notifications";
import i18n from "../../../../i18n";
import { MasterDataServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { genRandomString } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { mappingSelections } from "../../../../utils/Format";
import { CModalViewer, CSelect, CTAButtons, CTextInput } from "../../common";
import styles from "./styles";

type PropsType = {
  saInsets: any;
  isOpen: boolean;
  notificationType: any;
  onClose: any;
  onChange: any;
  onModalHide?: any;
};

const relatatedOptions: any = {
  activity: "activityCode",
};

const NotificationActivityCreation: FC<PropsType> = ({
  saInsets,
  isOpen,
  notificationType ={},
  onClose,
  onChange,
  onModalHide,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [itemState, setItemState]: any = useStateLazy({});
  const [errors, setErrors] = useStateLazy({});
  const reqIds: any = useRef({});

  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});

  const { activity } = itemState;

  const { catalogProfile, activities } = notificationType;

  const forms = [
    {
      type: "select",
      id: "activity",
      label: i18n.t("N.activity"),
      isRequired: true,
    },
    {
      type: "select",
      id: "activityCode",
      label: i18n.t("N.activityCode"),
      relatatedData: "activity",
      isRequired: true,
      canOpen: () => {
        if (!activity) toastRef.current.show("Vui lòng chọn " + i18n.t("N.activity"), { type: "warning" });
        return !!activity;
      },
    },
    {
      type: "input",
      id: "activityText",
      label: "Mô tả chi tiết",
      plh: "Nhập mô tả chi tiết",
      isRequired: true,
      multiline: 2,
    },
  ];

  useEffect(() => {
    setItemState({});
    setErrors({});
  }, [isOpen]);

  const handleCreateNotificationItem = async () => {
    let isValidData = true,
      _errors: any = {},
      focusInputField: any = null,
      setInvalid = (id: any) => {
        isValidData = false;
        _errors[id] = true;
        if (!focusInputField) focusInputField = id;
      };

    ["activity", "activityCode", "activityText"].forEach((requiredField: any) => {
      if (!itemState[requiredField]) setInvalid(requiredField);
    });

    setErrors(_errors);

    if (isValidData) onChange(itemState);
  };

  const handleChangeItem = (id: any, data: any) => {
    let nextState = { [id]: data };
    let relatatedOptionDataKey = relatatedOptions[id];
    if (relatatedOptionDataKey) {
      let prevData = itemState[id];
      if (prevData && prevData.value !== data?.value) {
        nextState[relatatedOptionDataKey] = null;
        setOptions((prevState: any) => ({ ...prevState, [relatatedOptionDataKey]: [] }));
      }
    }
    setItemState((prevState: any) => ({ ...prevState, ...nextState }));
  };

  const handleSearch = (id: any, relatedData: any) => async (keyword: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];
    let params: any = {
      keyword,
      maintenancePlantCodes: [],
    };
    let searchServiceApi: any, mappingOptions;

    switch (id) {
      case "activity":
        {
          Object.assign(params, {
            catalogProfile: catalogProfile,
            catalog: activities,
          });
          searchServiceApi = MasterDataServices.getCodeGroups;
          mappingOptions = { valueKey: "codeGroup", labelKey: "codeGroupDesc" };
        }
        break;
      case "activityCode":
        {
          Object.assign(params, {
            catalogProfile: catalogProfile,
            codeGroup: relatedData.value,
            catalog: activities,
          });

          searchServiceApi = MasterDataServices.getCodes;
          mappingOptions = { labelKey: "codeDesc" };
        }
        break;
      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));

    //console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    // await miniTimer(500); // TEMP, fake loading API

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const _renderFormCreateItem = () => {
    return (
      <CModalViewer
        open={isOpen}
        placement={"bottom"}
        onModalHide={onModalHide}
        title={"Thêm hành động khắc phục"}
        onBackdropPress={() => onClose()}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />

        <KeyboardAwareScrollView contentContainerStyle={styles.crudModalContent}>
          {forms.map((field: any) => {
            const { type, id, isSearch, relatatedData, ...fieldProps } = field;
            let _fieldProps = {
              key: id,
              isError: errors[id],
              value: itemState[id],
              onChange: (data: any) => handleChangeItem(id, data),
              ...fieldProps,
            };
            switch (type) {
              case "input":
                let isShow = !relatatedData || itemState[relatatedData];
                return isShow && <CTextInput {..._fieldProps} />;
              case "select": {
                let _onSearch = handleSearch(id, itemState[relatatedData]);
                let selectInputNode = (
                  <CSelect
                    {..._fieldProps}
                    isFullScreen
                    isFetching={fetchings[id]}
                    selected={itemState[id]}
                    menus={options[id]}
                    onSelect={(selected: any) => handleChangeItem(id, selected)}
                    onSearch={_onSearch}
                    {...(!isSearch && { onOpen: _onSearch })}
                  />
                );
                return selectInputNode;
              }
              default:
                return null;
            }
          })}
        </KeyboardAwareScrollView>

        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              text: i18n.t(`CM.add`),
              onPress: () => {
                handleCreateNotificationItem();
              },
            },
          ]}
        />
      </CModalViewer>
    );
  };

  return <>{_renderFormCreateItem()}</>;
};
export default NotificationActivityCreation;

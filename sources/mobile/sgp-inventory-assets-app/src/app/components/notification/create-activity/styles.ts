import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(24),
    paddingHorizontal: WR(24),
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalWrapper: {
    flex: 1,
    margin: 0,
    justifyContent: "flex-end",
  },
  crudModalContainer: {
    maxHeight: "85%",
    width: "100%",
    backgroundColor: "#262745",
    borderTopLeftRadius: WR(20),
    borderTopRightRadius: WR(20),
  },
  crudModalTitle: {
    marginVertical: WR(24),
    paddingHorizontal: WR(24),
    color: Color.White,
    fontWeight: "700",
    fontSize: FontSize.FontBigger,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(16),
  },
});

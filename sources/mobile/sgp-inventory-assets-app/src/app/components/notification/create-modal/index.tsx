import { widthResponsive as WR } from "@Constants";
import { get, isEmpty } from "lodash";
import React, { FC, useEffect, useRef } from "react";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-toast-notifications";
import i18n from "../../../../i18n";
import { MasterDataServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { genRandomString } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { mappingSelections } from "../../../../utils/Format";
import { CModalViewer, CSelect, CTAButtons, CTextInput } from "../../common";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  itemAction: string;
  notificationId: string;
  notificationType: any;
  initState: any;
  onClose: any;
  onChange: any;
  onModalHide?: any;
};

const relatatedOptions: any = {
  notifPart: "notifPartCode",
  notifProblem: "notifDame",
  notifCause: "notifCauseCode",
};

const NotificationItemCreation: FC<PropsType> = ({
  saInsets,
  isCrud,
  itemAction,
  notificationId,
  notificationType,
  initState,
  onClose,
  onChange,
  onModalHide,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [itemState, setItemState]: any = useStateLazy({});
  const [errors, setErrors] = useStateLazy({});
  const reqIds: any = useRef({});

  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});

  const [isFetching, setFetching] = useStateLazy(false);

  const { notifPart, notifProblem, notifCause } = itemState;

  const haveObjectParts = !!get(notificationType, "objectParts");
  const haveProblems = !!get(notificationType, "problems");
  const haveCauses = !!get(notificationType, "causes");

  const forms = [
    {
      type: "select",
      id: "notifPart",
      label: i18n.t("N.notifPart"),
      canOpen: () => {
        if (!haveObjectParts) toastRef.current.show("Không cần chọn " + i18n.t("N.notifPart"), { type: "success" });
        return haveObjectParts;
      },
    },
    {
      type: "select",
      id: "notifPartCode",
      label: i18n.t("N.notifPartCode"),
      relatatedData: "notifPart",
      canOpen: () => {
        if (haveObjectParts) {
          if (!notifPart) toastRef.current.show("Vui lòng chọn " + i18n.t("N.notifPart"), { type: "warning" });
          return !!notifPart;
        } else {
          toastRef.current.show("Không cần chọn " + i18n.t("N.notifPartCode"), { type: "success" });
        }
        return false;
      },
    },
    {
      type: "select",
      id: "notifProblem",
      label: i18n.t("N.notifProblem"),
      canOpen: () => {
        if (!haveProblems) toastRef.current.show("Không cần chọn " + i18n.t("N.notifProblem"), { type: "success" });
        return haveProblems;
      },
    },
    {
      type: "select",
      id: "notifDame",
      label: i18n.t("N.notifDame"),
      relatatedData: "notifProblem",
      canOpen: () => {
        if (haveProblems) {
          if (!notifProblem) toastRef.current.show("Vui lòng chọn " + i18n.t("N.notifProblem"), { type: "warning" });
          return !!notifProblem;
        } else {
          toastRef.current.show("Không cần chọn " + i18n.t("N.notifDame"), { type: "success" });
        }
        return false;
      },
    },
    {
      type: "select",
      id: "notifCause",
      label: i18n.t("N.notifCause"),
      canOpen: () => {
        if (!haveCauses) toastRef.current.show("Không cần chọn " + i18n.t("N.notifCause"), { type: "success" });
        return haveCauses;
      },
    },
    {
      type: "select",
      id: "notifCauseCode",
      label: i18n.t("N.notifCauseCode"),
      relatatedData: "notifCause",
      canOpen: () => {
        if (haveCauses) {
          if (!notifCause) toastRef.current.show("Vui lòng chọn " + i18n.t("N.notifCause"), { type: "warning" });
          return !!notifCause;
        } else {
          toastRef.current.show("Không cần chọn " + i18n.t("N.notifCauseCode"), { type: "success" });
        }
        return false;
      },
    },
    {
      type: "input",
      id: "notifCauseText",
      label: "Mô tả nguyên nhân lỗi",
      plh: "Nhập mô tả nguyên nhân lỗi",
      multiline: 1,
      maxLength: 40,
    },
    {
      type: "input",
      id: "notifText",
      label: "Mô tả chi tiết",
      plh: "Nhập mô tả chi tiết",
      multiline: 1,
      maxLength: 40,
    },
  ];

  useEffect(() => {
    setItemState({ ...initState });
    setErrors({});
  }, [initState]);

  const handleCreateNotificationItem = async () => {
    let isEmptyItem = Object.values(itemState).every(i => !i);
    if (isEmptyItem) {
      return toastRef.current.show("Vui lòng chọn ít nhất một nhóm ", { type: "warning" });
    }
    if (!!itemState.notifPart && isEmpty(itemState.notifPartCode)) {
      return handleShowError("notifPartCode", "Vui lòng chọn " + i18n.t("N.notifPartCode"));
    }
    if (!!itemState.notifProblem && isEmpty(itemState.notifDame)) {
      return handleShowError("notifDame", "Vui lòng chọn " + i18n.t("N.notifDame"));
    }
    if (!!itemState.notifCause) {
      if (isEmpty(itemState.notifCauseCode))
        return handleShowError("notifCauseCode", "Vui lòng chọn " + i18n.t("N.notifCauseCode"));

      if (isEmpty(itemState.notifCauseText))
        return handleShowError("notifCauseText", "Vui lòng nhập Mô tả nguyên nhân lỗi");
    }
    if (!!itemState.notifProblem && isEmpty(itemState.notifPart)) {
      return handleShowError("notifPart", "Vui lòng chọn " + i18n.t("N.notifPart"));
    }
    onChange(itemState);
  };

  const handleShowError = (id: string, message: string) => {
    let _errors = { [id]: true };
    setErrors(_errors);
    toastRef.current.show(message, { type: "warning" });
  };

  const handleChangeItem = (id: any, data: any) => {
    let nextState = { [id]: data };
    let relatatedOptionDataKey = relatatedOptions[id];
    if (relatatedOptionDataKey) {
      let prevData = itemState[id];
      if (prevData && prevData.value !== data?.value) {
        nextState[relatatedOptionDataKey] = null;
        //onChange("cleanSelections", relatatedOptionDataKey);
        setOptions((prevState: any) => ({ ...prevState, [relatatedOptionDataKey]: [] }));
      }
    }
    setItemState((prevState: any) => ({ ...prevState, ...nextState }));
  };

  const handleSearch = (id: any, relatedData: any) => async (keyword: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];
    let params: any = {
      keyword,
      maintenancePlantCodes: [],
    };
    let searchServiceApi: any, mappingOptions;

    switch (id) {
      case "notifPart":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            catalog: notificationType?.objectParts,
          });
          searchServiceApi = MasterDataServices.getCodeGroups;
          mappingOptions = { valueKey: "codeGroup", labelKey: "codeGroupDesc" };
        }
        break;
      case "notifPartCode":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            codeGroup: relatedData.value,
            catalog: notificationType?.objectParts,
          });
          searchServiceApi = MasterDataServices.getCodes;
          mappingOptions = { valueKey: "code", labelKey: "codeDesc" };
        }
        break;
      case "notifDame":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            codeGroup: relatedData.value,
            catalog: notificationType?.problems,
          });
          searchServiceApi = MasterDataServices.getCodes;
          mappingOptions = { valueKey: "code", labelKey: "codeDesc" };
        }
        break;
      case "notifCauseCode":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            codeGroup: relatedData.value,
            catalog: notificationType?.causes,
          });
          searchServiceApi = MasterDataServices.getCodes;
          mappingOptions = { valueKey: "code", labelKey: "codeDesc" };
        }
        break;
      case "notifProblem":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            catalog: notificationType?.problems,
          });
          searchServiceApi = MasterDataServices.getCodeGroups;
          mappingOptions = { valueKey: "codeGroup", labelKey: "codeGroupDesc" };
        }
        break;
      case "notifCause":
        {
          Object.assign(params, {
            catalogProfile: notificationType?.catalogProfile,
            catalog: notificationType?.causes,
          });
          searchServiceApi = MasterDataServices.getCodeGroups;
          mappingOptions = { valueKey: "codeGroup", labelKey: "codeGroupDesc" };
        }
        break;
      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));

    //console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    // await miniTimer(500); // TEMP, fake loading API

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const _renderFormCreateItem = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        onModalHide={onModalHide}
        isFetching={isFetching}
        title={"Tạo thông báo chi tiết"}
        onBackdropPress={() => onClose()}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />

        <KeyboardAwareScrollView contentContainerStyle={styles.crudModalContent}>
          {forms.map((field: any) => {
            const { type, id, isSearch, relatatedData, ...fieldProps } = field;
            let _fieldProps = {
              key: id,
              isError: errors[id],
              value: itemState[id],
              onChange: (data: any) => handleChangeItem(id, data),
              ...fieldProps,
            };
            switch (type) {
              case "input":
                let isShow = !relatatedData || itemState[relatatedData];
                return isShow && <CTextInput {..._fieldProps} />;
              case "select": {
                let _onSearch = handleSearch(id, itemState[relatatedData]);
                let selectInputNode = (
                  <CSelect
                    {..._fieldProps}
                    isFullScreen
                    isFetching={fetchings[id]}
                    selected={itemState[id]}
                    menus={options[id]}
                    onSelect={(selected: any) => handleChangeItem(id, selected)}
                    onSearch={_onSearch}
                    {...(!isSearch && { onOpen: _onSearch })}
                  />
                );
                return selectInputNode;
              }
              default:
                return null;
            }
          })}
        </KeyboardAwareScrollView>

        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Đóng",
              onPress: () => onClose(),
            },
            {
              text: i18n.t(`CM.${itemAction}`),
              onPress: () => {
                handleCreateNotificationItem();
              },
            },
          ]}
        />
      </CModalViewer>
    );
  };

  return <>{_renderFormCreateItem()}</>;
};
export default NotificationItemCreation;

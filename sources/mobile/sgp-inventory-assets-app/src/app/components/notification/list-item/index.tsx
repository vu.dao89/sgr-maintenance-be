import { isEqual } from "lodash";
import React, { FC, Fragment, memo } from "react";
import { TouchableOpacity, View } from "react-native";

import i18n from "@I18n";

import styles from "./styles";

import { CTags, ImageCache, TextCM } from "@Components";

import { IMEQDefault } from "@Assets/image";

const NListItem: FC<any> = ({ item, index, style, noTimeDiff, onPress }) => {
  const { title, notificationCode, equipment, location, createdBy, assignTo, createdAt, tags, timePassedFromNow } =
    item;
  return (
    <Fragment>
      {!!index && <View style={styles.listSeparator} />}
      <TouchableOpacity style={[styles.listItemWrapper, style]} onPress={onPress}>
        <TextCM style={styles.listItemTitle}>
          {title || i18n.t("CM.unknown")} ({notificationCode})
        </TextCM>
        <View style={styles.listItemContainer}>
          <View style={styles.listItemContent}>
            {[
              [i18n.t("N.equipment"), equipment],
              [i18n.t("N.location"), location],
              [i18n.t("N.createdBy"), createdBy],
              [i18n.t("N.assignTo"), assignTo || i18n.t("CM.unknown")],
              [i18n.t("N.createdAt"), createdAt],
            ].map(([label, value]) => (
              <View key={label} style={[styles.listItemInfoRow, !index && { marginTop: 0 }]}>
                <TextCM style={styles.listItemInfoLabel}>{label}:</TextCM>
                <TextCM style={styles.listItemInfoValue}>{value}</TextCM>
              </View>
            ))}
          </View>
          <View style={styles.listItemThumbnailWrapper}>
            <ImageCache imageDefault={IMEQDefault} style={styles.listItemThumbnailImg} />
          </View>
        </View>
        <View style={styles.listItemFooter}>
          <CTags data={tags} />
          {!noTimeDiff && <TextCM style={styles.listItemTimePassed}>{timePassedFromNow}</TextCM>}
        </View>
      </TouchableOpacity>
    </Fragment>
  );
};

const propsAreEqual = (prevProps: any, nextProps: any) =>
  ["item", "index"].every(i => isEqual(prevProps[i], nextProps[i]));

export default memo(NListItem, propsAreEqual);

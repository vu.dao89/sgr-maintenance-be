import { StyleSheet } from "react-native";

import { Color, FontSize, ScreenWidth, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  listSeparator: {
    width: ScreenWidth - WR(24) * 2,
    height: 1,
    marginHorizontal: WR(24),
    backgroundColor: "#414042",
  },
  listItemWrapper: {
    paddingVertical: WR(16),
    paddingHorizontal: WR(24),
  },
  listItemTitle: {
    fontWeight: "700",
    marginBottom: WR(12),
    color: Color.White,
  },
  listItemInfoRow: {
    minHeight: WR(20),
    flexDirection: "row",
    overflow: "hidden",
  },
  listItemInfoLabel: {
    marginRight: WR(4),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  listItemInfoValue: {
    flex: 1,
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  listItemTimePassed: {
    marginTop: WR(4),
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  listItemContainer: {
    flexDirection: "row",
  },
  listItemContent: {
    flex: 1,
    marginRight: WR(16),
  },
  listItemThumbnailWrapper: {
    width: WR(56),
    height: WR(56),
  },
  listItemThumbnailImg: {
    width: WR(56),
    height: WR(56),
    borderRadius: WR(16),
  },
});

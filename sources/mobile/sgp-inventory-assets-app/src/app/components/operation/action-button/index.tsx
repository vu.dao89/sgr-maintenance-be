import { CTAButtons } from "@Components";
import React, { FC } from "react";
import { useSelector } from "react-redux";
import { OperationHistoryActivity, Permission } from "../../../../constants";
import SystemStatus from "../../../../constants/SystemStatus";
import i18n from "../../../../i18n";
import { AppState } from "../../../../redux/reducers";
import { PermissionServices } from "../../../../services";
import { dayjsGetDate } from "../../../../utils/DateTime";
import { convertMsToTime } from "../../../../utils/Format";

import styles from "./styles";

type Props = {
  historyActivity: any;
  actualStart: any;
  actualStarTime: any;
  operationStatusCode: any;
  handleAction: any;
  isSticky: any;
  isBlueStyle: any;
  isDisabled: boolean;
};

const OperationActionButton: FC<Props> = ({
  historyActivity,
  actualStart,
  actualStarTime,
  operationStatusCode,
  handleAction,
  isSticky,
  isBlueStyle,
  isDisabled,
}) => {
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  if (!PermissionServices.checkPermission(reducer, Permission.OPERATION.CONFIRM_START)) {
    return null;
  }

  let actionNote;

  switch (true) {
    case historyActivity === OperationHistoryActivity.START: // Đã bắt đầu công việc
      {
        actionNote = (
          <CTAButtons
            isSticky={isSticky}
            styles={styles.ctaButton}
            buttons={[
              {
                isSub: true,
                text: "Tạm dừng",
                disabled: isDisabled,
                onPress: () => {
                  handleAction && handleAction(OperationHistoryActivity.HOlD);
                },
              },
              {
                text: "Hoàn tất",
                disabled: isDisabled,
                onPress: () => {
                  handleAction && handleAction(OperationHistoryActivity.COMPLETE);
                },
                isBlueStyle,
              },
            ]}
          />
        );
      }
      break;

    case operationStatusCode === SystemStatus.operation.READY && historyActivity === OperationHistoryActivity.NONE: // Chưa bắt đầu công việc
      {
        actionNote = (
          <CTAButtons
            isSticky={isSticky}
            styles={styles.ctaButton}
            buttons={[
              {
                text: i18n.t(`CM.startJob`),
                onPress: () => {
                  handleAction && handleAction(OperationHistoryActivity.START);
                },
                disabled: isDisabled,
                isBlueStyle,
              },
            ]}
          />
        );
      }
      break;

    case operationStatusCode == SystemStatus.operation.A_HAFT: // Đang tạm dừng công việc
      {
        const now = new Date();
        const startTime = dayjsGetDate(`${actualStart}${actualStarTime}`).getTime();
        const duration = convertMsToTime(Math.floor(now.getTime() - startTime));

        actionNote = (
          <CTAButtons
            isSticky={isSticky}
            styles={styles.ctaButton}
            buttons={[
              {
                text: `(${duration}) Tiếp tục việc`,
                onPress: () => {
                  handleAction && handleAction(OperationHistoryActivity.START);
                },
                isBlueStyle,
                disabled: isDisabled,
              },
            ]}
          />
        );
      }
      break;
  }

  return <>{actionNote}</>;
};

export default OperationActionButton;

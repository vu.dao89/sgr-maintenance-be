import { StyleSheet } from "react-native";

import { widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  ctaButton: {
    flex: 1,
    height: WR(36),
    marginBottom: WR(4),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
  },
});

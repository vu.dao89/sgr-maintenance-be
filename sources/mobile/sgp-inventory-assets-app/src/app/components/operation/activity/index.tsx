import React, { FC, useEffect, useState } from "react";
import { TouchableOpacity, View } from "react-native";
import Collapsible from "react-native-collapsible";
import { Shadow } from "react-native-shadow-2";

import styles from "./styles";

import { OperationHistoryActivity, ScreenName, widthResponsive as WR } from "@Constants";

import { OperationConfirmation, TextCM } from "@Components";

import { ICPauseCircle, ICPlayCircle } from "@Assets/image";
import { useNavigation } from "@react-navigation/native";
import dayjs from "dayjs";
import { get } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../../redux/actions";
import { OperationServices } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { useIsMounted } from "../../../../utils/Core";
import { dayjsGetDate } from "../../../../utils/DateTime";
import { convertMsToTime } from "../../../../utils/Format";

const statusMappings: any = {
  processing: ["#74A4FF", "ĐANG THỰC HIỆN"],
  paused: ["#FFC20E", "TẠM DỪNG"],
};

const OperationActivity: FC<any> = ({ saInsets, isShow, onVisible, onChange }) => {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();
  const toast = useToast();
  const nav = useNavigation();

  const [activeOperation, setActiveOpeation]: any = useState(null);
  const [isShowConfirmation, setShowConfirmation] = useState(false);

  useEffect(() => {
    handleGetCurrentActivity();
    // @ts-ignore
    global["operation_activity_cb"] = () => handleGetCurrentActivity();
    return () => {
      // @ts-ignore
      delete global["operation_activity_cb"];
    };
  }, []);

  const handleOnChange = (id: any, data: any) => {
    handleGetCurrentActivity();
  };

  const handleGetCurrentActivity = async () => {
    const { response } = await axiosHandler(() => OperationServices.getCurrentOperation());

    if (!isMounted()) return;

    const operation = get(response, "data.operation");

    if (operation) {
      const { operationId, operationDescription, workOrderId, actualStart, actualStartTime, operationHistory } =
        operation;

      const status = operationHistory?.activity === OperationHistoryActivity.START ? "processing" : "paused";

      const startTime =
        status === "paused"
          ? dayjsGetDate(`${actualStart}${actualStartTime}`).getTime()
          : dayjs.unix(operationHistory?.startDateTime).toDate().getTime();

      const now = new Date();

      const _activeOperation = {
        operationId: operationId,
        workOrderId: workOrderId,
        title: operationDescription,
        duration: convertMsToTime(Math.floor(now.getTime() - startTime)),
        status: status,
        actualStart: actualStart,
        actualStartTime: actualStartTime,
        operationHistory: operationHistory,
      };
      setActiveOpeation(_activeOperation);
      onVisible(true);
    } else {
      onVisible(false);
    }
  };

  const handleToggleStatus = async (isPaused: any) => {
    const { workOrderId, operationId } = activeOperation;
    if (isPaused) {
      dispatch(CommonActions.openLoading.request({}));

      const { error } = await axiosHandler(() => OperationServices.startOperation(workOrderId, operationId));

      dispatch(CommonActions.openLoading.success({}));
      if (!isMounted()) return;

      if (error) {
        toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
      } else {
        toast.show("Đã bắt đầu công việc thành công", { type: "success" });
        handleGetCurrentActivity();
        onChange();
      }
    } else {
      setShowConfirmation(true);
    }
  };

  let activityNode;

  if (activeOperation) {
    const { operationId, title, status, duration } = activeOperation;
    let [statusColor, statusLabel] = statusMappings[status];
    let isPaused = status === "paused";
    activityNode = (
      <View style={styles.activityContent}>
        {/* <Image source={{ uri: equipment.image }} style={styles.activityImage} /> */}
        <View style={styles.activityInfo}>
          <TextCM numberOfLines={1} style={styles.activityTitle}>
            {title}
          </TextCM>
          <View style={styles.activityFooter}>
            <TextCM style={styles.activityId}>{operationId}</TextCM>
            <View style={styles.activityStatus}>
              <View style={[styles.activityStatusDot, { backgroundColor: statusColor }]} />
              <TextCM style={[styles.activityStatusTxt, { color: statusColor }]}>{statusLabel}</TextCM>
            </View>
          </View>
        </View>
        <TextCM style={styles.activityDuration}>{duration}</TextCM>
        <TouchableOpacity style={styles.activityToggleStatusBtn} onPress={() => handleToggleStatus(isPaused)}>
          {isPaused ? <ICPlayCircle /> : <ICPauseCircle />}
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={styles.activityWrapper}>
      <Shadow distance={16} startColor={"#00000040"} offset={[3, 4]} style={{ width: "100%" }}>
        {/* @ts-ignore */}
        <Collapsible collapsed={!isShow} style={[styles.activityContainer, { height: saInsets.bottom + WR(64) }]}>
          <TouchableOpacity
            onPress={() => {
              nav.navigate(
                ScreenName.OperationDetail as never,
                {
                  workOrderId: activeOperation?.workOrderId,
                  operationId: activeOperation?.operationId,
                  callback: "operation_list_cb",
                } as never
              );
            }}>
            {activityNode}
          </TouchableOpacity>
        </Collapsible>
      </Shadow>
      <OperationConfirmation
        saInsets={saInsets}
        workOrderId={activeOperation?.workOrderId}
        operationId={activeOperation?.operationId}
        isShow={isShowConfirmation}
        onChange={handleOnChange}
        onClose={() => setShowConfirmation(false)}
        {...activeOperation}
        type={"hold"}
        toast={toast}
        operationDescription={activeOperation?.title}
      />
    </View>
  );
};

export default OperationActivity;

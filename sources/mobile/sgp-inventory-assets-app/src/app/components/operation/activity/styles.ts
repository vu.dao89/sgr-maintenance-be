import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  activityWrapper: {
    position: "absolute",
    zIndex: 1,
    bottom: 0,
    left: 0,
    right: 0,
  },
  activityContainer: {
    backgroundColor: "#262745",
    borderTopLeftRadius: WR(20),
    borderTopRightRadius: WR(20),
  },
  activityContent: {
    height: WR(64),
    paddingVertical: WR(12),
    paddingHorizontal: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  activityImage: {
    width: WR(40),
    height: WR(40),
    borderRadius: WR(16),
  },
  activityInfo: {
    flex: 1,
    marginLeft: WR(4),
  },
  activityTitle: {
    marginBottom: WR(4),
    color: Color.White,
    fontWeight: "700",
  },
  activityFooter: {
    flexDirection: "row",
    alignItems: "center",
  },
  activityId: {
    color: "#ACACAC",
    fontSize: FontSize.FontTiny,
  },
  activityStatus: {
    marginLeft: WR(8),
    flexDirection: "row",
    alignItems: "center",
  },
  activityStatusDot: {
    marginTop: WR(1),
    width: WR(6),
    height: WR(6),
    borderRadius: WR(3),
  },
  activityStatusTxt: {
    marginLeft: WR(4),
    fontWeight: "600",
    fontSize: FontSize.FontTiny,
    textTransform: "uppercase",
  },
  activityDuration: {
    marginRight: WR(6),
    marginLeft: WR(6),
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  activityToggleStatusBtn: {},
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
  btnSelectImg: {
    marginBottom: WR(16),
  },
  btnSelectImgTxt: {
    color: Color.Yellow,
    fontWeight: "600",
  },
});

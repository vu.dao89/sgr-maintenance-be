import React, { FC } from "react";

import styles from "./styles";

import { ScreenWidth as SW, widthResponsive as WR } from "@Constants";

import { CAttachFiles, CCollapsibleSection, TextCM } from "@Components";

type Props = {
  index?: any;
  header?: any;
  task?: any;
  style?: any;
  canRemove?: any;
  onChange?: any;
};

const OperationAttachFiles: FC<Props> = ({
  index,
  header,
  task = {},
  style,
  canRemove,
  onChange = () => null,
}) => {
  const { title, desc, fullDescription, files } = task;

  return (
    <CCollapsibleSection
      title={header || title || fullDescription}
      styles={[
        {
          wrapper: { marginTop: index ? WR(20) : 0 },
          container: { paddingBottom: WR(4) },
        },
        style,
      ]}>
      {!!desc && <TextCM style={styles.attachDesc}>{desc}</TextCM>}
      {!!fullDescription && <TextCM style={styles.attachDesc}>{fullDescription}</TextCM>}
      <CAttachFiles
        simpleAddBtn
        borderFile
        canRemove={canRemove}
        files={files || []}
        viewerMaxWidth={SW - WR((24 + 20) * 2)}
        style={{ marginBottom: WR(files?.length ? 10 : 20) }}
        onChange={onChange}
      />
    </CCollapsibleSection>
  );
};

export default OperationAttachFiles;

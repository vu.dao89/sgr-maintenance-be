import { StyleSheet } from "react-native";

import { Color, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  attachDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
});

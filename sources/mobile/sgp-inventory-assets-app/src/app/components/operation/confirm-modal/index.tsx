import { widthResponsive as WR } from "@Constants";
import dayjs from "dayjs";
import { log } from "libs/react-native-x-framework/js/logger";
import { isEmpty, isNull } from "lodash";
import React, { FC, useEffect, useRef, useState } from "react";
import { ScrollView } from "react-native";
import Toast from "react-native-toast-notifications";
import { OperationServices } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { OperationConfirmParamms } from "../../../../services/parameters";
import { useIsMounted } from "../../../../utils/Core";
import { dayjsFormatDate, formatDate, SAP_DATE_FORMAT, SAP_TIME_FORMAT } from "../../../../utils/DateTime";
import { CAttachFiles, CDatePicker, CInfoViewer, CModalViewer, CSelect, CTAButtons, CTextInput } from "../../common";
import { CFile } from "../../common/attach-files";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  workOrderId: any;
  operationId: any;
  operationDescription: any;
  operationHistory: any;
  isShow: any;
  onChange: any;
  onClose: any;
  type: any;
  toast: any;
};

const OperationConfirmation: FC<PropsType> = ({
  saInsets,
  workOrderId,
  operationId,
  operationDescription,
  operationHistory,
  isShow,
  onChange,
  onClose,
  type,
  toast,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [notes, setNotes] = useState("");
  const [attachFiles, setAttachFiles]: any = useState([]);
  const [varianceReasonCode, setVarianceReasonCode]: any = useState(null);
  const [endDate, setEndDate]: any = useState(null);
  const [isFetching, setFetching] = useState(false);

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  const [errors, setErrors]: any = useState({});
  const isSpecial = ["P", "X"].includes(varianceReasonCode?.value);

  useEffect(() => {
    handleResetForm();
  }, [isShow]);

  const handleAttachFiles = async (files: Array<CFile>) => {
    setAttachFiles([...files, ...attachFiles]);
  };

  const handleOnCTA = () => {
    const _error = {
      endDate: isNull(endDate),
      varianceReasonCode: isNull(varianceReasonCode),
      notes: isSpecial ? false : isEmpty(notes),
    };

    setErrors({ ..._error });

    if (_error.endDate || _error.varianceReasonCode || _error.notes) {
      return toastRef.current.show("Vui lòng nhập đầy đủ thông tin", { type: "warning" });
    }

    if (!isSpecial && isEmpty(attachFiles)) {
      return toastRef.current.show(`Vui lòng đính kèm hình ảnh`, {
        type: "warning",
      });
    }

    if (type === "hold") {
      handleHoldOperation();
    } else {
      handleCompleteOperation();
    }
  };

  const getConfirmParams = () => {
    const startTime = dayjs.unix(operationHistory?.startDateTime).toDate();
    const actualWork = Math.floor((endDate.getTime() - startTime.getTime()) / 60000);

    const params: OperationConfirmParamms = {
      workOrderId: workOrderId,
      operationId: operationId,
      superOperationId: "",
      actualWork: `${actualWork}`,
      workStartDate: dayjsFormatDate(startTime, SAP_DATE_FORMAT),
      workStartTime: dayjsFormatDate(startTime, SAP_TIME_FORMAT),
      workFinishDate: formatDate(endDate, SAP_DATE_FORMAT),
      workFinishTime: formatDate(endDate, SAP_TIME_FORMAT),
      varianceReasonCode: varianceReasonCode?.value || "",
      confirmation: notes,
      files: attachFiles,
    };

    log("getConfirmParams", params);

    return params;
  };

  const handleHoldOperation = async () => {
    setFetching(true);

    const { error } = await axiosHandler(() => OperationServices.holdOperation(getConfirmParams()));

    if (!isMounted()) return;
    setFetching(false);

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      handleCallback("hold", "Đã tạm dừng công việc thành công");
    }
  };

  const handleCompleteOperation = async () => {
    setFetching(true);

    const { error } = await axiosHandler(() => OperationServices.completeOperation(getConfirmParams()));

    if (!isMounted()) return;
    setFetching(false);

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      handleCallback("complete", "Đã hoàn thành công việc thành công");
    }
  };

  const handleCallback = (id: string, message: string) => {
    toast.show(message, { type: "success" });
    handleReloadCurrentOperation();
    onChange(id);
    onClose();
  };

  const handleReloadCurrentOperation = () => {
    // @ts-ignore
    (global["operation_activity_cb"] || (() => null))();
  };

  const handleResetForm = () => {
    const defaultReason =
      type === "hold"
        ? {
            label: "TẠM DỪNG CÔNG VIỆC",
            value: "X",
          }
        : null;
    setErrors({
      endDate: false,
      varianceReasonCode: false,
      notes: false,
    });
    setVarianceReasonCode(defaultReason);
    setEndDate(null);
    setNotes("");
  };

  return (
    <CModalViewer
      open={isShow}
      placement={"bottom"}
      isFetching={isFetching}
      title={
        type === "hold"
          ? `Tạm dừng công việc:\r\n${operationDescription}`
          : `Hoàn thành công việc:\r\n${operationDescription}`
      }
      onBackdropPress={onClose}>
      <Toast ref={(ref: any) => (toastRef.current = ref)} />
      <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom + WR(12) }]}>
        <CInfoViewer
          infos={[
            {
              label: "Thời gian bắt đầu",
              value: dayjsFormatDate(dayjs.unix(operationHistory?.startDateTime).toDate(), "DD/MM/YYYY - HH:mm:ss"),
            },
          ]}
        />
        <CDatePicker
          value={endDate}
          isError={errors.endDate}
          onChange={setEndDate}
          mode={"datetime"}
          isRequired
          label={type === "hold" ? "Thời gian tạm dừng" : "Thời gian kết thúc"}
          maximumDate={new Date()}
        />
        <CSelect
          isRequired
          isDisabled={type === "hold"}
          isError={errors.varianceReasonCode}
          label={"Kết quả công việc"}
          selected={varianceReasonCode}
          onSelect={setVarianceReasonCode}
          menus={[
            {
              label: "KHÔNG ĐẠT [không vận hành]",
              value: "F",
            },
            {
              label: "KHÔNG KIỂM TRA",
              value: "N/A",
            },
            {
              label: "ĐẠT",
              value: "P",
            },
            {
              label: "ĐẠT, NHƯNG CẦN GIÁM SÁT",
              value: "S/M",
            },
          ]}
        />
        <CTextInput
          multiline={2}
          isError={errors.notes}
          isRequired={!isSpecial}
          label={type === "hold" ? "Lý do tạm dừng" : "Mô tả chi tiết"}
          plh={type === "hold" ? "Nhập lý do dừng" : "Nhập mô tả chi tiết"}
          value={notes}
          onChange={setNotes}
        />
        <CAttachFiles files={attachFiles} simpleAddBtn onChange={handleAttachFiles} />
      </ScrollView>
      <CTAButtons
        isSticky
        buttons={[
          {
            text: type == "hold" ? "Dừng" : "Hoàn tất",
            onPress: () => {
              handleOnCTA();
            },
          },
        ]}
      />
    </CModalViewer>
  );
};
export default OperationConfirmation;

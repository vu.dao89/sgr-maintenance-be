import { StyleSheet } from "react-native";

import { widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
});

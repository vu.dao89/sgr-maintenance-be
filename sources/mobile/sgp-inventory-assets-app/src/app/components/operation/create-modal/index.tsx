import { widthResponsive as WR } from "@Constants";
import { get } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-toast-notifications";
import { useSelector } from "react-redux";
import { ICGhost, ICQRC } from "../../../../assets/image";
import i18n from "../../../../i18n";
import { AppState } from "../../../../redux/reducers";
import { EquipmentServices, MasterDataServices, PermissionServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { genRandomString } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatWorkCenterLabel,
  mappingSelections,
} from "../../../../utils/Format";
import {
  CAlertModal,
  CInfoViewer,
  CModalViewer,
  CScanQRCodeModal,
  CSelect,
  CSpinkit,
  CTAButtons,
  CTextInput,
} from "../../common";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  taskAction: string;
  initState: any;
  onClose: any;
  onChange: any;
};

const relatatedOptions: any = {
  functionalLocation: "equipment",
};

const OperationCreation: FC<PropsType> = ({ saInsets, isCrud, taskAction, initState, onClose, onChange }) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [taskState, setTaskState]: any = useStateLazy({});
  const [taskErrors, setTaskErrors] = useStateLazy({});

  const [isScanError, setScanError] = useState(false);
  const [isScanData, setScanDataStatus] = useState(false);
  const [isScanFetching, setScanFetching] = useStateLazy(false);

  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});
  const reqIds: any = useRef({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);

  const taskForms = [
    {
      type: "input",
      id: "fullDescription",
      label: "Mô tả chi tiết",
      plh: "Nhập mô tả chi tiết",
      multiline: 2,
      maxLength: 5000,
      isRequired: true,
    },
    {
      type: "select",
      id: "activityType",
      label: "Phân loại công việc",
      isRequired: true,
      canOpen: () => {
        if (!taskState.workCenter) toastRef.current.show("Vui lòng chọn Tổ đội thực hiện", { type: "warning" });
        return !!taskState.workCenter;
      },
    },
    {
      type: "select",
      id: "controlKey",
      label: "Nguồn lực",
      isRequired: true,
    },
    {
      type: "select",
      id: "maintPlant",
      label: "Nơi bảo trì",
      isRequired: true,
    },
    {
      type: "select",
      id: "workCenter",
      label: "Tổ đội thực hiện",
      isRequired: true,
    },
    {
      type: "select",
      id: "functionalLocation",
      label: "Khu vực chức năng",
      isRequired: true,
      canScan: true,
    },
    {
      type: "select",
      id: "equipment",
      label: "Mã thiết bị",
      isRequired: true,
      canScan: true,
      canOpen: () => {
        // TODO: need to convert toast.show to showToast() utils later...
        if (!taskState.functionalLocation)
          toastRef.current.show("Vui lòng chọn Khu vực chức năng", { type: "warning" });
        return !!taskState.functionalLocation;
      },
    },
    {
      type: "select",
      id: "personnel",
      label: "Người chịu trách nhiệm",
      isRequired: true,
    },

    {
      type: "input",
      id: "estimate",
      label: "Thời gian làm việc dự kiến",
      plh: "Nhập thời gian",
      keyboardType: "numeric",
      returnKeyType: "done",
    },
    {
      type: "info",
      id: "unit",
      label: "Đơn vị tính",
      value: "Phút",
    },
  ];

  useEffect(() => {
    setTaskState({
      ...initState,
    });
  }, [initState]);

  const handleCrudTask = () => {
    let isValidData = true,
      _errors: any = {},
      focusInputField: any = null,
      setInvalid = (id: any) => {
        isValidData = false;
        _errors[id] = true;
        if (!focusInputField) focusInputField = id;
      };

    [
      "fullDescription",
      "activityType",
      "controlKey",
      "maintPlant",
      "workCenter",
      "personnel",
      "functionalLocation",
      "equipment",
      "estimate",
    ].forEach((requiredField: any) => {
      if (!taskState[requiredField]) setInvalid(requiredField);
    });

    setTaskErrors(_errors);

    if (isValidData) {
      let task: any;
      if (taskAction === "create") {
        task = {
          ...taskState,
          uuid: genRandomString(),
          fullDescription: (taskState.fullDescription || "").trim(),
          childs: [],
          files: [],
        };
      } else {
        task = {
          ...taskState,
          fullDescription: (taskState.fullDescription || "").trim(),
        };
      }
      onChange(task);
      onClose();
    } else {
      // TODO: scroll into focusInputField
      toastRef.current.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
    }
  };

  const handleChange = (id: any, data: any) => {
    let nextState: any = { [id]: data },
      nextOptsState: any = {};
    let relatatedOptionDataKey = relatatedOptions[id];
    if (relatatedOptionDataKey && !!data) {
      nextState[relatatedOptionDataKey] = null;
      nextOptsState[relatatedOptionDataKey] = [];
    }
    switch (id) {
      case "workCenter":
        {
          if (data && !taskState.maintPlant) {
            nextState.maintPlant = data.maintPlant;
          }
        }
        break;
      case "equipment":
        {
          if (!!data && !taskState.functionalLocation) {
            nextState.functionalLocation = data.functionalLocation;
          }
        }
        break;
      default:
        break;
    }
    setTaskState((prevState: any) => ({ ...prevState, ...nextState }));
    setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
    setTaskErrors((prevState: any) => ({ ...prevState, [id]: false }));
  };

  const handleSearchScanResult = async (qrCode: any) => {
    setScanDataStatus(false);
    await setScanFetching(true);

    const { response, error } = await axiosHandler(() => MasterDataServices.getByQRCode(qrCode), { delay: true });
    // console.log(`🚀 Kds: handleSearchScanResult -> qrCode, response, error`, qrCode, response, error);

    if (!isMounted()) return;

    await setScanFetching(false);

    const { equipment, functionalLocation } = get(response, "data", {});

    if (!equipment && !functionalLocation) return setScanError(true);

    switch (true) {
      case !!functionalLocation:
        {
          let functionalLocationSelection = mappingSelections([functionalLocation], {
            valueKey: "functionalLocationId",
          })[0];
          handleChange("functionalLocation", functionalLocationSelection);
        }
        break;
      case !!equipment:
        {
          const { functionalLocation: _functionalLocation, functionalLocationId } = equipment;
          let functionalLocationSelection = _functionalLocation
            ? mappingSelections([_functionalLocation], { valueKey: "functionalLocationId" })[0]
            : { value: functionalLocationId, label: functionalLocationId };
          let equipmentSelection = mappingSelections([equipment], { valueKey: "equipmentId" })[0];
          handleChange("functionalLocation", functionalLocationSelection);
          handleChange("equipment", equipmentSelection);
        }
        break;
      default:
        break;
    }
  };

  const handleSearch = (id: any) => async (keyword?: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];

    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(taskState.maintPlant ? [taskState.maintPlant] : [], userPlants),
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "maintPlant":
        {
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenter: i.costCenter,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "functionalLocation":
        {
          searchServiceApi = MasterDataServices.getFunctionalLocations;
          mappingOptions = {
            valueKey: "functionalLocationId",
            format: (i: any) => ({ label: formatObjectLabel(i.functionalLocationId, i.description, true) }),
          };
        }
        break;
      case "equipment":
        {
          if (taskState.functionalLocation) params.functionalLocationCodes = [taskState.functionalLocation.value];
          searchServiceApi = EquipmentServices.getEquipments;
          mappingOptions = {
            valueKey: "equipmentId",
            format: (i: any) => ({
              label: formatObjectLabel(i.equipmentId, i.description, true),
              functionalLocation: {
                label: formatObjectLabel(i.functionalLocationId, i.functionalLocation?.description),
                value: i.functionalLocationId,
              },
            }),
          };
        }
        break;
      case "systemCondition":
        {
          searchServiceApi = MasterDataServices.getSystemConditions;
          mappingOptions = { labelKey: "text" };
        }
        break;
      case "personnel":
        {
          if (taskState.workCenter) params.workCenterIds = [taskState.workCenter.value];
          searchServiceApi = MasterDataServices.getPersonnel;
          mappingOptions = { labelKey: "name", format: (i: any) => ({ fullName: i.name }) };
        }
        break;
      case "activityType":
        {
          const costCenter = get(taskState, "workCenter.costCenter");
          if (costCenter) params.costCenterCodes = [costCenter];
          searchServiceApi = MasterDataServices.getActivityTypes;
          mappingOptions = { valueKey: "type", labelKey: "description" };
        }
        break;
      case "controlKey":
        {
          searchServiceApi = MasterDataServices.getControlKey;
        }
        break;

      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));
    // console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isScanData}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setScanDataStatus(false)}
          onResult={handleSearchScanResult}
        />
        <CAlertModal
          open={isScanError}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => setScanError(false)}
        />
      </Fragment>
    );
  };

  const _renderCrudTask = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        title={i18n.t(`CM.${taskAction}`) + " công việc"}
        onBackdropPress={onClose}>
        <KeyboardAwareScrollView contentContainerStyle={styles.crudModalContent}>
          {taskForms.map((field: any) => {
            const { type, id, isSearch, canScan, relatatedData, ...fieldProps } = field;
            let _fieldProps = {
              key: id,
              isError: taskErrors[id],
              value: taskState[id],
              onChange: (data: any) => handleChange(id, data),
              ...fieldProps,
            };
            switch (type) {
              case "input":
                return <CTextInput {..._fieldProps} />;
              case "select": {
                let _onSearch = handleSearch(id);
                let selectInputNode = (
                  <CSelect
                    {..._fieldProps}
                    isFullScreen
                    isFetching={fetchings[id]}
                    selected={taskState[id]}
                    menus={options[id]}
                    styles={canScan && { wrapper: { flex: 1, marginRight: WR(20) } }}
                    onSelect={(selected: any) => handleChange(id, selected)}
                    onSearch={_onSearch}
                    {...(!isSearch && { onOpen: _onSearch })}
                  />
                );
                if (canScan) {
                  return (
                    <View key={id} style={styles.row}>
                      {selectInputNode}
                      <TouchableOpacity onPress={() => setScanDataStatus(true)}>
                        <ICQRC />
                      </TouchableOpacity>
                    </View>
                  );
                }
                return selectInputNode;
              }
              case "info": {
                return <CInfoViewer key={id} infos={[fieldProps]} />;
              }
              default:
                return null;
            }
          })}
        </KeyboardAwareScrollView>
        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Đóng",
              onPress: () => onClose(),
            },
            {
              text: i18n.t(`CM.${taskAction}`),
              onPress: () => handleCrudTask(),
            },
          ]}
        />
        <Toast ref={(ref: any) => (toastRef.current = ref)} />
        {_renderScanQRCode()}
        {isScanFetching && (
          <View style={styles.fetchingDataOverlay}>
            <CSpinkit />
          </View>
        )}
      </CModalViewer>
    );
  };

  return <>{_renderCrudTask()}</>;
};
export default OperationCreation;

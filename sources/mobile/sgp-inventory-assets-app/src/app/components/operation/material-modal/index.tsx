import React, { FC, useEffect, useRef } from "react";
import { ScrollView } from "react-native";
import Toast from "react-native-toast-notifications";

import { CCollapsibleSection, CEmptyState, CInfoViewer, CModalViewer, CTAButtons, CTextInput } from "@Components";
import { widthResponsive as WR } from "@Constants";
import { isArray } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { dayjsFormatDate } from "../../../../utils/DateTime";
import { formatObjectLabel } from "../../../../utils/Format";

import { isEmpty } from "lodash";
import { OperationServices } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  workOrderId: string;
  operationId: string;
  initState: any;
  onClose: any;
  onChange: any;
};

const OperationMaterialSubmit: FC<PropsType> = ({
  saInsets,
  isCrud,
  initState,
  workOrderId,
  operationId,
  onClose,
  onChange,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [exportingMaterials, setExportingMaterials]: any = useStateLazy([]);
  const [errors, setErrors]: any = useStateLazy([]);
  const [isFetching, setFetching]: any = useStateLazy(false);

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  useEffect(() => {
    setExportingMaterials([...initState]);
  }, [initState]);

  const isValidWithdrawalQuantity = (material: any) => {
    const { withdrawalQuantity, remainQuantity } = material;
    return !isEmpty(withdrawalQuantity) && withdrawalQuantity <= remainQuantity;
  };

  const handleExportMaterials = async () => {
    let isValid = false;
    const _errors = exportingMaterials.map((ma: any) => {
      const valid = isValidWithdrawalQuantity(ma);
      if (valid) isValid = true;
      return !valid;
    });
    setErrors(_errors);

    if (!isValid) {
      return toastRef.current.show("Vui lòng nhập số lượng xuất kho hợp lệ", { type: "danger" });
    }

    setFetching(true);

    const items =
      exportingMaterials
        ?.filter((m: any) => isValidWithdrawalQuantity(m))
        ?.map((mate: any) => {
          return {
            reservation: mate.reservation,
            reservationItem: mate.reservationItem,
            material: mate.material,
            quantity: mate.withdrawalQuantity,
          };
        }) || [];

    const { error } = await axiosHandler(() =>
      OperationServices.createMaterialDocument(workOrderId, operationId, items)
    );

    if (!isMounted()) return;
    setFetching(false);

    if (error) {
      toastRef.current.show(getErrorDetail(error)?.errorMessage, { type: "success" });
    } else {
      onChange();
    }
  };

  const _renderModalExportingMaterials = () => (
    <CModalViewer
      open={isCrud}
      placement={"bottom"}
      title={"Xuất kho vật tư"}
      onBackdropPress={() => onClose()}
      isFetching={isFetching}>
      <Toast ref={(ref: any) => (toastRef.current = ref)} />
      <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom + WR(12) }]}>
        {isArray(exportingMaterials, 1) &&
          exportingMaterials.map((material: any, index: any) => (
            <CCollapsibleSection
              borderBg
              key={`${material.workOrderId}_${index}`}
              title={`Vật tư ${index + 1}`}
              styles={{ wrapper: { marginTop: 10 } }}
              infos={[
                {
                  label: "Vật tư",
                  value: formatObjectLabel(material.material, material.materialDescription, true),
                },
                {
                  label: "Kho bảo trì",
                  value: formatObjectLabel(material.plant, material.plantName, true),
                },
                {
                  label: "Kho vật lý",
                  value: formatObjectLabel(material.storageLocation, material.storageLocationDescription, true),
                },
                {
                  label: "Lô hàng",
                  value: `${dayjsFormatDate(material.batch)}`,
                },
              ]}>
              <CTextInput
                isRequired
                fullWidth
                label={"Số lượng xuất kho"}
                plh={"Nhập số lượng (cái)"}
                isError={errors[index]}
                value={material.withdrawalQuantity}
                keyboardType="numeric"
                styles={{ wrapper: { marginRight: WR(8) } }}
                onChange={(point: any) => {
                  Object.assign(exportingMaterials[index], { ...exportingMaterials[index], withdrawalQuantity: point });
                  setExportingMaterials([...exportingMaterials]);
                }}
              />
              <CInfoViewer
                infos={[
                  {
                    label: "Số lượng còn lại",
                    value: `${material.remainQuantity}`,
                  },
                  {
                    label: "Ghi chú",
                    value: `${material.note}`,
                  },
                ]}
              />
            </CCollapsibleSection>
          ))}
        {!isArray(exportingMaterials, 1) && (
          <>
            <CEmptyState />
          </>
        )}
      </ScrollView>
      {isArray(exportingMaterials, 1) && (
        <>
          <CTAButtons
            isSticky
            buttons={[
              {
                text: "Xuất kho",
                onPress: () => handleExportMaterials(),
              },
            ]}
          />
        </>
      )}
    </CModalViewer>
  );

  return <>{_renderModalExportingMaterials()}</>;
};
export default OperationMaterialSubmit;

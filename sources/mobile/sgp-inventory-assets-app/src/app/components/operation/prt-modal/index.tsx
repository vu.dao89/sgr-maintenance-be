import { widthResponsive as WR } from "@Constants";
import { isEmpty } from "lodash";
import moment from "moment";
import React, { FC, useEffect, useRef } from "react";
import { ScrollView, View } from "react-native";
import Toast from "react-native-toast-notifications";
import { MeasPointUpdateParams, OperationServices } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { genRandomString, genShortDescription, isArray } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import {
  CCollapsibleSection,
  CEmptyState,
  CInfoViewer,
  CModalViewer,
  CRadioSelect,
  CTAButtons,
  CTextInput,
} from "../../common";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  initState: any;
  employeeCode: string;
  onClose: any;
  onChange: any;
  workOrderId: string;
  operationId: string;
};

const selections = [
  {
    value: false,
    label: "Giá trị đo hiện tại",
  },
  {
    value: true,
    label: "Giá trị đo khác biệt",
  },
];

const OperationPrtSubmit: FC<PropsType> = ({
  saInsets,
  isCrud,
  initState,
  employeeCode,
  onClose,
  onChange,
  workOrderId,
  operationId,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [prtList, setPrtList]: any = useStateLazy([]);
  const [isFetching, setFetching]: any = useStateLazy(false);

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  useEffect(() => {
    setPrtList([...initState]);
  }, [initState]);

  const postMeasuringPoint = async () => {
    const isValidMs = prtList && prtList.find((p: any) => !isEmpty(p.point));
    if (!isValidMs) {
      toastRef.current.show("Vui lòng nhập ít nhất một thông số kỹ thuật", { type: "danger" });
      return;
    }

    setFetching(true);
    let isHaveSuccess = false;

    for (let index = 0; index < prtList.length; index++) {
      const prt = prtList[index];
      if (prt.point) {
        const params: MeasPointUpdateParams = {
          measPoint: prt.measurePoint,
          measDocDate: moment(new Date()).format("YYYYMMDD"),
          measDocTime: moment(new Date()).format("HHmmss"),
          measDocText: genShortDescription(prt.description),
          measDocLText: prt.description,
          readBy: employeeCode,
          measRead: "",
          countRead: "",
          diff: "",
          workOrder: workOrderId,
          operation: operationId,
          countReadId: "",
          isMeasDiff: prt.counter === "X" && prt.isDiff ? true : false,
        };
        if (prt.counter === "X") {
          params.diff = prt.isDiff ? prt.point : "";
          params.countRead = !prt.isDiff ? prt.point : "";
        } else {
          params.measRead = prt.point;
        }

        const { error } = await axiosHandler(() => OperationServices.postMeasuringPoint(params));

        if (!isMounted()) return;

        if (error) {
          toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
        } else {
          isHaveSuccess = true;
        }
      }
    }

    setFetching(false);
    if (isHaveSuccess) onChange();
  };

  const _renderModalSpecifications = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        title={"Thông số kỹ thuật"}
        onBackdropPress={() => onClose()}
        isFetching={isFetching}>
        {isArray(prtList, 1) ? (
          <>
            <Toast ref={(ref: any) => (toastRef.current = ref)} />
            <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom }]}>
              {prtList.map((item: any, index: any) => (
                <>
                  <CCollapsibleSection
                    borderBg
                    title={item.title}
                    styles={{ marginTop: WR(2) }}
                    key={genRandomString()}>
                    <View key={genRandomString()} style={{ flexDirection: "row" }}>
                      <CInfoViewer
                        infos={[
                          {
                            label: "Tiêu chí",
                            value: item.measurePointPosition,
                          },
                          {
                            label: "Giá trị đo nhỏ nhất",
                            value: `${item.lowerRange} (${item.rangeUnit})`,
                            isGrouped: true,
                          },
                          {
                            label: "Giá trị đo lớn nhất",
                            value: `${item.upperRange} (${item.rangeUnit})`,
                            isGrouped: true,
                          },
                        ]}
                      />
                    </View>
                    {item.counter === "X" && (
                      <>
                        <View style={{ flexDirection: "row" }}>
                          <CRadioSelect
                            options={selections}
                            selected={item.isDiff}
                            onSelect={() => {
                              Object.assign(prtList[index], { ...prtList[index], isDiff: !item.isDiff });
                              setPrtList([...prtList]);
                            }}
                            styles={{
                              wrapper: styles.radioBtnCtn,
                              item: styles.radioChild,
                            }}
                          />
                        </View>
                      </>
                    )}
                    <View style={{ flexDirection: "row" }}>
                      <CTextInput
                        fullWidth
                        label={"Kết quả đo"}
                        plh={`Nhập kết quả (${item.rangeUnit})`}
                        keyboardType="numeric"
                        value={item.point}
                        styles={{ wrapper: { marginRight: WR(8), marginTop: WR(8) } }}
                        onChange={(point: any) => {
                          Object.assign(prtList[index], { ...prtList[index], point: point });
                          setPrtList([...prtList]);
                        }}
                      />
                    </View>
                    <View style={{ flexDirection: "row" }}>
                      <CTextInput
                        fullWidth
                        label={"Ghi chú"}
                        plh={"Nhập ghi chú"}
                        multiline={1}
                        value={item.description}
                        styles={{ wrapper: { marginRight: WR(8), marginTop: WR(8) } }}
                        onChange={(desc: any) => {
                          Object.assign(prtList[index], { ...prtList[index], description: desc });
                          setPrtList([...prtList]);
                        }}
                      />
                    </View>
                  </CCollapsibleSection>
                </>
              ))}
            </ScrollView>
            <CTAButtons
              isSticky
              buttons={[
                {
                  text: "Lưu",
                  onPress: () => {
                    postMeasuringPoint();
                  },
                },
              ]}
            />
          </>
        ) : (
          <>
            <CEmptyState />
          </>
        )}
      </CModalViewer>
    );
  };

  return <>{_renderModalSpecifications()}</>;
};
export default OperationPrtSubmit;

import { StyleSheet } from "react-native";

import { heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(4),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  fetchingDataOverlay: {
    zIndex: 100,
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "#00000040",
  },
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
  radioBtnCtn: {
    paddingBottom: HR(10),
    marginLeft: 0,
    justifyContent: "space-between",
  },
  radioChild: {
    marginLeft: 0,
    paddingHorizontal: WR(8),
  },
});

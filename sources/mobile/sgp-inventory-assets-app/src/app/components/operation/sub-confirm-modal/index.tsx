import { widthResponsive as WR } from "@Constants";
import { isEmpty } from "lodash";
import React, { FC, useEffect, useRef } from "react";
import { ScrollView } from "react-native";
import Toast from "react-native-toast-notifications";
import { DocumentServices, OperationServices, SubOperationConfirmationParams } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { isArray } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { CAttachFiles, CCollapsibleSection, CModalViewer, CSelect, CTAButtons, CTextInput } from "../../common";
import { CFile } from "../../common/attach-files";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  initState: Array<SubOperationItemConfirmation>;
  onClose: any;
  onChange: any;
  onModalHide?: any;
};

export type SubOperationItemConfirmation = {
  description: string;
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  workCenterCode: string;
  maintenancePlantCode: string;
  personnel: string;
  activityType: string;
  unit: string;
};

const SubOperationConfirmation: FC<PropsType> = ({ saInsets, isCrud, initState, onClose, onChange }) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [errors, setErrors] = useStateLazy([]);
  const [fetchings, setFetchings] = useStateLazy(false);
  const [items, setItems]: any = useStateLazy({});

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  useEffect(() => {
    setItems(initState);
    setErrors([]);
  }, [initState]);

  const handleUploadFiles = async (
    workOrderId: string,
    operationId: string,
    superOperationId: string,
    files: Array<CFile>
  ) => {
    const { error } = await axiosHandler(() =>
      DocumentServices.uploadSubOperationDocuments(workOrderId, operationId, superOperationId, files)
    );

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "warning" });
    }
  };

  const handleValidateForm = () => {
    let validItems: any = [];
    let _errors: any = [];

    setErrors([]);

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let _error: any = {};

      _error["confirm"] = isEmpty(item.confirm);

      if (item.confirm?.value === "P" || item.confirm?.value === "X") {
        _error["desc"] = false;
      } else {
        _error["desc"] = isEmpty(item.desc);
      }

      _errors[i] = _error;

      if (!_error.confirm && _error.desc) {
        toastRef.current.show(`Vui lòng nhập mô tả cho công việc con ${item.confirm?.label}`, {
          type: "warning",
        });
        return [];
      }

      if (!_error.confirm && !_error.desc) {
        validItems.push({ ...item });
      }
    }

    if (isEmpty(validItems)) {
      validItems = [];
      toastRef.current.show("Vui lòng nhập đầy đủ thông tin", { type: "warning" });
      setErrors(_errors);
    }

    return validItems;
  };

  const handleValidateAttachment = (validItems: any) => {
    let isValid = true;

    for (let i = 0; i < validItems.length; i++) {
      const item = validItems[i];

      if (item.confirm?.value != "P" && item.confirm?.value != "X" && isEmpty(item.files)) {
        isValid = false;
        toastRef.current.show(`Vui lòng đính kèm hình ảnh cho công việc con ${item.confirm?.label}`, {
          type: "warning",
        });
      }
    }
    return isValid;
  };

  const handlePostSubOperation = async () => {
    let validItems = handleValidateForm();
    if (isEmpty(validItems)) return;

    let valid = handleValidateAttachment(validItems);
    if (!valid) return;

    setFetchings(true);
    let errorCount = 0;
    let errorMessage = "";

    for (let i = 0; i < validItems.length; i++) {
      const subOperation = validItems[i];
      const files = subOperation.files || [];

      if (isArray(files, 1)) {
        await handleUploadFiles(
          subOperation.workOrderId,
          subOperation.operationId,
          subOperation.superOperationId,
          files
        );
      }

      const params: SubOperationConfirmationParams = {
        confirmation: items[i].desc || "",
        maintenancePlant: subOperation.maintenancePlantCode,
        operationId: subOperation.operationId,
        personnel: subOperation.personnel,
        superOperationId: subOperation.superOperationId,
        workCenter: subOperation.workCenterCode,
        workOrderId: subOperation.workOrderId,
        reason: items[i].confirm?.value,
      };

      const { error } = await axiosHandler(() => OperationServices.confirmSubOperation(params));

      if (!isMounted()) return;
      setFetchings(false);

      if (error) {
        errorMessage = getErrorDetail(error).errorMessage;
        errorCount++;
      }
    }

    if (errorCount > 0) {
      if (errorCount === validItems.length) {
        return toastRef.current.show(errorMessage, { type: "danger" });
      }
      return toastRef.current.show(`Xác nhận hoàn thành công việc con với ${errorCount} công việc con bị lỗi`, {
        type: "warning",
      });
    }
    onChange();
  };

  const handleChange = (index: any, id: any, data: any) => {
    const item = items[index];
    Object.assign(items[index], { ...item, [id]: data });
    setItems([...items]);
  };

  const _renderSubConfirmation = (index: any) => {
    return (
      <>
        <CSelect
          isRequired
          label={"Kết quả công việc"}
          selected={items[index].confirm}
          isError={errors[index]?.confirm || false}
          menus={[
            {
              label: "KHÔNG ĐẠT [không vận hành]",
              value: "F",
            },
            {
              label: "KHÔNG KIỂM TRA",
              value: "N/A",
            },
            {
              label: "ĐẠT",
              value: "P",
            },
            {
              label: "ĐẠT, NHƯNG CẦN GIÁM SÁT",
              value: "S/M",
            },
            {
              label: "TẠM DỪNG CÔNG VIỆC",
              value: "X",
            },
          ]}
          onSelect={(selected: any) => handleChange(index, "confirm", selected)}
        />
        <CTextInput
          multiline={2}
          label={"Mô tả chi tiết"}
          plh={"Nhập mô tả chi tiết"}
          isError={errors[index]?.desc || false}
          value={items[index].desc}
          onChange={(input: any) => handleChange(index, "desc", input)}
        />
        <CAttachFiles
          simpleAddBtn
          borderFile
          canRemove={true}
          files={items[index].files || []}
          style={{ marginBottom: WR(items[index].files?.length ? 10 : 20) }}
          onChange={(files: any) => {
            const currentFiles = items[index].files || [];
            handleChange(index, "files", [...files, ...currentFiles]);
          }}
        />
      </>
    );
  };

  const _renderCollapSubConfirmation = (index: any, title: any) => {
    return (
      <CCollapsibleSection
        borderBg
        title={title}
        styles={{ wrapper: { marginTop: 0, marginBottom: WR(24) } }}
        key={index}>
        {_renderSubConfirmation(index)}
      </CCollapsibleSection>
    );
  };

  const _renderModalChangeStatus = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        title={
          isArray(initState, 2)
            ? `Xác nhận công việc con (${initState.length})`
            : `Xác nhận công việc con ${initState[0]?.description} (${initState[0]?.operationId})`
        }
        desc={isArray(initState, 2) && `Công việc này gồm có ${initState.length} công việc con`}
        isFetching={fetchings}
        onBackdropPress={() => onClose()}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />
        <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom + WR(12) }]}>
          {isArray(initState, 2)
            ? initState.map((i: any, index: any) =>
                _renderCollapSubConfirmation(index, `${index + 1}. ${i.description}`)
              )
            : _renderSubConfirmation(0)}
        </ScrollView>
        <CTAButtons
          isSticky
          buttons={[
            {
              text: "Đóng",
              onPress: () => onClose(),
              isSub: true,
            },
            {
              text: "Xác nhận",
              onPress: () => {
                handlePostSubOperation();
              },
            },
          ]}
        />
      </CModalViewer>
    );
  };

  return <>{isArray(items, 1) && _renderModalChangeStatus()}</>;
};
export default SubOperationConfirmation;

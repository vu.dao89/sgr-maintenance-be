import { StyleSheet } from "react-native";

import { Color, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  taskDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  taskViewMoreBtnTxt: {
    // marginRight: WR(2),
    color: Color.Yellow,
    fontWeight: "500",
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  dataFetching: {
    paddingVertical: WR(24),
  },
  ctaButton: {
    marginBottom: WR(20),
    height: HR(36),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#4C8BFF",
  },
  ctaButtonTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  taskAssigneeWrapper: {
    marginBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneePerson: {
    flex: 1,
  },
  modalContent: {
    paddingTop: 0,
    paddingLeft: WR(24),
    paddingRight: WR(24),
  },
  btnSelectImg: {
    marginBottom: WR(16),
  },
  btnSelectImgTxt: {
    color: Color.Yellow,
    fontWeight: "600",
  },
});

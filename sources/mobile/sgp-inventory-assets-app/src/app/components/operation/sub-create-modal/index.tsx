import { widthResponsive as WR } from "@Constants";
import React, { FC, useEffect, useRef } from "react";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-toast-notifications";
import { useSelector } from "react-redux";
import i18n from "../../../../i18n";
import { AppState } from "../../../../redux/reducers";
import { MasterDataServices, PermissionServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { genRandomString } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatWorkCenterLabel,
  mappingSelections,
} from "../../../../utils/Format";
import { CInfoViewer, CModalViewer, CSelect, CTAButtons, CTextInput } from "../../common";

import styles from "./styles";

const relatatedOptions: any = {
  maintPlant: "workCenter",
};

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  childAction: string;
  initState: any;
  onClose: any;
  onChange: any;
  onModalHide?: any;
};

const SubOperationCreation: FC<PropsType> = ({
  saInsets,
  isCrud,
  childAction,
  initState,
  onClose,
  onChange,
  onModalHide,
}) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});
  const reqIds: any = useRef({});

  const [childState, setChildState]: any = useStateLazy({});
  const [childErrors, setChildErrors] = useStateLazy({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);

  const childForms = [
    {
      type: "input",
      id: "fullDescription",
      label: "Mô tả chi tiết",
      plh: "Nhập mô tả chi tiết",
      multiline: 2,
      maxLength: 5000,
      isRequired: true,
    },
    {
      type: "select",
      id: "controlKey",
      label: "Nguồn lực",
      isRequired: true,
    },
    {
      type: "select",
      id: "maintPlant",
      label: "Nơi bảo trì",
      isRequired: true,
    },
    {
      type: "select",
      id: "workCenter",
      label: "Tổ đội thực hiện",
      isRequired: true,
    },
    {
      type: "input",
      id: "estimate",
      label: "Thời gian làm việc dự kiến",
      plh: "Nhập thời gian",
      keyboardType: "numeric",
      returnKeyType: "done",
    },
    {
      type: "info",
      id: "unit",
      label: "Đơn vị tính",
      value: "Phút",
    },
  ];

  useEffect(() => {
    setChildState({
      ...initState,
    });
  }, [initState]);

  const handleChange = (id: any, data: any) => {
    let nextState: any = { [id]: data },
      nextOptsState: any = {};
    let relatatedOptionDataKey = relatatedOptions[id];
    if (relatatedOptionDataKey && !!data) {
      nextState[relatatedOptionDataKey] = null;
      nextOptsState[relatatedOptionDataKey] = [];
    }
    switch (id) {
      case "workCenter":
        {
          if (data && !childState.maintPlant) {
            nextState.maintPlant = data.maintPlant;
          }
        }
        break;
      default:
        break;
    }
    setChildState((prevState: any) => ({ ...prevState, ...nextState }));
    setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
    setChildErrors((prevState: any) => ({ ...prevState, [id]: false }));
  };

  const handleSearch = (id: any) => async (keyword?: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];

    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(childState.maintPlant ? [childState.maintPlant] : [], userPlants),
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "maintPlant":
        {
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenter: i.costCenter,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "controlKey":
        {
          searchServiceApi = MasterDataServices.getControlKey;
        }
        break;

      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));
    // console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const handleCrudChild = () => {
    let isValidData = true,
      _errors: any = {},
      focusInputField: any = null,
      setInvalid = (id: any) => {
        isValidData = false;
        _errors[id] = true;
        if (!focusInputField) focusInputField = id;
      };

    ["fullDescription", "controlKey", "maintPlant", "workCenter"].forEach((requiredField: any) => {
      if (!childState[requiredField]) setInvalid(requiredField);
    });

    setChildErrors(_errors);

    if (isValidData) {
      let task: any;
      if (childAction === "create") {
        task = {
          ...childState,
          localId: genRandomString(),
          fullDescription: (childState.fullDescription || "").trim(),
          childs: [],
          files: [],
        };
      } else {
        task = {
          ...childState,
          fullDescription: (childState.fullDescription || "").trim(),
        };
      }

      onChange(task);
      onClose();
    } else {
      // TODO: scroll into focusInputField
      toastRef.current.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
    }
  };

  const _renderCrudChild = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        title={`${childAction === "create" ? "Tạo" : "Sửa"} công việc con`}
        onModalHide={onModalHide}
        onBackdropPress={() => onClose()}>
        <KeyboardAwareScrollView contentContainerStyle={styles.crudModalContent}>
          {childForms.map((field: any) => {
            const { type, id, isSearch, relatatedData, ...fieldProps } = field;
            let _fieldProps = {
              key: id,
              isError: childErrors[id],
              value: childState[id],
              onChange: (data: any) => handleChange(id, data),
              ...fieldProps,
            };
            switch (type) {
              case "input":
                return <CTextInput {..._fieldProps} />;
              case "select": {
                let _onSearch = handleSearch(id);
                let selectInputNode = (
                  <CSelect
                    {..._fieldProps}
                    isFullScreen
                    isFetching={fetchings[id]}
                    selected={childState[id]}
                    menus={options[id]}
                    onSelect={(selected: any) => handleChange(id, selected)}
                    onSearch={_onSearch}
                    {...(!isSearch && { onOpen: _onSearch })}
                  />
                );
                return selectInputNode;
              }
              case "info": {
                return <CInfoViewer key={id} infos={[fieldProps]} />;
              }
              default:
                return null;
            }
          })}
        </KeyboardAwareScrollView>
        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Đóng",
              onPress: () => onClose(),
            },
            {
              text: i18n.t(`CM.${childAction}`),
              onPress: () => handleCrudChild(),
            },
          ]}
        />
        <Toast ref={(ref: any) => (toastRef.current = ref)} />
      </CModalViewer>
    );
  };

  return <>{_renderCrudChild()}</>;
};
export default SubOperationCreation;

import React, { FC, Fragment, useState } from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import { CCollapsibleSection, CPersonInfo, CTags, SubOperationConfirmation, TextCM } from "@Components";

import { OperationHistoryActivity, ScreenName, widthResponsive as WR } from "@Constants";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity, View } from "react-native";
import SystemStatus from "../../../../constants/SystemStatus";
import styles from "./styles";

import { isArray } from "../../../../utils/Basic";
import { SubOperationItemConfirmation } from "../sub-confirm-modal";

export interface SubOperationTaskProps {
  uuid: string;
  id: string;
  superOperationId: string;
  workOrderId: string;
  title: string;
  desc: string;
  tags: any;
  assignee: {
    fullName: string;
    avatar: string;
  };
  files: any;
  operationSystemStatus?: any;
  parentOperationStatus?: any;
  parentHistoryActivity?: any;
  workCenter: string;
  personnel: string;
  maintenancePlant: string;
  unit: string;
}

const SubOperationTaskItem: FC<any> = ({
  title,
  desc,
  menus,
  isEdit = false,
  onMenu,
  workOrderId,
  id,
  superOperationId,
  tags,
  assignee,
  operationSystemStatus,
  parentOperationStatus,
  parentHistoryActivity,
  workCenterCode,
  maintenancePlantCode,
  personnel,
  activityType,
  unit,
  onChange,
}) => {
  const saInsets = useSafeAreaInsets();

  const nav = useNavigation();
  const isCompleted = SystemStatus.operation.isCompleted(operationSystemStatus);

  const [isSubmitSubOperation, setIsSubmitSubOperation] = useState(false);
  const [initState, setInitState]: any = useState([]);

  const isDisableConfirm = parentHistoryActivity != OperationHistoryActivity.START;

  return (
    <Fragment>
      <CCollapsibleSection {...{ title, menus, onMenu }}>
        <TextCM style={styles.taskDesc}>
          {desc ? `${desc} ` : ""}
          {!isEdit && (
            <>
              <TextCM
                style={styles.taskViewMoreBtnTxt}
                onPress={() => {
                  nav.navigate(
                    ScreenName.SubOprationDetail as never,
                    {
                      workOrderId: workOrderId,
                      operationId: id,
                      superOperationId: superOperationId,
                    } as never
                  );
                }}>
                Chi tiết
              </TextCM>
            </>
          )}
        </TextCM>

        {isArray(tags, 1) && (
          <View style={styles.listItemFooter}>
            <CTags style={{ marginBottom: WR(20) }} data={tags} />
          </View>
        )}

        {!!operationSystemStatus && !isCompleted && (
          <TouchableOpacity
            style={[styles.ctaButton, isDisableConfirm && { opacity: 0.5 }]}
            disabled={isDisableConfirm}
            onPress={() => {
              const sub: SubOperationItemConfirmation = {
                description: title,
                workOrderId: workOrderId,
                operationId: id,
                superOperationId: superOperationId,
                workCenterCode: workCenterCode,
                maintenancePlantCode: maintenancePlantCode,
                personnel: personnel,
                activityType: activityType,
                unit: unit,
              };
              setInitState([sub]);
              setIsSubmitSubOperation(true);
            }}>
            <TextCM style={styles.ctaButtonTxt}>Xác nhận</TextCM>
          </TouchableOpacity>
        )}

        <View style={styles.taskAssigneeWrapper}>
          <CPersonInfo {...assignee} label={"Đã giao cho"} style={styles.taskAssigneePerson} />
        </View>
      </CCollapsibleSection>
      <SubOperationConfirmation
        saInsets={saInsets}
        isCrud={isSubmitSubOperation}
        initState={initState}
        onClose={() => setIsSubmitSubOperation(false)}
        onChange={() => {
          setIsSubmitSubOperation(false);
          onChange("confirmSubOperationSuccess");
        }}
      />
    </Fragment>
  );
};

export default SubOperationTaskItem;

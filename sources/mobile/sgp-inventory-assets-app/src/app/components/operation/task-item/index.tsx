import { useNavigation } from "@react-navigation/native";
import { get, isEmpty, isEqual } from "lodash";
import React, { FC, Fragment, memo, useEffect, useState } from "react";
import { TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";

import { OperationServices, PermissionServices } from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { AppState } from "@Reducers";

import { CommonActions } from "@Actions";

import { isArray } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsFormatDate } from "src/utils/DateTime";

import {
  Color,
  OperationHistoryActivity,
  Permission,
  ScreenName,
  SystemStatus,
  widthResponsive as WR,
} from "@Constants";

import styles from "./styles";

import {
  CAlertModal,
  CCollapsibleSection,
  CInfoViewer,
  CPersonInfo,
  CPersonSelector,
  CSpinkit,
  CTags,
  OperationActionButton,
  OperationConfirmation,
  OperationMaterialSubmit,
  OperationPrtSubmit,
  SubOperationConfirmation,
  TextCM,
} from "@Components";

import { ICWarningOctagon } from "@Assets/image";

import dayjs from "dayjs";
import { SubOperationItemConfirmation } from "../sub-confirm-modal";

export type OperationTaskItemProps = {
  id: string;
  workOrderId: string;
  title: string;
  desc: string;
  tags: any;
  assignee: any;
  prtRequire: string;
  prtConfirm: string;
  reservation: string;
  reservationWithdraw: string;
  subOperationRequire: string;
  subOperationConfirm: string;
  mainPlantCode: string;
  workCenterCode: string;
  subTasks: Array<string>;
  history: any;
  operationStatusCode: string;
  workOrderStatusCode: string;
  actualStartDateTime: string;
  actualFinishDateTime: string;
  actualStart: string;
  actualStarTime: string;
  infos?: any;
  callback: string;
  supervisor: string;
};

export type OperationTaskItemAction = {
  handleAction: any;
};

const OperationTaskItem: FC<OperationTaskItemProps & OperationTaskItemAction> = ({
  workOrderId,
  id,
  title,
  desc,
  tags,
  assignee,
  prtRequire,
  prtConfirm,
  reservation,
  reservationWithdraw,
  subOperationRequire,
  subOperationConfirm,
  mainPlantCode,
  workCenterCode,
  subTasks = [],
  history = {},
  operationStatusCode,
  workOrderStatusCode,
  actualStartDateTime,
  actualFinishDateTime,
  actualStart,
  actualStarTime,
  handleAction,
  infos,
  callback,
  supervisor,
}) => {
  const saInsets = useSafeAreaInsets();
  const nav = useNavigation();
  const toast: any = useToast();
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const [confirmationType, setConfirmationType]: any = useState("");
  const [isShowConfirmation, setShowConfirmation] = useState(false);
  const [isChangeWorker, setChangeWorker] = useState(false);

  const [isUpdateSpecifications, setUpdateSpecifications] = useState(false);
  const [isUpdateExportingMaterials, setUpdateExportingMaterials] = useState(false);
  const [isUpdateCompleteSubTask, setUpdateCompleteSubTask] = useState(false);

  const [exportingMaterials, setExportingMaterials]: any = useStateLazy([]);
  const [historyActivity, setHistoryActivity]: any = useState(null);
  const [prtList, setPrtList]: any = useStateLazy([]);
  const [subOperations, setSubOperations]: any = useState([]);

  const isOpenWorkOrder = workOrderStatusCode === SystemStatus.workorder.CREATE_NEW;
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const employeeCode = PermissionServices.getEmployeeCode(reducer);

  const canChangeWorker = PermissionServices.canChangeWorker(
    reducer,
    operationStatusCode,
    historyActivity,
    assignee.code,
    supervisor
  );

  const [confirmDelete, setConfirmDelete]: any = useState(false);
  const [isFetching, setFetching] = useState(false);

  const menus = [
    {
      label: "Sửa",
      value: "edit",
    },
    {
      label: "Xoá",
      value: "delete",
      color: Color.Red,
    },
  ];

  useEffect(() => {
    setHistoryActivity(get(history, "activity"), OperationHistoryActivity.NONE);
  }, []);

  const handleChangeMainPerson = async (person: any) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      OperationServices.changePersonnelOperationParent(workOrderId, id, get(person, "code"))
    );

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (get(response, "status") === 204) {
      toast.show("Đã đổi người thành công", { type: "success" });
      handleAction("reload");
    } else {
      toast.show(getErrorDetail(error)?.errorMessage, { type: "danger" });
    }
  };

  const handleOnMenu = (menu: any) => {
    if (menu.value === "edit") {
      nav.navigate(
        ScreenName.OperationCreate as never,
        {
          workOrderId: workOrderId,
          operationId: id,
          isEdit: true,
          callback,
        } as never
      );
    }
    if (menu.value === "delete") {
      setConfirmDelete(true);
    }
  };

  const handleDeleteOperation = async () => {
    setConfirmDelete(false);
    setFetching(true);

    const { error } = await axiosHandler(() => OperationServices.deleteOperation(workOrderId, id), {
      delay: true,
    });

    setFetching(false);
    dispatch(CommonActions.openLoading.success({}));

    if (error) {
      const result = getErrorDetail(error);
      toast.show(result?.errorMessage, { type: "danger" });
    } else {
      toast.show("Xoá công việc thành công", { type: "success" });
      handleReloadCurrentOperation();
      handleAction("reload");
    }
  };

  const handleReloadCurrentOperation = () => {
    // @ts-ignore
    (global["operation_activity_cb"] || (() => null))();
  };

  const handleStartOperation = async () => {
    dispatch(CommonActions.openLoading.request({}));

    const { error } = await axiosHandler(() => OperationServices.startOperation(workOrderId, id));

    if (!isMounted) return;
    dispatch(CommonActions.openLoading.success({}));

    if (error) {
      const result = getErrorDetail(error);
      toast.show(result?.errorMessage, { type: "danger" });
    } else {
      toast.show("Bắt đầu công việc thành công", { type: "success" });
      handleReloadCurrentOperation();
      handleAction("reload");
    }
  };

  const getOperationMeasures = async () => {
    dispatch(CommonActions.openLoading.request({}));
    const { response, error } = await axiosHandler(() => OperationServices.getOperationMeasures(workOrderId, id));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
      return;
    }

    let data: any = get(response, "data.measure.items", []);
    let _prtList: any = [];

    data.forEach((item: any) => {
      const {
        operationId,
        workOrderDescription,
        operationDescription,
        prtItem,
        measurePoint,
        equipmentId,
        measurePointPosition,
        internalCharNo,
        targetValue,
        counter,
        lowerRange,
        upperRange,
        rangeUnit,
        text,
        codeGroup,
        result,
        description,
      } = item;
      const point = item.result;
      if (isEmpty(point)) {
        _prtList.push({
          id: operationId,
          title: description,
          workOrderDescription,
          operationDescription,
          prtItem,
          measurePoint,
          equipmentId,
          measurePointPosition,
          description: "",
          internalCharNo,
          targetValue,
          counter,
          lowerRange,
          upperRange,
          rangeUnit,
          text,
          codeGroup,
          result,
          point: 0,
          isDiff: false,
        });
      }
    });

    if (isArray(_prtList, 1)) {
      setPrtList(_prtList);
      setUpdateSpecifications(true);
    } else {
      toast.show("Không có thông số kỹ thuật cần nhập kết quả", { type: "warning" });
    }
  };

  const handleShowExportMaterialModal = async () => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() => OperationServices.getOperationMaterial(workOrderId, id));

    if (!isMounted()) return;
    dispatch(CommonActions.openLoading.success({}));

    if (error) {
      toast.show(getErrorDetail(error)?.errorMessage, { type: "danger" });
      return;
    }

    const materials = get(response, "data.component.item") || [];
    const _materials: any = [];

    materials.forEach((m: any) => {
      const remainQuantity = +m.remainQuantity;
      if (remainQuantity > 0) {
        _materials.push({
          workOrderId: m.workOrderId,
          material: m.material,
          materialDescription: m.materialDescription,
          plant: m.plant,
          plantName: m.plantName,
          storageLocation: m.storageLocation,
          storageLocationDescription: m.storageLocationDescription,
          batch: m.batch,
          remainQuantity,
          withdrawalQuantity: 0, // user input
          note: m.note,
          reservation: m.reservation,
          reservationItem: m.reservationItem,
        });
      }
    });

    if (isArray(_materials, 1)) {
      setExportingMaterials(_materials);
      setUpdateExportingMaterials(true);
    } else {
      toast.show("Không có vật tư cần xuất kho", { type: "warning" });
    }
  };

  const handleSubmitSubOperation = async () => {
    if (operationStatusCode === SystemStatus.operation.CREATE_NEW) {
      return toast.show("Bạn không thể xác nhận hoàn thành công việc", { type: "warning" });
    }

    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => OperationServices.getOperationDetail(workOrderId, id));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    const data = get(response, "data.subOperation.item", []);
    const operation = get(response, "data.operation");
    let _subOperations: any = [];

    const { activityType } = operation;

    data.forEach((sub: any) => {
      if (isEmpty(sub.result)) {
        const item: SubOperationItemConfirmation = {
          description: sub.operationDescription,
          workOrderId: sub.workOrderId,
          operationId: sub.operationId,
          superOperationId: sub.superOperationId,
          workCenterCode: sub.workCenterCode,
          maintenancePlantCode: sub.maintenancePlantCode,
          personnel: assignee.code || sub.personnel,
          activityType: activityType,
          unit: sub.unit,
        };
        _subOperations.push(item);
      }
    });

    if (isEmpty(_subOperations)) {
      return toast.show("Không có công việc con nào chưa xác nhận hoàn thành", { type: "warning" });
    }

    setSubOperations(_subOperations);
    setUpdateCompleteSubTask(true);
  };

  const _renderPrtPlusPlus = () => {
    const canAction = historyActivity === OperationHistoryActivity.START;
    const disabled = employeeCode != assignee?.code;

    return (
      <>
        {[
          {
            label: "Thông số kỹ thuật",
            count: prtRequire > prtConfirm,
            value: `${prtConfirm}/${prtRequire}`,
            cta: "+ Nhập",
            onUpdate: () => getOperationMeasures(),
          },
          {
            label: "Xuất kho vật tư",
            count: reservation,
            value: `${reservationWithdraw}/${reservation}`,
            cta: "+ Nhập",
            onUpdate: () => handleShowExportMaterialModal(),
          },
          {
            label: "Công việc con hoàn tất",
            count: subOperationRequire > subOperationConfirm,
            value: `${subOperationConfirm}/${subOperationRequire}`,
            cta: "Xác nhận",
            onUpdate: () => handleSubmitSubOperation(),
          },
        ].map((item: any, index: any) => {
          const { cta, label, count, value, onUpdate } = item;

          return (
            <View key={index} style={styles.taskDataRow}>
              <TextCM style={styles.taskDataLabel}>
                {label}{" "}
                <TextCM color={"White"} size={"FontTiny"}>
                  ({value})
                </TextCM>
              </TextCM>
              <TouchableOpacity onPress={onUpdate} disabled={disabled}>
                {count > 0 && canAction && (
                  <TextCM style={[styles.taskDataBtnTxt, disabled && { opacity: 0.5 }]}>{cta}</TextCM>
                )}
              </TouchableOpacity>
            </View>
          );
        })}
      </>
    );
  };

  const _renderCompletedOperation = () => {
    return (
      <>
        {!isEmpty(actualStartDateTime) && (
          <View style={[styles.taskCompleteWrapper, { marginTop: WR(16) }]}>
            <CInfoViewer
              noLastItemMargin
              infos={[
                {
                  label: "Thời gian bắt đầu",
                  value: actualStartDateTime,
                },
                {
                  label: "Thời gian kết thúc",
                  value: actualFinishDateTime,
                },
              ]}
            />
          </View>
        )}
        <View style={styles.taskCompleteWrapper}>{_renderPrtPlusPlus()}</View>
      </>
    );
  };

  const canStartOps = PermissionServices.checkPermission(reducer, Permission.OPERATION.CONFIRM_START);

  const _renderStartedOperation = () => {
    const startDateTime = dayjsFormatDate(dayjs.unix(history?.startDateTime), "DD/MM/YYYY - HH:mm:ss");

    return (
      <View style={[styles.taskDataWrapper]}>
        {!canStartOps ? null : (
          <>
            {history && <TextCM style={styles.taskStartedDate}>{`Đã bắt đầu vào ${startDateTime}`}</TextCM>}
            <View style={{ marginStart: WR(-20), marginEnd: WR(-20) }}>
              <OperationActionButton
                actualStart={actualStart}
                actualStarTime={actualStarTime}
                historyActivity={historyActivity}
                operationStatusCode={operationStatusCode}
                handleAction={(action: any) => {
                  if (action === OperationHistoryActivity.START) {
                    handleStartOperation();
                  } else {
                    setConfirmationType(action);
                    setShowConfirmation(true);
                  }
                }}
                isSticky={false}
                isBlueStyle={true}
                isDisabled={employeeCode != assignee?.code}
              />
            </View>
          </>
        )}
        <View style={styles.taskStartedWrapper}>{_renderPrtPlusPlus()}</View>
      </View>
    );
  };

  const cannotStartOperation = !canStartOps || isOpenWorkOrder || employeeCode != assignee?.code;
  const readyToStartOperation =
    operationStatusCode === SystemStatus.operation.A_HAFT || operationStatusCode === SystemStatus.operation.READY;

  const _renderNewOperation = () => {
    return (
      <View style={styles.taskDataWrapper}>
        {PermissionServices.checkPermission(reducer, Permission.OPERATION.CONFIRM_START) && readyToStartOperation && (
          <TouchableOpacity
            disabled={cannotStartOperation}
            style={[styles.ctaButton, { marginBottom: WR(16) }, cannotStartOperation && { opacity: 0.5 }]}
            onPress={() => handleStartOperation()}>
            <TextCM style={styles.ctaButtonTxt}>Bắt đầu công việc</TextCM>
          </TouchableOpacity>
        )}
        {_renderPrtPlusPlus()}
      </View>
    );
  };

  const _renderInfos = () => {
    return infos?.map((info: any) => {
      const { label, value } = info;
      if (isEmpty(value)) return null;
      return (
        <View key={label} style={[styles.listItemInfoRow, { marginTop: 0 }]}>
          <TextCM style={styles.listItemInfoLabel}>{label}: </TextCM>
          <TextCM style={styles.listItemInfoValue}>{value}</TextCM>
        </View>
      );
    });
  };

  const _renderActionInfos = () => {
    if (operationStatusCode === SystemStatus.operation.CREATE_NEW) {
      return <View style={styles.taskCompleteWrapper}>{_renderPrtPlusPlus()}</View>;
    }

    if (SystemStatus.operation.isCompleted(operationStatusCode)) {
      return _renderCompletedOperation();
    }

    if (operationStatusCode === SystemStatus.operation.READY && !historyActivity) {
      return _renderNewOperation();
    }

    return _renderStartedOperation();
  };

  return (
    <Fragment>
      <CCollapsibleSection {...{ title, menus, onMenu: handleOnMenu }} subtitle={`(${id})`}>
        <TextCM style={styles.taskDesc}>
          {desc ? `${desc} ` : ""}
          <TextCM
            style={styles.taskViewMoreBtnTxt}
            onPress={() => {
              nav.navigate(
                ScreenName.OperationDetail as never,
                {
                  workOrderId: workOrderId,
                  operationId: id,
                  callback,
                } as never
              );
            }}>
            Chi tiết
          </TextCM>
        </TextCM>

        {isArray(tags, 1) && (
          <View style={styles.listItemFooter}>
            <CTags style={{ marginBottom: WR(20) }} data={tags} />
          </View>
        )}

        {isArray(infos, 1) && (
          <View style={{ paddingBottom: WR(16), borderBottomWidth: 1, borderBottomColor: "#414042" }}>
            {_renderInfos()}
          </View>
        )}

        {_renderActionInfos()}

        <View style={styles.taskAssigneeWrapper}>
          <CPersonInfo
            {...assignee}
            workCenterCode
            operationStatusCode
            label={"Đã giao cho"}
            style={styles.taskAssigneePerson}
          />

          {canChangeWorker.isShow && (
            <TouchableOpacity
              style={[styles.taskAssigneeBtn, canChangeWorker.isDisabled && { opacity: 0.5 }]}
              disabled={canChangeWorker.isDisabled}
              onPress={() => setChangeWorker(true)}>
              <TextCM style={styles.taskAssigneeBtnTxt}>Đổi người</TextCM>
            </TouchableOpacity>
          )}
        </View>
      </CCollapsibleSection>

      <OperationPrtSubmit
        saInsets={saInsets}
        isCrud={isUpdateSpecifications}
        initState={prtList}
        employeeCode={employeeCode}
        onClose={() => setUpdateSpecifications(false)}
        onChange={() => {
          toast.show("Đã cập nhật thông số kỹ thuật thành công", { type: "success" });
          handleAction("reload");
        }}
        workOrderId={workOrderId}
        operationId={id}
      />

      <OperationMaterialSubmit
        saInsets={saInsets}
        isCrud={isUpdateExportingMaterials}
        operationId={id}
        initState={exportingMaterials}
        onClose={() => setUpdateExportingMaterials(false)}
        onChange={() => {
          toast.show(`Đã xuất kho vật tư thành công!`, { type: "success" });
          handleAction("reload");
        }}
        workOrderId={workOrderId}
      />

      <SubOperationConfirmation
        saInsets={saInsets}
        isCrud={isUpdateCompleteSubTask}
        initState={subOperations}
        onClose={() => setUpdateCompleteSubTask(false)}
        onChange={() => handleAction("reload")}
      />

      <OperationConfirmation
        saInsets={saInsets}
        workOrderId={workOrderId}
        operationId={id}
        operationHistory={history}
        isShow={isShowConfirmation}
        onChange={() => handleAction("reload")}
        onClose={() => setShowConfirmation(false)}
        type={confirmationType}
        operationDescription={desc}
        toast={toast}
      />

      <CPersonSelector
        open={isChangeWorker}
        title={"Đổi người"}
        selected={assignee}
        workCenterCode={workCenterCode}
        mainPlantCode={mainPlantCode}
        onClose={() => setChangeWorker(false)}
        onSelect={handleChangeMainPerson}
      />

      <CAlertModal
        open={confirmDelete}
        title={"Bạn có chắc muốn xoá công việc này? Công việc đã xoá không thể phục hồi"}
        icon={<ICWarningOctagon />}
        onClose={() => setConfirmDelete(false)}
        onCta={handleDeleteOperation}
        onModalHide={() => {
          if (isFetching) dispatch(CommonActions.openLoading.request({}));
        }}
      />

      {isFetching && (
        <View style={styles.dataFetching}>
          <CSpinkit />
        </View>
      )}
    </Fragment>
  );
};

const propsAreEqual = (prevProps: any, nextProps: any) =>
  ["item", "index"].every(i => isEqual(prevProps[i], nextProps[i]));

export default memo(OperationTaskItem, propsAreEqual);

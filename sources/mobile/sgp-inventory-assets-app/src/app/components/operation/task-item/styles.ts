import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, ScreenWidth as SW, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  taskDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  taskDataWrapper: {
    paddingBottom: WR(16),
    borderBottomWidth: 1,
    borderBottomColor: "#414042",
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  ctaButton: {
    height: HR(36),
    marginBottom: WR(4),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#4C8BFF",
  },
  ctaButtonTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  btnPause: {
    marginRight: WR(8),
    backgroundColor: "#585858",
  },
  btnPauseTxt: {
    color: "#C6C6C6",
  },
  taskDataRow: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  taskDataLabel: {
    flex: 1,
    marginRight: WR(8),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  taskDataBtnTxt: {
    color: Color.Yellow,
    fontSize: FontSize.FontTiny,
  },
  taskAssigneeWrapper: {
    marginTop: WR(20),
    marginBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneePerson: {
    flex: 1,
  },
  taskAssigneeBtn: {
    marginLeft: WR(8),
    height: WR(36),
    paddingHorizontal: WR(16),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#D8D8D8",
  },
  taskAssigneeBtnTxt: {
    fontWeight: "500",
  },
  taskOtherJobWrapper: {
    paddingTop: WR(16),
    paddingBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderTopWidth: 1,
    borderTopColor: "#414042",
  },
  taskOtherJobCounter: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  taskViewMoreBtn: {
    // paddingTop: WR(10),
    // marginLeft: WR(8),
    flexDirection: "row",
    alignItems: "center",
  },
  taskViewMoreBtnTxt: {
    // marginRight: WR(2),
    color: Color.Yellow,
    fontWeight: "500",
  },
  taskCompleteWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: "#414042",
    paddingBottom: WR(16),
  },
  taskStartedWrapper: {
    marginTop: WR(24),
    borderTopWidth: 1,
    borderTopColor: "#414042",
  },
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
  taskStartedDate: {
    marginTop: WR(16),
    marginBottom: WR(8),
    textAlign: "center",
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  dataFetching: {
    paddingVertical: WR(24),
  },
  listItemInfoRow: {
    minHeight: WR(20),
    flexDirection: "row",
  },
  listItemInfoValue: {
    flex: 1,
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  listItemInfoLabel: {
    marginRight: WR(4),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  taskStartedCta: {
    flexDirection: "row",
  },
  confirmSubTaskItem: {
    marginBottom: WR(24),
  },
  confirmSubTaskTitle: {
    marginBottom: WR(12),
    color: Color.White,
    fontWeight: "600",
  },
  btnSelectImg: {
    marginBottom: WR(16),
  },
  btnSelectImgTxt: {
    color: Color.Yellow,
    fontWeight: "600",
  },
  menuLineBreak: {
    width: SW - WR(48),
    height: 1,
    backgroundColor: "#3F4166",
  },
  radioBtnCtn: {
    paddingBottom: HR(10),
    marginLeft: 0,
    justifyContent: "space-between",
  },
  radioChild: {
    marginLeft: 0,
    paddingHorizontal: WR(8),
  },
});

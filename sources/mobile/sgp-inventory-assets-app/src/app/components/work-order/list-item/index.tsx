import React, { FC, Fragment, memo } from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { isEqual } from "lodash";

import styles from "./styles";

import { CTags, TextCM } from "@Components";

import { IMGDefault } from "@Assets/image";

const WOListItem: FC<any> = ({ item, index, onPress }) => {
  const { title, equipment, mainPlant, createdBy, dateTxt, timePassedFromNow, tags } = item;
  return (
    <Fragment>
      {!!index && <View style={styles.listSeparator} />}
      <TouchableOpacity style={styles.listItemWrapper} onPress={onPress}>
        <View style={styles.listItemHeader}>
          <View style={styles.listItemContent}>
            <TextCM style={styles.listItemTitle}>{title}</TextCM>

            {[
              ["Thiết bị", equipment.name],
              ["Khu vực", mainPlant],
              ["Giám sát", createdBy],
              ["Thời gian", dateTxt],
            ].map(([label, value]) => (
              <View key={label} style={[styles.listItemInfoRow, !index && { marginTop: 0 }]}>
                <TextCM style={styles.listItemInfoLabel}>{label}: </TextCM>
                <TextCM style={styles.listItemInfoValue}>{value}</TextCM>
              </View>
            ))}
          </View>

          <Image source={equipment.image ? { uri: equipment.image } : IMGDefault} style={styles.listItemImage} />
        </View>

        <View style={styles.listItemFooter}>
          <CTags data={tags} />
          <TextCM style={styles.listItemTimePassed}>{timePassedFromNow}</TextCM>
        </View>
      </TouchableOpacity>
    </Fragment>
  );
};

const propsAreEqual = (prevProps: any, nextProps: any) => ['item', 'index'].every(i => isEqual(prevProps[i], nextProps[i]));

export default memo(WOListItem, propsAreEqual);
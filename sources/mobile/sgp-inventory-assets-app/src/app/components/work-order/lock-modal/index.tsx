import { widthResponsive as WR } from "@Constants";
import { get } from "lodash";
import React, { FC, useEffect, useRef } from "react";
import { ScrollView, View } from "react-native";
import Toast from "react-native-toast-notifications";
import { MasterDataServices, WorkOrderServices } from "../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { isArray } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { CEmptyState, CModalViewer, CRadioSelect, CTAButtons } from "../../common";

import styles from "./styles";

type PropsType = {
  saInsets: any;
  isCrud: boolean;
  workOrderId: string;
  onClose: any;
  onChange: any;
};

const LockWorkOrder: FC<PropsType> = ({ saInsets, isCrud, workOrderId, onClose, onChange }) => {
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const [reasonList, setReasonList]: any = useStateLazy([]);
  const [isFetching, setFetching]: any = useStateLazy(false);
  const [reasonSelected, setReasonSelected]: any = useStateLazy(null);

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  const getReasonsList = async () => {
    setFetching(true);

    const { response, error } = await axiosHandler(() => MasterDataServices.getLockReasons());

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      let selections = get(response, "data.items", []);
      const reasons = selections.map((i: any) => {
        return {
          value: i["code"],
          label: i["reason"],
        };
      });
      if (isArray(reasons, 1)) setReasonSelected(reasons[0].value);
      setReasonList(reasons);
    }

    setFetching(false);
  };

  useEffect(() => {
    getReasonsList();
  }, []);

  const lockWorkOrder = async () => {
    setFetching(true);
    const { response, error } = await axiosHandler(() => WorkOrderServices.lockWorkOrder(workOrderId, reasonSelected));

    if (!isMounted()) {
      setFetching(false);
      return;
    }

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toastRef.current.show("Khóa lệnh bảo trì thành công", { type: "success" });
      onChange();
    }

    setFetching(false);
  };

  const _renderModal = () => {
    return (
      <CModalViewer
        open={isCrud}
        placement={"bottom"}
        title={"Khóa lệnh bảo trì này"}
        onBackdropPress={() => onClose()}
        isFetching={isFetching}>
        {isArray(reasonList, 1) ? (
          <>
            <Toast ref={(ref: any) => (toastRef.current = ref)} />
            <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom }]}>
              <View>
                <CRadioSelect
                  options={reasonList}
                  selected={reasonSelected}
                  onSelect={(selected: any) => {
                    setReasonSelected(selected.value);
                  }}
                  styles={{
                    wrapper: styles.radioBtnCtn,
                    item: styles.radioChild,
                  }}
                />
              </View>
            </ScrollView>
            <CTAButtons
              isSticky
              buttons={[
                {
                  text: "Bỏ qua",
                  isSub: true,
                  onPress: () => {
                    onClose();
                  },
                },
                {
                  text: "Xác nhận",
                  onPress: () => {
                    lockWorkOrder();
                  },
                },
              ]}
            />
          </>
        ) : (
          <>
            <CEmptyState />
          </>
        )}
      </CModalViewer>
    );
  };

  return <>{_renderModal()}</>;
};
export default LockWorkOrder;

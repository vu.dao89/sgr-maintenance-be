// External
import React, { FC, useRef } from "react";
import { useFormContext } from "react-hook-form";
import { View } from "react-native";

// Internal
import { DatePicker, SelectBox, SelectField, TextCM, TextField } from "@Components";
import GroupFilter from "@Screens/home/GroupFilter";
import UserFilter from "@Screens/home/UserFilter";
import InventoryClassifyFilter from "../../modal/inventory-classify-filter";
import UnitFilter from "../../modal/unit-filter";
import { FieldType, I18n } from "../../type";
import styles from "./styles";

type IProps = {};

export const getItemSelect = (item: any) => ({
  id: item?.id || "",
  label: item?.label || "",
});

const BasicInfo: FC<IProps> = ({}) => {
  const { setValue, getValues } = useFormContext();
  const unitRef = useRef<any>(null);
  const assetGroupRef = useRef<any>(null);
  const inventoryClassifyRef = useRef<any>(null);
  const userRef = useRef<any>(null);

  const onPressUnit = () => unitRef?.current?.onOpenModal();
  const onPressAssetGroup = () => assetGroupRef?.current?.onOpenModal();
  const onPressInventoryClassify = () => inventoryClassifyRef?.current?.onOpenModal();
  const onPressPersonUser = () => userRef?.current?.onOpenModal();

  const onSelectUnit = (item: any) => {
    setValue(FieldType.Unit, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    unitRef?.current?.onCloseModal();
  };

  const onSelectAssetGroup = (item: any) => {
    setValue(FieldType.AssetGroup, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    assetGroupRef?.current?.onCloseModal();
  };

  const onSelectInventoryClassify = (item: any) => {
    setValue(FieldType.InventoryClassify, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    inventoryClassifyRef?.current?.onCloseModal();
  };

  const onSelectPersonUse = (item: any) => {
    setValue(FieldType.PersonUse, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    userRef?.current?.onCloseModal();
  };

  return (
    <View style={styles.formWrapper}>
      <TextCM style={styles.title}>{I18n.BasicInfo}</TextCM>
      <TextField
        name={FieldType.AssetName}
        title={I18n.AssetName}
        placeholder={I18n.PlhAssetName}
        isRequired
        style={styles.field}
        maxLength={50}
      />

      <TextField
        name={FieldType.Serial}
        title={I18n.Serial}
        placeholder={I18n.PlhSerial}
        // isRequired - CR-302
        style={styles.field}
        maxLength={18}
      />

      <SelectField
        name={FieldType.Unit}
        title={I18n.Unit}
        placeholder={I18n.PlhUnit}
        style={styles.field}
        toggleModal={onPressUnit}
        isRequired
        renderModal={<UnitFilter ref={unitRef} onSelect={onSelectUnit} value={getValues(FieldType.Unit)?.id || ""} />}
      />

      <SelectField
        name={FieldType.AssetGroup}
        title={I18n.AssetGroup}
        placeholder={I18n.PlhAssetGroup}
        style={styles.field}
        toggleModal={onPressAssetGroup}
        renderModal={
          <GroupFilter
            ref={assetGroupRef}
            onSelect={onSelectAssetGroup}
            value={getValues(FieldType.AssetGroup)?.id || ""}
          />
        }
      />

      <DatePicker
        name={FieldType.CommissionedDate}
        title={I18n.CommissionedDate}
        placeholder={I18n.PlhCommissionedDate}
        style={styles.field}
      />

      <SelectBox
        name={FieldType.DepartmentUse}
        title={I18n.DepartmentUse}
        placeholder={I18n.PlhDepartmentUse}
        isRequired
        style={styles.field}
        objModal={{
          header: I18n.PlhDepartmentUse,
        }}
        disable
      />

      <SelectField
        name={FieldType.PersonUse}
        title={I18n.PersonUse}
        placeholder={I18n.PlhPersonUse}
        style={styles.field}
        toggleModal={onPressPersonUser}
        isRequired
        renderModal={
          <UserFilter ref={userRef} onSelect={onSelectPersonUse} value={getValues(FieldType.PersonUse)?.id || ""} />
        }
      />

      <SelectField
        name={FieldType.InventoryClassify}
        title={I18n.InventoryClassify}
        placeholder={I18n.PlhInventoryClassify}
        style={styles.field}
        toggleModal={onPressInventoryClassify}
        isRequired
        renderModal={
          <InventoryClassifyFilter
            ref={inventoryClassifyRef}
            onSelect={onSelectInventoryClassify}
            value={getValues(FieldType.InventoryClassify)?.id || ""}
          />
        }
      />

      <TextField
        name={FieldType.Room}
        title={I18n.Room}
        placeholder={I18n.PlhRoom}
        style={styles.field}
        maxLength={8}
      />

      <TextField
        name={FieldType.Model}
        title={I18n.Model}
        placeholder={I18n.PlhModel}
        style={styles.field}
        maxLength={15}
      />

      <TextField
        name={FieldType.Manufacturer}
        title={I18n.Manufacturer}
        placeholder={I18n.PlhManufacturer}
        style={styles.field}
        maxLength={30}
      />

      <TextField
        name={FieldType.Vendor}
        title={I18n.Vendor}
        placeholder={I18n.PlhVendor}
        style={styles.field}
        maxLength={30}
      />
    </View>
  );
};

export default BasicInfo;

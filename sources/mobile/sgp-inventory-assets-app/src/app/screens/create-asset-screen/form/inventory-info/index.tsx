import React, { FC, useEffect, useRef, useState } from "react";
import { useFormContext } from "react-hook-form";
import { AppState, AppStateStatus, Platform, View } from "react-native";
import Geolocation, { GeoError, GeoPosition, PositionError } from "react-native-geolocation-service";
import { isNull } from "lodash";

import { MapView, SelectField, TextArea, TextCM, TextField } from "@Components";
import { heightResponsive } from "@Constants";
import StatusFilter from "@Screens/create-asset-screen/modal/status-filter";
import ImagesField from "@Screens/inventory-asset/images-field";
import { LocationUtils } from "@Utils";
import { FieldType, I18n } from "../../type";
import { getItemSelect } from "../basic-info";
import styles from "./styles";

type IProps = {};

const InventoryInfo: FC<IProps> = ({}) => {
  const { setValue, getValues } = useFormContext();
  const statusRef = useRef<any>(null);
  const [hasPermission, setPermission] = useState<boolean>(false);

  const onPressStatus = () => statusRef?.current?.onOpenModal();

  const onSelectStatus = (item: any) => {
    setValue(FieldType.Status, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    statusRef?.current?.onCloseModal();
  };

  const onRefreshRegion = async (
    config?: Partial<{
      shouldValidate: boolean;
      shouldDirty: boolean;
      shouldTouch: boolean;
    }>
  ) => {
    const hasLocationPermission = await LocationUtils.hasLocationPermission();

    if (!hasLocationPermission) {
      return;
    }

    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
        ({ mocked, coords }: GeoPosition) => {
          setPermission(true);
          const { latitude, longitude } = coords;
          if (mocked) {
            setPermission(false);
            setValue(FieldType.Location, { latitude: null, longitude: null }, config);
          } else {
            const { latitude: lat, longitude: long } = getValues(FieldType.Location);

            setValue(FieldType.Location, { latitude, longitude }, config);
            if (Platform.OS === "ios") {
              if (isNull(lat) || isNull(long)) {
                setTimeout(() => {
                  onRefreshRegion();
                }, 300);
              }
            }
          }
        },
        ({ code }: GeoError) => {
          if (code === PositionError.POSITION_UNAVAILABLE) {
            setPermission(false);
            setValue(FieldType.Location, { latitude: null, longitude: null }, config);
          }
        },
        {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
          showLocationDialog: false,
        }
      );
    }
  };

  useEffect(() => {
    onRefreshRegion();
  }, []);

  const appState = useRef(AppState.currentState);

  useEffect(() => {
    const _handleAppStateChange = function handleAppStateChange(nextAppState: AppStateStatus) {
      if (appState.current.match(/background/) && nextAppState === "active") {
        onRefreshRegion();
      }

      appState.current = nextAppState;
    };

    AppState.addEventListener("change", _handleAppStateChange);
    return () => AppState.removeEventListener("change", _handleAppStateChange);
  }, []);

  return (
    <View style={[styles.formWrapper, { marginTop: heightResponsive(8) }]}>
      <TextCM style={styles.title}>{`${I18n.InventoryInfo}`}</TextCM>
      <TextField
        name={FieldType.Amount}
        title={I18n.Amount}
        placeholder={I18n.PlhAmount}
        isRequired
        style={styles.field}
        maxLength={13}
        keyboardType="numeric"
      />

      <SelectField
        name={FieldType.Status}
        title={I18n.Status}
        placeholder={I18n.PlhStatus}
        isRequired
        style={styles.field}
        toggleModal={onPressStatus}
        renderModal={
          <StatusFilter ref={statusRef} onSelect={onSelectStatus} value={getValues(FieldType.Status)?.id || ""} />
        }
      />

      <TextArea
        name={FieldType.StatusDescription}
        title={I18n.StatusDescription}
        placeholder={I18n.PlhStatusDescription}
        isRequired
        style={styles.field}
        maxLength={255}
      />

      <MapView
        name={FieldType.Location}
        title={I18n.Location}
        placeholder={I18n.PlhLocation}
        isRequired
        styleMap={styles.styleMap}
        onRefreshRegion={() => onRefreshRegion({ shouldDirty: true, shouldValidate: true })}
        hasPermission={hasPermission}
      />

      <ImagesField name={FieldType.Image} max={10} isRequired title={I18n.Image} />
    </View>
  );
};

export default InventoryInfo;

import { StyleSheet } from "react-native";
import { Color, FontSize, heightResponsive as H, widthResponsive as W } from "@Constants";

export default StyleSheet.create({
  title: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
    marginBottom: H(16),
    fontWeight: "700", // DESIGN_BASE 500
  },
  formWrapper: {
    marginTop: H(20),
    paddingHorizontal: W(16),
  },
  field: {
    marginBottom: H(16),
  },
  styleMap: {
    height: H(300),
    width: "100%",
    overflow: "hidden",
    borderRadius: 16,
  },
});

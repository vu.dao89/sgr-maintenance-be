// External
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigation, useIsFocused } from "@react-navigation/native";
import { debounce, get as _get } from "lodash";
import React, { FC, useCallback, useEffect, useState } from "react";
import { FormProvider, SubmitHandler, useFieldArray, useForm } from "react-hook-form";
import { ActivityIndicator, TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useSelector } from "react-redux";
import * as yup from "yup";

// Internal
import { Header, PopupAnalog, TextCM } from "@Components";
import { Color, ScreenName, String as Strings } from "@Constants";
import { AppState } from "@Reducers";
import { CreateAssetServices } from "@Services";
import { FormUtils, ImageUtils } from "@Utils";
import BasicInfo from "./form/basic-info";
import InventoryInfo from "./form/inventory-info";
import styles from "./styles";
import { CreateAssetBody, defaultValues, FieldType, FormField, I18n } from "./type";

const schema = yup
  .object({
    [FieldType.AssetName]: yup.string().trim().required(I18n.MsgRequired),
    // [FieldType.Serial]: yup.string().trim().required(I18n.MsgRequired), Remove - CR-302
    [FieldType.Unit]: yup
      .object({
        id: yup.string().trim().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.DepartmentUse]: yup
      .object({
        id: yup.string().trim().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.PersonUse]: yup
      .object({
        id: yup.string().trim().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.InventoryClassify]: yup
      .object({
        id: yup.string().trim().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.Amount]: yup.string().trim().required(I18n.MsgRequired),

    [FieldType.Status]: yup
      .object({
        id: yup.string().trim().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.StatusDescription]: yup.string().trim().required(I18n.MsgRequired),
    [FieldType.Location]: yup
      .object({
        longitude: yup.number().required(I18n.MsgRequired),
        latitude: yup.number().required(I18n.MsgRequired),
      })
      .required(I18n.MsgRequired),
    [FieldType.Image]: yup.array().required(I18n.MsgRequired).min(1, I18n.MsgRequired).max(10, I18n.ImageMax),
  })
  .required();

type Props = {};

const CreateAssetScreen: FC<Props> = ({}) => {
  const nav = useNavigation();
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const { objDepartment, email } = _get(reducer, "userInfo.data", {});
  const isFocusedHistory = useIsFocused();
  const detailAssets = useSelector((state: AppState) => state.detailAssets);
  const data = _get(detailAssets, "data", {} as any);
  const [showNotice, setShowNotice] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);
  const [msgError, setMsgError] = useState<string>(I18n.messageInventoryFailure);
  const [qrCode, setQrCode] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  const toggleNotice = () => setShowNotice(!showNotice);
  const toggleError = (message = I18n.messageInventoryFailure) => {
    setShowError(!showError);
    setMsgError(message);
  };

  const methods = useForm<FormField>({
    mode: "onChange",
    reValidateMode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: defaultValues(data),
  });

  const {
    handleSubmit,
    formState: { isDirty, isValid },
    setValue,
    setError,
    control,
    resetField,
  } = methods;

  // Reset thong tin kiem ke khi tao moi tuong tu
  useEffect(() => {
    if (isFocusedHistory) {
      resetField("image");
      resetField("quantity");
      resetField("description");
      resetField("status");
    }
  }, [isFocusedHistory]);
  const { replace } = useFieldArray({
    control,
    name: FieldType.Image,
  });

  const _onCreate = async (data: any) => {
    const listNotUpload = data.image?.filter(({ isSuccess }: any) => typeof isSuccess !== "boolean") || [];
    const listUploaded = data.image?.filter(({ isSuccess }: any) => typeof isSuccess === "boolean") || [];
    if (listUploaded.some(({ isSuccess }: any) => !isSuccess)) {
      toggleError();
      setLoading(false);
      return;
    }
    const allPromies = await ImageUtils.uploadImage(listNotUpload);
    const objImages = allPromies?.map((ele: any) => ({
      status: ele?.status,
      imageid: ele?.status === 201 ? ele?.imageid : ele?.uriLocal,
      isSuccess: ele?.status === 201,
      isDownload: ele?.status === 201,
    }));
    const isFaild = objImages?.some(el => el?.status !== 201);
    if (isFaild) {
      setLoading(false);
      setError(FieldType.Image, { message: I18n.ImageError });
      const newData = [...objImages, ...listUploaded]?.map(({ imageid, isSuccess, isDownload }: any) => ({
        imageid,
        isDownload,
        isSuccess,
        isShowStatus: true,
      }));
      replace(newData);
      toggleError();
      return;
    }

    const bodyReq: CreateAssetBody = {
      ...data,
      [FieldType.Unit]: FormUtils.getValueSelected(data, FieldType.Unit),
      [FieldType.AssetGroup]: FormUtils.getValueSelected(data, FieldType.AssetGroup),
      [FieldType.DepartmentUse]: FormUtils.getValueSelected(data, FieldType.DepartmentUse),
      [FieldType.InventoryClassify]: FormUtils.getValueSelected(data, FieldType.InventoryClassify),
      [FieldType.CommissionedDate]: FormUtils.getDateFormat(data, FieldType.CommissionedDate),
      [FieldType.PersonUse]: FormUtils.getValueSelected(data, FieldType.PersonUse),
      [FieldType.Status]: FormUtils.getValueSelected(data, FieldType.Status),

      [FieldType.Longitude]: String(data[FieldType.Location]?.[FieldType.Longitude] || ""),
      [FieldType.Latitude]: String(data[FieldType.Location]?.[FieldType.Latitude] || ""),

      [FieldType.Image]:
        [...objImages, ...listUploaded]?.map(el => ({
          imageid: el.imageid,
        })) || [],

      [FieldType.UserEmail]: email || "",
      [FieldType.Type]: "I",
    };
    delete bodyReq[FieldType.Location];

    const response = await CreateAssetServices.createNewAsset(bodyReq);

    const { qrcode, code, error: _error } = response && response.data;
    if (code === Strings.responseCode.ApiError100) {
      // case error seiral
      if (_error?.includes("Serial")) {
        setError(FieldType.Serial, { message: _error });
      }
      setLoading(false);

      const newData = [...objImages, ...listUploaded]?.map(({ imageid, isSuccess, isDownload }: any) => ({
        imageid,
        isDownload,
        isSuccess,
        isShowStatus: true,
      }));

      replace(newData);
      toggleError(_error);
    } else {
      setLoading(false);
      if (qrcode) {
        setQrCode(qrcode);
        toggleNotice();
      }
    }
  };

  const handlerCreate = useCallback(debounce(_onCreate, 300), []);

  const onSubmit: SubmitHandler<FormField> = data => {
    setLoading(true);
    handlerCreate(data);
  };

  const onPositive = (): void => {
    toggleNotice();
    setTimeout(() => {
      nav.navigate(ScreenName.DetailAssets as never, { qrcode: qrCode } as never);
      setQrCode("");
    }, 300);
  };

  const FixedAction = () => {
    return (
      <View style={styles.ctnFooterAction}>
        <TouchableOpacity
          disabled={!(isDirty && isValid)}
          style={isDirty && isValid ? styles.btnCreateNewAsset : styles.btnCreateNewAssetDisable}
          onPress={handleSubmit(onSubmit)}>
          <View style={styles.row}>
            {loading && <ActivityIndicator style={styles.mr4} size="small" color={Color.Black} />}
            <TextCM style={styles.createNewAsset}>{`${I18n.CreateNewAsset}`}</TextCM>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  useEffect(() => {
    if (objDepartment) {
      setValue(FieldType.DepartmentUse, objDepartment);
    }
  }, [objDepartment]);

  return (
    <FormProvider {...methods}>
      <View style={styles.container}>
        <Header title={I18n.CreateNewAsset} />

        <KeyboardAwareScrollView>
          <BasicInfo />
          <InventoryInfo />
        </KeyboardAwareScrollView>

        <PopupAnalog
          visible={showNotice}
          onClose={toggleNotice}
          content={I18n.CreateNewSuccess}
          titleButton={I18n.ButtonClose}
          onPressConfirm={onPositive}
        />
        <PopupAnalog
          visible={showError}
          onClose={toggleError}
          type="error"
          content={msgError}
          titlePopup={I18n.titleInventoryFailure}
          titleButton={I18n.ButtonClose}
          onPressConfirm={toggleError}
        />

        <FixedAction />
      </View>
    </FormProvider>
  );
};

export default CreateAssetScreen;

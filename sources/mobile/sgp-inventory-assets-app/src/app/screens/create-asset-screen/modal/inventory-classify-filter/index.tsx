// External
import React, { FC, forwardRef, useState } from "react";
import { t } from "i18next";

// Internal
import { SelectModal } from "@Components";
import { Search } from "@Utils";
import { ISearctItem } from "src/utils/Search";
import i18n from "@I18n";

type IProps = {
  ref: any;
  onSelect?: any;
  value?: any;
};

export const inventoryTypes = [
  {
    id: "01",
    label: String(i18n.t("CA.InventoryClassify01")),
  },
  {
    id: "02",
    label: String(i18n.t("CA.InventoryClassify02")),
  },
  {
    id: "03",
    label: String(i18n.t("CA.InventoryClassify03")),
  },
];

const InventoryClassifyFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const { value, onSelect } = props;
  const [dataList, setDataList] = useState<ISearctItem[] | undefined>(inventoryTypes);

  const _onSelect = (item: any) => {
    if (onSelect) onSelect(item);
    setDataList(undefined);
  };

  const _onClose = () => setDataList(undefined);

  const onSearch = (keyword: string) => {
    if (keyword) {
      const newData = Search.searchItems(inventoryTypes, keyword) || [];
      setDataList(newData);
    } else {
      setDataList(undefined);
    }
  };

  return (
    <SelectModal
      ref={ref}
      title={t("CA.InventoryClassify")}
      isSearch
      onSelect={_onSelect}
      value={value}
      data={dataList || inventoryTypes || []}
      onSearch={onSearch}
      onClose={_onClose}
    />
  );
});

InventoryClassifyFilter.displayName = "InventoryClassifyFilter";
export default InventoryClassifyFilter;

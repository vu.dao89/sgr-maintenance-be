// External
import React, { FC, forwardRef, useState } from "react";
import { t } from "i18next";

// Internal
import { SelectModal } from "@Components";
import { Search } from "@Utils";
import { ISearctItem } from "src/utils/Search";
import i18n from "@I18n";

type IProps = {
  ref: any;
  onSelect?: any;
  value?: any;
};

export const statusTypes = [
  {
    id: "1",
    label: String(i18n.t("CA.Status01")),
  },
  {
    id: "2",
    label: String(i18n.t("CA.Status02")),
  },
  {
    id: "3",
    label: String(i18n.t("CA.Status03")),
  },
];

const StatusFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const { value, onSelect } = props;
  const [dataList, setDataList] = useState<ISearctItem[] | undefined>(statusTypes);

  const _onSelect = (item: any) => {
    if (onSelect) onSelect(item);
    setDataList(undefined);
  };

  const _onClose = () => setDataList(undefined);

  const onSearch = (keyword: string) => {
    if (keyword) {
      const newData = Search.searchItems(statusTypes, keyword) || [];
      setDataList(newData);
    } else {
      setDataList(undefined);
    }
  };

  return (
    <SelectModal
      ref={ref}
      title={t("CA.InventoryClassify")}
      isSearch
      onSelect={_onSelect}
      value={value}
      data={dataList || statusTypes || []}
      onSearch={onSearch}
      onClose={_onClose}
    />
  );
});

StatusFilter.displayName = "StatusFilter";
export default StatusFilter;

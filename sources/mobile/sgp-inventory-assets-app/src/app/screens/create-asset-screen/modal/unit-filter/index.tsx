// External
import React, { FC, forwardRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { t } from "i18next";

// Internal
import { SelectModal } from "@Components";
import { AppState } from "@Reducers";
import { Search } from "@Utils";
import { ISearctItem } from "src/utils/Search";

type IProps = {
  ref: any;
  onSelect?: any;
  value?: any;
};

const UnitFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const dispatch = useDispatch();
  const { value, onSelect } = props;
  const [dataList, setDataList] = useState<ISearctItem[] | undefined>(undefined);
  const reducer: any = useSelector<AppState | null>(s => s && s.home);
  const { units: _data = [] } = reducer.filterData;

  const onSearch = (keyword: string) => {
    if (keyword) {
      const newData = Search.searchItems(_data, keyword) || [];
      setDataList(newData);
    } else {
      setDataList(undefined);
    }
  };

  const handleAfterSelect = () => setDataList(undefined);

  return (
    <SelectModal
      ref={ref}
      title={t("CA.Unit")}
      isSearch
      onSelect={onSelect}
      value={value}
      data={dataList || _data || []}
      onSearch={onSearch}
      onClose={handleAfterSelect}
    />
  );
});

UnitFilter.displayName = "UnitFilter";
export default UnitFilter;

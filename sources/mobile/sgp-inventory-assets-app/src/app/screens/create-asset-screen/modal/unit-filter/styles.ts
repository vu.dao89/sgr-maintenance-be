import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  txtItemName: {
    color: Color.White,
    width: W(250),
    lineHeight: H(24),
    fontSize: FontSize.FontMedium,
    fontWeight: "400",
  },
  txtItemNameSelected: {
    color: Color.Yellow,
    width: W(250),
    lineHeight: H(24),
    fontSize: FontSize.FontMedium,
    fontWeight: "400",
  },
  ctnItem: {
    paddingHorizontal: W(16),
    paddingVertical: H(12),
    maxHeight: H(48),
    flexDirection: "row",
    lineHeight: H(20),
    justifyContent: "space-between",
    // backgroundColor: 'red',
  },
  ctnSeparator: {
    marginHorizontal: W(16),
    height: H(1),
    backgroundColor: Color.Divider,
  },
});

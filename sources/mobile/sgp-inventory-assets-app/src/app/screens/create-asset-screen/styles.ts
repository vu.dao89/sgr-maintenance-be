import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, ScreenWidth, Color, FontSize } from "@Constants";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  btnCreateNewAsset: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: H(12),
    backgroundColor: Color.Yellow,
    borderRadius: H(10),
    width: ScreenWidth - W(32),
  },
  btnCreateNewAssetDisable: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: H(12),
    backgroundColor: Color.ButtonDisable,
    borderRadius: H(10),
    width: ScreenWidth - W(32),
  },
  createNewAsset: {
    color: Color.TextButton,
    fontSize: FontSize.FontMedium,
    fontWeight: "700", // DESIGN_BASE 500
  },
  ctnFooterAction: {
    width: ScreenWidth,
    minHeight: H(68),
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: H(8),
    paddingHorizontal: W(12),
    borderTopLeftRadius: H(12),
    borderTopRightRadius: H(12),
    justifyContent: "center",
    marginBottom: H(16),
  },
  row: {
    flexDirection: "row",
  },
  mr4: {
    marginRight: W(4),
  },
});

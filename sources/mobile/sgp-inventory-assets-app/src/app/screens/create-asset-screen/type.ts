import i18n from "@I18n";
import { AppState } from "@Reducers";
import { DateTimeUtil } from "@Utils";
import moment from "moment";
import { useSelector } from "react-redux";
import { inventoryTypes } from "./modal/inventory-classify-filter";

export enum FieldType {
  AssetName = "name",
  Serial = "serial",
  Unit = "unit",
  AssetGroup = "asset_group",
  CommissionedDate = "use_date",
  DepartmentUse = "costcenter",
  PersonUse = "person",
  InventoryClassify = "inventory_type",
  Room = "room",
  Model = "model",
  Manufacturer = "manufacture",
  Vendor = "vendor",

  Amount = "quantity",
  Status = "status",
  StatusDescription = "description",
  Location = "location",
  Image = "image",

  // Location child
  Longitude = "longitude",
  Latitude = "latitude",

  // Extends for body create/modify
  UserEmail = "user_email",
  Type = "type",
}

export type FormField = {
  [FieldType.AssetName]: string;
  [FieldType.Serial]?: string;
  [FieldType.Unit]: any;
  [FieldType.CommissionedDate]: string | Date;
  [FieldType.AssetGroup]: any;
  [FieldType.PersonUse]: any;
  [FieldType.DepartmentUse]: any;
  [FieldType.InventoryClassify]: any;
  [FieldType.Room]: string;
  [FieldType.Model]: string;
  [FieldType.Manufacturer]: string;
  [FieldType.Vendor]: string;

  [FieldType.Amount]: string;
  [FieldType.Status]: string;
  [FieldType.StatusDescription]: string;
  [FieldType.Location]: any;
  [FieldType.Image]: any[];
};

export type CreateAssetBody = FormField & {
  [FieldType.Longitude]?: number;
  [FieldType.Latitude]?: number;

  [FieldType.UserEmail]: string;
  [FieldType.Type]: "I" | "U";
};

export const defaultValues = (data: any): FormField => {
  const reducer: any = useSelector<AppState | null>(s => s?.home);
  const { units } = reducer.filterData;
  const currentUnit = units?.find((ele: any) => ele.id === data?.unit);

  return {
    // Basic Info
    [FieldType.AssetName]: data?.description || "",
    [FieldType.Serial]: data?.serial || "",
    [FieldType.Unit]: data?.unit
      ? {
          id: data?.unit || "",
          label: currentUnit?.label || "",
        }
      : "",
    [FieldType.AssetGroup]: data?.asset_group
      ? {
          id: data?.asset_group || "",
          label: data?.asset_groupname || "",
        }
      : "",
    [FieldType.CommissionedDate]: data?.use_date
      ? moment(DateTimeUtil.convertStringToDate(data?.use_date)).toDate()
      : "",
    [FieldType.DepartmentUse]: "",
    [FieldType.PersonUse]: data?.person
      ? {
          id: data?.person || "",
          label: data?.person_name || "",
        }
      : "",
    [FieldType.InventoryClassify]: data?.inventory_type
      ? {
          id: data?.inventory_type || "",
          label: inventoryTypes.find((ele: any) => ele.id === data?.inventory_type)?.label || "",
        }
      : "",
    [FieldType.Room]: data?.room || "",
    [FieldType.Model]: data?.model || "",
    [FieldType.Manufacturer]: data?.manufacture || "",
    [FieldType.Vendor]: data?.vendor || "",

    // Inventory Info
    [FieldType.Amount]: "",
    [FieldType.Status]: "",
    [FieldType.StatusDescription]: "",
    [FieldType.Location]: {
      latitude: null,
      longitude: null,
    },
    [FieldType.Image]: [],
  };
};

export const I18n = {
  CreateNewAsset: i18n.t("CA.CreateNewAsset"),
  BasicInfo: i18n.t("CA.BasicInfo"),
  AssetName: i18n.t("CA.AssetName"),
  PlhAssetName: i18n.t("CA.PlhAssetName"),
  Serial: i18n.t("CA.Serial"),
  PlhSerial: i18n.t("CA.PlhSerial"),
  Unit: i18n.t("CA.Unit"),
  PlhUnit: i18n.t("CA.PlhUnit"),
  AssetGroup: i18n.t("CA.AssetGroup"),
  PlhAssetGroup: i18n.t("CA.PlhAssetGroup"),
  CommissionedDate: i18n.t("CA.CommissionedDate"),
  PlhCommissionedDate: i18n.t("CA.PlhCommissionedDate"),
  DepartmentUse: i18n.t("CA.DepartmentUse"),
  PlhDepartmentUse: i18n.t("CA.PlhDepartmentUse"),
  PersonUse: i18n.t("CA.PersonUse"),
  PlhPersonUse: i18n.t("CA.PlhPersonUse"),
  InventoryClassify: i18n.t("CA.InventoryClassify"),
  PlhInventoryClassify: i18n.t("CA.PlhInventoryClassify"),
  Room: i18n.t("CA.Room"),
  PlhRoom: i18n.t("CA.PlhRoom"),
  Model: i18n.t("CA.Model"),
  PlhModel: i18n.t("CA.PlhModel"),
  Manufacturer: i18n.t("CA.Manufacturer"),
  PlhManufacturer: i18n.t("CA.PlhManufacturer"),
  Vendor: i18n.t("CA.Vendor"),
  PlhVendor: i18n.t("CA.PlhVendor"),

  InventoryInfo: i18n.t("CA.InventoryInfo"),
  Amount: i18n.t("CA.Amount"),
  PlhAmount: i18n.t("CA.PlhAmount"),
  Status: i18n.t("CA.Status"),
  PlhStatus: i18n.t("CA.PlhStatus"),
  StatusDescription: i18n.t("CA.StatusDescription"),
  PlhStatusDescription: i18n.t("CA.PlhStatusDescription"),
  Location: i18n.t("CA.Location"),
  PlhLocation: i18n.t("CA.PlhLocation"),
  Image: i18n.t("CA.Image"),

  CreateNewSuccess: i18n.t("CA.CreateNewSuccess"),
  ButtonClose: i18n.t("CM.Button.Close"),
  MsgRequired: i18n.t("CA.MsgRequired"),

  ImageError: i18n.t("CA.ImageError"),
  ImageMax: i18n.t("CA.ImageMax"),
  titleInventoryFailure: i18n.t("CA.titleInventoryFailure"),
  messageInventoryFailure: i18n.t("CA.messageInventoryFailure"),
};

// external
import { useNavigation, useRoute } from "@react-navigation/native";
import React, { FC, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { ImageBackground, ScrollView, TouchableOpacity, View } from "react-native";
import QRCode from "react-native-qrcode-svg";
import Share from "react-native-share";
import { captureRef } from "react-native-view-shot";
import { get, isEmpty } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import moment from "moment";

// Icons
import { BGQrCode, ICEdit, ICPlus, ICPrinter, ICShareNetwork, IMAvatarDefault } from "@Assets/image";

// internal
import { Header, ImageCache, TextCM, MapOnlyView } from "@Components";
import {
  heightResponsive as H,
  widthResponsive as W,
  InventoryType,
  ScreenName,
  StatusTypes,
  ScreenWidth,
  Types,
} from "@Constants";
import { dataImageInventoryFake, detailAssetI18n } from "./data";
import ModalPrintQR from "./modal-print-qr";
import styles from "./styles";
import {
  IBasicInformationProps,
  IContentComponentProps,
  IDetailAssetProps,
  IImageViewMoreProps,
  IInventoryInformationProps,
} from "./types";
import { getFullDMS } from "./utils";
import { DetailAssetsAction } from "@Actions";
import { AppState } from "@Reducers";
import { DateTimeUtil } from "@Utils";
import ImageView from "@Screens/inventory-asset/images-view";

const ViewContentComponent: FC<IContentComponentProps> = ({
  label,
  value,
  isMarginBottom = true,
  componentLeft,
  styleComponentRight = {},
}) => (
  <View style={[styles.ctnLabelAndValue, isMarginBottom && styles.marginLabelAndValue]}>
    <TextCM style={[styles.txtGeneral, styles.txtLabel, styles.viewLabelAndValue]}>{label}</TextCM>
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        ...styles.viewLabelAndValue,
      }}>
      {componentLeft && componentLeft}
      <TextCM style={[styles.txtGeneral, styles.txtValue, styleComponentRight]}>{value}</TextCM>
    </View>
  </View>
);

const ImageViewMore: FC<IImageViewMoreProps> = ({ uri, remainingAmount = 0 }) => (
  <View style={styles.ctnImageViewMore}>
    <ImageCache style={styles.ctnImage} uri={uri} imageDefault={uri} />
    <View style={styles.ctnRemainingAmount}>
      <TextCM style={styles.txtRemainingAmount}>{`+${remainingAmount}`}</TextCM>
    </View>
  </View>
);

const BasicInformationComponent: FC<IBasicInformationProps> = ({ data = {}, onEditInfoBasic, role }) => (
  <View style={styles.ctnInfoBasic}>
    <View style={styles.ctnInfoBasicTitle}>
      <TextCM style={styles.txtTitleInfoBasic}>{detailAssetI18n.txtInfoBasic}</TextCM>
      {role.includes(Types.PERMISSION_SAP.A70) ? (
        <TouchableOpacity activeOpacity={0.7} onPress={onEditInfoBasic}>
          <ICEdit width={H(24)} height={H(24)} />
        </TouchableOpacity>
      ) : (
        <></>
      )}
    </View>
    <ViewContentComponent label={detailAssetI18n.txtCodeAsset} value={data?.asset || ""} />
    <ViewContentComponent label={detailAssetI18n.txtSerial} value={data?.serial || ""} />
    <ViewContentComponent label={detailAssetI18n.txtUnit} value={data?.unit?.label || ""} />
    <ViewContentComponent
      label={detailAssetI18n.txtGroupAsset}
      value={data?.asset_groupname || data?.asset_group || ""}
    />
    <ViewContentComponent
      label={detailAssetI18n.txtDateToUsed}
      value={
        data?.use_date && moment(DateTimeUtil.convertStringToDate(data?.use_date)).isValid()
          ? moment(DateTimeUtil.convertStringToDate(data?.use_date)).format("DD/MM/YYYY")
          : ""
      }
    />
    <ViewContentComponent label={detailAssetI18n.txtPartUsed} value={data?.costcenter_name || data?.costcenter || ""} />
    <ViewContentComponent
      label={detailAssetI18n.txtUser}
      value={data?.person_name || data?.person || ""}
      componentLeft={
        data?.avatar ? (
          <ImageCache
            imageDefault={IMAvatarDefault}
            style={styles.imgAvatar}
            id={data?.avatar || ""}
            uri={data?.avatar || ""}
            isDownLoad={!!data?.avatar}
          />
        ) : data?.person_name ? (
          <View style={styles.ctnAvatar}>
            <TextCM style={styles.txtNameAvatar}>{data?.person_name?.charAt(0).toUpperCase() || ""}</TextCM>
          </View>
        ) : (
          <></>
        )
      }
      styleComponentRight={{
        width: (ScreenWidth - W(79)) / 2 - W(20),
        paddingLeft: W(8),
      }}
    />
    <ViewContentComponent
      label={detailAssetI18n.txtInventoryClassfication}
      value={data?.inventory_type ? InventoryType[data?.inventory_type] : ""}
    />
    <ViewContentComponent label={detailAssetI18n.txtRoom} value={data?.room || ""} />
    <ViewContentComponent label={detailAssetI18n.txtModel} value={data?.model || ""} />
    <ViewContentComponent label={detailAssetI18n.txtProducer} value={data?.manufacture || ""} />
    <ViewContentComponent label={detailAssetI18n.txtVendor} value={data?.vendor || ""} isMarginBottom={false} />
  </View>
);

const InventoryInformationComponent: FC<IInventoryInformationProps> = ({ data = {} }) => {
  const navigation = useNavigation();

  const onNavigateInventoryPicture = useCallback(
    (index: number) => {
      navigation?.navigate(
        ScreenName.InventoryPicture as never,
        {
          data: {
            images: data?.image || [],
            location:
              data?.latitude && data?.longitude ? getFullDMS(Number(data?.latitude), Number(data?.longitude)) : "",
          },
          index,
        } as never
      );
    },
    [data]
  );

  return (
    <View style={styles.ctnInfoInventory}>
      <View style={styles.ctnInfoInventoryText}>
        <TextCM style={styles.txtTitle}>{detailAssetI18n.txtInfoInventory}</TextCM>
        <View style={{ flexDirection: "row" }}>
          <TextCM style={[styles.txtLabel, styles.marginRight5]}>{detailAssetI18n.txtLastInventory}</TextCM>
          <TextCM style={styles.txtValue}>
            {`${data?.inventory?.inv_time ? DateTimeUtil.convertStringToTime(data?.inventory?.inv_time) : ""}${
              data?.inventory?.inv_time && data?.inventory?.inv_date ? " - " : ""
            }${data?.inventory?.inv_date ? DateTimeUtil.convertStringToDate(data?.inventory?.inv_date) : ""}`}
          </TextCM>
        </View>
      </View>
      <ViewContentComponent label={detailAssetI18n.txtQuality} value={parseInt(data?.inventory?.quantity) || "0"} />
      <ViewContentComponent
        label={detailAssetI18n.txtStatus}
        value={data?.inventory?.status ? StatusTypes[data?.inventory?.status] : ""}
      />
      <ViewContentComponent label={detailAssetI18n.txtDescriptionStatus} value={data?.inventory?.description || ""} />
      <ViewContentComponent
        label={detailAssetI18n.txtLocation}
        value={data?.latitude && data?.longitude ? getFullDMS(Number(data?.latitude), Number(data?.longitude)) : ""}
      />
      <View style={styles.ctnMap}>
        <MapOnlyView
          style={styles.map}
          region={{
            latitude: data?.latitude ? Number(data?.latitude) : 15.9783846,
            longitude: data?.longitude ? Number(data?.longitude) : 108.2598785,
          }}
        />
      </View>
      <View
        style={[
          styles.ctnLabelAndValue,
          {
            marginBottom: H(12),
          },
        ]}>
        <TextCM style={[styles.txtGeneral, styles.txtLabel]}>{detailAssetI18n.txtLastImageInventory}</TextCM>
      </View>
      <ImageView data={data?.image || []} onClick={onNavigateInventoryPicture} maxDisplay={6} widthImage={98} />
      {/* <View style={styles.ctnListImage}>
        {dataImageInventoryFake?.slice(0, 6)?.map((image: any, index: number) =>
          index === 5 && dataImageInventoryFake?.length > 6 ? (
            <TouchableOpacity
              key={'inventory-picture' + index}
              onPress={onNavigateInventoryPicture(index)}>
              <ImageViewMore
                uri={image}
                remainingAmount={dataImageInventoryFake?.length - 6}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              key={'inventory-picture' + index}
              onPress={onNavigateInventoryPicture(index)}>
              <ImageCache
                style={styles.ctnImage}
                imageDefault={image}
                // uri={image}
              />
            </TouchableOpacity>
          ),
        )}
      </View> */}
    </View>
  );
};

const DetailAssetsScreen: FC<IDetailAssetProps> = ({}) => {
  const appState: AppState = useSelector((state: AppState) => state);
  const { detailAssets, home: { filterData: { units = [] } = {} } = {} }: any = appState;

  const viewRef = useRef<any>();
  const refScroll = useRef<any>();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const route = useRoute();
  const params = get(route, "params", {} as any);
  const qrcode = get(params, "qrcode", undefined as any);
  const data = get(detailAssets, "data", {} as any);
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const role = get(reducer, "userInfo.data.role", []);

  const currentUnit = useMemo(() => (data?.unit ? units?.find((ele: any) => ele.id === data?.unit) : {}), [data]);

  const [isShowModalPrint, setIsShowModalPrint] = useState<boolean>(false);

  useEffect(() => {
    if (qrcode) {
      dispatch(DetailAssetsAction.getDetailAssetAction.request(qrcode));
    }
    return () => {
      dispatch(DetailAssetsAction.getDetailAssetAction.failure({}));
    };
  }, [qrcode]);

  useEffect(() => {
    refScroll.current?.scrollTo({
      y: 0,
      animated: true,
    });
  }, [data]);

  const onPressHistory = useCallback(() => {
    navigation?.navigate(ScreenName.History as never, { qrcode } as never);
  }, []);

  const RightComponent = () => {
    return (
      <TouchableOpacity style={{ zIndex: 1 }} onPress={onPressHistory}>
        <TextCM style={styles.txtHistory}>{detailAssetI18n.txtHistory}</TextCM>
      </TouchableOpacity>
    );
  };

  // Handle sharing
  const onShareAsset = async () => {
    try {
      // capture component
      const uri = await captureRef(viewRef, {
        format: "png",
        quality: 1,
      });

      // share
      const shareResponse = await Share.open({ url: uri });
      console.log(shareResponse, "shareResponse");
    } catch (error) {
      console.log("error", error);
    }
  };

  const onToggleModalPrint = useCallback(() => {
    setIsShowModalPrint(!isShowModalPrint);
  }, [isShowModalPrint]);

  const onCreateAsset = () => navigation.navigate(ScreenName.CreateAsset as never);

  const onEditInfoBasic = () => navigation.navigate(ScreenName.UpdateAsset as never);

  const onClickInventory = () => navigation.navigate(ScreenName.InventoryAsset as never);

  return (
    <View style={styles.ctn}>
      <Header title={detailAssetI18n.txtHeader} componentRight={<RightComponent />} />
      <ScrollView style={styles.ctnScrollView} horizontal={false} ref={refScroll}>
        {/* QR Code */}
        <ImageBackground source={BGQrCode} resizeMode="stretch" style={styles.bgCtnQrcode}>
          <View style={styles.viewQrCodeAndShared}>
            <View style={styles.viewEmptyIC} />
            <View style={styles.bgQrcode} ref={viewRef}>
              <QRCode value={data?.qrcode || "SGR"} />
            </View>
            <TouchableOpacity style={styles.btnIcon24} onPress={onShareAsset}>
              <ICShareNetwork width={H(24)} height={H(24)} />
            </TouchableOpacity>
          </View>

          <TextCM style={styles.txtNameAsset}>{data?.description || ""}</TextCM>

          <View
            style={[
              styles.ctnGroupButton,
              role.includes(Types.PERMISSION_SAP.A70) ? styles.justBetween : styles.justCenter,
            ]}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={onToggleModalPrint}
              style={[styles.ctnButton, styles.btnPrint]}>
              <ICPrinter width={H(24)} height={H(24)} />
              <TextCM style={styles.lblButton}>{detailAssetI18n.txtPrintQR}</TextCM>
            </TouchableOpacity>

            {role.includes(Types.PERMISSION_SAP.A70) ? (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={onCreateAsset}
                style={[styles.ctnButton, styles.btnCreate]}>
                <ICPlus width={H(24)} height={H(24)} />
                <TextCM style={styles.lblButton}>{detailAssetI18n.txtCreateNewAsset}</TextCM>
              </TouchableOpacity>
            ) : (
              <></>
            )}
          </View>
        </ImageBackground>

        {/* Not release */}
        <InventoryInformationComponent data={data} />

        <BasicInformationComponent
          data={{ ...data, unit: currentUnit }}
          onEditInfoBasic={onEditInfoBasic}
          role={role}
        />

        {role.includes(Types.PERMISSION_SAP.A70) ? (
          <TouchableOpacity
            style={[
              styles.ctnBtnInventory,
              {
                marginBottom: insets.bottom + H(16),
              },
            ]}
            onPress={onClickInventory}>
            <TextCM style={styles.btnInventory}>{detailAssetI18n.btnInventory}</TextCM>
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </ScrollView>
      {isShowModalPrint && <ModalPrintQR isShow={isShowModalPrint} data={data} onClose={onToggleModalPrint} />}
    </View>
  );
};

export default DetailAssetsScreen;

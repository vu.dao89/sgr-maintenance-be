// External
// import CameraRoll from '@react-native-community/cameraroll';
// import moment from 'moment';
import React, { FC, useEffect, useRef, useState } from "react";
import { FlatList, Platform, TouchableOpacity, View } from "react-native";
import Modal from "react-native-modal";
import QRCode from "react-native-qrcode-svg";
import { SafeAreaView, useSafeAreaInsets } from "react-native-safe-area-context";
import { captureRef } from "react-native-view-shot";
import {
  discoverPrinters,
  printImage,
  registerBrotherListener,
  searchWiFiPrinter,
} from "react-native-brother-printers";

// IC, IMG
import { ICClose, ICPrinterActive, ICPrinterLight, ICRefreshPrint, ICSelected, IMPrinting } from "@Assets/image";

// Internal
import { ImageCache, PopupAnalog, TextCM } from "@Components";
import { Color, heightResponsive as H, widthResponsive as W } from "@Constants";
// import {DateTimeUtil} from '@Utils';
import { modalPrintQrCode } from "../data";
import {
  // IContentComponentProps,
  IModalPrintProps,
} from "../types";
import styles from "./styles";
import { isArray } from "lodash";
import { MaterialIndicator } from "react-native-indicators";
// import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';

// const ViewContentComponent: FC<IContentComponentProps> = ({
//   label = '',
//   value = '',
//   isBorderBottom = true,
// }) => (
//   <View
//     style={[styles.ctnLabelAndValue, isBorderBottom && styles.borderBottom]}>
//     <View style={styles.ctnLabel}>
//       <TextCM style={styles.txtLabel}>{label}</TextCM>
//     </View>
//     <View style={styles.ctnValue}>
//       <TextCM style={styles.txtValue} ellipsizeMode="tail" numberOfLines={1}>
//         {value}
//       </TextCM>
//     </View>
//   </View>
// );

const ModalPrintQR: FC<IModalPrintProps> = ({ isShow = false, data = {}, onClose }) => {
  const viewRef = useRef<any>();
  const insets = useSafeAreaInsets();

  const [stateAnaLog, setStateAnaLog] = useState<any>({
    visible: false,
    type: "success",
    content: "",
  });

  const [listPrinterDevices, setListPrinterDevices] = useState<any>([]);

  const [deviceSelected, setDeviceSelected] = useState<any | null>(null);

  const [isLoadingSearch, setLoadingSearch] = useState<boolean>(true);
  const [isPrinting, setIsPrinting] = useState<boolean>(false);
  const refTimeout = useRef<any>(null);

  const onCloseAnalog = () => {
    setStateAnaLog({
      visible: false,
      type: "success",
      content: "",
    });
  };

  useEffect(() => {
    _searchPrinter();
  }, []);

  useEffect(() => {
    if (Platform.OS === "ios") {
      registerBrotherListener("onDiscoverPrinters", (printers: any) => {
        // Store these printers somewhere
        setListPrinterDevices(printers);
        setLoadingSearch(false);
      });
    }
  }, []);

  const _searchPrinter = () => {
    if (Platform.OS === "android") {
      searchWiFiPrinter(printers => {
        if (isArray(printers?.devices) && printers?.devices.length > 0) setListPrinterDevices(printers?.devices);

        setLoadingSearch(false);
      });
    } else {
      discoverPrinters({
        printerName: "Brother PT-P950NW",
      });
    }
  };

  const renderItemDevice = ({ item }: any) => {
    const onSelecteDevice = () => {
      setDeviceSelected(item?.ipAddress === deviceSelected?.ipAddress ? null : item);
    };
    return (
      <TouchableOpacity key={`item-${item?.ipAddress}`} style={styles.ctnDevice} onPress={onSelecteDevice}>
        <View style={styles.ctnDeviceName}>
          {item?.ipAddress === deviceSelected?.ipAddress ? (
            <ICPrinterActive width={W(24)} height={W(24)} />
          ) : (
            <ICPrinterLight width={W(24)} height={W(24)} />
          )}
          <TextCM style={[styles.txtNameDevice, item?.ipAddress === deviceSelected?.ipAddress && styles.txtNameActive]}>
            {`Brother PT-P950NW - ${Platform.OS === "android" ? item?.serNo : item?.serialNumber}`}
          </TextCM>
        </View>
        {item?.ipAddress === deviceSelected?.ipAddress && <ICSelected width={W(24)} height={W(24)} />}
      </TouchableOpacity>
    );
  };

  const onPrintQrCode = async () => {
    setIsPrinting(true);
    const uri = await captureRef(viewRef, {
      format: "png",
      result: "tmpfile",
      width: 226.77165354 * 4, // 6cm
      height: 136.06299213 * 4, // 3.6cm
    });
    refTimeout.current = setTimeout(() => {
      setIsPrinting(false);
      setStateAnaLog({
        visible: true,
        type: "error",
        content: modalPrintQrCode.txtPrintQRFailure,
      });
    }, 15000);
    printImage({ ip: deviceSelected?.ipAddress }, uri, {
      onSuccess: () => {
        setIsPrinting(false);
        setStateAnaLog({
          visible: true,
          type: "success",
          content: modalPrintQrCode.txtPrintQRSuccess,
        });
        clearTimeout(refTimeout.current);
      },
      onFailure: _ => {
        clearTimeout(refTimeout.current);
        setTimeout(() => {
          setIsPrinting(false);
          setStateAnaLog({
            visible: true,
            type: "error",
            content: modalPrintQrCode.txtPrintQRFailure,
          });
        }, 500);
      },
    });
  };

  const onCloseAnalogPrinting = () => {
    setIsPrinting(false);
  };

  // refresh find device
  const onRefresh = () => {
    setLoadingSearch(true);
    _searchPrinter();
  };
  return (
    <>
      <Modal isVisible={isShow} animationOutTiming={1} style={styles.ctnModal}>
        <View style={[styles.container]}>
          {/* Header */}
          <View style={styles.ctnHeader}>
            <View style={styles.emptyHeader} />
            <TextCM style={styles.txtHeader}>{modalPrintQrCode.txtHeader}</TextCM>
            <TouchableOpacity style={styles.btnIcon24} onPress={onClose}>
              <ICClose width={H(24)} height={H(24)} />
            </TouchableOpacity>
          </View>

          {/* Body */}
          <View style={styles.ctnBodyConnected}>
            {/* <View style={styles.ctnContentPrint} ref={viewRef}>
              <View style={styles.ctnQrCode}>
                <View style={styles.bgQrcode}>
                  <QRCode value={data?.qrcode || 'SGR'} size={W(76)} />
                </View>
              </View>
              <View style={styles.ctnDetailAsset}>
                <ViewContentComponent
                  label={modalPrintQrCode.txtGroupAsset}
                  value={data?.asset_groupname || data?.asset_group || ''}
                />
                <ViewContentComponent
                  label={modalPrintQrCode.txtCodeAsset}
                  value={data?.asset || ''}
                />
                <ViewContentComponent
                  label={modalPrintQrCode.txtTimeUsed}
                  value={
                    data?.use_date
                    ? moment(DateTimeUtil.convertStringToDate(data?.use_date)).format('DD/MM/YYYY')
                    : ''
                  }
                />
                <ViewContentComponent
                  label={modalPrintQrCode.txtUnitUsed}
                  value={data?.unit || ''}
                />
                <ViewContentComponent
                  label={modalPrintQrCode.txtUser}
                  value={data?.person_name || data?.person || ''}
                />
                <ViewContentComponent
                  label={modalPrintQrCode.txtSerial}
                  value={data?.serial || ''}
                  isBorderBottom={false}
                />
              </View>
            </View> */}
            <View style={{ borderRadius: H(12), overflow: "hidden" }}>
              <View style={styles.ctnContentPrintRelease} ref={viewRef}>
                <QRCode value={data?.qrcode || "SGR-DEMO-QR"} size={W(100)} />
                <TextCM style={styles.txtQrCode} numberOfLines={1}>
                  {data?.qrcode || ""}
                </TextCM>
                <TextCM style={styles.txtNameAsset} numberOfLines={2}>
                  {data?.description || ""}
                </TextCM>
              </View>
            </View>
          </View>
          <View style={styles.ctnListDevices}>
            <View style={styles.ctnChooseDevice}>
              <TextCM style={styles.txtChooseDevice}>{modalPrintQrCode.txtChooseDevice}</TextCM>
              <TouchableOpacity
                hitSlop={{
                  right: W(12),
                  left: W(12),
                  top: H(12),
                  bottom: H(12),
                }}
                onPress={onRefresh}
                disabled={isLoadingSearch}>
                <ICRefreshPrint width={H(24)} height={H(24)} opacity={isLoadingSearch ? 0.5 : 1} />
              </TouchableOpacity>
            </View>

            {isLoadingSearch ? (
              <MaterialIndicator color={Color.Yellow} size={H(40)} style={{ marginVertical: H(32) }} />
            ) : listPrinterDevices?.length > 0 ? (
              <FlatList
                data={listPrinterDevices}
                horizontal={false}
                renderItem={renderItemDevice}
                style={{
                  height: H(765) - (insets.bottom + H(408)),
                }}
                keyExtractor={({ item }: any) => `key-${item?.ipAddress}`}
              />
            ) : (
              <View
                style={{
                  ...styles.ctnNotDevice,
                  height: H(765) - (insets.bottom + H(408)),
                }}>
                <TextCM style={styles.txtNotDevice}>{modalPrintQrCode.txtNotFindDevice}</TextCM>
              </View>
            )}
            {/* {
              !isLoadingSearch && listPrinterDevices?.length > 0
              ? (
                <FlatList
                  data={listPrinterDevices}
                  horizontal={false}
                  renderItem={renderItemDevice}
                  style={{
                    height: H(765) - (insets.bottom + H(400))
                  }}
                  extraData={({item}: any) => item?.ipAddress}
                />
              )
              : (
                <View style={{
                  ...styles.ctnNotDevice,
                  height: H(765) - (insets.bottom + H(400))
                }}>
                  <TextCM style={styles.txtNotDevice}>{modalPrintQrCode.txtNotFindDevice}</TextCM>
                </View>
              )
            } */}
          </View>
        </View>

        {/* Button footer */}
        <SafeAreaView style={styles.ctnFooterAction}>
          <TouchableOpacity
            style={[
              styles.ctnBtnSettingBL,
              {
                marginBottom: insets.bottom + H(16),
              },
              !deviceSelected && styles.ctnBtnSettingBLDisabled,
            ]}
            disabled={!deviceSelected}
            onPress={onPrintQrCode}>
            <TextCM style={styles.btnSettingBL}>{modalPrintQrCode.btnExportImage}</TextCM>
          </TouchableOpacity>
        </SafeAreaView>

        <PopupAnalog
          visible={stateAnaLog?.visible}
          type={stateAnaLog.type}
          onPressOutside={onCloseAnalog}
          onPressConfirm={onCloseAnalog}
          titlePopup={stateAnaLog?.content}
          titleButton={modalPrintQrCode.btnClose}
          onClose={onCloseAnalog}
        />
        <PopupAnalog
          visible={isPrinting}
          type={"custom"}
          iconShow={
            <ImageCache
              imageDefault={IMPrinting}
              style={{
                width: "100%",
                height: H(150),
              }}
            />
          }
          onPressConfirm={() => {}}
          titlePopup={modalPrintQrCode.txtPrinting}
          titleButton={""}
          onClose={onCloseAnalogPrinting}
          isShowClose={false}
        />
      </Modal>
    </>
  );
};

export default ModalPrintQR;

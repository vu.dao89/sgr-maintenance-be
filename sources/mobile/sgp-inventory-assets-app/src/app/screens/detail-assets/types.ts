import { ReactElement } from "react";
import { StyleProp, ViewStyle } from "react-native";

export type IDetailAssetProps = {
  navigation?: any;
  route?: any;
};

export type IContentComponentProps = {
  label: string;
  value: string | number;
  isMarginBottom?: boolean;
  isBorderBottom?: boolean;
  componentLeft?: ReactElement<any>;
  styleComponentRight?: StyleProp<ViewStyle>;
};

export type IImageViewMoreProps = {
  uri: string;
  remainingAmount: number;
};

export type IModalPrintProps = {
  isShow: boolean;
  data: any;
  onClose: () => void;
};

export type IBasicInformationProps = {
  data: any;
  onEditInfoBasic: () => void;
  role?: any;
};
export type IInventoryInformationProps = {
  data: any;
};
export type CoordinatesState = {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
};

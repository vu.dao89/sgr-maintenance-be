import { truncate } from "src/utils/Format";

export const getDMS = (dd: number, longOrLat: "long" | "lat") => {
  let hemisphere = /^[WE]|(?:lon)/i.test(longOrLat) ? (dd < 0 ? "W" : "E") : dd < 0 ? "S" : "N";

  const absDD = Math.abs(dd);
  const degrees = truncate(absDD);
  const minutes = truncate((absDD - degrees) * 60);
  const seconds = ((absDD - degrees - minutes / 60) * Math.pow(60, 2)).toFixed(2);

  let dmsArray = [degrees, minutes, seconds, hemisphere];
  return `${dmsArray[0]}°${dmsArray[1]}'${dmsArray[2]}" ${dmsArray[3]}`;
};

export const getFullDMS = (lat: number, long: number): string => {
  const latDMS = getDMS(lat, "lat");
  const longDMS = getDMS(long, "long");
  return `${latDMS} ${longDMS}`;
};

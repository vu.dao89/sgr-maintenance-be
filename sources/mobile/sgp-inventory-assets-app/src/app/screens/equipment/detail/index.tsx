import React, { FC, useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import i18n from "@I18n";

import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import { Permission, ScreenName } from "@Constants";

import TabViewRoutes from "./tabs";

import { ICMap, IMEQDefault } from "@Assets/image";
import { CEmptyState, CNavbar, CSelect, CTabViewDetail, CTags, ImageCache, TextCM } from "@Components";
import { useRoute } from "@react-navigation/native";
import { AppState } from "@Reducers";
import { get, isEmpty } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import { CommonActions } from "../../../../redux/actions";
import { EquipmentServices, PermissionServices } from "../../../../services";
import { axiosHandler } from "../../../../services/httpClient";
import { isArray } from "../../../../utils/Basic";
import { useIsMounted, useStateLazy } from "../../../../utils/Core";
import { dayjsFormatDate } from "../../../../utils/DateTime";
import { formatObjectLabel, genAttachFiles, removeLeadingZeros } from "../../../../utils/Format";

const EquipmentDetail: FC<any> = props => {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();
  const toast = useToast();

  const [headerInfoMinHeight, setHeaderInfoMinHeight] = useState(0);
  const [tabViewMinHeight, setTabViewMinHeight] = useState(0);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const employeeCode = PermissionServices.getEmployeeCode(reducer);

  const route = useRoute();
  const params = get(route, "params", {} as any);

  const [state, setState] = useState(getScreenParams(props));

  const [detail, setDetail]: any = useState(null);
  const [isEmptyData, setEmptyData] = useState(false);

  const [isCreateAction, setCreateAction] = useState(false);
  const [attachFiles, setAttachFiles] = useStateLazy([]);

  const havePermission = PermissionServices.checkPermission(reducer, Permission.EQUIPMENT.VIEW);

  useEffect(() => {
    if (havePermission) {
      const equipmentId = get(params, "params.equipmentId");
      handleGetDetail(equipmentId);
    } else {
      setEmptyData(true);
    }
  }, []);

  const handleGetDetail = async (equipmentId: string) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => EquipmentServices.getEquipment(equipmentId));

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    const equipment = get(response, "data");

    if (equipment) {
      const {
        equipmentId,
        description,
        longDesc,
        equipmentTypeId,
        equipmentType,
        equipmentCategoryId,
        equipmentCategory,
        functionalLocationId,
        functionalLocation,
        parentEquipmentId,
        parentEquipment,
        maintenancePlantId,
        maintenancePlant,
        maintenanceWorkCenterId,
        maintenanceWorkCenter,
        sortField,
        costCenter,
        costCenterName,
        weight,
        size,
        manufacturer,
        modelNumber,
        manuPartNo,
        manuSerialNo,
        startupDate,
        endOfUseDate,
        manuCountry,
        constYear,
        qrCode,
        partners,
        cusWarrantyDate,
        cusWarrantyEnd,
        venWarrantyDate,
        venWarrantyEnd,
        measuringPoints,
        documents,
      } = equipment;
      const equipmentCategoryDesc = get(equipmentCategory, "description", equipmentCategoryId);
      const equipmentTypeDesc = get(equipmentType, "description", equipmentTypeId);
      const _measuringPoints: any = measuringPoints?.map((m: any) => {
        return {
          point: m.point,
          rangeUnit: m.rangeUnit,
          description: m.description,
          lowerRangeLimit: m.lowerRangeLimit ? `${m.lowerRangeLimit} ${m.rangeUnit}` : "",
          upperRangeLimit: m.lowerRangeLimit ? `${m.lowerRangeLimit} ${m.rangeUnit}` : "",
        };
      });

      const _detail = {
        equipmentId: equipmentId,
        description: description,
        // header
        name: formatObjectLabel(equipmentId, description, true),
        desc: longDesc,
        tags: [
          { color: "#3A73B7", label: equipmentTypeDesc },
          { color: "#3A73B7", label: equipmentCategoryDesc },
        ],
        functionalLocation: formatObjectLabel(functionalLocationId, get(functionalLocation, "description", ""), true),
        // tab info - session 1
        equipmentCategory: formatObjectLabel(equipmentCategoryId, equipmentCategoryDesc, true),
        equipmentType: formatObjectLabel(equipmentTypeId, equipmentTypeDesc, true),
        parentEquipmentId: removeLeadingZeros(parentEquipmentId),
        parentEquipmentLabel: formatObjectLabel(parentEquipmentId, get(parentEquipment, "description", ""), true),
        //- session 2
        maintenancePlant: formatObjectLabel(maintenancePlantId, get(maintenancePlant, "plantName", ""), true),
        maintenanceWorkCenter: formatObjectLabel(
          maintenanceWorkCenterId,
          get(maintenanceWorkCenter, "description", ""),
          true
        ),
        sortField: sortField,
        costCenter: costCenter,
        costCenterName: formatObjectLabel(costCenter, costCenterName, true),
        //- session 3
        weight: weight ? `${weight} KG`.trim() : "",
        size: size,
        manufacturer: manufacturer,
        modelNumber: modelNumber,
        manuPartNo: manuPartNo,
        manuSerialNo: manuSerialNo,
        startupDate: dayjsFormatDate(startupDate),
        endOfUseDate: dayjsFormatDate(endOfUseDate),
        manuCountry: manuCountry,
        constYear: constYear,
        qrCode,
        //- session 4
        partners: isArray(partners, 1)
          ? partners.map((p: any) => {
              return {
                label: "VW - person response",
                value: formatObjectLabel(p.partnerNumber, p.partnerFunction),
              };
            })
          : [],
        //- session 4
        cusWarrantyDate: dayjsFormatDate(cusWarrantyDate),
        cusWarrantyEnd: dayjsFormatDate(cusWarrantyEnd),
        venWarrantyDate: dayjsFormatDate(venWarrantyDate),
        venWarrantyEnd: dayjsFormatDate(venWarrantyEnd),
        // measuring points
        measuringPoints: _measuringPoints,
        //
        raw: equipment,
      };
      setDetail(_detail);
      setAttachFiles(genAttachFiles(documents));
    } else {
      setEmptyData(true);
    }
  };

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "postMeasPoint":
        toast.show("Lấy điểm đo thành công", { type: "success" });
        break;
      case "a":
      case "b":
        break;
      case "attachFiles":
        setAttachFiles([...data, ...attachFiles]);
        break;
      default:
        setState({ ...state, [id]: data });
        break;
    }
  };

  const handleSelectAction = (menu: any) => {
    const item = get(menu, "value");
    const equipment = detail.raw;

    switch (item) {
      case "createNoti":
        changeScreen(props, ScreenName.NotificationCreate, { openFrom: "equipDetail", equipment: equipment });
        break;
      case "createWorkOrder":
        changeScreen(props, ScreenName.WorkOrderCreate, { openFrom: "equipDetail", equipment: equipment });
        break;
      default:
        break;
    }
  };

  return (
    <View style={styles.screen}>
      <CNavbar title={i18n.t("DEQ.Title")} onLeftPress={() => changeScreen(props)} />

      {isEmptyData && <CEmptyState noPermission={!havePermission} />}

      {!!detail && (
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            height: headerInfoMinHeight + tabViewMinHeight,
          }}>
          <View style={styles.content} onLayout={e => setHeaderInfoMinHeight(e.nativeEvent.layout.height)}>
            <View style={styles.equipmentNameContainer}>
              <View style={styles.equipmentNameWrapper}>
                <TextCM style={styles.lblEquipmentName}>{detail.name}</TextCM>
                <View style={styles.tagsContainer}>
                  <CTags data={detail.tags} style={styles.infoTags} />
                </View>
              </View>
              <View style={styles.equipmentImageWrapper}>
                <ImageCache imageDefault={IMEQDefault} style={styles.imgEq} />
              </View>
            </View>

            {!isEmpty(detail.desc) && <TextCM style={styles.txtEquipmentDesc}>{detail.desc}</TextCM>}

            {!isEmpty(detail.functionalLocation) && (
              <TouchableOpacity style={styles.equipmentLocationContainer}>
                <ICMap />
                <TextCM style={styles.txtEquipLocation}>{detail.functionalLocation}</TextCM>
              </TouchableOpacity>
            )}

            <TouchableOpacity style={styles.btnCreateAction} onPress={() => setCreateAction(true)}>
              <TextCM style={styles.txtBtnCreateAction}>{`${i18n.t("DEQ.BtnCreateAction")}`}</TextCM>
            </TouchableOpacity>
          </View>

          <CTabViewDetail
            detail={detail}
            employeeCode={employeeCode}
            attachFiles={attachFiles}
            routes={TabViewRoutes}
            onChange={handleChange}
            onLayout={setTabViewMinHeight}
            onNavigate={(...args: any) => changeScreen(props, ...args)}
          />
        </ScrollView>
      )}
      <CSelect
        noInput
        isOpen={isCreateAction}
        menus={[
          {
            label: "Tạo lệnh thông báo",
            value: "createNoti",
          },
          {
            label: "Tạo lệnh bảo trì",
            value: "createWorkOrder",
          },
        ]}
        onSelect={handleSelectAction}
        onClose={() => setCreateAction(false)}
      />
    </View>
  );
};

export default EquipmentDetail;

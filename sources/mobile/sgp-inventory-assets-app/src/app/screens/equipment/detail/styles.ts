import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  fetchingWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: WR(16),
  },
  container: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  content: {
    paddingHorizontal: WR(20),
    paddingTop: WR(10),
  },
  lblEquipmentName: {
    color: Color.White,
    fontSize: FontSize.FontBigger,
    fontWeight: "700",
  },
  equipmentNameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  equipmentNameWrapper: {
    flex: 2,
  },
  equipmentImageWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  tagsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  infoTags: {
    marginTop: WR(8),
    marginBottom: WR(12),
  },
  tagWrapper: {
    backgroundColor: Color.BackgroundTagColor,
    padding: WR(3),
    margin: WR(8),
    borderRadius: HR(8),
  },
  txtTag: {
    color: Color.White,
    fontSize: FontSize.FontTiniest,
    fontWeight: "600",
  },
  txtEquipmentDesc: {
    marginTop: WR(15),
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  equipmentLocationContainer: {
    marginVertical: WR(10),
    flexDirection: "row",
    alignItems: "center",
  },
  iconMapPin: {
    marginRight: WR(5),
  },
  txtEquipLocation: {
    marginStart: WR(5),
    fontSize: FontSize.FontSmaller,
    color: Color.Yellow,
  },
  btnCreateAction: {
    marginVertical: WR(10),
    borderRadius: HR(10),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: "40%",
    paddingVertical: HR(10),
    backgroundColor: Color.Yellow,
  },
  txtBtnCreateAction: {
    color: Color.TextButton,
    fontSize: FontSize.FontSmaller,
    fontWeight: "500",
  },
  imgEq: {
    width: WR(80),
    height: HR(80),
    borderRadius: HR(20),
  },
  map: {
    height: HR(150),
    width: "100%",
    overflow: "hidden",
  },
});

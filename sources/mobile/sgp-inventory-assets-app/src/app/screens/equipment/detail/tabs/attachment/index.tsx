import React, { FC } from "react";

import { CAttachFiles, CEmptyState } from "@Components";
import { View } from "react-native";

import { widthResponsive as WR } from "@Constants";
import { get, isEmpty } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import { Permission } from "../../../../../../constants";
import { CommonActions } from "../../../../../../redux/actions";
import { AppState } from "../../../../../../redux/reducers";
import { DocumentServices, PermissionServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted } from "../../../../../../utils/Core";
import styles from "./styles";

const AttachmentDetail: FC<any> = ({ saInsets, attachFiles, onChange, detail }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();
  const equipmentId = get(detail, "equipmentId");

  const reducer: any = useSelector<AppState | null>(s => s?.common);

  const havePermission = PermissionServices.checkPermission(reducer, Permission.EQUIPMENT.ATTACHMENT_POST);

  const handleAttachFiles = async (files: any) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() => DocumentServices.uploadEquipmentDocuments(equipmentId, files));

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      onChange("attachFiles", files);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom + WR(36) }]}>
      <CAttachFiles files={attachFiles} onChange={havePermission && handleAttachFiles} />
      {isEmpty(attachFiles) && !havePermission && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default AttachmentDetail;

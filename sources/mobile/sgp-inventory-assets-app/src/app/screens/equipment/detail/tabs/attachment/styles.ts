import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  section: {
    marginTop: WR(20),
    paddingVertical: WR(28),
    paddingHorizontal: WR(20),
    borderRadius: WR(20),
    backgroundColor: "#262745",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
});

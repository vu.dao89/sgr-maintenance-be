import { get, groupBy } from "lodash";
import React, { FC } from "react";
import { View } from "react-native";

import { isArray } from "src/utils/Basic";

import styles from "./styles";

import { CCollapsibleSection } from "@Components";

const AttributeDetail: FC<any> = ({ saInsets, state }) => {
  const characteristics = get(state, ["params", "characteristics"], []);

  const grouped = groupBy(characteristics, "classId");

  const results: any[] = [];
  for (const [key, value] of Object.entries(grouped)) {
    const data = {
      key,
      infos: value.map((p: any) => {
        return {
          label: `${p.charValDesc}`,
          value: `${p.charValCounter || ""} ${p.charValue || ""}`,
        };
      }),
    };
    results.push(data);
  }
  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom }]}>
      {isArray(results, true) &&
        results.map((result, index) => {
          return (
            <CCollapsibleSection
              key={`${result.key}_${index}`}
              title={`Nhóm đặc tính ${results.length === 1 ? "" : index + 1}`}
              styles={{ wrapper: { marginTop: 0 } }}
              infoRow={{ label: "Tên đặc tính", unit: "Giá trị" }}
              infos={
                result && result.infos?.length > 0
                  ? result.infos.map((i: any) => {
                      return {
                        label: i.label,
                        value: i.value,
                      };
                    })
                  : []
              }
            />
          );
        })}
    </View>
  );
};

export default AttributeDetail;

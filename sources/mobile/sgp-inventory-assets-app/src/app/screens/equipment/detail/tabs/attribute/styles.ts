import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    padding: WR(24),
  },
  txtTabTitle: {
    fontWeight: "200",
    color: Color.GrayLight,
  },
});

import React, { FC } from "react";

import { View } from "react-native";

import i18n from "@I18n";

import styles from "./styles";

import { heightResponsive as H } from "@Constants";

import { CCollapsibleSection, MapOnlyView, TextCM } from "@Components";
import { isArray } from "../../../../../../utils/Basic";

const EquipmentDetailGeneral: FC<any> = ({ saInsets, detail }) => {
  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom }]}>
      <View>
        <TextCM style={styles.txtTabTitle}>Vị trí</TextCM>
        <View pointerEvents="none" style={styles.ctnMap}>
          <MapOnlyView
            style={styles.map}
            region={{
              latitude: 15.9783846,
              longitude: 108.2598785,
            }}
          />
        </View>
        <CCollapsibleSection
          title={i18n.t("DEQ.GeneralTabs.TitleInfo")}
          styles={{ wrapper: { marginTop: 0 } }}
          infos={[
            {
              label: "Loại thiết bị",
              value: detail?.equipmentCategory,
            },
            {
              label: "Nhóm thiết bị",
              value: detail?.equipmentType,
            },
            {
              label: "Thiết bị cha",
              value: detail?.parentEquipmentLabel,
            },
          ]}
        />
        <CCollapsibleSection
          title={i18n.t("DEQ.GeneralTabs.TitleInfoAdditional")}
          styles={{ wrapper: { marginTop: H(20) } }}
          infos={[
            {
              label: "Nơi bảo trì",
              value: detail?.maintenancePlant,
            },
            {
              label: "Tổ đội",
              value: detail?.maintenanceWorkCenter,
            },
            {
              label: "Mã nội bộ",
              value: detail?.sortField,
            },
            {
              label: "Bộ phận chịu chi phí",
              value: detail?.costCenterName,
            },
          ]}
        />
        <CCollapsibleSection
          title={i18n.t("DEQ.GeneralTabs.TitleInfoProducing")}
          styles={{ wrapper: { marginTop: H(20) } }}
          infos={[
            {
              label: "Trọng lượng",
              value: detail?.weight,
            },
            {
              label: "Kích thước",
              value: detail?.size,
            },
            {
              label: "Nhà sản xuất",
              value: detail?.manufacturer,
            },
            {
              label: "Số model",
              value: detail?.modelNumber,
            },
            {
              label: "Mã thiết bị của nhà sản xuất",
              value: detail?.manuPartNo,
            },
            {
              label: "Số Serial",
              value: detail?.manuSerialNo,
            },
            {
              label: `${i18n.t("CM.startDate")}`,
              value: detail?.startupDate,
            },
            {
              label: `${i18n.t("CM.endDate")}`,
              value: detail?.endOfUseDate,
            },
            {
              label: "Nước sản xuất",
              value: detail?.manuCountry,
            },
            {
              label: "Năm sản xuất",
              value: detail?.constYear,
            },
            {
              label: "Mã QR code",
              value: detail?.qrCode,
            },
          ]}
        />
        {isArray(detail?.partners, 1) ? (
          <>
            <CCollapsibleSection
              title={i18n.t("DEQ.subject")}
              styles={{ wrapper: { marginTop: H(20) } }}
              infos={detail?.partners}
            />
          </>
        ) : null}

        <CCollapsibleSection
          title={i18n.t("DEQ.insurance")}
          styles={{ wrapper: { marginTop: H(20) } }}
          infos={[
            {
              label: "",
              value: detail?.venWarrantyDate || detail?.cusWarrantyEnd ? "Bảo hành cho khách hàng" : "",
              labelVisible: !(detail?.venWarrantyDate || detail?.cusWarrantyEnd),
            },
            {
              label: `${i18n.t("CM.startDate")}`,
              value: detail?.venWarrantyDate,
              isGrouped: true,
            },
            {
              label: `${i18n.t("CM.endDate")}`,
              value: detail?.cusWarrantyEnd,
              isGrouped: true,
            },
            {
              label: "",
              value: detail?.venWarrantyDate || detail?.venWarrantyEnd ? "Bảo hành theo nhà cung cấp" : "",
              labelVisible: !(detail?.venWarrantyDate || detail?.venWarrantyEnd),
            },
            {
              label: `${i18n.t("CM.startDate")}`,
              value: detail?.venWarrantyDate,
              isGrouped: true,
            },
            {
              label: `${i18n.t("CM.endDate")}`,
              value: detail?.venWarrantyEnd,
              isGrouped: true,
            },
          ]}
        />
      </View>
    </View>
  );
};

export default EquipmentDetailGeneral;

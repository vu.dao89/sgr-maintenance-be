import { StyleSheet } from "react-native";

import { Color, heightResponsive as H, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    padding: WR(24),
  },
  ctnMap: {
    width: "100%",
    borderRadius: H(16),
    overflow: "hidden",
    marginBottom: H(16),
    marginTop: H(10),
  },
  map: {
    height: H(150),
    width: "100%",
    overflow: "hidden",
  },
  txtTabTitle: {
    fontWeight: "200",
    color: Color.GrayLight,
  },
});

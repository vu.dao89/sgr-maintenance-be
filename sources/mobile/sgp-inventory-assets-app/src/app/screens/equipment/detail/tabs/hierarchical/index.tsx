import React, { FC, useEffect, useState } from "react";

import { View } from "react-native";

// Internal
import { EHerarchyViewer } from "@Components";
import { EquipmentServices } from "@Services";
import { axiosHandler } from "@Services/httpClient";
import { get } from "lodash";
import { isArray } from "../../../../../../utils/Basic";
import styles from "./styles";

const HierarchicalDetail: FC<any> = ({ saInsets, detail }) => {
  const [viewingHierarchy, setViewingHierarchy]: any = useState(null);

  useEffect(() => {
    handLoadHierarchy();
  }, [detail]);

  const handLoadHierarchy = async () => {
    const { equipmentId, description } = detail;

    if (equipmentId) {
      const { response } = await axiosHandler(() => EquipmentServices.getEquipmentHierarchy(equipmentId));
      const data = get(response, "data", []);

      setViewingHierarchy({
        datas: isArray(data, 1) ? data : [{ element: { id: equipmentId, description: description } }],
        activeId: equipmentId,
      });
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom }]}>
      <EHerarchyViewer {...viewingHierarchy} />
    </View>
  );
};

export default HierarchicalDetail;

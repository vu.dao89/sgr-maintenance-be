import { StyleSheet } from "react-native";

import { widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    marginTop: WR(16),
  },
});

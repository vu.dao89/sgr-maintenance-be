import { default as TabAttachment } from "./attachment";
import { default as TabAttibute } from "./attribute";
import { default as TabGeneral } from "./general";
import { default as TabHierarchical } from "./hierarchical";
import { default as TabMeasuring } from "./measuring";

export default [
  {
    key: "0",
    padding: 24,
    title: "Tổng quan",
    Scene: TabGeneral,
  },
  {
    key: "1",
    padding: 24,
    title: "Điểm do",
    Scene: TabMeasuring,
  },
  {
    key: "2",
    padding: 24,
    title: "Đặc tính",
    Scene: TabAttibute,
  },
  {
    key: "3",
    padding: 24,
    title: "Phân cấp",
    Scene: TabHierarchical,
  },
  {
    key: "4",
    padding: 24,
    title: "Tài liệu",
    Scene: TabAttachment,
  },
];

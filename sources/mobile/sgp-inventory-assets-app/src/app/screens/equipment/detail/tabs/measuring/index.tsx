import React, { FC, useEffect, useRef, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";
import Toast, { useToast } from "react-native-toast-notifications";

import i18n from "@I18n";

import styles from "./styles";

import { Permission, widthResponsive as WR } from "@Constants";

import {
  CCollapsibleSection,
  CInfoViewer,
  CModalViewer,
  CRadioSelect,
  CSpinkit,
  CTAButtons,
  CTextInput,
  TextCM,
} from "@Components";
import { EquipmentServices, PermissionServices } from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { MeasPointUpdateParams } from "@Services/parameters";

import { log } from "libs/react-native-x-framework/js/logger";
import { get } from "lodash";
import moment from "moment";
import { useSelector } from "react-redux";
import { genShortDescription, isArray } from "src/utils/Basic";
import { AppState } from "../../../../../../redux/reducers";
import { useIsMounted } from "../../../../../../utils/Core";
import { formatSapDateTime, SAP_DATE_FORMAT, SAP_TIME_FORMAT } from "../../../../../../utils/DateTime";

const selections = [
  {
    value: false,
    label: "Hiện tại",
  },
  {
    value: true,
    label: "Khác biệt",
  },
];

const MeasuringDetail: FC<any> = ({ saInsets, detail, employeeCode, onChange }) => {
  const toastRef: any = useRef();
  const toast: any = useToast();
  const isMounted = useIsMounted();

  const [isCRUD, setShowCRUD]: any = useState(false);
  const [optionSelected, setOptionSelected] = useState("current");
  const [crudItem, setCrudItem]: any = useState({});
  const [measuringItem, setMeasuringItem]: any = useState({});
  const [error, setError]: any = useState(false);

  const [measuringPoints, setMeasuringPoints] = useState([] as any);
  const [fetching, setFetching]: any = useState(false);

  const { name: equipmentName } = detail;

  const reducer: any = useSelector<AppState | null>(s => s?.common);

  useEffect(() => {
    getMeasuringPoint();
  }, [detail]);

  const handleSelectSelection = () => {
    setOptionSelected(optionSelected === "current" ? "diff" : "current");
  };

  const getMeasuringPoint = async () => {
    const _measuringPoints = get(detail, "measuringPoints");
    const measuringPointIds = _measuringPoints.map((p: any) => p.point);

    if (isArray(measuringPointIds, 1)) {
      const { response } = await axiosHandler(() => EquipmentServices.getMeasPoints(measuringPointIds));

      if (!isMounted()) return;

      const data = get(response, "data.msp.item", []);

      const points = data.map((m: any) => {
        const _point = _measuringPoints.find((p: any) => p.point === m.measPoint);
        const pointValue = m.countReadId === "X" ? m.countRead : m.measRead;
        return {
          ..._point, // keep data from detail
          ...m, // keep data from api
          lastMeasuringPoint: {
            value: pointValue,
            label: `${pointValue} ${_point?.rangeUnit}`,
          },
          lastUpdate: m.measDocDate && m.measDocTime && formatSapDateTime(m.measDocDate, m.measDocTime),
        };
      });

      setMeasuringPoints(points);
    }
  };

  const postMeasuringPoint = async () => {
    if (!crudItem.value) {
      toastRef.current.show("Vui lòng nhập giá trị đo", { type: "warning" });
      return;
    }

    setFetching(true);

    const params: MeasPointUpdateParams = {
      measPoint: measuringItem.point,
      measDocDate: moment(new Date()).format(SAP_DATE_FORMAT),
      measDocTime: moment(new Date()).format(SAP_TIME_FORMAT),
      measDocText: genShortDescription(crudItem.desc),
      measDocLText: crudItem.desc || "",
      readBy: employeeCode || "",
      measRead: "",
      countRead: "",
      diff: "",
      workOrder: "",
      operation: "",
      countReadId: "",
      isMeasDiff: measuringItem.counter === "X" && crudItem.isDiff,
    };

    if (measuringItem.countReadId === "X") {
      params.diff = crudItem.isDiff ? crudItem.value : "";
      params.countRead = !crudItem.isDiff ? crudItem.value : "";
    } else {
      params.measRead = crudItem.value;
    }

    const { response, error } = await axiosHandler(() => EquipmentServices.postMeasPoint(params));

    log("===", response?.data);

    if (!isMounted()) return;
    setFetching(false);

    if (error) {
      toastRef.current.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      setShowCRUD(false);
      toast.show("Cập nhật số đo thành công", { type: "success" });
      getMeasuringPoint();
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom }]}>
      <View>
        {isArray(measuringPoints, 1) ? (
          <>
            {measuringPoints.map((m: any, index = 0) => {
              return (
                <CCollapsibleSection
                  key={`${m.id}_${index}`}
                  title={`Điểm đo: ${m.description}`}
                  infos={[
                    {
                      label: "Đặc tính",
                      value: m.description,
                    },
                    {
                      isGrouped: true,
                      label: "Giá trị nhỏ nhất",
                      value: m.lowerRangeLimit,
                    },
                    {
                      isGrouped: true,
                      label: "Giá trị tối đa",
                      value: m.upperRangeLimit,
                    },
                    {
                      label: "Giá trị đo gần nhất",
                      value: m.lastMeasuringPoint.label,
                    },
                  ]}
                  children={
                    PermissionServices.checkPermission(reducer, Permission.EQUIPMENT.MEAS_POST) && (
                      <>
                        <TouchableOpacity
                          style={[styles.taskStartBtn, { marginBottom: WR(16) }]}
                          onPress={() => {
                            setMeasuringItem(m);
                            setCrudItem({
                              isDiff: false,
                            });
                            setShowCRUD(true);
                          }}>
                          <TextCM style={styles.taskStartBtnTxt}>Lấy số đọc</TextCM>
                        </TouchableOpacity>
                        <TextCM style={styles.txtNotGetMeasuring}>
                          {m.lastUpdate ? `Đã lấy số đọc lần cuối vào ${m.lastUpdate}` : "Chưa lấy đo lần nào"}
                        </TextCM>
                      </>
                    )
                  }
                />
              );
            })}
          </>
        ) : null}
      </View>

      <CModalViewer
        open={isCRUD}
        placement={"bottom"}
        title={`Lấy số đọc cho ${measuringItem.description}`}
        onBackdropPress={() => setShowCRUD(false)}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />
        <View style={{ paddingBottom: saInsets.bottom || WR(12) }}>
          <ScrollView contentContainerStyle={styles.crudModalContent}>
            <CInfoViewer
              infos={[
                {
                  label: i18n.t("CM.equipment"),
                  value: equipmentName,
                },
              ]}
            />
            <CInfoViewer
              infos={[
                {
                  label: "Giá trị đo thấp nhất",
                  value: measuringItem.lowerRangeLimit,
                  isGrouped: true,
                },
                {
                  label: "Giá trị đo cao nhất",
                  value: measuringItem.upperRangeLimit,
                  isGrouped: true,
                },
              ]}
            />
            {measuringItem.countReadId === "X" && (
              <>
                <TextCM style={styles.infoRowHeaderTxt}>Chọn giá trị đo theo</TextCM>
                <CRadioSelect
                  options={selections}
                  selected={crudItem.isDiff}
                  onSelect={() => {
                    setCrudItem({ ...crudItem, isDiff: !crudItem.isDiff });
                  }}
                  styles={{
                    wrapper: styles.radioBtnCtn,
                    item: styles.radioChild,
                  }}
                />
              </>
            )}
            <CTextInput
              isRequired
              multiline={1}
              plh={i18n.t("DEQ.MeasuringPointTabs.EnterValuePoint")}
              keyboardType="numeric"
              value={crudItem.value}
              onChange={(text: any) => setCrudItem({ ...crudItem, value: text })}
            />
            <CInfoViewer
              infos={[
                {
                  label: i18n.t("DEQ.MeasuringPointTabs.RangeUnit"),
                  value: measuringItem.rangeUnit,
                },
              ]}
            />
            <CTextInput
              label={"Ghi chú"}
              multiline={1}
              plh={"Nhập ghi chú"}
              value={crudItem.desc}
              onChange={(text: any) => setCrudItem({ ...crudItem, desc: text })}
            />
          </ScrollView>
          <CTAButtons
            buttons={[
              {
                isSub: true,
                text: i18n.t("DEQ.MeasuringPointTabs.ButtonReset"),
                onPress: () => setShowCRUD(false),
              },
              {
                text: "Tạo",
                onPress: () => {
                  postMeasuringPoint();
                },
              },
            ]}
          />
          {fetching && (
            <View style={styles.selectionFetching}>
              <CSpinkit />
            </View>
          )}
        </View>
      </CModalViewer>
    </View>
  );
};

export default MeasuringDetail;

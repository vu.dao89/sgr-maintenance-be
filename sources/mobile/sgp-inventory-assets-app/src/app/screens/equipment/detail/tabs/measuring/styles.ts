import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as H, widthResponsive as W } from "@Constants";

export default StyleSheet.create({
  container: {
    padding: W(24),
  },
  ctnMap: {
    width: "100%",
    borderRadius: H(16),
    overflow: "hidden",
    marginBottom: H(16),
    marginTop: H(10),
  },
  selectionFetching: {
    width: "100%",
    height: "100%",
    position: "absolute",
    paddingVertical: W(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    height: H(150),
    width: "100%",
    overflow: "hidden",
  },
  txtTabTitle: {
    fontWeight: "200",
    color: Color.GrayLight,
  },
  taskStartBtn: {
    height: W(36),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: W(10),
    backgroundColor: "#4C8BFF",
  },
  taskStartBtnTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  crudModalContent: {
    paddingHorizontal: W(24),
    paddingBottom: W(4),
  },
  txtNotGetMeasuring: {
    marginTop: W(-5),
    marginBottom: W(10),
    color: "#8D9298",
    alignSelf: "center",
    fontSize: FontSize.FontTiny,
  },
  radioBtnCtn: {
    paddingBottom: H(10),
    marginLeft: 0,
    justifyContent: "space-between",
  },
  radioChild: {
    marginLeft: 0,
    paddingHorizontal: W(8),
    justifyContent: "space-between",
    width: "48%",
  },
  infoRowHeaderTxt: {
    marginBottom: H(8),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
});

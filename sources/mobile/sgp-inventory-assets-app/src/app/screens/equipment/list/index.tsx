import { useNavigation } from "@react-navigation/native";
import { get, isNull } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { FlatList, ScrollView, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";

import i18n from "@I18n";

import { MasterDataServices, PermissionServices } from "@Services";
import EquipmentServices from "@Services/equipment.services";
import { axiosHandler, getPagination } from "@Services/httpClient";
import { BaseParams, EquipmentSearchParams, FunctionalLocationParams } from "@Services/parameters";

import { AppState } from "@Reducers";

import { isArray } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import { convertFilterMultiValue, formatObjectLabel, formatWorkCenterLabel } from "src/utils/Format";
import { changeScreen } from "src/utils/Screen";

import { Permission, ScreenName, widthResponsive as WR } from "@Constants";

import styles from "./styles";

import {
  CAlertModal,
  CEmptyState,
  CFilterModal,
  CModalViewer,
  CNavbar,
  CRefreshControl,
  CScanQRCodeModal,
  CSpinkit,
  CTAButtons,
  EHerarchyViewer,
  EListItem,
  SearchInput,
} from "@Components";

import { ICGhost, ICQRC } from "@Assets/image";
import { CommonActions } from "../../../../redux/actions";

const FIRST_PAGE_NUMBER = 0;

const EquipmentScreen: FC<any> = props => {
  const nav = useNavigation();
  const isMounted = useIsMounted();
  const saInsets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);

  const [isFetching, setFetching] = useStateLazy(true);
  const [pagination, setPagination] = useStateLazy(getPagination());
  const [equipments, setEquipments] = useStateLazy([]);
  const [keyword, setKeyword]: any = useState("");

  const [isShowQRScan, setShowQRScan] = useStateLazy(false);
  const [isScanErrorAlert, setScanErrorAlert] = useStateLazy(false);

  const fetchingHierarchy: any = useRef(null);
  const [isFetchingHierarchy, setFetchingHierarchy]: any = useState(null);
  const [viewingHierarchy, setViewingHierarchy]: any = useState(null);

  const [filterSelecteds, setFilterSelecteds]: any = useState({});
  const filterOptions = [
    {
      id: "workCenters",
      label: "Theo tổ đội",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getWorkCenters(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              id: item.id,
              value: item.code,
              label: formatWorkCenterLabel(item.code, item.maintenancePlantId, item.description),
            };
          })
        );
      },
    },
    {
      id: "functionalLocations",
      label: "Theo khu vực chức năng",
      onSearch: async (params: any, cb: any) => {
        const { workCenters, plannerGroups } = filterSelecteds;

        const param: FunctionalLocationParams = {
          maintenancePlantCodes: userPlants,
          ...params,
          functionalLocationCodes: [],
          workCenterCodes: convertFilterMultiValue(workCenters),
          plannerGroups: convertFilterMultiValue(plannerGroups),
          objectTypeCodes: [],
          categories: [],
        };
        const { response } = await axiosHandler(() => MasterDataServices.getFunctionalLocations(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              id: item.id,
              value: item.functionalLocationId,
              label: formatObjectLabel(item.functionalLocationId, item.description),
            };
          })
        );
      },
    },
    {
      id: "equipmentCategories",
      label: "Theo phân loại thiết bị",
      onSearch: async (params: any, cb: any) => {
        const { response } = await axiosHandler(() => MasterDataServices.getEquipmentCategories(0));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { id: item.id, value: item.code, label: formatObjectLabel(item.code, item.description) };
          })
        );
      },
    },
    {
      id: "objectTypes",
      label: "Theo nhóm thiết bị",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getObjectTypes(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { id: item.id, value: item.code, label: formatObjectLabel(item.code, item.description) };
          })
        );
      },
    },
    {
      id: "plannerGroups",
      label: "Theo bộ phận kế hoạch",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPlannerGroups(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              id: item.id,
              value: item.plannerGroupId,
              label: formatObjectLabel(item.plannerGroupId, item.name),
            };
          })
        );
      },
    },
  ];

  const havePermission = PermissionServices.checkPermission(reducer, Permission.EQUIPMENT.SEARCH);

  useEffect(() => {
    if (havePermission) {
      handleGetList();
    } else {
      setFetching(false);
    }
  }, [filterSelecteds, keyword]);

  const handleScanResult = async (result: any) => {
    setShowQRScan(false);

    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => MasterDataServices.getByQRCode(result), { delay: true });
    let equipment = get(response, "data.equipment");

    dispatch(CommonActions.openLoading.success({}));

    if (isNull(get(equipment, "equipmentId", null))) {
      setScanErrorAlert(true);
    } else {
      handleNavigateDetailScreen({ raw: equipment });
    }
  };

  const handLoadHierarchy = async (equipment: any) => {
    const { id, name } = equipment;
    const { response } = await axiosHandler(() => EquipmentServices.getEquipmentHierarchy(id));
    if (!isMounted() || !fetchingHierarchy.current || fetchingHierarchy.current.id !== id) return;
    const data = get(response, "data", []);
    fetchingHierarchy.current = null;
    setFetchingHierarchy(false);
    setViewingHierarchy({
      datas: isArray(data, 1) ? data : [{ element: { id: id, description: name } }],
      activeId: id,
    });
  };

  const handleGetList = async (page: any = FIRST_PAGE_NUMBER) => {
    if (page === FIRST_PAGE_NUMBER) {
      await setEquipments([]);
      await setPagination(getPagination());
    }

    await setFetching(true);

    const { workCenters, functionalLocations, equipmentCategories, objectTypes, plannerGroups } = filterSelecteds;

    let param: EquipmentSearchParams = {
      functionalLocationCodes: convertFilterMultiValue(functionalLocations),
      workCenterCodes: convertFilterMultiValue(workCenters),
      equipmentCategoryCodes: convertFilterMultiValue(equipmentCategories),
      objectTypeCodes: convertFilterMultiValue(objectTypes),
      plannerGroups: convertFilterMultiValue(plannerGroups),
      maintenancePlantCodes: userPlants,
      keyword: keyword,
      page: page,
    };

    const { response } = await axiosHandler(() => EquipmentServices.getEquipments(param));
    const data: any = get(response, "data.items", []);

    if (!isMounted()) return;

    let nextPageEquipments: any = [];
    if (isArray(data, 1)) {
      data.forEach((item: any) => {
        const {
          equipmentId,
          description,
          functionalLocationId,
          functionalLocation,
          equipmentTypeId,
          equipmentType,
          equipmentCategoryId,
          equipmentCategory,
          systemStatusId,
          systemStatus,
        } = item;
        nextPageEquipments.push({
          id: equipmentId,
          name: formatObjectLabel(equipmentId, description, true),
          functionalLocation: formatObjectLabel(functionalLocationId, get(functionalLocation, "description", ""), true),
          tags: [
            { color: "#3A73B7", label: get(equipmentType, "description", equipmentTypeId || "") },
            { color: "#3A73B7", label: get(equipmentCategory, "description", equipmentCategoryId) },
          ],
          status: get(systemStatus, "userStatus", systemStatusId || ""),
          raw: item,
        });
      });
    }

    setPagination(getPagination(get(response, "data.pagination", {})));
    setEquipments((curPageEquipments: any) => [...curPageEquipments, ...nextPageEquipments]);
    setFetching(false);
  };

  const handleOnReachedEnd = () => {
    if (!isFetching && pagination.hasNext) {
      handleGetList(pagination.nextPage);
    }
  };

  const handleNavigateDetailScreen = (equipment: any) => {
    const item = get(equipment, "raw", null);
    if (isNull(item)) return;
    nav.navigate(
      ScreenName.EquipmentDetail as never,
      {
        params: item,
      } as never
    );
  };

  const _renderHeader = () => {
    const title = pagination.totalItems > 0 ? `${i18n.t("EQ.Title")} (${pagination.totalItems})` : i18n.t("EQ.Title");

    return (
      <CNavbar
        title={title}
        onLeftPress={() => changeScreen(props)}
        rightBtnNode={
          havePermission && (
            <CFilterModal
              showToggle
              title="Bộ lọc thiết bị"
              options={filterOptions}
              selecteds={filterSelecteds}
              onChange={setFilterSelecteds}
            />
          )
        }
      />
    );
  };

  const _renderSearch = () => {
    return (
      havePermission && (
        <View style={styles.searchBar}>
          <SearchInput
            placeholder={"Nhập mã hoặc tiêu đề"}
            wrapperStyle={styles.searchBarInput}
            onTextSearch={(_keyword: string) => {
              setKeyword(_keyword);
            }}
          />
          <TouchableOpacity style={styles.searchBarBtn} onPress={() => setShowQRScan(true)}>
            <ICQRC />
          </TouchableOpacity>
        </View>
      )
    );
  };

  const _renderBody = () => {
    let haveData = isArray(equipments, 1);
    if (!isFetching && !haveData) {
      return <CEmptyState noPermission={!havePermission} style={{ flex: 1, marginTop: 0 }} />;
    }
    return (
      <FlatList
        data={equipments}
        keyExtractor={(item, index) => item.id + "_" + index}
        contentContainerStyle={{ paddingBottom: saInsets.bottom }}
        refreshControl={<CRefreshControl onRefresh={handleGetList} />}
        onEndReachedThreshold={0.3}
        onEndReached={handleOnReachedEnd}
        ListFooterComponent={
          isFetching && (
            <View style={styles.dataFetching}>
              <CSpinkit />
            </View>
          )
        }
        renderItem={({ item, index }) => (
          <EListItem
            {...{ item, index }}
            onViewDetail={() => handleNavigateDetailScreen(item)}
            //onViewHierarchy={() => setViewingHierarchy({ datas: samepleHerarchy, active: activeHerarchy })}
            onViewHierarchy={() => {
              fetchingHierarchy.current = item;
              setFetchingHierarchy(true);
              handLoadHierarchy(item);
            }}
          />
        )}
      />
    );
  };

  const _renderHierarchyModal = () => {
    return (
      <CModalViewer
        open={!!(isFetchingHierarchy || viewingHierarchy)}
        title={"Sơ đồ phân cấp"}
        placement={"bottom"}
        onBackdropPress={() => setViewingHierarchy(null)}>
        <ScrollView contentContainerStyle={{ paddingBottom: (saInsets.bottom || WR(12)) + WR(44 + 24 + 16) }}>
          {isFetchingHierarchy ? (
            <View>
              <CSpinkit />
            </View>
          ) : (
            <EHerarchyViewer {...viewingHierarchy} />
          )}
        </ScrollView>
        <CTAButtons
          isSticky
          buttons={[
            {
              text: "Đóng",
              onPress: () => setViewingHierarchy(null),
            },
          ]}
        />
      </CModalViewer>
    );
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isShowQRScan}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setShowQRScan(false)}
          onResult={handleScanResult}
        />
        <CAlertModal
          open={isScanErrorAlert}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => {
            setScanErrorAlert(false);
          }}
          onClose={() => {
            setScanErrorAlert(false);
          }}
          buttons={[
            {
              id: "confirm",
              text: "Kết thúc",
            },
          ]}
        />
      </Fragment>
    );
  };

  return (
    <Fragment>
      <View style={styles.screen}>
        {_renderHeader()}
        {_renderSearch()}
        {_renderBody()}
        {_renderHierarchyModal()}
        {_renderScanQRCode()}
      </View>
    </Fragment>
  );
};

export default EquipmentScreen;

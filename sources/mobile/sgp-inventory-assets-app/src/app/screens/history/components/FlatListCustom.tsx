// external
import { ICAssetsEmpty } from "@Assets/image";
import { TextCM } from "@Components";
import { t } from "i18next";
import React, { FC, forwardRef, useImperativeHandle, useState } from "react";
// internal
import { Color, heightResponsive as H } from "@Constants";
import { ActivityIndicator, FlatList, ListRenderItem, RefreshControl, View } from "react-native";
import styles from "./styles";

type Props = {
  renderItem: ListRenderItem<any>;
  data: any[];
  isLoading: boolean;
  totalPage: number;
  loadingList: boolean;
  getData: (page: number, onFinishLoadmore: () => void) => void;
  ref: any;
};
const FlatListCustomComponent: FC<Props> = forwardRef((props: Props, ref) => {
  const { renderItem, data, totalPage = 0, getData, isLoading, loadingList } = props;

  const [pageIndex, setPageIndex] = useState(1);
  const [hasScrolled, setHasScrolled] = useState(false);
  const [isLoadingLoadMore, setLoadingLoadMore] = useState(false);

  useImperativeHandle(ref, () => ({
    resetPageIndex: () => {
      setPageIndex(1);
    },
  }));
  const onFinishLoadmore = () => {
    setLoadingLoadMore(false);
    setHasScrolled(false);
  };
  const handleLoadmore = (info: any) => {
    if (!hasScrolled || isLoadingLoadMore || pageIndex === totalPage) {
      return null;
    }
    setLoadingLoadMore(true);
    getData(pageIndex + 1, onFinishLoadmore);
    setPageIndex(pre => pre + 1);
  };

  const onScroll = () => {
    setHasScrolled(true);
  };

  const onRefresh = () => {
    setPageIndex(1);
    getData(1, onFinishLoadmore);
  };

  const ItemSeparator = () => {
    return <View style={{ height: H(16) }} />;
  };

  const EmptyComponent = () => (
    <View style={styles.ctnNotFound}>
      <ICAssetsEmpty />
      <TextCM style={styles.txtNotFound}>{`${t("HS.HistoryNotFound")}`}</TextCM>
    </View>
  );

  const FooterComponent = (
    <>
      {isLoadingLoadMore && (
        <ActivityIndicator animating={isLoadingLoadMore} color={Color.Yellow} size="large" style={styles.mt16} />
      )}
      <View style={styles.ctnFooter} />
    </>
  );
  const RefreshControlComponent = (
    <RefreshControl
      refreshing={isLoading && !isLoadingLoadMore}
      onRefresh={onRefresh}
      colors={[Color.Yellow]}
      tintColor={Color.TextWhite}
    />
  );
  if (loadingList) {
    return <ActivityIndicator animating={true} color={Color.Yellow} size="large" style={[styles.mt16, { flex: 1 }]} />;
  }
  return (
    <FlatList
      onScroll={onScroll}
      ItemSeparatorComponent={ItemSeparator}
      ListEmptyComponent={EmptyComponent}
      onEndReached={handleLoadmore}
      style={{ marginVertical: 20 }}
      onEndReachedThreshold={0.2}
      renderItem={renderItem}
      initialNumToRender={80}
      removeClippedSubviews={true}
      maxToRenderPerBatch={20}
      data={data}
      keyExtractor={(_item, index) => index.toString()}
      ListFooterComponent={FooterComponent}
      refreshControl={RefreshControlComponent}
    />
  );
});
FlatListCustomComponent.displayName = "FlatListCustomComponent";
export default FlatListCustomComponent;

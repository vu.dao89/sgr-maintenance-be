// external
import { ICXCircle } from "@Assets/image";
import { TextCM } from "@Components";
import React, { FC, useState } from "react";

// internal
import { TouchableOpacity, FlatList, ListRenderItem, View } from "react-native";
import { heightResponsive as H } from "@Constants";
import { SimpleFilterTypes, OptionType, filterOptions, getDateObject } from "../type";
import styles from "./styles";
import { DateObjectType } from "src/app/components/FilterModal/data";

type Props = {
  // selectedItem: SimpleFilterTypes;
  isFilterDirty: boolean;
  filterString: string;
  resetFilter: () => void;
  getData: (dateObject: DateObjectType, page: number, isNoLoading: boolean) => void;
};
const SimpleFilter: FC<Props> = ({ isFilterDirty, filterString, resetFilter, getData }) => {
  const [selectedFilter, setSelectedFilter] = useState(SimpleFilterTypes.ALL);

  const onSelectFilter = (value: SimpleFilterTypes) => {
    if (value === selectedFilter) return;
    setSelectedFilter(value);
    const dateObject = getDateObject(value);

    getData(dateObject, 1, true);
  };
  const onResetFilter = () => {
    if (resetFilter && typeof resetFilter === "function") {
      resetFilter();
    }
    setSelectedFilter(SimpleFilterTypes.ALL);
  };

  const renderItem: ListRenderItem<OptionType> = ({ item }: { item: OptionType }) => {
    const isSelected = item.value === selectedFilter;
    return (
      <TouchableOpacity
        onPress={() => onSelectFilter(item.value)}
        style={isSelected ? styles.ctnSelectedFilter : styles.ctnItemFilter}>
        <TextCM style={isSelected ? styles.txtFilterSelected : styles.txtFilter}>{item.label}</TextCM>
      </TouchableOpacity>
    );
  };

  if (isFilterDirty) {
    return (
      <View style={styles.ctnAdvanced}>
        <View style={styles.labelAdvanced}>
          <TextCM style={styles.txtAdvanced}>{filterString}</TextCM>
          <TouchableOpacity onPress={onResetFilter} style={styles.btnFilterDirtyDes}>
            <ICXCircle width={H(16)} height={H(16)} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return (
    <View style={styles.ctnFilter}>
      <FlatList
        data={filterOptions}
        horizontal
        scrollEnabled={false}
        keyExtractor={(_item, index) => index.toString()}
        renderItem={renderItem}
      />
    </View>
  );
};
SimpleFilter.displayName = "SimpleFilter";
export default SimpleFilter;

// External
import { get } from "lodash";
import React, { FC, useEffect, useRef, useState } from "react";
import { ActivityIndicator, TouchableOpacity, View } from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import { IImageInfo } from "react-native-image-zoom-viewer/built/image-viewer.type";

// Internal
import { ICCaretLeft, ICCaretRight } from "@Assets/image";
import { Header, ImageCache, TextCM } from "@Components";
import { Color, heightResponsive as H, ScreenHeight, ScreenWidth } from "@Constants";
import { inventoryPictureI18n } from "./data";
import styles from "./styles";

const FooterComponent = ({
  currentIndex = 1,
  total = 1,
  location = "",
  style = {},
  setIndexActive = (index: number) => null,
}: any) => {
  const onPrevious = () => {
    currentIndex > 0 && setIndexActive(currentIndex - 1);
  };
  const onNext = () => {
    currentIndex < total && setIndexActive(currentIndex + 1);
  };
  return (
    <View style={[styles.ctnFooter, style]}>
      <TextCM style={[styles.txtFooter, styles.marginBottom16]}>{location}</TextCM>
      <View style={styles.ctnIndicator}>
        <TouchableOpacity activeOpacity={0.7} onPress={onPrevious} disabled={currentIndex === 0}>
          <ICCaretLeft width={H(16)} height={H(16)} />
        </TouchableOpacity>

        <TextCM style={styles.txtIndicator}>{`${currentIndex + 1}/${total}`}</TextCM>

        <TouchableOpacity activeOpacity={0.7} onPress={onNext} disabled={currentIndex === total - 1}>
          <ICCaretRight width={H(16)} height={H(16)} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const InventoryPicture: FC<any> = props => {
  const { data, index, onCloseFullView } = props as any;
  const location = get(data, "location", "");
  const [imageUrls, setImageUrls] = useState<[IImageInfo] | undefined>(undefined);
  const [indexActive, setIndexActive] = useState<number>(index || 0);
  const refClick = useRef<any>(null);
  const [isOneTap, setIsOneTap] = useState<boolean>(false);
  const [isDoubleTap, setIsDoubleTap] = useState<boolean>(false);

  useEffect(() => {
    const newImageUrls = data?.images?.map((id: string) => ({
      props: {
        id: id,
      },
      width: ScreenWidth,
      height: ScreenHeight,
    }));
    setImageUrls(newImageUrls);
    // Clean up
    return () => setImageUrls(undefined);
  }, [data]);

  const onChange = (nextIndex: number | undefined) => {
    setIndexActive(nextIndex || 0);
  };

  const onClick = () => {
    refClick.current = setTimeout(() => {
      setIsOneTap(!isOneTap);
    }, 200);
  };

  const onDoubleClick = (onCancel?: any) => {
    if (refClick?.current) {
      clearTimeout(refClick.current);
      refClick.current = null;
    }
    setIsDoubleTap(!isDoubleTap);
    onCancel && onCancel();
  };

  const onBack = () => {
    onCloseFullView && onCloseFullView();
  };

  return (
    <View style={styles.ctnInventoryPicture}>
      {imageUrls && imageUrls.length > 0 && (
        <ImageViewer
          style={styles.ctnPictures}
          index={indexActive}
          imageUrls={imageUrls}
          backgroundColor={Color.Background}
          onChange={onChange}
          renderFooter={currentIndex =>
            !isOneTap ? (
              <FooterComponent
                currentIndex={currentIndex}
                total={imageUrls?.length}
                location={location}
                style={[
                  {
                    backgroundColor: Color.Background,
                  },
                  isDoubleTap ? styles.doubleTap : {},
                ]}
                setIndexActive={setIndexActive}
              />
            ) : (
              <></>
            )
          }
          renderHeader={() =>
            !isOneTap ? (
              <Header
                title={inventoryPictureI18n.txtHeader}
                actionLeft={onBack}
                style={[
                  {
                    zIndex: 9999,
                    position: "absolute",
                    backgroundColor: Color.Background,
                  },
                  isDoubleTap ? styles.doubleTap : {},
                ]}
              />
            ) : (
              <></>
            )
          }
          renderIndicator={() => <></>}
          onClick={onClick}
          onLongPress={() => {}}
          onDoubleClick={onDoubleClick}
          saveToLocalByLongPress={false}
          renderImage={(props: any) => (
            <ImageCache
              style={{
                ...props.style,
                ...styles.sizeImage,
              }}
              resizeMode="contain"
              uri={props.id || ""}
              id={props.id || ""}
            />
          )}
          onMove={(position: any) => {
            if (position && position?.scale === 1) {
              setIsDoubleTap(false);
            }
          }}
        />
      )}
    </View>
  );
};

export default InventoryPicture;

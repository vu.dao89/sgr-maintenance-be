import { StyleSheet } from "react-native";

import {
  Color,
  Font,
  FontSize,
  heightResponsive as H,
  ScreenHeight,
  ScreenWidth,
  widthResponsive as W,
} from "@Constants";

export default StyleSheet.create({
  ctnInventoryPicture: {
    flex: 1,
    backgroundColor: Color.BackgroundSelect,
    height: ScreenHeight,
    position: "relative",
  },
  ctnPictures: {
    height: ScreenHeight - H(126) - H(52),
  },
  ctnFooter: {
    width: ScreenWidth,
    height: H(126),
    // backgroundColor: Color.BackGroundFooterViewImage,
    // opacity: 0.6,
    flexDirection: "column",
    paddingVertical: H(16),
  },
  txtFooter: {
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontSmaller,
    fontWeight: "400",
    color: Color.White,
    lineHeight: 20,
    fontStyle: "normal",
    textAlign: "center",
  },
  txtIndicator: {
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontMedium,
    fontWeight: "400",
    color: Color.White,
    lineHeight: 24,
    fontStyle: "normal",
    textAlign: "center",
    marginHorizontal: W(8),
  },
  marginBottom16: {
    marginBottom: H(16),
  },
  sizeImage: {
    width: "100%",
  },
  ctnIndicator: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  doubleTap: {
    opacity: 0.6,
    backgroundColor: Color.BackGroundFooterViewImage,
  },
});

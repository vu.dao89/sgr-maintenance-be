import { StyleSheet } from "react-native";
import { Color, FontSize, ScreenWidth, widthResponsive as W, heightResponsive as H } from "@Constants";

export default StyleSheet.create({
  ctnSelectedFilter: {
    height: H(32),
    backgroundColor: Color.FilterBackground,
    borderRadius: H(50),
    paddingHorizontal: W(16),
    justifyContent: "center",
    // alignSelf:
  },
  ctnItemFilter: {
    height: H(32),
    borderRadius: H(50),
    paddingHorizontal: W(16),
    justifyContent: "center",
  },
  txtFilter: {
    fontWeight: "400",
    color: Color.LightGray,
    lineHeight: H(20),
    alignSelf: "center",
  },
  txtFilterSelected: {
    fontWeight: "400",
    color: Color.White,
    lineHeight: H(20),
    alignSelf: "center",
  },
  ctnFilter: {
    // backgroundColor: 'blue',
    height: H(32),
    paddingHorizontal: W(16),
    // marginBottom: 20
  },
  labelAdvanced: {
    backgroundColor: Color.TableHeaderBackground,
    borderRadius: H(50),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: W(8),
  },
  btnFilterDirtyDes: {
    width: H(32),
    height: H(32),
    justifyContent: "center",
    alignItems: "center",
  },
  txtAdvanced: {
    color: Color.White,
  },
  ctnAdvanced: {
    flexDirection: "row",
    justifyContent: "center",
  },
  mt16: {
    marginTop: 16,
  },
  ctnFooter: {
    height: H(50),
  },
  ctnNotFound: {
    marginTop: H(130),
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  txtNotFound: {
    marginTop: H(12),
    textAlign: "center",
    color: Color.TextGray,
    alignSelf: "center",
    fontSize: FontSize.FontSmaller,
  },
});

// External
import { get, isEmpty } from "lodash";
import React, { FC, forwardRef, useCallback, useState } from "react";
import { ScrollView, View } from "react-native";
import Modal from "react-native-modal";
// Internal
import { BaseModal, TextCM } from "@Components";
import styles from "./styles";
import { FieldType, getLabel, isShowItem } from "./type";
import ImageView from "@Screens/inventory-asset/images-view";
import { useNavigation } from "@react-navigation/native";
import { ScreenName, StatusTypes } from "@Constants";
import { dataImageInventoryFake } from "@Screens/inventory-asset/data";
import { LocationUtils } from "@Utils";
import i18n from "@I18n";
import InventoryPicture from "../components/inventory-picture";

type IProps = {
  ref: any;
  data: any;
  openModal: any;
  closeModal: () => void;
};

const DetailInventory: FC<any> = forwardRef(({ data, openModal, closeModal }: IProps, ref) => {
  const { ...fieldsChange } = data;
  const { getFullDMS, isLatitude, isLongitude } = LocationUtils;
  const images = get(fieldsChange, "image", []) || [];
  const latitude = get(fieldsChange, "latitude", null);
  const longitude = get(fieldsChange, "longitude", null);

  const dataView = {
    images: images.map((image: any) => image.imageid),
    location: isLongitude(longitude) && isLatitude(latitude) ? getFullDMS(latitude, longitude) : "",
  };
  const [isShowFullImg, setShowFullImg] = useState(false);
  const [indexImg, setIndexImg] = useState(0);
  const onClickImg = (idx: number) => {
    setIndexImg(idx);
    setShowFullImg(true);
  };

  const onCloseImg = () => setShowFullImg(false);
  return (
    <BaseModal header={i18n.t("HS.DetailAssetInventory")} ref={ref}>
      <View style={styles.container}>
        <ScrollView style={[styles.wFull]}>
          {!isEmpty(fieldsChange) &&
            Object.keys(fieldsChange)
              .map((key: any) => ({ key, value: fieldsChange[key] }))
              .map((ele: any, idx: number) => {
                if (isShowItem(ele.key)) {
                  return ele.key === FieldType.Image ? (
                    <ImageWrapper
                      key={idx}
                      obj={ele}
                      parentObj={fieldsChange}
                      openModal={openModal}
                      onClickImg={onClickImg}
                      baseRef={ref}
                    />
                  ) : (
                    <FieldWrapper key={idx} obj={ele} />
                  );
                }
              })}
        </ScrollView>
      </View>
      <Modal
        hideModalContentWhileAnimating={true}
        useNativeDriver={true}
        isVisible={isShowFullImg}
        style={{ flex: 1, margin: 0 }}>
        <InventoryPicture data={dataView} total={images?.length || 1} index={indexImg} onCloseFullView={onCloseImg} />
      </Modal>
    </BaseModal>
  );
});

export const FieldWrapper = ({ obj }: any) => {
  const { key, value } = obj;

  const getValue = (): string => {
    if (key === FieldType.Quantity) {
      return String(value?.split(".")?.[0]) || "0";
    } else if (key === FieldType.Location) {
      return value;
    } else if (key === FieldType.Status) {
      return StatusTypes[value];
    }
    return value;
  };

  return (
    <View style={[styles.fieldWrapper, styles.wFull]}>
      <TextCM numberOfLines={1} ellipsizeMode="tail" style={styles.txtLabel as any}>
        {getLabel(key) || ""}
      </TextCM>
      <TextCM
        numberOfLines={key === FieldType.StatusDescription ? 10 : 1}
        ellipsizeMode="tail"
        style={styles.txtValue as any}>
        {getValue() || ""}
      </TextCM>
    </View>
  );
};

export const ImageWrapper = ({ obj, onClickImg }: any) => {
  const { key, value: images } = obj;
  const onNavigateInventoryPicture = useCallback((index: number) => {
    onClickImg(index);
  }, []);

  return (
    <View style={[styles.imageContainer, styles.wFull]}>
      <TextCM numberOfLines={1} ellipsizeMode="tail" style={styles.txtLabel as any}>
        {getLabel(key) || ""}
      </TextCM>
      <View style={styles.imageWrapper}>
        <ImageView data={images} onClick={onNavigateInventoryPicture} maxDisplay={6} widthImage={108} />
      </View>
    </View>
  );
};

DetailInventory.displayName = "DetailInventory";
export default DetailInventory;

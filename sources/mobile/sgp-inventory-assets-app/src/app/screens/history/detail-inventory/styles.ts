import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize, Font, ScreenWidth } from "@Constants";
export default StyleSheet.create({
  container: {
    // height: H(500),
    width: "100%",
    paddingTop: H(8),
    marginBottom: H(32),
  },
  txtTime: {
    marginBottom: H(8),
    marginTop: H(24),
    marginLeft: W(16),
    color: Color.White,
    fontWeight: "500",
    fontSize: FontSize.FontMedium,
    lineHeight: FontSize.FontBiggest,
  },
  wFull: {
    width: "100%",
  },
  mb32: {
    marginBottom: H(32),
  },
  fieldWrapper: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingHorizontal: W(16),
    paddingVertical: H(8),
  },
  txtLabel: {
    fontSize: FontSize.FontSmaller,
    fontStyle: "normal",
    fontFamily: Font.Roboto,
    fontWeight: "400",
    lineHeight: 20,
    color: Color.MedalDescription,
    marginRight: W(15),
    width: (ScreenWidth - W(47)) / 2,
  },
  txtValue: {
    fontSize: FontSize.FontSmaller,
    fontStyle: "normal",
    fontFamily: Font.Roboto,
    fontWeight: "400",
    lineHeight: 20,
    color: Color.White,
    width: (ScreenWidth - W(47)) / 2,
  },
  imageContainer: {
    alignItems: "flex-start",
    paddingHorizontal: W(16),
    paddingVertical: H(8),
  },
  imageWrapper: {
    // alignItems: 'flex-start',
    // paddingHorizontal: W(16),
    paddingVertical: H(8),
  },
});

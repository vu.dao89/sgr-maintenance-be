import i18n from "@I18n";

export enum FieldType {
  Quantity = "quantity",
  Status = "status",
  StatusDescription = "description",
  Location = "location",
  Image = "zImage",
}

export const FieldLabel = {
  [FieldType.Quantity]: `${i18n.t("CA.Amount")}`,
  [FieldType.Status]: `${i18n.t("CA.Status")}`,
  [FieldType.StatusDescription]: `${i18n.t("CA.StatusDescription")}`,
  [FieldType.Location]: `${i18n.t("CA.Location")}`,
  [FieldType.Image]: `${i18n.t("CA.ImageInventory")}`,
};

export const getLabel = (field: FieldType | string): string => {
  let label = "";
  switch (field) {
    case FieldType.Quantity:
      label = FieldLabel[FieldType.Quantity];
      break;
    case FieldType.Status:
      label = FieldLabel[FieldType.Status];
      break;
    case FieldType.StatusDescription:
      label = FieldLabel[FieldType.StatusDescription];
      break;
    case FieldType.Location:
      label = FieldLabel[FieldType.Location];
      break;
    case FieldType.Image:
      label = FieldLabel[FieldType.Image];
      break;
    default:
      break;
  }
  return label;
};

export const isShowItem = (item: string): boolean => Object.values(FieldType).includes(item as any);

// External
import { get, isEmpty } from "lodash";
import moment from "moment";
import React, { FC, forwardRef } from "react";
import { ScrollView, View } from "react-native";
import { useSelector } from "react-redux";

// Internal
import { BaseModal, TextCM } from "@Components";
import { AppState } from "@Reducers";
import { inventoryTypes } from "@Screens/create-asset-screen/modal/inventory-classify-filter";
import styles from "./styles";
import { FieldType, getLabel, isShowItem, mockData } from "./type";
import i18n from "@I18n";

type IProps = {
  ref: any;
  data: any;
};

const getDate = (_date: any) => moment(_date).format("DD/MM/YYYY");

const DetailUpdateInfo: FC<any> = forwardRef(({ data = mockData[1] }: IProps, ref) => {
  const { change_date, change_time, ...fieldsChange } = data;

  return (
    <BaseModal header={i18n.t("HS.DetailHistoryChange")} ref={ref}>
      <View style={styles.container}>
        <TextCM style={styles.txtTime}>
          {moment(`${change_date}T${change_time}`, "YYYYMMDDTHHmmss").format("HH:mm:ss - DD/MM/YYYY") || "-"}
        </TextCM>

        <ScrollView style={[styles.wFull, styles.mb32]}>
          {!isEmpty(fieldsChange) &&
            Object.keys(fieldsChange)
              .map((key: any) => ({ key, value: fieldsChange[key] }))
              .map((ele: any, idx: number) => {
                return isShowItem(ele.key) && ele.value !== null && <FieldWrapper key={idx} obj={ele} />;
              })}
        </ScrollView>
      </View>
    </BaseModal>
  );
});

export const FieldWrapper = ({ obj }: any) => {
  const { key, value } = obj;
  const { home, createAsset }: any = useSelector<AppState | null>(s => s);
  const unitOptions = get(createAsset, "unit.data", []);
  const assetGrOption = get(home, "filterData.groupData", []);

  const getValue = (): string => {
    if (value === "#") {
      return "";
    }
    if (key === FieldType.CommissionedDate) {
      return value ? getDate(value) : "";
    } else if (key === FieldType.InventoryClassify) {
      return inventoryTypes.find((ele: any) => ele.id === String(value))?.label || "";
    } else if (key === FieldType.Unit) {
      return unitOptions.find((ele: any) => ele.id === String(value))?.label || "";
    } else if (key === FieldType.AssetGroup) {
      return assetGrOption.find((ele: any) => ele.id === String(value))?.label || "";
    } else if (key === FieldType.Type) {
      return value ? i18n.t(`CA.${value}`) : "";
    }
    return value;
  };

  return (
    <View style={[styles.fieldWrapper, styles.wFull]}>
      <TextCM numberOfLines={1} ellipsizeMode="tail" style={styles.txtLabel as any}>
        {getLabel(key) || ""}
      </TextCM>
      <TextCM numberOfLines={1} ellipsizeMode="tail" style={styles.txtValue as any}>
        {getValue() || ""}
      </TextCM>
    </View>
  );
};

DetailUpdateInfo.displayName = "DetailUpdateInfo";
export default DetailUpdateInfo;

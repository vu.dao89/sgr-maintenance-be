import i18n from "@I18n";

export enum FieldType {
  QrCode = "qrcode",
  AssetName = "name",
  Asset = "asset",
  Equipment = "equipment",
  Serial = "serial",
  Unit = "unit",
  AssetGroup = "asset_group",
  CommissionedDate = "use_date",
  DepartmentUse = "costcenter",
  PersonUse = "person",
  InventoryClassify = "inventory_type",
  Room = "room",
  Model = "model",
  Manufacturer = "manufacture",
  Vendor = "vendor",
  Type = "type",
  Email = "user_email",
}

export const FieldLabel = {
  [FieldType.QrCode]: `${i18n.t("CA.QrCode")}`,
  [FieldType.AssetName]: `${i18n.t("CA.AssetName")}`,
  [FieldType.Equipment]: `${i18n.t("CA.Equipment")}`,
  [FieldType.Serial]: `${i18n.t("CA.Serial")}`,
  [FieldType.AssetGroup]: `${i18n.t("CA.AssetGroup")}`,
  [FieldType.CommissionedDate]: `${i18n.t("CA.CommissionedDate")}`,
  [FieldType.DepartmentUse]: `${i18n.t("CA.DepartmentUse")}`,
  [FieldType.PersonUse]: `${i18n.t("CA.PersonUse")}`,
  [FieldType.InventoryClassify]: `${i18n.t("CA.InventoryClassify")}`,
  [FieldType.Room]: `${i18n.t("CA.Room")}`,
  [FieldType.Model]: `${i18n.t("CA.Model")}`,
  [FieldType.Manufacturer]: `${i18n.t("CA.Manufacturer")}`,
  [FieldType.Vendor]: `${i18n.t("CA.Vendor")}`,
  [FieldType.Unit]: `${i18n.t("CA.Unit")}`,
  [FieldType.Type]: `${i18n.t("CA.Type")}`,
  [FieldType.Email]: `${i18n.t("CA.User_email")}`,
  [FieldType.Asset]: `${i18n.t("CA.Asset")}`,
};

export const getLabel = (field: FieldType | string): string => {
  let label = "";
  switch (field) {
    case FieldType.AssetName:
      label = FieldLabel[FieldType.AssetName];
      break;
    case FieldType.Serial:
      label = FieldLabel[FieldType.Serial];
      break;
    case FieldType.AssetGroup:
      label = FieldLabel[FieldType.AssetGroup];
      break;
    case FieldType.CommissionedDate:
      label = FieldLabel[FieldType.CommissionedDate];
      break;
    case FieldType.DepartmentUse:
      label = FieldLabel[FieldType.DepartmentUse];
      break;
    case FieldType.PersonUse:
      label = FieldLabel[FieldType.PersonUse];
      break;
    case FieldType.InventoryClassify:
      label = FieldLabel[FieldType.InventoryClassify];
      break;
    case FieldType.Room:
      label = FieldLabel[FieldType.Room];
      break;
    case FieldType.Model:
      label = FieldLabel[FieldType.Model];
      break;
    case FieldType.Manufacturer:
      label = FieldLabel[FieldType.Manufacturer];
      break;
    case FieldType.Vendor:
      label = FieldLabel[FieldType.Vendor];
      break;
    case FieldType.Unit:
      label = FieldLabel[FieldType.Unit];
      break;
    case FieldType.Type:
      label = FieldLabel[FieldType.Type];
      break;
    case FieldType.Email:
      label = FieldLabel[FieldType.Email];
      break;
    case FieldType.Equipment:
      label = FieldLabel[FieldType.Equipment];
      break;
    case FieldType.Asset:
      label = FieldLabel[FieldType.Asset];
      break;
    default:
      label = FieldLabel[FieldType.QrCode];
      break;
  }
  return label;
};

export const isShowItem = (item: string): boolean => Object.values(FieldType).includes(item as any);

export const mockData = [
  {
    qrcode: "10000031",
    serial: "SERIAL",
    model: "MODEL",
    latitude: "LATITUDE",
    longitude: "LONGITUDE",
    room: "ROOM",
    change_date: "20220828",
    change_time: "114049",
  },
  {
    qrcode: "10000031",
    name: "name",
    serial: "serial",
    quantity: "1.000 ",
    asset_group: "03",
    use_date: "20220827",
    costcenter: "HO00100000",
    inventory_type: "01",
    manufacture: "Manufacture",
    vendor: "Vendor",
    model: "Model",
    person: "00019511",
    latitude: "latitude",
    longitude: "longitude",
    user_email: "duypt@gmail.com",
    room: "room",
    type: "I",
    change_date: "20220827",
    change_time: "114049",
  },
];

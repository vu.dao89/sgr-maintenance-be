// External
import { t } from "i18next";
import { get } from "lodash";
import moment from "moment";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";

// Internal
import { FilterModal, Header, TextCM } from "@Components";
import { heightResponsive as H, StatusTypes, widthResponsive as W } from "@Constants";
import i18n from "@I18n";

// Images & Styles
import { ICFilterDefault, ICFilterDirty } from "@Assets/image";
import { useNavigation } from "@react-navigation/native";
import { AppState } from "@Reducers";

import { useDispatch, useSelector } from "react-redux";
import { DateObjectType, FilterTypes } from "src/app/components/FilterModal/data";
import FlatListCustom from "./components/FlatListCustom";
import SimpleFilter from "./components/SimpleFilter";
import DetailInventory from "./detail-inventory";
import DetailUpdateInfo from "./detail-update-info";
import styles from "./styles";
import { FieldChangeValue, itemNotCount, TabsTypes } from "./type";
import { HistoryActions } from "@Actions";
import { CacheUtil, LocationUtils } from "@Utils";
import http from "@Services/httpClient";
import { initDataListHistory } from "src/redux/reducers/history.reducers";

type Props = {
  navigation: any;
};

const initDate: DateObjectType = {
  from_date: "",
  to_date: "",
};

const HistoryScreen: FC<Props> = navigation => {
  const qrcode = get(navigation, "route.params.qrcode", "10002216");
  const isFirstLoad = useRef(true);
  const nav = useNavigation();
  const dispatch = useDispatch();
  const filterModalRef = useRef<any>();
  const detailInventoryModalRef = useRef<any>();
  const detailHistoryChangeModalRef = useRef<any>();
  const listInventoryRef = useRef<any>();
  const listHistoryRef = useRef<any>();
  const [tabIndex, setTabIndex] = useState<TabsTypes>(TabsTypes.ASSET_INVENTORY);
  const [dateQuery, setDateQuery] = useState<DateObjectType>(initDate);
  const [isFilterDirty, setIsFilterDirty] = useState(false);
  const [filterString, setFilterString] = useState("");
  const [detailData, setDetailData] = useState({});
  const [loadingList, setLoadingList] = useState(false);
  const HistoryData = useSelector((state: AppState) => state.history);

  const dataHistoryChange = get(HistoryData.historyMainData, "historyChange", initDataListHistory);
  const dataHistoryInventory = get(HistoryData.historyMainData, "historyInventory", initDataListHistory);

  const totalPageHistoryChange = get(dataHistoryChange, "totalPage", 1);
  const totalPageHistoryInventory = get(dataHistoryInventory, "totalPage", 1);

  const isLoadingHistoryChange = get(dataHistoryChange, "isLoading", false);
  const isLoadingHistoryInventory = get(dataHistoryInventory, "isLoading", false);

  const getTokenCache = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
  };

  const getData = (date: DateObjectType, page: number, isNoLoading?: boolean) => {
    setDateQuery(date);
    if (!isFirstLoad.current) setLoadingList(true);
    listInventoryRef.current && listInventoryRef.current.resetPageIndex();
    listHistoryRef.current && listHistoryRef.current.resetPageIndex();
    dispatch(
      HistoryActions.getFilterBothTabsAsyncAction.request({
        params: {
          qrcode: qrcode,
          from_date: date.from_date,
          to_date: date.to_date,
          page_index: page,
        },
        onSuccess: () => {
          isFirstLoad.current = false;
          setLoadingList(false);
        },
        onFailure: () => {
          setLoadingList(false);
          isFirstLoad.current = false;
        },
        isNoLoading,
      })
    );
  };
  const getDataHistoryChange = (page: number, onFinishLoadmore: () => void) => {
    dispatch(
      HistoryActions.getAssetHistoryChangeAsyncAction.request({
        params: {
          qrcode: qrcode,
          from_date: dateQuery.from_date,
          to_date: dateQuery.to_date,
          page_index: page,
        },
        onSuccess: onFinishLoadmore,
        onFailure: onFinishLoadmore,
      })
    );
  };
  const getDataInventory = (page: number, onFinishLoadmore: () => void) => {
    dispatch(
      HistoryActions.getAssetHistoryInventoryAsyncAction.request({
        params: {
          qrcode: qrcode,
          from_date: dateQuery.from_date,
          to_date: dateQuery.to_date,
          page_index: page,
        },
        onSuccess: onFinishLoadmore,
        onFailure: onFinishLoadmore,
      })
    );
  };

  useEffect(() => {
    getData(initDate, 1, false);
  }, [qrcode]);

  const onResetFilter = () => {
    if (filterModalRef.current) {
      filterModalRef.current.resetFilter();
      setIsFilterDirty(false);
      getData(initDate, 1, true);
    }
  };

  const handleAdvancedFilter = (dateObject: any, filterType: FilterTypes) => {
    setIsFilterDirty(false);
    switch (filterType) {
      case FilterTypes.YEAR:
        setIsFilterDirty(true);
        setFilterString(
          i18n.t("HS.FormatYear", {
            year: moment(dateObject.from_date).format("YYYY"),
          })
        );
        break;
      case FilterTypes.QUARTER:
        setIsFilterDirty(true);
        setFilterString(
          i18n.t("HS.FormatQuarter", {
            quarter: moment(dateObject.to_date).quarter(),
            year: moment(dateObject.from_date).format("YYYY"),
          })
        );
        break;
      case FilterTypes.MONTH:
        setIsFilterDirty(true);
        setFilterString(
          i18n.t("HS.FormatMonth", {
            month: moment(dateObject.to_date).month() + 1,
            year: moment(dateObject.from_date).format("YYYY"),
          })
        );
        break;
      default:
        break;
    }
    getData(dateObject, 1, true);
  };

  const RightComponent = () => {
    const handleOnPress = () => {
      filterModalRef.current && filterModalRef.current.showFilter();
    };
    return (
      <TouchableOpacity onPress={handleOnPress}>
        {isFilterDirty ? (
          <ICFilterDirty width={W(24)} height={W(24)} />
        ) : (
          <ICFilterDefault width={W(24)} height={W(24)} />
        )}
      </TouchableOpacity>
    );
  };

  const renderItemData = (label: string, value: string) => {
    return (
      <View style={styles.ctnRenderData}>
        <View style={styles.ctnDataCardItemLeft}>
          <TextCM style={styles.txtLabelCard}>{`${label}`}</TextCM>
        </View>
        <View style={styles.ctnDataCardItemRight}>
          <TextCM numberOfLines={1} style={[styles.txtValueCard]}>{`${value}`}</TextCM>
        </View>
      </View>
    );
  };
  const RenderHistoryAssetItem = ({ item }: any) => {
    const { getFullDMS, isLatitude, isLongitude } = LocationUtils;
    const amount = Number(item?.quantity).toString();
    const location =
      isLatitude(item?.latitude) && isLongitude(item?.longitude)
        ? getFullDMS(Number(item?.latitude), Number(item?.longitude))
        : "";
    const openDetail = () => {
      setDetailData({ ...item, location: location, zImage: item.image });
      detailInventoryModalRef.current.onOpenModal();
    };

    return (
      <TouchableOpacity style={styles.ctnCardItem} onPress={openDetail}>
        <TextCM numberOfLines={1} style={styles.txtTimeChange}>
          {moment(`${item?.date}T${item?.time}`, "YYYYMMDDTHHmmss").format("HH:mm:ss - DD/MM/YYYY") || "-"}
        </TextCM>
        <View>
          {renderItemData(t("HS.Amount"), amount || "-")}
          {renderItemData(t("HS.Localtion"), location || "-")}
          {renderItemData(t("HS.Status"), StatusTypes[item?.status] || "-")}
          {renderItemData(t("HS.CurrentPhoto"), item?.image?.length || "0")}
        </View>
      </TouchableOpacity>
    );
  };

  const RenderHistoryChangeItem = ({ item }: any) => {
    const listFieldChange = Object.keys(item).filter(key => {
      if (!itemNotCount.includes(key) && item[key] !== null) {
        return true;
      }
      return false;
    });

    const openDetail = () => {
      setDetailData(item);
      detailHistoryChangeModalRef.current.onOpenModal();
    };

    const renderList = (value: any, index: number) => (
      <View key={index} style={styles.flexRow}>
        <Text style={styles.dot}>{`\u2B24`}</Text>
        <TextCM style={styles.txtFieldChange}>{`${FieldChangeValue[value as keyof typeof FieldChangeValue]}`}</TextCM>
      </View>
    );

    return (
      <TouchableOpacity style={styles.ctnCardItem} onPress={openDetail}>
        <TextCM numberOfLines={1} style={styles.txtTimeChange}>
          {moment(`${item?.change_date}T${item?.change_time}`, "YYYYMMDDTHHmmss").format("HH:mm:ss - DD/MM/YYYY") ||
            "-"}
        </TextCM>
        {renderItemData(t("HS.ListChange"), "")}
        <View style={{ marginTop: 8 }}>
          {listFieldChange.length > 4 ? (
            <>
              {listFieldChange.slice(0, 3).map((value, index) => renderList(value, index))}
              <View style={styles.flexRow}>
                <Text style={styles.dot}>{`\u2B24`}</Text>
                <TextCM style={styles.txtFieldChange}>{`${t("HS.MoreChange", {
                  count: listFieldChange.length - 3,
                })}`}</TextCM>
              </View>
            </>
          ) : (
            listFieldChange.map((value, index) => renderList(value, index))
          )}
        </View>
      </TouchableOpacity>
    );
  };

  const RenderTabs = () => {
    const isAssetInventory = tabIndex === TabsTypes.ASSET_INVENTORY;
    const isHistoryChange = tabIndex === TabsTypes.HISTORY_CHANGE;
    const onPressTab = (tab: TabsTypes) => {
      setTabIndex(tab);
    };
    return (
      <View>
        <View style={styles.ctnTabs}>
          <TouchableOpacity
            onPress={() => onPressTab(TabsTypes.ASSET_INVENTORY)}
            style={isAssetInventory ? styles.ctnTabItemSelected : styles.ctnTabItem}>
            <TextCM style={isAssetInventory ? styles.txtTabSelected : styles.txtTab}>{`${t(
              "HS.Tabs.InventoryAsset"
            )}`}</TextCM>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onPressTab(TabsTypes.HISTORY_CHANGE)}
            style={isHistoryChange ? styles.ctnTabItemSelected : styles.ctnTabItem}>
            <TextCM style={isHistoryChange ? styles.txtTabSelected : styles.txtTab}>{`${t(
              "HS.Tabs.UpdateInformation"
            )}`}</TextCM>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <Fragment>
      <View style={styles.container}>
        <Header componentRight={<RightComponent />} title={i18n.t("HS.Title")} />
        {/* Simple filter */}
        <SimpleFilter
          isFilterDirty={isFilterDirty}
          filterString={filterString}
          resetFilter={onResetFilter}
          getData={getData}
        />
        {/* Tab table */}
        <RenderTabs />
        <>
          {tabIndex === TabsTypes.ASSET_INVENTORY ? (
            <FlatListCustom
              loadingList={loadingList}
              ref={listInventoryRef}
              isLoading={isLoadingHistoryInventory}
              getData={getDataInventory}
              data={dataHistoryInventory.data}
              renderItem={RenderHistoryAssetItem}
              totalPage={totalPageHistoryInventory}
            />
          ) : (
            <FlatListCustom
              loadingList={loadingList}
              ref={listHistoryRef}
              isLoading={isLoadingHistoryChange}
              getData={getDataHistoryChange}
              data={dataHistoryChange.data}
              renderItem={RenderHistoryChangeItem}
              totalPage={totalPageHistoryChange}
            />
          )}
        </>
        <FilterModal ref={filterModalRef} onSelectOption={handleAdvancedFilter} />
        {/* Modal detal */}
        <DetailInventory data={detailData} ref={detailInventoryModalRef} />
        <DetailUpdateInfo data={detailData} ref={detailHistoryChangeModalRef} />
      </View>
    </Fragment>
  );
};

export default HistoryScreen;

import i18n from "@I18n";
import moment from "moment";

export enum SimpleFilterTypes {
  ALL = 1,
  CURRENT_MONTH = 2,
  CURRENT_QUARTER = 3,
  CURRENT_YEAR = 4,
}
export enum TabsTypes {
  ASSET_INVENTORY = 0,
  HISTORY_CHANGE = 1,
}

export type OptionType = {
  value: number;
  label: string;
};
type DateObjectType = {
  from_date: string;
  to_date: string;
};

export const filterOptions: OptionType[] = [
  { value: SimpleFilterTypes.ALL, label: i18n.t("HS.Filter.txtAll") },
  {
    value: SimpleFilterTypes.CURRENT_MONTH,
    label: i18n.t("HS.Filter.txtCurrentMonth"),
  },
  {
    value: SimpleFilterTypes.CURRENT_QUARTER,
    label: i18n.t("HS.Filter.txtCurrentQuarter"),
  },
  {
    value: SimpleFilterTypes.CURRENT_YEAR,
    label: i18n.t("HS.Filter.txtCurrentYear"),
  },
];

export const getDateObject = (value: SimpleFilterTypes) => {
  switch (value) {
    case SimpleFilterTypes.CURRENT_MONTH:
      return {
        from_date: moment().startOf("month").format("YYYYMMDD"),
        to_date: moment().endOf("month").format("YYYYMMDD"),
      };
      break;
    case SimpleFilterTypes.CURRENT_QUARTER:
      return {
        from_date: moment().startOf("quarter").format("YYYYMMDD"),
        to_date: moment().endOf("quarter").format("YYYYMMDD"),
      };
      break;
    case SimpleFilterTypes.CURRENT_YEAR:
      return {
        from_date: moment().startOf("year").format("YYYYMMDD"),
        to_date: moment().endOf("year").format("YYYYMMDD"),
      };
      break;
    default:
      return {
        from_date: "",
        to_date: "",
      };
      break;
  }
};

export const sample = [
  {
    qrcode: "10000055",
    asset: "211020000012-0000",
    costcenter: "HO00100000",
    quantity: "1.000 ",
    status: "status",
    description: "description",
    latitude: "16.054407",
    longitude: "108.202164",
    date: "20220920",
    time: "085725",
    image: [
      {
        imageid: "img1",
      },
      {
        imageid: "img2",
      },
    ],
  },
];

export const sample2 = [
  {
    qrcode: "10000031",
    name: "name",
    serial: "serial",
    quantity: "1.000 ",
    asset_group: "03",
    use_date: "20220827",
    costcenter: "HO00100000",
    inventory_type: "01",
    manufacture: "Manufacture",
    vendor: "Vendor",
    model: "Model",
    person: "00019511",
    latitude: "latitude",
    longitude: "longitude",
    user_email: "duypt@gmail.com",
    room: "room",
    type: "I",
    change_date: "20220827",
    change_time: "114049",
  },
];
// Count in Main screen
export const itemNotCount = [
  "qrcode",
  "type",
  "change_date",
  "change_time",
  "asset",
  "user_email",
  "latitude",
  "longitude",
];
// Not send to Modal detail
export const itemNotListed = ["latitude", "longitude"];

export const FieldChangeValue = {
  qrcode: `${i18n.t("CA.AssetName")}`,
  name: `${i18n.t("CA.AssetName")}`,
  serial: `${i18n.t("CA.Serial")}`,
  quantity: `${i18n.t("CA.Amount")}`,
  asset_group: `${i18n.t("CA.AssetGroup")}`,
  use_date: `${i18n.t("CA.CommissionedDate")}`,
  costcenter: `${i18n.t("CA.DepartmentUse")}`,
  inventory_type: `${i18n.t("CA.InventoryClassify")}`,
  manufacture: `${i18n.t("CA.Manufacturer")}`,
  vendor: `${i18n.t("CA.Vendor")}`,
  model: `${i18n.t("CA.Model")}`,
  person: `${i18n.t("CA.PersonUse")}`,
  latitude: `${i18n.t("CA.AssetName")}`,
  longitude: `${i18n.t("CA.AssetName")}`,
  user_email: `${i18n.t("CA.AssetName")}`,
  room: `${i18n.t("CA.Room")}`,
  type: `${i18n.t("CA.AssetName")}`,
};

// External
import React, { FC, Fragment, forwardRef, useImperativeHandle, useRef, useState } from "react";
import { t } from "i18next";
import { TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

// Internal
import { TextCM, BaseModal } from "@Components";

// Images & Styles
import { ICArrowDown, ICArrowDownYellow } from "@Assets/image";
import styles from "./styles";
import UserFilter from "../UserFilter";
import GroupFilter from "../GroupFilter";
import OrganizationFilter from "../OrganizationFilter";

const AdvancedFilter: FC<any> = forwardRef((props: any, $ref) => {
  const { organizationValue, userValue, groupValue, setFilter } = props;
  const [organization, setOrganization] = useState<any>(organizationValue);
  const [user, setUser] = useState<any>(userValue);
  const [group, setGroup] = useState<any>(groupValue);
  const OrganizationFilterRef = useRef<any>(null);
  const insets = useSafeAreaInsets();
  const UserFilterRef = useRef<any>(null);
  const GroupFilterRef = useRef<any>(null);
  const $refBaseMadal = useRef<any>(null);
  useImperativeHandle($ref, () => ({
    ...$refBaseMadal.current,
    setOrganizationFromMain: (_organization: any) => {
      setOrganization(_organization);
    },
    setUserFromMain: (_user: any) => {
      setUser(_user);
    },
    setGroupFromMain: (_group: any) => {
      setGroup(_group);
    },
  }));

  const onPressAssetGroupUser = () => OrganizationFilterRef?.current?.onOpenModal();

  const onPressUser = () => {
    UserFilterRef?.current?.onOpenModal();
    UserFilterRef?.current?.scrollToIndex();
  };
  const onPressGroup = () => GroupFilterRef?.current?.onOpenModal();

  const onSelectOrganization = (item: any) => {
    setOrganization(item);
    OrganizationFilterRef?.current?.onCloseModal();
  };
  const onSelectUser = (id: string) => {
    setUser(id);
    UserFilterRef?.current?.onCloseModal();
  };

  const onSelectGroup = (item: any) => {
    setGroup(item);
    GroupFilterRef?.current?.onCloseModal();
  };
  const onFilter = () => {
    if (setFilter) {
      setFilter(organization, user, group);
      if ($refBaseMadal) $refBaseMadal?.current.onCloseModal();
    }
  };
  // reset data khi khong click "Xem ket qua"
  const onCloseAdvancedModal = () => {
    setOrganization(organizationValue);
    setUser(userValue);
    setGroup(groupValue);
  };
  return (
    <Fragment>
      <BaseModal header={t("IS.FilterSetting")} ref={$refBaseMadal} onClosePress={onCloseAdvancedModal}>
        <View
          style={[
            styles.container,
            {
              marginBottom: insets.bottom,
            },
          ]}>
          <TextCM style={styles.txtLabelWithoutMargin}>{`${t("IS.AssetGroupUser")}`}</TextCM>
          <TouchableOpacity style={styles.ctnSelectFilter} onPress={onPressAssetGroupUser}>
            <TextCM style={organization ? styles.txtSelectedFilter : styles.txtSelectFilter}>
              {`${organization ? organization?.label : t("IS.phrAssetGroupUser")}`}
            </TextCM>
            {organization ? <ICArrowDownYellow /> : <ICArrowDown />}
          </TouchableOpacity>
          <TextCM style={styles.txtLabel}>{`${t("IS.AssetUser")}`}</TextCM>
          <TouchableOpacity style={styles.ctnSelectFilter} onPress={onPressUser}>
            <TextCM style={user ? styles.txtSelectedFilter : styles.txtSelectFilter}>{`${
              user ? user?.name : t("IS.phrAssetUser")
            }`}</TextCM>
            {user ? <ICArrowDownYellow /> : <ICArrowDown />}
          </TouchableOpacity>
          <TextCM style={styles.txtLabel}>{`${t("IS.AssetGroup")}`}</TextCM>
          <TouchableOpacity style={styles.ctnSelectFilter} onPress={onPressGroup}>
            <TextCM style={group ? styles.txtSelectedFilter : styles.txtSelectFilter}>
              {`${group ? group?.label : t("IS.phrAssetGroup")}`}
            </TextCM>
            {group ? <ICArrowDownYellow /> : <ICArrowDown />}
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnViewResults} onPress={onFilter}>
            <TextCM>{`${t("IS.ViewResults")}`}</TextCM>
          </TouchableOpacity>
        </View>

        <OrganizationFilter ref={OrganizationFilterRef} onSelect={onSelectOrganization} value={organization?.id} />
        <UserFilter ref={UserFilterRef} value={user?.id} onSelect={onSelectUser} />
        <GroupFilter ref={GroupFilterRef} value={group?.id} onSelect={onSelectGroup} />
      </BaseModal>
    </Fragment>
  );
});

AdvancedFilter.displayName = "AdvancedFilter";
export default AdvancedFilter;

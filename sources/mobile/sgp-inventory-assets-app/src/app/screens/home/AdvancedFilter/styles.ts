import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  container: {
    paddingHorizontal: W(16),
    paddingTop: H(16),
  },
  txtLabel: {
    color: Color.LightGray,
    marginTop: H(16),
  },
  txtLabelWithoutMargin: {
    color: Color.LightGray,
  },
  txtSelectFilter: {
    fontSize: FontSize.FontMedium,
    color: Color.White,
  },
  txtSelectedFilter: {
    fontSize: FontSize.FontMedium,
    color: Color.Yellow,
  },
  ctnSelectFilter: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: Color.FilterBackground,
    paddingHorizontal: W(12),
    paddingVertical: H(12),
    borderRadius: 10,
    marginTop: H(8),
  },
  txtViewResults: {
    fontSize: FontSize.FontMedium,
    color: Color.TextButton,
    fontWeight: "500",
  },
  btnViewResults: {
    backgroundColor: Color.Yellow,
    paddingVertical: H(12),
    borderRadius: 10,
    justifyContent: "center",
    flexDirection: "row",
    marginTop: H(24),
    marginBottom: H(16),
  },
});

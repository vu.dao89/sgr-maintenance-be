// External
import React, { FC, forwardRef, useEffect, useState } from "react";
import { t } from "i18next";
import { useDispatch, useSelector } from "react-redux";
import { get } from "lodash";
import { TouchableOpacity } from "react-native";

// Internal
import { SelectModal, TextCM } from "@Components";
import { AppState } from "@Reducers";
import { HomeActions } from "@Actions";
import { ICCheck } from "@Assets/image";
import { heightResponsive as H } from "@Constants";
import styles from "./styles";
import { CacheUtil } from "@Utils";
import http from "@Services/httpClient";

type IProps = {
  ref: any;
  value?: string;
  onSelect?: any;
};

const GroupFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const { value, onSelect } = props;
  const dispatch = useDispatch();
  const HomeData = useSelector((state: AppState) => state.home);
  const groupData = get(HomeData, "filterData.groupData", []);
  const [listSearch, setListSearch] = useState(groupData);
  const [keyword, setKeyword] = useState("");

  const getTokenCache = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
  };

  useEffect(() => {
    getTokenCache();

    setTimeout(() => {
      dispatch(
        HomeActions.getListAssetGroupAsyncAction.request({
          params: {
            unit: "x",
            asset_group: "X",
          },
        })
      );
    }, 100);
  }, []);

  const onSearch = (_keyword: string) => {
    setKeyword(_keyword);
    const result = groupData.filter((item: any) => item?.asset_groupname?.includes(_keyword));
    setListSearch(result);
  };

  const onCloseModal = () => {
    setKeyword("");
    setListSearch(groupData);
  };

  const renderItem = ({ item }: any) => {
    const isSelected = value === item.id;
    return (
      <TouchableOpacity style={styles.ctnItem} onPress={() => onSelect(item)}>
        <TextCM numberOfLines={1} style={isSelected ? styles.txtItemNameSelected : styles.txtItemName}>
          {item.asset_groupname}
        </TextCM>
        {isSelected ? <ICCheck style={styles.mr8} width={H(24)} height={H(24)} /> : null}
      </TouchableOpacity>
    );
  };

  return (
    <>
      <SelectModal
        onClose={onCloseModal}
        ref={ref}
        renderItem={renderItem}
        title={t("IS.AssetGroup")}
        isSearch={false}
        onSelect={onSelect}
        value={value}
        data={keyword.length > 0 ? listSearch : groupData}
        onSearch={onSearch}
      />
    </>
  );
});

GroupFilter.displayName = "GroupFilter";
export default GroupFilter;

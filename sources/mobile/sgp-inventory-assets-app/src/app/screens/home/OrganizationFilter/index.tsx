// External
import React, { FC, forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { t } from "i18next";
import { TouchableOpacity } from "react-native";
import { get } from "lodash";

// Internal
import { SelectModal, TextCM } from "@Components";
import { HomeActions } from "@Actions";
import { AppState } from "@Reducers";
import { heightResponsive as H, widthResponsive as W } from "@Constants";
import styles from "./styles";
import { ICCheck } from "@Assets/image";
import { CacheUtil } from "@Utils";
import http from "@Services/httpClient";

type IProps = {
  ref: any;
  onSelect?: any;
  value?: any;
};

const OrganizationFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const { value, onSelect } = props;
  const modalRef = useRef<any>(null);
  const dispatch = useDispatch();
  const HomeData = useSelector((state: AppState) => state.home);
  const organizationData = get(HomeData, "filterData.organizationData", []);
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const { objDepartment } = get(reducer, "userInfo.data", {});
  const [listSearch, setListSearch] = useState(organizationData);

  const getTokenCache = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
  };

  useEffect(() => {
    getTokenCache();

    setTimeout(() => {
      dispatch(
        HomeActions.getListOrganizationAsyncAction.request({
          nameOrCode: "",
          page: 0,
        })
      );
    }, 100);
  }, []);

  const indexSelected = listSearch.findIndex((item: any) => item.id === value);

  useImperativeHandle(ref, () => ({
    ...modalRef.current,
    scrollToIndex: () => {
      if (indexSelected !== -1) {
        modalRef.current.scrollIndex(indexSelected);
      }
    },
  }));

  const onSearch = (_keyword: string) => {
    const result = organizationData.filter((item: any) => item?.name?.includes(_keyword));

    setListSearch(result);
  };

  const renderItem = ({ item }: any) => {
    const isSelected = value === item.id;
    return (
      <TouchableOpacity style={styles.ctnItem} onPress={() => onSelect(item)}>
        <TextCM numberOfLines={1} style={isSelected ? styles.txtItemNameSelected : styles.txtItemName}>
          {item.label}
        </TextCM>
        {isSelected ? <ICCheck style={{ marginRight: W(8) }} width={H(24)} height={H(24)} /> : null}
      </TouchableOpacity>
    );
  };

  const onCloseModal = () => {
    setListSearch(organizationData);
  };

  return (
    <>
      <SelectModal
        onClose={onCloseModal}
        renderItem={renderItem}
        ref={modalRef}
        title={t("IS.AssetGroupUser")}
        isSearch={false}
        onSelect={onSelect}
        value={value}
        data={objDepartment?.id ? [objDepartment] : []}
        onSearch={onSearch}
      />
    </>
  );
});

OrganizationFilter.displayName = "OrganizationFilter";
export default OrganizationFilter;

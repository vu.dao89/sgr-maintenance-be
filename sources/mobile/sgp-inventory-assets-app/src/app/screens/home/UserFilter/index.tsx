// External
import React, { FC, forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import { t } from "i18next";
// Internal
import { ImageCache, SelectModal, TextCM } from "@Components";
// import {HomeActions} from '@Actions';

import styles from "./styles";
import { TouchableOpacity, View } from "react-native";
import { ICCheck } from "@Assets/image";
import { heightResponsive } from "@Constants";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "@Reducers";
import { HomeActions } from "@Actions";
import { get } from "lodash";
import { CacheUtil } from "@Utils";
import http from "@Services/httpClient";

type IProps = {
  ref: any;
  value?: any;
  onSelect?: any;
};

const UserFilter: FC<any> = forwardRef((props: IProps, ref) => {
  const dispatch = useDispatch();
  const modalRef = useRef<any>(null);

  const HomeData = useSelector((state: AppState) => state.home);
  const userData = get(HomeData, "filterData.userData", []);
  const [keyword, setKeyword] = useState("");
  const [page, setPage] = useState(0);
  const { value, onSelect } = props;

  const callInitData = () => {
    setPage(0);
    dispatch(
      HomeActions.getListUserAsyncAction.request({
        params: {
          nameOrCode: "",
          page: 0,
        },
      })
    );
  };
  const getTokenCache = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
  };

  useEffect(() => {
    getTokenCache();
    setTimeout(() => {
      callInitData();
    }, 100);
  }, []);

  const onSearch = (keySearch: string) => {
    setKeyword(keySearch);
    setPage(0);
    dispatch(
      HomeActions.getListUserAsyncAction.request({
        params: {
          nameOrCode: keySearch,
          page: 0,
        },
      })
    );
  };

  const onSelectItem = (item: any) => {
    if (onSelect) {
      onSelect(item);
      callInitData();
    }
  };
  const indexSelected = userData.findIndex((item: any) => item.id === value);

  useImperativeHandle(ref, () => ({
    ...modalRef.current,
    scrollToIndex: () => {
      if (indexSelected !== -1 && indexSelected < userData?.length) {
        modalRef.current.scrollIndex(indexSelected);
      }
    },
  }));

  const onRefresh = () => {
    dispatch(
      HomeActions.getListUserAsyncAction.request({
        params: {
          nameOrCode: keyword,
          page: 0,
        },
      })
    );
    setPage(0);
  };
  const onLoadMore = (handleIsLoadMore: any) => {
    dispatch(
      HomeActions.getListUserAsyncAction.request({
        params: {
          page: page + 1,
          nameOrCode: keyword,
        },
        onSuccess: handleIsLoadMore,
        onFailure: handleIsLoadMore,
      })
    );
    setPage(page + 1);
  };

  const renderItem = ({ item }: any) => {
    const isSelected = item.id === value;

    return (
      <TouchableOpacity onPress={() => onSelectItem(item)}>
        <View style={styles.ctnItemPeople}>
          <View style={styles.ctnLeftPeople}>
            {item?.uri ? (
              <ImageCache uri={item?.uri} style={styles.imgAvatar} />
            ) : (
              <View style={styles.ctnAvatar}>
                <TextCM style={styles.txtNameAvatar} numberOfLines={1}>
                  {item?.name.charAt(0)}
                </TextCM>
              </View>
            )}

            <View style={styles.ctnItemNameInfo}>
              <TextCM style={isSelected ? styles.txtItemNameSelected : styles.txtItemName}>{item?.name}</TextCM>
              <TextCM style={isSelected ? styles.txtItemSecondSelected : styles.txtItemSecond}>
                {item?.department?.departmentName}
              </TextCM>
              <TextCM style={isSelected ? styles.txtItemSecondSelected : styles.txtItemSecond}>{item?.email}</TextCM>
            </View>
          </View>

          {isSelected ? <ICCheck width={heightResponsive(24)} height={heightResponsive(24)} /> : null}
        </View>
      </TouchableOpacity>
    );
  };
  const onCloseModal = () => {
    setKeyword("");
    callInitData();
  };

  return (
    <>
      <SelectModal
        onLoadMore={onLoadMore}
        onRefresh={onRefresh}
        onClose={onCloseModal}
        ref={modalRef}
        title={t("IS.AssetUser")}
        isSearch={true}
        onSelect={onSelect}
        value={value}
        data={userData}
        onSearch={onSearch}
        renderItem={renderItem}
      />
    </>
  );
});

UserFilter.displayName = "UserFilter";
export default UserFilter;

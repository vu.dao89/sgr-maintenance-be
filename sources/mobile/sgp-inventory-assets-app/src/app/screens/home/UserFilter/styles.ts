import { StyleSheet } from "react-native";
import { heightResponsive, widthResponsive, ScreenWidth, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  ctnItemPeople: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: heightResponsive(8),
    paddingHorizontal: widthResponsive(12),
    alignContent: "center",
  },
  ctnLeftPeople: {
    flexDirection: "row",
  },
  imgAvatar: {
    width: widthResponsive(40),
    height: heightResponsive(40),
    borderRadius: heightResponsive(100),
  },
  ctnAvatar: {
    width: heightResponsive(40),
    height: heightResponsive(40),
    borderRadius: heightResponsive(100),
    backgroundColor: Color.Divider,
    justifyContent: "center",
    alignSelf: "center",
  },

  txtNameAvatar: {
    color: Color.White,
    textAlign: "center",
    fontSize: FontSize.FontBig,
    fontWeight: "700",
  },
  ctnItemNameInfo: {
    marginLeft: widthResponsive(16),
  },
  txtItemName: {
    color: Color.White,
    width: widthResponsive(220),
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
  },
  txtItemNameSelected: {
    color: Color.Yellow,
    width: widthResponsive(220),
    fontWeight: "400",
    fontSize: FontSize.FontMedium,
  },
  txtItemSecond: {
    color: Color.TextGray,
    marginTop: heightResponsive(4),
    fontSize: FontSize.FontSmaller,
    fontWeight: "400",
  },
  txtItemSecondSelected: {
    color: Color.White,
    marginTop: heightResponsive(4),
    fontSize: FontSize.FontSmaller,
    fontWeight: "400",
  },
});

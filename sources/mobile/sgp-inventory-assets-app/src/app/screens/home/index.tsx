// External
import { t } from "i18next";
import { get } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  Alert,
  Animated,
  FlatList,
  Platform,
  Pressable,
  RefreshControl,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  View,
} from "react-native";
import { openSettings, PERMISSIONS, request, RESULTS } from "react-native-permissions";
import XFramework from "react-native-x-framework";

// Internal
import { Header, SearchInput, TextCM } from "@Components";
import { Color, heightResponsive as H, ScreenName, ScreenWidth, Types, widthResponsive as W } from "@Constants";

import AdvancedFilter from "./AdvancedFilter";
import GroupFilter from "./GroupFilter";
import OrganizationFilter from "./OrganizationFilter";
import UserFilter from "./UserFilter";
// Images & Styles
import { CommonActions, HomeActions } from "@Actions";
import {
  ICAddCircle,
  ICArrowDown,
  ICAssetsEmpty,
  ICBackSVG,
  ICFilterDefault,
  ICPlus,
  ICQR,
  ICSearchYellow24,
} from "@Assets/image";
import { useNavigation } from "@react-navigation/native";
import { AppState } from "@Reducers";
import { CacheUtil } from "@Utils";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";

import http from "@Services/httpClient";
import { changeScreen } from "../../../utils/Screen";
type Props = {
  navigation: any;
};

const HomeScreen: FC<Props> = props => {
  const nav = useNavigation();
  const dispatch = useDispatch();
  const searchRef = useRef<any>(null);
  const userFilterRef = useRef<any>(null);
  const isFirstLoad = useRef<boolean>(true);
  const advancedFilterRef = useRef<any>(null);
  const organizationFilterRef = useRef<any>(null);
  const groupFilterRef = useRef<any>(null);
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const { objDepartment } = get(reducer, "userInfo.data", {});
  const role = get(reducer, "userInfo.data.role", []);
  const [user, setUser] = useState<any>(null);
  const [group, setGroup] = useState<any>(null);
  const [organization, setOrganization] = useState<any>(objDepartment);
  const [keySearch, setKeySearch] = useState("");
  const [pageIndex, setPageIndex] = useState(1);
  const [isLoadingLoadMore, setLoadingLoadMore] = useState(false);
  const [hasScrolled, setHasScrolled] = useState(false);

  const HomeData = useSelector((state: AppState) => state.home);
  const isLoadingRefresh = useSelector((state: AppState) => state.home.isRefresh);
  // const isLoadingRefresh = get(HomeData, 'loading', false);
  const assetList = get(HomeData, "assetList", []);
  const totalPage = get(HomeData, "totalPage", 0);
  const [viewSearchBar, setViewSearchBar] = useState(false);
  const [inputAnimation] = useState(new Animated.Value(0));

  const getTokenCache = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
  };

  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor("transparent", true);
    StatusBar.setTranslucent(true);
  }

  useEffect(() => {
    getTokenCache();
    setTimeout(() => {
      dispatch(
        CommonActions.getUserInfoAsyncAction.request({
          onSuccess: (data: any) => {
            if (data?.objDepartment?.costcenter) {
              setOrganization({
                id: data?.objDepartment?.costcenter,
                label: data?.objDepartment?.costcenter_name,
              });
              advancedFilterRef?.current.setOrganizationFromMain({
                id: data?.objDepartment?.costcenter,
                label: data?.objDepartment?.costcenter_name,
              });
            }
          },
        })
      );
    }, 100);
  }, []);

  useEffect(() => {
    if (objDepartment?.id) {
      setTimeout(() => {
        setPageIndex(1);
        dispatch(
          HomeActions.getListAssetsAsyncAction.request({
            params: {
              all: "",
              asset: "",
              equipment: "",
              description: "",
              costcenter: objDepartment?.id || "",
              person: user?.id?.toString() || "",
              asset_group: group?.id.toString() || "",
              page: pageIndex || 1,
            },
          })
        );
      }, 500);
    } else {
      dispatch(HomeActions.getListAssetsAsyncAction.failure({}));
    }
  }, [user, group, objDepartment?.id]);

  const inputWidth = inputAnimation.interpolate({
    inputRange: [0, 1.5],
    outputRange: [20, -10],
  });

  const onNavigateCreateAsset = () => nav.navigate(ScreenName.CreateAsset as never);
  const onNavigateDetailAsset = (qrcode: any) =>
    nav.navigate(
      ScreenName.DetailAssets as never,
      {
        qrcode,
      } as never
    );
  const handleOnPress = () => {
    setViewSearchBar(!viewSearchBar);
    Animated.timing(inputAnimation, {
      toValue: 1,

      useNativeDriver: true,
    }).start();

    setTimeout(() => {
      if (searchRef?.current) {
        searchRef?.current.focus();
      }
    }, 200);
  };

  const handleClearSearchInput = () => {
    if (viewSearchBar) {
      setKeySearch("");
      setPageIndex(1);
      setViewSearchBar(!viewSearchBar);
      Animated.timing(inputAnimation, {
        toValue: 0,
        useNativeDriver: true,
      }).start();
    }
  };

  const onSelectOrganization = (item: any) => {
    if (item?.id === organization?.id) {
      setOrganization(null);
      organizationFilterRef?.current?.onCloseModal();
      return;
    }
    setOrganization(item);
    organizationFilterRef?.current?.onCloseModal();
    advancedFilterRef?.current?.setOrganizationFromMain(item);
    // An o search khi chon filter
    if (viewSearchBar) {
      handleClearSearchInput();
    }
  };
  const onSelectUser = (item: any) => {
    if (item?.id === user?.id) {
      setUser(null);
      userFilterRef?.current?.onCloseModal();
      return;
    }
    setUser(item);
    userFilterRef?.current?.onCloseModal();
    advancedFilterRef?.current?.setUserFromMain(item);
    // An o search khi chon filter
    if (viewSearchBar) {
      handleClearSearchInput();
    }
  };

  const onSelectGroup = (item: any) => {
    if (item?.id === group?.id) {
      setGroup(null);
      groupFilterRef?.current?.onCloseModal();
      return;
    }
    setGroup(item);
    groupFilterRef?.current?.onCloseModal();
    advancedFilterRef?.current?.setGroupFromMain(item);
    // An o search khi chon filter
    if (viewSearchBar) {
      handleClearSearchInput();
    }
  };

  const handleSearch = (keyword: string) => {
    setKeySearch(keyword);
    setPageIndex(1);
    dispatch(
      HomeActions.getListAssetsAsyncAction.request({
        params: {
          all: keyword,
          asset: "",
          equipment: "",
          description: "",
          costcenter: objDepartment?.id || "",
          // Khi search thi khong filter (Da confirm voi khach)
          person: "",
          asset_group: "",
          page: 1,
        },
        isNoLoading: true,
      })
    );
  };

  const setFilter = (_orgnaziationValue: any, _userValue: any, _goup: any) => {
    setGroup(_goup);
    // setOrganization(_orgnaziationValue);
    setUser(_userValue);
    handleClearSearchInput();
  };
  const onScroll = () => {
    setHasScrolled(true);
  };

  const handleLoadMore = (value: any) => {
    if (!hasScrolled || isLoadingLoadMore || pageIndex === totalPage) {
      return null;
    }
    //start call api
    setLoadingLoadMore(true);
    dispatch(
      HomeActions.getListAssetsAsyncAction.request({
        params: {
          all: keySearch,
          asset: "",
          equipment: "",
          description: "",
          costcenter: objDepartment?.id || "",
          person: user?.id?.toString() || "",
          asset_group: group?.id.toString() || "",
          page: pageIndex + 1 || 1,
        },
        isNoLoading: true,
        onSuccess: () => {
          setLoadingLoadMore(false);
          setHasScrolled(false);
        },
        onFailure: () => {
          setLoadingLoadMore(false);
          setHasScrolled(false);
        },
      })
    );
    setPageIndex(e => e + 1);
  };

  const onRefreshData = () => {
    dispatch(
      HomeActions.getListAssetsAsyncAction.request({
        params: {
          all: keySearch,
          asset: "",
          equipment: "",
          description: "",
          costcenter: objDepartment?.id,
          person: user?.id?.toString() || "",
          asset_group: group?.id.toString() || "",
          page: 1,
        },
        isNoLoading: true,
      })
    );
    setPageIndex(1);
    setLoadingLoadMore(false);
  };

  const onPressAssetGroupUser = () => {
    organizationFilterRef?.current?.onOpenModal();
    organizationFilterRef?.current?.scrollToIndex();
  };
  const onPressUser = () => {
    userFilterRef?.current?.onOpenModal();
    userFilterRef?.current?.scrollToIndex();
  };
  const onPressGroup = () => groupFilterRef?.current?.onOpenModal();

  const FilterComponent = () => {
    const LabelWithIcon = (defaultLabel: string, onPress: any, label: string) => {
      return (
        <TouchableOpacity style={styles.lblWithIcon} onPress={onPress}>
          <TextCM style={styles.txtFilter}>{label ? label : defaultLabel}</TextCM>
          <ICArrowDown width={W(16)} height={W(16)} style={styles.icFilter} />
        </TouchableOpacity>
      );
    };
    return (
      <View style={styles.ctnFilter}>
        <TouchableOpacity onPress={() => advancedFilterRef?.current?.onOpenModal()}>
          <ICFilterDefault />
        </TouchableOpacity>
        <View style={styles.ctnLine} />
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {LabelWithIcon(t("IS.AssetGroupUser"), onPressAssetGroupUser, organization?.label)}
          {LabelWithIcon(t("IS.AssetUser"), onPressUser, user?.label)}
          {LabelWithIcon(t("IS.AssetGroup"), onPressGroup, group?.label)}
        </ScrollView>
      </View>
    );
  };

  const RightComponent = () => {
    return (
      <>
        {viewSearchBar && (
          <Animated.View style={[{ transform: [{ translateX: inputWidth }] }, { width: W(300) }]}>
            <SearchInput
              ref={searchRef}
              value={keySearch}
              handleOnBlur={handleClearSearchInput}
              onTextSearch={handleSearch}
              placeholder=""
            />
          </Animated.View>
        )}
        {!viewSearchBar && (
          <Pressable onPress={handleOnPress} hitSlop={10}>
            <ICSearchYellow24 width={W(24)} height={W(24)} />
          </Pressable>
        )}
      </>
    );
  };

  const RenderCardItem = ({ item }: any) => {
    const renderData = (label: string, value: string, avatar?: any) => {
      return (
        <View style={styles.ctnRenderData}>
          <View style={styles.ctnDataCardItem}>
            <TextCM style={styles.txtLabelCard}>{`${label}`}</TextCM>
          </View>
          <View style={styles.ctnDataCardItem}>
            {avatar && (
              <View style={styles.ctnAvatar}>
                <TextCM style={styles.txtNameAvatar}>{value?.charAt(0).toUpperCase()}</TextCM>
              </View>
            )}

            <TextCM
              style={[
                styles.txtValueCard,
                avatar && {
                  width: (ScreenWidth - 64 - W(13)) / 2 - W(32),
                },
              ]}>{`${value}`}</TextCM>
          </View>
        </View>
      );
    };
    return (
      <TouchableOpacity style={styles.ctnCardItem} onPress={() => onNavigateDetailAsset(item?.qrcode)}>
        <TextCM numberOfLines={1} style={styles.txtAssetCode}>
          {item?.asset || "-"}
        </TextCM>
        <View>
          {renderData(t("IS.AssetName"), item?.description) || "-"}
          {renderData(t("IS.AssetUser"), item?.person_name || "-", item?.person_name)}
          {renderData(t("IS.AssetGroup"), item?.asset_groupname || "-")}
          {renderData(t("IS.AssetGroupUser"), item?.costcenter_name || "-")}
        </View>
      </TouchableOpacity>
    );
  };

  const ItemSeparator = () => {
    return <View style={{ height: H(16) }} />;
  };

  const CreateAsset = () => {
    return (
      <TouchableOpacity onPress={onNavigateCreateAsset} style={styles.btnCreateAsset}>
        <ICAddCircle />
        <TextCM style={styles.txtCreateAsset}>{`${t("IS.CreateNewAsset")}`}</TextCM>
      </TouchableOpacity>
    );
  };

  const EmptyComponent = () => (
    <View style={styles.ctnNotFound}>
      <ICAssetsEmpty />
      <TextCM style={styles.txtNotFound}>
        {role.includes(Types.PERMISSION_SAP.A70) ? `${t("IS.txtNotFound")}` : `${t("IS.txtNotFoundWithoutPermission")}`}
      </TextCM>
      {role.includes(Types.PERMISSION_SAP.A70) ? (
        <TouchableOpacity style={styles.btnNewAsset} onPress={onNavigateCreateAsset}>
          <ICPlus />
          <TextCM style={styles.txtNewAsset}>{`${t("IS.txtNew")}`}</TextCM>
        </TouchableOpacity>
      ) : (
        <></>
      )}
    </View>
  );

  const QRComponent = () => {
    const AlertPermission = () => {
      Alert.alert(
        t("IS.txtError"),
        t("IS.txtRequestPermission"),
        [
          {
            text: t("IS.txtGoToSetting"),
            onPress: () => {
              openSettings();
            },
          },
          {
            text: t("IS.txtClose"),
          },
        ],
        { cancelable: false }
      );
    };
    const navigateToQrScreen = async () => {
      const result = await request(Platform.OS === "android" ? PERMISSIONS.ANDROID.CAMERA : PERMISSIONS.IOS.CAMERA);
      switch (result) {
        case RESULTS.GRANTED:
          nav.navigate(ScreenName.QrScan as never);
          break;
        case RESULTS.BLOCKED:
          AlertPermission();
          break;
        default:
          break;
      }
    };
    return (
      <TouchableOpacity style={styles.ctnQR} onPress={navigateToQrScreen}>
        <ICQR />
      </TouchableOpacity>
    );
  };

  const closeMiniApp = () => {
    XFramework.closeApp();
  };

  return (
    <Fragment>
      <View style={styles.container}>
        <StatusBar barStyle={"light-content"} translucent backgroundColor="transparent" />
        <QRComponent />

        <Header
          style={{ zIndex: 10 }}
          componentRight={<RightComponent />}
          title={viewSearchBar ? "" : "Thông tin tài sản"}
          componentLeft={
            <TouchableOpacity style={styles.navBtnLeft} onPress={() => changeScreen(props)}>
              <ICBackSVG />
            </TouchableOpacity>
          }
        />
        <FilterComponent />
        {role.includes(Types.PERMISSION_SAP.A70) ? <CreateAsset /> : <></>}
        <FlatList
          data={assetList}
          style={{ marginVertical: 16 }}
          renderItem={RenderCardItem}
          onScroll={onScroll}
          onEndReached={handleLoadMore}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={EmptyComponent}
          keyExtractor={item => item.qrcode}
          ItemSeparatorComponent={ItemSeparator}
          // onEndReachedThreshold={0.5}
          ListFooterComponent={
            <View>
              {isLoadingLoadMore && (
                <ActivityIndicator
                  animating={isLoadingLoadMore}
                  color={Color.Yellow}
                  size="large"
                  style={styles.mt16}
                />
              )}
              <View style={styles.ctnFooter}></View>
            </View>
          }
          refreshControl={
            <RefreshControl
              refreshing={isLoadingRefresh && !isLoadingLoadMore && !isFirstLoad.current}
              onRefresh={onRefreshData}
              colors={[Color.Yellow]}
              tintColor={Color.TextWhite}
            />
          }
        />
        {/* <RenderCardItem /> */}
        <AdvancedFilter
          ref={advancedFilterRef}
          organizationValue={organization}
          userValue={user}
          groupValue={group}
          setFilter={setFilter}
        />
        <OrganizationFilter ref={organizationFilterRef} onSelect={onSelectOrganization} value={organization?.id} />
        <UserFilter ref={userFilterRef} onSelect={onSelectUser} value={user?.id} />
        <GroupFilter ref={groupFilterRef} onSelect={onSelectGroup} value={group?.id} />
      </View>
    </Fragment>
  );
};

export default HomeScreen;

import { Color, FontSize, heightResponsive as H, ScreenWidth, widthResponsive as W } from "@Constants";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  navBtnLeft: {
    position: "absolute",
    left: W(8),
  },
  ctnFilter: {
    marginTop: H(20),
    paddingHorizontal: W(16),
    flexDirection: "row",
    alignItems: "center",
  },
  icHome: {
    position: "absolute",
    left: W(8),
    width: W(30),
  },
  ctnLine: {
    width: W(1),
    marginHorizontal: W(12),
    backgroundColor: Color.line,
  },
  txtFilter: {
    fontWeight: "400",
    color: Color.White,
    lineHeight: H(20),
    alignSelf: "center",
    fontSize: FontSize.FontMedium,
  },
  lblWithIcon: {
    marginRight: W(20),
    flexDirection: "row",
  },
  icFilter: {
    marginLeft: W(4),
    alignSelf: "center",
  },
  txtCreateAsset: {
    marginLeft: W(10),
    fontWeight: "400",
    color: Color.White,
    fontSize: FontSize.FontMedium,
    alignSelf: "center",
  },
  btnCreateAsset: {
    marginTop: H(20),
    borderRadius: 12,
    flexDirection: "row",
    paddingVertical: H(12),
    marginHorizontal: W(16),
    paddingHorizontal: W(8),
    backgroundColor: Color.CardBackground,
  },
  ctnCardItem: {
    borderRadius: W(16),
    paddingVertical: H(20),
    marginHorizontal: W(16),
    paddingHorizontal: W(16),
    backgroundColor: Color.CardBackground,
  },
  ctnRenderData: {
    marginTop: H(16),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  ctnDataCardItem: {
    flexDirection: "row",
    width: (ScreenWidth - 64 - W(13)) / 2,
    alignItems: "center",
  },
  txtLabelCard: {
    fontWeight: "400",
    color: Color.TextGray,
    fontSize: FontSize.FontSmaller,
  },
  txtValueCard: {
    fontWeight: "400",
    color: Color.White,
    fontSize: FontSize.FontSmaller,
  },
  txtAssetCode: {
    fontWeight: "500",
    color: Color.Yellow,
    fontSize: FontSize.FontMedium,
  },
  imAvatar: {
    width: H(24),
    height: H(24),
    marginRight: H(8),
    borderRadius: H(10),
    alignSelf: "center",
    alignItems: "center",
    alignContent: "center",
  },

  mt16: {
    marginTop: 16,
  },
  ctnFooter: {
    height: H(50),
  },
  ctnNotFound: {
    marginTop: H(130),
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  txtNotFound: {
    marginTop: H(12),
    textAlign: "center",
    color: Color.TextGray,
    alignSelf: "center",
    fontSize: FontSize.FontSmaller,
  },
  btnNewAsset: {
    width: W(115),
    marginTop: H(16),
    borderRadius: W(10),
    flexDirection: "row",
    paddingVertical: H(6),
    paddingHorizontal: W(16),
    backgroundColor: Color.Yellow,
    alignSelf: "center",
  },
  txtNewAsset: {
    marginLeft: W(8),
    lineHeight: H(20),
    fontWeight: "500",
    fontStyle: "normal",
    alignSelf: "center",
  },
  ctnQR: {
    zIndex: 1,
    right: W(16),
    bottom: H(34),
    position: "absolute",
  },
  ctnAvatar: {
    width: H(24),
    height: H(24),
    marginRight: W(8),
    borderRadius: H(100),
    backgroundColor: Color.Background,
    justifyContent: "center",
    alignSelf: "center",
  },

  txtNameAvatar: {
    color: Color.White,
    textAlign: "center",
    fontSize: FontSize.FontMedium,
    fontWeight: "700",
  },
});

import { isEmpty, isNil } from "lodash";
import React, { FC, forwardRef, useState } from "react";
import { Controller, useFieldArray, useFormContext } from "react-hook-form";
import { Alert, Platform, TouchableOpacity, TouchableOpacityProps, View } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import { launchImageLibrary } from "react-native-image-picker-supper";

import { ICCamera, ICCloseCircleYellow, ICFailure, ICSuccess } from "@Assets/image";
import { ImageCache, PopupAnalog, TextCM } from "@Components";
import { heightResponsive as H, widthResponsive as W } from "@Constants";
import TextError from "src/app/components/TextError";
import { I18n } from "../data";
import styles from "./styles";
import { FieldType } from "@Screens/create-asset-screen/type";
import moment from "moment";
import { t } from "i18next";
import { openSettings, PERMISSIONS, request, RESULTS } from "react-native-permissions";

export type Iprops = {
  name: string;
  useHookForm?: boolean;
  onChange?: (value: any) => void;
  value?: any;
  messageError?: string;
  max?: number;
  title?: string;
  isRequired?: boolean;
};

const ImagesFieldCustom: FC<TouchableOpacityProps & Iprops> = forwardRef(
  (
    {
      name,
      useHookForm = true,
      onChange,
      value = [],
      messageError = "",
      max,
      title = "",
      isRequired = false,
    }: TouchableOpacityProps & Iprops,
    $ref
  ) => {
    const isUseTitle = !isEmpty(title);
    const isError = !isEmpty(messageError);
    const [isShowWarning, setIsShowWaring] = useState<boolean>(false);
    const { control, clearErrors, getValues, setValue } = useFormContext();

    const { fields, insert, remove } = useFieldArray({
      control,
      name,
    });

    const ContainerImage = ({ uri, index, isDownload, isSuccess, isShowStatus, isOld }: any) => {
      const onRemoveImage = () => {
        const images = getValues(name);
        const imagesRemoved = getValues("imagesRemoved") || [];
        if (isOld) {
          setValue("imagesRemoved", [
            ...imagesRemoved,
            {
              imageid: uri,
              type: "R",
            },
          ]);
        }
        if (images?.every(({ isSuccess }: any) => (typeof isSuccess === "boolean" ? isSuccess : true))) {
          clearErrors(name);
        }
        remove(index);
      };

      return (
        <View style={[styles.ctnImage, (index + 2) % 3 != 0 && styles.mrImage]}>
          <ImageCache style={styles.imgCache} uri={uri} id={uri} imageDefault={uri} isDownLoad={isDownload} />
          {isShowStatus && typeof isSuccess === "boolean" && isSuccess && (
            <ICSuccess width={H(24)} height={H(24)} style={styles.icStatusUpload} />
          )}
          {isShowStatus && typeof isSuccess === "boolean" && !isSuccess && (
            <ICFailure width={H(24)} height={H(24)} style={styles.icStatusUpload} />
          )}
          <TouchableOpacity
            onPress={onRemoveImage}
            hitSlop={{ right: W(10), left: W(10), top: H(10), bottom: H(10) }}
            style={styles.btnCloseImg}>
            <ICCloseCircleYellow width={H(20)} height={H(20)} />
          </TouchableOpacity>
        </View>
      );
    };

    const AlertPermission = (text: string) => {
      Alert.alert(
        t("IS.txtError"),
        text,
        [
          {
            text: t("IS.txtGoToSetting"),
            onPress: () => {
              openSettings();
            },
          },
          {
            text: t("IS.txtClose"),
          },
        ],
        { cancelable: false }
      );
    };

    const selectImageCrop = () => {
      ImagePicker.openPicker({
        cropping: false,
        mediaType: "photo",
      }).then(image => {
        if (image) {
          const itemImage = {
            ...image,
            isSelect: true,
            id: value?.length + 1,
            uri: image?.path,
          };
          onChange && onChange(itemImage.uri);
          insert(0, {
            imageid: itemImage.uri,
            isDownload: false,
            type: image?.mime,
          });
        }
      });
    };

    const handleChoosePhoto = async () => {
      const result = await request(
        Platform.OS === "android" ? PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE : PERMISSIONS.IOS.PHOTO_LIBRARY
      );
      switch (result) {
        case RESULTS.GRANTED:
        case RESULTS.LIMITED:
          openLibrary();
          break;
        case RESULTS.BLOCKED:
          AlertPermission(t("IS.txtRequestPermissionLibrary"));
          break;
        default:
          break;
      }
    };

    const openLibrary = () => {
      clearErrors(FieldType.Image);
      if (Platform.OS === "ios") {
        selectImageCrop();
        return;
      }

      launchImageLibrary(
        {
          mediaType: "photo",
        },
        (response: any) => {
          if (response) {
            if (!isNil(response?.assets?.[0]?.fileSize)) {
              if (response?.assets?.[0]?.fileSize > 10485760) {
                onToggleWarning();
              } else {
                // TODO: Handle call api upload image
                onChange && onChange(response?.assets[0].uri);
                insert(0, {
                  imageid: response?.assets[0].uri,
                  isDownload: false,
                  type: response?.assets?.[0]?.type,
                });
              }
            }
          }
        }
      );
    };

    const onToggleWarning = () => setIsShowWaring(!isShowWarning);

    return (
      <>
        <PopupAnalog
          visible={isShowWarning}
          content={I18n.txtWarningImage}
          onClose={onToggleWarning}
          onPressConfirm={onToggleWarning}
          type="warning"
          titleButton={I18n.btnTryAgain}
        />

        {isUseTitle && (
          <View style={[{ flexDirection: "row", marginTop: H(16) }]}>
            <TextCM
              style={[
                styles.txtDefaultTitle,
                styles.txtTitle,
                // disabled && styles.txtDisable,
              ]}>
              {title}
            </TextCM>
            {isRequired && (
              <TextCM
                style={[
                  styles.txtDefaultTitle,
                  styles.txtTitleRequired,
                  // disabled && styles.txtDisable,
                ]}>
                *
              </TextCM>
            )}
          </View>
        )}

        <View style={styles.ctnListImage}>
          <TouchableOpacity
            style={[styles.ctnImageUpload, styles.mrImage]}
            onPress={handleChoosePhoto}
            disabled={max ? value?.length >= max : false}>
            <ICCamera width={W(56)} height={W(56)} />
          </TouchableOpacity>
          {fields?.map((_: any, index: number) => (
            <Controller
              key={`${_.id}${moment().valueOf()}`}
              control={control}
              name={`${name}.${index}`}
              render={({ field: { value: _value } }) => {
                const { imageid, isDownload, isSuccess, isShowStatus, isOld = false } = _value;
                return (
                  <ContainerImage
                    uri={imageid || ""}
                    key={`${name}-${index}`}
                    index={index}
                    isShowStatus={isShowStatus}
                    isSuccess={isSuccess}
                    isOld={isOld}
                    isDownload={typeof isDownload === "boolean" ? isDownload : true}
                  />
                );
              }}
            />
          ))}
        </View>

        {isError && <TextError styleWrapper={{ marginTop: 0 }}>{messageError || ""}</TextError>}
      </>
    );
  }
);

export default ImagesFieldCustom;

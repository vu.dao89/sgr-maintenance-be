import React, { FC } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { TouchableOpacityProps, View } from "react-native";
import ImagesFieldCustom, { Iprops } from "./ImagesFieldCustom";

type IImagesField = {
  name: string;
  useHookForm?: boolean;
  onChange?: Function;
  max?: number;
};

const ImagesField: FC<TouchableOpacityProps & IImagesField & Iprops> = ({
  name,
  useHookForm = true,
  onChange,
  max,
  ...more
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const getMsgError = (): string => {
    if (errors && Object.keys(errors) && Object.keys(errors).includes(name)) {
      const { [name]: field } = errors;
      return String(field?.message) || "";
    }
    return "";
  };

  const _onChangeValue = (value: any): void => {
    onChange && onChange(value);
  };

  return (
    <View>
      {useHookForm ? (
        <Controller
          control={control}
          name={name}
          render={({ field: { value, ...moreField } }) => {
            return (
              <ImagesFieldCustom
                {...more}
                {...moreField}
                onChange={_onChangeValue}
                name={name}
                useHookForm={useHookForm}
                value={value}
                max={max}
                messageError={getMsgError()}
              />
            );
          }}
        />
      ) : (
        <></>
      )}
    </View>
  );
};

export default ImagesField;

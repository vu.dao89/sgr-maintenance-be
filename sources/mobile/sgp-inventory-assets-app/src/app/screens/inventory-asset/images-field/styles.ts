import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, Color, FontSize, Font } from "@Constants";
export default StyleSheet.create({
  ctnListImage: {
    width: "100%",
    flexDirection: "row",
    flexWrap: "wrap",
    // justifyContent: 'space-between',
    // marginBottom: H(9)
  },
  ctnImageUpload: {
    width: W(108),
    height: W(108),
    borderRadius: H(8),
    marginBottom: H(12),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Color.FilterBackground,
  },
  mrImage: {
    marginRight: W(8),
  },
  ctnImage: {
    width: W(108),
    height: W(108),
    borderRadius: H(8),
    marginBottom: H(12),
    position: "relative",
  },
  imgCache: {
    width: W(108),
    height: W(108),
    borderRadius: H(8),
    marginBottom: H(12),
  },
  btnCloseImg: {
    position: "absolute",
    top: H(-6),
    right: W(-6),
    width: W(20),
    height: W(20),
    borderRadius: W(10),
    borderWidth: W(3),
    borderColor: Color.Background,
    backgroundColor: Color.Background,
    alignItems: "center",
    justifyContent: "center",
  },
  ctnError: {
    flexDirection: "row",
    alignItems: "flex-start",
    // marginTop: H(8),
  },
  txtError: {
    marginLeft: W(4),
    marginRight: W(8),
    color: Color.RedText,
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  icError: {
    marginTop: H(2),
  },

  ctnImageViewMore: {
    position: "relative",
    width: W(98),
    height: W(98),
    borderRadius: H(8),
    marginBottom: H(11),
  },
  ctnRemainingAmount: {
    position: "absolute",
    backgroundColor: Color.Background,
    opacity: 0.8,
    top: 0,
    left: 0,
    width: W(98),
    height: W(98),
    borderRadius: H(8),
    alignItems: "center",
    justifyContent: "center",
  },
  txtRemainingAmount: {
    fontSize: FontSize.FontBigger,
    fontStyle: "normal",
    fontFamily: Font.Roboto,
    color: Color.White,
    fontWeight: "400",
    lineHeight: 30,
  },
  txtDefaultTitle: {
    fontWeight: "400",
    fontSize: FontSize.FontSmaller,
    lineHeight: H(20),
  },
  txtTitle: {
    color: Color.LightGray,
    marginBottom: H(8),
  },
  txtTitleRequired: {
    marginLeft: W(2),
    color: Color.RedText,
  },
  icStatusUpload: {
    position: "absolute",
    bottom: H(4),
    left: W(4),
    alignItems: "center",
    justifyContent: "center",
  },
});

import React, { FC } from "react";
import { TouchableOpacity, TouchableOpacityProps, View } from "react-native";

import { ImageCache, TextCM } from "@Components";
import { dataImageInventoryFake } from "../data";
import { IImageViewMoreProps } from "../types";
import styles from "./styles";
import { widthResponsive as W } from "@Constants";

type IImagesView = {
  maxDisplay?: number;
  onClick?: (index: number) => void;
  data: any[];
  widthImage?: number;
};

const ImageViewMore: FC<IImageViewMoreProps> = ({ uri, remainingAmount = 0, width }) => (
  <View style={[styles.ctnImageViewMore, { width: W(width || 98), height: W(width || 98) }]}>
    <ImageCache
      style={[styles.ctnImage, { width: W(width || 98), height: W(width || 98) }]}
      uri={uri}
      id={uri}
      imageDefault={uri}
      isDownLoad={true}
    />
    <View style={[styles.ctnRemainingAmount, { width: W(width || 98), height: W(width || 98) }]}>
      <TextCM style={styles.txtRemainingAmount}>{`+${remainingAmount}`}</TextCM>
    </View>
  </View>
);

const ImagesView: FC<TouchableOpacityProps & IImagesView> = ({
  maxDisplay = 0,
  onClick,
  data = [],
  widthImage = 0,
}) => {
  const onClickImage = (index: number) => () => {
    onClick && onClick(index);
  };

  return (
    <View style={styles.ctnListImage}>
      {data?.slice(0, maxDisplay)?.map(({ imageid }: any, index: number) =>
        index === maxDisplay - 1 && data?.length > maxDisplay ? (
          <TouchableOpacity
            key={"picture" + index}
            onPress={onClickImage(index)}
            style={
              (index + 1) % 3 != 0 && {
                marginRight: W(widthImage === 98 ? 8 : 9),
              }
            }>
            <ImageViewMore uri={imageid} remainingAmount={data?.length - maxDisplay} width={widthImage} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            key={"inventory-picture" + index}
            onPress={onClickImage(index)}
            style={
              (index + 1) % 3 != 0 && {
                marginRight: W(widthImage === 98 ? 8 : 9),
              }
            }>
            <ImageCache
              style={[styles.ctnImage, { width: W(widthImage || 98), height: W(widthImage || 98) }]}
              imageDefault={imageid}
              uri={imageid}
              id={imageid}
              isDownLoad={true}
            />
          </TouchableOpacity>
        )
      )}
    </View>
  );
};

export default ImagesView;

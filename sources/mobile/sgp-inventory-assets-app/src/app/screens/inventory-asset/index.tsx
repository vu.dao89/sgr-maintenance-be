// external
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigation } from "@react-navigation/native";
import { debounce, get, isNull, parseInt } from "lodash";
import React, { FC, useCallback, useEffect, useRef, useState } from "react";
import { FormProvider, SubmitHandler, useFieldArray, useForm, useFormContext } from "react-hook-form";
import {
  ActivityIndicator,
  AppState as AppStateRN,
  AppStateStatus,
  Platform,
  SafeAreaView,
  TouchableOpacity,
  View,
} from "react-native";
import Geolocation, { GeoError, GeoPosition, PositionError } from "react-native-geolocation-service";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";

// internal
import { DetailAssetsAction } from "@Actions";
import {
  Header,
  // ImageCache,
  MapView,
  PopupAnalog,
  SelectField,
  TextArea,
  TextCM,
  TextField,
} from "@Components";
import { Color, heightResponsive as H, ScreenName, StatusTypes } from "@Constants";
import { AppState } from "@Reducers";
import { getItemSelect } from "@Screens/create-asset-screen/form/basic-info";
import StatusFilter from "@Screens/create-asset-screen/modal/status-filter";
import { DetailAssetsServices } from "@Services";
import { axiosHandler } from "@Services/httpClient";
import { ImageUtils, LocationUtils } from "@Utils";
import {
  // dataImageInventoryFake,
  I18n,
} from "./data";
import ImagesField from "./images-field";
import styles from "./styles";
import {
  BodyInventory,
  FieldName,
  FieldTypeForm,
  IBasicInformationProps,
  IContentComponentProps,
  IDetailAssetProps,
  IInventoryInformationProps,
} from "./types";

const schema = yup
  .object({
    [FieldName.Images]: yup.array().min(1, I18n.msgRequired).required(I18n.msgRequired),
    [FieldName.Location]: yup
      .object({
        longitude: yup.number().required(I18n.msgRequired),
        latitude: yup.number().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
    [FieldName.Status]: yup
      .object({
        id: yup.string().trim().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
    [FieldName.Description]: yup.string().trim().required(I18n.msgRequired),
    [FieldName.Quanlity]: yup.string().trim().required(I18n.msgRequired),
  })
  .required();

const ViewContentComponent: FC<IContentComponentProps> = ({ label, value }) => (
  <View style={[styles.ctnLabelAndValue]}>
    <TextCM style={[styles.txtGeneral, styles.txtLabel]}>{label}</TextCM>
    <TextCM style={[styles.txtGeneral, styles.txtValue]}>{value}</TextCM>
  </View>
);

const BasicInformationComponent: FC<IBasicInformationProps> = ({ data = {} }) => (
  <View style={styles.ctnInfoBasic}>
    <TextCM style={styles.txtCodeAsset}>{data?.asset || ""}</TextCM>
    <ViewContentComponent label={I18n.txtNameAsset} value={data?.description || ""} />
    <ViewContentComponent label={I18n.txtSerial} value={data?.serial || ""} />
  </View>
);

type ICurrentSelected = {
  [FieldName.Status]?: any;
};

type IProps = {
  currentSelected?: ICurrentSelected;
};

const InventoryInformationComponent: FC<IInventoryInformationProps & IProps> = ({
  // data = {},
  currentSelected,
}) => {
  const { setValue, getValues } = useFormContext();
  const statusRef = useRef<any>(null);
  const [hasPermission, setPermission] = useState<boolean>(false);

  const onRefreshRegion = async (
    config?: Partial<{
      shouldValidate: boolean;
      shouldDirty: boolean;
      shouldTouch: boolean;
    }>
  ) => {
    const hasLocationPermission = await LocationUtils.hasLocationPermission();

    if (!hasLocationPermission) {
      return;
    }

    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
        ({ mocked, coords }: GeoPosition) => {
          setPermission(true);
          const { latitude, longitude } = coords;
          if (mocked) {
            setPermission(false);
            setValue(FieldName.Location, { latitude: null, longitude: null }, config);
          } else {
            const { latitude: lat, longitude: long } = getValues(FieldName.Location);

            setValue(FieldName.Location, { latitude, longitude }, config);

            if (Platform.OS === "ios") {
              if (isNull(lat) || isNull(long)) {
                setTimeout(() => {
                  onRefreshRegion();
                }, 300);
              }
            }
          }
        },
        ({ code }: GeoError) => {
          if (code === PositionError.POSITION_UNAVAILABLE) {
            setPermission(false);
            setValue(FieldName.Location, { latitude: null, longitude: null }, config);
          }
        },
        {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
          showLocationDialog: false,
        }
      );
    }
  };

  useEffect(() => {
    onRefreshRegion();
  }, []);

  const appState = useRef(AppStateRN.currentState);

  useEffect(() => {
    const _handleAppStateChange = function handleAppStateChange(nextAppState: AppStateStatus) {
      if (appState.current.match(/background/) && nextAppState === "active") {
        onRefreshRegion();
      }

      appState.current = nextAppState;
    };

    AppStateRN.addEventListener("change", _handleAppStateChange);
    return () => AppStateRN.removeEventListener("change", _handleAppStateChange);
  }, []);

  const [currentValues, setCurrentValues] = useState<ICurrentSelected>({
    [FieldName.Status]: currentSelected?.[FieldName.Status] || "",
  });

  const onPressStatus = () => statusRef?.current?.onOpenModal();

  const onSelectStatus = (item: any) => {
    setValue(FieldName.Status, getItemSelect(item), {
      shouldValidate: true,
      shouldDirty: true,
    });
    setCurrentValues({
      ...currentValues,
      [FieldName.Status]: item?.id || "",
    });
    statusRef?.current?.onCloseModal();
  };
  return (
    <View style={styles.ctnInfoInventory}>
      <TextField
        name={FieldName.Quanlity}
        title={I18n.lblQuality}
        placeholder={I18n.plhQuality}
        isRequired
        style={styles.field}
        maxLength={13}
        keyboardType="numeric"
      />
      <SelectField
        name={FieldName.Status}
        title={I18n.lblStatus}
        placeholder={I18n.plhStatus}
        isRequired
        style={styles.field}
        toggleModal={onPressStatus}
        renderModal={
          <StatusFilter ref={statusRef} onSelect={onSelectStatus} value={currentValues[FieldName.Status] || ""} />
        }
      />
      <TextArea
        name={FieldName.Description}
        title={I18n.lblDescriptionCurrentStatus}
        placeholder={I18n.plhDescriptionCurrentStatus}
        isRequired
        style={styles.field}
      />
      <MapView
        name={FieldName.Location}
        title={I18n.lblLocation}
        placeholder={""}
        isRequired
        styleMap={styles.map}
        onRefreshRegion={() => onRefreshRegion({ shouldDirty: true, shouldValidate: true })}
        hasPermission={hasPermission}
      />
      <View
        style={[
          {
            flexDirection: "row",
            width: "100%",
            marginBottom: H(12),
            alignItems: "center",
          },
        ]}>
        <TextCM style={[styles.txtDefaultTitle, styles.txtLabelInput]}>{I18n.txtLastImageInventory}</TextCM>
        <TextCM style={[styles.txtDefaultTitle, styles.txtTitleRequired]}>*</TextCM>
      </View>
      <TextCM style={styles.txtCapacity}>{I18n.txtMaxCapacity(10, 10 - getValues(FieldName.Images).length)}</TextCM>
      <ImagesField name={FieldName.Images} max={10} />
    </View>
  );
};

const InventoryAssetsScreen: FC<IDetailAssetProps> = ({}) => {
  const dataDetailAssets = useSelector((state: AppState) => state.detailAssets);
  const data = get(dataDetailAssets, "data", {} as any);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const { objDepartment, email } = get(reducer, "userInfo.data", {});

  const [loading, setLoading] = useState<boolean>(false);
  const [showNotice, setShowNotice] = useState<any>({
    visible: false,
    type: "success",
    content: "",
    title: "",
  });

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onPressHistory = useCallback(() => {
    navigation?.navigate(ScreenName.Template as never, {} as never);
  }, []);

  const RightComponent = () => {
    return (
      <TouchableOpacity style={{ zIndex: 1 }} onPress={onPressHistory}>
        <TextCM style={styles.txtHistory}>{I18n.txtHistory}</TextCM>
      </TouchableOpacity>
    );
  };

  const methods = useForm<FieldTypeForm>({
    mode: "onChange",
    reValidateMode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: {
      ...data,
      quantity: String(parseInt(data?.inventory?.quantity || 0)),
      status: data?.inventory?.status
        ? {
            id: data?.inventory?.status,
            label: StatusTypes[data?.inventory?.status],
          }
        : "",
      description: data?.inventory?.description || "",
      location: {
        latitude: Number(data?.latitude) || null,
        longitude: Number(data?.longitude) || null,
      },
      image:
        data?.image?.map(({ imageid }: any) => ({
          imageid,
          isDownload: true,
          isSuccess: true,
          isShowStatus: false,
          type: null,
          isOld: true,
        })) || [],
      imagesRemoved: [],
    },
  });

  const {
    handleSubmit,
    formState: { isDirty, isValid },
    control,
    setError,
  } = methods;

  const { replace } = useFieldArray({
    control,
    name: FieldName.Images,
  });

  const getValueSelect = (values: any, field: string) => values[field]?.id || "";

  const _onInventory = async (data: FieldTypeForm) => {
    const listNotUpload = data.image?.filter(({ isSuccess }: any) => typeof isSuccess !== "boolean") || [];
    const listUploaded = data.image?.filter(({ isSuccess }: any) => typeof isSuccess === "boolean") || [];
    if (listUploaded.some(({ isSuccess }: any) => !isSuccess)) {
      setShowNotice({
        visible: true,
        type: "error",
        content: I18n.messageInventoryFailure,
        title: I18n.titleInventoryFailure,
      });
      setLoading(false);
      return;
    }
    const allPromies = await ImageUtils.uploadImage(listNotUpload);

    const objImages = allPromies?.map((ele: any) => ({
      status: ele?.status,
      imageid: ele?.status === 201 ? ele?.imageid : ele?.uriLocal,
      isSuccess: ele?.status === 201,
      isDownload: ele?.status === 201,
      type: "I",
    }));
    const isFaild = objImages?.some(el => el?.status !== 201);
    if (isFaild) {
      setError(FieldName.Images, { message: I18n.txtImageError });
      const newData = [...objImages, ...listUploaded]?.map(
        ({ imageid, isSuccess, isDownload, type, isOld = false }: any) => ({
          imageid,
          isDownload,
          isSuccess,
          isShowStatus: true,
          type,
          isOld,
        })
      );
      replace(newData);
      setShowNotice({
        visible: true,
        type: "error",
        content: I18n.messageInventoryFailure,
        title: I18n.titleInventoryFailure,
      });
      setLoading(false);
      return;
    }

    const bodyReq: BodyInventory = {
      [FieldName.QRCode]: data?.qrcode || "",
      [FieldName.CostCenter]: objDepartment?.id || "",
      [FieldName.Quanlity]: data?.quantity?.toString() || "0",
      [FieldName.Status]: getValueSelect(data, FieldName.Status) || "",
      [FieldName.Description]: data?.description || "",
      [FieldName.Latitude]: data?.location?.latitude?.toString() || "",
      [FieldName.Longitude]: data?.location?.longitude?.toString() || "",
      [FieldName.UserEmail]: email || "",
      [FieldName.Images]:
        [...objImages, ...listUploaded, ...(data?.imagesRemoved || [])]?.map(({ imageid, type }: any) => ({
          imageid,
          type,
        })) || [],
    };

    const { response } = await axiosHandler(() => DetailAssetsServices.postInventoryAsset(bodyReq));
    const { code, error: _error } = (response && response.data) || {};
    if (code === "200") {
      setLoading(false);
      setShowNotice({
        visible: true,
        type: "success",
        content: I18n.messageInventorySuccess,
        title: I18n.titleInventorySuccess,
      });
    } else {
      const newData = [...objImages, ...listUploaded]?.map(
        ({ imageid, isSuccess, isDownload, type, isOld = false }: any) => ({
          imageid,
          isDownload,
          isSuccess,
          isShowStatus: true,
          type,
          isOld,
        })
      );
      replace(newData);
      setLoading(false);

      setShowNotice({
        visible: true,
        type: "error",
        content: _error,
        title: I18n.titleInventoryFailure,
      });
    }
  };

  const handlerCreate = useCallback(debounce(_onInventory, 300), []);

  const onSubmit: SubmitHandler<FieldTypeForm> = data => {
    setLoading(true);
    handlerCreate(data);
  };

  const FixedAction = () => {
    return (
      <SafeAreaView style={styles.ctnFooterAction}>
        <TouchableOpacity
          disabled={!(isDirty && isValid)}
          style={isDirty && isValid ? styles.btnInventoryNewAsset : styles.btnInventoryNewAssetDisable}
          onPress={handleSubmit(onSubmit)}>
          {loading && <ActivityIndicator style={styles.mr4} size="small" color={Color.Black} />}
          <TextCM style={styles.btnInventory}>{`${I18n.btnComplete}`}</TextCM>
        </TouchableOpacity>
      </SafeAreaView>
    );
  };

  const onCloseNotice = () => {
    setShowNotice({
      visible: false,
      type: "success",
      content: "",
    });
  };

  const onPressConfirm = () => {
    if (showNotice.type === "success") {
      onCloseNotice();
      dispatch(DetailAssetsAction.getDetailAssetAction.request(data?.qrcode));
      navigation.navigate(
        ScreenName.DetailAssets as never,
        {
          qrcode: data?.qrcode,
        } as never
      );
    } else {
      onCloseNotice();
    }
  };
  return (
    <FormProvider {...methods}>
      <View style={styles.ctn}>
        <Header title={I18n.txtHeader} />
        <KeyboardAwareScrollView>
          <BasicInformationComponent data={data} />
          <InventoryInformationComponent
            data={data}
            currentSelected={{
              status: data?.inventory?.status,
            }}
          />
        </KeyboardAwareScrollView>

        <PopupAnalog
          visible={showNotice?.visible}
          onClose={onCloseNotice}
          content={showNotice.content}
          titlePopup={showNotice?.title}
          titleButton={showNotice?.type === "error" ? I18n.btnTryAgain : I18n.btnDone}
          type={showNotice?.type}
          onPressConfirm={onPressConfirm}
        />
        <FixedAction />
      </View>
    </FormProvider>
  );
};

export default InventoryAssetsScreen;

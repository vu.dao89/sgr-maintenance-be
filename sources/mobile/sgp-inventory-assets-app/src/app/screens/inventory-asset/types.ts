import { StyleProp, ViewStyle } from "react-native";
import { ReactElement } from "react";

export type IDetailAssetProps = {
  navigation?: any;
  route?: any;
};

export type IContentComponentProps = {
  label: string;
  value: string | number;
  isMarginBottom?: boolean;
  isBorderBottom?: boolean;
  componentLeft?: ReactElement<any>;
};

export type IImageViewMoreProps = {
  uri: string;
  remainingAmount: number;
  width?: number;
};

export type IModalPrintProps = {
  isShow: boolean;
  data: any;
  onClose: () => void;
};

export type IBasicInformationProps = {
  data: any;
};
export type IInventoryInformationProps = {
  data: any;
};
export type CoordinatesState = {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
};

export type FieldTypeForm = {
  [FieldName.QRCode]: string;
  [FieldName.CostCenter]: string;
  [FieldName.Quanlity]: number;
  [FieldName.Status]: any;
  [FieldName.Description]: string;
  [FieldName.Location]: any;
  // [FieldName.Latitude]: number | string,
  // [FieldName.Longitude]: number | string,
  // [FieldName.Date]: string,
  // [FieldName.Time]: string,
  [FieldName.Images]: any[];
  [FieldName.ImageRemoved]: any[];
};
export enum FieldName {
  QRCode = "qrcode",
  CostCenter = "costcenter",
  Quanlity = "quantity",
  Status = "status",
  Description = "description",
  Location = "location",
  Latitude = "latitude",
  Longitude = "longitude",
  Date = "date",
  Time = "time",
  UserEmail = "user_email",
  Images = "image",
  ImageRemoved = "imagesRemoved",
}

export type BodyInventory = {
  [FieldName.QRCode]: string;
  [FieldName.CostCenter]: string;
  [FieldName.Quanlity]: number | string;
  [FieldName.Status]: any;
  [FieldName.Description]: string;
  [FieldName.Latitude]: number | string;
  [FieldName.Longitude]: number | string;
  [FieldName.UserEmail]: string;
  [FieldName.Images]: any[];
};

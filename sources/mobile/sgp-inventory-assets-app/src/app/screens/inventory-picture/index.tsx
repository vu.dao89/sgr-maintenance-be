// External
import React, { FC, useEffect, useRef, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import { get } from "lodash";
import { TouchableOpacity, View } from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import { IImageInfo } from "react-native-image-zoom-viewer/built/image-viewer.type";

// Internal
import { Header, ImageCache, TextCM } from "@Components";
import { Color, heightResponsive as H, ScreenHeight, ScreenWidth, widthResponsive as W } from "@Constants";
import styles from "./styles";
import { ICCaretLeft, ICCaretRight } from "@Assets/image";
import { inventoryPictureI18n } from "./data";

const FooterComponent = ({
  currentIndex = 1,
  total = 1,
  location = "",
  style = {},
  setIndexActive = (index: number) => {},
}) => {
  const onPrevious = () => {
    currentIndex > 0 && setIndexActive(currentIndex - 1);
  };
  const onNext = () => {
    currentIndex < total && setIndexActive(currentIndex + 1);
  };
  return (
    <View style={[styles.ctnFooter, style]}>
      <TextCM style={[styles.txtFooter, styles.marginBottom16]}>{location}</TextCM>
      <View style={styles.ctnIndicator}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={onPrevious}
          disabled={currentIndex === 0}
          hitSlop={{ right: W(10), left: W(10), top: H(10), bottom: H(10) }}>
          <ICCaretLeft width={H(16)} height={H(16)} />
        </TouchableOpacity>

        <TextCM style={styles.txtIndicator}>{`${currentIndex + 1}/${total}`}</TextCM>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={onNext}
          disabled={currentIndex === total - 1}
          hitSlop={{ right: W(10), left: W(10), top: H(10), bottom: H(10) }}>
          <ICCaretRight width={H(16)} height={H(16)} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const InventoryPicture: FC = () => {
  const route = useRoute();
  const nav = useNavigation();
  const { data = [], index, onCloseFullView } = get(route, "params", {} as any);
  const location = get(data, "location", "");
  const [imageUrls, setImageUrls] = useState<[IImageInfo] | undefined>(undefined);
  const [indexActive, setIndexActive] = useState<number | undefined>(index || 0);
  const refClick = useRef<any>(null);
  const [isOneTap, setIsOneTap] = useState<boolean>(false);
  const [isDoubleTap, setIsDoubleTap] = useState<boolean>(false);

  useEffect(() => {
    const newImageUrls: any = data?.images?.map(({ imageid }: any) => ({
      url: "",
      props: {
        source: imageid || "",
        id: imageid,
        isDownLoad: true,
      },
      width: ScreenWidth,
      height: ScreenHeight,
    }));
    setImageUrls(newImageUrls);
    // Clean up
    return () => setImageUrls(undefined);
  }, [data]);

  const onChange = (nextIndex: number | undefined) => {
    setIndexActive(nextIndex || 0);
  };

  const onClick = (onCancel?: any) => {
    refClick.current = setTimeout(() => {
      setIsOneTap(!isOneTap);
    }, 200);
    onCancel && onCancel();
  };

  const onDoubleClick = (onCancel?: any) => {
    if (refClick?.current) {
      clearTimeout(refClick.current);
      refClick.current = null;
    }
    setIsDoubleTap(!isDoubleTap);
    onCancel && onCancel();
  };

  const onBack = () => {
    onCloseFullView && onCloseFullView();
    nav && nav.goBack();
  };

  return (
    <View style={styles.ctnInventoryPicture}>
      {imageUrls && imageUrls.length > 0 && (
        <ImageViewer
          style={styles.ctnPictures}
          index={indexActive}
          imageUrls={imageUrls}
          backgroundColor={Color.Background}
          onChange={onChange}
          renderFooter={currentIndex =>
            !isOneTap ? (
              <FooterComponent
                currentIndex={currentIndex}
                total={imageUrls?.length}
                location={location}
                style={[
                  {
                    backgroundColor: Color.Background,
                  },
                  isDoubleTap ? styles.doubleTap : {},
                ]}
                setIndexActive={setIndexActive}
              />
            ) : (
              <></>
            )
          }
          renderHeader={() =>
            !isOneTap ? (
              <Header
                title={inventoryPictureI18n.txtHeader}
                actionLeft={onBack}
                style={[
                  {
                    zIndex: 9999,
                    position: "absolute",
                    backgroundColor: Color.Background,
                  },
                  isDoubleTap ? styles.doubleTap : {},
                ]}
              />
            ) : (
              <></>
            )
          }
          renderIndicator={() => <></>}
          onClick={onClick}
          onLongPress={() => {}}
          onDoubleClick={onDoubleClick}
          saveToLocalByLongPress={false}
          renderImage={(props: any) => (
            <ImageCache
              style={{
                ...props.style,
                ...styles.sizeImage,
              }}
              resizeMode="contain"
              uri={props.id || ""}
              id={props.id || ""}
            />
          )}
          onMove={(position: any) => {
            if (position && position?.scale === 1) {
              setIsDoubleTap(false);
            }
          }}
        />
      )}
    </View>
  );
};

export default InventoryPicture;

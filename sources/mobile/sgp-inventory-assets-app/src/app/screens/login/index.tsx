import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import { authorize } from "react-native-app-auth";
import { useDispatch } from "react-redux";

import { CommonActions } from "@Actions";
import { TextCM } from "@Components";
import { Color, configLoginAzure } from "@Constants";
import { CacheUtil } from "@Utils";
import { log } from "libs/react-native-x-framework/js/logger";
import { isEmpty } from "lodash";

type Props = {
  navigation: any;
};

// Fake Login
const Login: FC<Props> = () => {
  const dispatch = useDispatch();

  const onSignIn = async () => {
    try {
      const authorizeResult = await authorize(configLoginAzure);
      const accessToken = authorizeResult?.accessToken;
      if (accessToken) {
        CacheUtil.setToken(isEmpty(accessToken) ? "" : accessToken);
        CacheUtil.setTokenExpDate(authorizeResult?.accessTokenExpirationDate);
        dispatch(CommonActions.setTokenAsyncAction(accessToken));
      }
    } catch (error) {
      log("signin error", error);
    }
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: Color.Background,
      }}>
      <TouchableOpacity
        onPress={onSignIn}
        style={{
          backgroundColor: Color.Yellow,
          paddingHorizontal: 30,
          paddingVertical: 10,
          borderRadius: 20,
        }}>
        <TextCM style={{ fontSize: 20 }}>Login</TextCM>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

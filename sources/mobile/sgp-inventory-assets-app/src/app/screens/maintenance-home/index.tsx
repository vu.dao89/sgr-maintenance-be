import i18n from "@I18n";
import React, { FC, Fragment, useEffect } from "react";
import { Platform, StatusBar, TouchableOpacity, View } from "react-native";
import XFramework from "react-native-x-framework";

import {
  ICChecklist,
  ICEquipment,
  ICFuntionLocation,
  ICHome,
  ICNotification,
  ICOperation,
  ICShipment,
  ICWorkOrder,
} from "@Assets/image";
import { CMenusGroup, Header, TextCM } from "@Components";
import { Color, ScreenName } from "@Constants";
import { isEmpty } from "lodash";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../redux/actions";
import http from "../../../services/httpClient";
import { CacheUtil } from "../../../utils";
import { changeScreen } from "../../../utils/Screen";
import styles from "./styles";

type Props = {
  navigation: any;
};

const MaintenanceHomeScreen: FC<Props> = props => {
  const dispatch = useDispatch();

  if (Platform.OS === "android") {
    StatusBar.setBackgroundColor("transparent", true);
    StatusBar.setTranslucent(true);
  }

  const closeMiniApp = () => {
    XFramework.closeApp();
  };

  useEffect(() => {
    handleInitMiniApp();
  }, []);

  const handleInitMiniApp = async () => {
    const token_cache = (await CacheUtil.getToken()) + "";
    http.setAuthorizationHeader(token_cache);
    if (isEmpty(token_cache)) {
      closeMiniApp();
    } else {
      dispatch(CommonActions.getPermissionAsyncAction.request({}));
    }
  };

  return (
    <Fragment>
      <View style={styles.container}>
        <StatusBar barStyle={"light-content"} translucent backgroundColor="transparent" />

        <Header
          style={{ zIndex: 10 }}
          title={i18n.t("MH.Title")}
          componentLeft={
            <TouchableOpacity style={styles.icHome} onPress={closeMiniApp} hitSlop={{ top: 5, left: 5, bottom: 5 }}>
              <ICHome />
            </TouchableOpacity>
          }
        />

        <View style={styles.menuGroup}>
          <CMenusGroup
            column={3}
            title="Dữ liệu dùng chung"
            menus={[
              {
                id: "propertyInformation",
                label: "Thông tin tài sản",
                icon: <ICChecklist />,
                onPress: () => changeScreen(props, ScreenName.Home),
              },
              {
                id: "equipment",
                label: "Thiết bị",
                icon: <ICEquipment />,
                onPress: () => changeScreen(props, ScreenName.Equipment),
              },
              {
                id: "functionLocation",
                label: "Khu vực chức năng",
                icon: <ICFuntionLocation />,
                onPress: () => null,
              },
            ]}
          />
        </View>
        <View style={styles.menuGroup}>
          <CMenusGroup
            column={1}
            title="Quản lý tài sản"
            menus={[
              {
                id: "inventory",
                label: "Kiểm kê tài sản",
                icon: <ICShipment />,
                onPress: () => changeScreen(props, ScreenName.InventoryAsset),
              },
            ]}
          />
        </View>

        <View style={styles.menuGroup}>
          <CMenusGroup
            column={3}
            title=" Quản lý bảo trì"
            menus={[
              {
                id: "notification",
                label: "Thông báo",
                icon: <ICNotification />,
                onPress: () => changeScreen(props, ScreenName.NotificationList),
              },
              {
                id: "workOrder",
                label: "Lệnh bảo trì",
                icon: <ICWorkOrder />,
                onPress: () => changeScreen(props, ScreenName.WorkOrderList),
              },
              {
                id: "operation",
                label: "Công việc",
                icon: <ICOperation />,
                onPress: () => changeScreen(props, ScreenName.OperationList),
              },
            ]}
          />
        </View>

        <View style={{ flex: 1 }} />

        <TextCM
          style={{
            fontSize: 12,
            textAlign: "center",
            paddingBottom: 24,
            color: Color.White,
            marginTop: 10,
          }}>
          Version 0.0.1 (08-Feb-2023-V2)
        </TextCM>
      </View>
    </Fragment>
  );
};

export default MaintenanceHomeScreen;

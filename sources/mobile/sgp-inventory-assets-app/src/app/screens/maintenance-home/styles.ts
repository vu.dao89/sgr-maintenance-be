import { Color, widthResponsive as W } from "@Constants";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  icHome: {
    position: "absolute",
    left: W(8),
    width: W(30),
  },
  menuGroup: {
    width: "100%",
    paddingHorizontal: 24,
    marginTop: W(20),
  },
});

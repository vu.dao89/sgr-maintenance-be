import { get } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { useToast } from "react-native-toast-notifications";
import { useSelector } from "react-redux";

import i18n from "@I18n";

import {
  DocumentServices,
  EquipmentServices,
  MasterDataServices,
  NotificationCreateParams,
  NotificationServices,
  PermissionServices,
} from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { AppState } from "src/redux/reducers";

import { genRandomString, isArray, miniTimer } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsGetDate } from "src/utils/DateTime";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatPriorityLabel,
  formatWorkCenterLabel,
  genAttachFiles,
  mappingSelections,
} from "src/utils/Format";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import TabViewRoutes from "./tabs";

import { CAlertModal, CEmptyState, CNavbar, CScanQRCodeModal, CSpinkit, CTAButtons, CTabViewCreate } from "@Components";

import { ICBackSVG, ICCloseY, ICGhost, ICWarningOctagon } from "@Assets/image";
import { Permission } from "../../../../constants";
import { CFile } from "../../../components/common/attach-files";

const relatatedOptions: any = {
  maintPlant: "workCenter",
  onlyWorkingPerson: "personnel",
};

const NotificationCreate: FC<any> = props => {
  const isMounted = useIsMounted();

  const toast = useToast();
  const reqIds: any = useRef({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const [employeeCode, setEmployeeCode] = useState(PermissionServices.getEmployeeCode(reducer));

  const dataId = useState(getScreenParams(props).dataId || "")[0];
  const isEdit = useState(!!dataId)[0];
  const [dataReady, setDataReady] = useState(!isEdit);
  const [dataValid, setDataValid] = useState(!isEdit);
  const havePermission = isEdit
    ? PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.CHANGE)
    : PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.POST);

  const ctaButtonTxts = ["next", "next", isEdit ? "edit" : "create"];

  const [errors, setErrors] = useStateLazy({});
  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});
  const [tabIdx, setTabIdx] = useState(0);
  const [state, setState]: any = useStateLazy({
    onlyWorkingPerson: true,
    breakDown: true,
    items: [],
  });

  const [isScanError, setScanError] = useState(false);
  const [isScanData, setScanDataStatus] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  useEffect(() => {
    if (havePermission) {
      const equipment = get(getScreenParams(props), "equipment");
      if (equipment) handleSelectedEquipment(equipment);

      isEdit && handleGetEditData();
    }
    // global['notification_crud_cb']();
  }, []);

  const handleSelectedEquipment = (equipment: any) => {
    const {
      equipmentId,
      description: equipmentDesc,
      maintenanceWorkCenter,
      functionalLocation,
      maintenancePlant,
    } = equipment;

    let _noti = {};

    if (equipmentId && equipmentDesc) {
      Object.assign(_noti, {
        ..._noti,
        equipment: { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDesc, true) },
      });
    }

    if (maintenanceWorkCenter) {
      const { code: workCenterCode, description } = maintenanceWorkCenter;
      const { code: plantCode, plantName } = maintenancePlant;

      Object.assign(_noti, {
        ..._noti,
        workCenter: {
          value: workCenterCode,
          label: formatWorkCenterLabel(workCenterCode, plantCode, description),
          maintPlant: { value: plantCode, label: formatObjectLabel(plantCode, plantName) },
        },
      });
    }

    if (maintenancePlant) {
      const { code, plantName } = maintenancePlant;

      Object.assign(_noti, {
        ..._noti,
        maintPlant: { value: code, label: formatObjectLabel(code, plantName, true) },
      });
    }

    if (functionalLocation) {
      const { functionalLocationId, description } = functionalLocation;

      Object.assign(_noti, {
        ..._noti,
        functionalLocation: {
          value: functionalLocationId,
          label: formatObjectLabel(functionalLocationId, description, true),
        },
      });
    }

    setState(_noti);
  };

  const handleGetEditData = async () => {
    const { response } = await axiosHandler(() => NotificationServices.getDetailNotification(dataId), { delay: true });
    if (!isMounted()) return;
    const data = get(response, "data", {} as any);
    // console.log(`🚀 Kds: handleGetEditData -> dataId, data:`, dataId, data);
    setDataReady(true);
    if (data) {
      let { header: detail, activities: _activities, notificationDocuments: _documents } = data;

      let detailState: any = {};
      const {
        id,
        desc,
        type,
        typeDesc,
        lText,
        maintenancePlant,
        maintenancePlantDesc,
        workCenter,
        workCenterDesc,
        functionalLocationId,
        functionalLocationDesc,
        equipmentId,
        equipmentDesc,
        priorityTypeObject,
        priority,
        priorityType,
        assignTo,
        assignName,
        date,
        time,
        requestStart,
        requestStartTime,
        requestEnd,
        requestEndTime,
        breakDown,
        malfunctionStart,
        malfunctionStartTime,
        malfunctionEnd,
        malfunctionEndTime,
        reportBy,
      } = detail;

      setEmployeeCode(reportBy);

      const { response } = await axiosHandler(() => MasterDataServices.getNotificationType(type, maintenancePlant));

      let priorityDesc = get(priorityTypeObject, "description", "");
      let notiType = get(response, "data", {});

      Object.assign(detailState, {
        notificationId: id,
        // section 1
        description: desc || "",
        notificationType: {
          value: type,
          label: formatObjectLabel(type, typeDesc),
          ...notiType,
        },
        notificationLongText: lText,
        maintPlant: { value: maintenancePlant, label: formatObjectLabel(maintenancePlant, maintenancePlantDesc) },
        workCenter: workCenter ? { value: workCenter, label: formatObjectLabel(workCenter, workCenterDesc) } : null,
        functionalLocation: {
          value: functionalLocationId,
          label: formatObjectLabel(functionalLocationId, functionalLocationDesc),
        },
        equipment: { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDesc, true) },
        priority: { value: priority, label: formatPriorityLabel(priorityType, priority, priorityDesc) },
        personnel: { value: assignTo, label: formatObjectLabel(assignTo, assignName) },
        // section 2
        notificationTime: dayjsGetDate(date + time),
        requestStart: dayjsGetDate(requestStart + requestStartTime),
        requestEnd: dayjsGetDate(requestEnd + requestEndTime),
        // section 3
        breakDown: breakDown === "X",
        malfunctionStart: malfunctionStart ? dayjsGetDate(malfunctionStart, malfunctionStartTime) : null,
        malfunctionEnd: malfunctionEnd ? dayjsGetDate(malfunctionEnd, malfunctionEndTime) : null,
        // others
        items: [],
        attachFiles: genAttachFiles(_documents),
      });

      let _items = get(data, "items.notificationItems", []);
      if (isArray(_items, true)) {
        detailState.items = _items.map((_item: any) => {
          const {
            Id,
            item,
            part,
            partDesc,
            partCode,
            partCodeDesc,
            problem,
            problemDesc,
            dame,
            dameDesc,
            idText,
            cause,
            causeDesc,
            causeCode,
            causeCodeDesc,
          } = _item;

          return {
            id: Id,
            item,
            uuid: genRandomString(),
            notifPart: part ? { value: part, label: partDesc } : null,
            notifPartCode: partCode ? { value: partCode, label: partCodeDesc } : null,
            notifProblem: problem ? { value: problem, label: problemDesc } : null,
            notifDame: dame ? { value: dame, label: dameDesc } : null,
            notifText: idText || "",
            notifCause: cause ? { value: cause, label: causeDesc } : null,
            notifCauseCode: causeCode ? { value: causeCode, label: causeCodeDesc } : null,
            notifCauseText: "", // missing notifCauseText
          };
        });
      }

      setState(detailState);
      setDataValid(true);
    }
  };

  const handleChangeTab = (targetIdx: any) => setTabIdx(tabIdx + targetIdx);

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "cleanSelections":
        {
          setOptions((prevState: any) => ({ ...prevState, [data]: [] }));
        }
        break;
      case "":
      default:
        {
          let nextState: any = { [id]: data },
            nextOptsState: any = {};
          let relatatedOptionDataKey = relatatedOptions[id];
          if (relatatedOptionDataKey && !!data) {
            nextState[relatatedOptionDataKey] = null;
            nextOptsState[relatatedOptionDataKey] = [];
          }
          switch (id) {
            case "workCenter":
              {
                if (!!data && !state.maintPlant) {
                  nextState.maintPlant = data.maintPlant;
                }
              }
              break;
            case "notificationTime":
              {
                if (!!data) {
                  if (!state.requestStart) nextState.requestStart = data;
                  if (!state.malfunctionStart) nextState.malfunctionStart = data;
                }
              }
              break;
            case "breakDown":
              if (!!data && !state.malfunctionStart && state.notificationTime) {
                nextState.malfunctionStart = state.notificationTime;
              }
              break;
            case "equipment":
              {
                if (!!data && !state.functionalLocation) {
                  nextState.functionalLocation = data.functionalLocation;
                }
              }
              break;
            default:
              break;
          }
          setState((prevState: any) => ({ ...prevState, ...nextState }));
          setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
          setErrors((prevState: any) => ({ ...prevState, [id]: false }));
        }
        break;
    }
  };

  const handleSearch = (id: any, relatedData: any) => async (keyword: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];
    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(state.maintPlant ? [state.maintPlant] : [], userPlants),
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "notificationType":
        {
          searchServiceApi = MasterDataServices.getNotificationTypes;
          mappingOptions = {
            valueKey: "notiType",
            format: (i: any) => ({
              priorityTypeId: i.priorityTypeId,
              catalogProfile: i.catalogProfile,
              objectParts: i.objectParts,
              problems: i.problems,
              causes: i.causes,
            }),
          };
        }
        break;
      case "maintPlant":
        {
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenter: i.costCenter,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "functionalLocation":
        {
          searchServiceApi = MasterDataServices.getFunctionalLocations;
          mappingOptions = { valueKey: "functionalLocationId" };
        }
        break;
      case "equipment":
        {
          if (state.functionalLocation) params.functionalLocationCodes = [state.functionalLocation.value];
          searchServiceApi = EquipmentServices.getEquipments;
          mappingOptions = {
            valueKey: "equipmentId",
            format: (i: any) => ({
              label: formatObjectLabel(i.equipmentId, i.description, true),
              functionalLocation: {
                label: formatObjectLabel(i.functionalLocationId, i.functionalLocation?.description),
                value: i.functionalLocationId,
              },
            }),
          };
        }
        break;
      case "priority":
        {
          let priorityType = get(state, "notificationType.priorityTypeId", "");
          params.types = [priorityType];
          searchServiceApi = MasterDataServices.getPriorities;
          mappingOptions = {
            valueKey: "priority",
            format: (i: any) => ({ label: formatPriorityLabel(i.type, i.priority, i.description) }),
          };
        }
        break;
      case "personnel":
        {
          if (state.onlyWorkingPerson) params.workCenterIds = convertFilterMultiValue([state.workCenter]);
          searchServiceApi = MasterDataServices.getPersonnel;
          mappingOptions = { labelKey: "name" };
        }
        break;
      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));
    // console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    await miniTimer(500); // TEMP, fake loading API

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const handleSearchScanResult = async (qrCode: any) => {
    setScanDataStatus(false);
    await setFetchings((prevState: any) => ({ ...prevState, scan: true }));

    const { response, error } = await axiosHandler(() => MasterDataServices.getByQRCode(qrCode), { delay: true });
    // console.log(`🚀 Kds: handleSearchScanResult -> qrCode, response, error`, qrCode, response, error);

    if (!isMounted()) return;

    await setFetchings((prevState: any) => ({ ...prevState, scan: false }));

    const { equipment, functionalLocation } = get(response, "data", {});

    if (!equipment && !functionalLocation) return setScanError(true);

    switch (true) {
      case !!functionalLocation:
        {
          let functionalLocationSelection = mappingSelections([functionalLocation], {
            valueKey: "functionalLocationId",
          })[0];
          handleChange("functionalLocation", functionalLocationSelection);
        }
        break;
      case !!equipment:
        {
          const { functionalLocation: _functionalLocation, functionalLocationId } = equipment;
          let functionalLocationSelection = _functionalLocation
            ? mappingSelections([_functionalLocation], { valueKey: "functionalLocationId" })[0]
            : { value: functionalLocationId, label: functionalLocationId };
          let equipmentSelection = mappingSelections([equipment], { valueKey: "equipmentId" })[0];
          handleChange("functionalLocation", functionalLocationSelection);
          handleChange("equipment", equipmentSelection);
        }
        break;
      default:
        break;
    }
  };

  const handleOnCTA = async () => {
    const {
      // section 1
      description,
      notificationType,
      notificationLongText,
      maintPlant,
      workCenter,
      functionalLocation,
      equipment,
      priority,
      personnel,
      // section 2
      notificationTime,
      requestStart,
      requestEnd,
      // section 3
      breakDown,
      malfunctionStart,
      malfunctionEnd,
      // others
      items = [],
      attachFiles = [],
    } = state;
    switch (tabIdx) {
      case 0:
        {
          let isValidData = true,
            _errors: any = {},
            focusInputField: any = null,
            setInvalid = (id: any) => {
              isValidData = false;
              _errors[id] = true;
              if (!focusInputField) focusInputField = id;
            };

          if (description === "test-khoinm") return handleChangeTab(1);

          const arrayValidate = [
            "description",
            "notificationType",
            "maintPlant",
            "workCenter",
            "priority",
            "personnel",
            // section 2
            "notificationTime",
            "requestStart",
            "requestEnd",
          ];
          if (breakDown) {
            arrayValidate.push("malfunctionStart");
          }
          arrayValidate.forEach((requiredField: any) => {
            if (!state[requiredField]) setInvalid(requiredField);
          });

          if (!state["equipment"] && !state["functionalLocation"]) {
            setInvalid("equipment");
            setInvalid("functionalLocation");
          }

          let wrongNotiDateTime = false;
          if (notificationTime) {
            if (requestStart && +notificationTime > +requestStart) {
              wrongNotiDateTime = true;
              setInvalid("requestStart");
            }
            if (requestEnd && +notificationTime > +requestEnd) {
              wrongNotiDateTime = true;
              setInvalid("requestEnd");
            }
            if (requestEnd && requestStart && requestStart > requestEnd) {
              wrongNotiDateTime = true;
              setInvalid("requestStart");
              setInvalid("requestEnd");
            }

            if (wrongNotiDateTime) toast.show("Thời gian yêu cầu xử lý không hợp lệ!", { type: "danger" });
          }

          let wrongBreakDown = breakDown && malfunctionStart && malfunctionEnd && malfunctionStart > malfunctionEnd;
          if (wrongBreakDown) {
            toast.show("Ngày dừng vận hành không hợp lệ!", { type: "danger" });
            setInvalid("malfunctionStart");
            setInvalid("malfunctionEnd");
          }

          setErrors(_errors);
          if (isValidData) {
            handleChangeTab(1);
          } else {
            if (wrongNotiDateTime || wrongBreakDown) return;
            toast.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
          }
        }
        break;
      case 1:
        {
          // TODO: handle validate before can go next tab
          handleChangeTab(1);
        }
        break;
      default:
        {
          await setFetchings((prevState: any) => ({ ...prevState, crud: true }));

          let documentIds: any = attachFiles?.filter((f: CFile) => f.isRemote)?.map((f: CFile) => f.uuid) || [];
          const uploadFiles = attachFiles?.filter((f: CFile) => !f.isRemote) || [];

          if (isArray(uploadFiles, true)) {
            const { response, error } = await axiosHandler(() => DocumentServices.uploadDocuments(uploadFiles));

            if (!isMounted()) return;

            const fileData = get(response, "data");
            if (isArray(fileData, 1)) {
              fileData.forEach((element: any) => {
                if (element.id) documentIds.push(element.id);
              });
            }

            if (error) {
              toast.show(getErrorDetail(error).errorMessage, { type: "warning" });
            }
          }

          const params = {
            id: dataId,
            title: description,
            notificationType: notificationType?.value,
            notificationLText: notificationLongText || "",
            mainPlant: maintPlant?.value,
            workCenter: workCenter?.value || "",
            functionalLocation: functionalLocation?.value,
            equipment: equipment?.value,
            priority: priority?.value,
            priorityType: notificationType?.priorityTypeId,
            personel: personnel?.value,
            discoveredDate: notificationTime,
            requestProcessingDate: requestStart,
            processingTillDate: requestEnd,
            isStopOperating: breakDown,
            stopOperatingFromDate: malfunctionStart,
            stopOperatingToDate: malfunctionEnd,
            items: items.map((_item: any) => {
              const {
                item,
                notifPart,
                notifPartCode,
                notifProblem,
                notifDame,
                notifText,
                notifCause,
                notifCauseText,
                notifCauseCode,
              } = _item;
              return {
                id: dataId,
                item: item,
                notifPart: notifPart?.value || "",
                notifPartCode: notifPartCode?.value || "",
                notifProblem: notifProblem?.value || "",
                notifDame: notifDame?.value || "",
                notifText: notifText || "",
                notifCause: notifCause?.value || "",
                notifCauseCode: notifCauseCode?.value || "",
                notifCauseText: notifCauseText || "",
              };
            }),
            documentIds,
            reportBy: employeeCode,
          } as NotificationCreateParams;

          // console.log(`🚀 Kds: handleOnCTA -> params`, params);

          const { response, error } = await axiosHandler(() =>
            (isEdit ? NotificationServices.changeNotificaiton : NotificationServices.postNotificaiton)(params)
          );
          if (!isMounted()) return;
          //console.log(`🚀 Kds: handleOnCTA -> response, error`, response, error);
          await setFetchings((prevState: any) => ({ ...prevState, crud: false }));

          if (error) {
            toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
          } else {
            if (isEdit) {
              toast.show(`Chỉnh sửa thông báo thành công!`, { type: "success" });
            } else {
              const document = get(response, "data.document");
              toast.show(`Tạo thông báo ${document} thành công!`, { type: "success" });
            }

            handleCallback();
          }
        }
        break;
    }
  };

  const handleCallback = () => {
    const callback = getScreenParams(props).callback || "notification_list_cb";
    // @ts-ignore
    (global[callback] || (() => null))();
    changeScreen(props);
  };

  const _renderHeader = () => {
    let isFirstTab = !tabIdx;
    return (
      <CNavbar
        title={`${isEdit ? "Chỉnh sửa" : "Tạo"} thông báo`}
        leftIcon={isFirstTab ? <ICCloseY /> : <ICBackSVG />}
        onLeftPress={() => (isFirstTab ? setShowConfirm(true) : handleChangeTab(-1))}
      />
    );
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isScanData}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setScanDataStatus(false)}
          onResult={handleSearchScanResult}
        />
        <CAlertModal
          open={isScanError}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => setScanError(false)}
        />
      </Fragment>
    );
  };

  const _renderBody = () => {
    return (
      havePermission && (
        <>
          {(!dataReady && (
            <View style={styles.bodyCenter}>
              <CSpinkit />
            </View>
          )) ||
            (isEdit && !dataValid && (
              <View style={styles.bodyCenter}>
                <CEmptyState />
              </View>
            )) || (
              <Fragment>
                <CTabViewCreate
                  {...{
                    isEdit,
                    toast,
                    state,
                    errors,
                    fetchings,
                    options,
                  }}
                  index={tabIdx}
                  routes={TabViewRoutes}
                  onChange={handleChange}
                  onSearch={handleSearch}
                  onScan={() => setScanDataStatus(true)}
                />

                <CTAButtons
                  isSticky
                  buttons={[
                    ...(!!tabIdx
                      ? [
                          {
                            isSub: true,
                            text: i18n.t("CM.back"),
                            onPress: () => handleChangeTab(-1),
                          },
                        ]
                      : []),
                    {
                      text: i18n.t(`CM.${ctaButtonTxts[tabIdx]}`),
                      onPress: handleOnCTA,
                    },
                  ]}
                />
              </Fragment>
            )}
        </>
      )
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {_renderBody()}
      {_renderScanQRCode()}
      <CAlertModal
        open={showConfirm}
        title={"Bạn có chắc chắn muốn thoát không?"}
        icon={<ICWarningOctagon />}
        onClose={() => setShowConfirm(false)}
        onCta={() => {
          setShowConfirm(false);
          changeScreen(props);
        }}
      />

      {!havePermission && <CEmptyState noPermission={!havePermission} />}

      {!!(fetchings.crud || fetchings.scan) && (
        <View style={styles.fetchingDataOverlay}>
          <CSpinkit />
        </View>
      )}
    </View>
  );
};

export default NotificationCreate;

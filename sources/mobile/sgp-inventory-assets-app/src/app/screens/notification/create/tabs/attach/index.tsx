import React, { FC } from "react";
import { ScrollView } from "react-native";

import styles from "./styles";

import { Permission, widthResponsive as WR } from "@Constants";

import { CAttachFiles, CEmptyState, TextCM } from "@Components";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices } from "../../../../../../services";
import { isArray } from "../../../../../../utils/Basic";

const NCTabAttach: FC<any> = ({ saInsets, title: tabTitle, state, onChange }) => {
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.ATTACHMENT_POST);

  const { attachFiles } = state;

  return (
    <ScrollView contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>3. Đính kèm tài liệu</TextCM>

      {!havePermission && !isArray(attachFiles, 1) && <CEmptyState noPermission={!havePermission} />}

      <CAttachFiles
        canRemove
        files={attachFiles}
        style={{ marginTop: WR(20) }}
        onChange={havePermission && ((files: any) => onChange("attachFiles", files))}
      />
    </ScrollView>
  );
};

export default NCTabAttach;

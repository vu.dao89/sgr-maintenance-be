import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
});

import { default as NCTabAttach } from "./attach";
import { default as NCTabInfo } from "./info";
import { default as NCTabItem } from "./item";

export default [
  {
    key: "0",
    Scene: NCTabInfo,
  },
  {
    key: "1",
    Scene: NCTabItem,
  },
  {
    key: "2",
    Scene: NCTabAttach,
  },
];

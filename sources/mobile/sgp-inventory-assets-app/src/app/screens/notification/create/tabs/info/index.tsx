import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CCheckBox, CCollapsibleSection, CDatePicker, CSelect, CSwitch, CTextInput, TextCM } from "@Components";

import { ICQRC } from "@Assets/image";

const NCTabInfo: FC<any> = ({
  isEdit,
  saInsets,
  toast,
  state,
  errors,
  fetchings,
  options,
  onChange,
  onSearch,
  onScan,
}) => {
  const { notificationType, workCenter, breakDown } = state;

  const forms = [
    {
      collapsible: false,
      fields: [
        {
          type: "input",
          id: "description",
          label: "Tiêu đề",
          plh: "Nhập tiêu đề",
          maxLength: 40,
          isRequired: true,
        },
        {
          type: "select",
          id: "notificationType",
          label: "Phân loại thông báo",
          isRequired: true,
          isDisabled: isEdit,
        },
        {
          type: "input",
          id: "notificationLongText",
          label: "Mô tả thông báo",
          plh: "Nhập mô tả thông báo",
          multiline: 2,
          maxLength: 5000,
        },
        {
          type: "select",
          id: "maintPlant",
          label: "Nơi bảo trì",
          isRequired: true,
        },
        {
          type: "select",
          id: "workCenter",
          label: "Tổ đội thực hiện",
          isRequired: true,
        },
        {
          type: "select",
          id: "functionalLocation",
          label: "Khu vực chức năng",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "equipment",
          label: "Mã thiết bị",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "priority",
          label: "Mức độ ưu tiên",
          isRequired: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!notificationType) toast.show("Vui lòng chọn Phân loại thông báo", { type: "warning" });
            return !!notificationType;
          },
        },
        {
          type: "select",
          id: "personnel",
          label: "Thông báo tới",
          isRequired: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!workCenter) toast.show("Vui lòng chọn Tổ đội thực hiện", { type: "warning" });
            return !!workCenter;
          },
        },
        {
          type: "checkbox",
          id: "onlyWorkingPerson",
          label: "Chỉ tìm người đang trực",
        },
      ],
    },
    {
      title: "Thời gian yêu cầu xử lý",
      fields: [
        {
          type: "datetime",
          id: "notificationTime",
          label: "Ngày phát hiện sự cố",
          maximumDate: new Date(),
          isRequired: true,
        },
        {
          type: "datetime",
          id: "requestStart",
          label: "Yêu cầu xử vào ngày",
          isRequired: true,
        },
        {
          type: "datetime",
          id: "requestEnd",
          label: "Đến ngày",
          isRequired: true,
        },
      ],
    },
    {
      collapsible: false,
      fields: breakDown
        ? [
            {
              type: "switch",
              id: "breakDown",
              label: "Dừng vận hành",
            },
            {
              type: "datetime",
              id: "malfunctionStart",
              isRequired: true,
              label: "Từ ngày",
            },
            {
              type: "datetime",
              id: "malfunctionEnd",
              label: "Đến ngày",
            },
          ]
        : [
            {
              type: "switch",
              id: "breakDown",
              label: "Dừng vận hành",
            },
          ],
    },
  ];

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>1. {`${isEdit ? "Chỉnh sửa" : "Nhập"} thông tin chính`}</TextCM>

      {forms.map((item: any, index: any) => {
        const { fields, ...section } = item;
        return (
          <CCollapsibleSection key={index} {...section}>
            {fields.map((field: any) => {
              const { type, id, isSearch, canScan, ...fieldProps } = field;
              let _fieldProps = {
                key: id,
                isError: errors[id],
                value: state[id],
                onChange: (data: any) => onChange(id, data),
                ...fieldProps,
              };
              switch (type) {
                case "input":
                  return <CTextInput {..._fieldProps} />;
                case "datetime":
                  return <CDatePicker {..._fieldProps} mode={type} />;
                case "select": {
                  let selectInputNode = (
                    <CSelect
                      {..._fieldProps}
                      isFullScreen
                      isFetching={fetchings[id]}
                      selected={state[id]}
                      menus={options[id]}
                      styles={canScan && { wrapper: { flex: 1, marginRight: WR(20) } }}
                      onSelect={(selected: any) => onChange(id, selected)}
                      onSearch={onSearch(id)}
                      {...(!isSearch && { onOpen: onSearch(id) })}
                    />
                  );
                  if (canScan) {
                    return (
                      <View key={id} style={styles.row}>
                        {selectInputNode}
                        <TouchableOpacity onPress={onScan}>
                          <ICQRC />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                  return selectInputNode;
                }
                case "checkbox": {
                  return (
                    <CCheckBox
                      {..._fieldProps}
                      checked={state[id]}
                      styles={{ wrapper: { marginTop: -WR(12), marginBottom: WR(20) } }}
                    />
                  );
                }
                case "switch": {
                  return <CSwitch {..._fieldProps} styles={{ wrapper: { marginBottom: WR(20) } }} />;
                }
                default:
                  return null;
              }
            })}
          </CCollapsibleSection>
        );
      })}
    </KeyboardAwareScrollView>
  );
};

export default NCTabInfo;

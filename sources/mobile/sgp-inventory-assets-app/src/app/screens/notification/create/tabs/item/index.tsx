import React, { FC, Fragment, useState } from "react";
import { ScrollView, TouchableOpacity } from "react-native";

import { genRandomString } from "src/utils/Basic";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, NotificationItemCreation, TextCM } from "@Components";
import { formatNotificationItemInfo } from "../../../../../../utils/Format";

const NCTabItem: FC<any> = ({ isEdit, saInsets, showTitle, state, onChange }) => {
  const { items = [] } = state;

  const [isCrud, setCrudStatus]: any = useState(false);
  const [itemAction, setItemAction]: any = useState("create");
  const [initState, setInitState]: any = useState({});

  const { notificationId, notificationType } = state;

  const handleNotificationItemChange = async (itemState: any) => {
    if (itemAction === "create") {
      items.push({
        ...itemState,
        uuid: genRandomString(),
        notifText: (itemState.notifText || "").trim(),
        notifCauseText: (itemState.notifCauseText || "").trim(),
      });
    } else {
      let itemIdx = items.findIndex((i: any) => i.uuid === itemState.uuid);
      Object.assign(items[itemIdx], {
        ...itemState,
        notifText: (itemState.notifText || "").trim(),
        notifCauseText: (itemState.notifCauseText || "").trim(),
      });
    }
    onChange("items", items);
    setCrudStatus(false);
  };

  return (
    <Fragment>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          {
            paddingVertical: WR(showTitle ? 30 : 20),
            paddingBottom: saInsets.bottom + (showTitle ? WR(44 + 24 + 8) : 0),
          },
        ]}>
        {!!showTitle && <TextCM style={styles.title}>2. {`${isEdit ? "Chỉnh sửa" : "Tạo"} notification item`}</TextCM>}

        <TouchableOpacity
          style={[styles.btnAddItem, { marginTop: showTitle ? WR(16) : 0 }]}
          onPress={() => {
            setInitState({});
            setItemAction("create");
            setCrudStatus(true);
          }}>
          <TextCM style={styles.btnAddTxt}>+ Thêm notification item</TextCM>
        </TouchableOpacity>

        {items.map((item: any, index: any) => {
          return (
            <CCollapsibleSection
              key={item.uuid}
              title={`Notification item ${index + 1}`}
              menus={[
                {
                  label: "Chỉnh sửa",
                  value: "edit",
                },
                {
                  label: "Xoá",
                  value: "delete",
                  color: Color.Red,
                },
              ]}
              infos={formatNotificationItemInfo(item)}
              onMenu={({ value: menuValue }: any) => {
                switch (menuValue) {
                  case "delete":
                    {
                      items.splice(index, 1);
                      onChange("items", items);
                    }
                    break;
                  case "edit":
                    setInitState(item);
                    setItemAction(menuValue);
                    setCrudStatus(true);
                    break;
                  default:
                    break;
                }
              }}
            />
          );
        })}
      </ScrollView>
      <NotificationItemCreation
        saInsets={saInsets}
        isCrud={isCrud}
        itemAction={itemAction}
        notificationId={notificationId || ""}
        notificationType={notificationType}
        initState={initState}
        onClose={() => setCrudStatus(false)}
        onChange={handleNotificationItemChange}
      />
    </Fragment>
  );
};

export default NCTabItem;

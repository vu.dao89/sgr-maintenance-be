import { get, isEmpty } from "lodash";
import React, { FC, Fragment, useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";
import { useToast } from "react-native-toast-notifications";

import i18n from "@I18n";

import { MasterDataServices, NotificationActivityParams, NotificationServices, PermissionServices } from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { isArray } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsFormatDate } from "src/utils/DateTime";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import { Permission, ScreenName, widthResponsive as WR } from "@Constants";

import TabViewRoutes from "./tabs";

import {
  CAlertModal,
  CEmptyState,
  CNavbar,
  CPersonInfo,
  CSpinkit,
  CTAButtons,
  CTabViewDetail,
  CTags,
  ImageCache,
  NotificationActivitiesCreation,
  TextCM,
} from "@Components";

import { ICEdit2, ICGhost, ICMap, IMEQDefault } from "@Assets/image";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import SystemStatus from "../../../../constants/SystemStatus";
import { CommonActions } from "../../../../redux/actions";
import { AppState } from "../../../../redux/reducers";
import {
  formatNotificationItem,
  formatObjectLabel,
  genAttachFiles,
  removeLeadingZeros,
} from "../../../../utils/Format";

const NotificationDetail: FC<any> = props => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();

  const saInsets = useSafeAreaInsets();

  const [headerInfoMinHeight, setHeaderInfoMinHeight] = useState(0);
  const [tabViewMinHeight, setTabViewMinHeight] = useState(0);

  const [isFetching, setFetching] = useStateLazy(true);
  const [detail, setDetail]: any = useStateLazy(null);
  const [attachFiles, setAttachFiles] = useStateLazy([]);
  const [activities, setActivities] = useStateLazy([]);

  const [isOpenCreateActivities, setIsOpenCreateActivities] = useState(false);

  const [errorAlert, setErrorAlert] = useState({});

  const [items, setItems] = useState([]);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.VIEW);

  useEffect(() => {
    if (havePermission) {
      handleGetDetail();
    } else {
      setFetching(false);
    }
    // @ts-ignore
    global["notification_detail_cb"] = () => handleCallback();
    return () => {
      // @ts-ignore
      delete global["notification_detail_cb"];
    };
  }, []);

  const handleCallback = () => {
    const callback = getScreenParams(props).callback || "notification_list_cb";
    // @ts-ignore
    (global[callback] || (() => null))();
    handleGetDetail();
  };

  const handleGetNotificationType = async (type: string, maintenancePlantCode: string) => {
    const { response } = await axiosHandler(() => MasterDataServices.getNotificationType(type, maintenancePlantCode));
    return get(response, "data");
  };

  const handleGetDetail = async () => {
    const { dataId } = getScreenParams(props);

    if (!isFetching) await setFetching(true);

    const { response } = await axiosHandler(() => NotificationServices.getDetailNotification(dataId));

    if (!isMounted()) return;
    const data = get(response, "data");

    if (data) {
      let { header: detail, activities: _activities, notificationDocuments: documents } = data;
      if (detail) {
        // console.log(`🚀 Kds: handleGetDetail -> dataId, data`, dataId, data);
        const { id, desc, type, typeDesc, priorityTypeObject, commonStatus, equipmentStatId, maintenancePlant } =
          detail;

        const notificationType = await handleGetNotificationType(type, maintenancePlant);

        let prorityColorCode = get(priorityTypeObject, "colorCode", "#FD4848");
        let prorityLabel = get(priorityTypeObject, "description", "");
        let statusColorCode = get(commonStatus, "colorCode", "#FD4848");
        let statusLabel = get(commonStatus, "description", "");

        const tags = [
          { color: prorityColorCode, label: prorityLabel },
          { color: statusColorCode, label: statusLabel },
          { color: "#3A73B7", label: typeDesc },
        ];

        const canConfirmNoti = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.CONFIRM_COMPLETE);

        setDetail({
          ...detail,
          code: removeLeadingZeros(id),
          tags: tags,
          notificationType: notificationType,
          title: formatObjectLabel(desc, id, true),
          isFinish: canConfirmNoti ? equipmentStatId === SystemStatus.notification.COMPLETED : true,
        });
        setActivities(get(_activities, "notificationActivities", []));

        setAttachFiles(genAttachFiles(documents));
      }

      let _items = get(data, "items.notificationItems", []);
      if (isArray(_items, true)) {
        _items = _items.map((i: any) => formatNotificationItem(i));
        setItems(_items);
      }
    }

    setFetching(false);
  };

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "items":
        setItems(data);
        break;

      case "attachFiles":
        setAttachFiles([...data, ...attachFiles]);
        break;
    }
  };

  const handleCloseNotifcation = async () => {
    let notificationId = get(detail, "id", "");
    if (isEmpty(notificationId)) return;

    dispatch(CommonActions.openLoading.request({}));

    const { error } = await axiosHandler(() => NotificationServices.closeNotification(notificationId, []));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toast.show(`Đóng thông báo thành công!`, { type: "success" });
      handleCallback();
    }
  };

  const handleOnCTA = () => {
    if (isEmpty(get(detail, "order", ""))) {
      if (isEmpty(detail?.notificationType?.activities)) handleCloseNotifcation();
      else setIsOpenCreateActivities(true);
    } else {
      let workOrderStatus = get(detail, ["workOrder", "systemStatus"], "");

      if (workOrderStatus === "I0045") {
        handleCloseNotifcation();
      } else {
        setErrorAlert({
          isShowAlert: true,
          errorMessage: "Thông báo này có lệnh bảo trì chưa hoàn tất, vui lòng hoàn thành lệnh bảo trì",
          workOrderId: get(detail, ["workOrder", "id"], ""),
        });
      }
    }
  };

  const finishOpeartionWithActivitiesGroup = async (activitiesGroup: any) => {
    let notificationId = get(detail, "id", "");
    if (isEmpty(notificationId)) return;

    if (!activitiesGroup || activitiesGroup.length === 0) {
      toast.show("Vui lòng tạo ít nhất 1 khắc phục", { type: "danger" });
      return;
    }

    const _activites: Array<NotificationActivityParams> = [];
    for (const activity of activitiesGroup) {
      _activites.push({
        activity: activity.activity?.value,
        activityCode: activity.activityCode?.value,
        activityText: activity.activityText,
      });
    }

    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      NotificationServices.closeNotification(notificationId, _activites)
    );

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toast.show(`Đóng thông báo thành công!`, { type: "success" });
      handleCallback();
    }
  };

  const canEdit =
    !!detail &&
    PermissionServices.canEditNotification(reducer, detail?.equipmentStatId, detail?.reportBy, detail?.assignTo);

  const _renderHeader = () => {
    return (
      <CNavbar
        title={"Chi tiết thông báo"}
        onLeftPress={() => changeScreen(props)}
        {...(canEdit && {
          rightIcon: <ICEdit2 />,
          onRightPress: () =>
            changeScreen(props, ScreenName.NotificationEdit, { dataId: detail.id, callback: "notification_detail_cb" }),
        })}
      />
    );
  };

  const _renderInfo = () => {
    const {
      code,
      desc,
      requestStart,
      requestEnd,
      equipmentId,
      equipmentDesc,
      functionalLocationId,
      functionalLocationDesc,
      reportAvatar,
      reportByName,
      tags,
    } = detail;

    return (
      <View style={styles.infoWrapper} onLayout={e => setHeaderInfoMinHeight(e.nativeEvent.layout.height)}>
        <View style={styles.infoHeader}>
          <View style={styles.infoContent}>
            <TextCM style={styles.infoTitle}>{desc}</TextCM>

            <TextCM style={styles.infoId}>
              {code}{" "}
              <TextCM style={styles.infoDates}>
                {dayjsFormatDate(requestStart)} - {dayjsFormatDate(requestEnd)}
              </TextCM>
            </TextCM>

            <View style={styles.listItemFooter}>
              <CTags data={tags} style={styles.infoTags} />
            </View>
          </View>
          <ImageCache imageDefault={IMEQDefault} style={styles.imgEq} />
        </View>

        <View style={styles.equipmentNameContainer}>
          <View style={styles.equipmentNameWrapper}>
            <View style={styles.infoField}>
              <TextCM style={styles.infoLabel}>{`${i18n.t("N.equipment")}`}</TextCM>
              <TextCM style={styles.infoValue}>{formatObjectLabel(equipmentId, equipmentDesc, true)}</TextCM>
            </View>
          </View>
        </View>

        <TouchableOpacity style={styles.infoLocation}>
          <ICMap />
          <TextCM style={styles.infoLocationTxt}>
            {formatObjectLabel(functionalLocationId, functionalLocationDesc)}
          </TextCM>
        </TouchableOpacity>

        <CPersonInfo
          avatar={reportAvatar}
          fullName={reportByName}
          label={i18n.t("N.createdBy")}
          style={{ marginTop: WR(16) }}
        />
      </View>
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {(isFetching && (
        <View style={styles.bodyCenter}>
          <CSpinkit />
        </View>
      )) ||
        (!detail && <CEmptyState noPermission={!havePermission} />) || (
          <Fragment>
            <ScrollView
              contentContainerStyle={{
                flexGrow: 1,
                height: headerInfoMinHeight + tabViewMinHeight,
              }}>
              {_renderInfo()}
              <CTabViewDetail
                detail={detail}
                activities={activities}
                items={items}
                attachFiles={attachFiles}
                routes={TabViewRoutes}
                onChange={handleChange}
                onLayout={setTabViewMinHeight}
                onNavigate={(...args: any) => changeScreen(props, ...args)}
              />
            </ScrollView>
            {!get(detail, "isFinish", false) && (
              <CTAButtons
                isSticky
                buttons={[
                  {
                    text: "Hoàn thành",
                    onPress: handleOnCTA,
                  },
                ]}
              />
            )}
            <CAlertModal
              open={get(errorAlert, "isShowAlert", false)}
              title={get(errorAlert, "errorMessage", "")}
              icon={<ICGhost />}
              buttons={[
                {
                  id: "back",
                  text: "Quay lại",
                },
              ]}
              subButton={
                isEmpty(get(errorAlert, "workOrderId", "")) ? (
                  <></>
                ) : (
                  <TouchableOpacity
                    style={styles.btnWorkOrder}
                    onPress={() => {
                      setErrorAlert(false);
                      changeScreen(props, ScreenName.WorkOrderDetail, get(detail, "workOrder"));
                    }}>
                    <TextCM style={styles.btnWorkOrderTxt}>Xem lệnh bảo trì</TextCM>
                  </TouchableOpacity>
                )
              }
              onCta={() => {
                setErrorAlert(false);
              }}
              onClose={() => {
                setErrorAlert(false);
              }}
            />

            <NotificationActivitiesCreation
              title={`Hoàn tất thông báo ${detail?.title}`}
              saInsets={saInsets}
              isOpen={isOpenCreateActivities}
              notificationType={detail?.notificationType}
              onClose={() => setIsOpenCreateActivities(false)}
              onChange={(_activites: any) => {
                setIsOpenCreateActivities(false);
                finishOpeartionWithActivitiesGroup(_activites);
              }}
            />
          </Fragment>
        )}
    </View>
  );
};

export default NotificationDetail;

import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  wrapper: {
    flexDirection: "row",
    paddingHorizontal: WR(24),
  },
  stickyWrapper: {
    position: "absolute",
    zIndex: 1,
    bottom: 0,
    left: 0,
    right: 0,
  },
  stickyContainer: {
    width: "100%",
    backgroundColor: "#262745",
    paddingVertical: WR(12),
    borderTopLeftRadius: WR(20),
    borderTopRightRadius: WR(20),
  },
  bodyCenter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  dataEmptyTxt: {
    textAlign: "center",
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  infoWrapper: {
    paddingVertical: WR(8),
    paddingHorizontal: WR(24),
  },
  infoHeader: {
    flexDirection: "row",
  },
  infoContent: {
    flex: 1,
    marginRight: WR(16),
  },
  infoTitle: {
    marginBottom: WR(4),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
  },
  infoId: {
    marginRight: 8,
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  infoDates: {
    color: "#ACACAC",
    fontSize: FontSize.FontTiny,
  },
  infoTags: {
    marginTop: WR(8),
    marginBottom: WR(12),
  },
  infoLocation: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  infoLocationTxt: {
    flex: 1,
    marginLeft: WR(8),
    color: Color.Yellow,
  },
  infoField: {
    marginBottom: WR(16),
  },
  infoLabel: {
    marginBottom: WR(8),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  infoValue: {
    color: Color.White,
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  btnWorkOrder: {
    height: HR(56),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  btnWorkOrderTxt: {
    color: Color.Yellow,
  },
  equipmentNameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  equipmentNameWrapper: {
    flex: 2,
  },
  imgEq: {
    width: WR(80),
    height: HR(80),
    borderRadius: HR(20),
  },
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center"
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
});

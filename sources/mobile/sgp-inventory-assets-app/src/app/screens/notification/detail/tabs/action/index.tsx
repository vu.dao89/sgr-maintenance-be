import React, { FC, useEffect, useState } from "react";
import { TouchableOpacity, View } from "react-native";

import i18n from "@I18n";

import styles from "./styles";

import { Permission, ScreenName } from "@Constants";

import { CEmptyState, TextCM, WOListItem } from "@Components";
import { get, isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices, WorkOrderServices } from "../../../../../../services";
import { axiosHandler } from "../../../../../../services/httpClient";
import { useIsMounted } from "../../../../../../utils/Core";
import { dayjsFormatDate, dayjsTimePassedFromNow } from "../../../../../../utils/DateTime";
import { formatObjectLabel } from "../../../../../../utils/Format";

const NDTabAction: FC<any> = ({ saInsets, detail, onNavigate }) => {
  const isMounted = useIsMounted();

  const [workOrder, setWorkOrder]: any = useState(null);
  const [isFetching, setFetching]: any = useState(true);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.CREATE_WORK_ORDER);
  const canCreateWO = !isFetching && !workOrder && havePermission;

  useEffect(() => {
    let workOrderId = get(detail, "order");
    if (workOrderId) {
      handleGetWorkOrderDetail(workOrderId);
    } else {
      setFetching(false);
    }
  }, []);

  const handleGetWorkOrderDetail = async (workOrderId: string) => {
    const { response } = await axiosHandler(() => WorkOrderServices.getWorkOrderDetails(workOrderId));

    if (!isMounted()) return;

    const item = get(response, "data.workOrder");

    if (item) {
      const {
        workOrderDescription,
        equipmentId,
        equipmentDescription,
        mainPlant,
        maintenancePlantDescription,
        personnelName,
        woStartDate,
        woStartTime,
        woFinishDate,
        workOrderType,
        priorityText,
        workOrderSystemStatusDescription,
        workOrderSystemStatus,
      } = item;

      const _workOrder: any = {
        id: workOrderId,
        title: formatObjectLabel(workOrderId, workOrderDescription),
        equipment: {
          name: formatObjectLabel(equipmentId, equipmentDescription, true),
          image: "",
        },
        mainPlant: formatObjectLabel(mainPlant, maintenancePlantDescription),
        createdBy: personnelName,
        dateTxt: `${dayjsFormatDate(woStartDate)} - ${dayjsFormatDate(woFinishDate)}`,
        timePassedFromNow: dayjsTimePassedFromNow(`${woStartDate}${woStartTime}`),
        tags: [
          { color: "#3A73B7", label: priorityText },
          { color: "#3A73B7", label: workOrderType },
          { color: "#3A73B7", label: workOrderSystemStatusDescription },
        ].filter((t: any) => !isEmpty(t.label)),
        systemStatus: workOrderSystemStatus,
      };

      setWorkOrder(_workOrder);
      detail.workOrder = _workOrder;
    }
    setFetching(false);
  };

  const handleCreateWorkOrder = () => {
    onNavigate(ScreenName.WorkOrderCreate, {
      notification: { value: detail.id, label: detail.desc },
      callback: "notification_detail_cb",
    });
  };

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: saInsets.bottom },
        !!workOrder && { paddingTop: 0, paddingHorizontal: 0 },
      ]}>
      {workOrder && (
        <WOListItem
          key={workOrder.id}
          item={workOrder}
          onPress={() => onNavigate(ScreenName.WorkOrderDetail, { id: workOrder.id })}
        />
      )}
      {canCreateWO && (
        <TouchableOpacity style={styles.btnAddItem} onPress={() => handleCreateWorkOrder()}>
          <TextCM style={styles.btnAddTxt}>{i18n.t("N.addWorkOrder")}</TextCM>
        </TouchableOpacity>
      )}
      {!workOrder && !havePermission && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default NDTabAction;

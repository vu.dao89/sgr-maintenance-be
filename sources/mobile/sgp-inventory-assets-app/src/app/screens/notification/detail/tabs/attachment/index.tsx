import React, { FC } from "react";
import { View } from "react-native";

import styles from "./styles";

import { Permission, SystemStatus, widthResponsive as WR } from "@Constants";

import { CAttachFiles, CEmptyState } from "@Components";
import { get, isEmpty } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { AppState } from "../../../../../../redux/reducers";
import { DocumentServices, PermissionServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted } from "../../../../../../utils/Core";
import { CFile } from "../../../../../components/common/attach-files";

const NDTabAttachment: FC<any> = ({ saInsets, detail, attachFiles, onChange }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.ATTACHMENT_POST);
  const canUpload = havePermission && detail?.equipmentStatId != SystemStatus.notification.COMPLETED;

  const handleAttachFiles = async (files: Array<CFile>) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      DocumentServices.uploadNotificationDocuments(detail.id, files)
    );

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      onChange("attachFiles", files);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <View
      style={[
        styles.container,
        {
          paddingTop: WR(24),
          paddingBottom: get(detail, "isFinish", false) ? saInsets.bottom + WR(16) : saInsets.bottom + WR(100),
        },
      ]}>
      <CAttachFiles files={attachFiles} onChange={canUpload && ((files: Array<CFile>) => handleAttachFiles(files))} />
      {isEmpty(attachFiles) && !canUpload && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default NDTabAttachment;

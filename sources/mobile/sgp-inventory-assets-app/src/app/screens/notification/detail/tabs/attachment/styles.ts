import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingHorizontal: WR(24),
  },
  dataEmptyTxt: {
    color: Color.White,
  },
});

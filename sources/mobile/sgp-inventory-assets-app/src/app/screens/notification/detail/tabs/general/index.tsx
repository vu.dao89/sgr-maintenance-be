import React, { FC } from "react";

import { View } from "react-native";

import styles from "./styles";

import { CCollapsibleSection, CTextInput, MapOnlyView, TextCM } from "@Components";

import i18n from "@I18n";

import { widthResponsive as WR } from "@Constants";

import { get } from "lodash";
import { formatSapDateTime } from "../../../../../../utils/DateTime";
import { formatObjectLabel, formatWorkCenterLabel } from "../../../../../../utils/Format";

const NDTabGeneral: FC<any> = ({ saInsets, detail, activities }) => {
  const _renderMalf = () => {
    const { breakDown, malfunctionStart, malfunctionStartTime, malfunctionEnd, malfunctionEndTime } = detail;

    if (breakDown === "X") {
      return (
        <CCollapsibleSection
          title="Thời gian dừng vận hành"
          infos={[
            {
              label: i18n.t("N.maifStart"),
              value: formatSapDateTime(malfunctionStart, malfunctionStartTime),
            },
            {
              label: i18n.t("N.maifEnd"),
              value: formatSapDateTime(malfunctionEnd, malfunctionEndTime),
            },
          ]}
        />
      );
    }
    return <CCollapsibleSection title="Không có dừng vận hành" />;
  };

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: get(detail, "isFinish", false) ? saInsets.bottom : saInsets.bottom + WR(72) },
      ]}>
      <TextCM style={styles.txtTabTitle}>Vị trí</TextCM>
      <View pointerEvents="none" style={styles.ctnMap}>
        <MapOnlyView
          style={styles.map}
          region={{
            latitude: 15.9783846,
            longitude: 108.2598785,
          }}
        />
      </View>
      <CCollapsibleSection
        title={i18n.t("N.generalDetail")}
        styles={{ wrapper: { marginTop: 0 } }}
        infos={[
          {
            label: i18n.t("N.mainPlant"),
            value: formatObjectLabel(detail.maintenancePlant, detail.maintenancePlantDesc),
          },
          {
            label: i18n.t("N.workCenter"),
            value: formatWorkCenterLabel(detail.workCenter, detail.maintenancePlant, detail.workCenterDesc),
          },
          {
            label: i18n.t("N.detail"),
            value: detail.lText,
          },
          {
            label: i18n.t("N.personel"),
            value: formatObjectLabel(detail.assignTo, detail.assignName),
          },
        ]}
      />

      <CCollapsibleSection
        title="Thời gian yêu cầu xử lý"
        infos={[
          {
            label: i18n.t("N.notifDate"),
            value: formatSapDateTime(detail.date, detail.time),
          },
          {
            label: i18n.t("N.reqStart"),
            value: formatSapDateTime(detail.requestStart, detail.requestStartTime),
          },
          {
            label: i18n.t("N.reqEnd"),
            value: formatSapDateTime(detail.requestEnd, detail.requestEndTime),
          },
        ]}
      />

      {_renderMalf()}

      {activities && activities.length > 0 ? (
        <>
          {activities.map(
            ({ activity, activityDesc, activityCode, activityCodeDesc, activityText }: any, index: any) => (
              <CCollapsibleSection
                key={`activity_${index}`}
                title={`${i18n.t("N.action")} ${activities.length === 1 ? "" : `${index + 1}`}`}
                infos={[
                  {
                    label: i18n.t("N.groupActivites"),
                    value: formatObjectLabel(activity, activityDesc),
                  },
                  {
                    label: i18n.t("N.detailActivites"),
                    value: formatObjectLabel(activityCode, activityCodeDesc),
                  },
                ]}
                children={
                  <CTextInput
                    multiline={3}
                    styles={{ wrapper: { marginTop: -WR(12) } }}
                    label={i18n.t("N.desDamageClassification")}
                    plh={i18n.t("CM.inputDes")}
                    value={activityText}
                    onChange={() => null}
                  />
                }
              />
            )
          )}
        </>
      ) : null}
    </View>
  );
};

export default NDTabGeneral;

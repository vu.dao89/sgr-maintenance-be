import { StyleSheet } from "react-native";

import { Color, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    padding: WR(24),
  },
  ctaButton: {
    height: HR(36),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#4C8BFF",
    marginBottom: HR(10),
  },
  ctaButtonTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  ctnMap: {
    width: "100%",
    borderRadius: HR(16),
    overflow: "hidden",
    marginBottom: HR(16),
    marginTop: HR(10),
  },
  map: {
    height: HR(150),
    width: "100%",
    overflow: "hidden",
  },
  txtTabTitle: {
    fontWeight: "200",
    color: Color.GrayLight,
  },
});

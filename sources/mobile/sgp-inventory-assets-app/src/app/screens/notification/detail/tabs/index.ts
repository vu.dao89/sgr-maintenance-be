import { default as NDTabAction } from "./action";
import { default as NDTabAttachment } from "./attachment";
import { default as NDTabGeneral } from "./general";
import { default as NDTabItem } from "./item";

export default [
  {
    key: "0",
    padding: 24,
    title: "Tổng quan",
    Scene: NDTabGeneral,
  },
  {
    key: "1",
    padding: 24,
    title: "Chi tiết",
    Scene: NDTabItem,
  },
  {
    key: "2",
    padding: 24,
    title: "Hành động",
    Scene: NDTabAction,
  },
  {
    key: "3",
    padding: 24,
    title: "Tài liệu",
    Scene: NDTabAttachment,
  },
];

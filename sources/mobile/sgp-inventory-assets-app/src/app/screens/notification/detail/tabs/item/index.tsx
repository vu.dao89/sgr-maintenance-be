import React, { FC, Fragment, useState } from "react";
import { TouchableOpacity, View } from "react-native";

import styles from "./styles";

import { CCollapsibleSection, NotificationItemCreation, TextCM } from "@Components";
import { widthResponsive as WR } from "@Constants";
import { get } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { NotificationItemCreateParams, NotificationServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted, useStateLazy } from "../../../../../../utils/Core";
import { formatNotificationItemInfo } from "../../../../../../utils/Format";

const NDTabItem: FC<any> = ({ saInsets, detail, items, onChange }) => {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();
  const toast = useToast();

  const [isCrud, setCrudStatus]: any = useState(false);
  const [itemAction, setItemAction]: any = useState("create");

  const [isFetching, setFetching] = useStateLazy(false);

  const { id: notificationId, notificationType } = detail;

  const handleCreateNotificationItem = async (itemState: any) => {
    setFetching(true);
    setCrudStatus(false);

    const { notifPart, notifPartCode, notifProblem, notifDame, notifText, notifCause, notifCauseText, notifCauseCode } =
      itemState;

    const item: NotificationItemCreateParams = {
      id: notificationId,
      notifPart: notifPart?.value || "",
      notifPartCode: notifPartCode?.value || "",
      notifProblem: notifProblem?.value || "",
      notifDame: notifDame?.value || "",
      notifText: notifText || "",
      notifCause: notifCause?.value || "",
      notifCauseCode: notifCauseCode?.value || "",
      notifCauseText: notifCauseText || "",
    };

    const { response, error } = await axiosHandler(() => NotificationServices.createNotificaitonItem(item));

    setFetching(false);
    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (get(response, "data")) {
      toast.show("Đã tạo thành công thông báo chi tiết.", { type: "success" });

      const notificationItem = { ...itemState, id: notificationId, item: get(response, "data.notificationItemId") };
      items.push(notificationItem);

      onChange("items", [...items]);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <Fragment>
      <View
        style={[
          styles.container,
          { paddingBottom: get(detail, "isFinish", false) ? saInsets.bottom : saInsets.bottom + WR(72) },
        ]}>
        <TouchableOpacity
          style={styles.btnAddItem}
          onPress={() => {
            setCrudStatus(true);
          }}>
          {notificationType && (
            <TextCM style={[styles.btnAddTxt, { paddingBottom: WR(24) }]}>+ Thêm thông báo chi tiết</TextCM>
          )}
        </TouchableOpacity>
        {items.map((item: any, index: any) => {
          return (
            <CCollapsibleSection
              key={index}
              title={`Thông báo chi tiết ${items.length > 1 ? `${index + 1}` : ""}`}
              styles={!index && { wrapper: { marginTop: 0 } }}
              infos={formatNotificationItemInfo(item)}
            />
          );
        })}

        <NotificationItemCreation
          saInsets={saInsets}
          isCrud={isCrud}
          itemAction={itemAction}
          notificationId={notificationId}
          notificationType={notificationType}
          initState={{}}
          onClose={() => setCrudStatus(false)}
          onChange={handleCreateNotificationItem}
          onModalHide={() => {
            if (isFetching) dispatch(CommonActions.openLoading.request({}));
          }}
        />
      </View>
    </Fragment>
  );
};

export default NDTabItem;

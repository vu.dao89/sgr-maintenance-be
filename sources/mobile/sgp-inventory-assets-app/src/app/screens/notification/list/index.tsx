import dayjs from "dayjs";
import { get, isEmpty } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";

import i18n from "@I18n";

import { MasterDataServices, NotificationServices, PermissionServices } from "@Services";
import { axiosHandler, getPagination } from "@Services/httpClient";
import {
  BaseParams,
  CommonStatusParams,
  MetricParams,
  NotificationSearchParams,
  PersonnelParams,
  PriorityParams,
} from "@Services/parameters";

import { CommonActions } from "src/redux/actions";
import { AppState } from "src/redux/reducers";

import { isArray } from "src/utils/Basic";
import { isCloseToBottom, useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsFormatDate, SAP_DATE_FORMAT } from "src/utils/DateTime";
import { convertFilterMultiValue, formatNotificationListItem } from "src/utils/Format";
import { changeScreen } from "src/utils/Screen";

import styles from "./styles";

import { Permission, ScreenName, widthResponsive as WR } from "@Constants";

import {
  CAlertModal,
  CEmptyState,
  CFilterModal,
  CMetricSection,
  CNavbar,
  COverlayAddBtn,
  CRefreshControl,
  CScanQRCodeModal,
  CSpinkit,
  NListItem,
  SearchInput,
} from "@Components";

const FIRST_PAGE_NUMBER = 1;

import { ICGhost, ICQRC } from "@Assets/image";

const NotificationList: FC<any> = props => {
  const saInsets = useSafeAreaInsets();
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const filterModalRef: any = useRef();

  const [isFetchingList, setFetchingList] = useStateLazy(true);
  const [pagination, setPagination] = useStateLazy(getPagination());
  const [notifications, setNotifications] = useStateLazy([]);
  const [keyword, setKeyword]: any = useState("");

  const [isFetchingMetric, setFetchingMetric] = useStateLazy(false);
  const [metric, setMetric]: any = useState([]);
  const [dates, setDates] = useStateLazy([dayjs().subtract(1, "month").toDate(), new Date()]);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const personPermission = PermissionServices.getPersonPermission(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.SEARCH);

  const filterOptions = useState([
    {
      id: "maintenancePlace",
      label: "Theo nơi bảo trì",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };

        const { response } = await axiosHandler(() => MasterDataServices.getMaintenancePlants(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: item.plantName + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "fromDate",
      type: "datetime",
      label: "Từ ngày thông báo",
      isRequired: true,
    },
    {
      id: "toDate",
      type: "datetime",
      label: "Đến ngày",
      isRequired: true,
    },
    {
      id: "creator",
      label: "Theo người tạo",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: PersonnelParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          workCenterIds: [],
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPersonnel(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: item.name + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "assignee",
      label: "Theo người được thông báo tới",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: PersonnelParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          workCenterIds: [],
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPersonnel(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: item.name + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "priority",
      label: "Theo mức độ ưu tiên",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, type } = filterSelecteds;

        const param: PriorityParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          types: convertFilterMultiValue(type),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPriorities(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.priority, label: item.description + " (" + item.priority + ")" };
          })
        );
      },
    },
    {
      id: "type",
      label: "Theo loại thông báo",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: BaseParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getNotificationTypes(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.notiType, label: item.description + " (" + item.notiType + ")" };
          })
        );
      },
    },
    {
      id: "planningDdepartment",
      label: "Theo bộ phận kế hoạch",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: BaseParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPlannerGroups(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.plannerGroupId, label: item.name + " (" + item.plannerGroupId + ")" };
          })
        );
      },
    },
    {
      id: "status",
      label: "Theo trạng thái",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: CommonStatusParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          codes: ["1 - Notification"], //TODO: remove soon
        };

        const { response } = await axiosHandler(() => MasterDataServices.getCommonStatus(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.status, label: item.description + " (" + item.status + ")" };
          })
        );
      },
    },
  ])[0];
  const [filterSelecteds, setFilterSelecteds]: any = useStateLazy({
    fromDate: dates[0],
    toDate: dates[1],
  });

  const [isShowQRScan, setShowQRScan] = useState(false);
  const [isScanErrorAlert, setScanErrorAlert] = useState(false);

  useEffect(() => {
    handleGetMetric();
  }, [dates]);

  useEffect(() => {
    if (havePermission) {
      handleSetDates();
      handleGetList();
    } else {
      setFetchingList(false);
    }

    // @ts-ignore
    global["notification_list_cb"] = () => handleGetList();
    return () => {
      // @ts-ignore
      delete global["notification_list_cb"];
    };
  }, [filterSelecteds, keyword]);

  // START: handler functions

  const handleSetDates = () => {
    if (dates[0] === filterSelecteds.fromDate && dates[1] === filterSelecteds.toDate) return;
    setDates([filterSelecteds.fromDate, filterSelecteds.toDate]);
  };

  const handleGetMetric = async (nextSelecteds?: any) => {
    if (!havePermission) return;
    const { fromDate, toDate } = nextSelecteds || filterSelecteds;

    await setFetchingMetric(true);

    let params: MetricParams = {
      mainPlantIds: userPlants,
      fromDate: dayjsFormatDate(fromDate, SAP_DATE_FORMAT),
      toDate: dayjsFormatDate(toDate, SAP_DATE_FORMAT),
      mainPerson: personPermission,
    };
    const { response } = await axiosHandler(() => NotificationServices.getNotificationMetric(params), { delay: true });
    if (!isMounted()) return;

    const data = get(response, "data.notification.item", []);
    setMetric(data);
    setFetchingMetric(false);
  };

  const handleGetList = async (page: any = FIRST_PAGE_NUMBER, functionalLocations: any = [], equipments: any = []) => {
    if (!havePermission) return;
    const { maintenancePlace, fromDate, toDate, creator, assignee, priority, type, planningDdepartment, status } =
      filterSelecteds;

    if (page === FIRST_PAGE_NUMBER) {
      await setNotifications([]);
      await setPagination(getPagination());
    }

    await setFetchingList(true);

    let params: NotificationSearchParams = {
      notiDateFrom: dayjsFormatDate(fromDate, SAP_DATE_FORMAT),
      notiDateTo: dayjsFormatDate(toDate, SAP_DATE_FORMAT),
      notiTypes: convertFilterMultiValue(type),
      reportBy: creator,
      assignTo: assignee,
      priorities: convertFilterMultiValue(priority),
      plannerGroups: convertFilterMultiValue(planningDdepartment),
      statuses: convertFilterMultiValue(status),
      maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
      keyword: keyword,
      page: page,
      employeeId: personPermission,
      functionalLocations: functionalLocations,
      equipments: equipments,
    };

    const { response } = await axiosHandler(() => NotificationServices.getNotifications(params), { delay: true });
    const data: any = get(response, "data.items", []);
    // console.log(`🚀 Kds: getNotifications -> response`, response);

    if (!isMounted()) return;

    let nextPageNotifications: any = [];
    if (isArray(data, 1)) {
      data.forEach((item: any) => {
        nextPageNotifications.push(formatNotificationListItem(item));
      });
    }

    setPagination(getPagination(get(response, "data.pagination", {})));
    setNotifications((curPageNotifications: any) => [...curPageNotifications, ...nextPageNotifications]);
    setFetchingList(false);
  };

  const handleOnReachedEnd = (e: any) => {
    if (!isFetchingList && pagination.hasNext && isCloseToBottom(e)) {
      handleGetList(pagination.nextPage);
    }
  };

  const handleScanResult = async (result: any) => {
    setShowQRScan(false);

    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => MasterDataServices.getByQRCode(result), { delay: true });
    let equipmentId = get(response, ["data", "equipment", "equipmentId"], "");
    let functionalLocationId = get(response, ["data", "functionalLocation", "functionalLocationId"], "");

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (isEmpty(equipmentId) && isEmpty(functionalLocationId)) {
      setScanErrorAlert(true);
    } else {
      handleGetList(FIRST_PAGE_NUMBER, [functionalLocationId], [equipmentId]);
    }
  };

  // END: handler functions

  // START: render functions

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isShowQRScan}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setShowQRScan(false)}
          onResult={handleScanResult}
        />
        <CAlertModal
          open={isScanErrorAlert}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => {
            setScanErrorAlert(false);
          }}
        />
      </Fragment>
    );
  };

  const _renderHeader = () => {
    const title =
      pagination.totalItems > 0 ? `${i18n.t("CM.notification")} (${pagination.totalItems})` : i18n.t("CM.notification");

    return (
      <CNavbar
        title={title}
        onLeftPress={() => changeScreen(props)}
        rightBtnNode={
          havePermission && (
            <CFilterModal
              showToggle
              ref={filterModalRef}
              title="Bộ lọc thông báo"
              options={filterOptions}
              selecteds={filterSelecteds}
              onChange={setFilterSelecteds}
            />
          )
        }
      />
    );
  };

  const _renderSearch = () => {
    return (
      havePermission && (
        <View style={styles.searchBar}>
          <SearchInput
            placeholder={"Nhập mã hoặc tiêu đề"}
            wrapperStyle={styles.searchBarInput}
            onTextSearch={(_keyword: string) => {
              setKeyword(_keyword);
            }}
          />
          <TouchableOpacity style={styles.searchBarBtn} onPress={() => setShowQRScan(true)}>
            <ICQRC />
          </TouchableOpacity>
        </View>
      )
    );
  };

  const _renderMetric = () => {
    return (
      <CMetricSection
        metric={metric}
        fetching={isFetchingMetric}
        dates={[filterSelecteds.fromDate, filterSelecteds.toDate]}
        countPriorityKey={"notificationCountPriority"}
        countStatusKey={"notificationCountStatus"}
        onFilter={(quickSelections: any) => {
          let nextSelecteds = {
            ...filterSelecteds,
            ...quickSelections,
          };
          setFilterSelecteds(nextSelecteds, () => {
            filterModalRef.current.setSelected(nextSelecteds);
            // if (quickSelections.fromDate) handleGetMetric(nextSelecteds);
          });
        }}
      />
    );
  };

  const _renderBody = () => {
    let haveData = isArray(notifications, 1);
    return (
      <ScrollView
        scrollEventThrottle={50}
        contentContainerStyle={{ paddingBottom: saInsets.bottom + WR(56 / 2 + 16) }}
        refreshControl={
          <CRefreshControl
            onRefresh={() => {
              handleGetMetric();
              handleGetList();
            }}
          />
        }
        onScroll={handleOnReachedEnd}>
        {_renderMetric()}
        {notifications.map((item: any, index: any) => (
          <NListItem
            key={item.id + "_" + String(index)}
            {...{ item, index }}
            onPress={() => changeScreen(props, ScreenName.NotificationDetail, { dataId: item.id })}
          />
        ))}
        {isFetchingList && (
          <View style={styles.dataFetching}>
            <CSpinkit />
          </View>
        )}
        {!isFetchingList && !haveData && <CEmptyState noPermission={!havePermission} />}
      </ScrollView>
    );
  };

  const _renderAddBtn = () => {
    if (!PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.POST)) return;
    return (
      <COverlayAddBtn
        onPress={() => changeScreen(props, ScreenName.NotificationCreate, { callback: "notification_list_cb" })}
      />
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {_renderSearch()}
      {_renderBody()}
      {_renderAddBtn()}
      {_renderScanQRCode()}
    </View>
  );

  // END: render functions
};

export default NotificationList;

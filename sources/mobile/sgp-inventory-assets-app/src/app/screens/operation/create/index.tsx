import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { View } from "react-native";

import i18n from "@I18n";

import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import TabViewRoutes from "./tabs";

import { CAlertModal, CEmptyState, CNavbar, CTAButtons, CTabViewCreate } from "@Components";

import { ICBackSVG, ICCloseY, ICWarningOctagon } from "@Assets/image";
import { useIsMounted, useStateLazy } from "src/utils/Core";

import { get, isNull } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "src/redux/reducers";

import { useToast } from "react-native-toast-notifications";

import {
  DocumentServices,
  EquipmentServices,
  MasterDataServices,
  OperationServices,
  PermissionServices,
  WorkOrderServices,
} from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";
import { genRandomString, isArray } from "src/utils/Basic";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatWorkCenterLabel,
  genAttachFiles,
  mappingSelections,
} from "src/utils/Format";
import { Permission } from "../../../../constants";
import { CommonActions } from "../../../../redux/actions";
import { SuperOperationParams } from "../../../../services/parameters";
import { CFile } from "../../../components/common/attach-files";

const ctaButtonTxts = ["next", "next", "next", "create"];

const relatatedOptions: any = {
  maintPlant: "workCenter",
  onlyWorkingPerson: "personnel",
  functionalLocation: "equipment",
};

const OperationCreate: FC<any> = props => {
  const [tabIdx, setTabIdx] = useState(0);
  const [state, setState]: any = useStateLazy({
    tasks: [],
    attachFiles: [],
  });

  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const toast = useToast();

  const params = getScreenParams(props);
  const { isEdit, operationId, workOrderId, callback, workOrder } = params;

  const [errors, setErrors] = useStateLazy({});
  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});

  const reqIds: any = useRef({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const personPermission = PermissionServices.getPersonPermission(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.POST);
  const [showConfirm, setShowConfirm] = useState(false);

  useEffect(() => {
    if (isEdit && havePermission) {
      if (operationId && workOrderId) {
        handleGetDetail(workOrderId, operationId);
      }
    }
    if (workOrder && havePermission) {
      handleSelectDefaultWorkOrder();
    }
  }, []);

  const handleSelectDefaultWorkOrder = () => {
    const {
      workOrderId,
      workOrderDescription,
      mainPlant,
      workCenter,
      workCenterDescription,
      functionalLocationId,
      functionalLocationDescription,
      equipmentId,
      equipmentDescription,
      workCenterDetail,
    } = workOrder;

    let detailState = {
      workOrder: { value: workOrderId, label: formatObjectLabel(workOrderId, workOrderDescription) },
      maintPlant: {
        value: mainPlant?.code,
        label: mainPlant?.name,
      },
      workCenter: {
        value: workCenter,
        label: formatWorkCenterLabel(workCenter, mainPlant?.code, workCenterDescription),
        costCenterCode: workCenterDetail?.costCenterCode,
      },
      functionalLocation: {
        value: functionalLocationId,
        label: formatObjectLabel(functionalLocationId, functionalLocationDescription),
      },
      equipment: { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDescription, true) },
      localId: genRandomString(),
    };
    setState(detailState);
  };

  const handleChangeTab = (targetIdx: any) => setTabIdx(tabIdx + targetIdx);

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "cleanSelections":
        {
          setOptions((prevState: any) => ({ ...prevState, [data]: [] }));
        }
        break;
      case "attachFiles":
        const files = state.attachFiles || [];
        setState((prevState: any) => ({ ...prevState, attachFiles: [...data, ...files] }));
        break;
      default:
        {
          let nextState: any = { [id]: data },
            nextOptsState: any = {};
          let relatatedOptionDataKey = relatatedOptions[id];
          if (relatatedOptionDataKey && !!data) {
            nextState[relatatedOptionDataKey] = null;
            nextOptsState[relatatedOptionDataKey] = [];
          }
          switch (id) {
            case "workCenter":
              {
                if (data && !state.maintPlant) {
                  if (data?.maintPlant?.value && data?.maintPlant?.label) {
                    nextState.maintPlant = data.maintPlant;
                  }
                }
              }
              break;
            case "equipment":
              {
                if (!!data && !state.functionalLocation) {
                  nextState.functionalLocation = data.functionalLocation;
                }
              }
              break;
            case "workOrder":
              {
                if (data) {
                  const {
                    mainPlant,
                    maintenancePlantDescription,
                    workCenter,
                    workCenterDescription,
                    functionalLocationId,
                    functionalLocationDescription,
                    equipmentId,
                    equipmentDescription,
                  } = data;

                  handleChange("workCenter", {
                    value: workCenter,
                    label: formatWorkCenterLabel(workCenter, mainPlant, workCenterDescription),
                    maintPlant: {
                      value: mainPlant,
                      label: formatObjectLabel(mainPlant, maintenancePlantDescription),
                    },
                  });

                  handleChange("functionalLocation", {
                    value: functionalLocationId,
                    label: formatObjectLabel(functionalLocationId, functionalLocationDescription),
                  });

                  handleChange("equipment", {
                    value: equipmentId,
                    label: formatObjectLabel(equipmentId, equipmentDescription),
                  });
                }
              }
              break;
            default:
              break;
          }
          setState((prevState: any) => ({ ...prevState, ...nextState }));
          setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
          setErrors((prevState: any) => ({ ...prevState, [id]: false }));
        }
        break;
    }
  };

  const handleGetDetail = async (workOrderId: string, operationId: string) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => OperationServices.getOperationDetail(workOrderId, operationId));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    const operation = get(response, "data.operation", null);

    if (isNull(operation)) {
      return;
    }

    const {
      workOrderDescription,
      operationDescription,
      controlKey,
      controlKeyData,
      personnel,
      personnelName,
      maintenancePlantCode,
      plantDescription,
      workCenterCode,
      workCenterDescription,
      functionalLocationId,
      functionalLocationDescription,
      equipmentId,
      equipmentDescription,
      activityType,
      activityTypeData,
      estimate,
      documents,
    } = operation;

    let detailState: any = {};
    Object.assign(detailState, {
      workOrder: { value: workOrderId, label: formatObjectLabel(workOrderId, workOrderDescription) },
      fullDescription: operationDescription,
      controlKey: { value: controlKey, label: formatObjectLabel(controlKey, controlKeyData?.description) },
      personnel: { value: personnel, label: formatObjectLabel(personnel, personnelName), name: personnelName },
      maintPlant: {
        value: maintenancePlantCode,
        label: formatObjectLabel(maintenancePlantCode, plantDescription, true),
      },
      workCenter: {
        value: workCenterCode,
        label: formatWorkCenterLabel(workCenterCode, maintenancePlantCode, workCenterDescription),
      },
      functionalLocation: {
        value: functionalLocationId,
        label: formatObjectLabel(functionalLocationId, functionalLocationDescription),
      },
      equipment: { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDescription, true) },
      activityType: { value: activityType, label: formatObjectLabel(activityType, activityTypeData?.type) },
      estimate: `${estimate}`,
      operationId: operation.operationId,
      localId: genRandomString(),
    });
    setState(detailState);

    if (documents) {
      const files = genAttachFiles(documents);
      setState((prevState: any) => ({ ...prevState, attachFiles: files }));
    }

    const _subOperation = get(response, "data.subOperation.item");
    const tasks =
      _subOperation?.map((sub: any) => {
        const item = {
          localId: genRandomString(),
          id: sub.operationId,
          superOperationId: sub.superOperationId,
          workOrderId: sub.workOrderId,
          fullDescription: sub.operationLongText || sub.operationDescription,
          tags: [
            {
              label: sub.operationSystemStatusDescription,
              color: sub.commonStatus?.colorCode || "#3A73B7",
            },
            {
              label: sub.result,
              color: sub.varianceReason?.colorCode || "#3A73B7",
            },
          ],
          controlKey: {
            value: sub.controlKey,
            label: formatObjectLabel(sub.controlKey, sub.controlKeyData?.description),
          },
          maintPlant: {
            value: sub.maintenancePlantCode,
            label: formatObjectLabel(sub.maintenancePlantCode, sub.maintenancePlantDesc, true),
          },
          workCenter: {
            value: workCenterCode,
            label: formatWorkCenterLabel(sub.workCenterCode, sub.maintenancePlantCode, sub.workCenterDesc),
          },
          estimate: `${estimate}`,
          files: genAttachFiles(sub.documents),
        };

        return item;
      }) || [];
    handleChange("tasks", tasks);
  };

  const handleUploadDocuments = async (attachFiles: any) => {
    if (isArray(attachFiles, 1)) {
      const { response, error } = await axiosHandler(() => DocumentServices.uploadDocuments(attachFiles));

      const fileData = get(response, "data");

      if (isArray(fileData, 1)) {
        return fileData.map((f: any) => f.id);
      }

      toast.show(getErrorDetail(error).errorMessage, { type: "warning" });

      return [];
    }
  };

  const editSuperOperation = async () => {
    dispatch(CommonActions.openLoading.request({}));

    const {
      operationId,
      // section 1
      workOrder,
      // section 2
      fullDescription,
      activityType,
      controlKey,
      personnel,
      maintPlant,
      workCenter,
      estimate,
      functionalLocation,
      equipment,
      localId,
      tasks,
      attachFiles,
    } = state;

    let documentIds: any = attachFiles.filter((f: CFile) => f.isRemote).map((f: CFile) => f.uuid);
    const files = attachFiles.filter((f: CFile) => !f.isRemote);
    if (isArray(files, 1)) {
      const { response } = await handleUploadDocuments(files);
      const fileData = get(response, "data");
      if (isArray(fileData, 1)) {
        fileData.forEach((element: any) => {
          if (element.id) documentIds.push(element.id);
        });
      }
    }

    for (let i = 0; i < tasks.length; i++) {
      let child = tasks[i];
      let childDocumentIds = child.files.filter((f: CFile) => f.isRemote).map((f: CFile) => f.uuid);
      let childFiles = child.files.filter((f: CFile) => !f.isRemote);

      if (isArray(childFiles, 1)) {
        const { response } = await handleUploadDocuments(child.files);
        const fileData = get(response, "data");
        if (isArray(fileData, 1)) {
          fileData.forEach((element: any) => {
            if (element.id) childDocumentIds.push(element.id);
          });
        }
      }
      Object.assign(tasks[i], { ...child, documentIds: childDocumentIds });
    }

    const params: SuperOperationParams = {
      workOrderId: workOrder?.value,
      operationId: operationId,
      superOperationId: "",
      operationLongText: fullDescription,
      equipmentId: equipment?.value || "",
      controlKey: controlKey?.value,
      maintenancePlantId: maintPlant?.value,
      workCenterId: workCenter?.value,
      functionalLocationId: functionalLocation?.value || "",
      personnel: personnel?.value,
      estimate: estimate,
      activityType: activityType?.value,
      localId: localId,
      subOperations:
        tasks?.map((item: any) => {
          const sub: SuperOperationParams = {
            workOrderId: workOrder?.value,
            operationId: item.id || "",
            superOperationId: operationId,
            operationLongText: item.fullDescription,
            equipmentId: equipment?.value || "",
            controlKey: item.controlKey?.value,
            maintenancePlantId: item.maintPlant?.value,
            workCenterId: item.workCenter?.value,
            functionalLocationId: functionalLocation?.value || "",
            personnel: personnel?.value,
            estimate: item.estimate,
            activityType: activityType?.value,
            localId: item.localId,
            subOperations: [],
            documentIds: item.documentIds || [],
          };
          return sub;
        }) || [],
      documentIds: documentIds,
    };

    const { response, error } = await axiosHandler(() => OperationServices.updateSuperOperation(params));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    console.log(`🚀 Kds: handleOnCTA -> response`, response);
    await setFetchings((prevState: any) => ({ ...prevState, crud: false }));

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toast.show("Chỉnh sửa công việc thành công!", { type: "success" });
      handleCallback();
    }
  };

  const handleCallback = () => {
    const callback = getScreenParams(props).callback || "operation_list_cb";
    // @ts-ignore
    (global[callback] || (() => null))();
    changeScreen(props);
  };

  const createSuperOperation = async () => {
    dispatch(CommonActions.openLoading.request({}));

    const {
      // section 1
      workOrder,
      // section 2
      fullDescription,
      activityType,
      controlKey,
      personnel,
      maintPlant,
      workCenter,
      estimate,
      functionalLocation,
      equipment,
      localId,
      tasks,
      attachFiles,
    } = state;

    let documentIds: any = [];
    const files = attachFiles.filter((f: CFile) => !f.isRemote);
    if (isArray(files, 1)) {
      documentIds = await handleUploadDocuments(files);
    }

    for (let i = 0; i < tasks.length; i++) {
      let child = tasks[i];
      let childFiles = child.files.filter((f: CFile) => !f.isRemote);

      if (isArray(childFiles, 1)) {
        let childDocumentIds = await handleUploadDocuments(child.files);
        Object.assign(tasks[i], { ...child, documentIds: childDocumentIds });
      }
    }

    const params: SuperOperationParams = {
      workOrderId: workOrder?.value,
      operationId: "",
      superOperationId: "",
      operationLongText: fullDescription,
      equipmentId: equipment?.value,
      controlKey: controlKey?.value,
      maintenancePlantId: maintPlant?.value,
      workCenterId: workCenter?.value,
      functionalLocationId: functionalLocation?.value,
      personnel: personnel?.value,
      estimate: estimate,
      activityType: activityType?.value,
      localId: localId,
      subOperations:
        tasks?.map((item: any) => {
          const sub: SuperOperationParams = {
            workOrderId: workOrder?.value,
            operationId: "",
            superOperationId: "",
            operationLongText: item.fullDescription,
            equipmentId: equipment?.value,
            controlKey: item.controlKey?.value,
            maintenancePlantId: item.maintPlant?.value,
            workCenterId: item.workCenter?.value,
            functionalLocationId: functionalLocation?.value,
            personnel: personnel?.value,
            estimate: item.estimate,
            activityType: activityType?.value,
            localId: item.localId,
            subOperations: [],
            documentIds: item.documentIds || [],
          };
          return sub;
        }) || [],
      documentIds: documentIds,
    };

    const { response, error } = await axiosHandler(() => OperationServices.createSuperOperation(params));

    if (!isMounted()) return;
    //console.log(`🚀 Kds: handleOnCTA -> response`, response);
    await setFetchings((prevState: any) => ({ ...prevState, crud: false }));
    dispatch(CommonActions.openLoading.success({}));

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      const document = get(response, "data.document");
      toast.show(`Tạo mới công việc ${document} thành công!`, { type: "success" });
      handleCallback();
    }
  };

  const handleOnCTA = async () => {
    switch (tabIdx) {
      case 0: {
        let isValidData = true,
          _errors: any = {},
          focusInputField: any = null,
          setInvalid = (id: any) => {
            isValidData = false;
            _errors[id] = true;
            if (!focusInputField) focusInputField = id;
          };
        [
          // section 1
          "workOrder",
          // section 2
          "fullDescription",
          "activityType",
          "controlKey",
          "personnel",
          "maintPlant",
          "workCenter",
          "estimate",
        ].forEach((requiredField: any) => {
          if (!state[requiredField]) setInvalid(requiredField);
        });

        if (!state["equipment"] && !state["functionalLocation"]) {
          setInvalid("equipment");
          setInvalid("functionalLocation");
        }

        setErrors(_errors);
        if (isValidData) {
          setState((prevState: any) => ({ ...prevState, localId: genRandomString() }));
          handleChangeTab(1);
        } else {
          // TODO: scroll into focusInputField
          toast.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
        }

        break;
      }
      case 1:
        {
          state.tasks?.forEach((task: any) => {
            task.localId = genRandomString();
          });
          handleChangeTab(1);
        }
        break;
      case 2:
        if (isEdit) {
          editSuperOperation();
        } else {
          createSuperOperation();
        }
        break;
      default:
        break;
    }
  };

  const handleSearch = (id: any, relatedData: any) => async (keyword: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];
    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(state.maintPlant ? [state.maintPlant] : [], userPlants),
      page: 1,
      size: 50,
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "workOrder":
        {
          params.statusIds = ["I0001"];
          params.mainPersonIds = [personPermission];
          searchServiceApi = WorkOrderServices.workOrderSearch;
          mappingOptions = {
            valueKey: "workOrderId",
            labelKey: "workOrderDescription",
            format: (i: any) => ({
              mainPlant: i.mainPlant,
              maintenancePlantDescription: i.maintenancePlantDescription,
              workCenter: i.workCenter,
              workCenterDescription: i.workCenterDescription,
              functionalLocationId: i.functionalLocationId,
              functionalLocationDescription: i.functionalLocationDescription,
              equipmentId: i.equipmentId,
              equipmentDescription: i.equipmentDescription,
            }),
          };
        }
        break;
      case "workOrderType":
        {
          searchServiceApi = MasterDataServices.getWorkOrderTypes;
          mappingOptions = { valueKey: "type", labelKey: "name" };
        }
        break;
      case "maintPlant":
        {
          params.maintenancePlantCodes = userPlants;
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenterCode: i.costCenterCode,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "functionalLocation":
        {
          searchServiceApi = MasterDataServices.getFunctionalLocations;
          mappingOptions = { valueKey: "functionalLocationId" };
        }
        break;
      case "equipment":
        {
          params.functionalLocationCodes = [state?.functionalLocationId || ""];
          searchServiceApi = EquipmentServices.getEquipments;
          mappingOptions = {
            valueKey: "equipmentId",
            format: (i: any) => ({
              label: formatObjectLabel(i.equipmentId, i.description, true),
              functionalLocation: {
                label: formatObjectLabel(i.functionalLocationId, i.functionalLocation?.description),
                value: i.functionalLocationId,
              },
            }),
          };
        }
        break;
      case "personnel":
        {
          params.workCenterIds = [get(state, "workCenter.value", "")];
          searchServiceApi = MasterDataServices.getPersonnel;
          mappingOptions = { labelKey: "name" };
        }
        break;
      case "activityType":
        {
          params.costCenterCodes = [get(state, "workCenter.costCenterCode", "")];
          searchServiceApi = MasterDataServices.getActivityTypes;
          mappingOptions = {
            valueKey: "type",
            format: (i: any) => ({ label: `${i.description} (${i.type})` }),
          };
        }
        break;
      case "controlKey":
        {
          searchServiceApi = MasterDataServices.getControlKey;
          mappingOptions = { valueKey: "code", labelKey: "description" };
        }
        break;

      default:
        break;
    }
    const { response } = await axiosHandler(() => searchServiceApi(params));

    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const _renderHeader = () => {
    let isFirstTab = !tabIdx;
    return (
      <CNavbar
        title={`${isEdit ? "Chỉnh sửa" : "Tạo"} công việc`}
        leftIcon={isFirstTab ? <ICCloseY /> : <ICBackSVG />}
        onLeftPress={() => (isFirstTab ? setShowConfirm(true) : handleChangeTab(-1))}
      />
    );
  };

  const _renderBody = () => {
    return (
      havePermission && (
        <Fragment>
          <CTabViewCreate
            {...{
              isEdit,
              toast,
              state,
              errors,
              fetchings,
              options,
            }}
            index={tabIdx}
            routes={TabViewRoutes}
            onChange={handleChange}
            onSearch={handleSearch}
          />

          <CTAButtons
            isSticky
            buttons={[
              ...(!!tabIdx
                ? [
                    {
                      isSub: true,
                      text: i18n.t("CM.back"),
                      onPress: () => handleChangeTab(-1),
                    },
                  ]
                : []),
              {
                text: i18n.t(`CM.${ctaButtonTxts[tabIdx]}`),
                onPress: handleOnCTA,
              },
            ]}
          />
        </Fragment>
      )
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {_renderBody()}
      <CAlertModal
        open={showConfirm}
        title={"Bạn có chắc chắn muốn thoát không?"}
        icon={<ICWarningOctagon />}
        onClose={() => setShowConfirm(false)}
        onCta={() => {
          setShowConfirm(false);
          changeScreen(props);
        }}
      />
      {!havePermission && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default OperationCreate;

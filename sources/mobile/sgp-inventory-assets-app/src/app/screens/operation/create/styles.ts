import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  bodyCenter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  dataEmptyTxt: {
    textAlign: "center",
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  fetchingDataOverlay: {
    zIndex: 100,
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "#00000040",
  },
});

import React, { FC } from "react";
import { ScrollView, View } from "react-native";

import styles from "./styles";

import { Permission, widthResponsive as WR } from "@Constants";

import { CAttachFiles, CEmptyState, OperationAttachFiles, TextCM } from "@Components";
import { isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices } from "../../../../../../services";

const OperatorTabAttachment: FC<any> = ({ saInsets, title: tabTitle, state, onChange }) => {
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.ATTACHMENT_POST);

  const { tasks = [], attachFiles = [] } = state;
  const isEmptyFiles = isEmpty(attachFiles) && tasks?.every((task: any) => isEmpty(task?.files));

  return (
    <ScrollView contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>3. {tabTitle}</TextCM>
      <View style={styles.attachFiles}>
        <CAttachFiles
          files={attachFiles}
          onChange={
            havePermission &&
            ((files: any) => {
              onChange("attachFiles", [...files, ...attachFiles]);
            })
          }
        />
      </View>
      <View style={styles.attachFiles}>
        {tasks.map((task: any, index: any) => {
          if (!havePermission && isEmpty(task?.files)) {
            return null;
          }
          return (
            <OperationAttachFiles
              {...{ task }}
              key={task.uuid}
              onChange={
                havePermission &&
                ((attachFiles: any) => {
                  const files = task.files;
                  Object.assign(tasks[index], { ...task, files: [...attachFiles, ...files] });
                  onChange("tasks", tasks);
                })
              }
            />
          );
        })}
      </View>
      {isEmptyFiles && !havePermission && <CEmptyState noPermission={!havePermission} />}
    </ScrollView>
  );
};

export default OperatorTabAttachment;

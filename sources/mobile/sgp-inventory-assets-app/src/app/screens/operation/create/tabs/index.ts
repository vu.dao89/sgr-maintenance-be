import { default as OperationTabAttachment } from "./attachment";
import { default as OperationTabInfo } from "./info";
import { default as OperationTabTask } from "./task";

export default [
  {
    key: "0",
    title: "Nhập thông tin chính",
    Scene: OperationTabInfo,
  },
  {
    key: "1",
    title: "Tạo Công việc con",
    Scene: OperationTabTask,
  },
  // {
  //   key: "2",
  //   title: "Thông số",
  //   Scene: OperatorTabParameter,
  // },
  // {
  //   key: "3",
  //   title: "Vật tư",
  //   Scene: OperatorTabSupplies,
  // },
  {
    key: "2",
    title: "Tài liệu",
    Scene: OperationTabAttachment,
  },
];

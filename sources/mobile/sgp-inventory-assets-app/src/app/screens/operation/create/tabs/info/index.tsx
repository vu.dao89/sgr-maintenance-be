import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, CDatePicker, CInfoViewer, CSelect, CTextInput, TextCM } from "@Components";

import { ICQRC } from "@Assets/image";

const OperatorTabInfo: FC<any> = ({
  saInsets,
  title: tabTitle,
  toast,
  state,
  isEdit,
  errors,
  fetchings,
  options,
  onChange,
  onSearch,
  onShowQRScan,
}) => {
  const { workCenter } = state;

  const forms = [
    {
      title: "Lệnh bảo trì liên quan",
      fields: [
        {
          type: "select",
          id: "workOrder",
          label: "Lệnh bảo trì liên quan",
          isRequired: true,
        },
      ],
    },
    {
      title: "Thông tin chính",
      fields: [
        {
          type: "input",
          id: "fullDescription",
          label: "Mô tả chi tiết",
          plh: "Nhập mô tả chi tiết",
          multiline: 2,
          maxLength: 5000,
        },
        {
          type: "select",
          id: "controlKey",
          label: "Nguồn lực",
          isRequired: true,
        },
        {
          type: "select",
          id: "personnel",
          label: "Người chịu trách nhiệm",
          isRequired: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!workCenter) toast.show("Vui lòng chọn Tổ đội thực hiện", { type: "warning" });
            return !!workCenter;
          },
        },
        {
          type: "select",
          id: "maintPlant",
          label: "Nơi bảo trì",
          isRequired: true,
        },
        {
          type: "select",
          id: "workCenter",
          label: "Tổ đội thực hiện",
          isRequired: true,
        },
        {
          type: "select",
          id: "functionalLocation",
          label: "Khu vực chức năng",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "equipment",
          label: "Mã thiết bị",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "activityType",
          label: "Phân loại công việc",
          isRequired: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!workCenter) toast.show("Vui lòng chọn Tổ đội thực hiện", { type: "warning" });
            return !!workCenter;
          },
        },
        {
          type: "input",
          id: "estimate",
          isRequired: true,
          label: "Thời gian làm việc dự kiến",
          plh: "Nhập thời gian làm việc",
          keyboardType: "numeric",
          returnKeyType: "done",
        },
        {
          type: "unit",
          id: "unit",
          label: "Đơn vị",
          value: "Phút",
        },
      ],
    },
  ];

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>1. {isEdit ? "Sửa thông tin chính" : tabTitle}</TextCM>
      {forms.map((item: any, index: any) => {
        const { fields, ...section } = item;
        return (
          <CCollapsibleSection key={`info_${index}`} {...section}>
            {fields.map((field: any) => {
              const { type, id, isSearch, canScan, ...fieldProps } = field;
              let _fieldProps = {
                key: `field_info_${id}_${index}`,
                isError: errors[id],
                value: state[id],
                onChange: (data: any) => onChange(id, data),
                ...fieldProps,
              };
              switch (type) {
                case "input":
                  return <CTextInput key={`view_${id}_${index}`} {..._fieldProps} />;
                case "date":
                  return <CDatePicker key={`view_${id}_${index}`} isRequired {..._fieldProps} />;
                case "select": {
                  let selectInputNode = (
                    <CSelect
                      {..._fieldProps}
                      isFullScreen
                      isFetching={fetchings[id]}
                      selected={state[id]}
                      menus={options[id]}
                      styles={canScan && { wrapper: { flex: 1, marginRight: WR(20) } }}
                      onSelect={(selected: any) => onChange(id, selected)}
                      onSearch={onSearch(id)}
                      {...(!isSearch && { onOpen: onSearch(id) })}
                    />
                  );
                  if (canScan) {
                    return (
                      <View key={`view_${id}_${index}`} style={styles.row}>
                        {selectInputNode}
                        <TouchableOpacity onPress={() => onShowQRScan(id)}>
                          <ICQRC />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                  return selectInputNode;
                }
                case "unit":
                  return (
                    <CInfoViewer
                      key={`view_${id}_${index}`}
                      infos={[
                        {
                          label: "Đơn vị",
                          value: "Phút",
                        },
                      ]}
                    />
                  );
                default:
                  return null;
              }
            })}
          </CCollapsibleSection>
        );
      })}
    </KeyboardAwareScrollView>
  );
};

export default OperatorTabInfo;

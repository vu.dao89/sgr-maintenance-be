import React, { FC, Fragment, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, CModalViewer, CSelect, CTAButtons, CTextInput, TextCM } from "@Components";

import i18n from "@I18n";

const OperatorTabParameter: FC<any> = ({ saInsets, showTitle, title: tabTitle, state, onChange }) => {
  const [isCRUD, setShowCRUD]: any = useState(false);
  const [crudItem, setCrudItem]: any = useState({});

  const { items = [] } = state;

  return (
    <Fragment>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          {
            paddingVertical: WR(showTitle ? 30 : 20),
            paddingBottom: saInsets.bottom + (showTitle ? WR(44 + 24 + 8) : 0),
          },
        ]}>
        {!!showTitle && <TextCM style={styles.title}>3. {tabTitle}</TextCM>}

        <TouchableOpacity
          style={[styles.btnAddItem, { marginTop: showTitle ? WR(16) : 0 }]}
          onPress={() => setShowCRUD(true)}>
          <TextCM style={styles.btnAddTxt}>+ Thêm thông số</TextCM>
        </TouchableOpacity>

        {items.map((item: any, index: any) => {
          return (
            <CCollapsibleSection
              key={index}
              title={`Thông số ${index + 1}`}
              menus={[
                {
                  label: "Chỉnh sửa",
                  value: "edit",
                },
                {
                  label: "Xoá",
                  value: "delete",
                  color: Color.Red,
                },
              ]}
              infos={[
                {
                  label: "Tiêu chí",
                  value: item.criteria && item.criteria.value,
                },
                {
                  label: "Đơn vị do",
                  value: item.unitOfMeasure && item.unitOfMeasure.value,
                },
                {
                  label: "Đơn vị thấp nhất",
                  value: item.minValue,
                },
                {
                  label: "Đơn vị cao nhất",
                  value: item.maxValue,
                },
                {
                  label: "Ghi chú",
                  value: item.desc,
                },
              ]}
              onMenu={(menu: any) => {
                switch (menu.value) {
                  case "delete":
                    {
                      items.splice(index, 1);
                      onChange("items", items);
                    }
                    break;
                  case "edit":
                    setCrudItem(item);
                    setShowCRUD(true);
                    break;
                  default:
                    break;
                }
              }}
              children={
                <>
                  <Fragment>
                    <CTAButtons
                      isSticky
                      buttons={[
                        {
                          text: i18n.t(`CM.enter`),
                        },
                      ]}
                      onCta={() => {
                        console.log("Enter parameter");
                      }}
                    />
                  </Fragment>
                </>
              }
            />
          );
        })}
      </ScrollView>

      <CModalViewer
        open={isCRUD}
        placement={"bottom"}
        title={"Tạo thông số"}
        onBackdropPress={() => setShowCRUD(false)}>
        <ScrollView contentContainerStyle={styles.crudModalContent}>
          <View style={styles.infoWrapper}>
            <View style={[{ width: "50%" }, { paddingRight: WR(8) }, { marginTop: WR(16) }]}>
              <CSelect
                isRequired
                label={"Chọn tiêu chí đo"}
                selected={crudItem.criteria}
                menus={[
                  {
                    value: "groupA",
                    label: "tiêu chí 1",
                  },
                  {
                    value: "groupB",
                    label: "tiêu chí 2",
                  },
                  {
                    value: "groupC",
                    label: "tiêu chí 3",
                  },
                ]}
                onSelect={(selected: any) => setCrudItem({ ...crudItem, criteria: selected })}
              />
            </View>
          </View>
          <View style={styles.infoWrapper}>
            <View style={[{ width: "50%" }, { paddingRight: WR(8) }, { marginTop: WR(16) }]}>
              <CSelect
                isRequired
                label={"Chọn đơn vị do"}
                selected={crudItem.unitOfMeasure}
                menus={[
                  {
                    value: "groupA",
                    label: "KG",
                  },
                  {
                    value: "groupB",
                    label: "cm",
                  },
                  {
                    value: "groupC",
                    label: "m",
                  },
                ]}
                onSelect={(selected: any) => setCrudItem({ ...crudItem, unitOfMeasure: selected })}
              />
            </View>
          </View>
          <View style={styles.infoWrapper}>
            <View style={[{ width: "50%" }, { paddingRight: WR(8) }, { marginTop: WR(16) }]}>
              <CTextInput
                isRequired
                multiline={1}
                label={"Đơn vị thấp nhất"}
                plh={"Nhập đơn vị thấp nhất"}
                value={crudItem.minValue}
                onChange={(text: any) => setCrudItem({ ...crudItem, minValue: text })}
              />
            </View>
          </View>
          <View style={styles.infoWrapper}>
            <View style={[{ width: "50%" }, { paddingRight: WR(8) }, { marginTop: WR(16) }]}>
              <CTextInput
                isRequired
                multiline={1}
                label={"Đơn vị cao nhất"}
                plh={"Nhập đơn vị cao nhất"}
                value={crudItem.maxValue}
                onChange={(text: any) => setCrudItem({ ...crudItem, maxValue: text })}
              />
            </View>
          </View>
          <CTextInput
            isRequired
            multiline={3}
            label={"Kêt quả đo"}
            plh={"Nhập kêt quả do"}
            value={crudItem.desc}
            onChange={(text: any) => setCrudItem({ ...crudItem, desc: text })}
          />
        </ScrollView>
        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Thôi",
              onPress: () => setShowCRUD(false),
            },
            {
              text: "Tạo",
              onPress: () => {
                setShowCRUD(false);
                if (crudItem.id) {
                  Object.assign(items[items.findIndex((i: any) => i.id === crudItem.id)], crudItem);
                } else {
                  items.unshift({
                    ...crudItem,
                    id: String(Math.random()).substring(1),
                  });
                }
                onChange("items", items);
              },
            },
          ]}
        />
      </CModalViewer>
    </Fragment>
  );
};

export default OperatorTabParameter;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingHorizontal: WR(24),
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(4),
  },
  infoWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
});

import React, { FC, Fragment, useState } from "react";
import { ScrollView, TouchableOpacity } from "react-native";

import styles from "./styles";

import { Color, widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, CModalViewer, CSelect, CTAButtons, CTextInput, TextCM } from "@Components";

const OperatorTabSupplies: FC<any> = ({ saInsets, showTitle, title: tabTitle, state, onChange }) => {
  const [isCRUD, setShowCRUD]: any = useState(false);
  const [crudItem, setCrudItem]: any = useState({});

  const { items = [] } = state;

  return (
    <Fragment>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          {
            paddingVertical: WR(showTitle ? 30 : 20),
            paddingBottom: saInsets.bottom + (showTitle ? WR(44 + 24 + 8) : 0),
          },
        ]}>
        {!!showTitle && <TextCM style={styles.title}>4. {tabTitle}</TextCM>}

        <TouchableOpacity
          style={[styles.btnAddItem, { marginTop: showTitle ? WR(16) : 0 }]}
          onPress={() => setShowCRUD(true)}>
          <TextCM style={styles.btnAddTxt}>+ Thêm vật tư</TextCM>
        </TouchableOpacity>

        {items.map((item: any, index: any) => {
          return (
            <CCollapsibleSection
              key={index}
              title={`Vật tư ${index + 1}`}
              menus={[
                {
                  label: "Chỉnh sửa",
                  value: "edit",
                },
                {
                  label: "Xoá",
                  value: "delete",
                  color: Color.Red,
                },
              ]}
              infos={[
                {
                  label: "Công việc",
                  value: item.workOrder && item.workOrder.value,
                },
                {
                  label: "Vật tư",
                  value: item.supplies && item.supplies.value,
                },
                {
                  label: "Kho bảo trì",
                  value: item.maintenance && item.maintenance.value,
                },

                {
                  label: "Kho vật lý",
                  value: item.warehouse && item.warehouse.value,
                },
                {
                  label: "Lô hàng",
                  value: item.consignment && item.consignment.value,
                },
                {
                  label: "Số lượng yêu cầu",
                  value: item.numOfRequest && item.numOfRequest.value,
                },
                {
                  label: "Đơn vị",
                  value: item.unitOfMeasure && item.unitOfMeasure.value,
                },

                {
                  label: "Ghi chú",
                  value: item.desc,
                },
              ]}
              onMenu={(menu: any) => {
                switch (menu.value) {
                  case "delete":
                    {
                      items.splice(index, 1);
                      onChange("items", items);
                    }
                    break;
                  case "edit":
                    setCrudItem(item);
                    setShowCRUD(true);
                    break;
                  default:
                    break;
                }
              }}
            />
          );
        })}
      </ScrollView>

      <CModalViewer
        open={isCRUD}
        placement={"bottom"}
        title={"Tạo thông số"}
        onBackdropPress={() => setShowCRUD(false)}>
        <ScrollView contentContainerStyle={styles.crudModalContent}>
          <CSelect
            isRequired
            label={"Chọn công việc liên quan"}
            selected={crudItem.workOrder}
            menus={[
              {
                value: "groupA",
                label: "Công việc 1",
              },
              {
                value: "groupB",
                label: "Công việc 2",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, workOrder: selected })}
          />
          <CSelect
            isRequired
            label={"Vật tư"}
            selected={crudItem.supplies}
            menus={[
              {
                value: "groupA",
                label: "Vật tư 1",
              },
              {
                value: "groupB",
                label: "Vật tư 2",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, supplies: selected })}
          />

          <CSelect
            isRequired
            label={"kho bảo trì"}
            selected={crudItem.maintenance}
            menus={[
              {
                value: "groupA",
                label: "Kho bảo trì 1",
              },
              {
                value: "groupB",
                label: "Kho bảo trì 2",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, maintenance: selected })}
          />

          <CSelect
            isRequired
            label={"kho vật lý "}
            selected={crudItem.warehouse}
            menus={[
              {
                value: "groupA",
                label: "Kho vật lý  1",
              },
              {
                value: "groupB",
                label: "Kho vật lý 2",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, warehouse: selected })}
          />

          <CSelect
            isRequired
            label={"Lô hàng"}
            selected={crudItem.consignment}
            menus={[
              {
                value: "groupA",
                label: "Lô hàng 1",
              },
              {
                value: "groupB",
                label: "Lô hàng 2",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, consignment: selected })}
          />

          <CSelect
            isRequired
            label={"Số lượng yêu cầu"}
            selected={crudItem.numOfRequest}
            menus={[
              {
                value: "groupA",
                label: "1",
              },
              {
                value: "groupB",
                label: "1",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, numOfRequest: selected })}
          />

          <CSelect
            isRequired
            label={"Chọn đơn vị do"}
            selected={crudItem.unitOfMeasure}
            menus={[
              {
                value: "groupA",
                label: "KG",
              },
              {
                value: "groupB",
                label: "cm",
              },
              {
                value: "groupC",
                label: "m",
              },
            ]}
            onSelect={(selected: any) => setCrudItem({ ...crudItem, unitOfMeasure: selected })}
          />
          <CTextInput
            isRequired
            multiline={3}
            label={"Ghi chú"}
            plh={"Nhập ghi chú"}
            value={crudItem.desc}
            onChange={(text: any) => setCrudItem({ ...crudItem, desc: text })}
          />
        </ScrollView>
        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Thôi",
              onPress: () => setShowCRUD(false),
            },
            {
              text: "Tạo",
              onPress: () => {
                setShowCRUD(false);
                if (crudItem.id) {
                  Object.assign(items[items.findIndex((i: any) => i.id === crudItem.id)], crudItem);
                } else {
                  items.unshift({
                    ...crudItem,
                    id: String(Math.random()).substring(1),
                  });
                }
                onChange("items", items);
              },
            },
          ]}
        />
      </CModalViewer>
    </Fragment>
  );
};

export default OperatorTabSupplies;

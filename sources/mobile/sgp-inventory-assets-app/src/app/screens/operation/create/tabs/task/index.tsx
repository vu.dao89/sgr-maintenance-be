import React, { FC, useState } from "react";

import { TouchableOpacity, View } from "react-native";

import { Color } from "@Constants";

import styles from "./styles";

import { CCollapsibleSection, CPersonInfo, CTags, SubOperationCreation, TextCM } from "@Components";

import { genShortDescription, isArray } from "src/utils/Basic";

import { widthResponsive as WR } from "@Constants";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { useStateLazy } from "../../../../../../utils/Core";

const OperatorTabTask: FC<any> = ({ saInsets, title: tabTitle, isEdit, state, onChange }) => {
  const dispatch = useDispatch();

  const { tasks = [] } = state;

  const [isCrudChild, setCrudChildStatus]: any = useStateLazy(false);
  const [childAction, setChildAction]: any = useStateLazy("create");
  const [childState, setChildState]: any = useStateLazy({});

  const [isFetching, setFetching]: any = useState(false);

  const assignee = {
    code: state?.personnel?.value,
    fullName: state?.personnel?.name,
    avatar: state?.personnel?.avatar,
  };

  const { maintPlant, workCenter, controlKey } = state;

  return (
    <View style={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24) }]}>
      <TextCM style={styles.title}>2. {isEdit ? "Sửa công việc con" : tabTitle}</TextCM>
      <TouchableOpacity
        style={styles.btnAddItem}
        onPress={() => {
          setChildState({
            maintPlant: maintPlant,
            workCenter: workCenter,
            controlKey: controlKey,
          });
          setChildAction("create");
          setCrudChildStatus(true);
        }}>
        <TextCM style={styles.btnAddTxt}>+ Thêm công việc con</TextCM>
      </TouchableOpacity>

      {tasks.map((item: any, index: any) => {
        const { localId, fullDescription, tags } = item;

        return (
          <CCollapsibleSection
            key={localId}
            title={genShortDescription(fullDescription)}
            menus={[
              {
                label: "Chỉnh sửa",
                value: "edit",
              },
              {
                label: "Xoá",
                value: "delete",
                color: Color.Red,
              },
            ]}
            onMenu={({ value: menuValue }: any) => {
              switch (menuValue) {
                case "delete":
                  {
                    tasks.splice(index, 1);
                    onChange("tasks", tasks);
                  }
                  break;
                case "edit":
                  setChildState({
                    ...item,
                    parentIndex: index,
                  });
                  setChildAction(menuValue);
                  setCrudChildStatus(true);
                  break;
                default:
                  break;
              }
            }}>
            {!!fullDescription && <TextCM style={styles.taskDesc}>{fullDescription}</TextCM>}

            <View style={styles.taskAssigneeWrapper}>
              <CTags data={isArray(tags, 1) ? tags : [{ label: "Đã nhận", color: "#3A73B7" }]} />
            </View>

            <CPersonInfo
              label={"Đã giao cho"}
              fullName={assignee.fullName}
              avatar={assignee.avatar}
              style={styles.taskAssigneePerson}
            />
          </CCollapsibleSection>
        );
      })}

      <SubOperationCreation
        saInsets={saInsets}
        isCrud={isCrudChild}
        childAction={childAction}
        initState={childState}
        onClose={() => setCrudChildStatus(false)}
        onChange={(child: any) => {
          if (childAction === "create") {
            tasks.push(child);
          } else {
            const { parentIndex } = child;
            Object.assign(tasks[parentIndex], { ...child });
          }
          onChange("tasks", tasks);
        }}
        onModalHide={() => {
          if (isFetching) {
            dispatch(CommonActions.openLoading.request({}));
          }
        }}
      />
    </View>
  );
};

export default OperatorTabTask;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  taskAssigneeWrapper: {
    marginBottom: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  taskDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  taskAssigneePerson: {
    marginBottom: WR(12),
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  btnAddItem: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(4),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
});

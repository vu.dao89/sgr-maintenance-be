import React, { FC, useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import { useSafeAreaInsets } from "react-native-safe-area-context";

import { widthResponsive as WR } from "@Constants";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import { useToast } from "react-native-toast-notifications";

import {
  CEmptyState,
  CNavbar,
  CPersonInfo,
  CPersonSelector,
  CSpinkit,
  CTabViewDetail,
  CTags,
  OperationConfirmation,
  TextCM,
} from "@Components";

import { ICEdit2, ICMap } from "@Assets/image";

import { OperationHistoryActivity, Permission, ScreenName, SystemStatus } from "@Constants";
import { OperationServices, PermissionServices, WorkOrderServices } from "@Services";
import { get, isEmpty, isNull } from "lodash";
import { useIsMounted } from "src/utils/Core";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { dayjsFormatDate, dayjsTimePassedFromNow } from "../../../../utils/DateTime";

import { logError } from "libs/react-native-x-framework/js/logger";
import { useDispatch, useSelector } from "react-redux";
import { formatObjectLabel, formatWorkCenterLabel, genAttachFiles } from "src/utils/Format";
import { CommonActions } from "../../../../redux/actions";
import { AppState } from "../../../../redux/reducers";
import { isArray } from "../../../../utils/Basic";
import OperationActionButton from "../../../components/operation/action-button";
import { SubOperationTaskProps } from "../../../components/operation/sub-task-item";
import TabViewRoutes from "./tabs";

const OprationDetail: FC<any> = props => {
  const isMounted = useIsMounted();
  const saInsets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const toast = useToast();

  const [headerInfoMinHeight, setHeaderInfoMinHeight] = useState(0);
  const [tabViewMinHeight, setTabViewMinHeight] = useState(0);

  const [isFetching, setFetching] = useState(true);
  const [tabIdx, setTabIdx] = useState(0);
  const [haveCTA, setHaveCTA] = useState(false);

  const [subOperations, setSubSubOperations] = useState([] as any);
  const [detail, setDetail]: any = useState(null);
  const [measures, setMeasures]: any = useState([]);
  const [components, setComponents]: any = useState([]);
  const [workOrderDetail, setWorkOrderDetail]: any = useState(null);
  const [historyActivity, setHistoryActivity]: any = useState(null);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const employeeCode = PermissionServices.getEmployeeCode(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.VIEW);
  const canCreateSubOps = PermissionServices.checkPermission(reducer, Permission.SUB_OPERATION.POST);

  const [isChangeWorker, setChangeWorker] = useState(false);

  const [confirmationType, setConfirmationType]: any = useState("");
  const [isShowConfirmation, setShowConfirmation] = useState(false);

  const [reloadCounting, setReloadCounting] = useState(0);

  useEffect(() => {
    if (havePermission) {
      handleGetDetail();
    } else {
      setFetching(false);
    }

    // @ts-ignore
    global["operation_detail_cb"] = () => handleCallback();
    return () => {
      // @ts-ignore
      delete global["operation_detail_cb"];
    };
  }, []);

  const getWorkOrderDetail = async (id: string) => {
    if (!id) return;

    let resp = await axiosHandler(() => WorkOrderServices.getWorkOrderDetails(id));

    if (!isMounted()) return;

    let { workOrder }: any = get(resp, "response.data", {});

    if (workOrder) {
      const {
        workOrderId,
        workOrderDescription,
        equipmentId,
        equipmentDescription,
        mainPlant,
        maintenancePlantDescription,
        personnelName,
        commonStatus,
        priorityType,
        priorityText,
        workOrderSystemStatusDescription,
        workOrderTypeDescription,
        mainPerson,
      } = workOrder;

      const woStartDate = dayjsFormatDate(workOrder.woStartDate);
      const woFinishDate = dayjsFormatDate(workOrder.woFinishDate);

      const _workOrdeDetail = {
        id: workOrderId,
        title: workOrderDescription,
        equipment: {
          name: formatObjectLabel(equipmentId, equipmentDescription, true),
          image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
        },
        mainPlant: formatObjectLabel(mainPlant, maintenancePlantDescription),
        createdBy: personnelName,
        timePassed: dayjsTimePassedFromNow(workOrder.woStartDate, "YYYYMMDD"),
        createdDateTxt: `${woStartDate} - ${woFinishDate}`,
        tags: [
          {
            label: priorityText || "",
            color: get(priorityType, "colorCode", "#FD4848"),
          },
          {
            label: workOrderSystemStatusDescription || "",
            color: get(commonStatus, "colorCode", "#FD4848"),
          },
          {
            label: workOrderTypeDescription || "",
            color: "#FD4848",
          },
        ],
        mainPerson,
      };

      setWorkOrderDetail(_workOrdeDetail);
    }
  };

  const handleCallback = () => {
    const callback = getScreenParams(props).callback || "operation_list_cb";
    // @ts-ignore
    (global[callback] || (() => null))();
    handleReloadCurrentOperation;
    handleGetDetail();
  };

  const handleGetDetail = async () => {
    setFetching(true);

    const { operationId, workOrderId } = getScreenParams(props);

    const { response } = await axiosHandler(() => OperationServices.getOperationDetail(workOrderId, operationId));

    if (!isMounted()) return;

    const operation = get(response, "data.operation", null);

    if (isNull(operation)) {
      setFetching(false);
      return;
    }

    setMeasures(get(response, "data.measure.items", []));
    setComponents(get(response, "data.component.item", []));

    const {
      operationDescription,
      equipmentDescription,
      equipmentId,
      workOrderDescription,
      workOrderTypes,
      workCenterCode,
      operationLongText,
      functionalLocationId,
      functionalLocationDescription,
      personnelName,
      commonStatus,
      operationSystemStatus,
      operationSystemStatusDescription,
      documents,
      operationHistory,
      controlKey,
      controlKeyData,
      maintenancePlantCode,
      plantDescription,
      workCenterDescription,
      estimate,
      activityType,
      activityTypeData,
      personnel,
      result,
      varianceReason,
      actualStart,
      actualStartTime,
    } = operation;

    const activity = get(operationHistory, "activity", OperationHistoryActivity.NONE);
    setHistoryActivity(activity);
    setHaveCTA(
      operationSystemStatus !== SystemStatus.operation.CREATE_NEW &&
        !SystemStatus.operation.isCompleted(operationSystemStatus)
    );

    const _detail = {
      id: operationId,
      title: operationDescription,
      operationHistory,
      equipment: {
        name: formatObjectLabel(equipmentId, equipmentDescription, true),
        image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
      },
      workOrder: {
        name: formatObjectLabel(workOrderId, workOrderDescription),
        image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
        id: workOrderId,
      },
      desc: operationLongText,
      maintPlant: {
        name: formatObjectLabel(maintenancePlantCode, plantDescription),
        label: formatObjectLabel(maintenancePlantCode, plantDescription),
        value: maintenancePlantCode,
      },
      workCenter: {
        label: formatWorkCenterLabel(workCenterCode, maintenancePlantCode, workCenterDescription),
        value: workCenterCode,
      },
      controlKey: {
        label: formatObjectLabel(controlKey, controlKeyData?.description),
        value: controlKey,
      },
      reportedBy: {
        fullName: personnelName,
        code: personnel,
        avatar: "https://gravatar.com/avatar/5767c215ab04b526f432520f3f33425c?s=400&d=robohash&r=x",
      },
      tags: [
        {
          label: operationSystemStatusDescription,
          color: get(commonStatus, "colorCode", "#3A73B7"),
        },
        {
          label: result,
          color: varianceReason?.color_code || "#3A73B7",
        },
      ],
      equipmentId: equipmentId,
      equipmentDescription: equipmentDescription,
      functionalLocationId: functionalLocationId,
      functionalLocationDescription: functionalLocationDescription,
      workOrderTypes,
      workCenterCode,
      //-infos
      infos: [
        {
          label: "Nguồn lực",
          value: formatObjectLabel(controlKey, get(controlKeyData, "description", "")),
        },
        {
          label: "Nơi bảo trì",
          value: formatObjectLabel(maintenancePlantCode, plantDescription),
        },
        {
          label: "Tổ đội thực hiện",
          value: formatWorkCenterLabel(workCenterCode, maintenancePlantCode, workCenterDescription),
        },
        { label: "Thời gian làm việc dự kiến", value: estimate || "" },
        {
          label: "Phân loại công việc",
          value: formatObjectLabel(activityType, get(activityTypeData, "description", "")),
        },
      ],
      // attachment
      files: genAttachFiles(documents),
      operationSystemStatus: operationSystemStatus,
      actualStart,
      actualStartTime,
    };
    if (workOrderId) await getWorkOrderDetail(workOrderId);

    if (!isMounted()) return;

    //console.log(_detail, '_detail')
    setDetail(_detail);
    setFetching(false);

    const _subOperation = get(response, "data.subOperation.item");
    setSubSubOperations(
      _subOperation?.map((sub: any) => {
        const uuid = `${sub.workOrderId}_${sub.operationId}_${sub.superOperationId}`;
        const item: SubOperationTaskProps = {
          uuid: uuid,
          id: sub.operationId,
          superOperationId: sub.superOperationId,
          workOrderId: sub.workOrderId,
          title: sub.operationDescription,
          desc: sub.operationLongText,
          operationSystemStatus: sub.operationSystemStatus,
          tags: [
            {
              label: sub.operationSystemStatusDescription,
              color: get(sub, "commonStatus.colorCode", "#3A73B7"),
            },
            {
              label: sub.result,
              color: sub.varianceReason?.color_code || "#3A73B7",
            },
          ],
          assignee: {
            fullName: personnelName,
            avatar: "https://gravatar.com/avatar/5767c215ab04b526f432520f3f33425c?s=400&d=robohash&r=x",
          },

          parentOperationStatus: _detail.operationSystemStatus,
          parentHistoryActivity: activity,
          workCenter: sub.workCenterCode,
          personnel: personnel,
          maintenancePlant: sub.maintenancePlant,
          unit: sub.unit,
          files: genAttachFiles(sub.documents),
        };

        return {
          ...item,
          estimate: `${sub.estimate}`,
          fullDescription: sub.operationLongText || sub.operationDescription,
          maintenancePlantCode: sub.maintenancePlantCode,
          maintenancePlantDesc: sub.maintenancePlantDesc,
          workCenterCode: sub.workCenterCode,
          workCenterDesc: sub.workCenterDesc,
          controlKey: sub.controlKey,
          controlKeyData: sub.controlKeyData,
        };
      }) || []
    );
  };

  const handleChangeMainPerson = async (person: any) => {
    dispatch(CommonActions.openLoading.request({}));

    const { operationId, workOrderId } = getScreenParams(props);

    const { response, error } = await axiosHandler(() =>
      OperationServices.changePersonnelOperationParent(workOrderId, operationId, get(person, "code"))
    );

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (get(response, "status") === 204) {
      toast.show("Đã đổi người thành công", { type: "success" });

      changeScreen(props);
      // @ts-ignore
      (global[`operation_list_cb`] || (() => null))();
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  const handleChange = async (id: any, data: any) => {
    switch (id) {
      case "confirmSubOperationSuccess":
        {
          toast.show("Xác nhận hoàn thành công việc con thành công", { type: "success" });
          handleCallback();
        }
        break;

      case "reloadOpertions":
        {
          handleCallback();
        }
        break;

      case "attachFiles":
        {
          const files = detail.files || [];
          setDetail({ ...detail, files: [...data, ...files] });
        }
        break;

      case "subOperations":
        {
          setSubSubOperations(data);
        }
        break;

      default:
        break;
    }
  };

  const handleReloadCurrentOperation = () => {
    // @ts-ignore
    (global["operation_activity_cb"] || (() => null))();
  };

  const handleStartOperation = async (workOrderId: any, operationId: any) => {
    if (workOrderId && operationId) {
      dispatch(CommonActions.openLoading.request({}));

      const { error } = await axiosHandler(() => OperationServices.startOperation(workOrderId, operationId));

      dispatch(CommonActions.openLoading.success({}));
      if (!isMounted()) return;

      if (error) {
        const result = getErrorDetail(error);
        toast.show(result.errorMessage, { type: "danger" });
      } else {
        toast.show("Đã bắt đầu công việc thành công", { type: "success" });
        handleCallback();
        setHistoryActivity(OperationHistoryActivity.START);
      }
    } else {
      logError("StartOperation", "Work Order ID and Operation ID must not null");
    }
  };

  const handleAction = (_activity: any) => {
    switch (_activity) {
      case OperationHistoryActivity.START:
        handleStartOperation(get(detail, "workOrder.id"), get(detail, "id"));
        break;

      case OperationHistoryActivity.HOlD:
      case OperationHistoryActivity.COMPLETE:
        {
          setConfirmationType(_activity);
          setShowConfirmation(true);
          handleCallback();
        }
        break;

      case "reload":
        {
          handleCallback();
        }
        break;

      default:
        break;
    }
  };

  const canEdit =
    !!detail &&
    PermissionServices.canEditOperation(
      reducer,
      detail?.operationSystemStatus,
      detail?.reportedBy?.code,
      workOrderDetail?.mainPerson
    );

  const _renderHeader = () => {
    return (
      <CNavbar
        title={"Chi tiết công việc"}
        onLeftPress={() => changeScreen(props)}
        {...(canEdit && {
          rightIcon: <ICEdit2 />,
          onRightPress: () =>
            changeScreen(props, ScreenName.OperationCreate, {
              workOrderId: detail?.workOrder?.id,
              operationId: detail?.id,
              isEdit: true,
              callback: "operation_detail_cb",
            }),
        })}
      />
    );
  };

  const _renderInfo = () => {
    const { id, operationSystemStatus, title, maintPlant, tags, workOrder, equipment, reportedBy, desc = "" } = detail;
    //console.log(detail, "detail");

    const canChangeWorker = PermissionServices.canChangeWorker(
      reducer,
      operationSystemStatus,
      historyActivity,
      reportedBy?.code,
      workOrderDetail?.mainPerson
    );

    return (
      <View style={styles.infoWrapper} onLayout={e => setHeaderInfoMinHeight(e.nativeEvent.layout.height)}>
        <View style={styles.content}>
          <View style={styles.equipmentNameContainer}>
            <View style={styles.equipmentNameWrapper}>
              <TextCM style={styles.infoTitle}>{title}</TextCM>

              <TextCM style={styles.infoId}>{id}</TextCM>

              {isArray(tags, 1) && (
                <View style={styles.listItemFooter}>
                  <CTags data={tags} />
                </View>
              )}
            </View>
            {/* <View style={styles.equipmentImageWrapper}>
              <Image source={{ uri: equipment.image }} style={styles.imgEq} />
            </View> */}
          </View>
        </View>

        {!isEmpty(desc) && <TextCM style={[styles.infoDesc, { marginTop: WR(16) }]}>{desc}</TextCM>}

        <TextCM style={styles.txtTabTitle}>Lệnh bảo trì</TextCM>

        <TextCM style={styles.infoDesc}>{workOrder.name}</TextCM>

        {!isEmpty(equipment?.name) && (
          <>
            <TextCM style={styles.txtTabTitle}>Thiết bị</TextCM>

            <TextCM style={styles.infoDesc}>{equipment.name}</TextCM>
          </>
        )}

        <TouchableOpacity style={styles.infoLocation}>
          <ICMap />
          <TextCM style={styles.infoLocationTxt}>{maintPlant.name}</TextCM>
        </TouchableOpacity>

        <View style={styles.infoPersonInCharge}>
          <CPersonInfo {...reportedBy} label={"Đã giao cho"} style={{ flex: 1 }} />

          {canChangeWorker.isShow && (
            <TouchableOpacity
              disabled={canChangeWorker.isDisabled}
              style={[styles.taskAssigneeBtn, canChangeWorker.isDisabled && { opacity: 0.5 }]}
              onPress={() => setChangeWorker(true)}>
              <TextCM style={styles.taskAssigneeBtnTxt}>Đổi người</TextCM>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {(isFetching && (
        <View style={styles.bodyCenter}>
          <CSpinkit />
        </View>
      )) ||
        (detail && (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              height: headerInfoMinHeight + tabViewMinHeight,
            }}>
            {_renderInfo()}
            <CTabViewDetail
              {...{
                saInsets,
                toast,
                detail,
                workOrderDetail,
                subOperations,
                haveCTA,
                measures,
                components,
                employeeCode,
              }}
              index={tabIdx}
              routes={TabViewRoutes}
              canCreateSubOps={canCreateSubOps}
              onChange={handleChange}
              onLayout={setTabViewMinHeight}
              onNavigate={(...args: any) => changeScreen(props, ...args)}
            />
          </ScrollView>
        )) || <CEmptyState noPermission={!havePermission} />}

      {detail && (
        <OperationActionButton
          isSticky={true}
          isBlueStyle={false}
          historyActivity={historyActivity}
          operationStatusCode={get(detail, "operationSystemStatus")}
          handleAction={handleAction}
          actualStart={get(detail, "actualStart")}
          actualStarTime={get(detail, "actualStartTime")}
          isDisabled={employeeCode != detail?.reportedBy?.code}
        />
      )}

      {detail && (
        <CPersonSelector
          open={isChangeWorker}
          title={"Đổi người"}
          selected={detail?.reportedBy}
          workCenterCode={detail?.workCenterCode}
          mainPlantCode={detail?.mainPlant?.code}
          onClose={() => setChangeWorker(false)}
          onSelect={handleChangeMainPerson}
        />
      )}

      {detail && (
        <OperationConfirmation
          saInsets={saInsets}
          workOrderId={detail?.workOrder?.id}
          operationId={detail?.id}
          operationHistory={detail?.operationHistory}
          isShow={isShowConfirmation}
          onChange={() => handleAction("reload")}
          onClose={() => setShowConfirmation(false)}
          type={confirmationType}
          operationDescription={detail?.title}
          toast={toast}
        />
      )}
    </View>
  );
};

export default OprationDetail;

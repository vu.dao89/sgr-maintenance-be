import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  bodyCenter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  dataEmptyTxt: {
    textAlign: "center",
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  infoWrapper: {
    paddingVertical: WR(8),
    paddingHorizontal: WR(24),
  },
  infoTitle: {
    marginBottom: WR(4),
    color: Color.White,
    fontWeight: "700",
    fontSize: FontSize.FontMedium,
  },
  infoId: {
    marginBottom: WR(4),
    color: Color.White,
    fontWeight: "700",
    fontSize: FontSize.FontMedium,
  },
  infoDates: {
    color: "#ACACAC",
    fontSize: FontSize.FontTiny,
  },
  infoDesc: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  infoDesc2: {
    marginBottom: WR(12),
    color: Color.White,
    fontSize: FontSize.FontTinier,
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  infoTags: {
    marginTop: WR(8),
    marginBottom: WR(16),
  },
  infoLocation: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  infoLocationTxt: {
    marginLeft: WR(8),
    color: Color.Yellow,
  },
  infoPersonInCharge: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneeBtn: {
    marginLeft: WR(8),
    height: WR(36),
    paddingHorizontal: WR(16),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#D8D8D8",
  },
  taskAssigneeBtnTxt: {
    fontWeight: "500",
  },
  content: {
    paddingHorizontal: WR(0),
    paddingTop: WR(10),
  },
  equipmentNameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  equipmentNameWrapper: {
    flex: 2,
  },
  equipmentImageWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  imgEq: {
    width: WR(90),
    height: HR(90),
    borderRadius: HR(20),
  },
  txtTabTitle: {
    marginTop: WR(20),
    marginBottom: WR(6),
    fontWeight: "200",
    fontSize: FontSize.FontTiny,
    color: Color.GrayLight,
  },
  startJobBtn: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  ctaButton: {
    flex: 1,
    height: WR(36),
    marginBottom: WR(4),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#4C8BFF",
  },
  ctaButtonTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  btnPause: {
    marginRight: WR(8),
    backgroundColor: "#585858",
  },
  btnPauseTxt: {
    color: "#C6C6C6",
  },
  modalContent: {
    padding: WR(24),
    paddingTop: 0,
  },
  btnSelectImg: {
    marginBottom: WR(16),
  },
  btnSelectImgTxt: {
    color: Color.Yellow,
    fontWeight: "600",
  },
  taskDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  taskAssigneeWrapper: {
    marginTop: WR(20),
    marginBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneePerson: {
    flex: 1,
  },
  taskStartedWrapper: {
    paddingTop: WR(20),
  },
  taskCompleteWrapper: {
    paddingVertical: WR(20),
    borderBottomWidth: 1,
    borderBottomColor: "#414042",
  },
  taskStartedDate: {
    marginBottom: WR(8),
    textAlign: "center",
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  taskOtherJobWrapper: {
    paddingTop: WR(16),
    paddingBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderTopWidth: 1,
    borderTopColor: "#414042",
  },
  taskOtherJobCounter: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  taskViewMoreBtn: {
    flexDirection: "row",
    alignItems: "center",
  },
  taskViewMoreBtnTxt: {
    color: Color.Yellow,
    fontWeight: "500",
  },
});

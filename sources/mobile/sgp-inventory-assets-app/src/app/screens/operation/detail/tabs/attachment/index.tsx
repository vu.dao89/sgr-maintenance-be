import React, { FC } from "react";
import { View } from "react-native";

import styles from "./styles";

import { CAttachFiles, CEmptyState, OperationAttachFiles } from "@Components";
import { SystemStatus, widthResponsive as WR } from "@Constants";
import { get, isEmpty } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import { Permission } from "../../../../../../constants";
import { CommonActions } from "../../../../../../redux/actions";
import { AppState } from "../../../../../../redux/reducers";
import { DocumentServices, PermissionServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted } from "../../../../../../utils/Core";
import { CFile } from "../../../../../components/common/attach-files";

const OperatorAttachment: FC<any> = ({ saInsets, subOperations = [], detail, onChange, haveCTA }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.ATTACHMENT_POST);

  const { id, files = [], workOrder, operationSystemStatus } = detail;
  const isCompleted = SystemStatus.operation.isCompleted(operationSystemStatus);
  const canUpload = havePermission && !!!isCompleted;
  const workOrderId = get(workOrder, "id", "");

  const isEmptyFiles = isEmpty(files) && subOperations?.every((sub: any) => isEmpty(sub?.files));

  const handleUploadOperationDocuments = async (attachFiles: Array<CFile>) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      DocumentServices.uploadOperationDocuments(workOrderId, id, attachFiles)
    );

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      onChange("attachFiles", [...attachFiles, ...files]);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  const handleUploadSubOperationDocuments = async (index: any, subOperationId: string, attachFiles: Array<CFile>) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      DocumentServices.uploadSubOperationDocuments(workOrderId, subOperationId, id, attachFiles)
    );

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      const sub = subOperations[index];
      const files = sub.files || [];
      Object.assign(subOperations[index], { ...sub, files: [...attachFiles, ...files] });
      onChange("subOperations", [...subOperations]);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: haveCTA ? saInsets.bottom + WR(80) : saInsets.bottom }]}>
      <View style={styles.attachFiles}>
        <CAttachFiles files={files} onChange={canUpload && handleUploadOperationDocuments} />
      </View>

      {subOperations.map((task: any, index: any) => {
        if (!canUpload && isEmpty(task?.files)) return null;
        return (
          <OperationAttachFiles
            {...{ task }}
            key={task.uuid}
            style={styles.attachFiles}
            onChange={
              canUpload &&
              ((attachFiles: any) => {
                handleUploadSubOperationDocuments(index, task.id, attachFiles);
              })
            }
          />
        );
      })}

      {isEmptyFiles && !canUpload && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default OperatorAttachment;

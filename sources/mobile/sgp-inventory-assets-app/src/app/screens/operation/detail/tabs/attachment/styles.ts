import { StyleSheet } from "react-native";

import { widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  attachFiles: {
    marginTop: WR(12),
  },
});

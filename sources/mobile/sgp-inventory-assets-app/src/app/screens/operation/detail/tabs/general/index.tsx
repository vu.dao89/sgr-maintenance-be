import React, { FC } from "react";

import { View } from "react-native";

import styles from "./styles";

import { CCollapsibleSection, CTags, TextCM } from "@Components";
import { ScreenName, widthResponsive as WR } from "@Constants";
import { useNavigation } from "@react-navigation/native";
import { isEmpty } from "lodash";
import { TouchableOpacity } from "react-native-gesture-handler";
import { formatObjectLabel } from "../../../../../../utils/Format";

const OperatorGeneral: FC<any> = ({ workOrderDetail, saInsets, haveCTA, detail }) => {
  const nav = useNavigation();

  const { equipmentName, maintPlant, woCreatedBy, createdDateTxt, timePassed } = workOrderDetail;

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom: haveCTA ? saInsets.bottom + WR(90) : saInsets.bottom,
        },
      ]}>
      {workOrderDetail && (
        <CCollapsibleSection title={"Thuộc lệnh bảo trì"}>
          <TouchableOpacity
            onPress={() => {
              nav.navigate(
                ScreenName.WorkOrderDetail as never,
                {
                  id: workOrderDetail?.id,
                } as never
              );
            }}>
            <View style={styles.listItemHeader}>
              <View style={styles.listItemContent}>
                <TextCM style={styles.listItemTitle}>
                  {formatObjectLabel(workOrderDetail?.id, workOrderDetail?.title)}
                </TextCM>
                {[
                  ["Thiết bị", equipmentName],
                  ["Khu vực", maintPlant],
                  ["Tạo bởi", woCreatedBy],
                  ["Thời gian", createdDateTxt],
                ]
                  .filter(([_, value]) => !isEmpty(value))
                  .map(([label, value]) => (
                    <View key={label} style={[styles.listItemInfoRow, { marginTop: 0 }]}>
                      <TextCM style={styles.listItemInfoLabel}>{label}: </TextCM>
                      <TextCM style={styles.listItemInfoValue}>{value}</TextCM>
                    </View>
                  ))}
              </View>
            </View>

            <View style={styles.listItemFooter}>
              <CTags data={workOrderDetail?.tags} />
              <TextCM style={styles.listItemTimePassed}>{timePassed}</TextCM>
            </View>
          </TouchableOpacity>
        </CCollapsibleSection>
      )}

      <CCollapsibleSection title={"Thông tin chính"} infos={detail.infos || []} />
    </View>
  );
};

export default OperatorGeneral;

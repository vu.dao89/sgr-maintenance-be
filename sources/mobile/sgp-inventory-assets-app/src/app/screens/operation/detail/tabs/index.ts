import { default as OperatorGeneral } from "./general";
import { default as OperatorTask } from "./task";
import { default as OperatorAttachment } from "./attachment";
import { default as OperatorParameter } from "./parameter";
import { default as OperatorSupplies } from "./supplies";

export default [
  {
    key: "0",
    padding: 24,
    title: "Tổng quan",
    Scene: OperatorGeneral,
  },
  {
    key: "1",
    padding: 30,
    title: "Công việc con",
    Scene: OperatorTask,
  },
  {
    key: "2",
    padding: 30,
    title: "Thông số",
    Scene: OperatorParameter,
  },
  {
    key: "3",
    padding: 30,
    title: "Vật tư",
    Scene: OperatorSupplies,
  },
  {
    key: "4",
    padding: 30,
    title: "Tài liệu",
    Scene: OperatorAttachment,
  },
];

import { widthResponsive as WR } from "@Constants";
import React, { FC, Fragment, useRef, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import styles from "./styles";

import {
  CCollapsibleSection,
  CEmptyState,
  CInfoViewer,
  CModalViewer,
  CRadioSelect,
  CTAButtons,
  CTextInput,
  TextCM,
} from "@Components";
import { get, isEmpty } from "lodash";
import moment from "moment";
import Toast from "react-native-toast-notifications";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { MeasPointUpdateParams, OperationServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { genShortDescription, isArray } from "../../../../../../utils/Basic";
import { useIsMounted } from "../../../../../../utils/Core";

const selections = [
  {
    value: false,
    label: "Giá trị đo hiện tại",
  },
  {
    value: true,
    label: "Giá trị đo khác biệt",
  },
];

const OperatorParameter: FC<any> = ({
  saInsets,
  title: tabTitle,
  measures = [],
  workOrderDetail,
  employeeCode,
  onChange,
  haveCTA,
}) => {
  const [isCRUD, setShowCRUD]: any = useState(false);
  const [crudItem, setCrudItem]: any = useState({});
  const [inValidMeasuringPoint, setInValidMeasuringPoint] = useState(false);

  const dispatch = useDispatch();
  const isMounted = useIsMounted();
  const toastRef: any = useRef();

  const openCRUDMeasuringPoint = (item: any) => {
    const itemData = {
      measurePointPosition: item.measurePointPosition,
      lowerRange: item.lowerRange,
      upperRange: item.upperRange,
      counter: item.counter,
      isDiff: false,
      measurePoint: item.measurePoint,
      decs: item.description,
      operationId: item.operationId,
    };
    setCrudItem({ ...itemData });
    setShowCRUD(true);

    console.log(crudItem, "crudItem");
  };

  const postMeasPoint = async () => {
    if (!crudItem) return;
    const isValidMs = crudItem.point && parseInt(crudItem.point) > 0 && !isEmpty(crudItem.desc);
    if (!isValidMs) {
      setInValidMeasuringPoint(true);
      return;
    }

    if (
      parseInt(crudItem.lowerRange) &&
      parseInt(crudItem.upperRange) &&
      (parseInt(crudItem.point) < parseInt(crudItem.lowerRange) ||
        parseInt(crudItem.point) > parseInt(crudItem.upperRange))
    ) {
      toastRef.current.show("Giá trị do không hợp lệ", { type: "danger" });
      return;
    }
    setInValidMeasuringPoint(false);

    dispatch(CommonActions.openLoading.request({}));

    const params: MeasPointUpdateParams = {
      measPoint: crudItem.measurePoint,
      measDocDate: moment(new Date()).format("YYYYMMDD"),
      measDocTime: moment(new Date()).format("HHmmss"),
      measDocText: genShortDescription(crudItem.desc),
      measDocLText: crudItem.desc,
      readBy: employeeCode,
      measRead: "",
      countRead: "",
      diff: "",
      workOrder: workOrderDetail.id,
      operation: crudItem.operationId,
      countReadId: "",
      isMeasDiff: crudItem.counter === "X" && crudItem.isDiff ? true : false,
    };
    if (crudItem.counter === "X") {
      params.diff = crudItem.isDiff ? crudItem.point : "";
      params.countRead = !crudItem.isDiff ? crudItem.point : "";
    } else {
      params.measRead = crudItem.point;
    }
    const { response, error } = await axiosHandler(() => OperationServices.postMeasuringPoint(params));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    const code = get(response, "data.code", 0);
    if (code === "S") {
      toastRef.current.show("Nhập kết quả thành công", { type: "success" });
      onChange("reloadOpertions");
    } else {
      const errorMessage = getErrorDetail(error)?.errorMessage;
      toastRef.current.show(errorMessage, { type: "danger" });
    }
  };

  return (
    <Fragment>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          {
            paddingVertical: WR(20),
            paddingBottom: haveCTA ? saInsets.bottom + WR(88) : saInsets.bottom,
          },
        ]}>
        {isArray(measures, 1) ? (
          measures.map((item: any, index: any) => {
            return (
              <CCollapsibleSection
                key={index}
                title={`Thông số ${index + 1}`}
                infos={[
                  {
                    label: "Tiêu chí",
                    value: item.measurePointPosition,
                  },
                  {
                    isGrouped: true,
                    label: "Đơn vị thấp nhất",
                    value: `${item.lowerRange} ${item.rangeUnit}`,
                  },
                  {
                    isGrouped: true,
                    label: "Đơn vị cao nhất",
                    value: `${item.upperRange} ${item.rangeUnit}`,
                  },
                  {
                    label: "Kết quả đo",
                    value: item.result,
                  },
                  {
                    label: "Ghi chứ",
                    value: item.description,
                  },
                ]}
                children={
                  !item.result && (
                    <>
                      <TouchableOpacity
                        style={styles.ctaButton}
                        onPress={() => {
                          openCRUDMeasuringPoint(item);
                        }}>
                        <TextCM style={styles.ctaButtonTxt}>Nhập kết quả</TextCM>
                      </TouchableOpacity>
                    </>
                  )
                }
              />
            );
          })
        ) : (
          <>
            <CEmptyState noPermission={false} />
          </>
        )}
      </ScrollView>

      <CModalViewer
        open={isCRUD}
        placement={"bottom"}
        title={"Nhập thông số kỹ thuật"}
        onBackdropPress={() => setShowCRUD(false)}>
        <Toast ref={(ref: any) => (toastRef.current = ref)} />

        <ScrollView contentContainerStyle={styles.crudModalContent}>
          <View style={{ flexDirection: "row" }}>
            <CInfoViewer
              infos={[
                {
                  label: "Tiêu chí",
                  value: crudItem.measurePointPosition,
                },
                {
                  label: "Giá trị đo nhỏ nhất",
                  value: crudItem.lowerRange,
                  isGrouped: true,
                },
                {
                  label: "Giá trị đo lớn nhất",
                  value: crudItem.upperRange,
                  isGrouped: true,
                },
              ]}
            />
          </View>
          {crudItem.counter === "X" && (
            <>
              <View style={{ flexDirection: "row" }}>
                <CRadioSelect
                  options={selections}
                  selected={crudItem.isDiff}
                  onSelect={() => {
                    setCrudItem({ ...crudItem, isDiff: !crudItem.isDiff });
                  }}
                  styles={{
                    wrapper: styles.radioBtnCtn,
                    item: styles.radioChild,
                  }}
                />
              </View>
            </>
          )}
          <View style={[{ paddingRight: WR(8) }, { marginTop: WR(16) }]}>
            <CTextInput
              isRequired
              fullWidth
              label={"Kết quả đo"}
              plh={"Nhập kết quả (mm)"}
              keyboardType="numeric"
              value={crudItem.point}
              isError={inValidMeasuringPoint}
              styles={{ wrapper: { marginRight: WR(8), marginTop: WR(8) } }}
              onChange={(point: any) => setCrudItem({ ...crudItem, point: point })}
            />
          </View>
          <View>
            <CTextInput
              isRequired
              multiline={3}
              label={"Mô tả"}
              plh={"Nhập mô tả chi tiết"}
              value={crudItem.desc}
              isError={inValidMeasuringPoint}
              onChange={(text: any) => setCrudItem({ ...crudItem, desc: text })}
            />
          </View>
        </ScrollView>
        <CTAButtons
          style={{ paddingBottom: saInsets.bottom || WR(12) }}
          buttons={[
            {
              isSub: true,
              text: "Thôi",
              onPress: () => setShowCRUD(false),
            },
            {
              text: "Nhập",
              onPress: () => {
                postMeasPoint();
                // setShowCRUD(false);
              },
            },
          ]}
        />
      </CModalViewer>
    </Fragment>
  );
};

export default OperatorParameter;

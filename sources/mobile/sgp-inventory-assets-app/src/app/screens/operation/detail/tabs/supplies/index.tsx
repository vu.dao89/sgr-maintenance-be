import React, { FC, Fragment } from "react";
import { View } from "react-native";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, CEmptyState } from "@Components";
import { formatObjectLabel } from "src/utils/Format";
import { isArray } from "../../../../../../utils/Basic";
import { dayjsFormatDate } from "../../../../../../utils/DateTime";

const OperatorSupplies: FC<any> = ({ saInsets, haveCTA, components, detail, onChange }) => {
  const { id, title } = detail;

  const genIdx = (index: any) => {
    return components.length > 1 ? ` ${index + 1}` : "";
  };

  return (
    <Fragment>
      <View
        style={[
          styles.container,
          {
            paddingBottom: haveCTA ? saInsets.bottom + WR(90) : saInsets.bottom,
          },
        ]}>
        {isArray(components, 1) &&
          components.map((item: any, index: any) => {
            return (
              <CCollapsibleSection
                key={index}
                title={`Vật tư${genIdx(index)}: ${formatObjectLabel(item.material, item.materialDescription, true)}`}
                infos={[
                  {
                    label: "Công việc liên quan",
                    value: formatObjectLabel(id, title),
                  },
                  {
                    label: "Kho bảo trì",
                    value: formatObjectLabel(item.plant, item.plantName, true),
                  },
                  {
                    label: "Kho vật lý",
                    value: formatObjectLabel(`${item.storageLocation}`, item.storageLocationDescription, true),
                  },
                  {
                    label: "Lô hàng",
                    value: `${dayjsFormatDate(item.batch)}`,
                  },
                  {
                    label: "Số lượng yêu cầu",
                    value: `${+item.quantity} ${item.unit}`,
                  },
                  {
                    label: "Số lượng đã xuất kho",
                    value: `${+item.withdrawalQuantity} ${item.unit}`,
                  },
                  {
                    label: "Ghi chú",
                    value: item.note,
                  },
                ]}
              />
            );
          })}
        {!isArray(components, 1) && (
          <>
            <CEmptyState noPermission={false} />
          </>
        )}
      </View>
    </Fragment>
  );
};

export default OperatorSupplies;

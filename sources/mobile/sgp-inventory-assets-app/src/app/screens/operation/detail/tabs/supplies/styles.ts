import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingHorizontal: WR(24),
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(4),
  },
  ctaButtonTxt: {
    color: Color.White,
    fontWeight: "500",
  },
  ctaButton: {
    height: HR(36),
    marginBottom: WR(4),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#4C8BFF",
  },
});

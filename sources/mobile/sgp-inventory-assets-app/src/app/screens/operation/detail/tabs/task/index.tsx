import React, { FC, useState } from "react";

import { TouchableOpacity, View } from "react-native";

import { Color } from "@Constants";

import styles from "./styles";

import { CAlertModal, SubOperationCreation, SubOperationTaskItem, TextCM } from "@Components";

import { widthResponsive as WR } from "@Constants";
import { get, isNull } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch } from "react-redux";
import { ICWarningOctagon } from "../../../../../../assets/image";
import { CommonActions } from "../../../../../../redux/actions";
import { OperationServices, SubOperationCreateParams } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted, useStateLazy } from "../../../../../../utils/Core";
import { formatObjectLabel, formatWorkCenterLabel } from "../../../../../../utils/Format";

const OperatorTask: FC<any> = ({ saInsets, subOperations = [], detail, onChange, haveCTA, canCreateSubOps }) => {
  const dispatch = useDispatch();
  const isMounted = useIsMounted();
  const toast = useToast();

  const [isCrudChild, setCrudChildStatus]: any = useStateLazy(false);
  const [childAction, setChildAction]: any = useStateLazy("create");
  const [childState, setChildState]: any = useStateLazy({});

  const [isFetching, setFetching]: any = useState(false);
  const [confirmDelete, setConfirmDelete]: any = useState(null);

  const {
    id: superOperationId,
    maintPlant,
    workCenter,
    controlKey,
    workOrder,
    equipmentId,
    functionalLocationId,
    reportedBy,
  } = detail;

  const handleCreateSubOperation = async (child: any) => {
    setFetching(true);

    const params: SubOperationCreateParams = {
      workOrderId: workOrder?.id,
      superOperationId: superOperationId,
      operationLongText: child?.fullDescription,
      controlKey: child?.controlKey?.value,
      estimate: child?.estimate,
      equipmentId: equipmentId,
      maintenancePlantId: child?.maintPlant?.value,
      workCenterId: child?.workCenter?.value,
      functionalLocationId: functionalLocationId,
      personnel: reportedBy?.code,
    };
    const { response, error } = await axiosHandler(() => OperationServices.createSubOperation(params));

    setFetching(false);
    dispatch(CommonActions.openLoading.request({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      const document = get(response, "data.document", "");
      toast.show(`Đã tạo công việc con (${document}) thành công`, { type: "success" });
      onChange("reloadOpertions");
    }
    dispatch(CommonActions.openLoading.success({}));
  };

  const handleSubOperationChange = async (child: any) => {
    setFetching(true);

    const params: SubOperationCreateParams = {
      workOrderId: workOrder?.id,
      superOperationId: superOperationId,
      operationLongText: child?.fullDescription,
      controlKey: child?.controlKey?.value,
      estimate: child?.estimate,
      equipmentId: equipmentId,
      maintenancePlantId: child?.maintPlant?.value,
      workCenterId: child?.workCenter?.value,
      functionalLocationId: functionalLocationId,
      personnel: reportedBy?.code,
    };

    const { error } = await axiosHandler(() => OperationServices.updateSubOperation(child?.id, params));

    setFetching(false);
    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toast.show("Đã cập nhật công việc con thành công", { type: "success" });
      onChange("reloadOpertions");
    }
  };

  const handleDeleteOperation = async () => {
    setFetching(true);

    const index = confirmDelete;
    const child = subOperations[index];

    setConfirmDelete(null);

    const { error } = await axiosHandler(() =>
      OperationServices.deleteSubOperation(workOrder?.id, child?.id, superOperationId)
    );

    setFetching(false);
    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      toast.show("Đã xoá công việc con thành công", { type: "success" });
      onChange("reloadOpertions");
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: haveCTA ? saInsets.bottom + WR(74) : saInsets.bottom }]}>
      {canCreateSubOps && (
        <TouchableOpacity
          style={styles.btnAddItem}
          onPress={() => {
            setChildState({
              maintPlant: maintPlant,
              workCenter: workCenter,
              controlKey: controlKey,
            });
            setChildAction("create");
            setCrudChildStatus(true);
          }}>
          <TextCM style={styles.btnAddTxt}>+ Thêm công việc con</TextCM>
        </TouchableOpacity>
      )}

      {subOperations.map((item: any, index: any) => (
        <SubOperationTaskItem
          isCRUD
          key={`sub_operation_${item.uuid}_${index}`}
          onChange={onChange}
          menus={[
            {
              label: "Sửa",
              value: "edit",
            },
            {
              label: "Xoá",
              value: "delete",
              color: Color.Red,
            },
          ]}
          onMenu={(menu: any) => {
            if (menu.value === "delete") {
              setConfirmDelete(index);
            } else {
              setChildState({
                id: item.id,
                parentIndex: index,
                maintPlant: {
                  label: formatObjectLabel(item?.maintenancePlantCode, item?.maintenancePlantDesc),
                  value: item?.maintenancePlantCode,
                },
                workCenter: {
                  label: formatWorkCenterLabel(item?.workCenterCode, item?.maintenancePlantCode, item?.workCenterDesc),
                  value: item?.workCenterCode,
                },
                controlKey: {
                  label: formatObjectLabel(item?.controlKey, item?.controlKeyData?.description),
                  value: item?.controlKey,
                },
                estimate: item.estimate,
                fullDescription: item.fullDescription,
              });
              setChildAction("edit");
              setCrudChildStatus(true);
            }
          }}
          {...item}
        />
      ))}

      <SubOperationCreation
        saInsets={saInsets}
        isCrud={isCrudChild}
        childAction={childAction}
        initState={childState}
        onClose={() => setCrudChildStatus(false)}
        onChange={(child: any) => {
          if (childAction === "create") {
            handleCreateSubOperation(child);
          } else {
            handleSubOperationChange(child);
          }
        }}
        onModalHide={() => {
          if (isFetching) {
            dispatch(CommonActions.openLoading.request({}));
          }
        }}
      />

      <CAlertModal
        open={!isNull(confirmDelete)}
        title={"Bạn có chắc muốn xoá công việc này? Công việc đã xoá không thể phục hồi"}
        icon={<ICWarningOctagon />}
        onClose={() => setConfirmDelete(null)}
        onCta={handleDeleteOperation}
        onModalHide={() => {
          if (isFetching) dispatch(CommonActions.openLoading.request({}));
        }}
      />
    </View>
  );
};

export default OperatorTask;

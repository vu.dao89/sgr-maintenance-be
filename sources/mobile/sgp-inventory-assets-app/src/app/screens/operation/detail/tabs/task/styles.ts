import { StyleSheet } from "react-native";

import { Color, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(24),
    paddingHorizontal: WR(24),
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(16),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
});

import dayjs from "dayjs";
import { get, isEmpty } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { ScrollView, StatusBar, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";

import i18n from "@I18n";

import { EquipmentServices, MasterDataServices, OperationServices, PermissionServices } from "@Services";
import { axiosHandler, getPagination } from "@Services/httpClient";

import { AppState } from "@Reducers";
import { CommonActions } from "src/redux/actions";

import { isArray } from "src/utils/Basic";
import { isCloseToBottom, useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsFormatDate, formatSapDateTime, SAP_DATE_FORMAT } from "src/utils/DateTime";
import { convertFilterMultiValue, formatObjectLabel } from "src/utils/Format";
import { changeScreen } from "src/utils/Screen";

import styles from "./styles";

import { Permission, ScreenName, widthResponsive as WR } from "@Constants";

import {
  CAlertModal,
  CEmptyState,
  CFilterModal,
  CMetricSection,
  CNavbar,
  CRefreshControl,
  CScanQRCodeModal,
  CSpinkit,
  OperationActivity,
  SearchInput,
} from "@Components";

import { ICGhost, ICPlus, ICQRC } from "@Assets/image";

import {
  BaseParams,
  CommonStatusParams,
  EquipmentSearchParams,
  FunctionalLocationParams,
  MetricParams,
  OperationSearchParams,
  PersonnelParams,
  PriorityParams,
} from "@Services/parameters";

import OperationTaskItem, { OperationTaskItemProps } from "../../../components/operation/task-item";

const OperationList: FC<any> = props => {
  const isMounted = useIsMounted();
  const saInsets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const filterModalRef: any = useRef();

  const [isFetchingList, setFetchingList] = useStateLazy(true);
  const [pagination, setPagination] = useStateLazy(getPagination());
  const [operations, setOperations] = useStateLazy([]);
  const [keyword, setKeyword]: any = useState("");

  const [isFetchingMetric, setFetchingMetric] = useStateLazy(false);
  const [metric, setMetric]: any = useState([]);
  const [dates, setDates] = useStateLazy([dayjs().subtract(1, "month").toDate(), new Date()]);

  const [isShowActivity, setShowActivity] = useState(false);
  const [isShowQRScan, setShowQRScan] = useState(false);
  const [isScanErrorAlert, setScanErrorAlert] = useState(false);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const personPermission = PermissionServices.getPersonPermission(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.SEARCH);

  const filterOptions = [
    {
      id: "workCenters",
      label: "Theo tổ đội",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getWorkCenters(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { id: item.id, value: item.code, label: item.description + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "equipment",
      label: "Theo thiết bị",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, functionalPlace, equipmentType } = filterSelecteds;
        const param: EquipmentSearchParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          functionalLocationCodes: convertFilterMultiValue(functionalPlace),
          objectTypeCodes: convertFilterMultiValue(equipmentType),
          ...params,
        };
        const { response } = await axiosHandler(() => EquipmentServices.getEquipments(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.equipmentId, label: item.description + " (" + item.equipmentId + ")" };
          })
        );
      },
    },
    {
      id: "functionalPlace",
      label: "Theo khu vực chức năng",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;
        const param: FunctionalLocationParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getFunctionalLocations(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              value: item.functionalLocationId,
              label: item.description + " (" + item.functionalLocationId + ")",
            };
          })
        );
      },
    },
    {
      id: "status",
      label: "Theo trạng thái",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: CommonStatusParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          codes: ["3 - Operation"], //TODO: remove soon
        };

        const { response } = await axiosHandler(() => MasterDataServices.getCommonStatus(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.status, label: item.description + " (" + item.status + ")" };
          })
        );
      },
    },
    {
      id: "priority",
      label: "Theo mức độ ưu tiên",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, type } = filterSelecteds;

        const param: PriorityParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          types: convertFilterMultiValue(type),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPriorities(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.priority, label: `${item.description} (${item.type}-${item.priority})` };
          })
        );
      },
    },
    {
      id: "objectTypes",
      label: "Theo nhóm thiết bị",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getObjectTypes(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { id: item.id, value: item.code, label: item.description + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "equipmentCategories",
      label: "Theo phân loại thiết bị",
      onSearch: async (params: any, cb: any) => {
        const { response } = await axiosHandler(() => MasterDataServices.getEquipmentCategories(0));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { id: item.id, value: item.code, label: item.description + " (" + item.code + ")" };
          })
        );
      },
    },
    {
      id: "responsiblePerson",
      label: "Theo người chịu trách nhiệm",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, workCenter } = filterSelecteds;

        const param: PersonnelParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          workCenterIds: convertFilterMultiValue(workCenter),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPersonnel(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: item.name + " (" + item.code + ")" };
          })
        );
      },
    },
  ];
  const [filterSelecteds, setFilterSelecteds]: any = useStateLazy({
    fromDate: dates[0],
    toDate: dates[1],
  });

  useEffect(() => {
    handleGetMetric();
  }, [dates]);

  useEffect(() => {
    if (havePermission) {
      handleSetDates();
      handleGetList();
    } else {
      setFetchingList(false);
    }

    // @ts-ignore
    global["operation_list_cb"] = () => handleGetList();
    return () => {
      // @ts-ignore
      delete global["operation_list_cb"];
    };
  }, [filterSelecteds, keyword]);

  const handleSetDates = () => {
    if (dates[0] === filterSelecteds.fromDate && dates[1] === filterSelecteds.toDate) return;
    setDates([filterSelecteds.fromDate, filterSelecteds.toDate]);
  };

  const handleGetMetric = async () => {
    if (!havePermission) return;

    await setFetchingMetric(true);

    let params: MetricParams = {
      mainPlantIds: userPlants,
      fromDate: dayjsFormatDate(dates[0], SAP_DATE_FORMAT),
      toDate: dayjsFormatDate(dates[1], SAP_DATE_FORMAT),
      mainPerson: personPermission,
    };
    const { response } = await axiosHandler(() => OperationServices.getOperationMetrics(params), { delay: true });
    if (!isMounted()) return;

    const data = get(response, "data.operation.item", []);
    setMetric(data);
    setFetchingMetric(false);
  };

  const handleScanResult = async (result: any) => {
    setShowQRScan(false);

    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => MasterDataServices.getByQRCode(result), { delay: true });
    let equipmentId = get(response, ["data", "equipment", "equipmentId"], "");
    let functionalLocationId = get(response, ["data", "functionalLocation", "functionalLocationId"], "");

    dispatch(CommonActions.openLoading.success({}));

    if (!isEmpty(equipmentId)) {
      let equipmentDesc = get(response, ["data", "equipment", "description"], "");
      setFilterSelecteds({
        ...filterSelecteds,
        equipment: { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDesc) },
      });
    } else if (!isEmpty(functionalLocationId)) {
      let functionalLocationDesc = get(response, ["data", "functionalLocation", "description"], "");
      setFilterSelecteds({
        ...filterSelecteds,
        equipment: {
          value: functionalLocationId,
          label: formatObjectLabel(functionalLocationId, functionalLocationDesc),
        },
      });
    } else {
      setScanErrorAlert(true);
    }
  };

  const handleGetList = async (page: any = 1) => {
    if (!havePermission) return;
    const { workCenters, equipment, functionalPlace, status, priority, responsiblePerson, fromDate, toDate } =
      filterSelecteds;

    if (page === 1) {
      await setOperations([]);
      await setPagination(getPagination());
    }

    await setFetchingList(true);

    let params: OperationSearchParams = {
      keyword,
      priorityIds: convertFilterMultiValue(priority),
      equipmentIds: convertFilterMultiValue(equipment),
      functionLocationIds: convertFilterMultiValue(functionalPlace),
      status: convertFilterMultiValue(status),
      personnels: convertFilterMultiValue(responsiblePerson),
      workCenterIds: convertFilterMultiValue(workCenters),
      maintenancePlantCodes: userPlants,
      workOrderFinishFrom: dayjsFormatDate(fromDate, SAP_DATE_FORMAT),
      workOrderFinishTo: dayjsFormatDate(toDate, SAP_DATE_FORMAT),
    };

    const { response } = await axiosHandler(() => OperationServices.searchOperation(params, personPermission), {
      delay: true,
    });

    if (!isMounted()) return;

    let data: any = get(response, "data.items", []);

    let nextPage: any = [];
    if (isArray(data, 1)) {
      data.forEach((item: any) => {
        const {
          operationId,
          workOrderId,
          operationDescription,
          operationLongText,
          personnel,
          personnelName,
          prtRequire,
          prtConfirm,
          reservation,
          subOperationRequire,
          commonStatus,
          subOperationConfirm,
          maintenancePlantCode,
          workCenterCode,
          operationHistory,
          operationSystemStatus,
          actualStart,
          actualStarTime,
          actualFinish,
          actualFinishTime,
          subOperation,
          reservationWithdraw,
          operationSystemStatusDescription,
          result,
          varianceReason,
          estimate,
          mainPerson,
        } = item;

        const taskItemProps: OperationTaskItemProps = {
          id: operationId,
          workOrderId: workOrderId,
          title: operationDescription,
          desc: operationLongText,
          tags: [
            {
              label: operationSystemStatusDescription,
              color: commonStatus?.colorCode ? `#${commonStatus.colorCode}` : "#3A73B7",
            },
            {
              label: result,
              color: varianceReason?.color_code || "#3A73B7",
            },
          ],
          assignee: {
            code: personnel,
            fullName: personnelName,
            avatar:
              "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e18f3f19-fcd2-4e22-819c-d185fb8f8b9a/da7akca-84816f09-4e19-49a0-8a38-a371bc2ea115.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2UxOGYzZjE5LWZjZDItNGUyMi04MTljLWQxODVmYjhmOGI5YVwvZGE3YWtjYS04NDgxNmYwOS00ZTE5LTQ5YTAtOGEzOC1hMzcxYmMyZWExMTUucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.W_fGZzsHXglkJSOW3FgL4PmiKh6T5tgBMzlre7tUBZk",
          },
          prtRequire: prtRequire,
          reservation: reservation,
          subOperationRequire: subOperationRequire,
          subOperationConfirm: subOperationConfirm,
          mainPlantCode: maintenancePlantCode,
          workCenterCode: workCenterCode,
          subTasks: subOperation?.item || [],
          operationStatusCode: operationSystemStatus,
          history: operationHistory,
          workOrderStatusCode: "",
          actualStartDateTime: formatSapDateTime(actualStart, actualStarTime),
          actualFinishDateTime: formatSapDateTime(actualFinish, actualFinishTime),
          actualStart: actualStart,
          actualStarTime: actualStarTime,
          prtConfirm: prtConfirm,
          reservationWithdraw: reservationWithdraw,
          infos: [
            {
              label: "Lệnh bảo trì",
              value: formatObjectLabel(workOrderId, item?.workOrderDescription, true),
            },
            {
              label: "Khu vực",
              value: formatObjectLabel(item?.functionalLocationId, item?.functionalLocationDescription, true),
            },
            {
              label: "Thiết bị",
              value: formatObjectLabel(item?.equipmentId, item?.equipmentDescription, true),
            },
            {
              label: "Thời gian dự kiến",
              value: estimate ? `${estimate} phút` : "",
            },
          ],
          callback: "operation_list_cb",
          supervisor: mainPerson,
        };

        nextPage.push(taskItemProps);
      });
    }

    setPagination(getPagination(get(response, "data.pagination", {})));
    setOperations((curPage: any) => [...curPage, ...nextPage]);
    setFetchingList(false);
  };

  const handleOnReachedEnd = (e: any) => {
    if (!isFetchingList && pagination.hasNext && isCloseToBottom(e)) {
      handleGetList(pagination.nextPage);
    }
  };

  const _renderHeader = () => {
    const title =
      pagination.totalItems > 0 ? `${i18n.t("CM.operation")} (${pagination.totalItems})` : i18n.t("CM.operation");

    return (
      <CNavbar
        title={title}
        onLeftPress={() => changeScreen(props)}
        rightBtnNode={
          havePermission && (
            <CFilterModal
              showToggle
              ref={filterModalRef}
              title="Bộ lọc công việc"
              options={filterOptions}
              selecteds={filterSelecteds}
              onChange={setFilterSelecteds}
            />
          )
        }
      />
    );
  };

  const _renderSearch = () => {
    return (
      havePermission && (
        <View style={styles.searchBar}>
          <SearchInput
            placeholder={"Nhập mã hoặc tiêu đề"}
            wrapperStyle={styles.searchBarInput}
            onTextSearch={(_keyword: string) => {
              setKeyword(_keyword);
            }}
          />
          <TouchableOpacity style={styles.searchBarBtn} onPress={() => setShowQRScan(true)}>
            <ICQRC />
          </TouchableOpacity>
        </View>
      )
    );
  };

  const _renderMetric = () => {
    return (
      <CMetricSection
        metric={metric}
        dates={dates}
        fetching={isFetchingMetric}
        countPriorityKey={"operationCountPriority"}
        countStatusKey={"operationCountStatus"}
        onFilter={(quickSelections: any) => {
          let nextSelecteds = {
            ...filterSelecteds,
            ...quickSelections,
          };
          setFilterSelecteds(nextSelecteds, () => {
            filterModalRef.current.setSelected(nextSelecteds);
          });
        }}
      />
    );
  };

  const _renderBody = () => {
    let haveData = isArray(operations, 1);
    return (
      <ScrollView
        scrollEventThrottle={50}
        contentContainerStyle={{ paddingBottom: saInsets.bottom + WR(56 / 2 + 16) + (isShowActivity ? WR(32) : 0) }}
        refreshControl={
          <CRefreshControl
            onRefresh={() => {
              handleGetMetric();
              handleGetList();
            }}
          />
        }
        onScroll={handleOnReachedEnd}>
        {_renderMetric()}
        {operations.map((item: any, index: any) => (
          <View key={item.id + "_" + String(index)} style={{ paddingHorizontal: WR(24) }}>
            <OperationTaskItem
              {...{ item, index }}
              handleAction={(activity: string) => {
                switch (activity) {
                  case "reload":
                    handleGetList();
                    break;
                  case "":
                  default:
                    break;
                }
              }}
              {...item}
            />
          </View>
        ))}
        {isFetchingList && (
          <View style={styles.dataFetching}>
            <CSpinkit />
          </View>
        )}
        {!isFetchingList && !haveData && <CEmptyState noPermission={!havePermission} />}
      </ScrollView>
    );
  };

  const _renderAddBtn = () => {
    if (!PermissionServices.checkPermission(reducer, Permission.OPERATION.POST)) return;
    return (
      <TouchableOpacity
        style={[styles.overlayAddBtn, isShowActivity && { bottom: saInsets.bottom + WR(64 + 24) }]}
        onPress={() => changeScreen(props, ScreenName.OperationCreate)}>
        <ICPlus />
      </TouchableOpacity>
    );
  };

  const _renderActivity = () => {
    return (
      <OperationActivity
        {...{ saInsets }}
        isShow={isShowActivity}
        onVisible={(isShow: any) => setShowActivity(isShow)}
        onChange={() => {
          setFetchingList(true);
          handleGetList();
        }}
      />
    );
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isShowQRScan}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setShowQRScan(false)}
          onResult={handleScanResult}
        />
        <CAlertModal
          open={isScanErrorAlert}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => {
            setScanErrorAlert(false);
          }}
        />
      </Fragment>
    );
  };

  return (
    <View style={styles.screen}>
      <StatusBar barStyle={"light-content"} translucent backgroundColor="transparent" />
      {_renderHeader()}
      {_renderSearch()}
      {_renderBody()}
      {_renderAddBtn()}
      {_renderActivity()}
      {_renderScanQRCode()}
    </View>
  );
};

export default OperationList;

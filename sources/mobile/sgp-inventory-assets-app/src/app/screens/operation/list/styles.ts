import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  dataFetching: {
    paddingVertical: WR(24),
  },
  dataEmptyTxt: {
    paddingHorizontal: WR(16),
    color: Color.White,
    fontSize: FontSize.FontMedium,
    textAlign: "center",
    paddingTop: WR(16),
    paddingBottom: WR(24),
  },
  searchBar: {
    flexDirection: "row",
    alignItems: "center",
  },
  searchBarInput: {
    marginTop: WR(8),
    marginBottom: WR(16),
    marginLeft: WR(24),
    flex: 1,
  },
  searchBarBtn: {
    width: WR(32),
    height: WR(32),
    marginTop: -WR(6),
    marginHorizontal: WR(16),
    alignItems: "center",
    justifyContent: "center",
  },
  overlayAddBtn: {
    width: WR(56),
    height: WR(56),
    position: "absolute",
    bottom: WR(24),
    right: WR(24),
    backgroundColor: "#FFC20E",
    borderRadius: WR(56 / 2),
    alignItems: "center",
    justifyContent: "center",
  },
});

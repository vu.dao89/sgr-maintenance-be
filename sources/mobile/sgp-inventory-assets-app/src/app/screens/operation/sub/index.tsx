import React, { FC, useEffect, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import { useSafeAreaInsets } from "react-native-safe-area-context";

import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import { useToast } from "react-native-toast-notifications";

import {
  CDatePicker,
  CEmptyState,
  CFileViewer,
  CModalViewer,
  CPersonInfo,
  CSelect,
  CSpinkit,
  CTAButtons,
  CTabViewDetail,
  CTags,
  CTextInput,
  Header,
  TextCM,
} from "@Components";

import { ICBackSVG, ICMap } from "@Assets/image";

import { widthResponsive as WR } from "@Constants";
import { OperationServices, WorkOrderServices } from "@Services";
import { get, isEmpty } from "lodash";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import { axiosHandler } from "../../../../services/httpClient";
import { dayjsFormatDate, dayjsTimePassedFromNow } from "../../../../utils/DateTime";

import { useDispatch } from "react-redux";
import { formatObjectLabel, formatWorkCenterLabel, genAttachFiles } from "src/utils/Format";
import TabViewRoutes from "./tabs";

const relatatedOptions: any = {
  workCenter: "personnel",
};

const SubOprationDetail: FC<any> = props => {
  const isMounted = useIsMounted();
  const saInsets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const modalPaddingBottom = (saInsets.bottom || WR(12)) + WR(44 + 24);

  const toast = useToast();

  const [headerInfoMinHeight, setHeaderInfoMinHeight] = useState(0);
  const [tabViewMinHeight, setTabViewMinHeight] = useState(0);

  const [isFetching, setFetching] = useState(true);
  const [tabIdx, setTabIdx] = useState(0);

  const [detail, setDetail]: any = useState(null);
  const [workOrderDetail, setWorkOrderDetail]: any = useState(null);
  const [historyActivity, setHistoryActivity]: any = useState(null);

  const [fetchings, setFetchings] = useStateLazy({});

  const [isOpenFinishModal, setIsOpenFinishModal] = useState(false);

  useEffect(() => {
    handleGetDetail();
  }, []);

  const getWorkOrderDetail = async (id: string) => {
    if (!id) return;

    let resp = await axiosHandler(() => WorkOrderServices.getWorkOrderDetails(id));
    let { workOrder }: any = get(resp, "response.data", {});

    if (workOrder) {
      const woStartDate = dayjsFormatDate(workOrder.woStartDate);
      const woFinishDate = dayjsFormatDate(workOrder.woFinishDate);

      const {
        workOrderId,
        workOrderDescription,
        equipmentId,
        equipmentDescription,
        mainPlant,
        maintenancePlantDescription,
        personnelName,
        commonStatus,
        priorityType,
        priorityText,
        workOrderSystemStatusDescription,
        workOrderTypeDescription,
      } = workOrder;

      const _workOrdeDetail = {
        id: workOrderId,
        title: workOrderDescription,
        label: formatObjectLabel(workOrderId, workOrderDescription),
        equipment: {
          name: formatObjectLabel(equipmentId, equipmentDescription, true),
          image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
        },
        mainPlant: formatObjectLabel(mainPlant, maintenancePlantDescription),
        createdBy: personnelName,
        createdDateTxt: `${woStartDate} - ${woFinishDate}`,
        timePassed: dayjsTimePassedFromNow(workOrder.woStartDate, "YYYYMMDD"),
        tags: [
          {
            label: priorityText || "",
            color: get(priorityType, "colorCode", "#FD4848"),
          },
          {
            label: workOrderSystemStatusDescription || "",
            color: get(commonStatus, "colorCode", "#FD4848"),
          },
          {
            label: workOrderTypeDescription || "",
            color: "#FD4848",
          },
        ],
      };

      setWorkOrderDetail(_workOrdeDetail);
    }
  };

  const handleGetDetail = async () => {
    const { operationId, workOrderId, superOperationId } = getScreenParams(props);

    const { response } = await axiosHandler(() => OperationServices.getOperationDetail(workOrderId, superOperationId));

    if (workOrderId) await getWorkOrderDetail(workOrderId);

    if (!isMounted()) return;

    const operation = get(response, "data.operation");
    const subOperations = get(response, "data.subOperation.item", []);
    const subOperation = subOperations.find(
      (s: any) => s.operationId === operationId && s.superOperationId === superOperationId
    );

    if (operation && subOperation) {
      const {
        equipmentId,
        equipmentDescription,
        workOrderDescription,
        personnelName,
        personnel,
        functionalLocationId,
        functionalLocationDescription,
        activityType,
        activityTypeData,
      } = operation;

      const {
        operationDescription,
        operationLongText,
        maintenancePlantCode,
        maintenancePlantDesc,
        operationSystemStatusDescription,
        commonStatus,
        workCenterCode,
        workCenterDesc,
        controlKey,
        controlKeyData,
        estimate,
        documents,
        operationSystemStatus,
      } = subOperation;

      const _detail = {
        id: operationId,
        title: operationDescription,
        superOperation: formatObjectLabel(operation?.operationId, operation?.operationDescription),
        equipment: {
          name: formatObjectLabel(equipmentId, equipmentDescription, true),
          image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
        },
        workOrder: {
          name: formatObjectLabel(workOrderId, workOrderDescription),
          image: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
          id: workOrderId,
        },
        desc: operationLongText,
        mainPlant: {
          name: formatObjectLabel(maintenancePlantCode, maintenancePlantDesc),
          code: maintenancePlantCode,
        },
        reportedBy: {
          fullName: personnelName,
          code: personnel,
          avatar: "https://gravatar.com/avatar/5767c215ab04b526f432520f3f33425c?s=400&d=robohash&r=x",
        },
        tags: [
          {
            label: operationSystemStatusDescription,
            color: get(commonStatus, "colorCode", "#3A73B7"),
          },
        ],
        equipmentId: equipmentId,
        equipmentDescription: equipmentDescription,
        functionalLocationId: functionalLocationId,
        functionalLocationDescription: functionalLocationDescription,
        functionalLocation: formatObjectLabel(functionalLocationId, functionalLocationDescription),
        workCenterCode,
        //-infos
        infos: [
          {
            label: "Nguồn lực",
            value: formatObjectLabel(controlKey, get(controlKeyData, "description", "")),
          },
          {
            label: "Nơi bảo trì",
            value: formatObjectLabel(maintenancePlantCode, maintenancePlantDesc),
          },
          {
            label: "Tổ đôi thực hiện",
            value: formatWorkCenterLabel(workCenterCode, maintenancePlantCode, workCenterDesc),
          },
          {
            label: "Thời gian làm việc dự kiến",
            value: `${estimate} phút`,
          },
        ],
        // attachment
        files: genAttachFiles(documents),
        operationSystemStatus: operationSystemStatus,
      };

      setDetail(_detail);
    }

    setFetching(false);
  };

  const handleOnCTA = () => {};

  const handleChange = async (id: any, data: any) => {
    if (id == "attachFiles") {
      const files = detail.files || [];
      setDetail({ ...detail, files: [...data, ...files] });
    }
  };

  const _renderHeader = () => {
    return (
      <Header
        title={"Chi tiết công việc con"}
        componentLeft={
          <TouchableOpacity style={styles.navBtnLeft} onPress={() => changeScreen(props)}>
            <ICBackSVG />
          </TouchableOpacity>
        }
        // componentRight={
        //   !isFetching && (
        //     <TouchableOpacity style={styles.navBtnRight}>
        //       <ICEdit2 />
        //     </TouchableOpacity>
        //   )
        // }
      />
    );
  };

  const _renderModalFinishOperation = () => {
    return (
      <CModalViewer
        open={isOpenFinishModal}
        placement={"bottom"}
        title={"Hoàn thành công việc:\n1. Kiểm tra hư hại khác"}
        onBackdropPress={() => setIsOpenFinishModal(false)}>
        <ScrollView contentContainerStyle={[styles.modalContent, { paddingBottom: modalPaddingBottom + WR(12) }]}>
          <CDatePicker isRequired label={"Thời gian bắt đầu"} />
          <CDatePicker isRequired label={"Thời gian kết thúc"} />
          <CSelect
            isRequired
            label={"Kết quả công việc"}
            menus={[
              {
                label: "Tiếp tục",
                value: "good",
              },
              {
                label: "Tạm dừng",
                value: "bad",
              },
            ]}
          />
          <CTextInput
            multiline={2}
            label={"Mô tả chi tiết"}
            plh={"Nhập mô tả chi tiết"}
            value={""}
            onChange={() => null}
          />
          <TouchableOpacity style={styles.btnSelectImg}>
            <TextCM style={styles.btnSelectImgTxt}>+ Thêm hình ảnh</TextCM>
          </TouchableOpacity>
          <CFileViewer
            simpleView
            files={[
              {
                uuid: "0001",
                type: "img",
                name: "Ho so...CNTT tham dinh1.jpg",
                size: "1,2Mb",
                date: "24/10/2022",
                source: "https://asiapark.sunworld.vn/wp-content/uploads/2018/08/MD42659.jpg",
              },
            ]}
          />
        </ScrollView>
        <CTAButtons
          isSticky
          buttons={[
            {
              text: "Hoàn tất",
              onPress: () => {
                setIsOpenFinishModal(false);
              },
            },
          ]}
        />
      </CModalViewer>
    );
  };

  const _renderInfo = () => {
    const { id, title, tags, superOperation, functionalLocation, reportedBy, desc = "" } = detail;
    return (
      <View style={styles.infoWrapper} onLayout={e => setHeaderInfoMinHeight(e.nativeEvent.layout.height)}>
        <View style={styles.content}>
          <TextCM style={styles.infoTitle}>{title}</TextCM>
          <TextCM style={styles.infoId}>{id}</TextCM>
          <View style={styles.tagsContainer}>
            <CTags data={tags} />
          </View>

          {!isEmpty(desc) && <TextCM style={styles.infoDesc}>{desc}</TextCM>}

          <TextCM style={styles.txtTabTitle}>Thuộc công việc</TextCM>
          <TextCM style={styles.infoId}>{superOperation}</TextCM>

          {!isEmpty(functionalLocation) && (
            <TouchableOpacity style={styles.infoLocation}>
              <ICMap />
              <TextCM style={styles.infoLocationTxt}>{functionalLocation}</TextCM>
            </TouchableOpacity>
          )}
        </View>

        <View style={styles.infoPersonInCharge}>
          <CPersonInfo {...reportedBy} label={"Đã giao cho"} style={{ flex: 1 }} />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {_renderModalFinishOperation()}

      {(isFetching && (
        <View style={styles.bodyCenter}>
          <CSpinkit />
        </View>
      )) ||
        (detail && (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              height: headerInfoMinHeight + tabViewMinHeight,
            }}>
            {_renderInfo()}
            <CTabViewDetail
              {...{
                saInsets,
                toast,
                fetchings,
                detail,
                workOrderDetail,
                historyActivity,
              }}
              index={tabIdx}
              routes={TabViewRoutes}
              onChange={handleChange}
              onLayout={setTabViewMinHeight}
              onNavigate={(...args: any) => changeScreen(props, ...args)}
            />
          </ScrollView>
        )) || <CEmptyState />}

      {detail && (
        <CTAButtons
          isSticky
          buttons={[
            {
              text: "Hoàn thành",
              onPress: handleOnCTA,
            },
          ]}
        />
      )}
    </View>
  );
};

export default SubOprationDetail;

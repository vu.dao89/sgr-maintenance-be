import React, { FC } from "react";
import { View } from "react-native";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CAttachFiles } from "@Components";
import { get } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { DocumentServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { useIsMounted } from "../../../../../../utils/Core";
import { CFile } from "../../../../../components/common/attach-files";

const SOTabAttachment: FC<any> = ({ saInsets, detail, onChange }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();

  const attachFiles = get(detail, "files", []);

  const handleAttachFiles = async (files: Array<CFile>) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      DocumentServices.uploadNotificationDocuments(detail.id, files)
    );

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      onChange("attachFiles", files);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <View
      style={[
        styles.container,
        {
          paddingTop: WR(24),
          paddingBottom: get(detail, "isFinish", false) ? saInsets.bottom : saInsets.bottom + WR(72),
        },
      ]}>
      <CAttachFiles files={attachFiles} onChange={(files: Array<CFile>) => handleAttachFiles(files)} />
    </View>
  );
};

export default SOTabAttachment;

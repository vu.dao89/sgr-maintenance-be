import React, { FC } from "react";

import { View } from "react-native";

import styles from "./styles";

import { CCollapsibleSection, CTags, TextCM } from "@Components";
import { OperationHistoryActivity, widthResponsive as WR } from "@Constants";
import { get, isEmpty } from "lodash";

const SubOperatorGeneral: FC<any> = ({ workOrderDetail, saInsets, historyActivity, detail }) => {
  const { equipment, mainPlant, woCreatedBy, createdDateTxt, timePassed, label, createdBy } = workOrderDetail;

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom:
            historyActivity === OperationHistoryActivity.COMPLETE ? saInsets.bottom : saInsets.bottom + WR(88),
        },
      ]}>
      <CCollapsibleSection title={"Thuộc lệnh bảo trì"}>
        <View style={styles.listItemHeader}>
          <View style={styles.listItemContent}>
            <TextCM style={styles.listItemTitle}>{label}</TextCM>
            {[
              ["Thiết bị", get(equipment, "name")],
              ["Khu vực", mainPlant],
              ["Giám sát", createdBy],
              ["Tạo bởi", woCreatedBy],
              ["Thời gian", createdDateTxt],
            ]
              .filter(([_, value]) => !isEmpty(value))
              .map(([label, value]) => (
                <View key={label} style={[styles.listItemInfoRow, { marginTop: 0 }]}>
                  <TextCM style={styles.listItemInfoLabel}>{label}: </TextCM>
                  <TextCM style={styles.listItemInfoValue}>{value}</TextCM>
                </View>
              ))}
          </View>
        </View>

        <View style={styles.listItemFooter}>
          <CTags data={workOrderDetail?.tags} />
          <TextCM style={styles.listItemTimePassed}>{timePassed}</TextCM>
        </View>
      </CCollapsibleSection>
      <CCollapsibleSection title={"Thông tin chính"} infos={detail.infos || []} />
    </View>
  );
};

export default SubOperatorGeneral;

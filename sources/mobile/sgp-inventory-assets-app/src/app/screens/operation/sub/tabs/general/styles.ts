import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, ScreenWidth, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingBottom: WR(24),
    paddingLeft: WR(24),
    paddingRight: WR(24),
  },
  ctnMap: {
    width: "100%",
    borderRadius: HR(16),
    overflow: "hidden",
    marginBottom: HR(16),
    marginTop: HR(10),
  },
  map: {
    height: HR(150),
    width: "100%",
    overflow: "hidden",
  },
  txtTabTitle: {
    fontWeight: "200",
    color: Color.GrayLight,
  },
  listSeparator: {
    width: ScreenWidth - WR(24) * 2,
    height: 1,
    marginHorizontal: WR(24),
    backgroundColor: "#414042",
  },
  listItemWrapper: {
    paddingVertical: WR(16),
    paddingHorizontal: WR(24),
  },
  listItemHeader: {
    flexDirection: "row",
  },
  listItemContent: {
    flex: 1,
    marginRight: WR(8),
  },
  listItemImage: {
    width: WR(56),
    height: WR(56),
    borderRadius: WR(16),
  },
  listItemTitle: {
    marginBottom: WR(8),
    fontWeight: "700",
    color: Color.White,
  },
  listItemInfoRow: {
    minHeight: WR(20),
    flexDirection: "row",
  },
  listItemInfoLabel: {
    marginRight: WR(4),
    color: "#8D9298",
    fontSize: FontSize.FontTiny,
  },
  listItemInfoValue: {
    flex: 1,
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  listItemFooter: {
    marginTop: WR(2),
    paddingBottom: WR(16),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  listItemTimePassed: {
    marginTop: WR(4),
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
});

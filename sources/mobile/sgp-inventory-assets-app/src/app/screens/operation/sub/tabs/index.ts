import { default as SOTabAttachment } from "./attachment";
import { default as SubOperatorGeneral } from "./general";

export default [
  {
    key: "0",
    padding: 24,
    title: "Tổng quan",
    Scene: SubOperatorGeneral,
  },
  {
    key: "1",
    padding: 30,
    title: "Tài liệu",
    Scene: SOTabAttachment,
  },
];

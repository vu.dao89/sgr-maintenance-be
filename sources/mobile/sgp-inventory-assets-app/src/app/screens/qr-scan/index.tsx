// External
import { useIsFocused, useNavigation } from "@react-navigation/native";
import { t } from "i18next";
import { get, isEmpty } from "lodash";
import React, { useState } from "react";
import { Platform, TouchableOpacity, View } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import { launchImageLibrary } from "react-native-image-picker-supper";
import { QRreader, QRscanner } from "react-native-qr-decode-image-camera";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";

// Internal

import { CommonActions } from "@Actions";
import { ICQRInvalid, ICQRNotFound, ICSelectImg, IMQrInvalid } from "@Assets/image";
import { Header, ImageCache, PopupAnalog, TextCM } from "@Components";
import { Color, heightResponsive, ScreenHeight, ScreenName, Types, widthResponsive } from "@Constants";
import { HomeService } from "@Services";
import { axiosHandler } from "@Services/httpClient";
import styles from "./styles";
import { AppState } from "@Reducers";

export default function BarcodeScanner() {
  const nav = useNavigation();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const insets = useSafeAreaInsets();
  const [isRepeatScan, setRepeatScan] = useState(true);
  const [visibleInvalid, setVisibleInvalid] = useState(false);
  const [visibleNotFound, setVisibleNotFound] = useState(false);
  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const role = get(reducer, "userInfo.data.role", []);

  const selectImageCrop = async () => {
    try {
      const img = await ImagePicker.openPicker({
        cropping: false,
      });
      if (img) {
        dispatch(CommonActions.openLoading.request({}));
        const qrdata = await QRreader(img.sourceURL);
        if (qrdata) {
          const response = await axiosHandler(() =>
            HomeService.verifyQR({
              qrcode: qrdata,
            })
          );
          if (!response?.data) {
            dispatch(
              CommonActions.openLoading.failure({
                callback: () => {
                  setVisibleNotFound(true);
                },
              })
            );
          } else {
            dispatch(
              CommonActions.openLoading.success({
                callback: () => {
                  nav.navigate(
                    ScreenName.DetailAssets as never,
                    {
                      qrcode: qrdata,
                    } as never
                  );
                },
              })
            );
          }
        }
      }
    } catch (error) {
      dispatch(
        CommonActions.openLoading.failure({
          callback: () => {
            setVisibleInvalid(true);
          },
        })
      );
    }
  };

  React.useEffect(() => {
    if (isFocused) {
      setRepeatScan(true);
    }
  }, [isFocused]);

  const onBarcodeScanned = async (results: any) => {
    await setRepeatScan(false);
    if (results?.data) {
      dispatch(CommonActions.openLoading.request({}));
      const response = await axiosHandler(() =>
        HomeService.verifyQR({
          qrcode: results?.data,
        })
      );

      if (!response?.data) {
        dispatch(
          CommonActions.openLoading.failure({
            callback: () => {
              setVisibleNotFound(true);
            },
          })
        );
      } else {
        dispatch(
          CommonActions.openLoading.success({
            callback: () => {
              nav.navigate(
                ScreenName.DetailAssets as never,
                {
                  qrcode: results?.data,
                } as never
              );
            },
          })
        );
      }
    } else {
      setVisibleInvalid(true);
    }
  };

  const _SelectFromLibrary = () => {
    if (Platform.OS === "ios") {
      selectImageCrop();
      return;
    }
    launchImageLibrary({ mediaType: "photo", includeBase64: true }, (response: any) => {
      if (response.didCancel) {
        // TODO
      } else if (response.error) {
        // TODO
        return;
      } else if (response.customButton) {
        // TODO
        return;
      } else {
        // here we can call a API to upload image on server
        if (!isEmpty(response)) {
          dispatch(CommonActions.openLoading.request({}));
          const data = response.assets[0];
          QRreader(data.uri)
            .then(async (dataQR: any) => {
              const responseQR = await axiosHandler(() =>
                HomeService.verifyQR({
                  qrcode: dataQR,
                })
              );
              if (!responseQR?.data) {
                dispatch(CommonActions.openLoading.failure({}));
                setVisibleNotFound(true);
                dispatch(CommonActions.openLoading.failure({}));
              } else {
                dispatch(CommonActions.openLoading.success({}));
                nav.navigate(
                  ScreenName.DetailAssets as never,
                  {
                    qrcode: dataQR,
                  } as never
                );
              }
            })
            .catch((err: any) => {
              dispatch(CommonActions.openLoading.failure({}));
              setTimeout(() => {
                setVisibleInvalid(true);
              }, 800);
              console.log(err);
            });
        }
      }
    });
  };
  const handlePopupInvalid = () => {
    handleDismiss();
    setVisibleInvalid(pre => !pre);
  };
  const handlePopupNotFound = () => {
    handleDismiss();
    setVisibleNotFound(false);
  };
  const handleDismiss = () => {
    setRepeatScan(true);
  };
  const createNewAsset = () => {
    setVisibleNotFound(pre => !pre);
    setTimeout(() => {
      nav.navigate(ScreenName.CreateAsset as never);
    }, 300);
  };
  return (
    <View style={styles.container}>
      <QRscanner
        zoom={0}
        cornerColor={Color.Yellow}
        isShowScanBar={false}
        rectHeight={widthResponsive(300)}
        rectWidth={widthResponsive(300)}
        onRead={onBarcodeScanned}
        hintTextPosition={
          Platform.OS === "ios" ? heightResponsive(ScreenHeight / 3 - 110) : heightResponsive(ScreenHeight / 3 - 100)
        }
        isRepeatScan={isRepeatScan}
        bottomHeight={100}
        hintText={`${t("IS.txtQRScanDescription")}`}
        hintTextStyle={styles.txtQrDescription}
        topViewStyle={styles.Header}
        renderTopView={() => <Header style={{ zIndex: 1000 }} title={t("IS.Title")} />}
        renderBottomView={() => (
          <View style={styles.overlay}>
            <TouchableOpacity
              onPress={_SelectFromLibrary}
              style={[styles.ctnSelectImg, { marginBottom: insets.bottom + heightResponsive(16) }]}>
              <ICSelectImg />
              <TextCM style={styles.txtSelectImg}>{`${t("IS.txtSelectImg")}`}</TextCM>
            </TouchableOpacity>
          </View>
        )}
      />

      <PopupAnalog
        titlePopup={t("IS.txtInvalidQr")}
        content={t("IS.txtSelectAnother")}
        titleButton={t("IS.txtTryAgain")}
        visible={visibleInvalid}
        onDismiss={handleDismiss}
        onClose={handlePopupInvalid}
        onPressOutside={handlePopupInvalid}
        onPressConfirm={handlePopupInvalid}
        type="custom"
        iconShow={<ImageCache imageDefault={IMQrInvalid} style={styles.imgInvalid} />}
      />
      <PopupAnalog
        titlePopup={t("IS.txtQRNotFound")}
        content={role.includes(Types.PERMISSION_SAP.A70) ? t("IS.txtQRCreateNew") : undefined}
        titleButton={role.includes(Types.PERMISSION_SAP.A70) ? t("IS.CreateNewAsset") : undefined}
        visible={visibleNotFound}
        onDismiss={handleDismiss}
        onClose={handlePopupNotFound}
        onPressOutside={handlePopupNotFound}
        onPressConfirm={createNewAsset}
        type="custom"
        iconShow={<ICQRNotFound />}
      />
    </View>
  );
}

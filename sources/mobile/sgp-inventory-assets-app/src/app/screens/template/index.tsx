// External
import React, { FC } from "react";

// Internal
import { ICBackSVG } from "@Assets/image";
import { TextCM } from "@Components";
import { View } from "react-native";

type Props = {
  navigation: any;
};
const Template: FC<Props> = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ICBackSVG />
      <TextCM>Mini app</TextCM>
    </View>
  );
};

export default Template;

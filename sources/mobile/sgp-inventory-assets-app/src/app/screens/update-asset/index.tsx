import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigation } from "@react-navigation/native";
import { debounce, get as _get } from "lodash";
import React, { FC, useCallback, useState } from "react";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import { ActivityIndicator, TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";

import { DetailAssetsAction } from "@Actions";
import { Header, PopupAnalog, TextCM } from "@Components";
import { Color, String as Strings } from "@Constants";
import { AppState } from "@Reducers";
import BasicInfo from "@Screens/create-asset-screen/form/basic-info";
import CreateAssetServices from "@Services/create-asset.services";
import { FormUtils } from "@Utils";
import styles from "./styles";
import { defaultValues, FieldType, FormField, I18n, UpdateAssetBody } from "./type";

const schema = yup
  .object({
    [FieldType.AssetName]: yup.string().trim().required(I18n.msgRequired),
    // [FieldType.Serial]: yup.string().trim().required(I18n.msgRequired), - CR-302
    [FieldType.Unit]: yup
      .object({
        id: yup.string().trim().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
    [FieldType.DepartmentUse]: yup
      .object({
        id: yup.string().trim().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
    [FieldType.PersonUse]: yup
      .object({
        id: yup.string().trim().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
    [FieldType.InventoryClassify]: yup
      .object({
        id: yup.string().trim().required(I18n.msgRequired),
      })
      .required(I18n.msgRequired),
  })
  .required();

type Props = {};

const UpdateAsset: FC<Props> = ({}) => {
  const dispatch = useDispatch();
  const dataDetailAssets = useSelector((state: AppState) => state.detailAssets);
  const reducerCommon: any = useSelector<AppState | null>(s => s?.common);
  const { email } = _get(reducerCommon, "userInfo.data", {});
  const nav = useNavigation();
  const [loading, setLoading] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);
  const [msgError, setMsgError] = useState<string>(I18n.messageInventoryFailure);
  const [showNotice, setShowNotice] = useState<boolean>(false);
  const toggleNotice = () => setShowNotice(!showNotice);
  const toggleError = (message = I18n.messageInventoryFailure) => {
    setShowError(!showError);
    setMsgError(message);
  };

  const data = _get(dataDetailAssets, "data", {} as any);

  const methods = useForm<UpdateAssetBody>({
    mode: "onChange",
    reValidateMode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: defaultValues(data),
  });

  const {
    handleSubmit,
    formState: { isDirty, isValid },
    setError,
  } = methods;

  const _onUpdate = async (data: any) => {
    const bodyReq: UpdateAssetBody = {
      ...data,
      [FieldType.Unit]: FormUtils.getValueSelected(data, FieldType.Unit),
      [FieldType.AssetGroup]: FormUtils.getValueSelected(data, FieldType.AssetGroup),
      [FieldType.DepartmentUse]: FormUtils.getValueSelected(data, FieldType.DepartmentUse),
      [FieldType.InventoryClassify]: FormUtils.getValueSelected(data, FieldType.InventoryClassify),
      [FieldType.CommissionedDate]: FormUtils.getDateFormat(data, FieldType.CommissionedDate),
      [FieldType.PersonUse]: FormUtils.getValueSelected(data, FieldType.PersonUse),
      [FieldType.QrCode]: (data && data.qrcode) || "",
      [FieldType.UserEmail]: email || "",
      [FieldType.Type]: "U",
    };
    const response = await CreateAssetServices.createNewAsset(bodyReq);
    const { code, error: _error } = response && response.data;
    setLoading(false);
    if (code === Strings.responseCode.ApiError100) {
      if (_error?.includes("Serial")) {
        setError(FieldType.Serial, { message: _error });
      }
      toggleError(_error);
    } else if (code === Strings.responseCode.ApiSuccess) {
      toggleNotice();
    }
  };

  const handlerCreate = useCallback(debounce(_onUpdate, 300), []);

  const onSubmit: SubmitHandler<FormField> = data => {
    setLoading(true);
    handlerCreate(data);
  };

  const onPositive = (): void => {
    toggleNotice();
    dispatch(DetailAssetsAction.getDetailAssetAction.request(data?.qrcode || ""));
    setTimeout(() => {
      nav.goBack();
    }, 300);
  };

  const FixedAction = () => {
    return (
      <View style={styles.ctnFooterAction}>
        <TouchableOpacity
          disabled={!(isDirty && isValid)}
          style={isDirty && isValid ? styles.btnUpdate : styles.btnUpdateDisable}
          onPress={handleSubmit(onSubmit)}>
          {loading && <ActivityIndicator style={styles.mr4} size="small" color={Color.Black} />}
          <TextCM style={styles.txtUpdateAsset}>{`${I18n.btnSave}`}</TextCM>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <FormProvider {...methods}>
      <View style={styles.container}>
        <Header title={I18n.txtHeader} />

        <KeyboardAwareScrollView>
          <BasicInfo />
        </KeyboardAwareScrollView>

        <PopupAnalog
          visible={showNotice}
          onClose={toggleNotice}
          content={I18n.txtUpdateSuccess}
          titlePopup={I18n.titleUpdateModal}
          titleButton={I18n.btnClose}
          onPressConfirm={onPositive}
        />
        <PopupAnalog
          visible={showError}
          onClose={toggleError}
          type="error"
          content={msgError}
          titlePopup={I18n.titleInventoryFailure}
          titleButton={I18n.ButtonClose}
          onPressConfirm={toggleError}
        />

        <FixedAction />
      </View>
    </FormProvider>
  );
};

export default UpdateAsset;

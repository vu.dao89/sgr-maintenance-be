import { StyleSheet } from "react-native";
import { heightResponsive as H, widthResponsive as W, ScreenWidth, Color, FontSize } from "@Constants";
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  btnUpdate: {
    width: ScreenWidth - W(32),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: H(12),
    backgroundColor: Color.Yellow,
    borderRadius: H(10),
  },
  btnUpdateDisable: {
    width: ScreenWidth - W(32),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: H(12),
    backgroundColor: Color.ButtonDisable,
    borderRadius: H(10),
  },
  txtUpdateAsset: {
    color: Color.TextButton,
    fontSize: FontSize.FontMedium,
    fontWeight: "700", // DESIGN_BASE 500
  },
  ctnFooterAction: {
    width: ScreenWidth,
    minHeight: H(68),
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: H(8),
    paddingHorizontal: W(12),
    borderTopLeftRadius: H(12),
    borderTopRightRadius: H(12),
    justifyContent: "center",
    marginBottom: H(16),
  },
  title: {
    color: Color.White,
    fontSize: FontSize.FontMedium,
    marginBottom: H(16),
    fontWeight: "700", // DESIGN_BASE 500
  },
  formWrapper: {
    marginTop: H(20),
    paddingHorizontal: W(16),
  },
  field: {
    marginBottom: H(16),
  },
  txtHistory: {
    color: Color.Yellow,
    fontSize: FontSize.FontMedium,
    fontWeight: "400",
  },
  mr4: {
    marginRight: W(4),
  },
});

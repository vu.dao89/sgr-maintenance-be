import i18n from "@I18n";
import { AppState } from "@Reducers";
import { inventoryTypes } from "@Screens/create-asset-screen/modal/inventory-classify-filter";
import { DateTimeUtil } from "@Utils";
import moment from "moment";
import { useSelector } from "react-redux";

export enum FieldType {
  QrCode = "qrcode",
  AssetName = "name",
  Serial = "serial",
  Unit = "unit",
  AssetGroup = "asset_group",
  CommissionedDate = "use_date",
  DepartmentUse = "costcenter",
  PersonUse = "person",
  InventoryClassify = "inventory_type",
  Room = "room",
  Model = "model",
  Manufacturer = "manufacture",
  Amount = "quantity",
  Status = "status", //?
  StatusDescription = "statusDescription", //?
  Location = "location",
  Image = "image",

  // Adds
  Vendor = "vendor",

  // Extends
  UserEmail = "user_email",
  Type = "type",
}

export type FormField = {
  [FieldType.AssetName]: string;
  [FieldType.Serial]?: string;
  [FieldType.Unit]: any;
  [FieldType.AssetGroup]: any;
  [FieldType.DepartmentUse]: any;
  [FieldType.PersonUse]: any;
  [FieldType.CommissionedDate]: Date | any;
  [FieldType.InventoryClassify]: any;
  [FieldType.Room]: string;
  [FieldType.Model]: string;
  [FieldType.Manufacturer]: string;
  [FieldType.Amount]: number;
  [FieldType.Vendor]: string;
};

export type UpdateAssetBody = FormField & {
  [FieldType.UserEmail]: string;
  [FieldType.Type]: "I" | "U";
  [FieldType.QrCode]: string;
};

export const defaultValues = (data: any) => {
  const reducer: any = useSelector<AppState | null>(s => s?.home);
  const { units } = reducer.filterData;
  const currentUnit = units?.find((ele: any) => ele.id === data?.unit);

  return {
    [FieldType.AssetName]: data?.description || "",
    [FieldType.Serial]: data?.serial || "",
    [FieldType.Unit]: {
      id: data?.unit || "",
      label: currentUnit?.label || "",
    },
    [FieldType.AssetGroup]: {
      id: data?.asset_group || "",
      label: data?.asset_groupname || "",
    },
    [FieldType.CommissionedDate]: data?.use_date
      ? moment(DateTimeUtil.convertStringToDate(data?.use_date)).toDate()
      : "",
    [FieldType.DepartmentUse]: {
      id: data?.costcenter || "",
      label: data?.costcenter_name || "",
    },
    [FieldType.PersonUse]: {
      id: data?.person || "",
      label: data?.person_name || "",
    },
    [FieldType.InventoryClassify]: {
      id: data?.inventory_type || "",
      label: inventoryTypes.find((ele: any) => ele.id === data?.inventory_type)?.label || "",
    },
    [FieldType.Room]: data?.room || "",
    [FieldType.Model]: data?.model || "",
    [FieldType.Manufacturer]: data?.manufacture || "",
    [FieldType.Vendor]: data?.vendor || "",
    [FieldType.QrCode]: data?.qrcode || "",
  };
};

export const I18n = {
  txtHeader: i18n.t("UA.txtHeader"),
  txtHistory: i18n.t("UA.txtHistory"),
  lblNameAsset: i18n.t("UA.lblNameAsset"),
  plhNameAsset: i18n.t("UA.plhNameAsset"),
  lblSerial: i18n.t("UA.lblSerial"),
  plhSerial: i18n.t("UA.plhSerial"),
  lblUnit: i18n.t("UA.lblUnit"),
  plhUnit: i18n.t("UA.plhUnit"),
  lblGroupAsset: i18n.t("UA.lblGroupAsset"),
  plhGroupAsset: i18n.t("UA.plhGroupAsset"),
  lblDateToUsed: i18n.t("UA.lblDateToUsed"),
  plhDateToUsed: i18n.t("UA.plhDateToUsed"),
  lblPartUsed: i18n.t("UA.lblPartUsed"),
  lblUser: i18n.t("UA.lblUser"),
  lblInventoryClassfication: i18n.t("UA.lblInventoryClassfication"),
  lblModel: i18n.t("UA.lblModel"),
  plhModel: i18n.t("UA.plhModel"),
  lblProducer: i18n.t("UA.lblProducer"),
  plhProducer: i18n.t("UA.plhProducer"),
  msgRequired: i18n.t("UA.msgRequired"),
  btnSave: i18n.t("UA.btnSave"),
  plhInventoryClassify: i18n.t("UA.plhInventoryClassify"),
  titleUpdateModal: i18n.t("UA.titleUpdateModal"),
  txtUpdateSuccess: i18n.t("UA.txtUpdateSuccess"),
  btnClose: i18n.t("UA.btnClose"),

  ButtonClose: i18n.t("CM.Button.Close"),
  titleInventoryFailure: i18n.t("DA.detail.titleInventoryFailure"),
  messageInventoryFailure: i18n.t("DA.detail.messageInventoryFailure"),
};

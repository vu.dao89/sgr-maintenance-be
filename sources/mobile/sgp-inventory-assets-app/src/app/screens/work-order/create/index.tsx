import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";

import {
  DocumentServices,
  EquipmentServices,
  MasterDataServices,
  NotificationServices,
  PermissionServices,
  WorkOrderCreateParams,
  WorkOrderOperationCreateParams,
  WorkOrderServices,
} from "@Services";

import styles from "./styles";

import { AppState } from "src/redux/reducers";

import { genRandomString, genShortDescription, isArray } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatPriorityLabel,
  formatWorkCenterLabel,
  mappingSelections,
} from "src/utils/Format";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import { get } from "lodash";
import { View } from "react-native";
import { ICBackSVG, ICCloseY, ICGhost, ICWarningOctagon } from "../../../../assets/image";
import { Permission, SystemStatus } from "../../../../constants";
import i18n from "../../../../i18n";
import { CommonActions } from "../../../../redux/actions";
import { axiosHandler, getErrorDetail } from "../../../../services/httpClient";
import { dayjsFormatDate, dayjsGetDate, SAP_DATE_FORMAT, SAP_TIME_FORMAT } from "../../../../utils/DateTime";
import { CAlertModal, CEmptyState, CNavbar, CScanQRCodeModal, CTAButtons, CTabViewCreate } from "../../../components";
import TabViewRoutes from "./tabs";

const ctaButtonTxts = ["next", "next", "create"];

const relatatedOptions: any = {
  maintPlant: "workCenter",
  workCenter: "personnel",
  workOrderType: "activityKey",
};

const WorkOrderCreate: FC<any> = props => {
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const toast = useToast();
  const reqIds: any = useRef({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const personPermission = PermissionServices.getPersonPermission(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.POST);

  const [errors, setErrors] = useStateLazy({});
  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});

  const [tabIdx, setTabIdx] = useState(0);

  const [state, setState]: any = useStateLazy({
    tasks: [],
    notification: getScreenParams(props).notification || null,
  });

  const [isScanError, setScanError] = useState(false);
  const [isScanData, setScanDataStatus] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  const handleChangeTab = (targetIdx: any) => setTabIdx(tabIdx + targetIdx);

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "cleanSelections":
        {
          setOptions((prevState: any) => ({ ...prevState, [data]: [] }));
        }
        break;
      case "":
      default:
        {
          let nextState: any = { [id]: data },
            nextOptsState: any = {};
          let relatatedOptionDataKey = relatatedOptions[id];
          if (relatatedOptionDataKey && !!data) {
            nextState[relatatedOptionDataKey] = null;
            nextOptsState[relatatedOptionDataKey] = [];
          }
          switch (id) {
            case "workCenter":
              {
                if (data && !state.maintPlant) {
                  nextState.maintPlant = data.maintPlant;
                }
              }
              break;

            case "equipment":
              {
                if (data && !state.functionalLocation) {
                  nextState.functionalLocation = data.functionalLocation;
                }
              }
              break;

            default:
              break;
          }
          setState((prevState: any) => ({ ...prevState, ...nextState }));
          setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
          setErrors((prevState: any) => ({ ...prevState, [id]: false }));
        }
        break;
    }
  };

  const handleSearch = (id: any) => async (keyword?: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];

    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(state.maintPlant ? [state.maintPlant] : [], userPlants),
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "notification":
        {
          params.employeeId = personPermission;
          params.statuses = [SystemStatus.notification.CREATE_NEW];
          searchServiceApi = NotificationServices.getNotifications;
          mappingOptions = { valueKey: "id" };
        }
        break;
      case "workOrderType":
        {
          searchServiceApi = MasterDataServices.getWorkOrderTypes;
          mappingOptions = {
            valueKey: "type",
            labelKey: "name",
            format: (i: any) => ({
              priorityTypeId: i.priorityType,
              maintenancePlantId: i.maintenancePlantId,
            }),
          };
        }
        break;
      case "activityKey":
        {
          if (state.workOrderType) params.workOrderTypes = [state.workOrderType.value];
          searchServiceApi = MasterDataServices.getActivityKeys;
          mappingOptions = { valueKey: "type" };
        }
        break;
      case "maintPlant":
        {
          params.maintenancePlantCodes = userPlants;
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenter: i.costCenter,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "functionalLocation":
        {
          if (state.plannerGroup) params.plannerGroups = [state.plannerGroup.value];
          searchServiceApi = MasterDataServices.getFunctionalLocations;
          mappingOptions = {
            valueKey: "functionalLocationId",
            format: (i: any) => ({ label: formatObjectLabel(i.functionalLocationId, i.description, true) }),
          };
        }
        break;
      case "equipment":
        {
          if (state.functionalLocation) params.functionalLocationCodes = [state.functionalLocation.value];
          if (state.plannerGroup) params.plannerGroups = [state.plannerGroup.value];
          searchServiceApi = EquipmentServices.getEquipments;
          mappingOptions = {
            valueKey: "equipmentId",
            format: (i: any) => ({
              label: formatObjectLabel(i.equipmentId, i.description, true),
              functionalLocation: {
                value: i.functionalLocation?.functionalLocationId,
                label: formatObjectLabel(i.functionalLocation?.functionalLocationId, i.functionalLocation?.description),
              },
            }),
          };
        }
        break;
      case "plannerGroup":
        {
          searchServiceApi = MasterDataServices.getPlannerGroups;
          mappingOptions = { valueKey: "plannerGroupId", labelKey: "name" };
        }
        break;
      case "systemCondition":
        {
          searchServiceApi = MasterDataServices.getSystemConditions;
          mappingOptions = { labelKey: "text" };
        }
        break;
      case "priority":
        {
          let priorityType = get(state, "workOrderType.priorityTypeId", "");
          params.types = [priorityType];

          searchServiceApi = MasterDataServices.getPriorities;
          mappingOptions = {
            valueKey: "priority",
            format: (i: any) => ({ label: formatPriorityLabel(i.type, i.priority, i.description) }),
          };
        }
        break;
      case "personnel":
        {
          if (state.workCenter) params.workCenterIds = [state.workCenter.value];
          searchServiceApi = MasterDataServices.getPersonnel;
          mappingOptions = { labelKey: "name", format: (i: any) => ({ fullName: i.name }) };
        }
        break;
      default:
        break;
    }

    const { response, error } = await axiosHandler(() => searchServiceApi(params));
    // console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  const handleSearchScanResult = async (qrCode: any) => {
    setScanDataStatus(false);
    await setFetchings((prevState: any) => ({ ...prevState, scan: true }));

    const { response, error } = await axiosHandler(() => MasterDataServices.getByQRCode(qrCode), { delay: true });
    // console.log(`🚀 Kds: handleSearchScanResult -> qrCode, response, error`, qrCode, response, error);

    if (!isMounted()) return;

    await setFetchings((prevState: any) => ({ ...prevState, scan: false }));

    const { equipment, functionalLocation } = get(response, "data", {});

    if (!equipment && !functionalLocation) return setScanError(true);

    switch (true) {
      case !!functionalLocation:
        {
          let functionalLocationSelection = mappingSelections([functionalLocation], {
            valueKey: "functionalLocationId",
          })[0];
          handleChange("functionalLocation", functionalLocationSelection);
        }
        break;
      case !!equipment:
        {
          const { functionalLocation: _functionalLocation, functionalLocationId } = equipment;
          let functionalLocationSelection = _functionalLocation
            ? mappingSelections([_functionalLocation], { valueKey: "functionalLocationId" })[0]
            : { value: functionalLocationId, label: functionalLocationId };
          let equipmentSelection = mappingSelections([equipment], { valueKey: "equipmentId" })[0];
          handleChange("functionalLocation", functionalLocationSelection);
          handleChange("equipment", equipmentSelection);
        }
        break;
      default:
        break;
    }
  };

  const handleGetNotificationType = async (type: string, maintenancePlantCode: string) => {
    const { response } = await axiosHandler(() => MasterDataServices.getNotificationType(type, maintenancePlantCode));
    return get(response, "data");
  };

  const fillNotificationRelated = async (notificationId: string) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => NotificationServices.getDetailNotification(notificationId));

    if (!isMounted()) return dispatch(CommonActions.openLoading.success({}));

    const notiData = get(response, "data.header", {});
    if (notiData) {
      const { priorityTypeObject, requestStart, requestStartTime, requestEnd, requestEndTime, type, maintenancePlant } =
        notiData;

      const notificationType = await handleGetNotificationType(type, maintenancePlant);

      dispatch(CommonActions.openLoading.success({}));
      if (!isMounted()) return;

      if (notificationType?.workOrderType) {
        const { type, name, priorityType } = notificationType?.workOrderType;

        handleChange("workOrderType", {
          value: type,
          label: formatObjectLabel(type, name),
          priorityTypeId: priorityType,
          maintenancePlantId: maintenancePlant,
        });

        if (priorityTypeObject) {
          const { priority, description } = priorityTypeObject;

          handleChange("priority", {
            value: priority,
            label: formatPriorityLabel(priorityType, priority, description),
          });
        }
      }

      if (requestStart) {
        handleChange("startDate", dayjsGetDate(`${requestStart}${requestStartTime}`));
      }

      if (requestEnd) {
        handleChange("endDate", dayjsGetDate(`${requestEnd}${requestEndTime}`));
      }

      handleChange("maintPlant", {
        value: notiData.maintenancePlant,
        label: formatObjectLabel(notiData.maintenancePlant, notiData.maintenancePlantDesc),
      });

      // auto fill work center
      handleChange("workCenter", {
        value: notiData.workCenter,
        label: formatWorkCenterLabel(notiData.workCenter, notiData.maintenancePlant, notiData.workCenterDesc),
        maintPlant: {
          value: notiData.maintenancePlant,
          label: formatObjectLabel(notiData.maintenancePlant, notiData.maintenancePlantDesc),
        },
      });

      // auto fill functional location
      handleChange("functionalLocation", {
        value: notiData.functionalLocationId,
        label: formatObjectLabel(notiData.functionalLocationId, notiData.functionalLocationDesc),
      });

      // auto fill equipment
      handleChange("equipment", {
        value: notiData.equipmentId,
        label: formatObjectLabel(notiData.equipmentId, notiData.equipmentDesc, true),
        functionalLocation: {
          value: notiData.functionalLocationId,
          label: formatObjectLabel(notiData.functionalLocationId, notiData.functionalLocationDesc),
        },
      });

      handleChange("plannerGroup", {
        value: notiData.plannerGroup,
        label: formatObjectLabel(notiData.plannerGroup, notiData.plannerGroupText),
      });
    }
  };

  useEffect(() => {
    if (havePermission) {
      const equipment = get(getScreenParams(props), "equipment");
      if (equipment) handleSelectedEquipment(equipment);
    }
  }, []);

  useEffect(() => {
    if (havePermission && state.notification && state.notification.value) {
      fillNotificationRelated(state.notification.value);
    }
  }, [state.notification]);

  const handleSelectedEquipment = (equipment: any) => {
    const {
      equipmentId,
      description: equipmentDesc,
      maintenanceWorkCenter,
      functionalLocation,
      maintenancePlant,
      plannerGroup,
      plannerGroupName,
    } = equipment;

    if (equipmentId && equipmentDesc) {
      handleChange("equipment", { value: equipmentId, label: formatObjectLabel(equipmentId, equipmentDesc, true) });
    }

    if (maintenanceWorkCenter) {
      const { code: workCenterCode, description } = maintenanceWorkCenter;
      const { code: plantCode, plantName } = maintenancePlant;

      handleChange("workCenter", {
        value: workCenterCode,
        label: formatWorkCenterLabel(workCenterCode, plantCode, description),
        maintPlant: {
          value: plantCode,
          label: formatObjectLabel(plantCode, plantName),
        },
      });
    } else {
      if (maintenancePlant) {
        const { code, plantName } = maintenancePlant;

        handleChange("maintPlant", {
          value: code,
          label: formatObjectLabel(code, plantName),
        });
      }
    }

    if (functionalLocation) {
      const { functionalLocationId, description } = functionalLocation;

      handleChange("functionalLocation", {
        value: functionalLocationId,
        label: formatObjectLabel(functionalLocationId, description),
      });
    }

    if (plannerGroup && plannerGroupName) {
      handleChange("plannerGroup", { value: plannerGroup, label: formatObjectLabel(plannerGroup, plannerGroupName) });
    }
  };

  const handleUploadDocuments = async (attachFiles: any) => {
    if (isArray(attachFiles, 1)) {
      const { response, error } = await axiosHandler(() => DocumentServices.uploadDocuments(attachFiles));

      const fileData = get(response, "data");

      if (isArray(fileData, 1)) {
        return fileData.map((f: any) => f.id);
      }

      toast.show(getErrorDetail(error).errorMessage, { type: "warning" });

      return [];
    }
  };

  const handleCreateWorkOrder = async () => {
    dispatch(CommonActions.openLoading.request({}));

    const {
      // section 1
      notification,
      // section 2
      fullDescription,
      workOrderType,
      activityKey,
      // section 3
      maintPlant,
      workCenter,
      functionalLocation,
      equipment,
      plannerGroup,
      systemCondition,
      priority,
      startDate,
      endDate,
      // section 4
      personnel,
      //
      tasks,
    } = state;

    let wo: WorkOrderCreateParams = {
      workOrderLongText: fullDescription || "",
      workOrderType: workOrderType?.value || "",
      maintenanceActivityKey: activityKey?.value || "",
      equipmentId: equipment?.value || "",
      functionalLocation: functionalLocation?.value || "",
      priority: priority?.value || "",
      mainPersonnel: personnel?.value || "",
      maintenancePlant: maintPlant?.value || "",
      workCenter: workCenter?.value || "",
      plannerGroup: plannerGroup?.value || "",
      systemCondition: systemCondition?.value || "",
      startDate: dayjsFormatDate(startDate, SAP_DATE_FORMAT),
      startTime: dayjsFormatDate(startDate, SAP_TIME_FORMAT),
      finishDate: dayjsFormatDate(endDate, SAP_DATE_FORMAT),
      finishTime: dayjsFormatDate(endDate, SAP_TIME_FORMAT),
      notification: notification?.value || "",
    };

    let lastOperationId = 0;
    let getOperationId = (operationId: any) => {
      operationId = String(operationId);
      while (operationId.length < 4) {
        operationId = "0" + operationId;
      }
      return operationId;
    };

    const mappingTaskToOperation = (task: any, superOperationId: string, documentIds: any) => {
      const {
        fullDescription,
        activityType,
        controlKey,
        maintPlant,
        workCenter,
        estimate,
        functionalLocation,
        equipment,
      } = task;
      lastOperationId += 10;
      let operationId = getOperationId(lastOperationId);
      let operation: WorkOrderOperationCreateParams = {
        workOrderId: "",
        operationId,
        superOperationId: superOperationId,
        operationDesc: genShortDescription(fullDescription),
        operationLongText: fullDescription,
        equipmentId: equipment?.value || "",
        maintenancePlant: maintPlant?.value,
        workCenter: workCenter?.value,
        functionalLocationId: functionalLocation?.value || "",
        controlKey: controlKey?.value,
        personnel: personnel?.value || "",
        estimate: estimate || "",
        unit: "MIN",
        activityType: activityType?.value || "",
        documentIds: documentIds,
        subOperations: [],
      };
      return operation;
    };

    const operations: any = [];

    for (let i = 0; i < tasks.length; i++) {
      let task = tasks[i];
      let childs = task.childs || [];
      let documentIds = await handleUploadDocuments(task.files);
      let parentOperation = mappingTaskToOperation(task, "", documentIds);

      for (let j = 0; j < childs.length; j++) {
        let child = childs[j];
        let childDocumentIds = await handleUploadDocuments(child.files);
        let childOpreation = mappingTaskToOperation(child, parentOperation.operationId, childDocumentIds);

        parentOperation.subOperations.push(childOpreation);
      }

      operations.push(parentOperation);
    }

    const { response, error } = await axiosHandler(() => WorkOrderServices.createWorkOrder(wo, operations));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      const document = get(response, "data.document");
      toast.show(`Tạo lệnh bảo trì ${document} thành công!`, { type: "success" });
      handleCallback();
    }
  };

  const handleCallback = () => {
    const callback = getScreenParams(props).callback || "work_order_list_cb";
    // @ts-ignore
    (global[callback] || (() => null))();
    changeScreen(props);
  };

  const handleOnCTA = async () => {
    const {
      // section 1
      notification,
      // section 2
      fullDescription,
      workOrderType,
      activityKey,
      // section 3
      maintPlant,
      workCenter,
      functionalLocation,
      equipment,
      plannerGroup,
      systemCondition,
      priority,
      startDate,
      endDate,
      // section 4
      personnel,
      //
      tasks,
    } = state;
    switch (tabIdx) {
      case 0:
        {
          let isValidData = true,
            _errors: any = {},
            focusInputField: any = null,
            setInvalid = (id: any) => {
              isValidData = false;
              _errors[id] = true;
              if (!focusInputField) focusInputField = id;
            };

          const requiredFields = [
            // section 2
            "fullDescription",
            "workOrderType",
            // section 3
            "maintPlant",
            "workCenter",
            "plannerGroup",
            "priority",
            "startDate",
            "endDate",
            // section 4
            "personnel",
          ];

          if (!!activityKey) {
            requiredFields.push("systemCondition");
          }

          requiredFields.forEach((requiredField: any) => {
            if (!state[requiredField]) setInvalid(requiredField);
          });

          if (!state["equipment"] && !state["functionalLocation"]) {
            setInvalid("equipment");
            setInvalid("functionalLocation");
          }

          if (startDate && endDate && +startDate > +endDate) {
            _errors.endDate = true;
          }
          setErrors(_errors);
          if (isValidData) {
            handleChangeTab(1);
          } else {
            // TODO: scroll into focusInputField
            toast.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
          }
        }
        break;
      case 1:
        {
          if (tasks.length) {
            handleChangeTab(1);
          } else {
            toast.show("Tối thiểu cần có một công việc!", { type: "danger" });
          }
        }
        break;
      default:
        {
          handleCreateWorkOrder();
        }
        break;
    }
  };

  const _renderHeader = () => {
    let isFirstTab = !tabIdx;
    return (
      <CNavbar
        title={"Tạo lệnh bảo trì"}
        leftIcon={isFirstTab ? <ICCloseY /> : <ICBackSVG />}
        onLeftPress={() => (isFirstTab ? setShowConfirm(true) : handleChangeTab(-1))}
      />
    );
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isScanData}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setScanDataStatus(false)}
          onResult={handleSearchScanResult}
        />
        <CAlertModal
          open={isScanError}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => setScanError(false)}
        />
      </Fragment>
    );
  };

  return (
    <View style={styles.screen}>
      <>
        {_renderHeader()}
        {havePermission && (
          <Fragment>
            <CTabViewCreate
              {...{
                toast,
                state,
                fetchings,
                errors,
                options,
              }}
              index={tabIdx}
              routes={TabViewRoutes}
              onChange={handleChange}
              onSearch={handleSearch}
              onScan={() => setScanDataStatus(true)}
            />

            <CTAButtons
              isSticky
              buttons={[
                ...(!!tabIdx
                  ? [
                      {
                        isSub: true,
                        text: i18n.t("CM.back"),
                        onPress: () => handleChangeTab(-1),
                      },
                    ]
                  : []),
                {
                  text: i18n.t(`CM.${ctaButtonTxts[tabIdx]}`),
                  onPress: handleOnCTA,
                },
              ]}
            />
          </Fragment>
        )}
        {_renderScanQRCode()}
        <CAlertModal
          open={showConfirm}
          title={"Bạn có chắc chắn muốn thoát không?"}
          icon={<ICWarningOctagon />}
          onClose={() => setShowConfirm(false)}
          onCta={() => {
            setShowConfirm(false);
            changeScreen(props);
          }}
        />
        {!havePermission && <CEmptyState noPermission={!havePermission} />}
      </>
    </View>
  );
};

export default WorkOrderCreate;

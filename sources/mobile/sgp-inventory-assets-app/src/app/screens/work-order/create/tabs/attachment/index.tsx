import React, { FC } from "react";
import { ScrollView, View } from "react-native";

import styles from "./styles";

import { Permission, widthResponsive as WR } from "@Constants";

import { CEmptyState, OperationAttachFiles, TextCM } from "@Components";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices } from "../../../../../../services";

const WOCTabAttachment: FC<any> = ({ state, onChange }) => {
  const saInsets = useSafeAreaInsets();
  const { tasks } = state;

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.ATTACHMENT_POST);

  return (
    <ScrollView contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>3. Đính kèm tài liệu</TextCM>
      <View style={styles.attachFiles}>
        {havePermission &&
          tasks.map((task: any, index: any) => {
            return (
              <>
                <OperationAttachFiles
                  {...{ task }}
                  key={task.uuid}
                  header={`Công việc ${index + 1}`}
                  onChange={(_attachFiles: any) => {
                    task.files = [..._attachFiles, ...task.files];
                    Object.assign(tasks[index], task);
                    onChange("tasks", tasks);
                  }}
                  onRemove={(_attachFiles: any) => {
                    task.files = _attachFiles;
                    Object.assign(tasks[index], task);
                    onChange("tasks", tasks);
                  }}
                />

                {task?.childs?.map((child: any, idx: any) => {
                  return (
                    <OperationAttachFiles
                      {...{ task: child }}
                      key={child.uuid}
                      header={`Công việc con ${idx + 1}`}
                      onChange={(_attachFiles: any) => {
                        Object.assign(task.childs[idx], { ...child, files: [..._attachFiles, ...child.files] });
                        onChange("tasks", tasks);
                      }}
                      onRemove={(_attachFiles: any) => {
                        Object.assign(task.childs[idx], { ...child, files: [..._attachFiles] });
                        onChange("tasks", tasks);
                      }}
                    />
                  );
                })}
              </>
            );
          })}
      </View>
      {!havePermission && <CEmptyState noPermission={!havePermission} />}
    </ScrollView>
  );
};

export default WOCTabAttachment;

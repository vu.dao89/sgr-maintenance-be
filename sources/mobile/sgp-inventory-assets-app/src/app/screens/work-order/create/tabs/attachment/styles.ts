import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  title: {
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  attachDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  btnAddItem: {
    marginVertical: WR(20),
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  attachFiles: {
    marginBottom: WR(8),
    marginTop: WR(8),
  },
});

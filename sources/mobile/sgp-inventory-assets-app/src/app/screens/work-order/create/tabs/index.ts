import { default as WOCTabAttachment } from "./attachment";
import { default as WOCTabInfo } from "./info";
import { default as WOCTabTask } from "./task";

export default [
  {
    key: "0",
    Scene: WOCTabInfo,
  },
  {
    key: "1",
    Scene: WOCTabTask,
  },
  {
    key: "2",
    Scene: WOCTabAttachment,
  },
];

import React, { FC } from "react";
import { TouchableOpacity, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import styles from "./styles";

import { widthResponsive as WR } from "@Constants";

import { CCollapsibleSection, CDatePicker, CSelect, CTextInput, TextCM } from "@Components";

import { ICQRC } from "@Assets/image";

const WOCTabInfo: FC<any> = ({
  isEdit,
  saInsets,
  toast,
  state,
  errors,
  fetchings,
  options,
  onChange,
  onSearch,
  onScan,
}) => {
  const { workCenter, workOrderType, maintPlant, activityKey } = state;

  const forms = [
    {
      title: "Thông báo liên quan",
      fields: [
        {
          type: "select",
          id: "notification",
          label: "Thông báo",
          isRequired: false,
        },
      ],
    },
    {
      title: "Thông tin chung",
      fields: [
        {
          type: "input",
          id: "fullDescription",
          label: "Mô tả chi tiết",
          plh: "Nhập mô tả chi tiết",
          isRequired: true,
          multiline: 2,
          maxLength: 5000,
        },
        {
          type: "select",
          id: "workOrderType",
          label: "Phân loại lệnh bảo trì",
          isRequired: true,
          canOpen: () => {
            if (!maintPlant) toast.show("Vui lòng chọn Nơi bảo trì", { type: "warning" });
            return !!maintPlant;
          },
        },
        {
          type: "select",
          id: "activityKey",
          label: "Loại công việc",
          isRequired: false,
        },
      ],
    },
    {
      title: "Thông tin khác",
      fields: [
        {
          type: "select",
          id: "maintPlant",
          label: "Nơi bảo trì",
          isRequired: true,
        },
        {
          type: "select",
          id: "workCenter",
          label: "Tổ đội thực hiện",
          isRequired: true,
        },
        {
          type: "select",
          id: "functionalLocation",
          label: "Khu vực chức năng",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "equipment",
          label: "Mã thiết bị",
          isRequired: true,
          canScan: true,
        },
        {
          type: "select",
          id: "plannerGroup",
          label: "Nhóm lập kế hoạch",
          isRequired: true,
        },
        {
          type: "select",
          id: "systemCondition",
          label: "Loại bảo trì định kỳ",
          isRequired: !!activityKey,
        },
        {
          type: "select",
          id: "priority",
          label: "Mức độ ưu tiên",
          isRequired: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!workOrderType) toast.show("Vui lòng chọn Phân loại lệnh bảo trì", { type: "warning" });
            return !!workOrderType;
          },
        },
        {
          type: "date",
          id: "startDate",
          label: "Thời gian bắt đầu",
          isRequired: true,
          mode: "datetime",
        },
        {
          type: "date",
          id: "endDate",
          label: "Thời gian kết thúc",
          isRequired: true,
          mode: "datetime",
        },
      ],
    },
    {
      title: "Nhân sự",
      fields: [
        {
          type: "select",
          id: "personnel",
          label: "Người giám sát",
          isRequired: true,
          // isSearch: true,
          canOpen: () => {
            // TODO: need to convert toast.show to showToast() utils later...
            if (!workCenter) toast.show("Vui lòng chọn Tổ đội thực hiện", { type: "warning" });
            return !!workCenter;
          },
        },
      ],
    },
  ];

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={[styles.container, { paddingBottom: saInsets.bottom + WR(44 + 24 + 8) }]}>
      <TextCM style={styles.title}>1. {`${isEdit ? "Chỉnh sửa" : "Nhập"} thông tin chính`}</TextCM>

      {forms.map((item: any, index: any) => {
        const { fields, ...section } = item;
        return (
          <CCollapsibleSection key={`info_${index}`} {...section}>
            {fields.map((field: any) => {
              const { type, id, isSearch, canScan, ...fieldProps } = field;
              let _fieldProps = {
                key: id,
                isError: errors[id],
                value: state[id],
                onChange: (data: any) => onChange(id, data),
                ...fieldProps,
              };
              switch (type) {
                case "input":
                  return <CTextInput isRequired {..._fieldProps} />;
                case "date":
                  return <CDatePicker isRequired {..._fieldProps} />;
                case "select": {
                  let selectInputNode = (
                    <CSelect
                      isRequired
                      {..._fieldProps}
                      isFullScreen
                      isFetching={fetchings[id]}
                      selected={state[id]}
                      menus={options[id]}
                      styles={canScan && { wrapper: { flex: 1, marginRight: WR(20) } }}
                      onSelect={(selected: any) => onChange(id, selected)}
                      onSearch={onSearch(id)}
                      {...(!isSearch && { onOpen: onSearch(id) })}
                    />
                  );
                  if (canScan) {
                    return (
                      <View key={id} style={styles.row}>
                        {selectInputNode}
                        <TouchableOpacity onPress={() => onScan("info")}>
                          <ICQRC />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                  if (id === "workOrderType" && isEdit) return null;
                  return selectInputNode;
                }
                default:
                  return null;
              }
            })}
          </CCollapsibleSection>
        );
      })}
    </KeyboardAwareScrollView>
  );
};

export default WOCTabInfo;

import React, { FC, Fragment } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";

import { genShortDescription } from "src/utils/Basic";
import { useStateLazy } from "src/utils/Core";

import styles from "./styles";

import { Color, Permission, widthResponsive as WR } from "@Constants";

import {
  CCollapsibleSection,
  CEmptyState,
  CPersonInfo,
  CTags,
  OperationCreation,
  SubOperationCreation,
  TextCM,
} from "@Components";
import { isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices } from "../../../../../../services";

const WOCTabTask: FC<any> = ({ isEdit, saInsets, showTitle, state, fetchings, options, onChange, onSearch }) => {
  const { tasks } = state;

  const [isCrud, setCrudStatus]: any = useStateLazy(false);
  const [taskState, setTaskState]: any = useStateLazy({});
  const [taskAction, setTaskAction]: any = useStateLazy("create");

  const [isCrudChild, setCrudChildStatus]: any = useStateLazy(false);
  const [childAction, setChildAction]: any = useStateLazy("create");
  const [childState, setChildState]: any = useStateLazy({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.POST);

  return (
    <Fragment>
      <ScrollView
        contentContainerStyle={[
          styles.container,
          {
            paddingBottom: saInsets.bottom + WR(44 + 24 + 8),
          },
        ]}>
        {!!showTitle && <TextCM style={styles.title}>2. {`${isEdit ? "Chỉnh sửa" : "Tạo"} công việc`}</TextCM>}

        {havePermission && (
          <TouchableOpacity
            style={[styles.btnAddItem, { marginTop: showTitle ? WR(16) : 0 }]}
            onPress={() => {
              const { maintPlant, workCenter, functionalLocation, equipment, personnel } = state;
              setTaskState({
                maintPlant: maintPlant,
                workCenter: workCenter,
                functionalLocation: functionalLocation,
                equipment: equipment,
                personnel: personnel,
              });
              setTaskAction("create");
              setCrudStatus(true);
            }}>
            <TextCM style={styles.btnAddTxt}>+ Tạo công việc</TextCM>
          </TouchableOpacity>
        )}

        {tasks.map((item: any, index: any) => {
          const { fullDescription, personnel, childs, maintPlant, workCenter, controlKey } = item;
          let totalChild = childs.length;
          return (
            <CCollapsibleSection
              key={item.uuid}
              title={genShortDescription(fullDescription)}
              menus={[
                {
                  label: "Chỉnh sửa",
                  value: "edit",
                },
                {
                  label: "Xoá",
                  value: "delete",
                  color: Color.Red,
                },
              ]}
              onMenu={({ value: menuValue }: any) => {
                switch (menuValue) {
                  case "delete":
                    {
                      tasks.splice(index, 1);
                      onChange("tasks", tasks);
                    }
                    break;
                  case "edit":
                    setTaskState({
                      ...item,
                      index,
                    });
                    setTaskAction(menuValue);
                    setCrudStatus(true);
                    break;
                  default:
                    break;
                }
              }}>
              {!!fullDescription && <TextCM style={styles.taskDesc}>{fullDescription}</TextCM>}

              <CTags data={[{ label: "Đã nhận", color: "#3A73B7" }]} />

              <View style={styles.taskAssigneeWrapper}>
                <CPersonInfo
                  label={"Đã giao cho"}
                  fullName={personnel?.fullName || personnel?.label}
                  avatar={personnel?.avatar}
                  style={styles.taskAssigneePerson}
                />
              </View>
              <View style={styles.taskOtherJobWrapper}>
                <TouchableOpacity
                  style={styles.btnAddItem}
                  onPress={() => {
                    setChildState({
                      parentIndex: index,
                      maintPlant: maintPlant,
                      workCenter: workCenter,
                      controlKey: controlKey,
                    });
                    setChildAction("create");
                    setCrudChildStatus(true);
                  }}>
                  <TextCM style={styles.btnAddTxt}>+ Tạo công việc con</TextCM>
                </TouchableOpacity>

                {!!totalChild && <TextCM style={styles.taskOtherJobCounter}>{totalChild} đã tạo</TextCM>}
              </View>
            </CCollapsibleSection>
          );
        })}

        {!havePermission && isEmpty(tasks) && <CEmptyState noPermission={!havePermission} />}
      </ScrollView>

      <OperationCreation
        saInsets={saInsets}
        isCrud={isCrud}
        taskAction={taskAction}
        initState={taskState}
        onClose={() => setCrudStatus(false)}
        onChange={(task: any) => {
          const { index } = task;
          if (taskAction === "create") {
            tasks.push(task);
          } else {
            Object.assign(tasks[index], { ...task });
          }
          onChange("tasks", tasks);
        }}
      />

      <SubOperationCreation
        saInsets={saInsets}
        isCrud={isCrudChild}
        childAction={childAction}
        initState={childState}
        onClose={() => setCrudChildStatus(false)}
        onChange={(child: any) => {
          const { parentIndex } = child;
          if (childAction === "create") {
            tasks[parentIndex].childs.push(child);
          }
          onChange("tasks", tasks);
        }}
      />
    </Fragment>
  );
};

export default WOCTabTask;

import { StyleSheet } from "react-native";

import { Color, Font, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(30),
    paddingHorizontal: WR(24),
  },
  title: {
    marginBottom: WR(8),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontBigger,
  },
  btnAddItem: {
    flexDirection: "row",
    alignItems: "center",
  },
  btnAddTxt: {
    color: Color.Yellow,
  },
  crudModalContent: {
    paddingHorizontal: WR(24),
    paddingBottom: WR(4),
  },
  fetchingDataOverlay: {
    zIndex: 100,
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "#00000040",
  },
  taskAssigneeWrapper: {
    marginTop: WR(20),
    marginBottom: WR(20),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneePerson: {
    flex: 1,
  },
  labelTxt: {
    color: "#8D9298",
    fontFamily: Font.Roboto,
    fontSize: FontSize.FontTiniest,
  },
  taskDesc: {
    marginBottom: WR(8),
    color: Color.White,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  createSubView: {
    marginTop: WR(20),
    marginBottom: WR(20),
  },
  taskOtherJobWrapper: {
    paddingTop: WR(16),
    paddingBottom: WR(12),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderTopWidth: 1,
    borderTopColor: "#414042",
  },
  taskOtherJobCounter: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
});

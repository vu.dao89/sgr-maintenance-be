import { get, isEmpty } from "lodash";
import React, { FC, useEffect, useState } from "react";
import { Image, ScrollView, TouchableOpacity, View } from "react-native";

import { NotificationServices, PermissionServices, WorkOrderServices } from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { useIsMounted } from "src/utils/Core";
import { formatSapDateTime } from "src/utils/DateTime";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import TabViewRoutes from "./tabs";

import {
  CEmptyState,
  CNavbar,
  CPersonInfo,
  CPersonSelector,
  CSpinkit,
  CTAButtons,
  CTabViewDetail,
  CTags,
  LockWorkOrder,
  NotificationActivitiesCreation,
  TextCM,
} from "@Components";
import { Permission, ScreenName, widthResponsive as WR } from "@Constants";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import { ICEdit2, ICMap, IMGDefault } from "@Assets/image";

import { isArray } from "../../../../utils/Basic";

import {
  formatNotificationListItem,
  formatObjectLabel,
  genAttachFiles,
  removeLeadingZeros,
} from "../../../../utils/Format";

import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import SystemStatus from "../../../../constants/SystemStatus";
import { CommonActions } from "../../../../redux/actions";
import { AppState } from "../../../../redux/reducers";
import { OperationTaskItemProps } from "../../../components/operation/task-item";

const WorderOrderDetail: FC<any> = props => {
  const toast = useToast();
  const isMounted = useIsMounted();
  const dispatch = useDispatch();

  const saInsets = useSafeAreaInsets();
  const [isLock, setIsLock] = useState(false);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.VIEW);

  const [headerInfoMinHeight, setHeaderInfoMinHeight] = useState(0);
  const [tabViewMinHeight, setTabViewMinHeight] = useState(0);

  const [isChangeWorker, setChangeWorker] = useState(false);

  const [isFetching, setFetching] = useState(true);
  const [detail, setDetail]: any = useState(null);
  const [operations, setOperations]: any = useState([]);

  const [ctaButton, setCtaButton]: any = useState(null);

  const [isOpenCreateActivities, setIsOpenCreateActivities] = useState(false);

  const [notificationType, setNotificationType] = useState({});

  useEffect(() => {
    if (havePermission) {
      handleGetDetail();
    } else {
      setFetching(false);
    }

    // @ts-ignore
    global["work_order_detail_cb"] = () => handleCallback();
    return () => {
      // @ts-ignore
      delete global["work_order_detail_cb"];
    };
  }, []);

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "tasks":
        setOperations(data);
        break;

      case "reload":
        handleGetDetail();
        break;

      default:
        break;
    }
  };

  const handleStartWorkOrder = async (workOrderId: string) => {
    dispatch(CommonActions.openLoading.request({}));

    const { error } = await axiosHandler(() => WorkOrderServices.startWorkOrder(workOrderId));

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      handleCallback();
    }
  };

  const handleCompleteWorkOrder = async (workOrderId: string, acitivites: any) => {
    const notificationId = detail?.notification?.id;
    if (notificationId && isEmpty(acitivites)) {
      setIsOpenCreateActivities(true);
      return;
    }

    dispatch(CommonActions.openLoading.request({}));

    const { error } = await axiosHandler(() =>
      WorkOrderServices.completeWorkOrder(workOrderId, notificationId || "", acitivites)
    );

    dispatch(CommonActions.openLoading.success({}));
    if (!isMounted()) return;

    if (error) {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    } else {
      handleCallback();
    }
  };

  const handleOnCTA = async (action: string, workOrderId: string) => {
    switch (action) {
      case "start":
        handleStartWorkOrder(workOrderId);
        break;

      case "complete":
        handleCompleteWorkOrder(workOrderId, []);
        break;
      case "lock":
        setIsLock(true);
        break;
    }
  };

  const handleChangeMainPerson = async (person: any) => {
    dispatch(CommonActions.openLoading.request({}));

    const workOrderId = detail?.id;
    const personnelId = get(person, "code");

    const { response, error } = await axiosHandler(() => WorkOrderServices.changePersonnel(workOrderId, personnelId));

    if (!isMounted()) return;

    dispatch(CommonActions.openLoading.success({}));

    if (get(response, "status") === 204) {
      toast.show("Đã đổi người thành công", { type: "success" });
      setFetching(true);
      changeScreen(props);
      // @ts-ignore
      (global[`work_order_list_cb`] || (() => null))();
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  const handleCallback = () => {
    // @ts-ignore
    (global["work_order_list_cb"] || (() => null))();
    handleGetDetail();
  };

  const handleGetDetail = async () => {
    try {
      setCtaButton(null);
      setDetail(null);
      setFetching(true);

      const { id } = getScreenParams(props);
      let resp = await axiosHandler(() => WorkOrderServices.getWorkOrderDetails(id));
      if (!isMounted()) return;
      let { workOrder, operation }: any = get(resp, "response.data", {});
      let operationItems = get(operation, "item", []);

      if (!workOrder) return;

      const {
        workOrderDescription,
        woStartDate,
        woStartTime,
        woFinishDate,
        woFinishTime,
        priorityText,
        workOrderType,
        workOrderTypeDescription,
        workOrderSystemStatus,
        workOrderSystemStatusDescription,
        workOrderLongDescription,
        equipmentId,
        equipmentDescription,
        mainPlant,
        maintenancePlantDescription,
        notificationId,
        personnelName,
        priorityType,
        commonStatus,
        mainPerson,
      } = workOrder;

      let priorityColor = get(priorityType, "colorCode", "#3A73B7");
      let statusColor = get(commonStatus, "colorCode", "#3A73B7");

      let _detail = {
        ...workOrder,
        id,
        title: workOrderDescription,
        startedDateTxt: formatSapDateTime(woStartDate, woStartTime),
        endedDateTxt: formatSapDateTime(woFinishDate, woFinishTime),
        tags: [
          { color: priorityColor, label: priorityText },
          { color: "#3A73B7", label: formatObjectLabel(workOrderType, workOrderTypeDescription) },
          { color: statusColor, label: workOrderSystemStatusDescription },
        ].filter((t: any) => !isEmpty(t.label)),
        desc: workOrderLongDescription || "",
        equipment: equipmentId && {
          id: equipmentId,
          name: formatObjectLabel(equipmentId, equipmentDescription, true),
          image: "",
        },
        mainPlant: mainPlant && {
          name: formatObjectLabel(mainPlant, maintenancePlantDescription, true),
          code: mainPlant,
        },
        reportedBy: personnelName && {
          fullName: personnelName,
          code: mainPerson,
        },
        workOrderSystemStatus,
      };

      if (notificationId) {
        let resp = await axiosHandler(() => NotificationServices.getDetailNotification(notificationId));
        let notification = get(resp, "response.data.header", null);
        _detail.notification = formatNotificationListItem(notification);
      }

      let ctaButtons = [];
      switch (workOrderSystemStatus) {
        case SystemStatus.workorder.CREATE_NEW:
          {
            if (PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.LOCK)) {
              ctaButtons.push({
                text: "Khóa",
                isSub: true,
                onPress: () => handleOnCTA("lock", id),
              });
            }
            if (PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.CONFIRM_START)) {
              ctaButtons.push({
                text: "Bắt đầu",
                onPress: () => handleOnCTA("start", id),
              });
            }
          }
          break;

        case SystemStatus.workorder.READY:
          {
            if (PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.LOCK)) {
              ctaButtons.push({
                text: "Khóa",
                isSub: true,
                onPress: () => handleOnCTA("lock", id),
              });
            }
          }
          break;

        case SystemStatus.workorder.FINISH_OPS:
          {
            if (PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.CONFIRM_COMPLETE)) {
              ctaButtons.push({
                text: "Hoàn thành",
                onPress: () => handleOnCTA("complete", id),
              });
            }
          }
          break;

        default:
          break;
      }

      setCtaButton(ctaButtons);
      setDetail(_detail);

      let _operations: any = [];

      if (isArray(operationItems, 1)) {
        let mainPlant = get(workOrder, "mainPlant", "");
        let workCenter = get(workOrder, "workCenter", "");

        operationItems.forEach((item: any) => {
          let {
            workOrderId,
            operationId,
            operationDescription,
            operationLongText,
            operationSystemStatus,
            operationSystemStatusDescription,
            personnel,
            personnelName,
            prtRequire,
            prtConfirm,
            reservation,
            reservationWithdraw,
            subOperationRequire,
            subOperationConfirm,
            documents,
            actualStart,
            actualStarTime,
            actualFinish,
            actualFinishTime,
            operationStatus,
            result,
            varianceReason,
            estimate,
            operationHistory,
          } = item;

          const operation: OperationTaskItemProps = {
            id: operationId,
            title: operationDescription,
            desc: operationLongText || operationDescription,
            tags: [
              {
                label: operationSystemStatusDescription,
                color: operationStatus?.colorCode ? `#${operationStatus.colorCode}` : "#3A73B7",
              },
              {
                label: result,
                color: varianceReason?.color_code || "#3A73B7",
              },
            ],
            infos: [
              {
                label: "Thời gian dự kiến",
                value: `${estimate} phút`,
              },
            ],
            assignee: personnel && {
              fullName: personnelName,
              code: personnel,
            },
            prtRequire: prtRequire,
            reservation: reservation,
            subOperationRequire: subOperationRequire,
            subOperationConfirm: subOperationConfirm,
            mainPlantCode: mainPlant,
            workCenterCode: workCenter,
            workOrderId,
            operationStatusCode: operationSystemStatus,
            subTasks: [],
            history: operationHistory,
            workOrderStatusCode: workOrderSystemStatus,
            actualStartDateTime: formatSapDateTime(actualStart, actualStarTime),
            actualFinishDateTime: formatSapDateTime(actualFinish, actualFinishTime),
            prtConfirm: prtConfirm,
            reservationWithdraw: reservationWithdraw,
            actualStart: actualStart,
            actualStarTime: actualStarTime,
            callback: "work_order_detail_cb",
            supervisor: mainPerson,
          };

          _operations.push({
            ...operation,
            uuid: `${workOrderId}_${operationId}`,
            files: genAttachFiles(documents),
            activityType: {
              label: operationDescription,
            },
            fullDescription: operationLongText,
          });
        });
        // console.log(`🚀 Kds: handleGeDetail -> _operations`, _operations);
      }

      setOperations(_operations);

      setFetching(false);
    } catch (error) {
      console.log(`🚀 Kds: WorderOrderDetail -> handleGetDetail -> error`, error);
    }
  };

  const canEdit =
    !!detail && PermissionServices.canEditWorkOrder(reducer, detail?.workOrderSystemStatus, detail?.mainPerson);

  const _renderHeader = () => {
    return (
      <CNavbar
        title={"Chi tiết lệnh bảo trì"}
        onLeftPress={() => changeScreen(props)}
        {...(canEdit && {
          rightIcon: <ICEdit2 />,
          onRightPress: () => changeScreen(props, ScreenName.WorkOrderEdit, { dataId: detail.id, openFrom: "detail" }),
        })}
      />
    );
  };

  const _renderInfo = () => {
    const {
      id,
      title,
      mainPlant,
      startedDateTxt,
      endedDateTxt,
      tags,
      equipment,
      reportedBy,
      mainPerson,
      desc,
      workOrderSystemStatus,
    } = detail;

    const canChangeSupervisor = PermissionServices.canChangeSupervisor(reducer, workOrderSystemStatus, mainPerson);

    return (
      <View style={styles.infoWrapper} onLayout={e => setHeaderInfoMinHeight(e.nativeEvent.layout.height)}>
        <View style={styles.content}>
          <View style={styles.equipmentNameContainer}>
            <View style={styles.equipmentNameWrapper}>
              <TextCM style={styles.infoTitle}>{title}</TextCM>

              <TextCM style={styles.infoId}>{removeLeadingZeros(id)}</TextCM>

              <TextCM style={styles.infoDates}>
                {startedDateTxt} - {endedDateTxt}
              </TextCM>

              <View style={styles.listItemFooter}>
                <CTags data={tags} style={styles.infoTags} />
              </View>
            </View>
            <View style={styles.equipmentImageWrapper}>
              <Image source={equipment?.image ? { uri: equipment.image } : IMGDefault} style={styles.imgEq} />
            </View>
          </View>

          {!!desc && <TextCM style={[styles.infoDesc, { marginTop: WR(16) }]}>{desc}</TextCM>}

          {!!equipment && (
            <View>
              <TextCM style={styles.txtTabTitle}>Thiết bị</TextCM>
              <TextCM style={styles.infoDesc}>{equipment.name}</TextCM>
            </View>
          )}

          {!!mainPlant && (
            <TouchableOpacity style={styles.infoLocation}>
              <ICMap />
              <TextCM style={styles.infoLocationTxt}>{mainPlant.name}</TextCM>
            </TouchableOpacity>
          )}
        </View>

        <View style={styles.infoPersonInCharge}>
          <CPersonInfo {...reportedBy} label={"Chịu trách nhiệm chính"} style={{ flex: 1 }} />

          {canChangeSupervisor.isShow && (
            <TouchableOpacity
              style={[styles.taskAssigneeBtn, canChangeSupervisor.isDisabled && { opacity: 0.5 }]}
              disabled={canChangeSupervisor.isDisabled}
              onPress={() => setChangeWorker(true)}>
              <TextCM style={styles.taskAssigneeBtnTxt}>Đổi người</TextCM>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.screenWrapper}>
      {_renderHeader()}

      {(isFetching && (
        <View style={styles.bodyCenter}>
          <CSpinkit />
        </View>
      )) ||
        (detail && (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              height: headerInfoMinHeight + tabViewMinHeight,
            }}>
            {_renderInfo()}
            <CTabViewDetail
              detail={detail}
              operations={operations}
              routes={TabViewRoutes}
              ctaButton={ctaButton}
              onLayout={setTabViewMinHeight}
              onChange={handleChange}
              onNavigate={(...args: any) => changeScreen(props, ...args)}
            />
          </ScrollView>
        )) || <CEmptyState noPermission={!havePermission} />}

      {isArray(ctaButton, 1) && <CTAButtons isSticky buttons={ctaButton} />}

      <CPersonSelector
        open={isChangeWorker}
        title={"Đổi người"}
        selected={{ code: detail?.mainPerson }}
        workCenterCode={detail?.workCenter}
        mainPlantCode={detail?.mainPlant?.code}
        onClose={() => setChangeWorker(false)}
        onSelect={handleChangeMainPerson}
      />

      <NotificationActivitiesCreation
        title={`Hoàn tất bảo trì ${formatObjectLabel(detail?.id, detail?.title)}`}
        saInsets={saInsets}
        isOpen={isOpenCreateActivities}
        notificationType={notificationType}
        onClose={() => setIsOpenCreateActivities(false)}
        onChange={(activities: any) => {
          setIsOpenCreateActivities(false);
          handleCompleteWorkOrder(detail?.id, activities);
        }}
      />

      <LockWorkOrder
        saInsets={saInsets}
        isCrud={isLock}
        workOrderId={detail?.workOrderId}
        onClose={() => setIsLock(false)}
        onChange={() => {
          setIsLock(false);
          handleCallback();
        }}
      />
    </View>
  );
};

export default WorderOrderDetail;

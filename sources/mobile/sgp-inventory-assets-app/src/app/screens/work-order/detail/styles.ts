import { StyleSheet } from "react-native";

import { Color, FontSize, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screenWrapper: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  bodyCenter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  dataEmptyTxt: {
    textAlign: "center",
    color: Color.White,
    fontSize: FontSize.FontMedium,
  },
  infoWrapper: {
    paddingVertical: WR(8),
    paddingHorizontal: WR(24),
  },
  infoTitle: {
    marginBottom: WR(4),
    color: Color.White,
    fontWeight: "600",
    fontSize: FontSize.FontMedium,
  },
  infoId: {
    marginBottom: WR(4),
    color: Color.White,
  },
  infoDates: {
    color: "#ACACAC",
    fontSize: FontSize.FontTiny,
  },
  infoDesc: {
    color: Color.White,
    fontSize: FontSize.FontTiny,
  },
  infoTags: {
    marginTop: WR(8),
  },
  listItemFooter: {
    marginTop: WR(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  infoLocation: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  infoLocationTxt: {
    marginLeft: WR(8),
    color: Color.Yellow,
  },
  infoPersonInCharge: {
    marginTop: WR(16),
    flexDirection: "row",
    alignItems: "center",
  },
  taskAssigneeBtn: {
    marginLeft: WR(8),
    height: WR(36),
    paddingHorizontal: WR(16),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: WR(10),
    backgroundColor: "#D8D8D8",
  },
  taskAssigneeBtnTxt: {
    fontWeight: "500",
  },
  content: {
    paddingHorizontal: WR(0),
    paddingTop: WR(10),
  },
  equipmentNameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  equipmentNameWrapper: {
    flex: 2,
  },
  equipmentImageWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  imgEq: {
    width: WR(90),
    height: HR(90),
    borderRadius: HR(20),
  },
  txtTabTitle: {
    marginTop: WR(12),
    marginBottom: WR(8),
    fontWeight: "200",
    fontSize: FontSize.FontTiny,
    color: Color.GrayLight,
  },
});

import React, { FC } from "react";
import { View } from "react-native";

import styles from "./styles";

import { CEmptyState, OperationAttachFiles } from "@Components";
import { Permission, SystemStatus, widthResponsive as WR } from "@Constants";
import { get, isEmpty } from "lodash";
import { useToast } from "react-native-toast-notifications";
import { useDispatch, useSelector } from "react-redux";
import { CommonActions } from "../../../../../../redux/actions";
import { AppState } from "../../../../../../redux/reducers";
import { DocumentServices, PermissionServices } from "../../../../../../services";
import { axiosHandler, getErrorDetail } from "../../../../../../services/httpClient";
import { isArray } from "../../../../../../utils/Basic";
import { useIsMounted } from "../../../../../../utils/Core";
import { CFile } from "../../../../../components/common/attach-files";

const WODTabAttachment: FC<any> = ({ saInsets, onChange, operations, ctaButton, detail }) => {
  const isMounted = useIsMounted();
  const toast = useToast();
  const dispatch = useDispatch();

  const { workOrderId, workOrderSystemStatus } = detail;

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.ATTACHMENT_POST);
  const isCompleted = SystemStatus.workorder.isCompleted(workOrderSystemStatus);
  const canUpload = havePermission && !!!isCompleted;
  const isEmptyFiles = operations?.every((op: any) => isEmpty(op?.files));

  const handleAttachFiles = async (operationId: string, index: any, attachFiles: Array<CFile>) => {
    dispatch(CommonActions.openLoading.request({}));

    const { response, error } = await axiosHandler(() =>
      DocumentServices.uploadOperationDocuments(workOrderId, operationId, attachFiles)
    );

    dispatch(CommonActions.openLoading.success({}));

    if (!isMounted()) return;

    if (get(response, "data")) {
      const task = operations[index];
      task.files = [...attachFiles, ...task.files];
      Object.assign(operations[index], task);

      onChange("tasks", [...operations]);
    } else {
      toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
    }
  };

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: isArray(ctaButton, 1) ? saInsets.bottom + WR(72) : saInsets.bottom + WR(24) },
      ]}>
      {operations.map((task: any, index: any) => {
        if (!canUpload && isEmpty(task?.files)) return null;
        return (
          <OperationAttachFiles
            {...{ task }}
            key={`${task.uuid}_${index}`}
            onChange={canUpload && ((attachFiles: any) => handleAttachFiles(task.id, index, attachFiles))}
          />
        );
      })}
      {isEmptyFiles && !canUpload && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default WODTabAttachment;

import { StyleSheet } from "react-native";

import { widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    paddingVertical: WR(12),
    paddingHorizontal: WR(24),
  },
});

import React, { FC } from "react";

import { View } from "react-native";

import { widthResponsive as WR } from "@Constants";

import styles from "./styles";

import { CCollapsibleSection, MapOnlyView, NListItem, TextCM } from "@Components";
import { isArray } from "../../../../../../utils/Basic";
import { formatObjectLabel } from "../../../../../../utils/Format";

const WODTabGeneral: FC<any> = ({ saInsets, detail, ctaButton }) => {
  const {
    workOrderType,
    workOrderTypeDescription,
    mainActKey,
    mainActivityKeyDescription,
    mainPlant,
    maintenancePlantDescription,
    workCenter,
    workCenterDescription,
    plannerGroup,
    plannerGroupDescription,
    systemConditionText,
    startedDateTxt,
    endedDateTxt,
    notification,
  } = detail;

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: isArray(ctaButton, 1) ? saInsets.bottom + WR(72) : saInsets.bottom + WR(24) },
      ]}>
      <View pointerEvents="none">
        <TextCM style={styles.mapLabel}>Vị trí</TextCM>
        <MapOnlyView
          style={styles.mapViewer}
          region={{
            latitude: 15.9783846,
            longitude: 108.2598785,
          }}
        />
      </View>

      <CCollapsibleSection
        title={"Thông tin chung"}
        infos={[
          {
            label: "Phân loại lệnh bảo trì",
            value: formatObjectLabel(workOrderType, workOrderTypeDescription, true),
          },
          {
            label: "Phân nhóm bảo trì",
            value: formatObjectLabel(mainActKey, mainActivityKeyDescription, true),
          },
        ]}
      />

      <CCollapsibleSection
        title={"Thông tin khác"}
        infos={[
          {
            label: "Nơi bảo trì",
            value: mainPlant?.name || maintenancePlantDescription,
          },
          {
            label: "Tổ đội thực hiện",
            value: formatObjectLabel(workCenter, workCenterDescription, true),
          },
          {
            label: "Nhóm lập kế hoạch",
            value: formatObjectLabel(plannerGroup, plannerGroupDescription, true),
          },
          {
            label: "Loại bảo trì định kỳ",
            value: systemConditionText,
          },
          {
            label: "Ngày bắt đầu",
            value: startedDateTxt,
          },
          {
            label: "Ngày kết thúc",
            value: endedDateTxt,
          },
        ]}
      />

      {!!notification && (
        <CCollapsibleSection
          title={"Thuộc thông báo"}
          children={
            <NListItem
              noTimeDiff
              item={notification}
              style={{
                paddingVertical: 0,
                paddingHorizontal: 0,
                paddingBottom: WR(28),
              }}
              // onPress={() => changeScreen(props, ScreenName.NotificationDetail, { item })}
            />
          }
        />
      )}
    </View>
  );
};

export default WODTabGeneral;

import { StyleSheet } from "react-native";

import { Color, heightResponsive as HR, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  container: {
    padding: WR(24),
  },
  mapLabel: {
    fontWeight: "400",
    color: "#8D9298",
  },
  mapViewer: {
    marginTop: HR(10),
    height: HR(150),
    borderRadius: HR(16),
    overflow: "hidden",
  },
});

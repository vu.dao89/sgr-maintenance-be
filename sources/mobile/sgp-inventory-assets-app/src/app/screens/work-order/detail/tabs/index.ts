import { default as WODTabGeneral } from "./general";
import { default as WODTabTask } from "./task";
import { default as WODTabAttachment } from "./attachment";

export default [
  {
    key: "0",
    padding: 24,
    title: "Tổng quan",
    Scene: WODTabGeneral,
  },
  {
    key: "1",
    padding: 24,
    title: "Công việc",
    Scene: WODTabTask,
  },
  {
    key: "2",
    padding: 30,
    title: "Tài liệu",
    Scene: WODTabAttachment,
  },
];

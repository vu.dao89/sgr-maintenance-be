import React, { FC } from "react";

import { TouchableOpacity, View } from "react-native";

import { Permission, ScreenName, SystemStatus, widthResponsive as WR } from "@Constants";

import styles from "./styles";

import { CEmptyState, OperationTaskItem, TextCM } from "@Components";
import { useNavigation } from "@react-navigation/native";
import { isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { AppState } from "../../../../../../redux/reducers";
import { PermissionServices } from "../../../../../../services";
import { isArray } from "../../../../../../utils/Basic";

const WOTabTask: FC<any> = ({ saInsets, operations, onChange, ctaButton, detail }) => {
  const nav = useNavigation();

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.OPERATION.POST);

  const { workOrderSystemStatus, mainPerson } = detail;

  const handleCreateOperation = () => {
    nav.navigate(
      ScreenName.OperationCreate as never,
      {
        workOrder: detail,
        callback: "work_order_detail_cb",
      } as never
    );
  };

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: isArray(ctaButton, 1) ? saInsets.bottom + WR(72) : saInsets.bottom + WR(24) },
      ]}>
      {havePermission && workOrderSystemStatus === SystemStatus.workorder.CREATE_NEW && (
        <TouchableOpacity style={[styles.btnAddItem]} onPress={() => handleCreateOperation()}>
          <TextCM style={styles.btnAddTxt}>+ Thêm công việc</TextCM>
        </TouchableOpacity>
      )}

      {operations.map((item: any, index: any) => (
        <OperationTaskItem
          key={item.id + "_" + index}
          handleAction={(activity: string) => {
            switch (activity) {
              case "reload":
                onChange("reload", null);
                break;
            }
          }}
          {...item}
          mainPerson={mainPerson}
          callback={"work_order_detail_cb"}
        />
      ))}

      {!havePermission && isEmpty(operations) && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default WOTabTask;

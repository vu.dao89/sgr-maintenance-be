import { get } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { useToast } from "react-native-toast-notifications";
import { useSelector } from "react-redux";

import i18n from "@I18n";

import { EquipmentServices, MasterDataServices, PermissionServices, WorkOrderServices } from "@Services";
import { axiosHandler, getErrorDetail } from "@Services/httpClient";

import { AppState } from "src/redux/reducers";

import { genRandomString } from "src/utils/Basic";
import { useIsMounted, useStateLazy } from "src/utils/Core";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatPriorityLabel,
  formatWorkCenterLabel,
  mappingSelections,
} from "src/utils/Format";
import { changeScreen, getScreenParams } from "src/utils/Screen";

import styles from "./styles";

import { CAlertModal, CEmptyState, CNavbar, CScanQRCodeModal, CSpinkit, CTAButtons } from "@Components";

import { ICCloseY, ICGhost, ICWarningOctagon } from "@Assets/image";
import { Permission } from "../../../../constants";
import { WorkOrderChangeParams } from "../../../../services/parameters";
import { dayjsFormatDate, dayjsGetDate, SAP_DATE_FORMAT, SAP_TIME_FORMAT } from "../../../../utils/DateTime";
import WOCTabInfo from "./tabs/info";

const relatatedOptions: any = {
  maintPlant: "workCenter",
  workCenter: "personnel",
  workOrderType: "activityKey",
};

const WorkOrderEdit: FC<any> = props => {
  const isMounted = useIsMounted();

  const toast = useToast();
  const reqIds: any = useRef({});

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.POST);

  const dataId = useState(getScreenParams(props).dataId || "")[0];
  const isEdit = useState(!!dataId)[0];
  const [dataReady, setDataReady] = useState(!isEdit);
  const [dataValid, setDataValid] = useState(!isEdit);

  const [errors, setErrors] = useStateLazy({});
  const [fetchings, setFetchings] = useStateLazy({});
  const [options, setOptions] = useStateLazy({});
  const [canChangeSupervisor, setCanChangeSupervisor] = useStateLazy(false);

  const [state, setState]: any = useStateLazy({
    tasks: [],
    notification: getScreenParams(props).notification || null,
    attachFiles: [],
  });

  const [isScanError, setScanError] = useState(false);
  const [isScanData, setScanDataStatus] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  useEffect(() => {
    isEdit && havePermission && handleGetEditWorkOrder();
  }, []);

  const handleGetEditWorkOrder = async () => {
    const { response } = await axiosHandler(() => WorkOrderServices.getWorkOrderDetails(dataId), { delay: true });

    if (!isMounted()) return;

    const workOrder = get(response, "data.workOrder");

    if (workOrder) {
      let detailState: any = {};

      const {
        workOrderDescription,
        workOrderLongDescription,
        workOrderType,
        workOrderTypeDescription,
        mainActKey,
        mainActivityKeyDescription,
        mainPlant,
        maintenancePlantDescription,
        workCenter,
        workCenterDescription,
        functionalLocationId,
        functionalLocationDescription,
        equipmentId,
        equipmentDescription,
        plannerGroup,
        plannerGroupDescription,
        systemCondition,
        systemConditionText,
        priorityType,
        mainPerson,
        personnelName,
        woStartDate,
        woStartTime,
        woFinishDate,
        woFinishTime,
        workOrderSystemStatus,
      } = workOrder;

      setCanChangeSupervisor(PermissionServices.canChangeSupervisor(reducer, workOrderSystemStatus, mainPerson));

      Object.assign(detailState, {
        fullDescription: workOrderLongDescription || workOrderDescription || "",
        workOrderType: workOrderType && {
          value: workOrderType,
          label: formatObjectLabel(workOrderType, workOrderTypeDescription),
          priorityTypeId: priorityType?.type,
        },
        activityKey: mainActKey && {
          value: mainActKey,
          label: formatObjectLabel(mainActKey, mainActivityKeyDescription),
        },
        maintPlant: mainPlant && { value: mainPlant, label: formatObjectLabel(mainPlant, maintenancePlantDescription) },
        workCenter: workCenter && {
          value: workCenter,
          label: formatWorkCenterLabel(workCenter, mainPlant, workCenterDescription),
        },
        functionalLocation: functionalLocationId && {
          value: functionalLocationId,
          label: formatObjectLabel(functionalLocationId, functionalLocationDescription),
        },
        equipment: equipmentId && {
          value: equipmentId,
          label: formatObjectLabel(equipmentId, equipmentDescription, true),
        },
        plannerGroup: plannerGroup && {
          value: plannerGroup,
          label: formatObjectLabel(plannerGroup, plannerGroupDescription),
        },
        systemCondition: systemCondition && {
          value: systemCondition,
          label: formatObjectLabel(systemCondition, systemConditionText),
        },
        priority: priorityType && {
          value: priorityType.priority,
          label: formatPriorityLabel(priorityType.type, priorityType.priority, priorityType.description),
        },
        personnel: mainPerson && { value: mainPerson, label: formatObjectLabel(mainPerson, personnelName) },
        startDate: dayjsGetDate(`${woStartDate}${woStartTime}`),
        endDate: dayjsGetDate(`${woFinishDate}${woFinishTime}`),
      });

      setState(detailState);
      setDataValid(true);
    } else {
      setDataValid(false);
    }

    setDataReady(true);
  };

  const handleChange = (id: any, data: any) => {
    switch (id) {
      case "cleanSelections":
        {
          setOptions((prevState: any) => ({ ...prevState, [data]: [] }));
        }
        break;
      case "":
      default:
        {
          let nextState: any = { [id]: data },
            nextOptsState: any = {};
          let relatatedOptionDataKey = relatatedOptions[id];
          if (relatatedOptionDataKey && !!data) {
            nextState[relatatedOptionDataKey] = null;
            nextOptsState[relatatedOptionDataKey] = [];
          }
          switch (id) {
            case "workCenter":
              {
                if (data && !state.maintPlant) {
                  nextState.maintPlant = data.maintPlant;
                }
              }
              break;
            default:
              break;
          }
          setState((prevState: any) => ({ ...prevState, ...nextState }));
          setOptions((prevState: any) => ({ ...prevState, ...nextOptsState }));
          setErrors((prevState: any) => ({ ...prevState, [id]: false }));
        }
        break;
    }
  };

  const handleSearch = (id: any) => async (keyword?: any) => {
    if (!keyword && fetchings[id]) return;

    let reqId = genRandomString();
    reqIds.current[id] = reqId;

    await setFetchings((prevState: any) => ({ ...prevState, [id]: true }));
    await setOptions((prevState: any) => ({ ...prevState, [id]: [] }));

    let _options: any = [];

    let params: any = {
      keyword,
      maintenancePlantCodes: convertFilterMultiValue(state.maintPlant ? [state.maintPlant] : [], userPlants),
    };
    let searchServiceApi: any, mappingOptions;
    switch (id) {
      case "activityKey":
        {
          if (state.workOrderType) params.workOrderTypes = [state.workOrderType.value];
          searchServiceApi = MasterDataServices.getActivityKeys;
          mappingOptions = { valueKey: "type" };
        }
        break;
      case "maintPlant":
        {
          params.maintenancePlantCodes = userPlants;
          searchServiceApi = MasterDataServices.getMaintenancePlants;
          mappingOptions = { labelKey: "plantName" };
        }
        break;
      case "workCenter":
        {
          searchServiceApi = MasterDataServices.getWorkCenters;
          mappingOptions = {
            format: (i: any) => ({
              label: formatWorkCenterLabel(i.code, i.maintenancePlantId, i.description),
              costCenter: i.costCenter,
              maintPlant: {
                label: formatObjectLabel(i.maintenancePlantId, i.maintenancePlantName),
                value: i.maintenancePlantId,
              },
            }),
          };
        }
        break;
      case "functionalLocation":
        {
          if (state.plannerGroup) params.plannerGroups = [state.plannerGroup.value];
          searchServiceApi = MasterDataServices.getFunctionalLocations;
          mappingOptions = { valueKey: "functionalLocationId" };
        }
        break;
      case "equipment":
        {
          if (state.functionalLocation) params.functionalLocationCodes = [state.functionalLocation.value];
          if (state.plannerGroup) params.plannerGroups = [state.plannerGroup.value];
          searchServiceApi = EquipmentServices.getEquipments;
          mappingOptions = {
            valueKey: "equipmentId",
            format: (i: any) => ({
              label: formatObjectLabel(i.equipmentId, i.description, true),
              functionalLocation: {
                description: i.functionalLocation.description,
                functionalLocationId: i.functionalLocation.functionalLocationId,
              },
            }),
          };
        }
        break;
      case "plannerGroup":
        {
          searchServiceApi = MasterDataServices.getPlannerGroups;
          mappingOptions = { valueKey: "plannerGroupId", labelKey: "name" };
        }
        break;
      case "systemCondition":
        {
          searchServiceApi = MasterDataServices.getSystemConditions;
          mappingOptions = { labelKey: "text" };
        }
        break;
      case "priority":
        {
          let priorityType = get(state, "workOrderType.priorityTypeId", "");
          params.types = [priorityType];

          searchServiceApi = MasterDataServices.getPriorities;
          mappingOptions = {
            valueKey: "priority",
            format: (i: any) => ({ label: formatPriorityLabel(i.type, i.priority, i.description) }),
          };
        }
        break;
      case "personnel":
        {
          if (state.workCenter) params.workCenterIds = [state.workCenter.value];
          searchServiceApi = MasterDataServices.getPersonnel;
          mappingOptions = { labelKey: "name", format: (i: any) => ({ fullName: i.name }) };
        }
        break;

      default:
        break;
    }

    const { response } = await axiosHandler(() => searchServiceApi(params));
    // console.log(`🚀 Kds: handleSearch -> id, params, response, error`, id, { params, response, error });
    if (!isMounted() || reqId !== reqIds.current[id]) return;
    _options = mappingSelections(response, mappingOptions);

    setFetchings((prevState: any) => ({ ...prevState, [id]: false }));
    setOptions((prevState: any) => ({ ...prevState, [id]: _options }));
  };

  useEffect(() => {
    if (havePermission && state.equipment && state.equipment.value && state.equipment.functionalLocation) {
      handleSelectedEquipment(state.equipment);
    }
  }, [state.equipment]);

  const handleSelectedEquipment = async (equipment: any) => {
    const { functionalLocation } = equipment;

    if (functionalLocation) {
      const { functionalLocationId, description } = functionalLocation;

      handleChange("functionalLocation", {
        value: functionalLocationId,
        label: formatObjectLabel(functionalLocationId, description),
      });
    }
  };

  const handleSearchScanResult = async (qrCode: any) => {
    setScanDataStatus(false);
    await setFetchings((prevState: any) => ({ ...prevState, scan: true }));

    const { response, error } = await axiosHandler(() => MasterDataServices.getByQRCode(qrCode), { delay: true });
    // console.log(`🚀 Kds: handleSearchScanResult -> qrCode, response, error`, qrCode, response, error);

    if (!isMounted()) return;

    await setFetchings((prevState: any) => ({ ...prevState, scan: false }));

    const { equipment, functionalLocation } = get(response, "data", {});

    if (!equipment && !functionalLocation) return setScanError(true);

    switch (true) {
      case !!functionalLocation:
        {
          let functionalLocationSelection = mappingSelections([functionalLocation], {
            valueKey: "functionalLocationId",
          })[0];
          handleChange("functionalLocation", functionalLocationSelection);
        }
        break;
      case !!equipment:
        {
          const { functionalLocation: _functionalLocation, functionalLocationId } = equipment;
          let functionalLocationSelection = _functionalLocation
            ? mappingSelections([_functionalLocation], { valueKey: "functionalLocationId" })[0]
            : { value: functionalLocationId, label: functionalLocationId };
          let equipmentSelection = mappingSelections([equipment], { valueKey: "equipmentId" })[0];
          handleChange("functionalLocation", functionalLocationSelection);
          handleChange("equipment", equipmentSelection);
        }
        break;
      default:
        break;
    }
  };

  const handleOnCTA = async () => {
    const { startDate, endDate, activityKey } = state;

    let isValidData = true,
      _errors: any = {},
      focusInputField: any = null,
      setInvalid = (id: any) => {
        isValidData = false;
        _errors[id] = true;
        if (!focusInputField) focusInputField = id;
      };

    const requiredFields = [
      // section 2
      "fullDescription",
      "workOrderType",
      // section 3
      "maintPlant",
      "workCenter",
      "plannerGroup",
      "priority",
      "startDate",
      "endDate",
      // section 4
      "personnel",
    ];

    if (!!activityKey) {
      requiredFields.push("systemCondition");
    }

    requiredFields.forEach((requiredField: any) => {
      if (!state[requiredField]) setInvalid(requiredField);
    });

    if (!state["equipment"] && !state["functionalLocation"]) {
      setInvalid("equipment");
      setInvalid("functionalLocation");
    }

    if (startDate && endDate && +startDate > +endDate) {
      _errors.endDate = true;
    }
    setErrors(_errors);
    if (isValidData) {
      await setFetchings((prevState: any) => ({ ...prevState, crud: true }));

      let params: WorkOrderChangeParams = {
        workOrderId: dataId,
        workOrderLongText: state.fullDescription,
        maintenanceActivityKey: state.activityKey?.value || "",
        equipmentId: state.equipment?.value || "",
        priority: state.priority?.value || "",
        mainPerson: state.personnel?.value || "",
        mainPlant: state.maintPlant?.value || "",
        workCenter: state.workCenter?.value || "",
        plannerGroup: state.plannerGroup?.value || "",
        systemCondition: state.systemCondition?.value || "",
        workOrderStartDate: dayjsFormatDate(state.startDate, SAP_DATE_FORMAT),
        workOrderStartTime: dayjsFormatDate(state.startDate, SAP_TIME_FORMAT),
        workOrderFinishDate: dayjsFormatDate(state.endDate, SAP_DATE_FORMAT),
        workOrderFinishTime: dayjsFormatDate(state.endDate, SAP_TIME_FORMAT),
      };
      const { response, error } = await axiosHandler(() => WorkOrderServices.changeWorkOrder(params));

      if (!isMounted()) return;
      // console.log(`🚀 Kds: handleOnCTA -> response, error`, response, error);
      await setFetchings((prevState: any) => ({ ...prevState, crud: false }));

      if (error) {
        toast.show(getErrorDetail(error).errorMessage, { type: "danger" });
      } else {
        toast.show(`Chỉnh sửa lệnh bảo trì thành công!`, { type: "success" });

        // @ts-ignore
        (global[`work_order_detail_cb`] || (() => null))();
        changeScreen(props);
      }
    } else {
      toast.show("Vui lòng nhập đầy đủ thông tin!", { type: "danger" });
    }
  };

  const _renderHeader = () => {
    return (
      <CNavbar title={`Chỉnh sửa lệnh bảo trì`} leftIcon={<ICCloseY />} onLeftPress={() => setShowConfirm(true)} />
    );
  };

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isScanData}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setScanDataStatus(false)}
          onResult={handleSearchScanResult}
        />
        <CAlertModal
          open={isScanError}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => setScanError(false)}
        />
      </Fragment>
    );
  };

  const _renderBody = () => {
    return (
      <>
        {(!dataReady && (
          <View style={styles.bodyCenter}>
            <CSpinkit />
          </View>
        )) ||
          (isEdit && !dataValid && <CEmptyState />) || (
            <Fragment>
              <WOCTabInfo
                {...{
                  isEdit,
                  toast,
                  state,
                  errors,
                  fetchings,
                  options,
                }}
                canChangeSupervisor={canChangeSupervisor}
                onChange={handleChange}
                onSearch={handleSearch}
                onScan={() => setScanDataStatus(true)}
              />

              <CTAButtons
                isSticky
                buttons={[
                  {
                    text: i18n.t(`CM.edit`),
                    onPress: handleOnCTA,
                  },
                ]}
              />
            </Fragment>
          )}
      </>
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {havePermission && _renderBody()}
      {_renderScanQRCode()}

      {!!(fetchings.crud || fetchings.scan) && (
        <View style={styles.fetchingDataOverlay}>
          <CSpinkit />
        </View>
      )}
      <CAlertModal
        open={showConfirm}
        title={"Bạn có chắc chắn muốn thoát không?"}
        icon={<ICWarningOctagon />}
        onClose={() => setShowConfirm(false)}
        onCta={() => {
          setShowConfirm(false);
          changeScreen(props);
        }}
      />

      {!havePermission && <CEmptyState noPermission={!havePermission} />}
    </View>
  );
};

export default WorkOrderEdit;

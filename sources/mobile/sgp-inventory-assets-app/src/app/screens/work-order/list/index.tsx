import dayjs from "dayjs";
import { get, isEmpty } from "lodash";
import React, { FC, Fragment, useEffect, useRef, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";

import { EquipmentServices, MasterDataServices, PermissionServices, WorkOrderServices } from "@Services";
import { axiosHandler, getPagination, PAGE_SIZE } from "@Services/httpClient";
import {
  BaseParams,
  CommonStatusParams,
  EquipmentSearchParams,
  FunctionalLocationParams,
  MetricParams,
  PersonnelParams,
  PriorityParams,
  WorkOrderSearchParams,
} from "@Services/parameters";

import { CommonActions } from "src/redux/actions";
import { AppState } from "src/redux/reducers";

import { isArray } from "src/utils/Basic";
import { isCloseToBottom, useIsMounted, useStateLazy } from "src/utils/Core";
import { dayjsFormatDate, dayjsTimePassedFromNow, SAP_DATE_FORMAT } from "src/utils/DateTime";
import {
  convertFilterMultiValue,
  formatObjectLabel,
  formatPriorityLabel,
  formatWorkCenterLabel,
} from "src/utils/Format";
import { changeScreen } from "src/utils/Screen";

import styles from "./styles";

import { Permission, ScreenName, widthResponsive as WR } from "@Constants";

import {
  CAlertModal,
  CEmptyState,
  CFilterModal,
  CMetricSection,
  CNavbar,
  COverlayAddBtn,
  CRefreshControl,
  CScanQRCodeModal,
  CSpinkit,
  SearchInput,
  WOListItem,
} from "@Components";

import { ICGhost, ICQRC } from "@Assets/image";
import i18n from "../../../../i18n";

const FIRST_PAGE_NUMBER = 1;

const WorkOrderList: FC<any> = props => {
  const isMounted = useIsMounted();
  const saInsets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const filterModalRef: any = useRef();

  const [isFetchingList, setFetchingList] = useStateLazy(true);
  const [pagination, setPagination] = useStateLazy(getPagination());
  const [workOrders, setWorkOrders] = useStateLazy([]);
  const [keyword, setKeyword]: any = useState("");

  const [isFetchingMetric, setFetchingMetric] = useStateLazy(false);
  const [metric, setMetric]: any = useState([]);
  const [dates, setDates] = useStateLazy([dayjs().subtract(1, "month").toDate(), new Date()]);

  const reducer: any = useSelector<AppState | null>(s => s?.common);
  const userPlants = PermissionServices.getPlantPermission(reducer);
  const personPermission = PermissionServices.getPersonPermission(reducer);

  const filterOptions = useState([
    {
      id: "maintenancePlace",
      label: "Theo nơi bảo trì",
      onSearch: async (params: any, cb: any) => {
        const param: BaseParams = {
          maintenancePlantCodes: userPlants,
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getMaintenancePlants(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: formatObjectLabel(item.code, item.plantName) };
          })
        );
      },
    },
    {
      id: "workCenter",
      label: "Theo tổ đội",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;
        const param: BaseParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getWorkCenters(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              value: item.code,
              label: formatWorkCenterLabel(item.code, item.maintenancePlantId, item.description),
            };
          })
        );
      },
    },
    {
      id: "workOrderType",
      label: "Theo loại lệnh bảo trì",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;
        const param: BaseParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getWorkOrderTypes(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.type, label: formatObjectLabel(item.type, item.name) };
          })
        );
      },
    },
    {
      id: "equipment",
      label: "Theo thiết bị",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, functionalPlace, equipmentType } = filterSelecteds;
        const param: EquipmentSearchParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          functionalLocationCodes: convertFilterMultiValue(functionalPlace),
          objectTypeCodes: convertFilterMultiValue(equipmentType),
          ...params,
        };
        const { response } = await axiosHandler(() => EquipmentServices.getEquipments(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.equipmentId, label: formatObjectLabel(item.equipmentId, item.description, true) };
          })
        );
      },
    },
    {
      id: "functionalPlace",
      label: "Theo khu vực chức năng",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;
        const param: FunctionalLocationParams = {
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          ...params,
        };
        const { response } = await axiosHandler(() => MasterDataServices.getFunctionalLocations(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return {
              value: item.functionalLocationId,
              label: formatObjectLabel(item.functionalLocationId, item.description),
            };
          })
        );
      },
    },
    {
      id: "status",
      label: "Theo trạng thái",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace } = filterSelecteds;

        const param: CommonStatusParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          codes: ["2 -Work order"], //TODO: remove soon
        };

        const { response } = await axiosHandler(() => MasterDataServices.getCommonStatus(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.status, label: formatObjectLabel(item.status, item.description) };
          })
        );
      },
    },
    {
      id: "priority",
      label: "Theo mức độ ưu tiên",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, type } = filterSelecteds;

        const param: PriorityParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          types: convertFilterMultiValue(type),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPriorities(param));

        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.priority, label: formatPriorityLabel(item.type, item.priority, item.description) };
          })
        );
      },
    },
    {
      id: "responsiblePerson",
      label: "Theo người chịu trách nhiệm",
      onSearch: async (params: any, cb: any) => {
        const { maintenancePlace, workCenter } = filterSelecteds;

        const param: PersonnelParams = {
          ...params,
          maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
          workCenterIds: convertFilterMultiValue(workCenter),
        };
        const { response } = await axiosHandler(() => MasterDataServices.getPersonnel(param));
        cb(
          get(response, "data.pagination", {}),
          get(response, "data.items", []).map((item: any) => {
            return { value: item.code, label: formatObjectLabel(item.code, item.name) };
          })
        );
      },
    },
  ])[0];
  const [filterSelecteds, setFilterSelecteds]: any = useStateLazy({
    fromDate: dates[0],
    toDate: dates[1],
  });

  const [isShowQRScan, setShowQRScan] = useState(false);
  const [isScanErrorAlert, setScanErrorAlert] = useState(false);

  const havePermission = PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.SEARCH);

  useEffect(() => {
    if (havePermission) {
      handleSetDates();
      handleGetList();
    } else {
      setFetchingList(false);
    }
    // @ts-ignore
    global["work_order_list_cb"] = () => handleGetList();
    return () => {
      // @ts-ignore
      delete global["work_order_list_cb"];
    };
  }, [filterSelecteds, keyword]);

  useEffect(() => {
    handleGetMetric();
  }, [dates]);

  // START: handler functions

  const handleSetDates = () => {
    if (dates[0] === filterSelecteds.fromDate && dates[1] === filterSelecteds.toDate) return;
    setDates([filterSelecteds.fromDate, filterSelecteds.toDate]);
  };

  const handleGetMetric = async () => {
    if (!havePermission) return;

    await setFetchingMetric(true);

    let params: MetricParams = {
      mainPlantIds: userPlants,
      fromDate: dayjsFormatDate(dates[0], SAP_DATE_FORMAT),
      toDate: dayjsFormatDate(dates[1], SAP_DATE_FORMAT),
      mainPerson: personPermission,
    };

    const { response } = await axiosHandler(() => WorkOrderServices.getWorkOrderMetric(params), { delay: true });
    if (!isMounted()) return;

    const data = get(response, "data.workOrder.item", []);
    setMetric(data);
    setFetchingMetric(false);
  };

  const handleScanResult = async (result: any) => {
    setShowQRScan(false);

    dispatch(CommonActions.openLoading.request({}));

    const { response } = await axiosHandler(() => MasterDataServices.getByQRCode(result), { delay: true });
    let equipmentId = get(response, ["data", "equipment", "equipmentId"], "");
    let functionalLocationId = get(response, ["data", "functionalLocation", "functionalLocationId"], "");

    dispatch(CommonActions.openLoading.success({}));

    if (!isEmpty(equipmentId)) {
      let equipmentDesc = get(response, ["data", "equipment", "description"], "");
      setFilterSelecteds({
        ...filterSelecteds,
        equipment: { value: equipmentId, label: `${equipmentDesc} (${equipmentId}` },
      });
    } else if (!isEmpty(functionalLocationId)) {
      let functionalLocationDesc = get(response, ["data", "functionalLocation", "description"], "");
      setFilterSelecteds({
        ...filterSelecteds,
        equipment: { value: functionalLocationId, label: `${functionalLocationDesc} (${functionalLocationId}` },
      });
    } else {
      setScanErrorAlert(true);
    }
  };

  const handleGetList = async (page: any = FIRST_PAGE_NUMBER) => {
    if (!havePermission) return;
    const {
      maintenancePlace,
      workCenter,
      equipment,
      workOrderType,
      functionalPlace,
      status,
      priority,
      responsiblePerson,
      fromDate,
      toDate,
    } = filterSelecteds;

    if (page === FIRST_PAGE_NUMBER) {
      await setWorkOrders([]);
      await setPagination(getPagination());
    }

    await setFetchingList(true);

    let params: WorkOrderSearchParams = {
      keyword,
      page: page,
      maintenancePlantCodes: convertFilterMultiValue(maintenancePlace, userPlants),
      workCenterIds: convertFilterMultiValue(workCenter),
      equipmentIds: convertFilterMultiValue(equipment),
      workOrderTypeIds: convertFilterMultiValue(workOrderType),
      functionalLocationIds: convertFilterMultiValue(functionalPlace),
      statusIds: convertFilterMultiValue(status),
      priorityIds: convertFilterMultiValue(priority),
      mainPersonIds: convertFilterMultiValue(responsiblePerson),
      workOrderFinishDateFrom: dayjsFormatDate(fromDate, SAP_DATE_FORMAT),
      workOrderFinishDateTo: dayjsFormatDate(toDate, SAP_DATE_FORMAT),
    };

    const { response } = await axiosHandler(() => WorkOrderServices.workOrderSearch(params, personPermission), {
      delay: true,
    });

    if (!isMounted()) return;

    let data: any = get(response, "data.items", []);

    let limited: any = isArray(data) && data.length > PAGE_SIZE ? data.slice(0, PAGE_SIZE) : data;
    let nextPage: any = [];
    if (isArray(limited, 1)) {
      limited.forEach((item: any) => {
        const {
          workOrderDescription,
          workOrderId,
          equipmentId,
          equipmentDescription,
          mainPlant,
          maintenancePlantDescription,
          personnelName,
          woStartDate,
          woStartTime,
          woFinishDate,
          workOrderTypeDescription,
          priorityText,
          workOrderSystemStatusDescription,
          thumbnailUrl,
          priorityType,
          commonStatus,
        } = item;

        let priorityColor = get(priorityType, "colorCode", "#3A73B7");
        let statusColor = get(commonStatus, "colorCode", "#3A73B7");

        nextPage.push({
          id: workOrderId,
          title: formatObjectLabel(workOrderId, workOrderDescription, true),
          equipment: {
            name: formatObjectLabel(equipmentId, equipmentDescription, true),
            image: thumbnailUrl,
          },
          mainPlant: formatObjectLabel(mainPlant, maintenancePlantDescription, true),
          createdBy: personnelName,
          dateTxt: `${dayjsFormatDate(woStartDate)} - ${dayjsFormatDate(woFinishDate)}`,
          timePassedFromNow: dayjsTimePassedFromNow(`${woStartDate}${woStartTime}`),
          tags: [
            { color: priorityColor, label: priorityText },
            { color: "#3A73B7", label: workOrderTypeDescription },
            { color: statusColor, label: workOrderSystemStatusDescription },
          ].filter((t: any) => !isEmpty(t.label)),
        });
      });
    }

    setPagination(getPagination(get(response, "data.pagination", {})));
    setWorkOrders((curPage: any) => [...curPage, ...nextPage]);
    setFetchingList(false);
  };

  const handleOnSearch = (_keyword: any) => {
    setKeyword(_keyword);
    handleGetList();
  };

  const handleOnReachedEnd = (e: any) => {
    if (!isFetchingList && pagination.hasNext && isCloseToBottom(e)) {
      handleGetList(pagination.nextPage);
    }
  };

  // END: handler functions

  // START: render functions

  const _renderHeader = () => {
    const title =
      pagination.totalItems > 0 ? `${i18n.t("CM.workOrder")} (${pagination.totalItems})` : i18n.t("CM.workOrder");
    return (
      <CNavbar
        title={title}
        onLeftPress={() => changeScreen(props)}
        rightBtnNode={
          havePermission && (
            <CFilterModal
              showToggle
              ref={filterModalRef}
              title="Lọc lệnh bảo trì"
              options={filterOptions}
              selecteds={filterSelecteds}
              onChange={setFilterSelecteds}
            />
          )
        }
      />
    );
  };

  const _renderSearch = () => {
    return (
      havePermission && (
        <View style={styles.searchBar}>
          <SearchInput
            placeholder={"Nhập mã hoặc tiêu đề"}
            wrapperStyle={styles.searchBarInput}
            onTextSearch={handleOnSearch}
          />
          <TouchableOpacity style={styles.searchBarBtn} onPress={() => setShowQRScan(true)}>
            <ICQRC />
          </TouchableOpacity>
        </View>
      )
    );
  };

  const _renderMetric = () => {
    return (
      <CMetricSection
        metric={metric}
        dates={dates}
        fetching={isFetchingMetric}
        countPriorityKey={"workOrderCountPriority"}
        countStatusKey={"workOrderCountStatus"}
        onFilter={(quickSelections: any) => {
          let nextSelecteds = {
            ...filterSelecteds,
            ...quickSelections,
          };
          setFilterSelecteds(nextSelecteds, () => {
            filterModalRef.current.setSelected(nextSelecteds);
            // if (quickSelections.fromDate) handleGetMetric(nextSelecteds);
          });
        }}
      />
    );
  };

  const _renderBody = () => {
    let haveData = isArray(workOrders, 1);
    return (
      <ScrollView
        scrollEventThrottle={50}
        contentContainerStyle={{ paddingBottom: saInsets.bottom + WR(56 / 2 + 16) }}
        refreshControl={
          <CRefreshControl
            onRefresh={() => {
              handleGetMetric();
              handleGetList();
            }}
          />
        }
        onScroll={handleOnReachedEnd}>
        {_renderMetric()}
        {workOrders.map((item: any, index: any) => (
          <WOListItem
            key={item.id + "_" + String(index)}
            {...{ item, index }}
            onPress={() => changeScreen(props, ScreenName.WorkOrderDetail, item)}
          />
        ))}
        {isFetchingList && (
          <View style={styles.dataFetching}>
            <CSpinkit />
          </View>
        )}
        {!isFetchingList && !haveData && <CEmptyState noPermission={!havePermission} />}
      </ScrollView>
    );
  };

  const _renderAddBtn = () =>
    PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.POST) && (
      <COverlayAddBtn onPress={() => changeScreen(props, ScreenName.WorkOrderCreate, { openFrom: "list" })} />
    );

  const _renderScanQRCode = () => {
    return (
      <Fragment>
        <CScanQRCodeModal
          open={isShowQRScan}
          dataName={"Thiết bị hoặc Khu vực chức năng"}
          onClose={() => setShowQRScan(false)}
          onResult={handleScanResult}
        />
        <CAlertModal
          open={isScanErrorAlert}
          title={"Không tìm thấy khu vực hoặc thiết bị này"}
          icon={<ICGhost />}
          onCta={() => {
            setScanErrorAlert(false);
          }}
        />
      </Fragment>
    );
  };

  return (
    <View style={styles.screen}>
      {_renderHeader()}
      {_renderSearch()}
      {_renderBody()}
      {_renderAddBtn()}
      {_renderScanQRCode()}
    </View>
  );

  // END: render functions
};

export default WorkOrderList;

import { StyleSheet } from "react-native";

import { Color, FontSize, widthResponsive as WR } from "@Constants";

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Color.Background,
  },
  searchBar: {
    flexDirection: "row",
    alignItems: "center",
  },
  searchBarInput: {
    marginTop: WR(8),
    marginBottom: WR(16),
    marginLeft: WR(24),
    flex: 1,
  },
  searchBarBtn: {
    width: WR(32),
    height: WR(32),
    marginTop: -WR(6),
    marginHorizontal: WR(16),
    alignItems: "center",
    justifyContent: "center",
  },
  dataFetching: {
    paddingVertical: WR(24),
  },
  dataEmptyTxt: {
    paddingHorizontal: WR(16),
    color: Color.White,
    fontSize: FontSize.FontMedium,
    textAlign: "center",
    paddingTop: WR(16),
    paddingBottom: WR(24),
  },
});

export const month = {
  jan: "01",
  feb: "02",
  mar: "03",
  apr: "04",
  may: "05",
  jun: "06",
  jul: "07",
  aug: "08",
  sep: "09",
  oct: "10",
  nov: "11",
  dec: "12",
};

export const quarter = {
  q1: "1",
  q2: "2",
  q3: "3",
  q4: "4",
};

import i18n from "@I18n";

const InventoryType: Object | any = {
  "01": i18n.t("CM.InventoryType.txtLabeledInventory"),
  "02": i18n.t("CM.InventoryType.txtUnlabelledInventory"),
  "03": i18n.t("CM.InventoryType.txtNotInventory"),
};
export const StatusTypes: Object | any = {
  "1": String(i18n.t("CA.Status01")),
  "2": String(i18n.t("CA.Status02")),
  "3": String(i18n.t("CA.Status03")),
};

export default InventoryType;

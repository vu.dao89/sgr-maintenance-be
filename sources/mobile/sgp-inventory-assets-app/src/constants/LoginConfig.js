// DEVELOP || BETA || STAGING || PRODUCTION (master) || BDEV
const RUN_ENVIRONMENT = "DEVELOP";

let HOST, DB, TENANT_ID, CLIENT_ID, CLIENT_SECRET_AZURE, BASE_URL, REDIRECT_URL, WEBSOCKET_HOST, MINIAPP_APP_HOST;

if (RUN_ENVIRONMENT === "DEVELOP") {
  HOST = "https://devmysgrapi.sungroup.com.vn/";
  DB = "mysgr_beta";
  TENANT_ID = "5be3fff1-bbb6-45dc-9e35-e0ffcb08c1b8";
  CLIENT_ID = "d1749664-ee12-4f57-a9f6-86a82c753489";
  CLIENT_SECRET_AZURE = "v687Q~hGhup3kBg4rqHouAQ2N2vUl1NdAFFjh";
  BASE_URL = "https://devmysgrchat.sungroup.com.vn";
  REDIRECT_URL = "com.sungroup.betamysgr://oauth/redirect/";
  WEBSOCKET_HOST = "wss://devmysgrchat.sungroup.com.vn/";
} else if (RUN_ENVIRONMENT === "BETA") {
  HOST = "https://betamysgrapi.sungroup.com.vn/";
  DB = "mysgr_beta";
  TENANT_ID = "5be3fff1-bbb6-45dc-9e35-e0ffcb08c1b8";
  CLIENT_ID = "d1749664-ee12-4f57-a9f6-86a82c753489";
  CLIENT_SECRET_AZURE = "v687Q~hGhup3kBg4rqHouAQ2N2vUl1NdAFFjh";
  BASE_URL = "https://betamysgrchat.sungroup.com.vn";
  REDIRECT_URL = "com.sungroup.betamysgr://oauth/redirect/";
  WEBSOCKET_HOST = "wss://betamysgrchat.sungroup.com.vn/";
} else if (RUN_ENVIRONMENT === "STAGING") {
  HOST = "https://stagingmysgrapi.sungroup.com.vn/";
  DB = "mysgr_staging";
  TENANT_ID = "24fbb0b2-32f5-492f-8fd9-3a2a7597e431";
  CLIENT_ID = "ab641d83-5c99-4b46-ac13-4a9a837b6b67";
  CLIENT_SECRET_AZURE = "yRT7.iUOL_0J6H0QO613F~Lyowf.1L230X";
  BASE_URL = "https://stagingmysgrchat.sungroup.com.vn";
  WEBSOCKET_HOST = "wss://stagingmysgrchat.sungroup.com.vn/";
  REDIRECT_URL = "com.sungroup.mysgr://oauth/redirect/";
}

const SCOPES = ["email", "openid", "profile", "offline_access"];
const AUTHORIZATION_ENDPOINT = `https://login.microsoftonline.com/${TENANT_ID}/oauth2/v2.0/authorize`;
const TOKEN_ENDPOINT = `https://login.microsoftonline.com/${TENANT_ID}/oauth2/v2.0/token`;

export const CONFIG_AZURE_WEBVIEW = {
  HOST: HOST,
  DB: DB,
  CLIENT_ID: CLIENT_ID,
  END_POINT: `https://login.microsoftonline.com/${TENANT_ID}/oauth2/v2.0`,
  REDIRECT_URL: REDIRECT_URL,
  SCOPES: SCOPES,
  AUTHORIZATION_ENDPOINT: AUTHORIZATION_ENDPOINT,
  TOKEN_ENDPOINT: TOKEN_ENDPOINT,
};

export const configLoginAzure = {
  issuer: CONFIG_AZURE_WEBVIEW.END_POINT,
  clientId: CONFIG_AZURE_WEBVIEW.CLIENT_ID,
  redirectUrl: CONFIG_AZURE_WEBVIEW.REDIRECT_URL,
  scopes: CONFIG_AZURE_WEBVIEW.SCOPES,
  serviceConfiguration: {
    authorizationEndpoint: CONFIG_AZURE_WEBVIEW.AUTHORIZATION_ENDPOINT,
    tokenEndpoint: CONFIG_AZURE_WEBVIEW.TOKEN_ENDPOINT,
  },
};

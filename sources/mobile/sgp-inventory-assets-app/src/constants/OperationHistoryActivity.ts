const OperationHistoryActivity = {
  START: "start", // already started
  HOlD: "hold", // still holding
  COMPLETE: "complete",
  NONE: "none", // not yet started
};
export default OperationHistoryActivity;

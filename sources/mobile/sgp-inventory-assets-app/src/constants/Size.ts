import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Dimensions, Platform, StatusBar } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

const WIDTH_IPHONE_X = 375;
const HEIGHT_IPHONE_X = 812;

// use when Left/Right/Width
export const widthResponsive = (widthSize: number) => {
  return wp((widthSize / WIDTH_IPHONE_X) * 100);
};

// use when Top/Bottom/Height
export const heightResponsive = (heightSize: number) => {
  return hp((heightSize / HEIGHT_IPHONE_X) * 100);
};
export const { height: ScreenHeight, width: ScreenWidth } = Dimensions.get("window");

const FontSize = {
  // Font size
  FontTiniest: heightResponsive(10),
  FontTinier: heightResponsive(11),
  FontTiny: heightResponsive(12),
  FontSmallest: heightResponsive(13),
  FontSmaller: heightResponsive(14),
  FontSmall: heightResponsive(15),
  FontMedium: heightResponsive(16),
  FontMediumPlus: heightResponsive(17),
  FontBig: heightResponsive(18),
  FontBigger: heightResponsive(20),
  FontBigBigger: heightResponsive(22),
  FontBiggest: heightResponsive(24),
  FontHuge: heightResponsive(26),
  FontHuger: heightResponsive(30),
  FontHugest: heightResponsive(32),
  FontGreat: heightResponsive(34),
  FontMidGreater: heightResponsive(36),
  FontGreater: heightResponsive(38),
  FontGreatest: heightResponsive(60),

  //LIST_SIZE
  MaxRenderInList: 10,
};

export const StatusBarTop = () => {
  const F = (number: number | undefined | null) => {
    if (number === undefined || number === null) return 0;
    return number;
  };

  const insets = useSafeAreaInsets();
  return F(Platform.select({ ios: insets.top, android: StatusBar.currentHeight }));
};

export default FontSize;

const SystemStatus = {
  notification: {
    CREATE_NEW: "I0068", // Tạo mới
    DOING: "I0070", // Đang thực hiện
    WO_CREATED: "I0071", // Đã tạo lệnh bảo trì
    COMPLETED: "I0072", // Hoàn thành
  },
  operation: {
    CREATE_NEW: "I0001", //Tạo mới
    READY: "I0002", //Đã ban hành thực hiện
    A_HAFT: "I0010", //Thực hiện 1 phần công việc
    FINISH_OPS: "I0009", //Thực hiện xong công việc
    CLOSED: "I0045", //Đã đóng
    LOCKED: "I0043", //Đã khóa
    PRICED: "I0046", //Đã tính giá thành
    isCompleted: (operationStatusCode: any) => {
      return [
        SystemStatus.operation.FINISH_OPS,
        SystemStatus.operation.CLOSED,
        SystemStatus.operation.LOCKED,
        SystemStatus.operation.PRICED,
      ].includes(operationStatusCode);
    },
    isOpening: (operationStatusCode: any) => {
      return operationStatusCode === SystemStatus.operation.CREATE_NEW;
    },
  },
  workorder: {
    CREATE_NEW: "I0001",
    READY: "I0002",
    A_HAFT: "I0010", //Thực hiện 1 phần công việc
    FINISH_OPS: "I0009", //Thực hiện xong công việc
    CLOSED: "I0045", //Đã đóng
    LOCKED: "I0043", //Đã khóa
    PRICED: "I0046", //Đã tính giá thành
    isCompleted: (workOrderStatus: any) => {
      return [SystemStatus.workorder.CLOSED, SystemStatus.workorder.LOCKED, SystemStatus.workorder.PRICED].includes(
        workOrderStatus
      );
    },
  },
};
export default SystemStatus;

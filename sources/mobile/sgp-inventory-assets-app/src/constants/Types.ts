const Types = {
  // HOME -> LETTER TYPE
  ERROR: {
    E400: 400,
    E404: 404,
    E405: 405,
    E406: 406,
    E500: 500,
  },
  PERMISSION_SAP: {
    A69: "ASSET_SAP_DISPLAY_69",
    A70: "ASSET_SAP_UPDATE_70",
  },
};
export default Types;

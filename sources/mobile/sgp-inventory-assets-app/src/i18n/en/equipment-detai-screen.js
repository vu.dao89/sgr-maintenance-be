export default {
  Title: "Equipment Detail",
  FindEquipment: "Find Equipment",
  BtnCreateAction: "Tạo hành động",
  Tabs: {
    General: "General",
    MeasuringPoint: "Measuring Point",
    Attribute: "Attribute",
    Hierarchical: "Hierarchical",
    Document: "Documents",
  },
  GeneralTabs: {
    TitleInfo: "Equipment Information",
    TitleInfoAdditional: "Equipment Additional Information",
    TitleInfoProducing: "Equipment Producing Information",
  },
  MeasuringPointTabs: {
    ButtonReset: "Reset",
    ButtonGet: "Get",
    Note: "Note",
    EnterValuePoint: "Enter Value",
    RangeUnit: "RangeUnit",
  },
  subject: "Subject",
  insurance: "Insurance",
  atrributes: "Atrributes",
};

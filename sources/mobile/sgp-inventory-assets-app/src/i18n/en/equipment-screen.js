export default {
  Title: "Equipment",
  FindEquipment: "Find Equipment",
  HierarchyTitle: "Hierarchy Diagram",
  HierarchyModalClose: "Close",
  equipment: "Equipment",
  measure: "Measure",
};

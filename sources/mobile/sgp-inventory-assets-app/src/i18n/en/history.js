export default {
  Title: 'Lịch sử',

  Filter: {
    txtAll: 'Tất cả',
    txtCurrentMonth: 'Tháng này',
    txtCurrentQuarter: 'Quý này',
    txtCurrentYear: 'Năm nay',
  },

  Tabs: {
    InventoryAsset: 'Kiểm kê',
    UpdateInformation: 'Cập nhật thông tin',
  },

  Month: 'T{{month}}',
  Quarter: 'Q{{quarter}}',
  SelectQuarter: 'Theo Quý',
  SelectMonth: 'Theo Tháng',
  SelectYear: 'Theo Năm',
  ResetFilter: 'Xem toàn thời gian',
  CurrentQuarter: 'Xem quý này',
  CurrentMonth: 'Xem tháng này',
  CurrentYear: 'Xem năm nay',
  SelectTime: 'Chọn thời gian',

  Amount: 'Số lượng',
  Localtion: 'Vị trí',
  Status: 'Trạng thái',
  CurrentPhoto: 'Ảnh hiện trạng',

  ListChange: 'Nội dung thay đổi',
  MoreChange: '+{{count}} mục khác',

  FormatQuarter: 'Quý {{quarter}}/{{year}}',
  FormatMonth: 'Tháng {{month}}/{{year}}',
  FormatYear: 'Năm {{year}}',
  HistoryNotFound: 'Chưa có dữ liệu',

  DetailHistoryChange: 'Chi tiết cập nhật thông tin',
  DetailAssetInventory: 'Chi tiết kiểm kê',
};

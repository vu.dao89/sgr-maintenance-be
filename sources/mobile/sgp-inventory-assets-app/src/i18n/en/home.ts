export default {
  Title: "Quản Lý Tài Sản",
  CreateNewAsset: "Tạo mới tài sản",
  AssetName: "Tên tài sản",
  AssetUser: "Người sử dụng",
  AssetGroupUser: "Bộ phận sử dụng",
  AssetGroup: "Nhóm tài sản",
  FilterSetting: "Thiết lập bộ lọc",

  phrAssetGroupUser: "Chọn bộ phận",
  phrAssetUser: "Chọn người",
  phrAssetGroup: "Chọn nhóm",

  ViewResults: "Xem kết quả",

  txtNotFound: "Không tìm thấy tài sản này.\nBạn có muốn tạo mới tài sản?",
  txtNotFoundWithoutPermission: "Không tìm thấy tài sản này.",
  txtQRNotFound: "Không tìm thấy tài sản này.",
  txtQRCreateNew: "Bạn có muốn tạo mới tài sản?",
  txtNew: "Tạo mới",

  txtQRScanDescription: "Quét mã QR để xem thông tin tài sản",
  txtSelectImg: "Chọn từ thư viện ảnh",
  txtInvalidQr: "Ảnh mã QR không hợp lệ",
  txtSelectAnother: "Vui lòng thử lại hoặc chọn ảnh khác.",
  txtTryAgain: "Thử lại",
  txtError: "Lỗi",
  txtRequestPermission: "Không thể mở camera, vui lòng cấp quyền và thử lại",
  txtGoToSetting: "Mở cài đặt",
  txtBack: "Trở lại",
  txtClose: "Đóng",
  txtRequestPermissionLibrary: "Không thể mở thư viện ảnh, vui lòng cấp quyền và thử lại",
};

import { NativeModules, Platform } from "react-native";
import i18n from "i18next";

import * as enTranslation from "./en";
import * as viTranslation from "./vi";

const getLanguageCode = () => {
  let locale = "vi";
  // if (Platform.OS === "android") {
  //   locale = NativeModules.I18nManager.localeIdentifier;
  // } else {
  //   locale = NativeModules.SettingsManager.settings.AppleLocale;
  // }

  // if (!locale) {
  //   locale = NativeModules.SettingsManager.settings.AppleLanguages[0];
  // }

  return locale.replace("_", "-");
};

const languageDetector: any = {
  type: "languageDetector",
  detect: getLanguageCode,
  init: () => {
    // init
  },
  cacheUserLanguage: () => {
    // init
  },
};

i18n.use(languageDetector).init({
  compatibilityJSON: "v3",
  // lng: "vi",
  resources: {
    en: {
      translation: enTranslation,
    },
    vi: {
      translation: viTranslation,
    },
  },
  fallbackLng: "en",
  debug: true,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;

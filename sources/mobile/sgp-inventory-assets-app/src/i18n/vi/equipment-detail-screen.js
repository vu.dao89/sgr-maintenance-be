export default {
  Title: "Chi tiết thiết bị",
  FindEquipment: "Tìm thiết bị",
  BtnCreateAction: "Tạo hành động",
  Tabs: {
    General: "Tổng quan",
    MeasuringPoint: "Điểm đo",
    Attribute: "Đặc tính",
    Hierarchical: "Phân cấp",
    Document: "Tài liệu",
  },
  GeneralTabs: {
    TitleInfo: "Thông tin thiết bị",
    TitleInfoAdditional: "Thông tin bổ sung",
    TitleInfoProducing: "Thông tin sản xuất",
  },
  MeasuringPointTabs: {
    ButtonReset: "Đặt lại",
    ButtonGet: "Tạo",
    Note: "Ghi chú",
    EnterValuePoint: "Nhập giá trị đo",
    RangeUnit: "Đơn vị đo",
  },
  subject: "Đối tượng",
  insurance: "Bảo hành",
  atrributes: "Đặc tính",
};

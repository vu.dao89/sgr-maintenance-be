import { useState } from "react";

// export const useCreateObject = () => {
//   const [object, setObject] = useState<Object | null>(null);
//   const realm = useRealm();
//   // const records = useQuery(name); for validate

//   const createObject = function (name: string, values: Object): any {
//     console.log("\nCreating object");
//     try {
//       return realm.write(() => {
//         const model = models[name];
//         const obj = realm.create(name, new model(values));
//         setObject(obj);

//         return obj;
//       });
//     } catch (err) {
//       console.log(err);
//     }
//   };

//   return { object, createObject };
// };

export const useUpdateObject = () => {
  const [object, setObject] = useState(null);

  // @TODO
  const updateObject = function (name: string, values: any): any {
    return;
  };

  return { object, updateObject };
};

export const useDeleteObject = () => {
  const [object, setObject] = useState(null);

  // @TODO
  const deleteObject = function (name: string, id: string): any {
    return;
  };

  return { object, deleteObject };
};

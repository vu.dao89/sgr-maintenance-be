import { SchemaName } from "@Constants";
import { Field } from "../utils/interfaces";

const optionString: Field = {
  type: "string",
  required: false,
};

const belongToEquipment: Field = {
  type: "reference",
  relation: "belongTo",
  target: SchemaName.Equipment,
};

const belongToNotification: Field = {
  type: "reference",
  relation: "belongTo",
  target: SchemaName.Notification,
};

export { optionString, belongToEquipment, belongToNotification };

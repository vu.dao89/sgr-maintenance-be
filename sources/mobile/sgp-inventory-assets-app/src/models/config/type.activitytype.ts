import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const activityType: Schema = {
  name: SchemaName.ActivityType,
  key: "",
  fields: {
    costCenter: optionString,
    costCenterDesc: optionString,
    activityType: optionString,
    activityTypeDesc: optionString,
  },
};

export default activityType;

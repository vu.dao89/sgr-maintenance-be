import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToEquipment, optionString } from "./field.common";

const attachment: Schema = {
  name: SchemaName.Attachment,
  key: "documentId",
  fields: {
    equipment: belongToEquipment,
    relationshipId: optionString,
    objectKey: optionString,
  },
};

export default attachment;

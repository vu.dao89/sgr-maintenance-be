import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToEquipment, optionString } from "./field.common";

const characteristic: Schema = {
  name: SchemaName.Characteristic,
  key: "charId",
  fields: {
    equipment: belongToEquipment,
    objClassFlag: optionString,
    internCounter: optionString,
    classType: optionString,
    charValCounter: optionString,
    valueRel: optionString,
    charvalDesc: optionString,
    classId: optionString,
  },
};

export default characteristic;

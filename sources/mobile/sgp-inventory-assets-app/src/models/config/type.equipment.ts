import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const equipment: Schema = {
  name: SchemaName.Equipment,
  key: "equipId",
  fields: {
    equipId: {
      type: "string",
      required: true,
      computed: true,
    },
    constYear: optionString,
    constMonth: optionString,
    maintPlant: optionString,
    pmObjectType: optionString,
    funcLocIdIntern: optionString,
    equipCategory: optionString,
    equipDesc: optionString,
    equipType: optionString,
    location: optionString,
    maintWorkCenter: optionString,
    manufCountry: optionString,
    manufPartNo: optionString,
    manufSerialNo: optionString,
    manufacturer: optionString,
    modelNum: optionString,
    plannerGroup: optionString,
    planningPlant: optionString,
    plantSection: optionString,
    size: optionString,
    weight: optionString,
    costCenter: optionString,
    superiorEquip: optionString,
    createDate: optionString,
    warrantyEnd: optionString,
    warrantyDate: optionString,
  },
};

export default equipment;

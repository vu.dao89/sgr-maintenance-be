import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const equipmentCategory: Schema = {
  name: SchemaName.EquipmentCategory,
  key: "",
  fields: {
    equiCat: optionString,
    equiCatDesc: optionString,
  },
};

export default equipmentCategory;

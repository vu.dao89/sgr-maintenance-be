import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const groupCode: Schema = {
  name: SchemaName.GroupCode,
  key: "",
  fields: {
    catalogProfile: optionString,
    catalogProfileDesc: optionString,
    catalog: optionString,
    catalogDesc: optionString,
    codeGroup: optionString,
    codeGroupDesc: optionString,
    code: optionString,
    codeDesc: optionString,
  },
};

export default groupCode;

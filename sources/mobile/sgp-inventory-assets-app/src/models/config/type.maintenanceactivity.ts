import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const maintenanceActivity: Schema = {
  name: SchemaName.MaintenanceActivity,
  key: "",
  fields: {
    workOrderType: optionString,
    maintenanceActivityType: optionString,
    maintenanceActivityTypeDesc: optionString,
  },
};

export default maintenanceActivity;

import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const maintenancePlant: Schema = {
  name: SchemaName.MaintenancePlant,
  key: "",
  fields: {
    plant: optionString,
    plantDesc: optionString,
    plantPlanning: optionString,
  },
};

export default maintenancePlant;

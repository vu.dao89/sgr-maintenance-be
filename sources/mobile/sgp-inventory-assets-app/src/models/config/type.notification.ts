import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToEquipment, optionString } from "./field.common";

const notification: Schema = {
  name: SchemaName.Notification,
  key: "notificationId",
  fields: {
    notificationType: optionString,
    notificationDesc: optionString,
    notificationLongText: optionString,
    priorityType: optionString,
    priority: optionString,
    notificationCreatedOn: optionString,
    equipment: belongToEquipment,
    equipmentId: optionString,
    functionalLocation: optionString,
    notificationDate: optionString,
    notificationTime: optionString,
    notificationReportedBy: optionString,
    requestStart: optionString,
    requestStartTime: optionString,
    requestEnd: optionString,
    requestEndTime: optionString,
    malfunctionStart: optionString,
    malfunctionStartTime: optionString,
    malfunctionEnd: optionString,
    malfunctionEndTime: optionString,
    breakDown: optionString,
    breakDownDuration: optionString,
    breakDownDurationUnit: optionString,
    order: optionString,
    notificationCompletionDate: optionString,
    notificationCompletionTime: optionString,
    catalogProfile: optionString,
    plannerGroup: optionString,
    sortField: optionString,
    workCenter: optionString,
    maintenancePlant: optionString,
    statusId: optionString,
  },
};

export default notification;

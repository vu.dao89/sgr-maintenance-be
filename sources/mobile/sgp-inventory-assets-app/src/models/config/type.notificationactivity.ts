import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToNotification, optionString } from "./field.common";

const notificationActivity: Schema = {
  name: SchemaName.NotificationActivity,
  key: "notificationActivityId",
  fields: {
    notification: belongToNotification,
    notificationActivityGroup: optionString,
    notificationActivityCode: optionString,
  },
};

export default notificationActivity;

import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToNotification, optionString } from "./field.common";

const notificationItem: Schema = {
  name: SchemaName.NotificationItem,
  key: "notificationItemNo",
  fields: {
    notification: belongToNotification,
    notificationPartCode: optionString,
    notificationCodeGroupProblem: optionString,
    notificationDamageCode: optionString,
  },
};

export default notificationItem;

import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const notificationType: Schema = {
  name: SchemaName.NotificationType,
  key: "",
  fields: {
    notiType: optionString,
    notiTypeDesc: optionString,
    catalogProfile: optionString,
    problems: optionString,
    causes: optionString,
    activities: optionString,
    objectParts: optionString,
    workOrderType: optionString,
    priorityType: optionString,
  },
};

export default notificationType;

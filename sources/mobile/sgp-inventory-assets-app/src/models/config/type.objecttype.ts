import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const objectType: Schema = {
  name: SchemaName.ObjectType,
  key: "",
  fields: {
    objType: optionString,
    objTypeDesc: optionString,
  },
};

export default objectType;

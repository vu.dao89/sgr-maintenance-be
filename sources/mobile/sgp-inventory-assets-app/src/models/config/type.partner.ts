import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToEquipment, optionString } from "./field.common";

const partner: Schema = {
  name: SchemaName.Partner,
  key: "partnerNum",
  fields: {
    equipment: belongToEquipment,
    counter: optionString,
    partnerFunction: optionString,
    objectType: optionString,
  },
};

export default partner;

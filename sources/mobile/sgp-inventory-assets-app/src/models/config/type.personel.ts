import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const personel: Schema = {
  name: SchemaName.Personel,
  key: "",
  fields: {
    personel: optionString,
    perStartDate: optionString,
    perEndDate: optionString,
    personelName: optionString,
    workCenter: optionString,
  },
};

export default personel;

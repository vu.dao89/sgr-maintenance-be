import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const storageLocation: Schema = {
  name: SchemaName.StorageLocation,
  key: "",
  fields: {
    plant: optionString,
    sloc: optionString,
    slocDesc: optionString,
  },
};

export default storageLocation;

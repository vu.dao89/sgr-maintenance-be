import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const systemCondition: Schema = {
  name: SchemaName.SystemCondition,
  key: "",
  fields: {
    systemCondition: optionString,
    systemConditionText: optionString,
  },
};

export default systemCondition;

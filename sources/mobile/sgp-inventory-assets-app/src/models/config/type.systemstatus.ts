import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { belongToEquipment, optionString } from "./field.common";

const systemStatus: Schema = {
  name: SchemaName.SystemStatus,
  key: "statusId",
  fields: {
    equipment: belongToEquipment,
    statusCode: optionString,
    statusDescription: optionString,
    userStatusProfile: optionString,
    userStatus: optionString,
  },
};

export default systemStatus;

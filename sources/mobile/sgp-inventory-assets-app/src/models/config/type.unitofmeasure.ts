import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const unitOfMeasure: Schema = {
  name: SchemaName.UnitOfMeasure,
  key: "",
  fields: {
    uom: optionString,
    uomDesc: optionString,
  },
};

export default unitOfMeasure;

import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const workCenter: Schema = {
  name: SchemaName.WorkCenter,
  key: "",
  fields: {
    workCenter: optionString,
    plant: optionString,
    workCenterCategory: optionString,
    workCenterDesc: optionString,
  },
};

export default workCenter;

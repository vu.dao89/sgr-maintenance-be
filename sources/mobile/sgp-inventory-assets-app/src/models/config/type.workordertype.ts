import { SchemaName } from "@Constants";
import { Schema } from "../utils/interfaces";
import { optionString } from "./field.common";

const workOrderType: Schema = {
  name: SchemaName.WorkOrderType,
  key: "",
  fields: {
    plant: optionString,
    workOrderType: optionString,
    workOrderTypeDesc: optionString,
  },
};

export default workOrderType;

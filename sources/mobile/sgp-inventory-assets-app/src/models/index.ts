import { useDeleteObject, useUpdateObject } from "./actions";
import { models, schemas } from "./types";

export { schemas, models, useDeleteObject, useUpdateObject };

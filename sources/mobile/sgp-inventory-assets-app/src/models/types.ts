import ActivityType from "./config/type.activitytype";
import Attactment from "./config/type.attactment";
import Characteristic from "./config/type.characteristic";
import Equipment from "./config/type.equipment";
import EquipmentCategory from "./config/type.equipmentcategory";
import GroupCode from "./config/type.groupcode";
import MaintenanceActivity from "./config/type.maintenanceactivity";
import MaintenancePlant from "./config/type.maintenanceplant";
import NotificationType from "./config/type.notificationtype";
import ObjectType from "./config/type.objecttype";
import Partner from "./config/type.partner";
import Personel from "./config/type.personel";
import StorageLocation from "./config/type.storagelocation";
import SystemCondition from "./config/type.systemcondition";
import SystemStatus from "./config/type.systemstatus";
import UnitOfMeasure from "./config/type.unitofmeasure";
import WorkCenter from "./config/type.workcenter";
import WorkOrderType from "./config/type.workordertype";

import { Models, Schemas } from "./utils/interfaces";
import { createModel } from "./utils/realm";

const schemas: Schemas = {
  Equipment,
  Characteristic,
  Attactment,
  Partner,
  SystemStatus,
  ActivityType,
  EquipmentCategory,
  GroupCode,
  MaintenanceActivity,
  MaintenancePlant,
  NotificationType,
  ObjectType,
  Personel,
  StorageLocation,
  SystemCondition,
  UnitOfMeasure,
  WorkCenter,
  WorkOrderType,
};

const models: Models = {};
for (const key in schemas) {
  models[schemas[key].name] = createModel(schemas[key]);
}

export { schemas, models };

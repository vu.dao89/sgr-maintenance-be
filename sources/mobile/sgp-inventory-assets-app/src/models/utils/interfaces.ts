import { ObjectClass } from "realm";

export interface Field {
  type: string | null | undefined;
  relation?: string | null | undefined;
  target?: string | null | undefined;
  required?: boolean | undefined;
  computed?: boolean | undefined;
}

export interface Fields {
  [key: string]: Field;
}

export interface Schema {
  name: string;
  key: string;
  fields: Fields;
}

export interface Schemas {
  [key: string]: Schema;
}

export interface Object {
  id?: any;
  [key: string]: any;
}

export interface Model extends ObjectClass {
  new (object: Object): Object;
}

export interface Models {
  [key: string]: Model;
}

import { get, isEmpty, set } from "lodash";
import { schemas } from "../types";
import { Object } from "./interfaces";
import { scalaTypes } from "./realm";

export const objectToModel = (name: string, data: Object, context = {}) => {
  const schema = schemas[name];
  const object = {};

  if (!isEmpty(schema.key)) {
    const key = tranformKey(schema.key);
    set(schema, key, tranformValue(get(data, key)));
  }

  for (const key in schema.fields) {
    const tKey = tranformKey(key);
    const schemaType = String(schema.fields[key].type);

    if (!scalaTypes.includes(schemaType)) continue;

    if (schemaType == "reference") {
      const targetPath = get(schema, `fields.${key}.target`);
      if (!targetPath) continue;

      const refKey = tranformKey(schemas[targetPath].key);
      const refs = get(context, targetPath);
      if (!refs) continue;

      const refId = get(refs, data[refKey]);
      set(object, `${key}_id`, refId);
      set(object, schemas[targetPath].key, tranformValue(data[refKey]));

      continue;
    }

    set(object, key, tranformValue(get(data, tKey)));
  }

  return object;
};

const tranformKey = (field: any) => String(field).toUpperCase();
const tranformValue = (value: any) => String(value);

export const exportToXml = () => null;

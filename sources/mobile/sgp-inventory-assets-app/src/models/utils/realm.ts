import { get, set } from "lodash";
import { Model, Schema } from "./interfaces";

// @TODO Add more support scala type here.
export const scalaTypes = ["string", "reference"];

export const createModel = (schema: Schema) => {
  let properties = {
    _id: "objectId",
    _createdAt: "date",
    _updatedAt: "date",
  };

  for (const field in schema.fields) {
    const schemaType = String(schema.fields[field].type);
    if (!scalaTypes.includes(schemaType)) continue;

    switch (schemaType) {
      case "reference":
        // Only support belongTo relation at this time.
        if (schema.fields[field].relation !== "belongTo") {
          break;
        }

        properties = { ...properties, [`${field}_id`]: "string" };

        const targetSchema = get(schema, schema.fields[field].target ?? "_");
        if (targetSchema && targetSchema.key) {
          properties = { ...properties, [targetSchema.key]: "string" };
        }

        break;

      default:
        properties = { ...properties, [field]: schemaType };
        break;
    }
  }

  const model: Model = class {
    _createdAt: Date;
    _updatedAt: Date | undefined;
    _id: any;

    constructor({ id = new Realm.BSON.ObjectId(), ...fields }) {
      for (const field in fields) {
        // @TODO check specifice field
        set(this, field, fields[field]);
      }

      this._createdAt = new Date();
      this._updatedAt = new Date();
      this._id = id;
    }

    // To use a class as a Realm object type, define the object schema on the static property "schema".
    static schema = {
      name: schema.name,
      primaryKey: "_id",
      properties,
    };
  };

  return model;
};

// export const createModelsContext = (models: Models) => {
//   const schemas: (ObjectClass | ObjectSchema)[] = [];
//   for (const model in models) {
//     schemas.push(models[model].schema);
//   }

//   return createRealmContext({
//     schema: schemas,
//     deleteRealmIfMigrationNeeded: true,
//   });
// };

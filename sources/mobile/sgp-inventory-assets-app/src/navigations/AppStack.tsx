import { ScreenName } from "@Constants";
import { createStackNavigator } from "@react-navigation/stack";
import { LoginScreen } from "@Screens";
import React from "react";

const Stack = createStackNavigator();

export const AppStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={ScreenName.Login}
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={ScreenName.Login} component={LoginScreen} />
    </Stack.Navigator>
  );
};

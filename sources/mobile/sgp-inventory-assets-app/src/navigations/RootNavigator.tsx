// External
import { DarkTheme, NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { default as dayjs } from "dayjs";
import { get, isEmpty } from "lodash";
import * as React from "react";
import { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// Internal
import { CommonActions } from "@Actions";
import { LoadingGlobal } from "@Components";
import { Color, ScreenName } from "@Constants";
import {
  CreateAssetScreen,
  DetailAssetsScreen,
  EquipmentDetailScreen,
  EquipmentScreen,
  HistoryScreen,
  HomeScreen,
  InventoryAssetScreen,
  InventoryPictureScreen,
  MaintenanceHomeScreen,
  NotificationCreateScreen,
  NotificationDetailScreen,
  NotificationEditScreen,
  NotificationListScreen,
  OperationCreateScreen,
  OperationDetailScreen,
  OperationListScreen,
  QrScan,
  SubOprationDetailScreen,
  TeamplateScreen,
  UpdateAssetScreen,
  WorkOrderCreateScreen,
  WorkOrderDetailScreen,
  WorkOrderEditScreen,
  WorkOrderListScreen,
} from "@Screens";
import { CacheUtil } from "@Utils";
import { View } from "react-native";
import { AppState } from "../redux/reducers";
import { AppStack } from "./AppStack";

const Stack = createStackNavigator();

function RootNavigator() {
  const dispatch = useDispatch();
  const commonState: any = useSelector((state: AppState) => state.common);
  const [status, setStatus] = useState("loading");

  const getTokenCache = async () => {
    const token_exp = (await CacheUtil.getTokenExpDate()) + ""; // token_exp is not empty when running on single miniapp

    if (isEmpty(token_exp) || dayjs(token_exp).isAfter(new Date())) {
      const token_cache = (await CacheUtil.getToken()) + "";
      dispatch(CommonActions.setTokenAsyncAction(token_cache));
      setStatus(isEmpty(token_cache) ? "login" : "token");
    } else {
      setStatus("login");
    }
  };

  useEffect(() => {
    getTokenCache();
  });

  if (status == "loading") {
    return (
      <NavigationContainer theme={DarkTheme}>
        <Fragment>
          <View style={{ flex: 1, backgroundColor: Color.Background }}></View>
        </Fragment>
      </NavigationContainer>
    );
  }

  if (status == "login") {
    return (
      <NavigationContainer theme={DarkTheme}>
        <AppStack />
      </NavigationContainer>
    );
  }

  const inventory = get(commonState, "inventory");

  return (
    <NavigationContainer theme={DarkTheme}>
      <Stack.Navigator
        initialRouteName={ScreenName.Login}
        screenOptions={{
          headerShown: false,
        }}>
        {/** Maintenance Screens */}
        {[
          { name: ScreenName.MaintenanceHomeScreen, comp: MaintenanceHomeScreen },
          { name: ScreenName.Equipment, comp: EquipmentScreen },
          { name: ScreenName.EquipmentDetail, comp: EquipmentDetailScreen },
          { name: ScreenName.NotificationList, comp: NotificationListScreen },
          { name: ScreenName.NotificationCreate, comp: NotificationCreateScreen },
          { name: ScreenName.NotificationDetail, comp: NotificationDetailScreen },
          { name: ScreenName.NotificationEdit, comp: NotificationEditScreen },
          { name: ScreenName.WorkOrderList, comp: WorkOrderListScreen },
          { name: ScreenName.WorkOrderCreate, comp: WorkOrderCreateScreen },
          { name: ScreenName.WorkOrderDetail, comp: WorkOrderDetailScreen },
          { name: ScreenName.WorkOrderEdit, comp: WorkOrderEditScreen },
          { name: ScreenName.OperationList, comp: OperationListScreen },
          { name: ScreenName.OperationDetail, comp: OperationDetailScreen },
          { name: ScreenName.OperationCreate, comp: OperationCreateScreen },
          { name: ScreenName.SubOprationDetail, comp: SubOprationDetailScreen },
        ].map((s: any) => {
          return <Stack.Screen key={s.name} name={s.name} component={s.comp} />;
        })}
        {/** Inventory Asset Screens */}
        {[
          { name: ScreenName.Home, comp: HomeScreen },
          { name: ScreenName.History, comp: HistoryScreen },
          { name: ScreenName.CreateAsset, comp: CreateAssetScreen },
          { name: ScreenName.Template, comp: TeamplateScreen },
          { name: ScreenName.DetailAssets, comp: DetailAssetsScreen },
          { name: ScreenName.InventoryPicture, comp: InventoryPictureScreen },
          { name: ScreenName.QrScan, comp: QrScan },
          { name: ScreenName.UpdateAsset, comp: UpdateAssetScreen },
          { name: ScreenName.InventoryAsset, comp: InventoryAssetScreen },
        ].map((s: any) => {
          return <Stack.Screen key={s.name} name={s.name} component={s.comp} />;
        })}
      </Stack.Navigator>
      <LoadingGlobal />
    </NavigationContainer>
  );
}

export default RootNavigator;

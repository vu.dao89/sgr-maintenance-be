import { createAction, createAsyncAction } from "typesafe-actions";

export const setTokenAsyncAction = createAction("SET_TOKEN_AZURE")<string>();

export const setInitialPropsAction = createAction("SET_INITIAL_PROPS")<any>();

export const getUserInfoAsyncAction = createAsyncAction(
  "DO_GET_USER_INFO_REQUEST",
  "GET_USER_INFO_SUCCESS",
  "GET_USER_INFO_FAILURE"
)<any, any, any>();

export const getDepartmentOfUserAsyncAction = createAsyncAction(
  "DO_GET_DEPARTMENT_OF_USER_REQUEST",
  "GET_DEPARTMENT_OF_USER_SUCCESS",
  "GET_DEPARTMENT_OF_USER_FAILURE"
)<any, any, any>();

export const openLoading = createAsyncAction("DO_LOADING_REQUEST", "GET_LOADING_SUCCESS", "GET_LOADING_FAILURE")<
  any,
  any,
  any
>();

export const getPermissionAsyncAction = createAsyncAction(
  "DO_GET_PERMISSION_REQUEST",
  "GET_PERMISSION_SUCCESS",
  "GET_PERMISSION_FAILURE"
)<any, any, any>();

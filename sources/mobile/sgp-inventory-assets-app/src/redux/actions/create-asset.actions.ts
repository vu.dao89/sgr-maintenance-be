import { createAsyncAction } from "typesafe-actions";

export const getInvoClassifyAsyncAction = createAsyncAction(
  "DO_GET_INVO_CLASSIFY_REQUEST",
  "GET_INVO_CLASSIFY_SUCCESS",
  "GET_INVO_CLASSIFY_FAILURE"
)<any, any, any>();

export const getStatusAsyncAction = createAsyncAction(
  "DO_GET_STATUS_REQUEST",
  "GET_STATUS_SUCCESS",
  "GET_STATUS_FAILURE"
)<any, any, any>();

export const getUnitAsyncAction = createAsyncAction("DO_GET_UNIT_REQUEST", "GET_UNIT_SUCCESS", "GET_UNIT_FAILURE")<
  any,
  any,
  any
>();

export const getAssetGroupAsyncAction = createAsyncAction(
  "DO_GET_ASSET_GROUP_REQUEST",
  "GET_ASSET_GROUP_SUCCESS",
  "GET_ASSET_GROUP_FAILURE"
)<any, any, any>();

import { createAsyncAction } from "typesafe-actions";

export const getDetailAssetAction = createAsyncAction(
  "DO_GET_DETAIL_ASSET_REQUEST",
  "GET_DETAIL_ASSET_SUCCESS",
  "GET_DETAIL_ASSET_FAILURE"
)<any, any, any>();

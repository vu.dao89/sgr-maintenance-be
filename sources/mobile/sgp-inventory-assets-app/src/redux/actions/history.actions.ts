import { createAsyncAction } from "typesafe-actions";

export const getAssetHistoryInventoryAsyncAction = createAsyncAction(
  "GET_ASSET_HISTORY_INVENTORY_REQUEST",
  "GET_ASSET_HISTORY_INVENTORY_SUCCESS",
  "GET_ASSET_HISTORY_INVENTORY_FAILURE"
)<any, any, any>();

export const getAssetHistoryChangeAsyncAction = createAsyncAction(
  "GET_ASSET_HISTORY_CHANGE_REQUEST",
  "GET_ASSET_HISTORY_CHANGE_SUCCESS",
  "GET_ASSET_HISTORY_CHANGE_FAILURE"
)<any, any, any>();

export const getFilterBothTabsAsyncAction = createAsyncAction(
  "GET_FILTER_BOTH_TABS_REQUEST",
  "GET_FILTER_BOTH_TABS_SUCCESS",
  "GET_FILTER_BOTH_TABS_FAILURE"
)<any, any, any>();

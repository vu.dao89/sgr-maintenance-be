import { createAsyncAction } from "typesafe-actions";

export const getListAssetsAsyncAction = createAsyncAction(
  "DO_GET_LIST_ASSETS_REQUEST",
  "GET_LIST_ASSETS_SUCCESS",
  "GET_LIST_ASSETS_FAILURE"
)<any, any, any>();

export const getListOrganizationAsyncAction = createAsyncAction(
  "DO_GET_LIST_ORGANIZATION_REQUEST",
  "GET_LIST_ORGANIZATION_SUCCESS",
  "GET_LIST_ORGANIZATION_FAILURE"
)<any, any, any>();

export const getListUserAsyncAction = createAsyncAction(
  "DO_GET_LIST_USER_REQUEST",
  "GET_LIST_USER_SUCCESS",
  "GET_LIST_USER_FAILURE"
)<any, any, any>();

export const getListAssetGroupAsyncAction = createAsyncAction(
  "DO_GET_LIST_ASSET_GROUP_REQUEST",
  "GET_LIST_ASSET_GROUP_SUCCESS",
  "GET_LIST_ASSET_GROUP_FAILURE"
)<any, any, any>();

import { createAsyncAction, createAction } from "typesafe-actions";

export const getInfoTemplateAsyncAction = createAsyncAction(
  "DO_GET_INFO_TEMPLATE_REQUEST",
  "GET_INFO_TEMPLATE_SUCCESS",
  "GET_INFO_TEMPLATE_FAILURE"
)<any, any, any>();

export const setShowTemplateAsyncAction = createAction("SET_SHOW_TEMPLATE")();

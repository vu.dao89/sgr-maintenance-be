import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { rootReducer } from "./reducers";
import rootSaga from "./sagas";

const configureStore = () => {
  // Create the saga middleware
  const sagaMiddleware = createSagaMiddleware();

  const reduxMiddleware = applyMiddleware(sagaMiddleware);

  // Mount it on the Store
  const store = createStore(rootReducer, reduxMiddleware);
  // Then run the saga
  sagaMiddleware.run(rootSaga);

  return store;
};
export default configureStore();

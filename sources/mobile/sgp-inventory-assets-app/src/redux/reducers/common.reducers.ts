// External
import { flattenDeep, get, isEmpty } from "lodash";
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

// Internal
import { CommonActions, DetailAssetsAction, HistoryActions, HomeActions, TemplateActions } from "@Actions";
import http from "@Services/httpClient";
import { Permission } from "../../constants";

// type state public
export type CommonState = {
  loading: boolean;
  token: string;
  userinfo: any;
  permission: any;
  initialProps: any;
  data: any;
};

export const initialCommonState: CommonState = {
  loading: false,
  token: "string",
  userinfo: {},
  permission: {},
  initialProps: {},
  data: {},
};

export const CommonReducer = combineReducers({
  initialProps: createReducer<any>({ offlineMode: true }).handleAction(
    [CommonActions.setInitialPropsAction],
    (_state: any, action: any) => {
      return action?.payload;
    }
  ),
  token: createReducer<string>("").handleAction([CommonActions.setTokenAsyncAction], (_state: any, action: any) => {
    const accessToken = action?.payload;
    http.setAuthorizationHeader(accessToken);
    return isEmpty(accessToken) ? "" : accessToken;
  }),
  loading: createReducer(false as boolean)
    .handleAction(
      [
        HistoryActions.getFilterBothTabsAsyncAction.request,
        TemplateActions.getInfoTemplateAsyncAction.request,
        HomeActions.getListAssetsAsyncAction.request,
        DetailAssetsAction.getDetailAssetAction.request,
        CommonActions.openLoading.request,
        CommonActions.getPermissionAsyncAction.request,
      ],
      (_state: any, action: any) => {
        const isNoLoading = get(action, "payload.isNoLoading", false);

        return !isNoLoading;
      }
    )
    .handleAction(
      [
        HistoryActions.getFilterBothTabsAsyncAction.success,
        HistoryActions.getFilterBothTabsAsyncAction.failure,
        TemplateActions.getInfoTemplateAsyncAction.success,
        TemplateActions.getInfoTemplateAsyncAction.failure,
        HomeActions.getListAssetsAsyncAction.success,
        HomeActions.getListAssetsAsyncAction.failure,
        CommonActions.openLoading.success,
        CommonActions.openLoading.failure,
        CommonActions.getPermissionAsyncAction.success,
        CommonActions.getPermissionAsyncAction.failure,
        DetailAssetsAction.getDetailAssetAction.success,
        DetailAssetsAction.getDetailAssetAction.failure,
      ],
      (_state: any, action: any) => {
        const callback = action?.payload?.callback;
        if (typeof callback === "function") {
          setTimeout(() => {
            callback();
          }, 200);
        }

        return false;
      }
    ),
  permission: createReducer<any>({}).handleAction(
    [CommonActions.getPermissionAsyncAction.success],
    (_state: any, _action: any) => {
      const departmentRoles = get(_action.payload, "departmentRoles", []);
      let flattenPermission: any = [];

      departmentRoles?.forEach((dep: any) => {
        dep?.roles?.forEach((role: any) => {
          role?.permissions?.forEach((permission: any) => {
            if (permission.code && permission.appCode.includes("MAINTENANCE")) {
              !flattenPermission.includes(permission.code) && flattenPermission.push(permission.code);
            }
          });
        });
      });

      const plant = flattenPermission
        .filter((c: string) => c.startsWith("MTN_PLANT_"))
        .map((code: any) => code.split("_").pop());

      const employeeCode = get(_action.payload, "employeeCode", "");
      return {
        personPermission: flattenPermission.includes(Permission.MTN_ALL_PERSON) ? "" : employeeCode,
        employeeCode: employeeCode,
        mail: get(_action.payload, "email", ""),
        flattenPermission: flattenPermission,
        userPlants: plant,
      };
    }
  ),
  userInfo: createReducer<CommonState>(initialCommonState)
    .handleAction([CommonActions.getDepartmentOfUserAsyncAction.success], (state: CommonState, action: any) => {
      const { objDepartment, departmentRoles, departmentCode, ...payload } = action.payload;
      // Parse list permission
      const listPermission = departmentRoles.map((departmentRole: any) => {
        return departmentRole.roles.map((role: any) => {
          return role.permissions.map((permission: any) => {
            return permission.code;
          });
        });
      });
      const flattenPermission = flattenDeep(listPermission);

      return {
        ...state,
        data: {
          ...payload,
          objDepartment: {
            id: objDepartment?.costcenter,
            label: objDepartment?.costcenter_name,
          },
          role: flattenPermission,
        },
      };
    })
    .handleAction(
      [CommonActions.getDepartmentOfUserAsyncAction.failure, CommonActions.getUserInfoAsyncAction.failure],
      (_state: CommonState, _: any) => {
        return _state;
      }
    ),
});

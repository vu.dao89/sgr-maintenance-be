import { CreateAssetsAction } from "@Actions";
import { isArray } from "lodash";
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

export type CreateAssetState = {
  loading: boolean;
  data: DataCreateAssetState;
};

type DataCreateAssetState = {};

const initialCreateAssetState: DataCreateAssetState = {};

export const CreateAssetReducer = combineReducers({
  loading: createReducer(false as boolean)
    .handleAction([CreateAssetsAction.getUnitAsyncAction.request], (_: any, _action: any) => true)
    .handleAction(
      [CreateAssetsAction.getUnitAsyncAction.success, CreateAssetsAction.getUnitAsyncAction.failure],
      (_: any, _action: any) => false
    ),
  units: createReducer<DataCreateAssetState>(initialCreateAssetState)
    .handleAction([CreateAssetsAction.getUnitAsyncAction.success], (state: DataCreateAssetState, action: any) => {
      const { data_msehi } = action.payload;

      const data_msehi_convert =
        data_msehi &&
        isArray(data_msehi) &&
        data_msehi.map((e: any) => ({
          id: e?.unit || "",
          label: e?.unit_description || "",
        }));

      return {
        ...state,
        data: data_msehi_convert,
      };
    })
    .handleAction([CreateAssetsAction.getUnitAsyncAction.failure], (_state: DataCreateAssetState, _: any) => {
      return _state;
    }),

  assetGroups: createReducer<DataCreateAssetState>(initialCreateAssetState)
    .handleAction([CreateAssetsAction.getAssetGroupAsyncAction.success], (state: DataCreateAssetState, action: any) => {
      const { data_ord4x } = action.payload;

      const data_ord4x_convert =
        data_ord4x &&
        isArray(data_ord4x) &&
        data_ord4x.map((e: any) => ({
          id: e?.asset_group || "",
          label: e?.asset_groupname || "",
        }));

      return {
        ...state,
        data: data_ord4x_convert,
      };
    })
    .handleAction([CreateAssetsAction.getUnitAsyncAction.failure], (_state: DataCreateAssetState, _: any) => {
      return _state;
    }),
});

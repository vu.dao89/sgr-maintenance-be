// External
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

// Internal
import { DetailAssetsAction } from "@Actions";

// type state public
export type DetailAssetsState = {
  // loading: boolean
  data: object;
};

export const DetailAssetsReducer = combineReducers({
  data: createReducer({} as object)
    .handleAction([DetailAssetsAction.getDetailAssetAction.request], (_state: object, _action: any) => {
      return {};
    })
    .handleAction([DetailAssetsAction.getDetailAssetAction.success], (_state: object, action: any) => {
      return action?.payload?.data || {};
    })
    .handleAction([DetailAssetsAction.getDetailAssetAction.failure], (_state: object, _action: any) => {
      return {};
    }),
});

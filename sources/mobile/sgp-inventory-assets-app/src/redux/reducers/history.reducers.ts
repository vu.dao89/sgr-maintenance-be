// External
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

// Internal
import { HistoryActions, HomeActions } from "@Actions";
import { get } from "lodash";

type DataListHistoryType = {
  data: any[];
  isLoading: boolean;
  totalPage: number;
  code: string;
};
export const initDataListHistory = {
  data: [],
  isLoading: false,
  totalPage: 0,
  code: "",
};
// type state public
export type HistoryState = {
  isRefresh: boolean;
  historyMainData: {
    historyChange: DataListHistoryType;
    historyInventory: DataListHistoryType;
  };
};

const initHistoryMainData = {
  historyChange: initDataListHistory,
  historyInventory: initDataListHistory,
};

export const HistoryReducer = combineReducers({
  historyMainData: createReducer(initHistoryMainData)
    .handleAction([HistoryActions.getFilterBothTabsAsyncAction.request], (_state: any, _action: any) => {
      return {
        historyChange: initDataListHistory,
        historyInventory: initDataListHistory,
      };
    })
    .handleAction([HistoryActions.getFilterBothTabsAsyncAction.success], (state: any, _action: any) => {
      const { response1, response2 } = _action.payload;
      const data1: DataListHistoryType = {
        data: response1.data,
        code: response1.code,
        isLoading: false,
        totalPage: response1.page_total,
      };
      const data2: DataListHistoryType = {
        data: response2.data,
        code: response2.code,
        isLoading: false,
        totalPage: response2.page_total,
      };

      return {
        ...state,
        historyChange: data2,
        historyInventory: data1,
      };
    })
    .handleAction([HistoryActions.getAssetHistoryChangeAsyncAction.request], (state: any, _action: any) => {
      return {
        ...state,
        historyChange: {
          ...state.historyChange,
          isLoading: true,
        },
      };
    })

    .handleAction([HistoryActions.getAssetHistoryInventoryAsyncAction.request], (state: any, _action: any) => {
      return {
        ...state,
        historyInventory: {
          ...state.historyInventory,
          isLoading: true,
        },
      };
    })
    .handleAction([HistoryActions.getAssetHistoryChangeAsyncAction.success], (state: any, _action: any) => {
      const { response, params } = _action.payload;
      const data1: DataListHistoryType = {
        data:
          params.page_index === 1
            ? response.data
            : response.data
            ? [...state.historyChange.data, ...response.data]
            : response.data,
        code: response.code,
        isLoading: false,
        totalPage: response.page_total,
      };

      return {
        ...state,
        historyChange: data1,
      };
    })
    .handleAction([HistoryActions.getAssetHistoryInventoryAsyncAction.success], (state: any, _action: any) => {
      const { response, params } = _action.payload;
      const data1: DataListHistoryType = {
        data:
          params.page_index === 1
            ? response.data
            : response.data
            ? [...state.historyInventory.data, ...response.data]
            : response.data,
        code: response.code,
        isLoading: false,
        totalPage: response.page_total,
      };

      return {
        ...state,
        historyInventory: data1,
      };
    })
    .handleAction([HistoryActions.getAssetHistoryInventoryAsyncAction.failure], (state: any, _action: any) => {
      const { response } = _action.payload;
      const data1: DataListHistoryType = {
        data: response.data ? response.data : [],
        code: response.code,
        isLoading: false,
        totalPage: response.page_total,
      };

      return {
        ...state,
        historyInventory: data1,
      };
    })
    .handleAction([HistoryActions.getAssetHistoryChangeAsyncAction.failure], (state: any, _action: any) => {
      const { response } = _action.payload;
      const data1: DataListHistoryType = {
        data: response.data ? response.data : [],
        code: response.code,
        isLoading: false,
        totalPage: response.page_total,
      };

      return {
        ...state,
        historyChange: data1,
      };
    }),
});

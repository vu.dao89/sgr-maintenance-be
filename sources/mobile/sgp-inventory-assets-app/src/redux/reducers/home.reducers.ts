// External
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

// Internal
import { HomeActions } from "@Actions";
import { get } from "lodash";

type FilterType = {
  organizationData: any[];
  userData: any[];
  groupData: any[];
  units: any[];
};

const initFilterData = {
  organizationData: [],
  userData: [],
  groupData: [],
  units: [],
};
// type state public
export type HomeState = {
  isRefresh: boolean;
  assetList: any[];
  totalPage: number;
  filterData: FilterType;
};

export const HomeReducer = combineReducers({
  isRefresh: createReducer(false)
    .handleAction([HomeActions.getListAssetsAsyncAction.request], (_state: any, _action: any) => {
      return true;
    })
    .handleAction(
      [HomeActions.getListAssetsAsyncAction.success, HomeActions.getListAssetsAsyncAction.failure],
      (_state: any, _action: any) => {
        return false;
      }
    ),
  assetList: createReducer([] as any).handleAction(
    [HomeActions.getListAssetsAsyncAction.success],
    (state: any, action: any) => {
      const data = get(action.payload, "data", []);
      const pageIndex = get(action.payload, "page", 1);
      const finalData = pageIndex === 1 ? data : [...state, ...data];
      return finalData;
    }
  ),
  totalPage: createReducer(0).handleAction(
    [HomeActions.getListAssetsAsyncAction.success],
    (_state: any, action: any) => {
      const total_page = get(action.payload, "totalPage", 0);
      return total_page;
    }
  ),
  filterData: createReducer(initFilterData)
    .handleAction(
      [HomeActions.getListOrganizationAsyncAction.request, HomeActions.getListUserAsyncAction.request],

      (state: any, _action: any) => {
        return {
          ...state,
        };
      }
    )
    .handleAction(
      [
        HomeActions.getListOrganizationAsyncAction.success,
        HomeActions.getListOrganizationAsyncAction.failure,
        HomeActions.getListUserAsyncAction.success,
        HomeActions.getListUserAsyncAction.failure,
      ],
      (state: any, _action: any) => {
        return {
          ...state,
        };
      }
    )

    .handleAction([HomeActions.getListOrganizationAsyncAction.success], (state: any, action: any) => {
      const result = action.payload.map((item: any) => {
        return {
          ...item,
          label: item?.name,
          // id: item?.id,
        };
      });
      return {
        ...state,
        organizationData: result,
      };
    })
    .handleAction([HomeActions.getListOrganizationAsyncAction.failure], (state: any, _action: any) => {
      return {
        ...state,
      };
    })
    .handleAction([HomeActions.getListUserAsyncAction.success], (state: any, action: any) => {
      const data = action.payload.data.map((item: any) => {
        return {
          ...item,
          label: item?.name,
          _id: item?.id,
          id: item?.employeeId,
        };
      });

      const currentData = get(state, "userData", []);

      const pageIndex = get(action.payload, "page", 0);
      const finalData = pageIndex === 0 ? data : [...currentData, ...data];
      return {
        ...state,
        userData: finalData,
      };
    })
    .handleAction([HomeActions.getListUserAsyncAction.failure], (state: any, action: any) => {
      return {
        ...state,
      };
    })
    .handleAction([HomeActions.getListAssetGroupAsyncAction.success], (state: any, action: any) => {
      const { data_ord4x, data_msehi } = action.payload;

      const data_ord4x_convert = data_ord4x.map((item: any) => {
        return {
          ...item,
          label: item?.asset_groupname,
          id: item?.asset_group,
        };
      });
      const data_msehi_convert = data_msehi.map((item: any) => {
        return {
          ...item,
          label: item?.unit_description,
          id: item?.unit,
        };
      });
      return {
        ...state,
        groupData: data_ord4x_convert,
        units: data_msehi_convert,
      };
    })
    .handleAction([HomeActions.getListAssetGroupAsyncAction.failure], (state: any, _action: any) => {
      return {
        ...state,
      };
    }),
});

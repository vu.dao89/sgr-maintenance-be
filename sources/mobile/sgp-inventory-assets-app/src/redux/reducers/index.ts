import { combineReducers } from "redux";

import { CommonReducer, CommonState } from "./common.reducers";
import { TemplateReducer, TemplateState } from "./template.reducers";

import { CreateAssetReducer, CreateAssetState } from "./create-asset.reducers";
import { DetailAssetsReducer, DetailAssetsState } from "./detail-assets.reducers";
import { HistoryReducer, HistoryState } from "./history.reducers";
import { HomeReducer, HomeState } from "./home.reducers";

export interface AppState {
  template: TemplateState;
  common: CommonState;
  detailAssets: DetailAssetsState;
  home: HomeState;
  createAsset: CreateAssetState;
  history: HistoryState;
}

const appReducer = combineReducers({
  template: TemplateReducer,
  common: CommonReducer,
  detailAssets: DetailAssetsReducer,
  home: HomeReducer,
  createAsset: CreateAssetReducer,
  history: HistoryReducer,
});

export const rootReducer = (state: any, action: any) => {
  if (action.type === "LOG_OUT") {
    delete state?.template;
    return appReducer(state, action);
  }
  return appReducer(state, action);
};

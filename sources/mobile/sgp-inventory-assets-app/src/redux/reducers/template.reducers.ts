// External
import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";

// Internal
import { TemplateActions } from "@Actions";

// type state public
export type TemplateState = {
  loading: boolean;
  dataTemplate: DataTemplateState;
};

type DataTemplateState = {
  data: object;
  number: number;
};

const initialTemplateState: DataTemplateState = {
  data: {},
  number: 0,
};

export const TemplateReducer = combineReducers({
  loading: createReducer(false as boolean)
    .handleAction([TemplateActions.getInfoTemplateAsyncAction.request], (_state: any, _action: any) => true)
    .handleAction(
      [TemplateActions.getInfoTemplateAsyncAction.success, TemplateActions.getInfoTemplateAsyncAction.failure],
      (_state: any, _action: any) => false
    ),
  dataTemplate: createReducer<DataTemplateState>(initialTemplateState)
    .handleAction([TemplateActions.getInfoTemplateAsyncAction.success], (state: DataTemplateState, action: any) => {
      return {
        ...state,
        data: action.payload,
      };
    })
    .handleAction([TemplateActions.getInfoTemplateAsyncAction.failure], (_state: DataTemplateState, _action: any) => {
      return _state;
    }),
});

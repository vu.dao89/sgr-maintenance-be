import { all, call, put, takeLatest } from "redux-saga/effects";

import { CommonActions } from "@Actions";
import { CommonServices } from "@Services";
import { logError } from "libs/react-native-x-framework/js/logger";
import UserServices from "../../services/user.services";

export function* saga() {
  yield all([
    takeLatest(CommonActions.getUserInfoAsyncAction.request, getUserInfoSaga),
    takeLatest(CommonActions.getDepartmentOfUserAsyncAction.request, getDepartmentOfUserSaga),
    takeLatest(CommonActions.getPermissionAsyncAction.request, getPermissionSaga),
  ]);
}

function* getPermissionSaga(_action: ReturnType<typeof CommonActions.getPermissionAsyncAction.request>): Generator {
  const { onSuccess, onFailure } = _action.payload;
  try {
    const response: any = yield call(UserServices.getUserInfo);
    yield put(
      CommonActions.getPermissionAsyncAction.success({
        ...response?.data,
      })
    );
    onSuccess && onSuccess({ ...response?.data });
  } catch (error) {
    logError("getPermissionSaga", error);
    yield put(CommonActions.getPermissionAsyncAction.failure(error));
    onFailure && onFailure();
  }
}

function* getUserInfoSaga(_action: ReturnType<typeof CommonActions.getUserInfoAsyncAction.request>): Generator {
  const { onSuccess, onFailure } = _action.payload;

  try {
    const response: any = yield call(CommonServices.getUserInfo);
    yield put(
      CommonActions.getDepartmentOfUserAsyncAction.request({
        params: response?.data,
        onSuccess,
        onFailure,
      })
    );
  } catch (error) {
    yield put(CommonActions.getUserInfoAsyncAction.failure(error));
  }
}

function* getDepartmentOfUserSaga(
  action: ReturnType<typeof CommonActions.getDepartmentOfUserAsyncAction.request>
): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const { employeeId = "" } = params;
    const response: any = yield call(CommonServices.getDeparmentOfUser, employeeId);
    yield put(
      CommonActions.getDepartmentOfUserAsyncAction.success({
        ...params,
        objDepartment: response?.data?.data,
      })
    );
    onSuccess && onSuccess({ objDepartment: response?.data?.data });
  } catch (error) {
    yield put(CommonActions.getDepartmentOfUserAsyncAction.failure(error));
    onFailure && onFailure();
  }
}

import { all, call, put, takeLatest } from "redux-saga/effects";

import { CreateAssetsAction } from "@Actions";
import { CreateAssetServices } from "@Services";

export function* saga() {
  yield all([
    takeLatest(CreateAssetsAction.getUnitAsyncAction.request, getUnitSaga),
    takeLatest(CreateAssetsAction.getAssetGroupAsyncAction.request, getAssetGroupSaga),
  ]);
}

function* getUnitSaga(action: ReturnType<typeof CreateAssetsAction.getUnitAsyncAction.request>): Generator {
  try {
    const response: any = yield call(CreateAssetServices.getUnitOrAssetGroup, action.payload);

    yield put(CreateAssetsAction.getUnitAsyncAction.success(response.data));
  } catch (error) {
    yield put(CreateAssetsAction.getUnitAsyncAction.failure(error));
  }
}

function* getAssetGroupSaga(action: ReturnType<typeof CreateAssetsAction.getAssetGroupAsyncAction.request>): Generator {
  try {
    const response: any = yield call(CreateAssetServices.getUnitOrAssetGroup, action.payload);

    yield put(CreateAssetsAction.getAssetGroupAsyncAction.success(response.data));
  } catch (error) {
    yield put(CreateAssetsAction.getAssetGroupAsyncAction.failure(error));
  }
}

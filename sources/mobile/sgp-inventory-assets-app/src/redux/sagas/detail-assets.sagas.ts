// External
import { all, call, put, takeEvery } from "redux-saga/effects";

// Internal
import { DetailAssetsAction } from "@Actions";
import { DetailAssetsServices } from "@Services";

export function* saga() {
  yield all([takeEvery(DetailAssetsAction.getDetailAssetAction.request, getDetailAssetSaga)]);
}

function* getDetailAssetSaga(action: ReturnType<typeof DetailAssetsAction.getDetailAssetAction.request>): Generator {
  try {
    const response: any = yield call(DetailAssetsServices.getDetailAsset, action.payload);

    // dispatch success
    yield put(DetailAssetsAction.getDetailAssetAction.success(response.data));
  } catch (error) {
    // dispatch failure
    yield put(DetailAssetsAction.getDetailAssetAction.failure(error));
  }
}

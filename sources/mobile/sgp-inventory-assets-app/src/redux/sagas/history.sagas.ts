import { all, call, put, takeEvery } from "redux-saga/effects";

import { HistoryActions } from "@Actions";
import { HistoryServices } from "@Services";

export function* saga() {
  yield all([
    takeEvery(HistoryActions.getFilterBothTabsAsyncAction.request, filterBothTabsSaga),
    takeEvery(HistoryActions.getAssetHistoryChangeAsyncAction.request, getHistoryChangeSaga),
    takeEvery(HistoryActions.getAssetHistoryInventoryAsyncAction.request, getInventoryChangeSaga),
  ]);
}

function* filterBothTabsSaga(
  action: ReturnType<typeof HistoryActions.getFilterBothTabsAsyncAction.request>
): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const response1: any = yield call(HistoryServices.getInventoryAssetHistory, params);
    const response2: any = yield call(HistoryServices.getHistoryChange, params);
    // dispatch success
    yield put(
      HistoryActions.getFilterBothTabsAsyncAction.success({
        response1: response1.data,
        response2: response2.data,
      })
    );
    onSuccess && onSuccess(response1.data);
  } catch (error) {
    // dispatch failure
    yield put(HistoryActions.getFilterBothTabsAsyncAction.failure(error));
    onFailure && onFailure(error);
  }
}
function* getHistoryChangeSaga(
  action: ReturnType<typeof HistoryActions.getAssetHistoryChangeAsyncAction.request>
): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const response1: any = yield call(HistoryServices.getHistoryChange, params);

    yield put(
      HistoryActions.getAssetHistoryChangeAsyncAction.success({
        response: response1.data,
        params,
      })
    );
    onSuccess && onSuccess(response1.data);
  } catch (error) {
    // dispatch failure
    yield put(HistoryActions.getAssetHistoryChangeAsyncAction.failure(error));
    onFailure && onFailure(error);
  }
}
function* getInventoryChangeSaga(
  action: ReturnType<typeof HistoryActions.getAssetHistoryInventoryAsyncAction.request>
): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const response1: any = yield call(HistoryServices.getInventoryAssetHistory, params);

    yield put(
      HistoryActions.getAssetHistoryInventoryAsyncAction.success({
        response: response1.data,
        params,
      })
    );
    onSuccess && onSuccess(response1.data);
  } catch (error) {
    // dispatch failure
    yield put(HistoryActions.getAssetHistoryInventoryAsyncAction.failure(error));
    onFailure && onFailure(error);
  }
}

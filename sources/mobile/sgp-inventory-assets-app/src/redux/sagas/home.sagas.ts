import { all, call, put, takeEvery } from "redux-saga/effects";

import { HomeActions } from "@Actions";
import { HomeService } from "@Services";

export function* saga() {
  yield all([
    takeEvery(HomeActions.getListUserAsyncAction.request, getListUserSagas),
    takeEvery(HomeActions.getListAssetsAsyncAction.request, getListAssetsSagas),
    takeEvery(HomeActions.getListAssetGroupAsyncAction.request, getListGroupSagas),
    takeEvery(HomeActions.getListOrganizationAsyncAction.request, getListOrganizationSagas),
  ]);
}

function* getListAssetsSagas(action: ReturnType<typeof HomeActions.getListAssetsAsyncAction.request>): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const response: any = yield call(HomeService.postSearchAssets, params);

    // dispatch success
    yield put(
      HomeActions.getListAssetsAsyncAction.success({
        data: response?.data?.data,
        page: params.page,
        totalPage: response?.data?.page_total,
      })
    );
    onSuccess && onSuccess(response.data);
  } catch (error) {
    // dispatch failure
    yield put(HomeActions.getListAssetsAsyncAction.failure(error));
    onFailure && onFailure(error);
  }
}

function* getListOrganizationSagas(
  action: ReturnType<typeof HomeActions.getListOrganizationAsyncAction.request>
): Generator {
  try {
    const response: any = yield call(HomeService.getOrganizationInfomation, action.payload);

    // dispatch success
    yield put(HomeActions.getListOrganizationAsyncAction.success(response?.data?.data));
  } catch (error) {
    // dispatch failure
    yield put(HomeActions.getListOrganizationAsyncAction.failure(error));
  }
}
function* getListUserSagas(action: ReturnType<typeof HomeActions.getListUserAsyncAction.request>): Generator {
  const { params, onSuccess, onFailure } = action.payload;
  try {
    const response: any = yield call(HomeService.getEmployees, params);

    // dispatch success
    yield put(
      HomeActions.getListUserAsyncAction.success({
        data: response?.data?.data,
        page: params.page,
      })
    );
    onSuccess && onSuccess(response?.data?.data);
  } catch (error) {
    // dispatch failure
    yield put(HomeActions.getListUserAsyncAction.failure(error));
    onFailure && onFailure();
  }
}

function* getListGroupSagas(action: ReturnType<typeof HomeActions.getListAssetGroupAsyncAction.request>): Generator {
  const { onSuccess, onFailure, params } = action.payload;
  try {
    const response: any = yield call(HomeService.postAssetGroup, params);
    // dispatch success
    yield put(
      HomeActions.getListAssetGroupAsyncAction.success({
        data_ord4x: response?.data?.data_ord4x,
        data_msehi: response?.data?.data_msehi,
      })
    );
    onSuccess && onSuccess(response.data.data);
  } catch (error) {
    // dispatch failure
    yield put(HomeActions.getListAssetGroupAsyncAction.failure(error));
    onFailure && onFailure(error);
  }
}

import { all, call } from "redux-saga/effects";

import { saga as commonSaga } from "./common.sagas";
import { saga as createAssetsSaga } from "./create-asset.sagas";
import { saga as detailAssetsSaga } from "./detail-assets.sagas";
import { saga as historySaga } from "./history.sagas";
import { saga as homeSaga } from "./home.sagas";
import { saga as templateSagas } from "./template.sagas";

export default function* rootSaga(): Generator {
  yield all([
    call(templateSagas),
    call(detailAssetsSaga),
    call(createAssetsSaga),
    call(homeSaga),
    call(commonSaga),
    call(historySaga),
  ]);
}

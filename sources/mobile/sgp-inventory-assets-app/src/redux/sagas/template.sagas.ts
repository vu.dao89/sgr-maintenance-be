import { all, call, put, takeEvery } from "redux-saga/effects";

import { TemplateActions } from "@Actions";
import { TemplateServices } from "@Services";

export function* saga() {
  yield all([takeEvery(TemplateActions.getInfoTemplateAsyncAction.request, getTemplateSaga)]);
}

function* getTemplateSaga(action: ReturnType<typeof TemplateActions.getInfoTemplateAsyncAction.request>): Generator {
  try {
    const response: any = yield call(TemplateServices.getTemplates, action.payload);

    // dispatch success
    yield put(TemplateActions.getInfoTemplateAsyncAction.success(response.data.payload));
  } catch (error) {
    // dispatch failure
    yield put(TemplateActions.getInfoTemplateAsyncAction.failure(error));
  }
}

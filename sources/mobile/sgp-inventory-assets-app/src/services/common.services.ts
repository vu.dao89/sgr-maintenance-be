import { get } from "lodash";
import { CacheUtil } from "../utils";
import HttpClient, { AXIOS_AUTH_TOKEN_URL } from "./httpClient";

const BASE_URL = "/iss/inventoryasset";

export default class CommonServices {
  // [GET] /cos/authorization/users/user-info | Get user info
  static getUserInfo() {
    return HttpClient.post(`/cos/authorization/users/user-info/ASSET`, {});
  }

  // [POST] iss/inventoryasset/asset-get-costcenter | Get department of user
  static getDeparmentOfUser(employeeid: string) {
    return HttpClient.post(`${BASE_URL}/asset-get-costcenter`, { employeeid });
  }

  // [GET] cos/content/download/image1xxx | Get download image
  static downloadImage(imageId: string) {
    return HttpClient.put(`/cos/content/download/${imageId}`);
  }

  static async uploadImage(params: any) {
    try {
      const token_cache = (await CacheUtil.getToken()) + "";
      const uri = get(params, "body.file.src", "");
      const name = get(params, "name", "");
      const type = get(params, "body.file.type", "");
      const form_data = new FormData();
      form_data.append("file", {
        uri,
        name,
        type,
      });

      const res = await fetch(AXIOS_AUTH_TOKEN_URL + `/cos/content/global/storage/upload?name=${params?.name}`, {
        method: "PUT",
        headers: {
          "content-type": "multipart/form-data",
          "X-Authorization": `Bearer ${token_cache}`,
          "x-ms-blob-type": "BlockBlob",
        },
        body: form_data,
      }).then((responseUp: any) => {
        return responseUp;
      });
      return { ...res, imageid: params?.name, uriLocal: uri };
    } catch (error) {
      if (__DEV__) {
        console.log("Error UploadGiftCard:", error);
      }
      const uri = get(params, "body.file.src", "");
      return { imageid: params?.name, uriLocal: uri, status: 500 }; // exception status
    }
  }
}

import HttpClient from "./httpClient";

const BASE_URL = "/iss/inventoryasset";

export default class CreateAssetServices {
  // [POST] /iss/inventoryasset/asset-modify | Create new asset
  static createNewAsset(body: any) {
    return HttpClient.post(`${BASE_URL}/asset-modify`, body);
  }

  // [POST] iss/inventoryasset/asset-list | Get unit or asset group
  static getUnitOrAssetGroup(body: any) {
    return HttpClient.post(`${BASE_URL}/asset-list`, body);
  }
}

import HttpClient from "./httpClient";

const BASE_URL = "/iss/inventoryasset/asset-detail";

export default class DetailAssetsServices {
  // [GET] /api/detail-assets | get detail assets ...
  static getDetailAsset(qrcode: string) {
    return HttpClient.post(BASE_URL, {
      qrcode,
    });
  }

  // [GET] /api/asset-inventory | get detail assets ...
  static postInventoryAsset(body: any) {
    return HttpClient.post("/iss/inventoryasset/asset-inventory", body);
  }
}

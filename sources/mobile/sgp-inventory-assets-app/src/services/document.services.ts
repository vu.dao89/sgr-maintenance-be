import { CFile } from "../app/components/common/attach-files";
import HttpClient from "./httpClient";

const DOCUMENT_PATH = "/iss/inventoryasset";

export default class DocumentServices {
  static uploadDocuments(files: Array<CFile>) {
    let body = new FormData();

    files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(DOCUMENT_PATH + "/global-document/uploads", body);
  }

  static uploadNotificationDocuments(notificationId: string, files: Array<CFile>) {
    let body = new FormData();
    body.append("notificationId", notificationId);

    files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(DOCUMENT_PATH + "/notification-document/uploads", body);
  }

  static uploadOperationDocuments(workOrderId: string, operationId: string, files: Array<CFile>) {
    return DocumentServices.uploadSubOperationDocuments(workOrderId, operationId, "", files);
  }

  static uploadSubOperationDocuments(
    workOrderId: string,
    operationId: string,
    superOperationId: string,
    files: Array<CFile>
  ) {
    let body = new FormData();
    body.append("workOrderId", workOrderId);
    body.append("operationId", operationId);
    body.append("superOperationId", superOperationId);

    files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(DOCUMENT_PATH + "/operation-document/uploads", body);
  }

  static uploadEquipmentDocuments(equipmentId: string, files: Array<CFile>) {
    let body = new FormData();
    body.append("equipmentId", equipmentId);

    files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(DOCUMENT_PATH + "/equipment-document/uploads", body);
  }
}

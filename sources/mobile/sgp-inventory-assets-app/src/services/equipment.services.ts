import { toString } from "../utils/Format";
import HttpClient from "./httpClient";
import { convertBaseParamsToRequest, EquipmentSearchParams, MeasPointUpdateParams } from "./parameters";

const EQUIPMENT_PATH = "/iss/inventoryasset";

export default class EquipmentServices {
  static getEquipments(param: EquipmentSearchParams) {
    const body = {
      functionalLocationCodes: toString(param.functionalLocationCodes),
      workCenterCodes: toString(param.workCenterCodes),
      equipmentCategoryCodes: toString(param.equipmentCategoryCodes),
      objectTypeCodes: toString(param.objectTypeCodes),
      plannerGroups: toString(param.plannerGroups),
      ...convertBaseParamsToRequest(param),
    };

    return HttpClient.get(EQUIPMENT_PATH + "/masterdata/equipments", { params: body });
  }

  static getEquipment(equipmentId: string) {
    return HttpClient.get(EQUIPMENT_PATH + "/masterdata/equipment/" + equipmentId);
  }

  static getEquipmentHierarchy(equipmentId: string) {
    return HttpClient.get(EQUIPMENT_PATH + "/equipment/hierarchy", { params: { equipmentId: equipmentId } });
  }

  static getMeasPoints(measPointIds: string[]) {
    return HttpClient.get(EQUIPMENT_PATH + "/equipment/meas-point", {
      params: { measPointIds: toString(measPointIds) },
    });
  }

  static postMeasPoint(params: MeasPointUpdateParams) {
    return HttpClient.post(EQUIPMENT_PATH + "/equipment/post/meas-point", { data: params });
  }
}

import HttpClient from "./httpClient";

// const BASE_URL = '/iss/inventoryasset/asset-search';

export default class HistoryServices {
  // [GET] /api/detail-assets | get detail assets ...
  static getInventoryAssetHistory(body: any) {
    return HttpClient.post("/iss/inventoryasset/asset-history-inventory", body);
  }
  static getHistoryChange(body: any) {
    return HttpClient.post("/iss/inventoryasset/asset-history-chg", body);
  }
}

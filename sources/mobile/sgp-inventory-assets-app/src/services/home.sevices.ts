import HttpClient from "./httpClient";

// const BASE_URL = '/iss/inventoryasset/asset-search';

export default class HomeServices {
  // [GET] /api/detail-assets | get detail assets ...
  static postSearchAssets(params: any) {
    return HttpClient.post("/iss/inventoryasset/asset-search", params);
  }
  static postAssetGroup(params: any) {
    return HttpClient.post("/iss/inventoryasset/asset-list", params);
  }
  static async verifyQR(params: any) {
    return HttpClient.post("/iss/inventoryasset/asset-detail", params);
  }
  static getOrganizationInfomation(params: any) {
    const url = `/cos/masterdata/departments/organizations?pageIndex=${params?.page || 0}&pageSize=10&name=${
      params?.nameOrCode || ""
    }`;
    return HttpClient.get(url, params);
  }

  static getEmployees(params: any) {
    const url = `/cos/masterdata/employees?pageIndex=${params?.page || 0}&pageSize=10&nameOrCode=${
      params?.nameOrCode || ""
    }`;
    return HttpClient.get(url, params);
  }
}

import { API_URL } from "@env";
import { AxiosResponse, default as Axios } from "axios";
import { log } from "libs/react-native-x-framework/js/logger";
import { get, isEmpty } from "lodash";
import XFramework from "react-native-x-framework";

import { isBoolean, miniTimer } from "../utils/Basic";

interface ConfigParams {
  params: object;
}

export const AXIOS_AUTH_TOKEN_URL = API_URL;
const TIME_REQUEST = 60000;
export const PAGE_SIZE = 20;
export const UNKNOWN_ERROR = "0x00000";
export const RESPONSE_SUCCESS_CODE = "S";

export const getPagination = (pagination?: any) => {
  const { totalItems = 0, totalPages = 0, page = 1, size = PAGE_SIZE } = pagination || {};
  let nextPage = page + 1,
    hasNext = totalItems > 0 ? nextPage * size < totalItems : nextPage <= totalPages;
  return { totalItems, totalPages, page, size, hasNext, nextPage };
};

export const getErrorDetail = (error?: any) => {
  const detail = get(error, "data");
  if (detail) {
    const { errorCode, errorMessage } = detail;
    if (errorMessage) return { errorCode: errorCode, errorMessage: errorMessage };
  }
  return { errorCode: UNKNOWN_ERROR, errorMessage: "Đã có lỗi xảy ra, vui lòng thử lại!" };
};

// Axios default
export const AxiosInstanceDefault = Axios.create({
  baseURL: AXIOS_AUTH_TOKEN_URL,
  timeout: TIME_REQUEST,
  headers: {
    "Content-Type": "application/json",
    "User-Agent": "mobile",
  },
});

export const AxiosUploadInstance = Axios.create({
  headers: {
    "Content-Type": "multipart/form-data",
    "User-Agent": "mobile",
  },
});

[AxiosInstanceDefault, AxiosUploadInstance].forEach((axios: any) => {
  axios.interceptors.request.use(
    (response: any) => response,
    (error: any) => {
      Promise.reject(error.response || error.request || error.message);
    }
  );

  axios.interceptors.response.use(
    (response: any) => response,
    async (error: any) => {
      const { response } = error;

      if (!response) {
        return Promise.reject(error);
      }
      // close mini app khi token het han
      if (response.status === 401) {
        XFramework.closeApp();
      }
      return Promise.reject(response);
    }
  );
});

const http = {
  setAuthorizationHeader(accessToken: string) {
    if (isEmpty(accessToken)) {
      [AxiosInstanceDefault, AxiosUploadInstance].forEach((axios: any) => {
        delete axios.defaults.headers.common["Authorization"];
      });
    } else {
      [AxiosInstanceDefault, AxiosUploadInstance].forEach((axios: any) => {
        axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
      });
      log("setAuthorizationHeader", accessToken);
    }
  },

  get(url: string, config?: ConfigParams, isHandleErrorCM?: boolean) {
    if (config) {
      config.params = {
        ...config.params,
      };
    }
    log("HTTP Client GET - url/params", url, config?.params || "n/a");

    // console.log(url, "URl");
    return AxiosInstanceDefault.get(url, config);
  },

  post(url: string, data: any, config?: ConfigParams, isHandleErrorCM?: boolean) {
    if (config) {
      config.params = {
        ...config.params,
      };
    }
    log("HTTP Client POST - url/body", url, data);
    return AxiosInstanceDefault.post(url, data, config);
  },

  put(url: string, data = {}, config?: ConfigParams, isHandleErrorCM?: boolean) {
    if (config) {
      config.params = {
        ...config.params,
      };
    }
    log("HTTP Client PUT - url/body", url, data);
    return AxiosInstanceDefault.put(url, data, config);
  },

  patch(url: string, data = {}, config?: ConfigParams, isHandleErrorCM?: boolean) {
    if (config) {
      config.params = {
        ...config.params,
      };
    }
    log("HTTP Client PATCH - url/body", url, data);
    return AxiosInstanceDefault.patch(url, data, config);
  },

  delete(url: string, config?: ConfigParams, isHandleErrorCM?: boolean) {
    if (config) {
      config.params = {
        ...config.params,
      };
    }
    log("HTTP Client DELETE - url/body", url, config);
    return AxiosInstanceDefault.delete(url, config);
  },

  upload(url: string, formData: FormData) {
    log("HTTP Client UPLOAD - url/body", url);

    return AxiosUploadInstance({
      method: "POST",
      url: AXIOS_AUTH_TOKEN_URL + url,
      data: formData,
      transformRequest: (data, error) => {
        return formData;
      },
    });
  },
};

export const axiosHandler = (service: any, options?: any) => {
  // eslint-disable-next-line no-async-promise-executor
  const { delay } = options || {};
  return new Promise<any>(async resolve => {
    let resp,
      reqAt = +new Date();
    try {
      const response: AxiosResponse = await service();
      const data = get(response, "data.data", null);
      const code = get(response, "data.code", 0);
      const result = get(response, "data.result", "");

      resp = {
        data,
        isSuccess: code === 200 && result === "OK",
        error: null,
        code,
        response,
      };
    } catch (error: any) {
      log("axiosHandler - error", error);
      resp = {
        data: null,
        isSuccess: false,
        error: error,
        code: error?.code,
      };
    }
    if (delay) {
      let delayTime = 0;
      let resAt = +new Date();
      let minReqDur = isBoolean(delay) ? 500 : delay;
      if ((delayTime = resAt - reqAt) < minReqDur) delayTime = minReqDur - delayTime;
      await miniTimer(delayTime);
    }
    resolve(resp);
  });
};

export default http;

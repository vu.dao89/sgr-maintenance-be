import { toString } from "../utils/Format";
import HttpClient, { PAGE_SIZE } from "./httpClient";
import {
  ActivityKeysParams,
  ActivityTypeParams,
  BaseParams,
  CodeGroupParams,
  CodeParams,
  CommonStatusParams,
  convertBaseParamsToRequest,
  FunctionalLocationParams,
  PersonnelParams,
  PriorityParams,
} from "./parameters";

const MASTER_DATA_PATH = "/iss/inventoryasset/masterdata";

export default class MasterDataServices {
  static getObjectTypes(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/object-types", { params: convertBaseParamsToRequest(param) });
  }

  static getMaintenancePlants(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/maintenance-plants", { params: convertBaseParamsToRequest(param) });
  }

  static getEquipmentCategories(page: number) {
    return HttpClient.get(MASTER_DATA_PATH + "/equipment-categories", { params: { page: page, size: PAGE_SIZE } });
  }

  static getActivityTypes(param: ActivityTypeParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      costCenterCodes: toString(param.costCenterCodes),
    };

    return HttpClient.get(MASTER_DATA_PATH + "/activity-types", { params: body });
  }

  static getWorkOrderTypes(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/work-order-types", { params: convertBaseParamsToRequest(param) });
  }

  static getWorkCenters(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/work-centers", { params: convertBaseParamsToRequest(param) });
  }

  static getFunctionalLocations(param: FunctionalLocationParams) {
    const body = {
      functionalLocationCodes: toString(param.functionalLocationCodes),
      workCenterCodes: toString(param.workCenterCodes),
      plannerGroups: toString(param.plannerGroups),
      objectTypeCodes: toString(param.objectTypeCodes),
      categories: toString(param.categories),
      ...convertBaseParamsToRequest(param),
    };

    return HttpClient.get(MASTER_DATA_PATH + "/functional-locations", { params: body });
  }

  static getNotificationTypes(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/notification-types", { params: convertBaseParamsToRequest(param) });
  }

  static getPlannerGroups(param: BaseParams) {
    return HttpClient.get(MASTER_DATA_PATH + "/planner-groups", { params: convertBaseParamsToRequest(param) });
  }

  static getPriorities(param: PriorityParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      types: toString(param.types),
    };
    return HttpClient.get(MASTER_DATA_PATH + "/priorities", { params: body });
  }

  static getPersonnel(param: PersonnelParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      workCenterIds: toString(param.workCenterIds),
    };
    return HttpClient.get(MASTER_DATA_PATH + "/personnel", { params: body });
  }

  static getCommonStatus(param: CommonStatusParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      codes: toString(param.codes),
    };
    return HttpClient.get(MASTER_DATA_PATH + "/common-status", { params: body });
  }

  static getByQRCode(qrCode: string) {
    return HttpClient.get(MASTER_DATA_PATH + "/qrcode", { params: { qrCode: qrCode } });
  }

  static getCodeGroups(param: CodeGroupParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      catalogProfiles: param.catalogProfile,
      catalogs: param.catalog,
    };
    return HttpClient.get(MASTER_DATA_PATH + "/code-groups", { params: body });
  }

  static getCodes(param: CodeParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      catalogProfile: param.catalogProfile,
      codeGroups: param.codeGroup,
      catalog: param.catalog,
    };

    return HttpClient.get(MASTER_DATA_PATH + "/codes", { params: body });
  }

  static getControlKey(param: CodeParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
    };

    return HttpClient.get(MASTER_DATA_PATH + "/control-keys", { params: body });
  }

  static getActivityKeys(param: ActivityKeysParams) {
    const body = {
      ...convertBaseParamsToRequest(param),
      workOrderTypes: toString(param.workOrderTypes),
    };
    return HttpClient.get(MASTER_DATA_PATH + "/activity-keys", { params: body });
  }

  static getSystemConditions() {
    return HttpClient.get(MASTER_DATA_PATH + "/system-conditions");
  }

  static getNotificationType(code: string, maintenancePlantCode: string) {
    return HttpClient.get(`${MASTER_DATA_PATH}/notification-type/${code}/${maintenancePlantCode}`);
  }

  static getLockReasons() {
    return HttpClient.get(MASTER_DATA_PATH + "/lock-reasons");
  }
}

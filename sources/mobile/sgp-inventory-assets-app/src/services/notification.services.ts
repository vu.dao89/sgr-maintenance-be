import moment from "moment";
import { isArray } from "../utils/Basic";
import { toString } from "../utils/Format";
import HttpClient from "./httpClient";
import {
  convertBaseParamsToRequest,
  convertNotificationParamsToRequest,
  convertNotifItemToRequest,
  MetricParams,
  NotificationActivityParams,
  NotificationCreateParams,
  NotificationItemCreateParams,
  NotificationSearchParams,
} from "./parameters";

const NOTIFICATION_PATH = "/iss/inventoryasset/notification";

export default class NotificationServices {
  static getNotifications(param: NotificationSearchParams) {
    var body: any = {
      ...convertBaseParamsToRequest(param),
      createFrom: "",
      createTo: "",
      Id: param.keyword || "",
      description: param.keyword || "",
      dateFrom: param.notiDateFrom || "",
      dateTo: param.notiDateTo || "",
      requestStartDateFrom: "",
      requestStartDateTo: "",
      requestEndDateFrom: "",
      requestEndDateTo: "",
      reportBy: param.reportBy || "",
      assignTo: param.assignTo || "",
      employeeId: param.employeeId || "",
      qrCode: "",
    };
    [
      { values: param.priorities, field: "priorities" },
      { values: param.notiTypes, field: "types" },
      { values: param.plannerGroups, field: "plannerGroups" },
      { values: param.statuses, field: "status" },
      { values: param.equipments, field: "equipments" },
      { values: param.functionalLocations, field: "functionalLocations" },
    ].forEach((t: any) => {
      if (isArray(t.values, 1)) {
        body[t.field] = toString(t.values);
      }
    });

    return HttpClient.get(NOTIFICATION_PATH + "/get/notification-search", { params: body });
  }

  static getDetailNotification(notificationId: string) {
    return HttpClient.get(NOTIFICATION_PATH + "/get/notification", { params: { notificationId: notificationId } });
  }

  static postNotificaiton(params: NotificationCreateParams) {
    return HttpClient.post(NOTIFICATION_PATH + "/post/notification-post", convertNotificationParamsToRequest(params));
  }

  static changeNotificaiton(params: NotificationCreateParams) {
    return HttpClient.post(NOTIFICATION_PATH + "/post/notification-change", convertNotificationParamsToRequest(params));
  }

  static closeNotification(notificationId: string, activites: Array<NotificationActivityParams>) {
    return HttpClient.post(NOTIFICATION_PATH + "/post/notification-close", {
      id: notificationId,
      referDate: moment(new Date()).format("YYYYMMDD"),
      referDateTime: moment(new Date()).format("HHmmss"),
      notificationActivities: activites.map((act: any) => {
        return { ...act, id: notificationId };
      }),
    });
  }

  static createNotificaitonItem(params: NotificationItemCreateParams) {
    return HttpClient.post(NOTIFICATION_PATH + "/post/notification-item", convertNotifItemToRequest(params));
  }

  static getNotificationMetric(params: MetricParams) {
    const body = {
      mainPlantIds: toString(params.mainPlantIds),
      notificationDateFrom: params.fromDate,
      notificationDateTo: params.toDate,
      employeeId: params.mainPerson,
    };
    return HttpClient.get(NOTIFICATION_PATH + "/metric", { params: body });
  }
}

import { CFile } from "../app/components/common/attach-files";
import { genShortDescription, isArray } from "../utils/Basic";
import { toString } from "../utils/Format";
import HttpClient from "./httpClient";
import {
  convertBaseParamsToRequest,
  MaterialDocumentItemParams,
  MeasPointUpdateParams,
  MetricParams,
  OperationBaseParams,
  OperationSearchParams,
  SubOperationConfirmationParams,
  SubOperationCreateParams,
  SuperOperationParams,
} from "./parameters";

const OPERATION_PATH = "/iss/inventoryasset/api/operation";
const OPERATION_UNIT = "MIN";
const OPERATION_TYPE_ADD = "ADD";
const OPERATION_TYPE_CHANGE = "CHANGE";

export default class OperationServices {
  static searchOperation(params: OperationSearchParams, personPermission: string) {
    const body = {
      ...convertBaseParamsToRequest(params),
      workOrderDateForm: params.workOrderDateForm || "",
      workOrderDateTo: params.workOrderDateTo || "",
      workOrderFinishFrom: params.workOrderFinishFrom || "",
      workOrderFinishTo: params.workOrderFinishTo || "",
      workOrderIds: toString(params.workOrderIds),
      operationIds: toString(params.operationIds),
      priorityIds: toString(params.priorityIds),
      equipmentIds: toString(params.equipmentIds),
      functionLocationIds: toString(params.functionLocationIds),
      status: toString(params.status),
      personnels: isArray(params.personnels, 1) ? toString(params.personnels) : personPermission,
      workCenterIds: toString(params.workCenterIds),
    };
    return HttpClient.get(OPERATION_PATH + "/search", { params: body });
  }

  static getOperationComponent(workOrderId: string, operationId: string) {
    const params: OperationBaseParams = {
      workOrderId: workOrderId,
      operationId: operationId,
    };
    return HttpClient.get(OPERATION_PATH + "/component", { params: params });
  }

  static getCurrentOperation() {
    return HttpClient.get(OPERATION_PATH + "/current");
  }

  static getOperationDetail(workOrderId: string, operationId: string) {
    const params: OperationBaseParams = {
      workOrderId: workOrderId,
      operationId: operationId,
    };
    return HttpClient.get(OPERATION_PATH + "/detail", { params: params });
  }

  static getOperationMeasures(workOrderId: string, operationId: string) {
    const params: OperationBaseParams = {
      workOrderId: workOrderId,
      operationId: operationId,
    };
    return HttpClient.get(OPERATION_PATH + "/prt", { params: params });
  }

  static getSubOperationDetail(workOrderId: string, operationId: string) {
    const params: OperationBaseParams = {
      workOrderId: workOrderId,
      operationId: operationId,
    };
    return HttpClient.get(OPERATION_PATH + "/sub/detail", { params: params });
  }

  static createSubOperation(params: SubOperationCreateParams) {
    return HttpClient.post(OPERATION_PATH + "/sub/change", {
      ...params,
      operationDesc: genShortDescription(params.operationLongText),
      unit: OPERATION_UNIT,
      type: OPERATION_TYPE_ADD,
    });
  }

  static updateSubOperation(operationId: string, params: SubOperationCreateParams) {
    return HttpClient.post(OPERATION_PATH + "/sub/change", {
      ...params,
      operationId: operationId,
      operationDesc: genShortDescription(params.operationLongText),
      unit: OPERATION_UNIT,
      type: OPERATION_TYPE_CHANGE,
    });
  }

  static updateSuperOperation(params: SuperOperationParams) {
    const subOperations = params.subOperations.map((child: SuperOperationParams) => {
      return {
        ...child,
        operationDesc: genShortDescription(child.operationLongText),
        unit: OPERATION_UNIT,
        type: OPERATION_TYPE_CHANGE,
      };
    });

    return HttpClient.post(OPERATION_PATH + "/update", {
      ...params,
      operationDesc: genShortDescription(params.operationLongText),
      subOperations: subOperations,
      unit: OPERATION_UNIT,
      type: OPERATION_TYPE_CHANGE,
    });
  }

  static createSuperOperation(params: any) {
    const subOperations = params.subOperations.map((child: SuperOperationParams) => {
      return {
        ...child,
        operationDesc: genShortDescription(child.operationLongText),
        unit: OPERATION_UNIT,
        type: OPERATION_TYPE_ADD,
      };
    });
    return HttpClient.post(OPERATION_PATH + "/create", {
      ...params,
      operationDesc: genShortDescription(params.operationLongText),
      unit: OPERATION_UNIT,
      type: OPERATION_TYPE_ADD,
      subOperations: subOperations,
    });
  }

  static deleteOperation(workOrderId: string, operationId: string) {
    return HttpClient.delete(OPERATION_PATH + "/delete", {
      params: { workOrderId: workOrderId, operationId: operationId },
    });
  }

  static deleteSubOperation(workOrderId: string, operationId: string, superOperationId: string) {
    return HttpClient.delete(OPERATION_PATH + "/delete", {
      params: { workOrderId: workOrderId, operationId: superOperationId, subOperationId: operationId },
    });
  }

  static confirmSubOperation(params: SubOperationConfirmationParams) {
    return HttpClient.post(OPERATION_PATH + "/confirmation", {
      confirmation: { ...params, finalConfirmation: "X" },
    });
  }

  static createMaterialDocument(workOrderId: string, operationId: string, items: Array<MaterialDocumentItemParams>) {
    return HttpClient.post(OPERATION_PATH + "/material/create", { workOrderId, operationId, items });
  }

  static getOperationMaterial(workOrderId: string, operationId: string) {
    return HttpClient.get(OPERATION_PATH + "/material", { params: { workOrderId, operationId } });
  }

  static startOperation(workOrderId: string, operationId: string) {
    return HttpClient.post(OPERATION_PATH + "/start", { workOrderId: workOrderId, operationId: operationId });
  }

  static holdOperation(params: any) {
    let body = new FormData();
    body.append("workOrderId", params.workOrderId);
    body.append("operationId", params.operationId);
    body.append("superOperationId", params.superOperationId);
    body.append("actualWork", params.actualWork);
    body.append("workStartDate", params.workStartDate);
    body.append("workStartTime", params.workStartTime);
    body.append("workFinishDate", params.workFinishDate);
    body.append("workFinishTime", params.workFinishTime);
    body.append("varianceReasonCode", params.varianceReasonCode);
    body.append("confirmation", params.confirmation);

    params?.files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(OPERATION_PATH + "/hold", body);
  }

  static completeOperation(params: any) {
    let body = new FormData();
    body.append("workOrderId", params.workOrderId);
    body.append("operationId", params.operationId);
    body.append("superOperationId", params.superOperationId);
    body.append("actualWork", params.actualWork);
    body.append("workStartDate", params.workStartDate);
    body.append("workStartTime", params.workStartTime);
    body.append("workFinishDate", params.workFinishDate);
    body.append("workFinishTime", params.workFinishTime);
    body.append("varianceReasonCode", params.varianceReasonCode);
    body.append("confirmation", params.confirmation);

    params?.files.forEach((f: CFile) => {
      body.append("files", f.file);
    });

    return HttpClient.upload(OPERATION_PATH + "/complete", body);
  }

  static changePersonnelOperationParent(workOrderId: string, operationId: string, personnelId: string) {
    return HttpClient.put(OPERATION_PATH + "/parent/personnel-change", {
      workOrderId: workOrderId,
      operationId: operationId,
      personnelId: personnelId,
    });
  }

  static getOperationMetrics(params: MetricParams) {
    const body = {
      maintenancePlantIds: toString(params.mainPlantIds),
      mainPerson: params.mainPerson,
      operationFinishFrom: params.fromDate,
      operationFinishTo: params.toDate,
    };
    return HttpClient.get(OPERATION_PATH + "/metric", { params: body });
  }

  static postMeasuringPoint(params: MeasPointUpdateParams) {
    return HttpClient.post(OPERATION_PATH + "/measuring-point", { data: params });
  }
}

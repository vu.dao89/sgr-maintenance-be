import { CFile } from "../app/components/common/attach-files";
import { DateTimeUtil } from "../utils";
import { PAGE_SIZE } from "./httpClient";

export interface BaseParams {
  maintenancePlantCodes: Array<string>;
  keyword?: string;
  page?: number;
  size?: number;
}

export const convertBaseParamsToRequest = (params: BaseParams) => {
  return {
    maintenancePlantCodes: params.maintenancePlantCodes.join(","),
    filterText: params.keyword || "",
    page: params.page || 1,
    size: params.size || PAGE_SIZE,
  };
};

export const convertNotificationParamsToRequest = (params: NotificationCreateParams) => {
  const header = {
    id: params.id || "",
    type: params.notificationType,
    desc: params.title,
    lText: params.notificationLText,
    maintenancePlant: params.mainPlant,
    equipmentId: params.equipment,
    functionalLocationId: params.functionalLocation,
    workCenter: params.workCenter,
    priorityType: params.priorityType,
    priority: params.priority,
    reportBy: params.reportBy,
    assignTo: params.personel,
    breakDown: params.isStopOperating ? "X" : "",
    date: params.discoveredDate ? DateTimeUtil.formatDateSAP(params.discoveredDate) : "",
    time: params.discoveredDate ? DateTimeUtil.formatTimeSAP(params.discoveredDate) : "",
    requestStart: params.requestProcessingDate ? DateTimeUtil.formatDateSAP(params.requestProcessingDate) : "",
    requestStartTime: params.requestProcessingDate ? DateTimeUtil.formatTimeSAP(params.requestProcessingDate) : "",
    requestEnd: params.processingTillDate ? DateTimeUtil.formatDateSAP(params.processingTillDate) : "",
    requestEndTime: params.processingTillDate ? DateTimeUtil.formatTimeSAP(params.processingTillDate) : "",
    malfunctionStart: params.stopOperatingFromDate ? DateTimeUtil.formatDateSAP(params.stopOperatingFromDate) : "",
    malfunctionStartTime: params.stopOperatingFromDate ? DateTimeUtil.formatTimeSAP(params.stopOperatingFromDate) : "",
    malfunctionEnd:
      params.stopOperatingToDate && params.isStopOperating
        ? DateTimeUtil.formatDateSAP(params.stopOperatingToDate)
        : "",
    malfunctionEndTime:
      params.stopOperatingToDate && params.isStopOperating
        ? DateTimeUtil.formatTimeSAP(params.stopOperatingToDate)
        : "",
  };

  const items = params.items.map((item: NotificationItemCreateParams) => convertNotifItemToRequest(item));

  return { header: header, documentIds: params.documentIds, items: { notificationItems: items } };
};

export const convertNotifItemToRequest = (item: NotificationItemCreateParams) => {
  return {
    id: item.id || "",
    item: item.item || "",
    part: item.notifPart || "",
    partCode: item.notifPartCode || "",
    problem: item.notifProblem || "",
    dame: item.notifDame || "",
    idText: item.notifText || "",
    cause: item.notifCause || "",
    causeCode: item.notifCauseCode || "",
    causeText: item.notifCauseText || "",
  };
};

export interface EquipmentSearchParams extends BaseParams {
  functionalLocationCodes: Array<string>;
  workCenterCodes: Array<string>;
  equipmentCategoryCodes: Array<string>;
  objectTypeCodes: Array<string>;
  plannerGroups: Array<string>;
}

export interface MeasPointUpdateParams {
  measPoint: string;
  measDocDate: string;
  measDocTime: string;
  measDocText: string;
  measDocLText: string;
  readBy: string;
  measRead: string;
  countRead: string;
  diff: string;
  workOrder: string;
  operation: string;
  countReadId: string;
  isMeasDiff: boolean;
}

export interface NotificationSearchParams extends BaseParams {
  employeeId: string;
  notiDateFrom: string;
  notiDateTo: string;
  notiTypes: Array<string>;
  reportBy: string;
  assignTo: string;
  priorities: Array<string>;
  plannerGroups: Array<string>;
  functionalLocations: Array<string>;
  equipments: Array<string>;
  statuses: Array<string>;
}

export interface NotificationCreateParams {
  id?: string;
  reportBy: string;
  notificationType: string;
  title: string;
  notificationLText: string;
  mainPlant: string;
  equipment: string;
  functionalLocation: string;
  workCenter: string;
  priority: string;
  priorityType: string;
  personel: string;
  isStopOperating: boolean;
  discoveredDate: string | null;
  requestProcessingDate: string | null;
  processingTillDate: string | null;
  stopOperatingFromDate: string | null;
  stopOperatingToDate: string | null;
  items: Array<NotificationItemCreateParams>;
  documentIds: Array<string>;
}

export interface NotificationItemCreateParams {
  id?: string;
  item?: string;
  notifPart: string;
  notifPartCode: string;
  notifProblem: string;
  notifDame: string;
  notifText: string;
  notifCause: string;
  notifCauseCode: string;
  notifCauseText: string;
}

export interface FunctionalLocationParams extends BaseParams {
  functionalLocationCodes: Array<string>;
  workCenterCodes: Array<string>;
  plannerGroups: Array<string>;
  objectTypeCodes: Array<string>;
  categories: Array<string>;
}

export interface CommonStatusParams extends BaseParams {
  codes: Array<string>;
}

export interface PriorityParams extends BaseParams {
  types: Array<string>;
}

export interface PersonnelParams extends BaseParams {
  workCenterIds: Array<string>;
}

export interface CodeGroupParams extends BaseParams {
  catalogProfile: string;
  catalog?: string;
}

export interface CodeParams extends BaseParams {
  catalogProfile: string;
  codeGroup: string;
  catalog: string;
}

export interface WorkOrderSearchParams extends BaseParams {
  workCenterIds: Array<string>;
  workOrderTypeIds: Array<string>;
  equipmentIds: Array<string>;
  functionalLocationIds: Array<string>;
  statusIds: Array<string>;
  priorityIds: Array<string>;
  mainPersonIds: Array<string>;
  workOrderFinishDateFrom: string;
  workOrderFinishDateTo: string;
}

export interface WorkOrderChangeParams {
  workOrderId: string;
  workOrderLongText: string;
  maintenanceActivityKey: string;
  equipmentId: string;
  priority: string;
  mainPerson: string;
  mainPlant: string;
  workCenter: string;
  plannerGroup: string;
  systemCondition: string;
  workOrderStartDate: string;
  workOrderStartTime: string;
  workOrderFinishDate: string;
  workOrderFinishTime: string;
}

export interface WorkOrderCreateParams {
  workOrderLongText: string;
  workOrderType: string;
  maintenanceActivityKey: string;
  equipmentId: string;
  functionalLocation: string;
  priority: string;
  mainPersonnel: string;
  maintenancePlant: string;
  workCenter: string;
  plannerGroup: string;
  systemCondition: string;
  startDate: string;
  startTime: string;
  finishDate: string;
  finishTime: string;
  notification: string;
}
export interface MetricParams {
  mainPlantIds: Array<string>;
  mainPerson: string;
  fromDate: string;
  toDate: string;
}

export interface WorkOrderConfirmationParams {
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  workCenter: string;
  maintenancePlant: string;
  personnel: string;
  actualWork: string;
  actualWorkUnit: string;
  activityType: string;
  postingDate: string;
  finalConfirmation: string;
  workStartDate: string;
  workStartTime: string;
  workFinishDate: string;
  workFinishTime: string;
  reason: string;
  confirmation: string;
  confirmLongText: string;
  email: string;
}

export interface OperationSearchParams extends BaseParams {
  workOrderDateForm?: string;
  workOrderDateTo?: string;
  workOrderFinishFrom?: string;
  workOrderFinishTo?: string;
  workOrderIds?: Array<string>;
  operationIds?: Array<string>;
  priorityIds: Array<string>;
  equipmentIds: Array<string>;
  functionLocationIds: Array<string>;
  status: Array<string>;
  personnels: Array<string>;
  workCenterIds: Array<string>;
}
export interface SuperOperationParams {
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  operationLongText: string;
  equipmentId: string;
  controlKey: string;
  maintenancePlantId: string;
  workCenterId: string;
  functionalLocationId: string;
  personnel: string;
  estimate: string;
  activityType: string;
  localId: string;
  documentIds: Array<string>;
  subOperations: Array<SuperOperationParams>;
}

export interface OperationBaseParams {
  workOrderId: string;
  operationId: string;
}

export interface OperationHistoryCreateParams extends OperationBaseParams {
  userId: string;
}

export interface ActivityKeysParams extends BaseParams {
  workOrderTypes: Array<string>;
}

export interface WorkOrderOperationCreateParams {
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  operationDesc: string;
  operationLongText: string;
  equipmentId: string;
  maintenancePlant: string;
  workCenter: string;
  functionalLocationId: string;
  controlKey: string;
  personnel: string;
  estimate: string;
  unit: string;
  activityType: string;
  documentIds: Array<string>;
  subOperations: Array<WorkOrderOperationCreateParams>;
}

export interface SubOperationCreateParams {
  workOrderId: string;
  superOperationId: string;
  operationLongText: string;
  equipmentId: string;
  maintenancePlantId: string;
  workCenterId: string;
  functionalLocationId: string;
  personnel: string;
  controlKey: string;
  estimate: string;
}

export interface SubOperationConfirmationParams {
  confirmation: string;
  maintenancePlant: string;
  operationId: string;
  personnel: string;
  superOperationId: string;
  workCenter: string;
  workOrderId: string;
  reason: string;
}

export interface MaterialDocumentHeaderParams {
  documentDate: string;
  postingDate: string;
  documentText: string;
  email: string;
}

export interface MaterialDocumentItemParams {
  reservation: string;
  reservationItem: string;
  material: string;
  quantity: string;
}

export interface ActivityTypeParams extends BaseParams {
  costCenterCodes: Array<string>;
}

export interface NotificationActivityParams {
  activity: string; // Nhóm khắc phục
  activityCode: string; // Chi tiết khắc phục
  activityText: string; // Mô tả chi tiết
}

export interface OperationConfirmParamms {
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  actualWork: string;
  workStartDate: string;
  workStartTime: string;
  workFinishDate: string;
  workFinishTime: string;
  varianceReasonCode: string;
  confirmation: string;
  files: Array<CFile>;
}

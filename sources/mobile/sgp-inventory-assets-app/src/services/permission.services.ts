import { get, isEmpty } from "lodash";
import { OperationHistoryActivity, Permission, SystemStatus } from "../constants";

export interface PermissionResult {
  isShow: boolean;
  isDisabled: boolean;
}

export default class PermissionServices {
  static canChangeWorker(
    reducer: any,
    operationStatusCode: any,
    operationHistory: any,
    operationPersonnel: string,
    supervisor: string
  ) {
    const employeeCode = PermissionServices.getEmployeeCode(reducer);
    const haveAllPersonPermission = isEmpty(PermissionServices.getPersonPermission(reducer));

    const shouldEnabled = () => {
      if (haveAllPersonPermission && isEmpty(operationPersonnel)) return true;
      return employeeCode === supervisor || employeeCode === operationPersonnel;
    };

    return {
      isShow:
        (operationStatusCode === SystemStatus.operation.CREATE_NEW ||
          operationStatusCode === SystemStatus.operation.A_HAFT ||
          operationStatusCode === SystemStatus.operation.READY) &&
        operationHistory != OperationHistoryActivity.START,
      isDisabled:
        !shouldEnabled() || !PermissionServices.checkPermission(reducer, Permission.OPERATION.CHANGE_MAIN_PERSON),
    } as PermissionResult;
  }

  static canChangeSupervisor(reducer: any, workOrderStatusCode: any, supervisor: string) {
    const haveAllPersonPermission = isEmpty(PermissionServices.getPersonPermission(reducer));
    const employeeCode = PermissionServices.getEmployeeCode(reducer);

    const shouldEnabled = () => {
      if (haveAllPersonPermission && isEmpty(supervisor)) return true;
      return employeeCode === supervisor;
    };

    return {
      isShow: workOrderStatusCode === SystemStatus.workorder.CREATE_NEW,
      isDisabled:
        !shouldEnabled() || !PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.CHANGE_MAIN_PERSON),
    } as PermissionResult;
  }

  static getPlantPermission(reducer: any) {
    return get(reducer, "permission.userPlants", []);
  }

  static getPersonPermission(reducer: any) {
    return get(reducer, "permission.personPermission", "");
  }

  static getEmployeeCode(reducer: any) {
    return get(reducer, "permission.employeeCode");
  }

  static checkPermission(reducer: any, permissionCode: any) {
    const flattenPermission = get(reducer, "permission.flattenPermission", []);
    return flattenPermission.includes(permissionCode);
  }

  static canEditNotification(reducer: any, notificationStatusCode: any, reporter: any, assignee: any) {
    const employeeCode = PermissionServices.getEmployeeCode(reducer);
    if (employeeCode != reporter && employeeCode != assignee) return false;

    return (
      notificationStatusCode != SystemStatus.notification.COMPLETED &&
      PermissionServices.checkPermission(reducer, Permission.NOTIFICATION.CHANGE)
    );
  }

  static canEditWorkOrder(reducer: any, workOrderStatusCode: any, mainPerson: string) {
    if (SystemStatus.workorder.isCompleted(workOrderStatusCode)) return false;

    const employeeCode = PermissionServices.getEmployeeCode(reducer);
    if (mainPerson != employeeCode) return false;

    return PermissionServices.checkPermission(reducer, Permission.WORK_ORDER.POST);
  }

  static canEditOperation(reducer: any, operationStatusCode: any, operationPersonnel: string, supervisor: string) {
    const employeeCode = PermissionServices.getEmployeeCode(reducer);
    if (employeeCode != operationPersonnel && employeeCode != supervisor) return false;

    return (
      !SystemStatus.operation.isCompleted(operationStatusCode) &&
      PermissionServices.checkPermission(reducer, Permission.OPERATION.POST)
    );
  }
}

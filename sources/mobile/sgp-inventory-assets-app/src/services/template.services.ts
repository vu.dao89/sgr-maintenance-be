import HttpClient from "./httpClient";

const BASE_URL = "/api/templates";

export default class TemplateServices {
  // [GET] /api/templates | get info ...
  static getTemplates(params: any) {
    return HttpClient.get(BASE_URL, params);
  }
}

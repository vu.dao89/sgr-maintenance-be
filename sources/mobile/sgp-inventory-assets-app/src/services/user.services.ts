import HttpClient from "./httpClient";

export default class UserServices {
  static getUserInfo() {
    return HttpClient.get("/iss/inventoryasset/users/user-info");
  }
}

import { genShortDescription, isArray } from "../utils/Basic";
import { toString } from "../utils/Format";
import HttpClient from "./httpClient";
import {
  convertBaseParamsToRequest,
  MetricParams,
  NotificationActivityParams,
  WorkOrderChangeParams,
  WorkOrderConfirmationParams,
  WorkOrderCreateParams,
  WorkOrderOperationCreateParams,
  WorkOrderSearchParams,
} from "./parameters";

const WORK_ORDER_PATH = "/iss/inventoryasset/api/work-order";

export default class WorkOrderServices {
  static workOrderSearch(params: WorkOrderSearchParams, personPermission: string) {
    const body = {
      ...convertBaseParamsToRequest(params),
      workCenterIds: toString(params.workCenterIds),
      workOrderTypeIds: toString(params.workOrderTypeIds),
      equipmentIds: toString(params.equipmentIds),
      functionalLocationIds: toString(params.functionalLocationIds),
      statusIds: toString(params.statusIds),
      priorityIds: toString(params.priorityIds),
      mainPersonIds: isArray(params.mainPersonIds, 1) ? toString(params.mainPersonIds) : personPermission,
      workOrderFinishDateFrom: params.workOrderFinishDateFrom || "",
      workOrderFinishDateTo: params.workOrderFinishDateTo || "",
    };
    return HttpClient.get(WORK_ORDER_PATH + "/search", { params: body });
  }

  static getWorkOrderDetails(workOrderId: string) {
    return HttpClient.get(WORK_ORDER_PATH, { params: { workOrderId: workOrderId } });
  }

  static createWorkOrder(workOrderParams: WorkOrderCreateParams, operations: Array<WorkOrderOperationCreateParams>) {
    return HttpClient.post(WORK_ORDER_PATH, {
      workOrder: { ...workOrderParams, workOrderDesc: genShortDescription(workOrderParams.workOrderLongText) },
      operation: { items: operations },
    });
  }

  static changeWorkOrder(params: WorkOrderChangeParams) {
    return HttpClient.put(WORK_ORDER_PATH, {
      ...params,
      workOrderDescription: genShortDescription(params.workOrderLongText),
    });
  }

  static confirmWorkOrder(params: WorkOrderConfirmationParams) {
    return HttpClient.post(WORK_ORDER_PATH + "/confirmation", { confirmation: params });
  }

  static completeWorkOrder(workOrderId: string, notificationId: string, activities: Array<NotificationActivityParams>) {
    return HttpClient.post(WORK_ORDER_PATH + "/complete", {
      workOrderId: workOrderId,
      notificationId: notificationId,
      activities: activities.map((act: any) => {
        return { ...act, id: notificationId };
      }),
    });
  }

  static startWorkOrder(workOrderId: string) {
    return HttpClient.post(WORK_ORDER_PATH + "/start", { workOrderId: workOrderId });
  }

  static lockWorkOrder(workOrderId: string, reasonCode: string) {
    return HttpClient.post(WORK_ORDER_PATH + "/lock", { workOrderId: workOrderId, reasonCode: reasonCode });
  }

  static getWorkOrderMetric(params: MetricParams) {
    const body = {
      mainPlantIds: toString(params.mainPlantIds),
      mainPerson: params.mainPerson,
      workOrderFinishFrom: params.fromDate,
      workOrderFinishTo: params.toDate,
    };
    return HttpClient.get(WORK_ORDER_PATH + "/metric", { params: body });
  }

  static changePersonnel(workOrderId: string, personnelId: string) {
    return HttpClient.put(WORK_ORDER_PATH + "/personnel-change", {
      workOrderId: workOrderId,
      personnelId: personnelId,
    });
  }
}

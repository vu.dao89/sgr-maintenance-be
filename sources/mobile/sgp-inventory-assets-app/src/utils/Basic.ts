export const isArray = (value: any, minLength?: any) =>
  Array.isArray(value) && (minLength ? value.length >= (typeof minLength === "number" ? minLength : 1) : true);

export const isNumber = (value: any) => typeof value === "number";

export const isBoolean = (value: any) => typeof value === "boolean";

export const miniTimer = (time: any) =>
  new Promise(resolve => (isNumber(time) ? setTimeout(resolve, time) : resolve(true)));

export const removeViChar = (str: any) => {
  if (!str) return "";
  str = String(str).toLowerCase();
  return str
    .replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, "a")
    .replace(/[èéẹẻẽêềếệểễ]/g, "e")
    .replace(/[ìíịỉĩ]/g, "i")
    .replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, "o")
    .replace(/[ùúụủũưừứựửữ]/g, "u")
    .replace(/[ỳýỵỷỹ]/g, "y")
    .replace(/đ/g, "d");
};

export const genShortDescription = (fullDescription?: string) => (fullDescription || "").substring(0, 40);

export const randomString = (input: any, strLength: any) => {
  let sources: any = {
    num: "0123456789",
    char: "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
    mix: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
    charLow: "abcdefghiklmnopqrstuvwxyz",
    charUp: "ABCDEFGHIJKLMNOPQRSTUVWXTZ",
    numLow: "0123456789abcdefghiklmnopqrstuvwxyz",
    numUp: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ",
  };
  let str = sources[input] || input || "";
  let length = strLength || str.length;
  return Array.from({ length })
    .map(() => {
      let rnum = Math.floor(Math.random() * str.length);
      return str.substring(rnum, rnum + 1);
    })
    .join("");
};

export const shuffleString = (str: any) => {
  let a = str.split(""),
    n = a.length;
  for (let i = n - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }
  return a.join("");
};

export const genRandomString = () => shuffleString(randomString("char", 11) + +new Date());

/*export const genUuid = (version: any = "1", options: any = {}) => {
  let custom_namespace;
  version = String(version);
  if (version === "3" || version === "5") {
    if (!isArray(options, true)) {
      return;
    }
    switch (options[1]) {
      case "DNS":
        custom_namespace = uuidv5.DNS;
        break;
      case "URL":
        custom_namespace = uuidv5.URL;
        break;
      default:
        custom_namespace = options[1];
        break;
    }
  }
  switch (version) {
    case "0":
      return shuffleString(randomString("char", 11) + +new Date());
    case "1": // timestamp => '2c5ea4c0-4067-11e9-8bad-9b1deb4d3b7d';
      return uuidv1();
    case "3": // namespace
      // using predefined DNS namespace (for domain names) => '9125a8dc-52ee-365b-a5aa-81b0b3681cf6';
      // using predefined URL namespace (for, well, URLs) => 'c6235813-3ba4-3801-ae84-e0a6ebb7d138';
      // using a custom namespace;
      // => Note: Custom namespaces should be a UUID string specific to your application!
      // => E.g. the one here was generated using this modules `uuid` CLI.
      // => const MY_NAMESPACE = '1b671a64-40d5-491e-99b0-da01ff1f3341';
      // => 'e8b5a51d-11c8-3310-a6ab-367563f20686';
      return uuidv3(options[0], custom_namespace);
    case "4":
      return uuidv4(); // random => '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed';
    case "5":
      return uuidv5(options[0], custom_namespace); // namespace, same input type as v3;
    default:
      return;
  }
};*/

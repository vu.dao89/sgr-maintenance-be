import { isEmpty } from "lodash";
// import AsyncStorage from '@react-native-async-storage/async-storage';
import EncryptedStorage from "react-native-encrypted-storage";

const Key = {
  Token: "ACCESS_TOKEN_KEY",
  TokenExpDate: "ACCESS_TOKEN_EXPIRATION_DATE",
};

// Token
const setToken = async (value: string) => {
  try {
    await EncryptedStorage.setItem(Key.Token, value);
  } catch (e) {
    // saving error
  }
};
const getToken = async () => {
  try {
    const token = await EncryptedStorage.getItem(Key.Token);
    return isEmpty(token) ? "" : token?.replace(/\"/g, "");
  } catch (e) {
    // saving error
  }
};

// AccessTokenExpirationDate
const setTokenExpDate = async (value: string) => {
  try {
    await EncryptedStorage.setItem(Key.TokenExpDate, value);
  } catch (e) {
    // saving error
  }
};
const getTokenExpDate = async () => {
  try {
    const token = await EncryptedStorage.getItem(Key.TokenExpDate);
    return isEmpty(token) ? "" : token?.replace(/\"/g, "");
  } catch (e) {
    // saving error
  }
};

export default {
  setToken,
  getToken,
  setTokenExpDate,
  getTokenExpDate,
};

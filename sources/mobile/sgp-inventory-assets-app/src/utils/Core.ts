import { useCallback, useEffect, useRef, useState } from "react";

// const apiInstance = ApiMgr.getInstance();

// export const doRequest = (...args) => apiInstance.doRequest(...args);
// export const simpleRequest = (...args) => apiInstance.simpleRequest(...args);
// export const fetchRequest = (...args) => apiInstance.fetchRequest(...args);
// export const cancelRequest = (cancelId) => apiInstance.cancelRequest(cancelId);

export const isCloseToBottom = (event: any, offset: any = 100) => {
  const { layoutMeasurement, contentOffset, contentSize } = event.nativeEvent || event;
  return layoutMeasurement.height + contentOffset.y >= contentSize.height - offset;
};

export const asyncSetState = (_this: any, state: object) =>
  new Promise(resolve => _this.setState(state, () => resolve(true)));

export const useIsMounted = () => {
  const isMounted = useRef(false);
  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);
  return useCallback(() => isMounted.current, []);
};

export const useStateLazy = (initialValue: any) => {
  const callbackRef: any = useRef(null);
  const [value, setValue] = useState(initialValue);
  useEffect(() => {
    if (callbackRef.current) {
      callbackRef.current(value);
      callbackRef.current = null;
    }
  }, [value]);
  const setValueWithCallback = useCallback((newValue: any, callback: Function) => {
    callbackRef.current = callback;
    return setValue(newValue);
  }, []);
  return [value, setValueWithCallback];
};

/*export const apiCommonRequest = (api: any, params?: any, options?: any) =>
  new Promise(async resolve => {
    let { delay, debug, skipValidate } = options || {};
    let result,
      reqAt = +new Date();
    try {
      let { status, data } = await api(...(isArray(params) ? params : [params]));
      if (skipValidate) {
        result = data;
      } else {
        if (status === 200 || (data && data.Code === "S")) {
          result = { data };
        } else {
          if (!data) data = { status };
          throw data;
        }
      }
    } catch (_error) {
      if (debug) console.log(`🚀 : apiCommonRequest -> error`, _error);
      result = { error: _error };
    }
    if (delay) {
      let delayTime = 0;
      let resAt = +new Date();
      let minReqDur = isBoolean(delay) ? 500 : delay;
      if ((delayTime = resAt - reqAt) < minReqDur) delayTime = minReqDur - delayTime;
      await miniTimer(delayTime);
    }
    resolve(result);
  });*/

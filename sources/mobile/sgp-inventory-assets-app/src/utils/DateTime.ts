import { default as dayjs } from "dayjs";
import "dayjs/locale/vi";
import relativeTime from "dayjs/plugin/relativeTime";
import { isEmpty } from "lodash";
import moment from "moment";
import { removeLeadingZeros } from "./Format";

dayjs.extend(relativeTime);

export const SAP_DATE_FORMAT = "YYYYMMDD";
export const SAP_TIME_FORMAT = "HHmmss";

/**
 *
 * @param date
 * @param format DD/MM/YYYY | YY.MM.DD HH:mm | YY.MM.DD | YYYY.MM.DD HH:mm:ss
 * @returns
 */
export const formatDate = (date: Date | string | number, format: string): string => {
  format =
    {
      dmy: "DD/MM/YYYY",
      "hm:dmy": "HH:mm DD/MM/YYYY",
      "dmy:hm": "DD/MM/YYYY HH:mm",
    }[format] || format;
  return moment(date).format(format);
};

const formatDateSAP = (date: string): string => {
  return moment(date).format(SAP_DATE_FORMAT);
};

const formatTimeSAP = (date: string): string => {
  return moment(date).format(SAP_TIME_FORMAT);
};

export const formatSapDateTime = (date: string, time: string) => {
  if (isEmpty(removeLeadingZeros(date))) return "";
  return dayjsFormatDate(dayjsGetDate(`${date}${time}`), "DD/MM/YYYY - HH:mm");
};

const getCurrentYear = () => {
  const d = new Date();
  return d.getFullYear();
};
const getCurrentMonth = () => {
  const d = new Date();
  return d.getMonth() + 1;
};
const getCurrentQuarter = () => {
  const m = getCurrentMonth();
  if (m === 1 || m === 2 || m === 3) return 1;
  if (m === 4 || m === 5 || m === 6) return 2;
  if (m === 7 || m === 8 || m === 9) return 3;
  if (m === 10 || m === 11 || m === 12) return 4;
  return 1;
};

const CURRENT_YEAR = getCurrentYear();
const SELECT_MONTH = 0;
const SELECT_QUARTER = 1;
const SELECT_YEAR = 2;
const SELECT_NONE = -1;

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const convertStringToDate = (str: string): string => {
  return str?.length >= 6 ? str?.replace(/(\d{4})(\d{1,2})(\d{1,2})/gi, "$1-$2-$3") : str;
};
const convertStringToTime = (str: string): string => {
  const newString = str?.length >= 6 ? str?.replace(/(\d{2})(\d{2})(\d{2})/gi, "$1:$2") : str;
  return newString;
};

export const convertStringToTimeFormat = (str: string): string => {
  return str?.length >= 6 ? str?.replace(/(\d{2})(\d{2})(\d{2})/gi, "$1:$2:$3") : str;
};

export const dayjsFormatDate = (d: any, format = "DD/MM/YYYY") => (d ? dayjs(d).format(format) : "");
export const dayjsGetDate = (d: any, format = "YYYYMMDDHHmss") => dayjs(d, format).toDate();
export const dayjsTimePassedFromNow = (d: any, format = "YYYYMMDDHHmmss") => dayjs(d, format).locale("vi").fromNow();

export default {
  CURRENT_YEAR,
  SELECT_MONTH,
  SELECT_QUARTER,
  SELECT_YEAR,
  SELECT_NONE,
  SAP_DATE_FORMAT,
  SAP_TIME_FORMAT,
  formatDate,
  getCurrentMonth,
  getCurrentQuarter,
  getCurrentYear,
  sleep,
  convertStringToDate,
  convertStringToTime,
  formatDateSAP,
  formatTimeSAP,
};

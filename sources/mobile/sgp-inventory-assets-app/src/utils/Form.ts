import moment from "moment";

/**
 *
 * @param values Object value of form
 * @param field A field of form
 * @returns Id of field
 */
const getValueSelected = (values: any, field: string) => values[field]?.id || "";

/**
 *
 * @param values Object value of form
 * @param field A field of form
 * @param format Option format date
 * @returns String date after format
 */
const getDateFormat = (values: any, field: string, format?: string): string =>
  values[field] ? moment(values[field]).format(format || "YYYYMMDD") : "";

export default {
  getValueSelected,
  getDateFormat,
};

import { heightResponsive } from "@Constants";
import { get, isEmpty, isNull, isString, isUndefined } from "lodash";
import { useEffect, useState } from "react";
import { Keyboard, KeyboardEvent } from "react-native";
import { CFile } from "../app/components/common/attach-files";

import { isArray } from "./Basic";
import { dayjsFormatDate, dayjsTimePassedFromNow } from "./DateTime";

const formatCoin = (num: number | string | null | undefined) => {
  if (isNull(num) || isUndefined(num)) return "0";
  if (isString(num)) {
    const tempNum = parseFloat(num);
    return tempNum.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }
  return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

export const truncate = (n: number) => {
  return n > 0 ? Math.floor(n) : Math.ceil(n);
};

export const useKeyboard = () => {
  const [keyboardHeight, setKeyboardHeight] = useState(32);

  function onKeyboardDidShow(e: KeyboardEvent) {
    // Remove type here if not using TypeScript
    setKeyboardHeight(e.endCoordinates.height + heightResponsive(16));
  }

  function onKeyboardDidHide() {
    setKeyboardHeight(heightResponsive(32));
  }

  useEffect(() => {
    const showSubscription = Keyboard.addListener("keyboardDidShow", onKeyboardDidShow);
    const hideSubscription = Keyboard.addListener("keyboardDidHide", onKeyboardDidHide);
    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  return keyboardHeight;
};
const acronymText = (text: any) => {
  if (!isString(text)) return "";
  let textAcr: any = text?.split(" ") || [];
  textAcr = textAcr?.map((item: string) => item[0].toUpperCase()).join("");
  return textAcr;
};

const getNameDefault = (name: any) => {
  const splitName = name?.split(" ");
  const format = splitName[splitName.length - 1]?.charAt(0) || "";
  return format;
};

const padTo2Digits = (num: number) => num.toString().padStart(2, "0");

export const convertMsToTime = (milliseconds: number) => {
  let seconds = Math.floor(milliseconds / 1000);
  let minutes = Math.floor(seconds / 60);
  const hours = Math.floor(minutes / 60);

  seconds = seconds % 60;
  minutes = minutes % 60;

  return `${padTo2Digits(hours)}:${padTo2Digits(minutes)}:${padTo2Digits(seconds)}`;
};

const getLetterName = (name: string) => {
  if (!isString(name)) return "";
  return name.split(" ")[name.split(" ").length - 1].charAt(0).toUpperCase();
};

export const convertFilterMultiValue = (values: any, defaultValue = []) =>
  isArray(values, 1) ? values.map((t: any) => get(t, "value", "")) : defaultValue;

export const convertFilterSingleValue = (item: any, defaultValue = []) => {
  const { value } = item;
  return isUndefined(value) || isNull(value) || isEmpty(value) || !isString(value) ? defaultValue : [value];
};

export const genAttachFiles = (documents?: any) => {
  if (!isArray(documents, 1)) return [];

  const isImage = (d: any) => {
    const fileName = d.fileName?.toLowerCase() || "";
    return (
      d.fileType.startsWith("image") ||
      (d.fileType === "application/octet-stream" && (fileName.includes(".heic") || fileName.includes(".jpg")))
    );
  };

  const _attachFiles = documents
    ?.map((i: any) => i.globalDocument)
    ?.map((d: any) => {
      const file: CFile = {
        type: isImage(d) ? "img" : "file",
        uuid: d.id,
        date: dayjsFormatDate(d.uploadedAt),
        size: formatBytes(d.fileSize),
        name: d.fileName,
        source: d.originalUrl,
        isRemote: true,
        file: {
          name: d.fileName,
          type: d.fileType,
          size: formatBytes(d.fileSize),
          uri: d.thumbnailUrl,
        },
      };
      return file;
    });

  return _attachFiles || [];
};

export const mappingSelections = (resp: any, { inputKey, valueKey, labelKey, format = () => null }: any = {}) => {
  let selections = isArray(resp) ? resp : get(resp, inputKey || "data.items", []);
  return selections.map((i: any) => {
    let value = i[valueKey || "code"];
    let selection = {
      value,
      label: formatObjectLabel(value, i[labelKey || "description"]),
      ...format(i),
    };
    return selection;
  });
};

export const formatObjectLabel = (code: string, description: string, haveRemoveLeadingZeros: boolean = false) => {
  if (isEmpty(code)) {
    if (isEmpty(description)) return "";
    return description;
  }
  let id = haveRemoveLeadingZeros ? removeLeadingZeros(code) : code;
  return isEmpty(description) ? id : `${description} (${id})`;
};

export const formatPriorityLabel = (type: string, priority: string, description: string) => {
  if (isEmpty(description)) return formatObjectLabel(type, priority);
  return `${description} (${type}-${priority})`;
};

export const formatWorkCenterLabel = (code: string, plant: string, description: string) => {
  const objectLabel = formatObjectLabel(code, description);
  if (isEmpty(plant)) return objectLabel;
  return `${plant} - ${objectLabel}`;
};

export const formatNotificationItem = (i: any) => {
  return {
    notifPart: {
      label: formatObjectLabel(i.part, i.partDesc),
      value: i.part,
    },
    notifPartCode: {
      label: formatObjectLabel(i.partCode, i.partCodeDesc),
      value: i.partCode,
    },
    notifProblem: {
      label: formatObjectLabel(i.problem, i.problemDesc),
      value: i.problem,
    },
    notifDame: {
      label: formatObjectLabel(i.dame, i.dameDesc),
      value: i.dame,
    },
    notifCause: {
      label: formatObjectLabel(i.cause, i.causeDesc),
      value: i.cause,
    },
    notifCauseCode: {
      label: formatObjectLabel(i.causeCode, i.causeCodeDesc),
      value: i.causeCode,
    },
    notifCauseText: i.causeText || "",
    notifText: i.idText || "",
  };
};

export const formatNotificationItemInfo = (item: any) => {
  const result = [
    ["notifPart", "Nhóm bộ phận"],
    ["notifPartCode", "Bộ phận"],
    ["notifProblem", "Phân loại hư hỏng"],
    ["notifDame", "Phân loại hư hỏng chi tiết"],
    ["notifCause", "Nhóm nguyên nhân lỗi "],
    ["notifCauseCode", "Nguyên nhân lỗi chi tiết"],
  ].map((i: any) => ({
    label: i[1],
    value: item[i[0]]?.label,
  }));
  const texts = [
    ["notifCauseText", "Mô tả nguyên nhân lỗi"],
    ["notifText", "Mô tả chi tiết"],
  ].map((i: any) => ({
    label: i[1],
    value: item[i[0]],
  }));
  return [...result, ...texts];
};

export const formatNotificationListItem = (item: any) => {
  if (!item) return;
  let {
    id,
    description,
    workCenter,
    equipmentId,
    equipmentDescription = "",
    functionalLocationId,
    notificationDate,
    notificationTime,
    commonStatus,
    priorityType,
    notificationTypeDesc = "",
    functionalLocationDesc = "",
    reportedByName,
    assignName,
  } = item;

  let getTagColor = (color: any) => {
    if (color[0] !== "#") color = "#" + color;
    return color;
  };

  let prorityColorCode = get(priorityType, "colorCode", "#FD4848");
  let prorityLabel = get(priorityType, "description", "");
  let statusColorCode = get(commonStatus, "colorCode", "#FD4848");
  let statusLabel = get(commonStatus, "description", "");

  const notificationCode = removeLeadingZeros(id);

  return {
    id,
    notificationCode,
    title: description,
    workCenter: workCenter,
    equipment: formatObjectLabel(equipmentId, equipmentDescription, true),
    location: formatObjectLabel(functionalLocationId, functionalLocationDesc),
    createdBy: reportedByName,
    assignTo: assignName,
    createdAt: dayjsFormatDate(notificationDate),
    timePassedFromNow: dayjsTimePassedFromNow(`${notificationDate}${notificationTime}`),
    tags: [
      { color: getTagColor(prorityColorCode), label: prorityLabel },
      { color: getTagColor(statusColorCode), label: statusLabel },
      { color: "#3A73B7", label: notificationTypeDesc },
    ],
  };
};

export const toString = (arr?: Array<string>) => (isArray(arr, 1) ? arr?.join(",") : "");

export const formatBytes = (a: any, b?: any, space?: any) => {
  if (0 == a) return "0 Bytes";
  let c = 1024,
    d = b || 2,
    e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    f = Math.floor(Math.log(a) / Math.log(c));
  return parseFloat((a / Math.pow(c, f)).toFixed(d)) + (space ? " " : "") + e[f];
};

export const removeLeadingZeros = (str?: string) => {
  return str ? str.replace(/^0+/, "") : "";
};

export default {
  formatCoin,
  useKeyboard,
  acronymText,
  getNameDefault,
  getLetterName,
};

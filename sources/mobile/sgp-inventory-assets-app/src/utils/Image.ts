import { String as Strings } from "@Constants";
import { CommonServices } from "../services";
import moment from "moment";

type IImage = {
  imageid: string;
  type: string;
};

const uploadImage = async (images: IImage[]) => {
  const allResponse = await Promise.all(
    images.map(({ imageid, type }: IImage, idx: number) =>
      CommonServices.uploadImage({
        name: `${moment().valueOf()}x` + idx,
        body: {
          mode: "file",
          file: {
            src: imageid,
            type,
          },
        },
      })
    )
  );
  return allResponse || Strings.error;
};

export default { uploadImage };

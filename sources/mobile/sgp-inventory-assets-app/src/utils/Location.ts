import { Platform } from "react-native";
import Geolocation from "react-native-geolocation-service";
import { openSettings, PERMISSIONS, request, RESULTS } from "react-native-permissions";

import { truncate } from "./Format";

const getDMS = (dd: number, longOrLat: "long" | "lat") => {
  const hemisphere = /^[WE]|(?:lon)/i.test(longOrLat) ? (dd < 0 ? "W" : "E") : dd < 0 ? "S" : "N";

  const absDD = Math.abs(dd);
  const degrees = truncate(absDD);
  const minutes = truncate((absDD - degrees) * 60);
  const seconds = ((absDD - degrees - minutes / 60) * Math.pow(60, 2)).toFixed(2);

  const dmsArray = [degrees, minutes, seconds, hemisphere];
  return `${dmsArray[0]}°${dmsArray[1]}'${dmsArray[2]}"${dmsArray[3]}`;
};

const getFullDMS = (lat: number, long: number): string => {
  if (!lat || !long) return "";
  const latDMS = getDMS(lat, "lat");
  const longDMS = getDMS(long, "long");
  return `${latDMS} ${longDMS}`;
};

const openSettingLocation = () => openSettings().catch(() => console.warn("Unable to open settings"));

const hasPermissionIOS = async () => {
  const perLocation = await Geolocation.requestAuthorization("whenInUse");
  if (perLocation === RESULTS.GRANTED) {
    return true;
  } else {
    return false;
  }
};

const hasPermissionAndroid = async () => {
  try {
    const perLocation = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    if (perLocation === RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err: any) {
    return false;
  }
};

const hasLocationPermission = async () => {
  if (Platform.OS === "ios") {
    const hasPermission = await hasPermissionIOS();
    return hasPermission;
  } else if (Platform.OS === "android") {
    const hasPermission = await hasPermissionAndroid();
    return hasPermission;
  }
};
const isLatitude = (num: any) => isFinite(Number(num)) && Math.abs(Number(num)) <= 90;
const isLongitude = (num: any) => isFinite(Number(num)) && Math.abs(Number(num)) <= 180;

export default {
  openSettingLocation,
  isLatitude,
  isLongitude,
  getFullDMS,
  getDMS,
  hasPermissionAndroid,
  hasPermissionIOS,
  hasLocationPermission,
};

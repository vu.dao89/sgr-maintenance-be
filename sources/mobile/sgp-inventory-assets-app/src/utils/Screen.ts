import { Keyboard } from "react-native";
import { DrawerActions } from "@react-navigation/native";

export const changeScreen = (_this: any, screen?: string, params?: any) => {
  try {
    Keyboard.dismiss();
    if (!_this) return;
    let _navigation = _this.navigation || _this.props.navigation;
    switch (screen) {
      case "openDrawer":
        _navigation.dispatch(DrawerActions.openDrawer());
        break;
      case "closeDrawer":
        _navigation.dispatch(DrawerActions.closeDrawer());
        break;
      case "toggleDrawer":
        _navigation.dispatch(DrawerActions.toggleDrawer());
        break;
      case "back":
        _navigation.goBack();
        break;
      default:
        if (!screen) {
          _navigation.goBack();
        } else {
          _navigation.navigate(screen, params);
        }
        break;
    }
  } catch {}
};

export const getScreenParams = (_this: any) => (_this.route || _this.props.route || {}).params || {};

export type ISearctItem = {
  id: string;
  label: string;
};

const searchItems = (items: ISearctItem[], keySearch: string): ISearctItem[] => {
  return (items && items.filter(e => e.label?.includes(keySearch))) || [];
};

export default {
  searchItems,
};

import invertColor from "invert-color";

export const getInvertColor = (color: any, options?: any) => invertColor(color, options);

/**
 * @param {string=} uriString - uri string from internet
 * @return boolean
 */
const isImageUri = (uriString: string) => {
  return /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|jpeg|jpe|jif|jfif|jfi|gif|png|webp|tiff|tif|svg|svgz)/g.test(uriString);
};

export default {
  isImageUri,
};

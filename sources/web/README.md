This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Link dev: [https://veek-sapp-fe.vercel.app/](https://veek-sapp-fe.vercel.app/)

Link local: [http://localhost:3000](http://localhost:3000)

## Git rules

### Branch name:

- feat/TASK-111-do-something
- fix/TASK-111-do-something
- hotfix/TASK-111-do-something

### Commit message

- [TASK-111] Integrate login API

### PR title:

- feat/TASK-111: Do-something
- fix/TASK-111: Do-something
- hotfix/TASK-111: Do-something

### Notes:

-Use rebase with autosquash (Don’t use merge commit)

```
# Use bash
$ git checkout develop && git pull
$ git checkout {your_branch}
$ git rebase -i --autosquash develop
# An editor will be shown. Please change "pick" to "s" excepting the first commit then save and quit editor.
$ git push -f

# Use zsh
$ gco develop && gl
$ gco {your_branch}
$ grbi --autosquash develop
$ gp -f
```

## Workflow:

- Basic smooth flow: `OPEN -> TODAY SELECTED -> IN PROGRESS -> REVIEW -> QA -> ACCEPTED -> DONE`
- `BLOCKED`: It’s being blocked by one or some other tasks. It should have the linked issue.
- `REJECTED`: IF the task is already deployed to develop but it’s not working well, THEN QA guy will move it to `REJECTED` column.
- `Important`: Every day when you start working, the first thing you MUST do is take a look from RIGHT to LEFT on Board:
  - For DEV: You should have a look from `REJECTED` column, then `REVIEW`, `IN PROGRESS`, `BLOCKED`, `TODO` as finally. It means DEVs are only responsible from column `REJECTED` back to the left.
  - For QA: Focus on only `QA` & `ACCEPTED` columns. As I mentioned above, IF a task is not matching with Acceptance Test criterial, THEN move it to `REJECTED`, otherwise move to `ACCEPTED` then assign to Martin (PM)
- `QA` will start writing the Acceptance Test Criterial 2 days before the new version is started (planning)

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

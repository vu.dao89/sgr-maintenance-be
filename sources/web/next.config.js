/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  clientBrowserSourceMaps: true,
  productionBrowserSourceMaps: false,
  output: 'standalone',
  publicRuntimeConfig: {
    NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL
  },
  basePath: '/iss/assetmanagement',
  images: {
    domains: ['http://localhost:8000/iss/assetmanagement', 'https://devmysgr.sungroup.com.vn/iss/assetmanagement']
  }
};

module.exports = nextConfig;

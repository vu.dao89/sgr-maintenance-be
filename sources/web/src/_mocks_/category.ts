export const CATEGORY: IOption[] = [
  { label: 'Sun EQ - Thiết bị chính', value: '1' },
  { label: 'Construction machinery', value: '2' },
  { label: 'Sun EQ - Xe của cư dân', value: '3' },
  { label: 'GG Equip Category (IS-HT-SW)', value: '4' },
  { label: 'Machines', value: '5' },
  { label: 'Production resources/tools', value: '6' },
  { label: 'Test/measurement equipment', value: '7' },
  { label: 'Customer equipment', value: '8' },
  { label: 'Thiết bị lv1 hoặc độc lập (TS)', value: '9' }
];

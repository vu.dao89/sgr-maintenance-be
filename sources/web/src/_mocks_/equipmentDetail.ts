import { IDataEquipmentTab, IEquipmentInfo } from '@modules/Equipment';

export const equipmentInfo: IEquipmentInfo = {
  id: 1,
  name: 'Nhà trạm biến áp số 2 (NT)',
  image: '/equipment_example.png',
  description:
    'Căn cứ tình hình hoạt động của Sun Plaza Ancora. Ban MKT Tập Đoàn kính trình Tổng Giám Đốc phê duyệt. Căn cứ tình hình hoạt động của  Căn cứ tình hình hoạt động của Sun Plaza Ancora. Ban MKT Tập Đoàn kính trình Tổng Giám Đốc phê duyệt. Căn cứ tình hình hoạt động của  Căn cứ tình hình hoạt động của Sun Plaza Ancora. Ban MKT Tập Đoàn kính trình Tổng Giám Đốc phê duyệt. Căn cứ tình hình hoạt động của.',
  location: 'Khu vực hạ tầng trạm biến áp số 2'
};

export const equipmentTab: IDataEquipmentTab[] = [
  {
    id: 1,
    name: 'Tổng quan',
    files: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      }
    ],
    items: [
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      }
    ]
  },
  {
    id: 2,
    name: 'Điểm đo',
    files: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      }
    ],
    items: [
      {
        name: 'Thông tin thiết bị 1',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      }
    ]
  },
  {
    id: 3,
    name: 'Đặc tính',
    files: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      }
    ],
    items: [
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      }
    ]
  },
  {
    id: 4,
    name: 'Phân cấp',
    treeline: {
      id: 1,
      name: 'Cơ sở hạ tầng',
      level: 1,
      child: [
        {
          id: 2,
          name: 'Nhà hàng buffet',
          level: 2,
          child: [
            {
              id: 3,
              name: 'Khu vực hạ tầng trạm biến áp số 2',
              level: 3,
              child: [
                {
                  id: 4,
                  name: 'Đèn khẩn cấp',
                  level: 4
                },
                {
                  id: 5,
                  name: 'Điều hòa',
                  level: 4
                },
                {
                  id: 6,
                  name: 'Nhà trạm biến áp số 2 (NT)',
                  level: 4
                },
                {
                  id: 7,
                  name: 'Hệ thống chiếu sáng',
                  level: 4
                },
                {
                  id: 8,
                  name: 'Điều hòa',
                  level: 4
                }
              ]
            },
            {
              id: 9,
              name: 'Kết cấu hạ tầng',
              level: 3
            },
            {
              id: 10,
              name: 'Thiết bị phụ trợ',
              level: 3
            }
          ]
        },
        {
          id: 11,
          name: 'Thiệt bị phụ tr',
          level: 2
        }
      ]
    }
  },
  {
    id: 5,
    name: 'Tài liệu',
    files: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      }
    ],
    items: [
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin bổ sung',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      },
      {
        name: 'Thông tin thiết bị',
        items: [
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          },
          {
            label: 'Loại thiết bị',
            value: 'Nooi dung'
          }
        ]
      }
    ]
  }
];

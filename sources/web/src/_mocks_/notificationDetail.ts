export const notificationDetail: any = {
  id: '1',
  code: '2023',
  image: '/equipment_example.png',
  notifDes: 'Sửa tàu lượn Cobra',
  notifReportBy: 'Theresa Web',
  assignTo: 'Theresa Web',
  eqStatId: 'Đã nhận',
  notifDate: 'Feb 15, 2020',
  reqStart: '20/10/2022',
  reqEnd: '25/10/2022',
  functionalLocation: 'Khu vực hạ tầng trạm biến áp số 2 (B003-CVR)',
  preview: [
    {
      id: 1,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Ngày phát hiện sự cố',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Yêu cầu xử vào ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 2,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Ngày phát hiện sự cố',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Yêu cầu xử vào ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 3,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Từ ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 4,
      title: 'Hành động 1',
      items: [
        {
          label: 'Nhóm khắc phục',
          value: 'Máy móc - tự sửa chữa (0120)'
        },
        {
          label: 'Chi tiết khắc phục',
          value: 'Máy móc - tự sửa chữa (0120)'
        },
        {
          label: 'Mô tả chi tiết',
          value: 'Nội dung'
        }
      ]
    }
  ],
  detail: [
    {
      id: 1,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Ngày phát hiện sự cố',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Yêu cầu xử vào ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 2,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Ngày phát hiện sự cố',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Yêu cầu xử vào ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 3,
      title: 'Thời gian yêu cầu xử lý',
      items: [
        {
          label: 'Từ ngày',
          value: '24/11/2022 14:00'
        },
        {
          label: 'Đến ngày',
          value: '24/11/2022 14:00'
        }
      ]
    },
    {
      id: 4,
      title: 'Hành động 1',
      items: [
        {
          label: 'Nhóm khắc phục',
          value: 'Máy móc - tự sửa chữa (0120)'
        },
        {
          label: 'Chi tiết khắc phục',
          value: 'Máy móc - tự sửa chữa (0120)'
        },
        {
          label: 'Mô tả chi tiết',
          value: 'Nội dung'
        }
      ]
    }
  ],
  action: [
    {
      id: 1,
      title: 'Sửa tàu lượn Cobra (50004835)',
      label: 'Làn trượt samurai phải (alr)',
      asignTo: 'Giao cho: Trần Trung Tín',
      startDate: '20/11/2022',
      endDate: '24/11/2022',
      chip1: 'KHẨN CẤP',
      chip2: 'Đã nhận',
      chip3: 'PM01',
      image: '/equipment_example.png',
      time: '17 tiếng trước'
    }
  ],
  document: {
    files: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.pdf',
        size: '26Kb',
        createAt: '27/07/2021'
      }
    ],
    items: [
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.jpg',
        size: '26Kb',
        createdAt: '24/10/2022',
        image: '/equipment_example.png'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.jpg',
        size: '26Kb',
        createdAt: '24/10/2022',
        image: '/equipment_example.png'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.jpg',
        size: '26Kb',
        createdAt: '24/10/2022',
        image: '/equipment_example.png'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.jpg',
        size: '26Kb',
        createdAt: '24/10/2022',
        image: '/equipment_example.png'
      },
      {
        name: 'Ho so da duoc Ban CNTT tham dinh.jpg',
        size: '26Kb',
        createdAt: '24/10/2022',
        image: '/equipment_example.png'
      }
    ]
  }
};

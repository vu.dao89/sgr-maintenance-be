export const OBJECT_TYPE: IOption[] = [
  { label: 'BUI-Thiết bị cha', value: 'BUI_BUI_00' },
  { label: 'BUI-Hệ gỗ', value: 'BUI_BUI_012' },
  { label: 'BUI-Hệ sắt thép', value: 'BUI_BUI_02' },
  { label: 'BUI-Hệ sàn', value: 'BUI_BUI_03' },
  { label: 'BUI-Hệ trần/mái', value: 'BUI_BUI_04' },
  { label: 'BUI-Hệ tường (gạch)', value: 'BUI_BUI_05' }
];

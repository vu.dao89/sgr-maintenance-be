export const PLANER_GROUP: IOption[] = [
  { label: 'Plan group 1', value: '1' },
  { label: 'Plan group 2', value: '2' },
  { label: 'Plan group 3', value: '3' }
];

import Axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { redirect, toCamelCase } from 'common/helpers';
import { getToken } from './storage';

export const axiosConfigs: AxiosRequestConfig = {
  baseURL: process.env.NEXT_PUBLIC_DEV_API_URL || process.env.NEXT_PUBLIC_LOCAL_API_URL,
  timeout: 50000,
  headers: {
    'Content-type': 'application/json',
    Accept: 'application/json'
  }
};

const createInstance = (config?: AxiosRequestConfig): AxiosInstance => {
  return Axios.create(config ? { ...axiosConfigs, ...config } : axiosConfigs);
};

const apiService: AxiosInstance = createInstance();

apiService.interceptors.request.use(
  (config: any) => {
    const token = getToken();

    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }

    return Promise.resolve(config);
  },
  (error: AxiosError): Promise<Error> => {
    return Promise.reject(error);
  }
);

// interceptors response
apiService.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse => {
    return toCamelCase(response);
  },
  (error: AxiosError) => {
    if (error?.code === 'ERR_NETWORK') {
      return redirect('401');
    }

    return error?.response;
  }
);

// class ServiceSingleton {
//   private static instance: AxiosInstance;

//   /**
//    * Singleton's constructor must be private
//    */
//   private constructor() {}

//   // getInstance ==============================================================
//   public static getInstance(): AxiosInstance {
//     if (!ServiceSingleton.instance) {
//       ServiceSingleton.instance = Axios.create(axiosConfigs);
//       ServiceSingleton.setupAxiosInterceptors();
//     }
//     return ServiceSingleton.instance;
//   }

//   // setupAxiosInterceptors ====================================================
//   private static setupAxiosInterceptors() {
//     // interceptors request
//     ServiceSingleton.instance.interceptors.request.use(
//       (config: any) => {
//         const token = getToken();
//         if (token) {
//           config.headers['Authorization'] = `Bearer ${token}`;
//         }

//         config.headers['Content-Type'] = 'application/json';
//         config.headers['Accept'] = 'application/json';
//         // config.headers['Content-Type'] = 'multipart/form-data';
//         // config.headers['Accept'] = '*/*';

//         // if (!isEmpty(config.data)) {
//         //   config.data = toSnakeCase(config.data);
//         // }

//         // if (isEmpty(config.params)) {
//         //   config.params = toSnakeCase(config.params);
//         // }

//         return Promise.resolve(config);
//       },
//       (error: AxiosError): Promise<Error> => {
//         return Promise.reject(error);
//       }
//     );

//     // interceptors response
//     ServiceSingleton.instance.interceptors.response.use(
//       (response: AxiosResponse): AxiosResponse => {
//         return toCamelCase(response);
//       },
//       (error: AxiosError) => {
//         if (error?.response?.status === 401) {
//           return redirect('/401');
//         }

//         return error?.response;
//       }
//     );
//   }
// }

// const apiService = ServiceSingleton.getInstance();
export { apiService };

import { RootState } from 'common';
import { Dispatch } from 'redux';

// Type
declare global {
  type ThunkAPIConfig = {
    dispatch: Dispatch;
    state: RootState;
    extra: any;
    rejectValue: {
      errorMessage: string;
    };
  };
  type TCurrentRequest = string | undefined;
  type TLoading = 'idle' | 'pending';
  type TSortBy = 'acs' | 'desc';
}

// Interface
declare global {
  interface Window {}

  interface TColumn {
    [key as any]: string;
  }

  interface TFilter {
    [key as any]: IOption;
  }

  interface IOption {
    label: string;
    value: string;
  }

  interface IInput {
    label: string;
    value: number | string;
  }

  interface IPaginationReq {
    page: number;
    size: number;
  }
  interface IPagination {
    totalItems?: number;
    totalPages?: number;
    size: number;
    page: number;
  }

  interface IBaseEntity {
    id?: string | number;
  }
}

import { localStorageCache } from './handle';

// - Token ------------------------------------------------
export const getToken = () =>
  localStorageCache.retrieve(process.env.NEXT_PUBLIC_TOKEN_KEY || 'TOKEN_AZURE') || process.env.NEXT_PUBLIC_TOKEN;
export const removeToken = () => localStorageCache.remove(process.env.NEXT_PUBLIC_TOKEN_KEY || 'TOKEN_AZURE');
export const setToken = (data: { token: string; expiredIn?: number }) =>
  localStorageCache.store(process.env.NEXT_PUBLIC_TOKEN_KEY || 'TOKEN_AZURE', data.token, data.expiredIn!!);

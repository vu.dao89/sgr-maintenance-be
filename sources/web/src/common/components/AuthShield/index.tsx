import { userActions } from '@modules/User';
import { removeToken, useAppDispatch } from 'common/app';
import { NextShield } from 'next-shield';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { Loading } from '../Loading';

const PRIVATE_ROUTER = [
  '/equipment',
  '/equipment/[id]',
  '/notification',
  '/notification/[id]',
  '/maintenance',
  '/maintenance/[id]',
  '/work-order',
  '/work-order/[id]',
  '/components',
  '/user',
  '/logout'
];

interface IAuthShieldProps {
  children: React.ReactNode;
}

export default function AuthShield({ children }: IAuthShieldProps) {
  const router = useRouter();
  const dispatch = useAppDispatch();

  /**
   * Check token from url
   */
  useEffect(() => {
    const requestUser = async () => {
      await dispatch(userActions.getUserInfo())
        .unwrap()
        .catch(() => removeToken());
    };

    requestUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  return (
    <NextShield
      isAuth={true}
      isLoading={false}
      router={router}
      privateRoutes={PRIVATE_ROUTER}
      publicRoutes={['/']}
      accessRoute={'/equipment'}
      loginRoute={'/'}
      LoadingComponent={<Loading type="icon" />}
    >
      {children}
    </NextShield>
  );
}

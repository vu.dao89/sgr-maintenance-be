import {
  Box,
  BoxProps,
  Card,
  CardContent,
  CardMedia,
  Collapse,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Tab,
  Tabs
} from '@mui/material';
import { Icon } from 'common/components';
import React, { useState } from 'react';
import { CardHeading } from '../CardHeading';

interface DataLeftCardProp extends BoxProps {
  children: React.ReactNode | string;
  image?: string;
}

export const LeftCardContent: React.FC<DataLeftCardProp> = ({ image, children }) => {
  return (
    <Card>
      <CardContent sx={{ p: 4 }}>
        <CardMedia sx={{ borderRadius: 2, objectFit: 'cover' }} component="img" height="240" image={image} />
        {children}
      </CardContent>
    </Card>
  );
};

// --------------------------------------------------------

export interface ITabHeading {
  name?: string;
}

interface DataRightCardProp extends BoxProps {
  children?: React.ReactNode | string;
  tabs?: any;
  currentTab?: number;
  handleChange?: any;
}

//TODO refactor after has api
export const RightCardContent: React.FC<DataRightCardProp> = ({ children, tabs, currentTab, handleChange }) => {
  return (
    <Stack>
      <Tabs aria-label="tabs" onChange={handleChange} value={currentTab}>
        {Object.keys(tabs).map((item, index) => (
          <Tab key={index} disableRipple label={tabs[item]} className="capitalize" />
        ))}
      </Tabs>
      {children}
    </Stack>
  );
};

// --------------------------------------------------------

interface CardItemProp extends BoxProps {
  title?: string;
  children?: React.ReactNode | string;
  className?: string;
  collapsible?: boolean;
  clearable?: boolean;
  action?: boolean;
  handleEdit?: (data?: any) => void;
  handleRemove?: (data?: any) => void;
  data?: any;
}

export const CardItem: React.FC<CardItemProp> = ({
  title,
  action,
  children,
  collapsible = true,
  clearable = false,
  handleEdit,
  handleRemove,
  data
}) => {
  const [open, setOpen] = useState<boolean>(true);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openMenu = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <Box display="flex" justifyContent="space-between" alignContent="center">
          {title && (
            <CardHeading
              open={open}
              collapsible={collapsible}
              clearable={clearable}
              handleCollapse={() => setOpen(!open)}
              handleClear={handleRemove}
            >
              {title}
            </CardHeading>
          )}
          {action && (
            <Box>
              <IconButton
                sx={{
                  width: 40,
                  height: 40,
                  borderRadius: '50%',
                  '&:hover, &:active': {
                    bgcolor: 'grey.600'
                  },
                  bgcolor: openMenu ? 'grey.600' : ''
                }}
                id="action-card"
                aria-controls={openMenu ? 'action' : undefined}
                aria-haspopup="true"
                aria-expanded={openMenu ? 'true' : undefined}
                onClick={handleClick}
                disableRipple
              >
                <Icon name="more" width={14} height={14} />
              </IconButton>
              <Menu
                id="action-card"
                anchorEl={anchorEl}
                open={openMenu}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right'
                }}
                transformOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right'
                }}
                MenuListProps={{
                  'aria-labelledby': 'action-card'
                }}
              >
                {handleEdit ? (
                  <MenuItem
                    onClick={() => {
                      handleEdit(data);
                      setAnchorEl(null);
                    }}
                    sx={{ pl: 6, pr: 12 }}
                  >
                    Sửa
                  </MenuItem>
                ) : null}
                {handleRemove ? (
                  <MenuItem
                    onClick={() => {
                      handleRemove(data);
                      setAnchorEl(null);
                    }}
                    sx={{ pl: 6, pr: 12 }}
                  >
                    Xóa
                  </MenuItem>
                ) : null}
              </Menu>
            </Box>
          )}
        </Box>
        <Collapse in={open}>{children}</Collapse>
      </CardContent>
    </Card>
  );
};

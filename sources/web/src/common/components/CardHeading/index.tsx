import ClearIcon from '@mui/icons-material/Clear';
import { Box, BoxProps, IconButton, Typography } from '@mui/material';
import { isString } from 'lodash';
import Image from 'next/image';
import React from 'react';

interface ICardHeadingProps extends BoxProps {
  children: React.ReactNode | string;
  className?: string;
  open?: boolean;
  handleCollapse?: () => void;
  handleClear?: () => any;
  collapsible?: boolean;
  clearable?: boolean;
}

export const CardHeading: React.FC<ICardHeadingProps> = ({
  children,
  className,
  open = true,
  handleCollapse,
  handleClear,
  collapsible = true,
  clearable = false,
  ...props
}) => {
  return isString(children) ? (
    <Box className="flex justify-between" width="100%" alignItems="center" {...props}>
      <Box className="flex" alignItems="center">
        {collapsible && (
          <Image
            src={
              open ? '/iss/assetmanagement/static/icons/collapse.svg' : '/iss/assetmanagement/static/icons/expand.svg'
            }
            alt="Icon"
            width={44}
            height={44}
            className="ml-[-10px] cursor-pointer"
            onClick={handleCollapse}
          />
        )}
        <Typography variant="h5" ml={1}>
          {children}
        </Typography>
      </Box>
      {clearable && (
        <IconButton onClick={handleClear}>
          <ClearIcon
            sx={{
              '&:hover, &:active': {
                bgcolor: 'grey.600'
              }
            }}
          />
        </IconButton>
      )}
    </Box>
  ) : (
    <>{children}</>
  );
};

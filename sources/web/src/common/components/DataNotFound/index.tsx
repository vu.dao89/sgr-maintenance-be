import DescriptionIcon from '@mui/icons-material/Description';
import { BoxProps, Stack, Typography } from '@mui/material';
import { TEXT_DATA_NOT_FOUND } from 'common/constants';
import { themes } from 'common/themes';
import React, { Fragment } from 'react';

export interface DataNotFoundProps extends BoxProps {
  text?: string;
  showIcon?: boolean;
  className?: string;
}

export const DataNotFound: React.FC<DataNotFoundProps> = ({
  text = TEXT_DATA_NOT_FOUND,
  showIcon = false,
  className,
  ...props
}) => {
  return (
    <Fragment>
      {showIcon ? (
        <Stack alignItems="center" py={8} className={className} {...props}>
          <DescriptionIcon sx={{ fontSize: 40, color: themes.palette.grey[100] }} />
          <Typography color="GrayText">{text}</Typography>
        </Stack>
      ) : (
        <Typography className={className}>{text}</Typography>
      )}
    </Fragment>
  );
};

import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import { DataTableHeader } from './DataTableHeader';
import { DataTableRow } from './DataTableRow';

export interface ICellType {
  type: 'text' | 'label' | 'image' | 'icon';
  color?: string;
  bgColor?: string;
  value?: string;
}

export interface IColumnType<T> {
  key: string;
  title: string;
  width?: number;
  enable?: boolean;
  required?: boolean;
  type?: (column: IColumnType<T>, item: T) => ICellType;
  render?: (column: IColumnType<T>, item: T) => void;
}

interface Props<T> {
  data: T[];
  columnId?: string; // Define id using for deltail and delete action
  columns: IColumnType<T>[];
  hasActionColumn?: boolean;
  canViewDetail?: boolean;
  canDelete?: boolean;
  url?: string;
  noDataText?: string;
  handleDelete?: (value: any) => any;
}

export function DataTable<T extends IBaseEntity>({
  data,
  columnId = 'id',
  columns,
  hasActionColumn = false,
  canViewDetail = false,
  canDelete = false,
  url = '',
  noDataText = 'No data',
  handleDelete
}: Props<T>): JSX.Element {
  return (
    <Table sx={{ minWidth: 1400 }} aria-label="simple table">
      <TableHead>
        <DataTableHeader columns={columns} hasActionColumn={hasActionColumn} />
      </TableHead>
      <TableBody>
        {!data.length ? (
          <TableRow>
            <TableCell
              colSpan={hasActionColumn ? columns.length + 1 : columns.length}
              sx={{ p: 4, textAlign: 'center' }}
            >
              <Typography>{noDataText}</Typography>
            </TableCell>
          </TableRow>
        ) : (
          <DataTableRow
            data={data}
            columnId={columnId}
            columns={columns}
            hasActionColumn={hasActionColumn}
            canViewDetail={canViewDetail}
            canDelete={canDelete}
            url={url}
            handleDelete={handleDelete}
          />
        )}
      </TableBody>
    </Table>
  );
}

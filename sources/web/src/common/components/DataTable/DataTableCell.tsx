import { Box, CardMedia, Typography } from '@mui/material';
import TableCell from '@mui/material/TableCell';
import { get } from 'lodash';
import { Icon, TIcon } from '../Icon';

import { ICellType, IColumnType } from './index';

interface Props<T> {
  item: T;
  column: IColumnType<T>;
}

const renderItem = (cellType: ICellType, value: any) => {
  if (typeof value !== 'string') {
    return '';
  }

  switch (cellType?.type) {
    case 'label':
      return (
        <Typography
          textAlign="center"
          color="white"
          variant="body2"
          sx={{ p: '4px 10px', backgroundColor: `#${cellType?.color}` || '#FFF', borderRadius: '6px' }}
        >
          {value || '-'}
        </Typography>
      );
    case 'image':
      return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <CardMedia sx={{ borderRadius: 0.5, width: 24, height: 24 }} component="img" image={cellType?.value} />
          <Typography ml={3}>{value || '-'}</Typography>
        </Box>
      );
    case 'icon':
      return (
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <Icon name={cellType?.value as TIcon} width={24} height={24} />
          <Typography ml={3}>{value || '-'}</Typography>
        </Box>
      );
    default:
      return <Typography>{value || '-'}</Typography>;
  }
};

export function DataTableCell<T extends IBaseEntity>({ item, column }: Props<T>): JSX.Element {
  const value = typeof column?.render === 'function' ? column.render(column, item) : get(item, column.key);
  const type: ICellType = column?.type ? column.type(column, item) : { type: 'text' };

  return (
    <TableCell key={`table-cell-${item.id}`} width={type.type === 'label' ? 280 : 'auto'}>
      {renderItem(type, value)}
    </TableCell>
  );
}

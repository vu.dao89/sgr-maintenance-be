import { TableCell, TableRow, useTheme } from '@mui/material';
import { IColumnType } from './DataTable';

interface Props<T> {
  columns: IColumnType<T>[];
  hasActionColumn?: boolean;
  canViewDetail?: boolean;
  canDelete?: boolean;
}

export function DataTableHeader<T>({ columns, hasActionColumn = true }: Props<T>): JSX.Element {
  const theme = useTheme();

  return (
    <TableRow className="uppercase" sx={{ backgroundColor: theme.palette.primary.light }}>
      {columns.map(
        (column, columnIndex) =>
          column.enable && (
            <TableCell
              key={`table-head-cell-${columnIndex}`}
              sx={{ width: column?.width || 'auto', fontSize: 12, borderLeft: '2px solid white' }}
            >
              {column.title}
            </TableCell>
          )
      )}
      {hasActionColumn && <TableCell sx={{ width: 110, fontSize: 12, borderLeft: '2px solid white' }} />}
    </TableRow>
  );
}

import { IconButton, styled, TableCell, TableRow, Tooltip, tooltipClasses, TooltipProps } from '@mui/material';
import { useRouter } from 'next/router';
import { Icon } from '../Icon';
import { IColumnType } from './DataTable';
import { DataTableCell } from './DataTableCell';

const BootstrapTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} arrow classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: theme.palette.common.black
  },
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.black
  }
}));

interface Props<T extends IBaseEntity> {
  data: T[];
  columnId?: string; // Define id using for deltail and delete action
  columns: IColumnType<T>[];
  hasActionColumn?: boolean;
  canViewDetail?: boolean;
  canDelete?: boolean;
  url: string;
  handleDelete?: (value: any) => any;
}

export function DataTableRow<T extends IBaseEntity>({
  data,
  columnId = 'id',
  columns,
  hasActionColumn = true,
  canViewDetail = true,
  canDelete = false,
  url = '',
  handleDelete
}: Props<T>): JSX.Element {
  const router = useRouter();

  return (
    <>
      {data.map((item: any, itemIndex) => {
        const fullActionField = canDelete && typeof handleDelete === 'function' && canViewDetail;
        return (
          <TableRow key={itemIndex} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            {columns.map(
              (column, columnIndex) =>
                column.enable && (
                  <DataTableCell key={`table-row-cell-${item.id}-${columnIndex}`} item={item} column={column} />
                )
            )}
            {hasActionColumn && (
              <TableCell align="center" width={fullActionField ? 170 : 120}>
                {canViewDetail && (
                  <BootstrapTooltip placement="top" title="Xem chi tiết">
                    <IconButton
                      onClick={() => {
                        url && item[columnId] ? router.push(`${url}/${item[columnId]}`) : console.log('wrong url');
                      }}
                    >
                      <Icon name="eye" width={18} height={18} />
                    </IconButton>
                  </BootstrapTooltip>
                )}
                {canDelete && typeof handleDelete === 'function' && (
                  <BootstrapTooltip placement="top" title="Xóa">
                    <IconButton onClick={() => handleDelete(item.id)}>
                      <Icon name="delete" width={18} height={18} />
                    </IconButton>
                  </BootstrapTooltip>
                )}
              </TableCell>
            )}
          </TableRow>
        );
      })}
    </>
  );
}

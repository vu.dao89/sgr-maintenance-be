import { Box, BoxProps, Card, CardMedia, CircularProgress, IconButton, Stack, Typography } from '@mui/material';
import { Icon } from 'common/components';
import { IGlobalDocument } from 'common/dto';
import { convertStringToDateTime } from 'common/helpers';
import Image from 'next/image';

const handleName = (name: string) => {
  const arName = name.split('.');
  if (!arName.length) return name;

  const newName = arName.slice(0, -1).join('');
  const typeFile = arName[arName.length - 1];

  return `${newName.slice(0, 8)}...${newName.slice(-8)}.${typeFile}`;
};

export type TFileUpload = {
  name?: string;
  size?: string;
  createAt?: string;
  path?: string;
};

interface DataProp extends BoxProps {
  data?: IGlobalDocument;
  allowClear?: boolean;
  className?: string;
  loading?: boolean;
  onClose?: () => void;
  onDelete?: () => void;
  onDownload?: () => void;
}

export const FileDownloadComponent: React.FC<DataProp> = ({
  data,
  loading,
  allowClear,
  className,
  onDelete,
  onDownload
}) => {
  return (
    <Card variant="outlined" sx={{ p: 4, height: '100%' }} className={className}>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: '100%',
          gap: 2
        }}
      >
        <Box className="flex items-center">
          <Image src="/iss/assetmanagement/static/icons/filePdf.svg" alt="Icon" width={26} height={30} />
          <Box ml={'14px'}>
            <Typography sx={{ whiteSpace: 'nowrap' }} variant="body2">
              {handleName(String(data?.fileName))}
            </Typography>
            <Typography variant="caption" color={'grey.300'}>
              {data?.fileSize ? `${data?.fileSize}KB - ` : null}
              {convertStringToDateTime(String(data?.uploadedAt))}
            </Typography>
          </Box>
        </Box>
        <Box display="flex">
          {loading ? (
            <CircularProgress size={30} />
          ) : (
            <>
              {allowClear && (
                <Box display="flex" gap={2}>
                  <IconButton onClick={onDownload} sx={{ width: 18, height: 18 }}>
                    <Icon name="download" width={18} height={18} />
                  </IconButton>
                  <IconButton onClick={onDelete} sx={{ width: 18, height: 18 }}>
                    <Icon name="delete" width={18} height={18} />
                  </IconButton>
                </Box>
              )}
            </>
          )}
        </Box>
      </Box>
    </Card>
  );
};

export const ImageCardComponent: React.FC<DataProp> = ({ data, loading, onDelete }) => {
  return (
    <Stack sx={{ position: 'relative' }}>
      {loading ? (
        <Stack
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'grey.900',
            height: 250
          }}
        >
          <CircularProgress />
        </Stack>
      ) : (
        <>
          <Box sx={{ position: 'relative' }} className="image">
            <CardMedia
              sx={{
                borderRadius: 0.5,
                height: 250,
                objectFit: 'cover'
              }}
              component="img"
              image={data?.originalUrl}
            />
            <Box
              className="opacity-0 hover:opacity-100"
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                gap: 1,
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                background: 'rgba(7, 6, 50, 0.4)',
                transition: '0.2 ease-out'
              }}
            >
              <IconButton sx={{ width: 20, height: 18 }}>
                <Icon name="eye-white" width={18} height={18} />
              </IconButton>
              <IconButton sx={{ width: 20, height: 18 }} onClick={onDelete}>
                <Icon name="delete-white" width={18} height={18} />
              </IconButton>
            </Box>
          </Box>
          <Typography sx={{ whiteSpace: 'nowrap', overflow: 'hidden' }} variant="subtitle2" mt={1}>
            {handleName(String(data?.fileName))}
          </Typography>
          <Typography mt={1} fontSize={12} color={'grey.800'}>
            {data?.fileSize ? `${data?.fileSize}KB - ` : null}
            {convertStringToDateTime(String(data?.uploadedAt))}
          </Typography>
        </>
      )}
    </Stack>
  );
};

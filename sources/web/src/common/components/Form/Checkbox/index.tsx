import { Checkbox as MuiCheckbox, CheckboxProps, styled } from '@mui/material';

const BpIcon = styled('span')(({ theme }) => ({
  borderRadius: 3,
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === 'dark'
      ? '0 0 0 1px rgb(16 22 26 / 40%)'
      : `inset 0 0 0 1px ${theme.palette.grey[200]}, inset 0 -1px 0 ${theme.palette.grey[200]}`,

  '.Mui-focusVisible &': {
    outline: `2px auto ${theme.palette.primary.main}`,
    outlineOffset: 2
  },
  'input:hover ~ &': { backgroundColor: theme.palette.mode === 'dark' ? '#30404d' : '#ebf1f5' },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: theme.palette.grey[100]
  }
}));

const BpCheckedIcon = styled(BpIcon)(({ theme }) => ({
  boxShadow: 'none',
  backgroundColor: theme.palette.primary.main,
  '&:before': {
    display: 'block',
    width: 18,
    height: 18,
    backgroundImage:
      "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 18 18'%3E%3Cpath" +
      " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
      "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
    content: '""'
  },

  'input:not(:disabled):hover ~ &': {
    backgroundColor: theme.palette.action.selected
  }
}));

export function Checkbox(props: CheckboxProps) {
  return (
    <MuiCheckbox
      sx={{
        '&:hover': { bgcolor: 'transparent' }
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      inputProps={{ 'aria-label': props.name }}
      {...props}
    />
  );
}

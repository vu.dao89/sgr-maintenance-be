import CloseIcon from '@mui/icons-material/Close';
import {
  Badge,
  Box,
  BoxProps,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Popover,
  PopoverProps,
  Typography
} from '@mui/material';
import { Icon } from 'common/components';
import { FILTER_BUTTON, RESET_BUTTON } from 'common/constants';
import { isString } from 'lodash';
import React from 'react';

interface IFilterInputProps extends BoxProps {
  title: string;
  children: React.ReactNode;
  isLoading?: boolean;
  isDisabled?: boolean;
  btnResetText?: string;
  btnSubmitText?: string;
  popoverProps?: PopoverProps;
  filterCount?: number;
  onReset: () => void;
  onSubmit: () => void;
  hideCloseBtn?: (value: any) => void;
}

export const FilterInput = ({
  id,
  isLoading,
  isDisabled = true,
  children,
  title,
  btnResetText = RESET_BUTTON,
  btnSubmitText = FILTER_BUTTON,
  popoverProps,
  filterCount,
  onReset,
  onSubmit,
  hideCloseBtn,
  ...props
}: IFilterInputProps) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSubmit = () => {
    onSubmit();
    handleClose();
  };

  const handleReset = () => {
    onReset();
    handleClose();
  };

  const open = Boolean(anchorEl);
  const popoverId = open ? `popover-${id}` : undefined;

  return (
    <Box {...props}>
      <Button
        aria-describedby={popoverId}
        color="primary"
        variant="contained"
        onClick={handleOpen}
        sx={{ borderRadius: 1, minWidth: 46, width: 46, height: 46, ml: 4, p: 4, textAlign: 'center' }}
      >
        <Badge badgeContent={filterCount} color="success">
          <Icon name="filter" width={46} height={46} />
        </Badge>
      </Button>

      <Popover
        id={popoverId}
        anchorEl={anchorEl}
        PaperProps={{ sx: { maxWidth: props.maxWidth || 600 } }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        {...popoverProps}
        open={open}
      >
        {isString(title) ? (
          <DialogTitle className="flex items-center justify-between py-2.5">
            <Typography component="span" variant="h5">
              {title}
            </Typography>
            {!hideCloseBtn ? (
              <IconButton aria-label="close" disableRipple className="p-0 mr-[-4px]" onClick={handleClose}>
                <CloseIcon />
              </IconButton>
            ) : null}
          </DialogTitle>
        ) : (
          title
        )}
        <DialogContent dividers className="pt-2 pb-6">
          {children}
        </DialogContent>
        <DialogActions className="px-6 py-5">
          <Button
            disableRipple
            disabled={isDisabled}
            color="inherit"
            type="button"
            size="small"
            variant="contained"
            onClick={handleReset}
            className="bg-primaryLight normal-case"
          >
            {btnResetText}
          </Button>
          <Button
            disableRipple
            disabled={isDisabled}
            color="primary"
            type="button"
            size="small"
            variant="contained"
            onClick={handleSubmit}
            className="normal-case"
          >
            {btnSubmitText}
          </Button>
        </DialogActions>
      </Popover>
    </Box>
  );
};

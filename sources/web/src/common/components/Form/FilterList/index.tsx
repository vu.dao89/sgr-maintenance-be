import ClearIcon from '@mui/icons-material/Clear';
import { Box, Chip, Typography, useTheme } from '@mui/material';
import classNames from 'classnames';
import { isObject } from 'lodash';

interface IFilterListProps {
  data: any;
  label: any;
  onRemove: (e: any) => any;
}

export const FilterList = ({ data, label, onRemove }: IFilterListProps) => {
  const theme = useTheme();

  return (
    <Box display="flex" mb={3}>
      <Typography variant="body2" className="shrink-0 pt-1">
        Tìm kiếm:
      </Typography>
      <Box display="flex" flexWrap="wrap" alignItems="center" ml={6}>
        {Object.keys(data).map((key, index) => {
          const value = isObject(data[key]) ? data[key]?.label : data[key];
          return (
            <Chip
              key={index}
              label={`${label[key] || ''}: ${value}`}
              variant="outlined"
              deleteIcon={
                <ClearIcon className={classNames('w-[14px] h-[14px] mr-3', `text-[${theme.palette.primary.dark}]`)} />
              }
              onDelete={() => onRemove(key)}
              sx={{
                fontSize: 14,
                backgroundColor: '#FFF',
                border: 'none',
                borderRadius: 100,
                mx: 1,
                mb: 2,
                fontWeight: 500
              }}
            />
          );
        })}
      </Box>
    </Box>
  );
};

import { BoxProps, Stack, Typography } from '@mui/material';
import React from 'react';

interface IFormDataValueProps extends BoxProps {
  label: string;
  value: React.ReactNode | string;
  className?: string;
}

export const FormDataValue: React.FC<IFormDataValueProps> = ({ label, value, className, ...props }) => {
  return (
    <Stack mt={4} className={className} {...props}>
      <Typography variant="body2" mb={1}>
        {label}
      </Typography>
      {value ? (
        <Typography variant="subtitle1">{value}</Typography>
      ) : (
        <Typography className="text-[#a3a3a3]">&#9472;</Typography>
      )}
    </Stack>
  );
};

import { BoxProps, FormLabel, Typography } from '@mui/material';
import React from 'react';

interface FormLabelCustomProps extends BoxProps {
  required?: boolean;
  className?: string;
  children?: React.ReactNode | string;
}

export const FormLabelCustom: React.FC<FormLabelCustomProps> = ({ required, children }) => {
  return (
    <FormLabel sx={{ display: 'flex', alignContent: 'center' }}>
      {children}
      {required && (
        <Typography ml="6px" color="error">
          *
        </Typography>
      )}
    </FormLabel>
  );
};

import { Autocomplete, CircularProgress, TextField } from '@mui/material';
import classNames from 'classnames';
import { Icon } from 'common/components';
import { PLACEHOLDER_SEARCH_INPUT } from 'common/constants';
import { sleep } from 'common/helpers';
import { Fragment, useEffect, useState } from 'react';

interface ISearchAutocompleteProps {
  name: string;
  className?: string;
  options: IInput[];
}

export const SearchAutocomplete: React.FC<ISearchAutocompleteProps> = ({ name, className, options = [], ...props }) => {
  const [openAutocomplete, setOpenAutocomplete] = useState(false);
  const [dataOptions, setDataOptions] = useState<IInput[]>([]);
  const loadingAutocomplete = openAutocomplete && dataOptions.length === 0;

  useEffect(() => {
    let active = true;
    if (!loadingAutocomplete) return undefined;

    (async () => {
      await sleep(1000); // For demo purposes.

      if (active) {
        setDataOptions(options);
      }
    })();

    return () => {
      active = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingAutocomplete]);

  useEffect(() => {
    if (!openAutocomplete) setDataOptions([]);
  }, [openAutocomplete]);

  return (
    <Autocomplete
      id={name}
      fullWidth
      open={openAutocomplete}
      onOpen={() => setOpenAutocomplete(true)}
      onClose={() => setOpenAutocomplete(false)}
      className={classNames('w-full bg-white rounded-[8px] overflow-hidden search', className)}
      isOptionEqualToValue={(option: IInput, value) => option.label === value.label}
      getOptionLabel={(option: IInput) => option.label}
      options={dataOptions}
      loading={loadingAutocomplete}
      clearIcon={<Icon name="close" className="text-[#2B2512]" />}
      popupIcon={<Icon name="search" width={24} height={24} />}
      renderInput={params => (
        <TextField
          {...params}
          placeholder={PLACEHOLDER_SEARCH_INPUT}
          InputProps={{
            ...params.InputProps,
            className: 'py-[5px]',
            endAdornment: (
              <Fragment>
                {loadingAutocomplete ? <CircularProgress color="inherit" size={20} sx={{ mr: 4 }} /> : null}
                {params.InputProps.endAdornment}
              </Fragment>
            )
          }}
        />
      )}
      {...props}
    />
  );
};

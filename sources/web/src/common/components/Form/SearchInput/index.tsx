import { OutlinedInputProps, TextField } from '@mui/material';
import { Icon } from 'common/components';
import { PLACEHOLDER_SEARCH_INPUT } from 'common/constants';

interface ISearchInputProps extends OutlinedInputProps {
  className?: string;
  onSearch: (e: any) => any;
}

export const SearchInput: React.FC<ISearchInputProps> = ({ name, onSearch }) => {
  const onSearchEnter = (e: any) => {
    if (e.keyCode == 13) {
      onSearch(e.target.value);
    }
  };

  return (
    <TextField
      id={`search-${name}`}
      variant="outlined"
      fullWidth
      placeholder={PLACEHOLDER_SEARCH_INPUT}
      InputProps={{
        endAdornment: <Icon name="search" width={24} height={24} />,
        inputProps: { className: 'py-3' }
      }}
      className="bg-white rounded-[8px] overflow-hidden"
      onKeyDown={onSearchEnter}
    />
  );
};

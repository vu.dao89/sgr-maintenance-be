import {
  Box,
  BoxProps,
  Button,
  Divider,
  FormControlLabel,
  ListItemIcon,
  MenuItem,
  Popover,
  Typography,
  useTheme
} from '@mui/material';
import classNames from 'classnames';
import { Icon } from 'common/components';
import { PLACEHOLDER_DISPLAY_COLUMN } from 'common/constants';
import React from 'react';
import SimpleBar from 'simplebar-react';
import { Checkbox } from '../Checkbox';

interface ISelectDisplayColumnProps extends BoxProps {
  options: any;
  className?: string;
  onSelect: (e: any) => void;
  onSubmit?: (e: any) => void;
}

export const SelectDisplayColumn = ({
  id,
  options,
  className,
  onSelect,
  onSubmit,
  ...props
}: ISelectDisplayColumnProps) => {
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
  const open = Boolean(anchorEl);
  const popoverId = open ? `popover-${id}` : undefined;

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newColumns = options.map((item: any) => {
      if (item.key === event.target.value) {
        return { ...item, enable: event.target.checked };
      }
      return item;
    });

    onSelect(newColumns);
  };

  return (
    <Box id={id} {...props}>
      <Button
        id={`button-${id}`}
        aria-describedby={popoverId}
        onClick={handleClick}
        color="inherit"
        variant="outlined"
        className={classNames(
          'flex justify-between rounded-lg min-w-[180px] px-3 py-2 normal-case',
          open ? 'border-primary' : `border-[${theme.palette.grey[600]}]`
        )}
      >
        <Typography component="span" variant="body2">
          {PLACEHOLDER_DISPLAY_COLUMN}
        </Typography>
        <Icon name="arrow-down" width={12} height={12} />
      </Button>
      <Popover
        id={popoverId}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        PaperProps={{
          className: 'w-[310px]'
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <SimpleBar style={{ maxHeight: '360px' }}>
          {options.map((item: any, index: number) => (
            <MenuItem
              key={item.key}
              value={item.key}
              className={classNames('px-6 py-2', index === 0 && 'mt-3')}
              disabled={item.required ?? false}
            >
              <ListItemIcon>
                <FormControlLabel
                  id={`checkbox-${id}`}
                  label={item.title}
                  control={
                    <Checkbox
                      size="small"
                      name={item.title}
                      value={item.key}
                      checked={item.enable}
                      disabled={item.required ?? false}
                      onChange={onChange}
                      className="py-0 pr-3 text-[16px]"
                    />
                  }
                />
              </ListItemIcon>
            </MenuItem>
          ))}
        </SimpleBar>
        <Divider className="mt-2" />
      </Popover>
    </Box>
  );
};

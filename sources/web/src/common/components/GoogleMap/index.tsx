import { Loader, LoaderOptions } from '@googlemaps/js-api-loader';
import { Status } from 'common/constants';
import { useEffect, useRef, useState } from 'react';

export interface GoogleMapProp {
  zoom?: number;
  center: google.maps.LatLngLiteral;
}

export const RenderMapComponent = ({
  zoom = Number(process.env.NEXT_PUBLIC_GOOGLE_MAP_ZOOM_DEFAULT),
  center
}: GoogleMapProp) => {
  const apiKey = String(process.env.NEXT_PUBLIC_GOOGLE_API_KEY);

  return (
    <WrapperMap apiKey={apiKey}>
      <MyMapComponent center={center} zoom={zoom} />
    </WrapperMap>
  );
};

//---------------------------------------------------------

const MyMapComponent = ({ zoom = Number(process.env.NEXT_PUBLIC_GOOGLE_MAP_ZOOM_DEFAULT), center }: GoogleMapProp) => {
  const refMap = useRef<any>(null);

  useEffect(() => {
    new window.google.maps.Map(refMap.current, {
      center,
      zoom,
      disableDefaultUI: true
    });
  });

  return <div ref={refMap} id="map" />;
};

//---------------------------------------------------------

interface WrapperMapProps extends LoaderOptions {
  children?: React.ReactNode;
  render?: (status: Status) => React.ReactElement;
  callback?: (status: Status, loader: Loader) => void;
}

const WrapperMap = ({ children, render, callback, ...options }: WrapperMapProps) => {
  const [status, setStatus] = useState(Status.LOADING);

  useEffect(() => {
    const loader = new Loader(options);

    const setStatusAndExecuteCallback = (status: Status) => {
      if (callback) callback(status, loader);
      setStatus(status);
    };

    setStatusAndExecuteCallback(Status.LOADING);

    loader.load().then(
      () => setStatusAndExecuteCallback(Status.SUCCESS),
      () => setStatusAndExecuteCallback(Status.FAILURE)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (status === Status.SUCCESS && children) return <>{children}</>;

  if (render) return render(status);

  return <></>;
};

import Image from 'next/image';
import React from 'react';

export type TIcon =
  | 'logo'
  | 'loader'
  | 'dashboard'
  | 'dashboard-active'
  | 'equipment'
  | 'equipment-active'
  | 'notification'
  | 'notification-active'
  | 'task'
  | 'task-active'
  | 'collapse'
  | 'eye'
  | 'inbox'
  | 'filter'
  | 'search'
  | 'close'
  | 'arrow-down'
  | 'arrow-left'
  | 'delete'
  | 'add'
  | 'add-blue'
  | 'pencil'
  | 'upload'
  | 'eye-white'
  | 'delete-white'
  | 'warning'
  | 'calendar'
  | 'more'
  | 'tick-done'
  | 'no-image'
  | 'download';

export interface IconProps {
  name?: TIcon;
  width?: number;
  height?: number;
  className?: string;
}

export const Icon: React.FC<IconProps> = ({ name, width = 20, height = 20, className }) => {
  const iconName = name ? name : 'no-image';
  return (
    <Image
      src={`/iss/assetmanagement/static/icons/${iconName}.svg`}
      alt={iconName}
      width={width}
      height={height}
      className={className}
      priority
    />
  );
};

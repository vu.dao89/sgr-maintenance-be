import { BoxProps, Typography } from '@mui/material';
import { Loading } from 'common/components';
import { TEXT_DATA_NOT_FOUND } from 'common/constants';

interface DataProp extends BoxProps {
  loading?: boolean;
  hasData: boolean;
  children: any;
}

export const InnerLayout = ({ loading, hasData, children }: DataProp) => {
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {hasData ? (
            <>{children}</>
          ) : (
            <Typography px={2} mt={5}>
              {TEXT_DATA_NOT_FOUND}
            </Typography>
          )}
        </>
      )}
    </>
  );
};

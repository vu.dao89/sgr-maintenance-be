import { Box, BoxProps } from '@mui/material';
import classNames from 'classnames';
import { FC, ReactNode } from 'react';

interface BlankLayoutProps extends BoxProps {
  children?: ReactNode;
  className?: string;
}

export const BlankLayout: FC<BlankLayoutProps> = ({ children, className, ...props }): JSX.Element => {
  return (
    <Box className={classNames('bg-white h-screen', className)} {...props}>
      {children}
    </Box>
  );
};

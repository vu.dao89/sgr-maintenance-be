import { appActions, useSelectorSidebarStore } from '@modules/App';
import { userActions, useSelectUserStore } from '@modules/User';
import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBarProps as MuiAppBarProps,
  Box,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography
} from '@mui/material';
import { removeToken, useAppDispatch } from 'common/app';
import { Icon } from 'common/components/Icon';
import { redirect, stackCallback } from 'common/helpers';
import Image from 'next/image';
import { useCallback, useState } from 'react';
import { AppBar } from './style';

const HEADER_MENU = [
  {
    name: 'user',
    title: 'Thông tin tài khoản',
    link: '/user'
  },
  {
    name: 'logout',
    title: 'Đăng xuất',
    link: '/logout'
  }
];
export interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

export const HeaderPrivate = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
  const open = useSelectorSidebarStore();
  const { user } = useSelectUserStore();

  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorElUser(null);
  };

  const handleToggleSidebar = useCallback(() => dispatch(appActions.onToggleSidebar()), [dispatch]);

  const handleLogout = useCallback(
    (name: string, link: string) => {
      if (name === 'logout') {
        dispatch(userActions.logout());
        removeToken();
        stackCallback(() => redirect('/'));
      } else {
        redirect(link);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dispatch]
  );

  return (
    <AppBar
      position="fixed"
      open={open}
      sx={{ bgcolor: 'white', borderBottom: '1px solid #D9DCE4', gap: 8 }}
      elevation={0}
    >
      <Toolbar className="flex px-6 py-3">
        <IconButton disableRipple onClick={handleToggleSidebar} sx={{ ml: -2 }}>
          <MenuIcon />
        </IconButton>

        <Box ml="auto" sx={{ display: { xs: 'none', md: 'flex' } }}>
          <IconButton size="large" disableRipple aria-label="Show inbox" color="inherit" className="p-0 mr-4">
            <Icon name="inbox" width={40} height={40} />
          </IconButton>
          {user && (
            <Box mr={2}>
              <Typography variant="body2">{user.name}</Typography>
              <Typography fontSize={12}>{user.departments[0]?.name}</Typography>
            </Box>
          )}

          <Image
            src={user?.picture || '/iss/assetmanagement/static/images/default-avatar.svg'}
            alt="Avatar"
            width={40}
            height={40}
          />
          <IconButton
            id="menu-profile-button"
            aria-controls={open ? 'menu-profile' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            disableRipple
            onClick={handleOpen}
            className="p-0 ml-3"
          >
            <Icon name="arrow-down" width={14} height={14} />
          </IconButton>
          <Menu
            sx={{ mt: '52px' }}
            id="menu-profile"
            aria-labelledby="menu-profile"
            anchorEl={anchorElUser}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right'
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right'
            }}
            open={Boolean(anchorElUser)}
            onClose={handleClose}
          >
            {HEADER_MENU.map(({ title, name, link }, index) => (
              <Box key={index}>
                <MenuItem onClick={() => handleLogout(name, link)}>
                  <Typography textAlign="center" variant="body2">
                    {title}
                  </Typography>
                </MenuItem>
                {index !== HEADER_MENU.length - 1 && <Divider />}
              </Box>
            ))}
          </Menu>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

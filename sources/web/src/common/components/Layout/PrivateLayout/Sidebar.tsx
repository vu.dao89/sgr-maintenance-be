import { useSelectorSidebarStore } from '@modules/App';
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText, useTheme } from '@mui/material';
import { Icon, TIcon } from 'common/components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Drawer, DrawerHeader } from './style';

interface IMenu {
  name: string;
  icon: string;
  link: string;
}

const PRIVATE_MENU: IMenu[] = [
  { name: 'Thiết bị', icon: 'equipment', link: '/equipment' },
  { name: 'Thông báo', icon: 'notification', link: '/notification' },
  { name: 'Work order', icon: 'dashboard', link: '/work-order' },
  { name: 'Lệnh bảo trì', icon: 'task', link: '/maintenance' }
];

export const SidebarPrivate = (): JSX.Element => {
  const theme = useTheme();
  const { pathname } = useRouter();
  const open = useSelectorSidebarStore();

  return (
    <Drawer
      variant="permanent"
      open={open}
      PaperProps={{
        sx: {
          color: 'white',
          backgroundColor: theme.palette.secondary.light
        }
      }}
    >
      <DrawerHeader open={open as boolean}>
        <Icon name="logo" width={open ? 70 : 42} height={24} />
      </DrawerHeader>
      <List component="div" className="pt-6">
        {PRIVATE_MENU.map((item: IMenu, index) => (
          <ListItem component="div" key={index} sx={{ px: 3, py: 0.5 }}>
            <ListItemButton
              component={Link}
              href={item.link}
              selected={item.link === pathname}
              passHref
              sx={{
                minHeight: 46,
                justifyContent: open ? 'initial' : 'center',
                px: 4,
                py: 2.5,
                borderRadius: 1,
                backgroundColor: item.link === pathname ? `${theme.palette.secondary.main}!important` : undefined,
                color: item.link === pathname ? theme.palette.primary.main : undefined
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : 'auto'
                }}
              >
                <Icon name={(item.link === pathname ? `${item.icon}-active` : item.icon) as TIcon} />
              </ListItemIcon>
              <ListItemText primary={item.name} sx={{ opacity: open ? 1 : 0 }} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Drawer>
  );
};

import { Box, Typography } from '@mui/material';
import { theme } from 'common/themes/mui';
import { FC, ReactNode } from 'react';
import SimpleBar from 'simplebar-react';
import { HeaderPrivate } from './Header';
import { SidebarPrivate } from './Sidebar';
import { DrawerHeader } from './style';

interface PrivateLayoutProps {
  children?: ReactNode;
  title: string;
}

export const PrivateLayout: FC<PrivateLayoutProps> = ({ children, title }): JSX.Element => {
  return (
    <Box className="flex h-full">
      <HeaderPrivate />
      <SidebarPrivate />
      <Box className="w-full h-full overflow-hidden">
        <DrawerHeader />
        <Box className="relative h-full overflow-hidden pt-0.5" bgcolor={theme.colors.backgroundBody}>
          <SimpleBar style={{ maxHeight: 'calc(100% - 64px)' }}>
            <Box p={6}>
              {title && (
                <Typography component="h1" variant="h4" mb={4}>
                  {title}
                </Typography>
              )}
              <Box component="section">{children}</Box>
            </Box>
          </SimpleBar>
        </Box>
      </Box>
    </Box>
  );
};

import { Box } from '@mui/material'
import { FC, Fragment, ReactNode } from 'react'

interface PublicLayoutProps {
  children?: ReactNode
}

export const PublicLayout: FC<PublicLayoutProps> = ({
  children
}): JSX.Element => {
  return (
    <Fragment>
      <Box component="main">{children}</Box>
    </Fragment>
  )
}

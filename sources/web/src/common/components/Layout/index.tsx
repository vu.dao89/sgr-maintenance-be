import { Box, BoxProps } from '@mui/material';
import { EAppLayout } from 'common/constants';
import { Fragment, ReactNode } from 'react';
import { ISeo, Seo } from '../Seo';
import { BlankLayout } from './BlankLayout';
import { PrivateLayout } from './PrivateLayout';
import { PublicLayout } from './PublicLayout';

export interface MainLayoutProps extends BoxProps {
  children: ReactNode;
  variant?: EAppLayout;
  seoData: ISeo;
  className?: string;
}

export const MainLayout: React.FC<MainLayoutProps> = ({
  children,
  variant = EAppLayout.PRIVATE,
  className,
  seoData,
  ...props
}) => {
  return (
    <Fragment>
      <Seo data={seoData} />
      <Box id={`${variant}-layout`} height="100%" className={className} {...props}>
        {variant === EAppLayout.PUBLIC ? (
          <PublicLayout>{children}</PublicLayout>
        ) : variant === EAppLayout.PRIVATE ? (
          <PrivateLayout title={seoData.title}>{children}</PrivateLayout>
        ) : (
          <BlankLayout className={className}>{children}</BlankLayout>
        )}
      </Box>
    </Fragment>
  );
};

import {
  Backdrop,
  CircularProgress,
  CircularProgressProps,
  Stack,
  Typography,
  TypographyProps,
  useTheme
} from '@mui/material';
import classNames from 'classnames';
import React, { Fragment } from 'react';
import { Icon } from '../Icon';

interface ILoadingProps extends CircularProgressProps {
  isFullScreen?: boolean;
  rootClassName?: string;
  type?: 'default' | 'icon';
}

export const Loading: React.FC<ILoadingProps> = ({
  isFullScreen = false,
  rootClassName,
  type = 'default',
  ...props
}) => {
  const theme = useTheme();
  return (
    <Fragment>
      {!isFullScreen ? (
        <Stack
          pt={6}
          pb={type === 'icon' ? 40 : 20}
          className={classNames('flex items-center justify-center', type === 'icon' && 'h-screen', rootClassName)}
        >
          {type === 'icon' ? (
            <Icon name="loader" width={120} height={120} {...props} />
          ) : (
            <CircularProgress color="inherit" sx={{ color: theme.palette.grey[300] }} {...props} />
          )}
        </Stack>
      ) : (
        <Backdrop sx={{ color: '#fff', zIndex: theme => theme.zIndex.drawer + 500 }} open={true}>
          <CircularProgress color="inherit" {...props} />
        </Backdrop>
      )}
    </Fragment>
  );
};

interface ITextLoadingProps extends TypographyProps {
  text: string;
}

export const TextLoading: React.FC<ITextLoadingProps> = ({ text, ...props }) => {
  return <Typography {...props}>{text}</Typography>;
};

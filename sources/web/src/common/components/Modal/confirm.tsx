import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography } from '@mui/material';
import { Icon } from '../Icon';

export interface ConfirmationDialogProps {
  id: string;
  title: string;
  content?: string;
  keepMounted: boolean;
  open: boolean;
  onCancel: () => void;
  onOk: () => void;
  btnCancelText?: string;
  btnOKText?: string;
}

export const ConfirmationDialog = (props: ConfirmationDialogProps) => {
  const { onCancel, onOk, open, title, content, btnCancelText = 'Giữ lại', btnOKText = 'Xoá', ...other } = props;

  const handleCancel = () => {
    onCancel();
  };

  const handleOk = () => {
    onOk();
  };

  return (
    <Dialog sx={{ '& .MuiDialog-paper': { width: '80%' } }} maxWidth="xs" open={open} {...other}>
      <DialogTitle className="flex" alignItems="center">
        <Icon name="warning" width={24} height={24} className="mr-4" />
        <Typography sx={{ fontSize: 20, fontWeight: 600 }}>{title}</Typography>
      </DialogTitle>
      {content && <DialogContent>{content}</DialogContent>}
      <DialogActions className="px-6 py-5">
        <Button
          disableRipple
          color="inherit"
          type="button"
          size="small"
          variant="contained"
          className="bg-primaryLight normal-case"
          onClick={handleCancel}
        >
          {btnCancelText}
        </Button>
        {btnOKText ? (
          <Button
            disableRipple
            color="primary"
            type="button"
            size="small"
            variant="contained"
            className="normal-case"
            onClick={handleOk}
          >
            {btnOKText}
          </Button>
        ) : null}
      </DialogActions>
    </Dialog>
  );
};

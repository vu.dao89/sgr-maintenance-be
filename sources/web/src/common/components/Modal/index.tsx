import CloseIcon from '@mui/icons-material/Close';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogProps,
  DialogTitle,
  IconButton,
  Typography
} from '@mui/material';
import { isString } from 'lodash';
import React from 'react';

interface IModalProps extends DialogProps {
  children: React.ReactNode;
  onReset?: (e: any) => void;
  onSubmit?: (e: any) => void;
  onClose?: (e?: any) => void;
  title?: any;
  btnResetText?: string;
  btnSubmitText?: string;
  btnClose?: string;
}

export const Modal = ({
  open,
  children,
  title,
  btnResetText = 'Đặt lại',
  btnSubmitText = 'Lọc ngay',
  btnClose = 'Hủy bỏ',
  onReset,
  onSubmit,
  onClose,
  ...props
}: IModalProps) => {
  return (
    <Dialog
      sx={{ '& .MuiDialog-paper': { width: '80%' } }}
      open={open}
      fullWidth
      maxWidth="md"
      onClose={onClose}
      {...props}
    >
      {isString(title) ? (
        <DialogTitle
          className="flex items-center justify-between py-2.5"
          sx={{ borderBottom: '1px solid', borderColor: 'grey.600' }}
        >
          <Typography sx={{ fontSize: 24, fontWeight: 'bold' }}>{title}</Typography>
          {onClose ? (
            <IconButton aria-label="close" disableRipple className="p-0" onClick={onClose}>
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
      ) : (
        title
      )}

      <DialogContent>{children}</DialogContent>
      <DialogActions className="px-6 py-5">
        <Button
          disableRipple
          onClick={onReset ? onReset : onClose}
          color="inherit"
          type="button"
          size="small"
          variant="contained"
          className="bg-primaryLight normal-case"
        >
          {btnResetText ? btnResetText : btnClose}
        </Button>
        <Button
          disableRipple
          color="primary"
          type="submit"
          size="small"
          variant="contained"
          onClick={onSubmit}
          className="normal-case"
        >
          {btnSubmitText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

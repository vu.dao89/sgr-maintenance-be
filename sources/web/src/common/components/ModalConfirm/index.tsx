import { Box, BoxProps, Button, Dialog, DialogActions, DialogTitle, Typography } from '@mui/material';
import { Icon } from 'common/components';

interface DataProp extends BoxProps {
  open: boolean;
  onClose?: any;
  onConfirm?: any;
  handleOpenModal?: () => void;
  handleDelete?: () => any;
}

export const ModalConfirmDelete: React.FC<DataProp> = ({ open, onClose, onConfirm }) => {
  const confirm = () => {
    onConfirm();
    onClose();
  };

  return (
    <Dialog open={open}>
      <DialogTitle sx={{ p: '20px 32px' }} fontSize={24}>
        <Box display="flex" alignItems="center">
          <Icon name="warning" width={18} height={18} />
          <Typography ml={2}>Bạn có chắc chắn xoá tập tin này?</Typography>
        </Box>
      </DialogTitle>
      <DialogActions sx={{ p: '20px 32px' }}>
        <Button
          onClick={onClose}
          sx={{ backgroundColor: 'primary.light', color: 'black', fontSize: 14, borderRadius: 1 }}
        >
          No
        </Button>
        <Button
          disableRipple
          size="large"
          color="primary"
          type="submit"
          variant="contained"
          sx={{ fontSize: 14, borderRadius: 1 }}
          onClick={confirm}
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
};

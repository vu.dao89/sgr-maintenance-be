import {
  Box,
  Pagination as PaginationMui,
  PaginationProps as PaginationMuiProps,
  TextField,
  Typography
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import classNames from 'classnames';

export interface PaginationProps extends PaginationMuiProps {
  rootClassName?: string;
  totalPages: number;
  totalItems: number;
  page: number;
  limit: number;
  onGoToPage: (e: any) => any;
}

const useStyles = makeStyles({
  item: {
    '& .MuiPaginationItem-previousNext': {
      '@media (max-width: 768px)': {
        margin: 0
      }
    }
  },
  input: {
    '& input[type=number]': {
      '-moz-appearance': 'textfield'
    },
    '& input[type=number]::-webkit-outer-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0
    },
    '& input[type=number]::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0
    }
  }
});

export const Pagination: React.FC<PaginationProps> = ({
  limit,
  totalPages,
  totalItems,
  page,
  rootClassName,
  onGoToPage,
  ...props
}): JSX.Element => {
  const classes = useStyles();
  if (!totalPages && !totalItems) return <></>;

  const pages = totalPages ? totalPages : Math.ceil(totalItems / limit);

  return (
    <Box
      className={classNames(rootClassName, classes.item)}
      display="flex"
      sx={{ alignItems: 'center', justifyContent: 'end' }}
    >
      <Typography variant="body2" fontWeight={500}>
        {totalItems ? `${totalItems} items` : `${totalPages} pages`}
      </Typography>
      <PaginationMui page={page} count={pages} sx={{ ml: 6, mr: 6 }} shape="rounded" {...props} />
      <Typography variant="body2" fontWeight={400}>
        Go to
      </Typography>
      <TextField
        type="number"
        variant="outlined"
        placeholder="1"
        inputProps={{ min: '1', max: totalPages }}
        size="small"
        sx={{ width: 50, ml: 2, py: 1 }}
        onKeyDown={onGoToPage}
        defaultValue={page}
        className={classes.input}
      />
    </Box>
  );
};

import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Icon } from 'common/components';
import { FC, useState } from 'react';
import { Controller } from 'react-hook-form';
import { RHFInputProps } from '../types';

const useStyles = makeStyles(() => ({
  root: {
    'flex-direction': 'row-reverse'
  }
}));
interface RHFCalendarInputProps extends RHFInputProps {
  name: string;
  variant?: string;
}

export const RHFCalendar: FC<RHFCalendarInputProps> = ({ name, control, ...props }) => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);
  return (
    <Controller
      name={name as string}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <DatePicker
            inputFormat="DD/MM/YYYY"
            value={value}
            onChange={onChange}
            components={{
              OpenPickerIcon: () => <Icon name="calendar" />
            }}
            renderInput={(params: any) => (
              <TextField
                {...params}
                size="small"
                name={name}
                error={!!error}
                helperText={error ? error.message : null}
                onClick={() => setIsOpen(true)}
                onKeyDown={e => e.preventDefault()}
                onKeyUp={e => e.preventDefault()}
                fullWidth
              />
            )}
            InputProps={{
              classes: { root: classes.root }
            }}
            open={isOpen}
            onOpen={() => setIsOpen(true)}
            onClose={() => setIsOpen(false)}
            {...props}
          />
        );
      }}
    />
  );
};

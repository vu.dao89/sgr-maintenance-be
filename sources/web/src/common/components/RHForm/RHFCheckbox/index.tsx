import { Checkbox, FormControlLabel, FormControlLabelProps, styled, Typography } from '@mui/material';
import { theme } from 'common/themes/mui';
import React from 'react';
import { Controller } from 'react-hook-form';

export const BpIcon = styled('span')(({ theme }) => ({
  borderRadius: '3px',
  width: 16,
  height: 16,
  backgroundColor: '#E2E2E2',
  '.Mui-focusVisible &': {},
  'input:hover ~ &': {
    backgroundColor: '#E2E2E2'
  },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: theme.palette.mode === 'dark' ? 'rgba(57,75,89,.5)' : 'rgba(206,217,224,.5)'
  }
}));

export const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: theme.primary,
  '&:before': {
    display: 'block',
    width: 16,
    height: 16,
    background: "url('/iss/assetmanagement/static/icons/checked.svg') no-repeat center center",
    content: '""'
  },
  'input:hover ~ &': {
    backgroundColor: theme.primary
  }
});
interface RHFCheckboxProps extends Omit<FormControlLabelProps, 'id'> {
  id: number | string;
  control: any;
  labelClassName?: string;
  checkboxClassName?: string;
  sizeCheckbox?: 'small' | 'large';
}

export const RHFCheckbox: React.FC<RHFCheckboxProps> = ({
  id,
  name,
  label,
  control,
  labelClassName,
  checkboxClassName,
  ...props
}) => {
  return (
    <Controller
      name={name as any}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <FormControlLabel
            id={`RHFCheckbox-${id}`}
            control={
              <Checkbox
                size="small"
                disableRipple
                checkedIcon={<BpCheckedIcon />}
                icon={<BpIcon />}
                className={checkboxClassName}
              />
            }
            label={
              <Typography fontWeight={300} className={labelClassName} color={!!error ? 'error' : ''}>
                {label}
              </Typography>
            }
            checked={value ?? false}
            onChange={onChange}
            {...props}
          />
        );
      }}
    />
  );
};

import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { Icon } from 'common/components';
import { FC, useState } from 'react';
import { Controller } from 'react-hook-form';
import { RHFInputProps } from '../types';

const useStyles = makeStyles(() => ({
  root: {
    'flex-direction': 'row-reverse'
  }
}));
interface RHFDateTimePickerInputProps extends RHFInputProps {
  name: string;
  disableFuture?: boolean;
  clearable?: boolean;
}

export const RHFDateTimePicker: FC<RHFDateTimePickerInputProps> = ({ name, control, ...props }) => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);
  return (
    <Controller
      name={name as string}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="vn">
            <DateTimePicker
              inputFormat="DD/MM/YYYY HH:mm"
              value={value}
              onChange={onChange}
              components={{
                OpenPickerIcon: () => <Icon name="calendar" />
              }}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  size="small"
                  name={name}
                  error={!!error}
                  helperText={error ? error.message : null}
                  onClick={() => setIsOpen(true)}
                  onKeyDown={e => e.preventDefault()}
                  onKeyUp={e => e.preventDefault()}
                  fullWidth
                />
              )}
              InputProps={{
                classes: { root: classes.root }
              }}
              open={isOpen}
              onOpen={() => setIsOpen(true)}
              onClose={() => setIsOpen(false)}
              {...props}
            />
          </LocalizationProvider>
        );
      }}
    />
  );
};

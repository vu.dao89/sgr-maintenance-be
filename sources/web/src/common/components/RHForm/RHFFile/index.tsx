import { Box, Stack, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import classNames from 'classnames';
import { Icon } from 'common/components/Icon';
import { FILE } from 'common/constants';
import { checkAllItemIsStringArr } from 'common/helpers';
import { isArray, isEmpty, isFunction } from 'lodash';
import { FC, useCallback, useEffect, useState } from 'react';
import Dropzone, { DropzoneOptions } from 'react-dropzone';
import { Controller } from 'react-hook-form';
import { RHFInputProps } from '..';
import { RootInnerStyle, RootStyle } from './styles';

const useStyles = makeStyles((theme: Theme) => ({
  active: {
    borderColor: theme.palette.success.main
  },

  accept: {
    borderColor: '#00e676'
  },

  reject: {
    borderColor: theme.palette.error.main
  }
}));

interface RHFFileProps extends DropzoneOptions, RHFInputProps {
  id?: string;
  name: string;
  maxWidth?: number;
  maxHeight?: number;
  loading?: boolean;
  onChangeCb?: (e: any) => void;
  handleUpload?: any;
}

export const RHFFile: FC<RHFFileProps> = ({
  id,
  name,
  control,
  accept,
  multiple = false,
  onChangeCb,
  setValue,
  maxWidth = 100,
  maxHeight = 100,
  loading = false,
  handleUpload,
  ...rest
}) => {
  const classes = useStyles();
  //const fileSizeText = `File size: ${(FILE.LIMIT / 1048576).toFixed()}MB`
  //const fileTypeText = `File type: ${FILE.ACCEPT.replaceAll('images/', '.')}`

  /**
   * showError
   */
  const showError = useCallback(() => {
    // enqueueSnackbar(VALID.FILE_LIMIT, { variant: 'error' })
    // TODO
  }, []);

  const [fileUpload, setFileUpload] = useState([] as any);

  useEffect(() => {
    if (fileUpload.length) {
      handleUpload(fileUpload);
      setFileUpload([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fileUpload]);

  return (
    <div id={id} className="upload">
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value }, formState: { errors } }) => {
          if (isArray(value) && checkAllItemIsStringArr(value)) {
            setFileUpload(value);
          }
          const error = errors[name];

          return (
            <Dropzone
              noClick={loading || false}
              noDrag={loading || false}
              onDrop={(acceptedFiles, rejectedFiles) => {
                if (rejectedFiles[0]?.errors?.length) return showError();
                if (acceptedFiles[0].size > FILE.LIMIT) return showError();

                const allFiles = multiple ? [...fileUpload, ...acceptedFiles] : acceptedFiles;
                setFileUpload(allFiles);
                onChange(allFiles);
                isFunction(onChangeCb) && onChangeCb(allFiles);
              }}
              multiple={multiple ? multiple : false}
              accept={accept ? accept : (FILE.ACCEPT as any)}
              {...rest}
            >
              {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject }) => {
                return (
                  <div className={classNames(multiple && 'flex flex-col', 'upload')}>
                    <RootStyle
                      {...getRootProps({
                        className: `${
                          (isDragActive && classes.active,
                          isDragAccept && classes.accept,
                          (isDragReject || !isEmpty(error)) && classes.reject)
                        }`
                      })}
                      className={classNames(
                        multiple && 'w-full h-100 min-w-[100px] min-h-[100px] border-dashed',
                        'relative cursor-pointer'
                      )}
                    >
                      <input {...getInputProps()} />
                      <RootInnerStyle className={classNames('upload-form-inner')}>
                        {isDragActive ? (
                          <Typography variant="caption">
                            {multiple ? 'Drop the files here ...' : 'Drop the file here ...'}
                          </Typography>
                        ) : (
                          <Stack alignContent="center#758098">
                            <Box className="flex items-center justify-center">
                              <Icon name="upload" width={20} height={19} />
                              <Typography variant="body1" fontWeight={500} ml={4} color={'primary.dark'}>
                                Đính kèm tài liệu
                              </Typography>
                            </Box>
                            <Typography mt={1} variant="body2" color={'grey.800'}>
                              Kích thước tối đa 10MB (pdf, xlsx, jpg, png)
                            </Typography>
                          </Stack>
                        )}
                      </RootInnerStyle>
                    </RootStyle>
                  </div>
                );
              }}
            </Dropzone>
          );
        }}
      />
    </div>
  );
};

import { Box, CircularProgress, FormHelperText, MenuItem, Select, SelectProps } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Icon } from 'common/components';
import { isEmpty } from 'lodash';
import { FC, Fragment } from 'react';
import { Controller } from 'react-hook-form';
import { RHFInputProps } from '../types';

const ITEM_TOTAL = 9;
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * ITEM_TOTAL + ITEM_PADDING_TOP
    }
  }
};

const usePlaceholderStyles = makeStyles(() => ({
  placeholder: {
    color: '#aaa'
  }
}));
interface RHFSelectProps extends RHFInputProps, SelectProps {
  loading?: boolean;
  disabled?: boolean;
  options?: IOption[];
  displayEmpty?: boolean;
  isPlaceholderOption?: boolean;
}

export const SelectPlaceholder = ({ children }: any) => {
  const classes = usePlaceholderStyles();
  return <div className={classes.placeholder}>{children}</div>;
};

export const RHFSelect: FC<RHFSelectProps> = ({
  loading,
  disabled,
  name,
  size,
  control,
  label,
  setValue,
  placeholder,
  options = [],
  displayEmpty = false,
  isPlaceholderOption,
  ...props
}) => {
  return (
    <Controller
      name={name as string}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <Fragment>
            <Select
              id={`RHFSelect-${name}`}
              size="small"
              variant="outlined"
              fullWidth
              label={label}
              disabled={disabled}
              value={value}
              onChange={onChange}
              displayEmpty={displayEmpty}
              renderValue={value ? undefined : () => <SelectPlaceholder>{placeholder}</SelectPlaceholder>}
              error={!!error}
              IconComponent={() => (
                <Box display="flex" justifyContent="center" pr={3}>
                  {loading ? <CircularProgress size={20} /> : <Icon name="arrow-down" width={12} height={12} />}
                </Box>
              )}
              MenuProps={MenuProps}
              {...props}
            >
              {isPlaceholderOption && <MenuItem value="">{placeholder}</MenuItem>}
              {!isEmpty(options) &&
                options.map((option: any, index: number) => (
                  <MenuItem key={index} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
            </Select>

            {error && (
              <FormHelperText
                component="div"
                error={!isEmpty(error.message)}
                sx={{ marginLeft: '14px', marginRight: '14px' }}
              >
                {error.message}
              </FormHelperText>
            )}
          </Fragment>
        );
      }}
    />
  );
};

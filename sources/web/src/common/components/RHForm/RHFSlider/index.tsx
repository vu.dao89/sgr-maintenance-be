import { Slider, SliderProps } from '@mui/material';
import React from 'react';
import { Controller } from 'react-hook-form';

interface RHFSliderProps extends Omit<SliderProps, 'id'> {
  id: number | string;
  control: any;
}

export const RHFSlider: React.FC<RHFSliderProps> = ({ id, name, control, ...props }) => {
  return (
    <Controller
      name={name as any}
      control={control}
      render={({ field: { onChange, value } }) => {
        return <Slider id={`RHFSlider-${id}`} name={name} size="small" value={value} onChange={onChange} {...props} />;
      }}
    />
  );
};

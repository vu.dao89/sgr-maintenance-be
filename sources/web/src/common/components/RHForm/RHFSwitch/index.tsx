import { Switch, SwitchProps } from '@mui/material';
import React from 'react';
import { Controller } from 'react-hook-form';

interface RHFSwitchProps extends SwitchProps {
  control: any;
  setValue?: any;
}

export const RHFSwitch: React.FC<RHFSwitchProps> = ({ id, name, control, setValue, ...props }) => {
  return (
    <Controller
      name={name as any}
      control={control}
      render={({ field: { onChange, value } }) => {
        return <Switch id={`RHFSwitch-${id}`} onChange={onChange} value={value} checked={value} {...props} />;
      }}
    />
  );
};

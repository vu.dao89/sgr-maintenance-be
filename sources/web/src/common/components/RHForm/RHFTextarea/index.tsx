import { InputLabel, OutlinedTextFieldProps, Typography } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import { RHFTextInput } from '../RHFTextInput';
import { RHFInputProps } from '../types';

export interface RHFTextareaProps extends RHFInputProps, OutlinedTextFieldProps {
  inputLabel?: string;
  valueWatch?: any;
  isShowCount?: boolean;
}

export const RHFTextarea: FC<RHFTextareaProps> = ({
  id,
  inputLabel,
  control,
  valueWatch,
  isShowCount = false,
  ...props
}) => {
  const maxLength = (process.env.formLimitTextarea as any) || 255;
  const [countTextAreaLength, setCountTextAreaLength] = useState<number>(maxLength);

  useEffect(() => setCountTextAreaLength(maxLength - valueWatch), [maxLength, valueWatch]);
  return (
    <>
      {inputLabel && <InputLabel htmlFor={id}>{inputLabel}</InputLabel>}
      <RHFTextInput id={id} control={control} inputProps={{ maxLength: maxLength }} multiline rows={5} {...props} />
      {isShowCount && (
        <Typography variant="caption" align="right" className="textarea-counter" sx={{ color: '#BDBDBD' }} mt={0.5}>
          {countTextAreaLength} characters remaining.
        </Typography>
      )}
    </>
  );
};

import CloseIcon from '@mui/icons-material/Close';
import CollectionsIcon from '@mui/icons-material/Collections';
import { Box, FormHelperText, IconButton, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import classNames from 'classnames';
import { Thumbnail } from 'common/components';
import { FILE } from 'common/constants';
import { checkAllItemIsStringArr } from 'common/helpers';
import { theme } from 'common/themes/mui';
import { isArray, isEmpty, isFunction } from 'lodash';
import Image from 'next/image';
import { FC, useCallback, useState } from 'react';
import Dropzone, { DropzoneOptions } from 'react-dropzone';
import { Controller } from 'react-hook-form';
import { RHFInputProps } from '..';
import { RootInnerStyle, RootStyle, ThumbItemStyle, ThumbItemStyleGallary, ThumbStyle } from './styles';

const useStyles = makeStyles((theme: Theme) => ({
  active: {
    borderColor: theme.palette.success.main
  },

  accept: {
    borderColor: '#00e676'
  },

  reject: {
    borderColor: theme.palette.error.main
  }
}));

interface RHFUploadProps extends DropzoneOptions, RHFInputProps {
  id?: string;
  name: string;
  maxWidth?: number;
  maxHeight?: number;
  onChangeCb?: (e: any) => void;
}

export const RHFUpload: FC<RHFUploadProps> = ({
  id,
  name,
  control,
  accept,
  multiple = false,
  onChangeCb,
  setValue,
  maxWidth = 100,
  maxHeight = 100,
  ...rest
}) => {
  const classes = useStyles();
  //const fileSizeText = `File size: ${(FILE.LIMIT / 1048576).toFixed()}MB`
  //const fileTypeText = `File type: ${FILE.ACCEPT.replaceAll('images/', '.')}`

  /**
   * showError
   */
  const showError = useCallback(() => {
    // enqueueSnackbar(VALID.FILE_LIMIT, { variant: 'error' })
    // TODO
  }, []);

  const [fileImage, setFileImage] = useState([] as any);

  const removeFileImages = (file: any, onChange: (value: any) => void) => {
    const newFiles = [...fileImage];
    newFiles.splice(newFiles.indexOf(file), 1);
    onChange(newFiles);
    setFileImage(newFiles);
  };

  return (
    <div id={id} className="upload">
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value }, formState: { errors } }) => {
          if (isArray(value) && checkAllItemIsStringArr(value)) {
            setFileImage(value);
          }
          const error = errors[name];
          const previewImageList = (
            <ThumbStyle className={classNames(!multiple && 'justify-center')}>
              {isArray(value) ? (
                value.map((file, index) => {
                  const isFileString = typeof file === 'string';
                  return (
                    <ThumbItemStyleGallary
                      key={index}
                      className={classNames(!multiple && `w-[190px] max-w-[190px] my-0`, 'relative')}
                    >
                      <Thumbnail
                        imageUrl={isFileString ? file : URL.createObjectURL(file)}
                        alt={file.name}
                        size="1x1"
                        className={classNames(!multiple && '-my-[1px]', 'thumbnail-centered')}
                        width={'100%'}
                      />
                      {multiple && (
                        <IconButton
                          onClick={() => removeFileImages(file, onChange)}
                          sx={{
                            width: 22,
                            height: 22,
                            position: 'absolute',
                            right: -5,
                            top: -5,
                            zIndex: 20,
                            borderRadius: '100%',
                            border: `1px solid ${theme.gray[600]}`,
                            backgroundColor: theme.white,
                            ':hover': {
                              backgroundColor: theme.white
                            }
                          }}
                        >
                          <CloseIcon sx={{ width: 12, height: 12 }} />
                        </IconButton>
                      )}
                    </ThumbItemStyleGallary>
                  );
                })
              ) : (
                <ThumbItemStyle className={`max-w-[${maxWidth}px] max-h-[${maxHeight}px]`}>
                  <ThumbItemStyle>
                    <Thumbnail imageUrl={value} alt={name} size="1x1" className="thumbnail-centered" width={'100%'} />
                  </ThumbItemStyle>
                </ThumbItemStyle>
              )}
            </ThumbStyle>
          );
          return (
            <Dropzone
              onDrop={acceptedFiles => {
                if (acceptedFiles[0].size > FILE.LIMIT) {
                  return showError();
                }

                const allFiles = multiple ? [...fileImage, ...acceptedFiles] : acceptedFiles;
                setFileImage(allFiles);
                onChange(allFiles);
                isFunction(onChangeCb) && onChangeCb(allFiles);
              }}
              noDrag={!multiple}
              multiple={multiple ? multiple : false}
              accept={accept ? accept : (FILE.ACCEPT as any)}
              {...rest}
            >
              {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject }) => {
                return (
                  <div className={classNames(multiple && 'flex flex-col', 'upload')}>
                    <RootStyle
                      {...getRootProps({
                        className: `${
                          (isDragActive && classes.active,
                          isDragAccept && classes.accept,
                          (isDragReject || !isEmpty(error)) && classes.reject)
                        }`
                      })}
                      className={classNames(
                        multiple && 'w-full h-100 min-w-[100px] min-h-[100px] my-[5px] mr-[5px] border-dashed',
                        'relative cursor-pointer'
                      )}
                    >
                      <input {...getInputProps()} />
                      {multiple && (
                        <CollectionsIcon name="gallery" className="absolute left-[32px] top-[32px] text-[32px]" />
                      )}
                      {isEmpty(value) && (
                        <RootInnerStyle className={classNames('upload-form-inner')}>
                          {isDragActive ? (
                            <Typography variant="caption">Drop the files here ...</Typography>
                          ) : (
                            isEmpty(value) &&
                            !multiple && (
                              <Box className="flex items-center">
                                <Image
                                  src="/iss/assetmanagement/static/icons/upload-file.svg"
                                  alt="Icon"
                                  width={20}
                                  height={19}
                                />

                                <Typography variant="body1" fontWeight={500} ml={4} color={'primary.dark'}>
                                  Đính kèm tài liệu
                                </Typography>
                              </Box>
                            )
                          )}
                        </RootInnerStyle>
                      )}
                      {!!value?.length && !multiple && previewImageList}
                    </RootStyle>

                    {!!value?.length && multiple && previewImageList}
                    <Box display="flex" justifyContent="space-between">
                      {!isEmpty(error) && (
                        <FormHelperText error>
                          <span>{error?.message as any}</span>
                        </FormHelperText>
                      )}
                    </Box>
                  </div>
                );
              }}
            </Dropzone>
          );
        }}
      />
    </div>
  );
};

import { styled } from '@mui/material';
const theme = require('../../../themes/baseTheme');

export const RootStyle = styled('div')(() => ({
  display: 'flex',
  borderWidth: 1,
  borderRadius: 5,
  backgroundColor: theme.colors.primaryLight,
  borderColor: theme.colors.primary,
  borderStyle: 'dashed',
  color: '#939393',
  fontSize: 12,
  lineHeight: '17px',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  minHeight: '100px',
  maxWidth: '100%',
  textTransform: 'capitalize'
}));

export const RootInnerStyle = styled('div')(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  width: '100%'
}));

export const ButtonStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontWeight: 500,
  marginBottom: '8px',
  color: theme.palette.primary.main,
  borderRadius: 4,
  borderWidth: 1,
  borderColor: theme.palette.primary.main,
  borderStyle: 'solid',
  minHeight: '32px',
  minWidth: '110px'
}));

export const ThumbStyle = styled('div')(() => ({
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'flex-start',
  width: '100%'
}));

export const ThumbItemStyle = styled('div')(() => ({
  borderRadius: 4,
  border: '1px solid #eaeaea',
  width: '100%',
  maxWidth: 190,
  margin: '0 auto'
}));

export const ThumbItemStyleGallary = styled('div')(() => ({
  width: '100%',
  maxWidth: 100,
  margin: '5px'
}));

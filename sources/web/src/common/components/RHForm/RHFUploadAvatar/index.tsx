import { Box, BoxProps, Button } from '@mui/material'
import { Thumbnail } from 'common/components'
import { isString } from 'lodash'
import { FC } from 'react'

const BoxStyle = {
  position: 'absolute',
  left: 0,
  bottom: 0,
  zIndex: 1,
  p: 1,
  width: '100%',
  textTransform: 'capitalize'
}
interface RHFUploadAvatarProps extends BoxProps {
  name: string
  previewUrl: File | string
  register: any
  width?: number
  height?: number
  onChangeCb?: (e: any) => void
}

export const RHFUploadAvatar: FC<RHFUploadAvatarProps> = ({
  name,
  previewUrl,
  width = 100,
  height = 100,
  register,
  ...props
}) => {
  return (
    <Box className="relative overflow-hidden rounded-full" width={width} height={height} {...props}>
      <Thumbnail imageUrl={isString(previewUrl) ? previewUrl : URL.createObjectURL(previewUrl)} size="1x1" />
      <Button component="label" sx={BoxStyle}>
        <span className="text-sm text-white">更換</span>
        <input {...register} hidden accept="image/*" type="file" />
      </Button>
    </Box>
  )
}

import { Box, BoxProps } from '@mui/material';
import classNames from 'classnames';
import Image from 'next/image';
import React from 'react';

interface ThumbnailProps extends BoxProps {
  imageUrl?: string;
  alt?: string;
  size?: '1x1' | '6x4' | '4x3' | '5x7' | '16x9' | '24x9' | 'brand' | 'main';
  className?: string;
}
export const Thumbnail: React.FC<ThumbnailProps> = ({ imageUrl, className, size = '6x4', sx, ...props }) => {
  let sizeStyle = {};
  switch (size) {
    case '1x1':
      sizeStyle = { paddingBottom: '100%' };
      break;
    case '4x3':
      sizeStyle = { paddingBottom: '75%' };
      break;
    case '5x7':
      sizeStyle = { paddingBottom: '115%' };
      break;
    case '16x9':
      sizeStyle = { paddingBottom: '56.25%' };
      break;
      break;
    case '24x9':
      sizeStyle = { paddingBottom: '37.5%' };
      break;
    case 'brand':
      sizeStyle = { paddingBottom: '80%' };
      break;
    case 'main':
      sizeStyle = { paddingBottom: '53.6%' };
      break;

    default:
      sizeStyle = { paddingBottom: '66.67%' };
  }
  return (
    <Box className={classNames('thumbnail', className)} sx={{ ...sx, ...sizeStyle }} {...props}>
      <Image
        src={imageUrl ? imageUrl : '/iss/assetmanagement/static/images/logo.svg'}
        alt="thumbnail"
        layout="fill"
        priority
      />
    </Box>
  );
};

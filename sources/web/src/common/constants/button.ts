export const SHOW_MORE_BUTTON = 'Xem thêm';
export const SHOW_LESS_BUTTON = 'Ẩn đi';

export const CLOSE_BUTTON = 'Close';
export const RESET_BUTTON = 'Đặt lại';
export const FILTER_BUTTON = 'Lọc ngay';
export const SAVE_BUTTON = 'Lưu';

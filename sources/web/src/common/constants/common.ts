export const TEXT_DATA_NOT_FOUND = 'Data Not Found.';
export const SUCCESS_MESSAGE = 'Thực hiện thành công';
export const DEFAULT_PER_PAGE = 10;
export const TEXT_SUCCESS_ACTION = 'Thực hiện thành công'

export const TOKEN = 'token';
export const TOKEN_AZURE = 'token_azure';

export enum Status {
  LOADING = 'LOADING',
  FAILURE = 'FAILURE',
  SUCCESS = 'SUCCESS'
}

export const AUTH_URL = '/auth?token=';

export enum DATE_FORMAT {
  DATE_TIME = 'DD/MM/YYYY HH:mm:ss'
}

export enum NOT_FOUND_DATA {
  EMPTY = 'N/A'
}
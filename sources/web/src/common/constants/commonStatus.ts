export enum COMMON_STATUS_STAT {
  CREATE_NEWS = 'I0001',
  ISSUED_IMPLEMENT = 'I0002',
  IMPLEMENT = 'I0010',
  DONE = 'I0009',
  CLOSE = 'I0045',
  LOCK = 'I0043',
  CALCULATED = 'I0046',
  COMPLETE = 'I0072'
}

export const COMMON_STATUS_STAT_NAME: { [key: string]: string } = {
  [COMMON_STATUS_STAT.COMPLETE]: 'Hoàn thành'
};

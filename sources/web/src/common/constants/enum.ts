export enum EAppLayout {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE',
  BLANK = 'BLANK'
}

export enum EApiStatus {
  PENDING = 'pending',
  SUCCESS = 'success',
  FAILED = 'failed'
}
export enum ELoading {
  PENDING = 'pending',
  IDLE = 'idle'
}

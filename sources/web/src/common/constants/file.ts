export const FILE = {
  TYPE: 'FILE_TYPE',
  ACCEPT: process.env.uploadAcceptType || {
    'images/png': ['.png'],
    'images/jpg': ['.jpg'],
    'application/pdf': ['.pdf	'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx']
  },
  LIMIT: (Number(process.env.NEXT_PUBLIC_UPLOAD_SIZE_LIMIT) as number) || 2097152,
  STATUS: {
    UPLOAD_FAILED: '上傳失敗!'
  }
};

export enum FILE_TYPE {
  IMAGE_PNG = 'image/png',
  IMAGE_JPG = 'image/jpeg',
  APPLICATION_PDF = 'application/pdf',
  APPLICATION_XML = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
}

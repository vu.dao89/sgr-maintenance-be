import { handleOutputLimitMessage } from 'common/helpers'
import { FILE } from './file'

export const VALID = {
  FIELD_REQUIRED: '',

  NAME_REQUIRED: '',
  PHONE_REQUIRED: '',
  FIRST_NAME_REQUIRED: '',
  LAST_NAME_REQUIRED: '',

  EMAIL_REQUIRED: '',
  EMAIL_FORMAT: '',

  TERM: '',

  PASSWORD_REQUIRED: '',
  PASSWORD_CONFIRM_REQUIRED: '',
  PASSWORD_NOT_MATCH: '',
  PASSWORD_FORMAT: '必須包含 8 個字符。\n一個大寫字母。\n一個小寫字母。\n一個數字和一個特殊字符',
  PASSWORD_REG: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,

  PHONE_REG_EXP:
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
  PHONE_NOT_MATCH: '',

  MESSAGE_REQUIRED: '',

  MAX_LENGTH_4: handleOutputLimitMessage(4),
  MAX_LENGTH_12: handleOutputLimitMessage(12),
  MAX_LENGTH_20: handleOutputLimitMessage(20),
  MAX_LENGTH_40: handleOutputLimitMessage(40),
  MAX_LENGTH_50: handleOutputLimitMessage(50),
  MAX_LENGTH_100: handleOutputLimitMessage(100),
  MAX_LENGTH_255: handleOutputLimitMessage(255),

  MIN_LENGTH_2: handleOutputLimitMessage(2, 'min'),
  MIN_LENGTH_8: handleOutputLimitMessage(8, 'min'),

  DATE_BETWEEN: '',
  INPUT_TYPE_NUMBER: '',

  FILE_LIMIT: `文件大過 ${(FILE.LIMIT / 1048576).toFixed()}MB`,

  PLEASE_ENTER_NUMBER: '',
  FIELD_INVALID: ''
}

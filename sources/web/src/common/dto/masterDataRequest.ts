export interface IEquipmentCategory {
  id: string;
  code: string;
  description: string;
}

export interface IEquipmentType {
  id: string;
  code: string;
  description: string;
}

export interface IMaintenancePlant {
  id: string;
  code: string;
  plantName: string;
  planningPlan: string;
}

export interface ISystemStatus {
  id: string;
  statusId: string;
  code: string;
  userStatusProfile: string;
  userStatus: string;
}

export interface IFunctionalLocation {
  id: string;
  functionalLocationId: string;
  maintenancePlantId: string;
  maintenancePlant: IMaintenancePlant;
  description: string;
  maintenanceWorkCenterId: string;
  costCenter: string;
  plannerGroup: string;
  planningPlantId: string;
  weight: string;
  createDate: string;
  startupDate: string;
  endOfUseDate: string;
  measuringPoints: [];
  characteristics: [];
}

export interface IMaintenancePlant extends IBaseEntity {
  code: string;
  planName: string;
  planningPlant: string;
}

export interface IPartners extends IBaseEntity {
  counter: string;
  partnerFunction: string;
  partnerNumber: string;
  objectTypeCode: string;
}

export interface ICharacteristics extends IBaseEntity {
  charValue: string;
  valueRel: string;
  objectClassFlag: string;
  internalCounter: string;
  classType: string;
  charValCounter: string;
  charId: string;
  charValDesc: string;
  classId: string;
}

export interface IMeasuringPoint extends IBaseEntity {
  point: string;
  position: string;
  description: string;
  characteristic: ICharacteristics;
  targetValue: string;
  counter: string;
  lowerRangeLimit: string;
  upperRangeLimit: string;
  rangeUnit: string;
  text: string;
  codeGroup: string;
  lastMeasuringPointValue: string;
}
export interface IMaintenanceWorkCenter extends IBaseEntity {
  description: string;
  code: string;
}

export interface ICodeItem extends IBaseEntity {
  catalogProfile: string;
  catalogProfileText: string;
  catalog: string;
  catalogDesc: string;
  codeGroup: string;
  codeGroupDesc: string;
  code: string;
  codeDesc: string;
}

export interface IGroupDocument<T> {
  files?: T[];
  images?: T[];
}

export interface IDocumentBase {
  id?: string;
  notificationCode?: string;
  documentId: string;
  globalDocument: IGlobalDocument;
}

export interface IGlobalDocument {
  id: string;
  fileName: string;
  fileType: string;
  originalUrl: string;
  thumbnailUrl: string;
  uploadedAt: string;
  uploadedAtTime: string;
  fileSize?: string;
}

export interface IPriorityType extends IBaseEntity {
  type: string;
  priority: string;
  description: string;
  colorName: string;
  colorCode: string;
}

export interface ICommonStatus {
  id: string;
  code: string;
  status: string;
  description: string;
  colorName: string;
  colorCode: string;
}
import dayjs from 'dayjs';
import { camelCase, isFunction, isNumber, isObject, snakeCase } from 'lodash';
import Router from 'next/router';

/**
 * redirect
 * @param path string
 * @returns void
 */
export const redirect = (path: string) => {
  if (typeof window === 'undefined') {
    return;
  }

  Router.push(path);
};

export const replaceUrl = (path: string, query: string) => {
  delete Router.query.shouldRefetchUser;
  Router.replace({ pathname: path, query }, undefined, { shallow: true });
};

/**
 * parserJson
 * @param data any
 * @returns json
 */
export const parserJson = (data: any) => {
  if (!data) return;

  return JSON.parse(JSON.stringify(data));
};

/**
 * sleep
 * @param ms number
 * @returns void
 */
export const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

/**
 * stackCallback
 * @param cb
 * @param time
 * @returns
 */
export const stackCallback = (cb: Function, time = 1000) => {
  if (!isFunction(cb)) return;
  setTimeout(() => cb(), time);
};
/**
 * handleOutputLimitMessage
 * @param type min or max
 * @param size number
 * @returns string
 */
export const handleOutputLimitMessage = (size: number, type: 'min' | 'max' = 'max') => {
  return type === 'max' ? `Must be ${size} characters or less` : `Must be ${size} characters or more`;
};

/**
 * findOcc
 * @param arr Array
 * @param key string
 * @returns void
 */
export const findOcc = (arr: [], key: string) => {
  const arr2: any[] = [];
  arr.forEach(x => {
    if (arr2.some(val => val[key] == x[key])) {
      arr2.forEach(k => k[key] === x[key] && k['occurrence']++);
    } else {
      const a: any = {};
      a[key] = x[key];
      a['occurrence'] = 1;
      arr2.push(a);
    }
  });
  return arr2;
};

export const getFileName = (url: string) => {
  const filename = url.substring(url.lastIndexOf('/') + 1);
  return filename;
};

export const checkAllItemIsStringArr = (array: Array<string | File>) => {
  return array.every((item: string | File) => typeof item === 'string');
};

export const toCamelCase: any = (obj: any) => {
  if (!obj || typeof obj !== 'object') return obj;

  if (!Array.isArray(obj)) {
    return Object.fromEntries(Object.entries(obj).map(([key, val]) => [camelCase(key), toCamelCase(val)]));
  } else {
    return obj.map(el => toCamelCase(el));
  }
};

export const toSnakeCase: any = (obj: any) => {
  if (!obj || typeof obj !== 'object') return obj;

  if (!Array.isArray(obj)) {
    return Object.fromEntries(Object.entries(obj).map(([key, val]) => [snakeCase(key), toSnakeCase(val)]));
  } else {
    return obj.map(el => toSnakeCase(el));
  }
};

export const convertObjectToQueryParams = (paramObj: { [key: string]: any }) => {
  let paramStr = '';

  if (isObject(paramObj)) {
    paramStr = Object.keys(paramObj)
      .map(function (key) {
        return key + '=' + (paramObj as any)[key];
      })
      .join('&');
  }

  return paramStr;
};

export const convertStringToDateTime = (timeString: string, format?: string): string => {
  if (!timeString) return '';

  const newFormat = format ? format : 'DD/MM/YYYY';
  const date = dayjs(timeString).format(newFormat);

  return date;
};

export const toDateString = (yyyymmddFormat: string) => {
  if (!isNumber(parseInt(yyyymmddFormat))) {
    return '';
  }
  const date: Date = new Date(
    +yyyymmddFormat.substr(0, 4),
    +yyyymmddFormat.substr(4, 2) - 1,
    +yyyymmddFormat.substr(6, 2)
  );

  return dayjs(date.toDateString()).format('MMM DD, YYYY');
};

export const findItemInArrayByKey = (key: string, value: any, array: any[]) => {
  if (!array.length && !key) return;
  let result: any;

  array.map((item: any) => {
    if (item[key] === value) {
      result = item;
      return;
    }
  });

  return result;
};

export const convertDataOption = (label: string, value: any, data: any) => {
  if (!data?.length) return [];
  const newData = [...data];

  return newData.map((item: any) => ({ label: item[label], value: item[value] }));
};

export const handleRedirect = () => {
  const currentPathURL = Router.pathname;
  const currentPathURLs = currentPathURL.split('/');
  if (currentPathURLs.length > 1) {
    Router.push(`/${currentPathURLs[1]}`);
  } else {
    Router.back();
  }
};

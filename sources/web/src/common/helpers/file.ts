import { IDocumentBase, IGlobalDocument } from 'common/dto';

export const groupFileDocument: any = (documents: IDocumentBase[] | IGlobalDocument[]) => {
  const result = {
    files: [],
    images: []
  } as any;

  if (!documents.length) return result;
  const regexImg = /^image/;
  const regexApplication = /^application/;

  documents.forEach((item: any) => {
    if (item?.globalDocument && regexImg.test(item?.globalDocument?.fileType)) {
      result.images.push(item);
    } else if (!item?.globalDocument && regexImg.test(item?.fileType)) {
      result.images.push(item);
    }

    if (item?.globalDocument && regexApplication.test(item?.globalDocument?.fileType)) {
      result.files.push(item);
    } else if (!item?.globalDocument && regexApplication.test(item?.fileType)) {
      result.files.push(item);
    }
  });

  return result;
};

import { IDepartmentRole, IPermission, IRole, IUser } from '@modules/User';
import { REGEX_SEARCH_PLAN_CODE } from 'common/constants';
import { uniq } from 'lodash';

export const handleGetMaintenanPlanCode = (user: IUser) => {
  if (!user.departmentRoles.length) return [];
  const result: string[] = [];

  user.departmentRoles?.map((departmentRole: IDepartmentRole) => {
    departmentRole?.roles?.map((role: IRole) => {
      role?.permissions?.map((permission: IPermission) => {
        if (REGEX_SEARCH_PLAN_CODE.test(permission.code)) {
          const splitPlants = permission.code.split('_');

          result.push(String(splitPlants.pop()));
        }
      });
    });
  });

  return uniq(result).toString();
};

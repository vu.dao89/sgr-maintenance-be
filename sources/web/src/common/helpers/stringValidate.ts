/**
 * stringValidate
 * @param value
 * @returns
 */
export const stringValidate = (value: string) => {
  return {
    isAlphabetical: () => {
      return !new RegExp(/[0-9`~!@#$%^&*()<>?/,\-_+=:;\\|[\]{}\s]/).test(value);
    },
    isAlphanumericPunctuationSpecial: () => {
      return !new RegExp(/[^ -~]+/g).test(value);
    },
    isSpecialAlphabetical: () => {
      return !new RegExp(/[0-9]/).test(value);
    },
    isAlphanumric: () => {
      return !new RegExp(/^[a-zA-Z0-9]*$/gm).test(value);
    },
    isSpecialAlphanumeric: () => {
      return !new RegExp(/[`~!@#$%^&*()<>?/,\-_+=:;\\|[\]{}\s]/).test(value);
    },
    isNumber: () => {
      return !new RegExp(/^[0-9]*$/gm).test(value);
    },
    maxLength: (length: Number) => {
      return value.length <= length;
    },
    minLength: (length: Number) => {
      return value.length >= length;
    },
    isRequire: () => {
      return !!value;
    }
  };
};

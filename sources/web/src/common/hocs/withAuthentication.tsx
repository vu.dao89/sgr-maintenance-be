import { userActions } from '@modules/User';
import { getToken, removeToken, useAppDispatch } from 'common/app';
import Router from 'next/router';
import { useEffect } from 'react';

export const withAuthentication = (Component: any): any => {
  const AuthHOC = (props: any) => {
    const dispatch = useAppDispatch();

    useEffect(() => {
      const requestUser = async () => {
        await dispatch(userActions.getUserInfo())
          .unwrap()
          .then(res => res && Router.push('/equipment'))
          .catch(() => removeToken());
      };

      if (!getToken()) {
        Router.push('401');
      }

      if (getToken()) {
        requestUser();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
      const requestUser = async () => {
        await dispatch(userActions.getUserInfo())
          .unwrap()
          .catch(() => removeToken());
      };
      if (getToken()) {
        requestUser();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch]);

    return <Component {...props} />;
  };

  return AuthHOC;
};

import { instanceToPlain, plainToClass, Type } from 'class-transformer';
import { isObject } from 'lodash';
import { useRouter } from 'next/router';
import NProgress from 'nprogress';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

export class ResTemplate<T> {
  statusCode!: number;
  message?: string;
  data?: T;
  success!: boolean;
}

const convertDto = <T>(dto: any) => {
  class ResponseTemplateData extends ResTemplate<T> {
    @Type(() => dto)
    data: T | undefined;
  }

  return ResponseTemplateData;
};

type ApiCallerParam = {
  apiCaller: (value?: any) => any;
  messageFail?: boolean | string;
  messageSuccess?: boolean | string;
  setData?: (data: any) => void;
  resDto?: any;
  errorCodeObject?: { [key: string]: any };
  prefixMessage?: string;
};

export function useApiCaller<TRes>({ apiCaller, setData, resDto, messageSuccess, messageFail }: ApiCallerParam) {
  const [loading, setLoading] = useState<boolean>(false);
  const router = useRouter();

  useEffect(() => {
    if (loading) {
      NProgress.start();
    } else {
      NProgress.done();
    }
  }, [loading]);

  const showMessage = (responseData: ResTemplate<TRes>) => {
    if (responseData.statusCode === 404) {
      toast.error('Not found API endpoint');
    }

    if (responseData.statusCode === 401) {
      router.push('/401');
    }

    if (responseData.statusCode === 200) {
      if (messageSuccess === true) {
        messageSuccess = responseData?.message;
      }

      if (messageSuccess) {
        toast.info(messageSuccess);
      }
    }

    if (responseData.statusCode !== 200) {
      if (messageFail && responseData?.message) {
        messageFail = responseData?.message;
      }
      if (messageFail) {
        toast.error(messageSuccess);
      }
    }
  };

  const request = async (dataOrig?: any): Promise<ResTemplate<TRes>> => {
    setLoading(true);
    let newDataOrig = dataOrig;

    //TODO
    if (false) {
      newDataOrig = instanceToPlain(newDataOrig, {
        exposeDefaultValues: true
      }) as any;
    }

    const responseData = await apiCaller(newDataOrig);

    setLoading(false);

    let newResponseData = {} as any;
    if (isObject(responseData?.data)) {
      newResponseData.data = plainToClass(convertDto<TRes>(resDto), responseData?.data, { exposeDefaultValues: true });
    }

    newResponseData.statusCode = responseData?.status;

    if (newResponseData.statusCode === 401) {
      // TODO
      //router.push('/401');
    }

    if (newResponseData.statusCode === 400) {
      toast.error(newResponseData.data.errorMessage);
    }

    if (newResponseData.statusCode === 500) {
      toast.error(newResponseData.data.errorMessage);
    }

    showMessage(newResponseData);

    if (setData) {
      setData(newResponseData.data);
    }

    return newResponseData;
  };

  return {
    loading,
    request,
    setLoading
  };
}

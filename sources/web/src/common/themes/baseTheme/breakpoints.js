module.exports = {
  xs: 0,
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1200,
  xxl: 1400,
  xxxl: 1900
}

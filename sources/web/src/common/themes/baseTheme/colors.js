module.exports = {
  black: '#000000',
  white: '#ffffff',

  primary: '#F7C245',
  primaryLight: '#F9F1D9',
  secondary: '#414143',
  secondaryLight: '#545355',

  typography100: '#C5C7CD',
  typography200: '#AAAFBB',
  typography300: '#646A7A',
  typography400: '#3E3A55',
  typography500: '#0E092A',

  backgroundBody: '#F1F3F9',
  backgroundGray: '#F5F2FD',
  background100: '#FCF8FA',
  background900: '#250E18',

  lineLight: '#D9DCE4',
  greyLight100: '#F7F8F9',
  grey200: '#F2F3F5',

  buttonYellow100: '#FFC20E',
  dark100: '#2B2512',

  error1: '#F74360',
  error2: '#FFE8EB',
  warning1: '#FF934B',
  warning2: '#FFF1E9',
  alert1: '#FBA707',
  alert2: '#FFF8E7',
  success1: '#00C48C',
  success2: '#D9F6EE',
  info1: '#3765DE',
  info2: '#E5EDFE',

  disable: '#9399A9',

  // Tab
  tab: '#758098',

  pink1: '#FD4848'
};

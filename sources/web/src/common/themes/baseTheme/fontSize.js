module.exports = {
  display1: 72,
  display2: 60,
  display3: 48,
  display4: 40,
  heading1: 36,
  heading2: 32,
  heading3: 28,
  heading4: 24,
  heading5: 20,
  heading6: 18,
  base: 16,
  body2: 14,
  caption: 12
}

const fontSize = require('./fontSize')
const lineHeight = require('./lineHeight')
const colors = require('./colors')
const spacing = require('./spacing')
const breakpoints = require('./breakpoints')

module.exports = {
  fontFamily: 'Inter',
  fontSize,
  lineHeight,
  colors,
  spacing,
  breakpoints
}

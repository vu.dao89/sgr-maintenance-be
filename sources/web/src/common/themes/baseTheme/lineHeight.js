module.exports = {
  base: 28,
  heading1: 52,
  heading2: 48,
  heading3: 40,
  heading4: 36,
  heading5: 32,
  body2: 26
}

import { createTheme } from '@mui/material';
const theme = require('./baseTheme');

const Theme = createTheme({
  palette: {
    primary: {
      main: theme.colors.primary,
      light: theme.colors.primaryLight,
      dark: theme.colors.buttonYellow100
    },
    secondary: {
      main: theme.colors.secondary,
      light: theme.colors.secondaryLight
    },
    info: {
      main: theme.colors.info1,
      light: theme.colors.info2
    },
    success: {
      main: theme.colors.success1,
      light: theme.colors.success2
    },
    error: {
      main: theme.colors.error1,
      light: theme.colors.error2
    },
    warning: {
      main: theme.colors.warning1,
      light: theme.colors.warning2
    },
    background: {
      default: theme.colors.white
    },
    text: {
      primary: theme.colors.background900,
      disabled: theme.colors.disabled
    },
    grey: {
      100: theme.colors.typography100,
      200: theme.colors.typography200,
      300: theme.colors.typography300,
      400: theme.colors.typography400,
      500: theme.colors.typography500,
      600: theme.colors.lineLight,
      700: theme.colors.greyLight100,
      800: theme.colors.tab,
      900: theme.colors.grey200
    },
    action: {
      selected: theme.colors.buttonYellow100
    }
  },

  typography: {
    htmlFontSize: theme.fontSize.base,
    fontFamily: [theme.fontFamily].join(','),
    fontSize: 16,
    h1: {
      fontWeight: 600,
      fontSize: 36,
      lineHeight: theme.lineHeight.heading1 / 16
    },
    h2: {
      fontWeight: 600,
      fontSize: theme.fontSize.heading2,
      lineHeight: theme.lineHeight.heading2 / 16
    },
    h3: {
      fontWeight: 600,
      fontSize: theme.fontSize.heading3,
      lineHeight: theme.lineHeight.heading3 / 16
    },
    h4: {
      fontWeight: 600,
      fontSize: theme.fontSize.heading4,
      lineHeight: theme.lineHeight.heading4 / 16
    },
    h5: {
      fontWeight: 600,
      fontSize: theme.fontSize.heading5,
      lineHeight: theme.lineHeight.heading5 / 16
    },
    h6: {
      fontSize: theme.fontSize.heading6
    },
    body1: {
      fontSize: theme.fontSize.base
    },
    body2: {
      fontSize: theme.fontSize.body2,
      lineHeight: theme.lineHeight.body2 / 16
    },
    button: {
      fontWeight: 500
    },
    caption: {
      fontSize: theme.fontSize.caption,
      textTransform: 'uppercase'
    },
    subtitle1: {
      fontSize: theme.fontSize.base,
      fontWeight: 600
    },
    subtitle2: {
      fontSize: theme.fontSize.body2,
      fontWeight: 600
    },
    overline: {
      fontSize: theme.fontSize.caption
    }
  },
  shape: {
    borderRadius: 8
  },
  spacing: 4,
  components: {
    MuiCard: {
      styleOverrides: {
        root: {
          borderRadius: 12
        }
      }
    },
    MuiCardHeader: {
      styleOverrides: {
        root: {
          padding: 24
        }
      }
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: 24
        }
      }
    },
    MuiCardActionArea: {
      styleOverrides: {
        root: {
          padding: 24
        }
      }
    },
    MuiCardActions: {
      styleOverrides: {
        root: {
          padding: 24
        }
      }
    },
    MuiPopover: {
      styleOverrides: {
        paper: {
          borderTopLeftRadius: 4,
          borderBottomLeftRadius: 4,
          borderBottomRightRadius: 4,
          boxShadow: `0px 0px 25px ${theme.colors.borderOpacity}`
        }
      }
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          borderRadius: 12,
          overflow: 'hidden'
        }
      }
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          border: 'none'
        }
      }
    },
    MuiChip: {
      styleOverrides: {
        root: {
          borderRadius: 20,
          color: 'white',
          '&.MuiChip-colorDefault': {
            color: theme.colors.background900
          }
        },
        label: {
          paddingLeft: 16,
          paddingRight: 16
        }
      }
    },
    MuiTab: {
      styleOverrides: {
        root: {
          fontWeight: 700,
          color: theme.colors.tab,
          paddingLeft: 0,
          paddingRight: 0,
          marginLeft: 10,
          marginRight: 10,
          minWidth: 'unset',
          '&.Mui-selected': {
            color: theme.colors.typography500
          }
        }
      }
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: theme.colors.typography300,
          marginBottom: 8
        }
      }
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          textTransform: 'initial'
        }
      }
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          '&.Mui-disabled.MuiMenuItem-root': {
            opacity: 0.75
          }
        }
      }
    },
    MuiFormGroup: {
      styleOverrides: {
        root: {
          marginTop: 16
        }
      }
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          '& MuiInputBase-input': { paddingTop: 12.5, paddingBottom: 12.5 }
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          paddingLeft: 24,
          paddingRight: 24,
          borderRadius: 24,
          boxShadow: 'none'
        }
      }
    },
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          '&.search': {
            '& .MuiAutocomplete-endAdornment': {
              '& .MuiButtonBase-root': {
                marginRight: 4
              },
              '& .MuiAutocomplete-popupIndicatorOpen': {
                transform: 'none'
              }
            }
          }
        }
      }
    }
  }
});

export { theme };
export default Theme;

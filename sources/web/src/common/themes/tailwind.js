const theme = require('./baseTheme');

module.exports = {
  theme: {
    colors: {
      primary: theme.colors.primary,
      primaryLight: theme.colors.primaryLight,
      secondary: theme.colors.secondary,
      green: {
        DEFAULT: theme.colors.success
      },
      red: {
        DEFAULT: theme.colors.error
      },
      yellow: {
        DEFAULT: theme.colors.warning
      },
      blue: {
        DEFAULT: theme.colors.info
      },
      white: {
        DEFAULT: theme.colors.white
      },
      error: {
        DEFAULT: theme.colors.error
      },
      transparent: 'transparent',
      pink: {
        DEFAULT: theme.colors.pink1
      }
    }
  }
};

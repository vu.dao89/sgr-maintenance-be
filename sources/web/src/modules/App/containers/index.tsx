import 'react-toastify/dist/ReactToastify.css';
import { Box } from '@mui/material';
import { Loading } from 'common/components';
import { delay } from 'lodash';
import { Fragment, useEffect, useState } from 'react';
import { ToastContainer } from 'react-toastify';
import { useIsMounted } from 'usehooks-ts';
import { useSelectorLoaderStore } from '../redux';

interface AppContainerProps {
  children: any;
}

const AppContainer: React.FC<AppContainerProps> = ({ children }) => {
  const [loader, setLoader] = useState(true);
  const loading = useSelectorLoaderStore();
  const isMounted = useIsMounted();

  useEffect(() => {
    delay(() => {
      if (isMounted()) setLoader(false);
    }, 1000);
  }, [isMounted]);

  if (loader) return <Fragment />;
  return (
    <Box component="main" className="h-screen overflow-hidden">
      {children}
      <ToastContainer
        position="top-right"
        autoClose={2000}
        limit={5}
        hideProgressBar={false}
        closeOnClick={false}
        pauseOnHover={false}
        pauseOnFocusLoss={false}
      />
      {loading && <Loading isFullScreen />}
    </Box>
  );
};

export default AppContainer;

import { createSlice } from '@reduxjs/toolkit';
import reducers, { defaultState } from './reducers';

const { actions, reducer } = createSlice({
  name: 'app',
  initialState: { ...defaultState },
  reducers,
  extraReducers: () => {}
});

const extraActions = {
  ...actions
};

export * from './selectors';
export { extraActions as appActions, reducer as appReducer };

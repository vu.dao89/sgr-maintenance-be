import { IAppState } from '@modules/App';
import { set } from 'lodash';

export const defaultState: IAppState = {
  loader: false,
  isShowSidebar: true,
  loading: 'idle',
  currentRequestId: undefined,
  error: null
};

const reducers = {
  onToggleLoader: (state: any) => {
    set(state, 'loader', !state.loader);
  },
  onToggleSidebar: (state: any) => {
    set(state, 'isShowSidebar', !state.isShowSidebar);
  }
};

export default reducers;

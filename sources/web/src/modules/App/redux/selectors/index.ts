import { RootState } from 'common/app';
import { useSelector } from 'react-redux';

/**
 * useSelectorLoaderStore
 */
export const useSelectorLoaderStore = () => {
  return useSelector<RootState, boolean>(state => state.app.loader);
};

/**
 * useSelectorSidebarStore
 */
export const useSelectorSidebarStore = (): boolean => {
  return useSelector<RootState, boolean>(state => state.app.isShowSidebar);
};

export interface IAppState {
  loader: boolean;
  isShowSidebar: boolean;
  currentRequestId: TCurrentRequest;
  loading: TLoading;
  error: any;
}

import { IEquipmentDetailItem } from '@modules/Equipment';
import { Box, BoxProps, Button, Card, CardContent, CardMedia, Chip, Popover, Typography } from '@mui/material';
import classNames from 'classnames';
import { NOT_FOUND_DATA, SHOW_LESS_BUTTON, SHOW_MORE_BUTTON } from 'common/constants';
import { isEmpty } from 'lodash';
import Image from 'next/image';
import React, { useEffect, useRef, useState } from 'react';

interface DataProp extends BoxProps {
  data?: IEquipmentDetailItem;
}

export const CardContentComponent: React.FC<DataProp> = ({ data }) => {
  const spanRef = useRef<HTMLSpanElement>(null);
  const [isViewMore, setIsViewMore] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleOpenPopover = (event: React.MouseEvent<HTMLButtonElement>) => {
    setIsViewMore(!isViewMore);
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const handleClose = () => {
    setIsViewMore(false);
    setAnchorEl(null);
  };

  useEffect(() => {
    if (isEmpty(data)) return;
    //TODO
  }, [data]);

  return (
    <Card>
      <CardContent sx={{ p: 4 }}>
        <CardMedia
          sx={{ borderRadius: 2, objectFit: 'cover' }}
          component="img"
          height="240"
          image={data?.thumbnailUrl || '/iss/assetmanagement/static/images/no-image.png'}
        />
        <Typography component="h4" variant="h4">
          {data?.description}
        </Typography>
        <Box mt={2} className="flex" gap={3}>
          {data?.equipmentType && <Chip label={data?.equipmentType?.description} color="info" sx={{ fontSize: 14 }} />}
          {data?.equipmentCategory && (
            <Chip label={data?.equipmentCategory?.description} color="info" sx={{ fontSize: 14 }} />
          )}
        </Box>
        <Box ref={spanRef} position="relative">
          {data?.longDesc && (
            <Typography
              mt={4}
              variant="body2"
              textAlign="justify"
              component="span"
              className={classNames('inline-block', !isViewMore && 'line-clamp-3')}
            >
              {data?.description?.length >= 175 ? (
                <>
                  {data.description?.slice(0, 175)} ...
                  <Button
                    onClick={(e: any) => handleOpenPopover(e)}
                    size="small"
                    color="info"
                    component="span"
                    disableRipple
                    className="inline-block p-0 text-sm capitalize hover:bg-transparent min-w-min  bg-white"
                  >
                    {!isViewMore ? SHOW_MORE_BUTTON : SHOW_LESS_BUTTON}
                  </Button>
                  <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'left'
                    }}
                  >
                    <Typography sx={{ fontSize: 14, px: 4, py: 5, maxWidth: 700, textAlign: 'justify' }}>
                      {data.description}
                    </Typography>
                  </Popover>
                </>
              ) : (
                data?.description
              )}
            </Typography>
          )}
        </Box>

        <Box mt={4} p={'8px 16px'} borderRadius={'20px'} className="flex " sx={{ backgroundColor: 'primary.light' }}>
          <Image
            src="/iss/assetmanagement/static/icons/location.svg"
            alt="Icon"
            width={17}
            height={20}
            className="mr-[10px]"
          />
          <Typography ml={3} fontSize={14}>
            {data?.functionalLocation?.description || NOT_FOUND_DATA.EMPTY}
          </Typography>
        </Box>
        <Box sx={{ p: 0, mt: 5 }}>
          <Button
            disableRipple
            fullWidth
            color="primary"
            type="submit"
            variant="contained"
            sx={{ maxHeight: 40, borderRadius: '10px', textTransform: 'capitalize', fontWeight: 500, fontSize: 14 }}
          >
            Tạo hành động
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
};

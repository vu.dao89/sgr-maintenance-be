import { IFileData } from '@modules/Equipment';
import { Box, BoxProps, Card, Typography } from '@mui/material';
import Image from 'next/image';

interface DataProp extends BoxProps {
  data: IFileData;
  allowClear?: boolean;
}

export const FileDownloadComponent: React.FC<DataProp> = ({ data, allowClear }) => {
  return (
    <Card variant="outlined" sx={{ p: 4 }}>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Box className="flex items-center">
          <Image src="/iss/assetmanagement/static/icons/filePdf.svg" alt="Icon" width={26} height={30} />
          <Box ml={'14px'}>
            <Typography variant="body2">{data?.name}</Typography>
            <Typography variant="caption" color={'grey.300'}>
              {data?.date} {data?.creator && ` | ${data?.creator}`}
            </Typography>
          </Box>
        </Box>
        {allowClear && <Image src="/iss/assetmanagement/static/icons/close.svg" alt="Icon" width={24} height={24} />}
      </Box>
    </Card>
  );
};

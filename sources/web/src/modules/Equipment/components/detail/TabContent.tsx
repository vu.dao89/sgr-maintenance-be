import { BoxProps } from '@mui/material';
import { RightCardContent } from 'common/components';
import {
  EQUIPMENT_TAB_CONTENT,
  EQUIPMENT_TAB_DATA,
  IEquipmentDetailItem,
  IFormRollerEquipment,
  IHierarchyItem
} from 'modules/Equipment';
import React, { useState } from 'react';
import {
  DocumentTabComponent,
  HierarchicalTabComponent,
  MeasurePointTabComponent,
  NatureTabComponent,
  PreviewTabComponent
} from './tab';

interface DataProp extends BoxProps {
  loadingUpload?: boolean;
  loadingMeasPoint?: boolean;
  data?: IEquipmentDetailItem;
  hierarchy?: IHierarchyItem[];
  handleSubmitForm?: (data: IFormRollerEquipment, isCounterX?: boolean) => void;
  handleUpload?: any;
}

export const TabContentComponent: React.FC<DataProp> = ({ data, hierarchy, loadingMeasPoint, loadingUpload, handleSubmitForm, handleUpload }) => {
  const [currentTab, setCurrentTab] = useState<number>(0);

  const handleChange = (_e: React.SyntheticEvent, newValue: number) => {
    setCurrentTab(newValue);
  };

  const renderTab = () => {
    switch (currentTab) {
      case EQUIPMENT_TAB_CONTENT.MEASURE_POINT:
        return (
          <MeasurePointTabComponent
            equipment={data}
            loadingMeasPoint={loadingMeasPoint}
            handleSubmitForm={handleSubmitForm}
          />
        );
      case EQUIPMENT_TAB_CONTENT.NATURE:
        return <NatureTabComponent data={data} />;
      case EQUIPMENT_TAB_CONTENT.HIERARCHICAL:
        return <HierarchicalTabComponent hierarchy={hierarchy} data={data} />;
      case EQUIPMENT_TAB_CONTENT.DOCUMENT:
        return <DocumentTabComponent loadingUpload={loadingUpload} data={data} handleUpload={handleUpload} />;

      default:
        return <PreviewTabComponent data={data} />;
    }
  };

  return (
    <RightCardContent tabs={EQUIPMENT_TAB_DATA} currentTab={currentTab} handleChange={handleChange}>
      {renderTab()}
    </RightCardContent>
  );
};

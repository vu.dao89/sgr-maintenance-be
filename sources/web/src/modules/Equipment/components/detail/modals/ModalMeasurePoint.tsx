import { yupResolver } from '@hookform/resolvers/yup';
import { validationSchema } from '@modules/Equipment/containers/detail/validate';
import { Box, BoxProps, FormGroup, Grid, Radio, Typography } from '@mui/material';
import { FormLabelCustom, Modal, RHFRadio, RHFTextarea, RHFTextInput } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import { IMeasuringPoint } from 'common/dto';
import { EQUIPMENT_COUNTER, IEquipmentDetailItem, IFormRollerEquipment } from 'modules/Equipment';
import { useEffect, useRef } from 'react';
import { useForm } from 'react-hook-form';

interface DataProp extends BoxProps {
  open: boolean;
  equipment?: IEquipmentDetailItem;
  measurePoint?: IMeasuringPoint;
  onSubmit?: any;
  onClose?: any;
}

const initFormData: IFormRollerEquipment = {
  isMeasDiff: 0,
  read: '',
  measNote: ''
};

export default function ModalMeasurePoint({ open, equipment, measurePoint, onClose, onSubmit }: DataProp) {
  const form = useRef<HTMLButtonElement>(null);
  const { control, handleSubmit, reset } = useForm<any>({
    defaultValues: initFormData,
    resolver: yupResolver(validationSchema)
  });
  const dataArea = [
    { value: 0, label: 'Giá trị đo hiện tại', control: <Radio /> },
    { value: 1, label: 'Giá trị đo khác biệt', control: <Radio /> }
  ];

  const onSubmitForm = (data: any) => {
    onClose();

    const isMeasRead = measurePoint?.counter === EQUIPMENT_COUNTER.MEASURE_POINT;
    data.measPoint = measurePoint?.point;
    data.isMeasDiff = Number(data.isMeasDiff);

    onSubmit(data, isMeasRead);
  };

  useEffect(() => {
    reset(initFormData);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      onSubmit={() => form?.current?.click()}
      title={`Lấy số đọc cho ${measurePoint?.description}`}
      btnResetText="Đặt lại"
      btnSubmitText="Lấy ngay"
    >
      <form onSubmit={handleSubmit(onSubmitForm)}>
        <button hidden={true} ref={form} type="submit" />
        <Box mt={4}>
          <Grid container columnSpacing={6}>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 0 }}>
                <FormLabelCustom>Thiết bị </FormLabelCustom>
                <Typography>{equipment?.description || NOT_FOUND_DATA.EMPTY}</Typography>
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}></Grid>
            {measurePoint?.counter == EQUIPMENT_COUNTER.MEASURE_POINT && (
              <Grid item xs={12} md={6}>
                <FormGroup>
                  <FormLabelCustom>Theo khu vực chức năng</FormLabelCustom>
                  <RHFRadio
                    sx={{ justifyContent: 'space-between', mt: 0 }}
                    id="isMeasDiff"
                    name="isMeasDiff"
                    row
                    control={control}
                    options={dataArea}
                  />
                </FormGroup>
              </Grid>
            )}
            <Grid item xs={12} md={6}>
              <FormGroup>
                <FormLabelCustom required>Nhập giá trị đo</FormLabelCustom>
                <RHFTextInput id="read" name="read" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6} mt={4}>
              <FormGroup sx={{ mt: 0 }}>
                <FormLabelCustom>Đơn vị đo</FormLabelCustom>
                <Typography>{measurePoint?.rangeUnit || NOT_FOUND_DATA.EMPTY}</Typography>
              </FormGroup>
            </Grid>
            <Grid item xs={12} mt={4}>
              <FormGroup sx={{ mt: 0 }}>
                <FormLabelCustom>Ghi chú</FormLabelCustom>
                <RHFTextarea id="measNote" name="measNote" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
          </Grid>
        </Box>
      </form>
    </Modal>
  );
}

import { Box, BoxProps, Card, CardContent, Grid } from '@mui/material';
import { FileDownloadComponent, ImageCardComponent, Loading, ModalConfirmDelete, RHFFile } from 'common/components';
import { IDocumentEquipment, IEquipmentDetailItem } from 'modules/Equipment';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

interface DataProp extends BoxProps {
  data?: IEquipmentDetailItem;
  loadingUpload?: boolean;
  handleUpload?: any;
}

export const DocumentTabComponent: React.FC<DataProp> = ({ data, loadingUpload, handleUpload }) => {
  const { control } = useForm<any>({
    defaultValues: {}
  });
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);

  const handleOpenModal = () => {
    setConfirmDelete(!confirmDelete);
  };

  return (
    <Box mt={5}>
      <Box>
        <RHFFile
          id="files"
          name="files"
          loading={loadingUpload}
          control={control}
          handleUpload={handleUpload}
          multiple
        />
      </Box>
      {!loadingUpload ? (
        <>
          {data?.groupDocuments?.files?.length ? (
            <Box mt={3}>
              <Grid container rowSpacing={2} columnSpacing={2}>
                {data?.groupDocuments?.files?.map((item: IDocumentEquipment, index: number) => (
                  <Grid item key={index} xs={12} sm={6} md={4}>
                    <FileDownloadComponent
                      className="[&:not(:first-child)]:mt-3"
                      data={item.globalDocument}
                      allowClear
                      onDelete={handleOpenModal}
                    />
                  </Grid>
                ))}
              </Grid>
            </Box>
          ) : null}
          {data?.groupDocuments?.images?.length ? (
            <Card variant="outlined" sx={{ mt: 3 }}>
              <CardContent>
                <Grid container columnSpacing={2} rowGap={4}>
                  {data?.groupDocuments?.images?.map((item: IDocumentEquipment, key: number) => (
                    <Grid key={key} item xs={12} sm={6} md={4} lg={3}>
                      <ImageCardComponent id={item.id} key={key} data={item.globalDocument} />
                    </Grid>
                  ))}
                </Grid>
              </CardContent>
            </Card>
          ) : null}
        </>
      ) : (
        <Loading />
      )}
      <ModalConfirmDelete open={confirmDelete} />
    </Box>
  );
};

import TreeView from '@mui/lab/TreeView';
import { Box, BoxProps, Card, CardContent } from '@mui/material';
import { InnerLayout } from 'common/components';
import { IEquipmentDetailItem, IHierarchyItem } from 'modules/Equipment';
import Image from 'next/image';
import Treeline from './Treeline';
interface DataProp extends BoxProps {
  hierarchy?: IHierarchyItem[];
  data?: IEquipmentDetailItem;
}

export const HierarchicalTabComponent: React.FC<DataProp> = ({ data, hierarchy }) => {
  const handleDefineNodeExpand = (items: any, result: string[]): string[] => {
    if (items.length) {
      items.map((item: any) => {
        result.push(item.element.id);
        if (item.children) handleDefineNodeExpand(item.children, result);
      });
    }

    return result;
  };

  return (
    <InnerLayout hasData={!!hierarchy?.length}>
      <Box mt={5}>
        <Card variant="outlined">
          <CardContent>
            <TreeView
              defaultSelected={String(data?.equipmentId)}
              defaultExpanded={handleDefineNodeExpand(hierarchy, [])}
              defaultCollapseIcon={
                <Image
                  src="/iss/assetmanagement/static/icons/collapse.svg"
                  alt="Icon"
                  width={44}
                  height={44}
                  className="ml-[-10px]"
                />
              }
              defaultExpandIcon={
                <Image
                  src="/iss/assetmanagement/static/icons/expand.svg"
                  alt="Icon"
                  width={44}
                  height={44}
                  className="ml-[-10px]"
                />
              }
              defaultEndIcon={
                <Image
                  src="/iss/assetmanagement/static/icons/collapse.svg"
                  alt="Icon"
                  width={44}
                  height={44}
                  className="ml-[-10px]"
                />
              }
            >
              {hierarchy?.map((item: IHierarchyItem, key: number) => (
                <Treeline key={key} data={item} />
              ))}
            </TreeView>
          </CardContent>
        </Card>
      </Box>
    </InnerLayout>
  );
};

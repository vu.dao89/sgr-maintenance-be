import { Box, BoxProps, Button, Grid, Typography } from '@mui/material';
import { CardItem, FormDataValue, InnerLayout } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import { IMeasuringPoint } from 'common/dto';
import { IEquipmentDetailItem } from 'modules/Equipment';
import { useState } from 'react';
import ModalMeasurePoint from '../modals/ModalMeasurePoint';

interface DataProp extends BoxProps {
  equipment?: IEquipmentDetailItem;
  handleSubmitForm?: any;
  loadingMeasPoint?: boolean;
}

export const MeasurePointTabComponent: React.FC<DataProp> = ({ equipment, loadingMeasPoint, handleSubmitForm }) => {
  const [openModalMeasurePoint, setOpenModalMeasurePoint] = useState<boolean>(false);
  const [selectedMeasurePoint, setSelectedMeasurePoint] = useState<IMeasuringPoint>();

  return (
    <InnerLayout loading={loadingMeasPoint} hasData={!!equipment?.measuringPoints?.length}>
      <Box mt={2}>
        {equipment?.measuringPoints?.map((item: IMeasuringPoint, index: number) => (
          <Box key={index} sx={{ mt: 3 }}>
            <CardItem title={`Điểm đo: ${item.description}`}>
              <Grid container>
                <Grid item xs={12}>
                  <FormDataValue label="Đặc tính" value={item?.characteristic?.charValDesc || NOT_FOUND_DATA.EMPTY} />
                </Grid>
                <Grid item xs={6}>
                  <FormDataValue label="Giá trị nhỏ nhất" value={item?.lowerRangeLimit || NOT_FOUND_DATA.EMPTY} />
                </Grid>
                <Grid item xs={6}>
                  <FormDataValue label="Giá trị tối đa" value={item?.upperRangeLimit || NOT_FOUND_DATA.EMPTY} />
                </Grid>
                <Grid item xs={12}>
                  <FormDataValue label="Giá trị đo gần nhất" value={item?.point || NOT_FOUND_DATA.EMPTY} />
                </Grid>
                <Grid item xs={12}>
                  <Box
                    mt={4}
                    borderRadius={3}
                    bgcolor={'grey.700'}
                    sx={{
                      p: '12px 16px',
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center'
                    }}
                  >
                    <Typography variant="body2" color={'grey.800'}>
                      {item?.text || NOT_FOUND_DATA.EMPTY}
                    </Typography>
                    <Button
                      onClick={() => {
                        setSelectedMeasurePoint(item);
                        setOpenModalMeasurePoint(true);
                      }}
                      disableRipple
                      size="large"
                      color="primary"
                      type="submit"
                      variant="contained"
                      sx={{ fontSize: 14, fontWeight: 500 }}
                    >
                      Lấy số đọc
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </CardItem>
          </Box>
        ))}
      </Box>
      <ModalMeasurePoint
        open={openModalMeasurePoint}
        equipment={equipment}
        measurePoint={selectedMeasurePoint}
        onSubmit={handleSubmitForm}
        onClose={() => setOpenModalMeasurePoint(false)}
      />
    </InnerLayout>
  );
};

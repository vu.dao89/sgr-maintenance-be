import { IEquipmentDetailItem } from '@modules/Equipment';
import { Box, BoxProps, Grid, Typography } from '@mui/material';
import { CardItem, InnerLayout } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import { ICharacteristics } from 'common/dto';
import { groupBy } from 'lodash';

interface DataProp extends BoxProps {
  data?: IEquipmentDetailItem;
}

export const NatureTabComponent: React.FC<DataProp> = ({ data }) => {
  const characteristics = groupBy(data?.characteristics, 'classId');

  return (
    <InnerLayout hasData={!!data?.characteristics?.length}>
      <Box mt={5}>
        <Grid container columnSpacing={6} rowSpacing={'20px'}>
          {Object.keys(characteristics).map((item: any, index: number) => (
            <Grid key={index} item xs={12} md={6}>
              <CardItem title={`Nhóm đặc tính ${item}`}>
                <Box key={index} mt={4} sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  <Typography variant="body2" mb={1}>
                    Tên đặc tính
                  </Typography>
                  <Typography variant="body2" mb={1}>
                    Giá trị
                  </Typography>
                </Box>
                {characteristics[item].map((e: ICharacteristics, index: number) => (
                  <Box
                    py={4}
                    key={index}
                    borderColor={'grey.600'}
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      ':not(:last-child)': { borderBottom: '1px solid', borderColor: 'grey.600' }
                    }}
                  >
                    <Typography variant="body2" fontWeight={500} mb={1}>
                      {e?.charValDesc || NOT_FOUND_DATA.EMPTY}
                    </Typography>
                    <Typography variant="body1" fontWeight={500} mb={1}>
                      {e?.charValue || NOT_FOUND_DATA.EMPTY}
                    </Typography>
                  </Box>
                ))}
              </CardItem>
            </Grid>
          ))}
        </Grid>
      </Box>
    </InnerLayout>
  );
};

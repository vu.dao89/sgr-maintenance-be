import { IEquipmentDetailItem } from '@modules/Equipment';
import { Box, BoxProps, Grid } from '@mui/material';
import { CardItem, FormDataValue, RenderMapComponent } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import { IPartners } from 'common/dto';

interface DataProp extends BoxProps {
  data?: IEquipmentDetailItem;
}

export const PreviewTabComponent: React.FC<DataProp> = ({ data }) => {
  // TODO mock data
  const center = { lat: 20.865139, lng: 106.68383 };
  const zoom = 10;

  return (
    <Box mt={5}>
      <Box sx={{ '> div': { minHeight: 240, borderRadius: 2 } }}>
        <RenderMapComponent center={center} zoom={zoom} />
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Thông tin thiết bị">
          <Grid container>
            <Grid item xs={6}>
              <FormDataValue
                label="Loại thiết bị"
                value={data?.equipmentCategory?.description || NOT_FOUND_DATA.EMPTY}
              />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Nhóm thiết bị" value={data?.equipmentType?.description || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            {/* missing */}
            <Grid item xs={6}>
              <FormDataValue
                label="Thiết bị cha"
                value={data?.equipmentCategory?.description || NOT_FOUND_DATA.EMPTY}
              />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Thông tin bổ sung">
          <Grid container>
            <Grid item xs={6}>
              <FormDataValue label="Nơi bảo trì" value={data?.maintenancePlant?.planName || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Tổ đội" value={data?.maintenanceWorkCenter?.description || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Mã nội bộ" value={data?.sortField || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Bộ phận chịu chi phí" value={data?.costCenter || NOT_FOUND_DATA.EMPTY} />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Thông tin sản xuất">
          <Grid container>
            <Grid item xs={6}>
              <FormDataValue label="Trọng lượng" value={data?.weight || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Kích thước" value={data?.size || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Nhà sản xuất" value={data?.manufacturer || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Số model" value={data?.modelNumber || NOT_FOUND_DATA.EMPTY} />
            </Grid>

            <Grid item xs={6}>
              <FormDataValue label="Mã thiết bị của nhà sản xuất" value={data?.modelNumber || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Số serial" value={data?.manuSerialNo || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày bắt đầu" value={data?.startupDate || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày kết thúc" value={data?.endOfUseDate || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Nước sản xuất" value={data?.manuCountry || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Năm sản xuất" value={data?.constYear || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Mã QR code" value={data?.qrCode || NOT_FOUND_DATA.EMPTY} />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Đối tượng">
          <Grid container>
            {data?.partners?.length
              ? data?.partners?.map((item: IPartners, key: number) => (
                  <Grid key={key} item xs={6}>
                    <FormDataValue
                      label="VW - person response"
                      value={item?.partnerFunction + ' ' + item?.objectTypeCode || NOT_FOUND_DATA.EMPTY}
                    />
                  </Grid>
                ))
              : NOT_FOUND_DATA.EMPTY}
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Bảo hành">
          <Grid container>
            {/* missing */}
            <Grid item xs={12}>
              <FormDataValue
                label="Khách hàng bảo hành"
                value={data?.maintenancePlant?.planName || NOT_FOUND_DATA.EMPTY}
              />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày bắt đầu" value={data?.cusWarrantyDate || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày kết thúc" value={data?.cusWarrantyEnd || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            {/* missing */}
            <Grid item xs={12}>
              <FormDataValue label="Vendor warranty" value={data?.maintenancePlant?.planName || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày bắt đầu" value={data?.venWarrantyDate || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Ngày kết thúc" value={data?.venWarrantyEnd || NOT_FOUND_DATA.EMPTY} />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
    </Box>
  );
};

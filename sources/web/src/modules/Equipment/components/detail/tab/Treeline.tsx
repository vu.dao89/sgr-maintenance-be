/* eslint-disable react/jsx-key */
import TreeItem, { treeItemClasses, TreeItemProps } from '@mui/lab/TreeItem';
import { alpha, BoxProps, styled } from '@mui/material';
import { IHierarchyItem } from 'modules/Equipment';

const StyledTreeItem = styled((props: TreeItemProps) => <TreeItem {...props} />)(({ theme }) => ({
  [`& .${treeItemClasses.iconContainer}`]: {
    '& .close': {
      opacity: 0.3,
      backgroundColor: 'red'
    }
  },
  '& .is-child': {
    [`& .${treeItemClasses.content}`]: {
      '&::before': {
        content: '""',
        display: 'block',
        width: 8,
        height: 1,
        background: alpha(theme.palette.text.primary, 0.5),
        position: 'absolute',
        top: '50%',
        left: -18
      }
    }
  },
  '& .last-node': {
    [`& .${treeItemClasses.content}`]: {
      padding: '6px 0'
    },
    [`& .${treeItemClasses.iconContainer}`]: {
      '& img': {
        display: 'none'
      },
      width: 5,
      height: 5,
      background: '#8D9298',
      borderRadius: '50%'
    }
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: 11,
    paddingLeft: 18,
    borderLeft: `1px solid ${alpha(theme.palette.text.primary, 0.5)}`
  },
  [`& .${treeItemClasses.content}`]: {
    position: 'relative',
    '&:hover': {
      color: theme.palette.action.selected,
      background: 'white',
      fontWeight: 600
    },
    '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
      color: theme.palette.action.selected,
      background: 'white',
      fontWeight: 600
    },
    [`& .${treeItemClasses.label}`]: {
      fontWeight: 'inherit',
      color: 'inherit'
    }
  }
}));

interface DataProp extends BoxProps {
  data?: IHierarchyItem;
}

const Treeline: React.FC<DataProp> = ({ data }) => {
  return (
    <>
      <StyledTreeItem
        className={`${Number(data?.element?.level) != 0 && 'is-child'} ${
          Number(data?.element?.level) === 4 && 'last-node'
        }`}
        nodeId={String(data?.element?.id)}
        label={data?.element?.description}
      >
        {data?.children &&
          data?.children?.map((itemChild: any, keyChild: number) => <Treeline key={keyChild} data={itemChild} />)}
      </StyledTreeItem>
    </>
  );
};

export default Treeline;

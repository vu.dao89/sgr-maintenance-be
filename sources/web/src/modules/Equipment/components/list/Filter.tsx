import { EQUIPMENT_FILTER_FIELD } from '@modules/Equipment/constants';
import { IEquipmentFilterReqDto, IFormFilterEquipment } from '@modules/Equipment/types';
import { FormGroup, FormLabel, Grid } from '@mui/material';
import { FilterInput, RHFSelect } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { compact, isEmpty } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';

interface IFilterProps {
  filter: IEquipmentFilterReqDto | undefined;
  filterCount?: number;
  workCenter: IOption[];
  category: IOption[];
  plannerGroup: IOption[];
  functionLocation: IOption[];
  objectType: IOption[];
  onFilter: (value: any) => any;
}

const defaultValues: IFormFilterEquipment = {
  workCenterCodes: '',
  functionalLocationCodes: '',
  equipmentCategoryCodes: '',
  objectTypeCodes: '',
  plannerGroups: ''
};

export const FilterEquipment = ({
  filter,
  filterCount,
  workCenter,
  category,
  objectType,
  functionLocation,
  plannerGroup,
  onFilter
}: IFilterProps) => {
  const { control, handleSubmit, watch, reset } = useForm<IFormFilterEquipment>({
    defaultValues
  });

  const formValues = watch([
    EQUIPMENT_FILTER_FIELD.WORK_CENTER,
    EQUIPMENT_FILTER_FIELD.FUNCTIONAL_LOCATION,
    EQUIPMENT_FILTER_FIELD.EQUIPMENT_CATEGORY,
    EQUIPMENT_FILTER_FIELD.OBJECT_TYPE,
    EQUIPMENT_FILTER_FIELD.PLANNER_GROUP
  ]);
  const isDisabled = useMemo(() => isEmpty(compact(formValues)), [formValues]);

  const onSubmit = handleSubmit((formData: IFormFilterEquipment) => {
    onFilter(formData);
  });

  const onReset = handleSubmit(() => {
    reset(defaultValues);
    onFilter({ filterText: filter?.filterText });
  });

  useEffect(() => {
    reset({
      workCenterCodes: filter?.workCenterCodes || '',
      functionalLocationCodes: filter?.functionalLocationCodes || '',
      equipmentCategoryCodes: filter?.equipmentCategoryCodes || '',
      objectTypeCodes: filter?.objectTypeCodes || '',
      plannerGroups: filter?.plannerGroups || ''
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <FilterInput
      title="Lọc thiết bị"
      filterCount={filterCount}
      isDisabled={isDisabled}
      onSubmit={onSubmit}
      onReset={onReset}
    >
      <Grid container spacing={2} columnSpacing={6}>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo tổ đội</FormLabel>
            <RHFSelect
              name={EQUIPMENT_FILTER_FIELD.WORK_CENTER}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={workCenter ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo khu vực chức năng</FormLabel>
            <RHFSelect
              name={EQUIPMENT_FILTER_FIELD.FUNCTIONAL_LOCATION}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={functionLocation ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo phân loại</FormLabel>
            <RHFSelect
              name={EQUIPMENT_FILTER_FIELD.EQUIPMENT_CATEGORY}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={category ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo phân nhóm</FormLabel>
            <RHFSelect
              name={EQUIPMENT_FILTER_FIELD.OBJECT_TYPE}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={objectType ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo bộ phận kế hoạch</FormLabel>
            <RHFSelect
              name={EQUIPMENT_FILTER_FIELD.PLANNER_GROUP}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={plannerGroup ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
      </Grid>
    </FilterInput>
  );
};

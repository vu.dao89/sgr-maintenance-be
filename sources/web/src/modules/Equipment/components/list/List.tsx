import { Box, FormControl, Paper, Stack, TableContainer, Typography } from '@mui/material';
import { Pagination, SelectDisplayColumn } from 'common/components';
import { DataTable, IColumnType } from 'common/components/DataTable';
import { PLACEHOLDER_DISPLAY_COLUMN } from 'common/constants';
import { IEquipmentItem } from 'modules/Equipment';

interface IEquipmentListProps {
  items: IEquipmentItem[];
  pagination?: IPagination;
  handleChangePaging?: any;
  columns: IColumnType<IEquipmentItem>[];
  setColumns: any;
}

export const EquipmentList = ({ items, pagination, handleChangePaging, columns, setColumns }: IEquipmentListProps) => {
  const onGoToPage = (e: any) => {
    if (e.keyCode == 13) {
      handleChangePaging(e, e.target.value);
    }
  };

  return (
    <Stack p={4} sx={{ borderRadius: 3, backgroundColor: '#FFF' }}>
      <TableContainer component={Paper} elevation={0}>
        <Box display="flex" justifyContent="space-between" mb={5}>
          <Typography variant="h5">Danh sách thiết bị</Typography>
          <FormControl sx={{ width: '180px' }}>
            <SelectDisplayColumn
              id="display-column"
              options={columns}
              placeholder={PLACEHOLDER_DISPLAY_COLUMN}
              onSelect={setColumns}
            />
          </FormControl>
        </Box>
        <DataTable
          data={items}
          columnId={'equipmentId'}
          columns={columns}
          hasActionColumn={true}
          canViewDetail={true}
          canDelete={false}
          url={'/equipment'}
          noDataText="Không tìm thấy thiết bị"
        />
      </TableContainer>
      <Pagination
        page={pagination?.page ?? 0}
        limit={pagination?.size ?? 0}
        totalPages={pagination?.totalPages ?? 0}
        totalItems={pagination?.totalItems ?? 0}
        onGoToPage={onGoToPage}
        onChange={handleChangePaging}
      ></Pagination>
    </Stack>
  );
};

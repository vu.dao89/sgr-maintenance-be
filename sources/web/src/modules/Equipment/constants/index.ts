export * from './detail';
export * from './list';
export const API_EQUIPMENT = {
  FETCH: '/'
};
export const ACTION_EQUIPMENT = {
  FETCH: 'equipment'
};

export const TAB_TYPE = {
  OVERVIEW: 0,
  MEASURING_POINT: 1,
  CHARACTERISTIC: 2,
  HIERARCHY: 3,
  DOCUMENT: 4
};

export enum FILTER_TYPE {
  EQ_WC = 'EQ_WC',
  FUN_LOC_ID = 'FUN_LOC_ID',
  EQUI_CAT = 3,
  OBJ_TYPE = 4,
  EQ_PLANNER_GR = 5,
  EQ_IC = 6
}

export enum EQUIPMENT_FILTER_FIELD {
  WORK_CENTER = 'workCenterCodes',
  FUNCTIONAL_LOCATION = 'functionalLocationCodes',
  EQUIPMENT_CATEGORY = 'equipmentCategoryCodes',
  OBJECT_TYPE = 'objectTypeCodes',
  PLANNER_GROUP = 'plannerGroups',
  FILTER_TEXT = 'filterText'
}

export const EQUIPMENT_FILTER_FIELD_LABEL: { [key: string]: string } = {
  [EQUIPMENT_FILTER_FIELD.WORK_CENTER]: 'Tổ đội',
  [EQUIPMENT_FILTER_FIELD.FUNCTIONAL_LOCATION]: 'Khu vực chức năng',
  [EQUIPMENT_FILTER_FIELD.EQUIPMENT_CATEGORY]: 'Phân loại',
  [EQUIPMENT_FILTER_FIELD.OBJECT_TYPE]: 'Phân nhóm',
  [EQUIPMENT_FILTER_FIELD.PLANNER_GROUP]: 'Bộ phận kế hoạch',
  [EQUIPMENT_FILTER_FIELD.FILTER_TEXT]: 'Keyword'
};

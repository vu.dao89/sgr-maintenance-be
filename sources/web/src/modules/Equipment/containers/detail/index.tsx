import { Grid } from '@mui/material';
import { Loading } from 'common/components';
import { CardContentComponent, TabContentComponent } from 'modules/Equipment/components';
import { Fragment } from 'react';
import useEquipmentDetailLogic from './useEquipmentDetailLogic';

export default function EquipmentDetailComponent() {
  const { data, hierarchy, loading, loadingMeasPoint, loadingUpload, handleSubmitForm, handleUpload } =
    useEquipmentDetailLogic();

  return (
    <Fragment>
      {loading ? (
        <Loading />
      ) : (
        <Grid container columnSpacing={5}>
          <Grid item xs={12} lg={5} xl={4} className="xl:flex-[0_0_500px]">
            <CardContentComponent data={data} />
          </Grid>
          <Grid item xs={12} lg={7} xl={8} sx={{ maxWidth: { xl: '100%' } }}>
            <TabContentComponent
              loadingUpload={loadingUpload}
              data={data}
              hierarchy={hierarchy}
              loadingMeasPoint={loadingMeasPoint}
              handleSubmitForm={handleSubmitForm}
              handleUpload={handleUpload}
            />
          </Grid>
        </Grid>
      )}
    </Fragment>
  );
}

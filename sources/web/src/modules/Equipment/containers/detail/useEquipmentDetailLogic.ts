import { SUCCESS_MESSAGE } from 'common/constants';
import { IGroupDocument } from 'common/dto';
import { groupFileDocument } from 'common/helpers';
import { useApiCaller } from 'common/hooks';
import {
  createMeasPoint,
  getEquipmentDetail,
  getEquipmentHierarchy,
  IDocumentEquipment,
  IEquipmentDetailItem,
  IFormRollerEquipment,
  IHierarchyItem,
  IMeasurePointRequest,
  uploadDocumentEquipment
} from 'modules/Equipment';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

const useEquipmentDetailLogic = () => {
  const router = useRouter();
  const [equipmentId, setEquipmentId] = useState<string | undefined>();
  const [hierarchy, setHierarchy] = useState<IHierarchyItem[]>();
  const [data, setData] = useState<IEquipmentDetailItem>();

  const { request, loading } = useApiCaller<IEquipmentDetailItem>({
    apiCaller: getEquipmentDetail,
    resDto: {} as IEquipmentDetailItem,
    setData
  });

  const { request: requestMeasPoint, loading: loadingMeasPoint } = useApiCaller<any>({
    apiCaller: createMeasPoint,
    resDto: {} as any
  });

  const { request: requestHierarchy } = useApiCaller<IHierarchyItem[]>({
    apiCaller: getEquipmentHierarchy,
    resDto: [] as IHierarchyItem[]
  });

  const { request: requestUpload, loading: loadingUpload } = useApiCaller<IDocumentEquipment[]>({
    apiCaller: uploadDocumentEquipment,
    resDto: [] as IDocumentEquipment[]
  });

  const handleGetEquipment = async (id: string) => {
    const response = await request(id);

    if (response.statusCode && response.data) {
      const groupDocuments = groupFileDocument(response?.data?.documents);
      const newData = { ...response?.data, groupDocuments };

      setData(newData);
    }
  };

  const handleGetHierarchy = async (equipmentId: string) => {
    const response = (await requestHierarchy({ equipmentId })) as any;

    if (response.statusCode) {
      setHierarchy(response?.data);
    }
  };

  const handleSubmitForm = async (data: IFormRollerEquipment, isCounterX?: boolean) => {
    const newData = {} as IMeasurePointRequest;
    newData.measPoint = data.measPoint;
    newData.measNote = data.measNote;

    if (isCounterX) {
      newData.isMeasDiff = Boolean(data?.isMeasDiff);
      if (data.isMeasDiff) {
        newData.diff = data?.read;
      } else {
        newData.countRead = data?.read;
      }
    } else {
      newData.measRead = data.read;
    }

    const response = (await requestMeasPoint({ data: newData })) as any;

    if (response.statusCode === 200) {
      toast.success(response?.data?.message || SUCCESS_MESSAGE);
    }
  };

  const handleUpload = async (files: any) => {
    if (!data) return;

    const formData = new FormData() as FormData;

    files.forEach((file: any) => {
      formData.append('files', file);
    });

    const response = await requestUpload({ id: data?.equipmentId, files: formData });

    if (response.statusCode === 200) {
      const groupDocuments = groupFileDocument(response.data) as IGroupDocument<IDocumentEquipment>;
      groupDocuments.files = [...(data?.groupDocuments?.files || []), ...(groupDocuments.files || [])];
      groupDocuments.images = [...(data?.groupDocuments?.images || []), ...(groupDocuments.images || [])];

      setData({ ...data, groupDocuments });
    }
  };

  useEffect(() => {
    const id = router?.query?.id as string;
    if (!id) router.push('/404');

    setEquipmentId(id);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.isReady]);

  useEffect(() => {
    if (!equipmentId) return;

    handleGetEquipment(equipmentId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [equipmentId]);

  useEffect(() => {
    if (!data?.equipmentId) return;

    handleGetHierarchy(data?.equipmentId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return { data, hierarchy, loading, loadingUpload, loadingMeasPoint, handleSubmitForm, handleUpload };
};

export default useEquipmentDetailLogic;

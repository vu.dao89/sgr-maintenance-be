import { EquipmentList, FilterEquipment } from '@modules/Equipment/components';
import { EQUIPMENT_FILTER_FIELD_LABEL } from '@modules/Equipment/constants';
import { Box, Stack } from '@mui/material';
import { FilterList, Loading, SearchInput } from 'common/components';
import { size } from 'lodash';
import useEquipmentListLogic from './useEquipmentListLogic';

const EquipmentListContainer = () => {
  const {
    items,
    filter,
    filterCount,
    pagination,
    loading,
    columns,
    workCenter,
    plannerGroup,
    functionLocation,
    category,
    objectType,
    selectedFilter,
    handleChangePaging,
    removeFilter,
    setColumns,
    handleFilter
  } = useEquipmentListLogic();

  const onSearch = (filterText: string) => {
    handleFilter({ ...filter, filterText });
  };

  return (
    <Stack>
      <Stack>
        <Box display="flex" mb={5}>
          <SearchInput name="equipment" onSearch={onSearch} />
          <FilterEquipment
            filter={filter}
            filterCount={filterCount}
            workCenter={workCenter}
            category={category}
            objectType={objectType}
            functionLocation={functionLocation}
            plannerGroup={plannerGroup}
            onFilter={handleFilter}
          />
        </Box>
        {size(selectedFilter) > 0 && (
          <FilterList data={selectedFilter} label={EQUIPMENT_FILTER_FIELD_LABEL} onRemove={removeFilter} />
        )}
      </Stack>

      {loading ? (
        <Loading />
      ) : (
        <EquipmentList
          items={items}
          pagination={pagination}
          handleChangePaging={handleChangePaging}
          columns={columns}
          setColumns={setColumns}
        />
      )}
    </Stack>
  );
};

export default EquipmentListContainer;

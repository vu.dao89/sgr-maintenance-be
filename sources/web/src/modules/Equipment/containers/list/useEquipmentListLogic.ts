import { EquipmentResDto, IEquipmentFilterReqDto, IEquipmentItem, IFormFilterEquipment } from '@modules/Equipment';
import { getEquipments } from '@modules/Equipment/services';
import {
  getEquipmentCategories,
  getFunctionLocations,
  getObjectTypes,
  getPlannerGroups,
  getWorkCenters
} from '@modules/MasterData/services';
import { IColumnType } from 'common/components/DataTable';
import { DEFAULT_PER_PAGE } from 'common/constants';
import { useApiCaller } from 'common/hooks';
import { cloneDeep, isUndefined } from 'lodash';
import { useSelectUserStore } from 'modules/User';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const initColumns: IColumnType<IEquipmentItem>[] = [
  {
    key: 'equipmentId',
    title: 'Mã thiết bị',
    width: 200,
    enable: true,
    required: true
  },
  {
    key: 'description',
    title: 'Tên thiết bị',
    width: 200,
    enable: true,
    required: true,
    type: (_, { description }) => (description ? { type: 'icon', value: 'no-image' } : { type: 'label' })
  },
  {
    key: 'equipmentType',
    title: 'Nhóm thiết bị',
    width: 200,
    enable: true,
    render: (_, { equipmentType }) => (equipmentType ? equipmentType?.description : '')
  },
  {
    key: 'equipmentCategory',
    title: 'Loại thiết bị',
    width: 200,
    enable: true,
    render: (_, { equipmentCategory }) => (equipmentCategory ? equipmentCategory?.description : '')
  },
  {
    key: 'functionalLocation',
    title: 'Khu vực chức năng ',
    width: 200,
    enable: true,
    render: (_, { functionalLocation }) => (functionalLocation ? functionalLocation?.description : '')
  }
];

const initialFilter: IEquipmentFilterReqDto = {
  page: 1,
  size: DEFAULT_PER_PAGE
};

const initPagination = {
  page: 0,
  size: DEFAULT_PER_PAGE,
  total: 0
};

const useEquipmentListLogic = () => {
  const { user } = useSelectUserStore();
  const router = useRouter();
  const [items, setItems] = useState<IEquipmentItem[]>([]);
  const [filter, setFilter] = useState<IEquipmentFilterReqDto>(initialFilter);
  const [filterCount, setFilterCount] = useState<number>(0);
  const [pagination, setPagination] = useState<IPagination>(initPagination);
  const [columns, setColumns] = useState<IColumnType<IEquipmentItem>[]>(initColumns);
  const [workCenter, setWorkCenter] = useState<any[]>([]);
  const [objectType, setObjectType] = useState<any[]>([]);
  const [plannerGroup, setPlannerGroup] = useState<any[]>([]);
  const [functionLocation, setFunctionLocation] = useState<any[]>([]);
  const [category, setCategory] = useState<any[]>([]);
  const [selectedFilter, setSelectedFilter] = useState<IOption[]>([]);

  const { request, loading } = useApiCaller<EquipmentResDto>({
    apiCaller: getEquipments,
    resDto: {} as EquipmentResDto
  });
  const { request: requestWorkCenter } = useApiCaller<any[]>({
    apiCaller: getWorkCenters
  });
  const { request: requestObjectType } = useApiCaller<any[]>({
    apiCaller: getObjectTypes
  });
  const { request: requestPlannerGroup } = useApiCaller<any[]>({
    apiCaller: getPlannerGroups
  });
  const { request: requestFunctionLocation } = useApiCaller<any[]>({
    apiCaller: getFunctionLocations
  });
  const { request: requestEquipmentCategory } = useApiCaller<any[]>({
    apiCaller: getEquipmentCategories
  });

  const handleGetData = async (params: IEquipmentFilterReqDto) => {
    const result: any = await request(params);

    if (result) {
      setItems(result?.data?.items);
      setPagination(result?.data?.pagination);
    }
  };

  const handleGetWorkCenter = async () => {
    const result: any = await requestWorkCenter({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setWorkCenter(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.code })));
    }
  };

  const handleGetObjectType = async () => {
    const result: any = await requestObjectType({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setObjectType(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.code })));
    }
  };

  const handleGetPlannerGroup = async () => {
    const result: any = await requestPlannerGroup({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setPlannerGroup(result?.data?.items?.map((item: any) => ({ label: item?.name, value: item?.plannerGroupId })));
    }
  };

  const handleGetFunctionLocation = async () => {
    const result: any = await requestFunctionLocation({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setFunctionLocation(
        result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.functionalLocationId }))
      );
    }
  };

  const handleGetCategory = async () => {
    const result: any = await requestEquipmentCategory({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setCategory(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.code })));
    }
  };

  useEffect(() => {
    if (!router.isReady) {
      return;
    }

    const data = router?.query as any;

    data.page = Number(data.page) || initialFilter.page;
    data.size = Number(data.size) || initialFilter.size;
    data.maintenancePlantCodes = user?.maintenancePlantCodes;

    handleGetData(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  useEffect(() => {
    const selected: any = {};

    if (filter?.workCenterCodes) {
      selected['workCenterCodes'] = workCenter.filter(item => item.value === filter?.workCenterCodes?.toString())[0];
    }

    if (filter?.equipmentCategoryCodes) {
      selected['equipmentCategoryCodes'] = category.filter(
        item => item.value === filter?.equipmentCategoryCodes?.toString()
      )[0];
    }

    if (filter?.functionalLocationCodes) {
      selected['functionalLocationCodes'] = functionLocation.filter(
        item => item.value === filter?.functionalLocationCodes?.toString()
      )[0];
    }

    if (filter?.objectTypeCodes) {
      selected['objectTypeCodes'] = objectType.filter(item => item.value === filter?.objectTypeCodes?.toString())[0];
    }

    if (filter?.plannerGroups) {
      selected['plannerGroups'] = plannerGroup.filter(item => item.value === filter?.plannerGroups?.toString())[0];
    }

    setSelectedFilter(selected);

    setFilterCount(Object.keys(selected).length);

    handleGetData({ ...filter, maintenancePlantCodes: String(user?.maintenancePlantCodes) });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  useEffect(() => {
    handleGetWorkCenter();
    handleGetCategory();
    handleGetFunctionLocation();
    handleGetObjectType();
    handleGetPlannerGroup();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChangePaging = (_event: any, page: number) => {
    setFilter({ ...filter, page });
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  };

  const removeFilter = (
    filterKey:
      | 'workCenterCodes'
      | 'functionalLocationCodes'
      | 'equipmentCategoryCodes'
      | 'objectTypeCodes'
      | 'plannerGroups'
  ) => {
    const newFilter = cloneDeep(filter);
    if (!isUndefined(newFilter)) {
      delete newFilter[filterKey];
      setFilter({ ...newFilter });
    }
  };

  const handleFilter = (value: IFormFilterEquipment) => {
    const dataFilter = {} as IEquipmentFilterReqDto;

    if (value.filterText && !isUndefined(value.filterText)) {
      dataFilter['filterText'] = value.filterText;
    }

    if (value.workCenterCodes && !isUndefined(value.workCenterCodes)) {
      dataFilter['workCenterCodes'] = value.workCenterCodes;
    }

    if (value.equipmentCategoryCodes && !isUndefined(value.equipmentCategoryCodes)) {
      dataFilter['equipmentCategoryCodes'] = value.equipmentCategoryCodes;
    }

    if (value.plannerGroups && !isUndefined(value.plannerGroups)) {
      dataFilter['plannerGroups'] = value.plannerGroups;
    }

    if (value.functionalLocationCodes && !isUndefined(value.functionalLocationCodes)) {
      dataFilter['functionalLocationCodes'] = value.functionalLocationCodes;
    }

    if (value.objectTypeCodes && !isUndefined(value.objectTypeCodes)) {
      dataFilter['objectTypeCodes'] = value.objectTypeCodes;
    }

    setFilter({ ...initialFilter, ...dataFilter });
  };

  return {
    loading,
    pagination,
    items: items ?? [],
    filter,
    filterCount,
    columns,
    workCenter,
    category,
    functionLocation,
    objectType,
    plannerGroup,
    selectedFilter,
    handleChangePaging,
    handleGetData,
    removeFilter,
    handleFilter,
    setColumns
  };
};

export default useEquipmentListLogic;

import { createSlice } from '@reduxjs/toolkit';
import reducers, { defaultState } from './reducers';

const { actions, reducer } = createSlice({
  name: 'equipment',
  initialState: { ...defaultState },
  reducers,
  extraReducers: _builder => {}
});

export * from './api';
export * from './selectors';
export { actions as equipmentActions };
export { reducer as equipmentReducer };

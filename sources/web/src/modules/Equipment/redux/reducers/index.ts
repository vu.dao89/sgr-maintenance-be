import { IEquipmentState } from '@modules/Equipment';

export const defaultState: IEquipmentState = {
  entities: [],
  loading: 'idle',
  currentRequestId: undefined,
  error: null
};

const reducers = {
  onUpdate: (state: any) => {
    return state;
  }
};

export default reducers;

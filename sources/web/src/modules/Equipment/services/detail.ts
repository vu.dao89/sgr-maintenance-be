import { apiService } from 'common/app';
import { IMeasurePointRequest } from '../types';

export const getEquipmentDetail = (id: string) => {
  return apiService.get(`/masterdata/equipment/${id}`);
};

export const getEquipmentHierarchy = (params: any) => {
  return apiService.get('equipment/hierarchy', { params });
};

export const createMeasPoint = (data: IMeasurePointRequest) => {
  return apiService.post('equipment/post/meas-point', data);
};

export const uploadDocumentEquipment = ({ id, files }: { id: string; files: any }) => {
  return apiService.post(`/equipment-document/uploads?equipmentId=${id}`, files, {
    headers: { 'Content-Type': 'multipart/form-data' }
  });
};

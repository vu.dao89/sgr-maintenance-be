import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getEquipments = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/equipments?${paramStr}`);
};

export const getWorkCenter = () => {
  return apiService.get(`/workCenter`);
};

export const getObjectType = () => {
  return apiService.get(`/objectType`);
};

export const getPlanerGroup = () => {
  return apiService.get(`/planerGroup`);
};

export const getFunctionLocation = () => {
  return apiService.get(`/functionLocation`);
};

export const getSortField = () => {
  return apiService.get(`/sortField`);
};

export const getCategory = () => {
  return apiService.get(`/category`);
};

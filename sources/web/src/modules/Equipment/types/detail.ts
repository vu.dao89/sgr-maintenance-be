import { Expose, Type } from 'class-transformer';
import { TFileUpload } from 'common/components';
import {
  ICharacteristics,
  IDocumentBase,
  IEquipmentCategory,
  IEquipmentType,
  IFunctionalLocation,
  IMaintenancePlant,
  IMaintenanceWorkCenter,
  IMeasuringPoint,
  IPartners,
  ISystemStatus
} from 'common/dto';
import { IGroupDocument } from './../../../common/dto/masterDataRequest';

export interface IFileData {
  name?: string;
  date?: string;
  creator?: string;
  linkDownload?: string;
}

export interface IDataEquipmentCard {
  name?: string;
  items?: IItemEquipmentCard[];
}

export interface IItemEquipmentCard {
  label?: string;
  value?: string;
}

export interface ITreeLine {
  name?: string;
  level?: number;
  child?: any;
}

export class IEquipmentInfo {
  id?: number;
  name?: string;
  image?: string;
  description?: string;
  location?: string;
}

export class IDataEquipmentTab {
  id?: number;
  name?: string;
  files?: TFileUpload[];
  items?: IDataEquipmentCard[];
  treeline?: any;
}

// ---------- Form
export class IFormRollerEquipment {
  measPoint?: string;
  isMeasDiff?: number | boolean;
  read?: string;
  measNote?: string;
  countRead?: string;
}

export class IMeasurePointRequest {
  measPoint?: string;
  isMeasDiff?: number | boolean;
  measRead?: string;
  diff?: string;
  countRead?: string;
  measNote?: string;
}

// ---------- Response

// ---------- Response
export class IEquipmentDetailResponse {
  @Expose()
  @Type(() => IEquipmentInfo)
  item?: IEquipmentInfo;

  @Type(() => IDataEquipmentTab)
  tab: IDataEquipmentTab[] = [];
}

export interface IEquipmentDetailItem extends IBaseEntity {
  thumbnailUrl: string;
  originalUrl: string;
  equipmentId: string;
  description: string;
  parentEquipmentId: string;
  equipmentTypeId: any;
  longDesc: string;
  plannerGroup: string;
  functionalLocation: IFunctionalLocation;
  equipmentCategory: IEquipmentCategory;
  systemStatus: ISystemStatus;
  maintenanceWorkCenter: IMaintenanceWorkCenter;
  equipmentType: IEquipmentType;
  manufPartNo: string;
  constYear: string;
  constMonth: string;
  maintenancePlantId: string;
  maintenancePlant: IMaintenancePlant;
  functionalLocationId: string;
  equipmentCategoryId: string;
  systemStatusId: string;
  location: string;
  maintenanceWorkCenterId: string;
  costCenter: string;
  manuCountry: string;
  manuPartNo: string;
  manuSerialNo: string;
  manufacturer: string;
  modelNumber: string;
  planningPlantId: string;
  plantSection: string;
  size: string;
  weight: string;
  createDate: string;
  createTime: string;
  cusWarrantyEnd: any;
  cusWarrantyDate: any;
  venWarrantyEnd: any;
  venWarrantyDate: any;
  qrCode: string;
  startupDate: string;
  endOfUseDate: string;
  sortField: string;
  partners: IPartners[];
  measuringPoints: IMeasuringPoint[];
  characteristics: ICharacteristics[];
  documents: IDocumentEquipment[];
  groupDocuments?: IGroupDocument<IDocumentEquipment>;
}

export class EquipmentFilterResponse {
  eq_wc?: string;
  func_loc_id?: string;
  equi_cat?: string;
  equi_cat_des?: string;
  obj_type?: string;
  obj_type_des?: string;
  eq_planner_gr?: string;
  sort_field?: string;
}

export interface IHierarchyElement {
  id: string;
  description: string;
  parentId: string;
  level: string;
  selected: boolean;
}

export interface IHierarchyItem {
  element: IHierarchyElement;
  children: IHierarchyItem[];
}

export interface IDocumentEquipment extends IDocumentBase {
  equipmentCode: string;
}

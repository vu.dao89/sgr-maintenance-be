import { IEquipmentCategory, IEquipmentType, IFunctionalLocation, ISystemStatus } from 'common/dto';
export interface IEquipmentStoreState {
  entities: IEquipmentItem[];
  loading: TLoading;
  error: any;
}

export interface IEquipmentState extends IEquipmentStoreState {
  currentRequestId: TCurrentRequest;
}

export interface IEquipmentItem extends IBaseEntity {
  id: string | number;
  equipmentId: string;
  description: string;
  longDesc: string;
  plannerGroup: string;
  functionalLocation: IFunctionalLocation;
  equipmentCategory: IEquipmentCategory;
  systemStatus: ISystemStatus;
  maintenanceWorkCenter: string;
  equipmentType: IEquipmentType;
}

export interface EquipmentResDto {
  statusCode: string;
  items: IEquipmentItem[];
  pagination: IPagination;
}

export interface IEquipmentFilterReqDto extends IPaginationReq {
  filterText?: string;
  workCenterCodes?: string;
  functionalLocationCodes?: string;
  equipmentCategoryCodes?: string;
  objectTypeCodes?: string;
  plannerGroups?: string;
  maintenancePlantCodes?: string;
}

export interface IFormFilterEquipment {
  filterText?: string;
  workCenterCodes?: string;
  functionalLocationCodes?: string;
  equipmentCategoryCodes?: string;
  objectTypeCodes?: string;
  plannerGroups?: string;
}

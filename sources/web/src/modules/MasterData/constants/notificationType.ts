export interface INotificationType {
  id: string;
  notiType: string;
  description: string;
  catalogProfile: string;
  problems: string;
  causes: string;
  activities: string;
  objectParts: string;
  workOrderTypeId: string;
  priorityTypeId: string;
  personResponsible: string;
}

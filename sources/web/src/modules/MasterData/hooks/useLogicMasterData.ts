import { ICodeItem } from 'common/dto';
import { useApiCaller } from 'common/hooks';
import { uniqBy } from 'lodash';
import { getCodeGroups, getCodes, getType, ICodeParam } from 'modules/MasterData';
import { INotificationType } from 'modules/Notification';
import { useState } from 'react';

export interface ResTemplateData<T> {
  items: T;
}

export default function useLogicMasterData() {
  const [notificationType, setNotificationType] = useState<INotificationType>();
  const [parts, setParts] = useState<ICodeItem[]>();
  const [partCodes, setPartCodes] = useState<ICodeItem[]>();
  const [problems, setProblems] = useState<ICodeItem[]>();
  const [dames, setDames] = useState<ICodeItem[]>();
  const [causes, setCauses] = useState<ICodeItem[]>();
  const [causeCodes, setCauseCodes] = useState<ICodeItem[]>();

  const { request: requestNotificationType } = useApiCaller<INotificationType>({
    apiCaller: getType,
    resDto: {} as INotificationType
  });

  const { request: requestGetParts, loading: loadingGetParts } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodeGroups,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const { request: requestGetProblems, loading: loadingGetProblems } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodeGroups,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const { request: requestGetCauses, loading: loadingGetCauses } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodeGroups,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const { request: requestGetPartCodes, loading: loadingGetPartCodes } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodes,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const { request: requestDames, loading: loadingDames } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodes,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const { request: requestGetCauseCodes, loading: loadingGetCauseCodes } = useApiCaller<ResTemplateData<ICodeItem[]>>({
    apiCaller: getCodes,
    resDto: {} as ResTemplateData<ICodeItem[]>
  });

  const handleGetNotificationType = async (type: String) => {
    const response = await requestNotificationType(type);
    const { statusCode, data } = response;

    if (statusCode) {
      setNotificationType(data);
    }
  };

  const handleGetParts = async (params: ICodeParam) => {
    const response = await requestGetParts(params);
    const { statusCode, data } = response;

    if (statusCode && data?.items.length) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setParts(newData);
    }
  };

  const handleGetProblems = async (params: ICodeParam) => {
    const response = await requestGetProblems(params);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setProblems(newData);
    }
  };

  const handleGetPartCodes = async (params: ICodeParam) => {
    const response = await requestGetPartCodes(params);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setPartCodes(newData);
    }
  };

  const handleGetDames = async (params: ICodeParam) => {
    const response = await requestDames(params);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setDames(newData);
    }
  };

  const handleGetCauses = async (params: ICodeParam) => {
    const response = await requestGetCauses(params);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setCauses(newData);
    }
  };

  const handleGetCauseCodes = async (params: ICodeParam) => {
    const response = await requestGetCauseCodes(params);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = uniqBy(data?.items, 'codeGroup') as ICodeItem[];
      setCauseCodes(newData);
    }
  };

  return {
    notificationType,
    parts,
    partCodes,
    problems,
    dames,
    causes,
    causeCodes,
    loadingGetParts,
    loadingGetProblems,
    loadingGetCauses,
    loadingGetPartCodes,
    loadingDames,
    loadingGetCauseCodes,
    handleGetNotificationType,
    handleGetParts,
    handleGetPartCodes,
    handleGetProblems,
    handleGetDames,
    handleGetCauses,
    handleGetCauseCodes
  };
}

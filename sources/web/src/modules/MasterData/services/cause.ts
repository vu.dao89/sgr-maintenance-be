import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getCauses = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/code-groups?${paramStr}`);
};

export const getCauseCodes = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/codes?${paramStr}`);
};

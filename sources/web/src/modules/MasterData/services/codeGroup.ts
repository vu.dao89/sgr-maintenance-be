import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export interface ICodeParam {
  catalogProfiles?: string;
  catalogs?: string;
  codeGroups?: string;
  maintenancePlantCodes?: string;
}

export const getCodeGroups = (data: ICodeParam) => {
  const paramStr = convertObjectToQueryParams(data);

  return apiService.get(`/masterdata/code-groups?${paramStr}`);
};

export const getCodes = (data: ICodeParam) => {
  const paramStr = convertObjectToQueryParams(data);

  return apiService.get(`/masterdata/codes?${paramStr}`);
};

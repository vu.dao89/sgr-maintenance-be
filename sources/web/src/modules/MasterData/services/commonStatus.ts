import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getStatus = (params: any) => {
  const paramStr = convertObjectToQueryParams(params);

  return apiService.get(`/masterdata/common-status?${paramStr}`);
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getEquipments = (params?: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/equipments?${paramStr}`);
};

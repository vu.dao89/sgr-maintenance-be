import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getEquipmentCategories = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params});

  return apiService.get(`/masterdata/equipment-categories?${paramStr}`);
};

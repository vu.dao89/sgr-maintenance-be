import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getFunctionLocations = (params?: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/functional-locations?${paramStr}`);
};

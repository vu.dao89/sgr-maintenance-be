import { apiService } from 'common/app';

export const deleteGlobalDocument = (id: string) => {
  return apiService.delete(`/global-document/${id}`);
};

export const uploadGlobalDocument = (formData: FormData) => {
  return apiService.post(`/global-document/uploads`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getMainPlants = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params});

  return apiService.get(`/masterdata/maintenance-plants?${paramStr}`);
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getTypes = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/notification-types?${paramStr}`);
};

export const getType = (type: string) => {
  return apiService.get(`/masterdata/notification-type/${type}`);
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getObjectParts = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/code-groups?${paramStr}`);
};

export const getObjectPartCodes = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/codes?${paramStr}`);
};

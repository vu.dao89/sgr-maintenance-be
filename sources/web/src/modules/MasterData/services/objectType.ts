import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getObjectTypes = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/object-types?${paramStr}`);
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getPlannerGroups = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/planner-groups?${paramStr}`);
};

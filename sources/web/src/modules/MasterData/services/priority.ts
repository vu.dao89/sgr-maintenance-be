import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getPriorities = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/priorities?${paramStr}`);
};

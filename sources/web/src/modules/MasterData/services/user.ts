import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getReportBy = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/personnel?${paramStr}`);
};

export const getAssignTo = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/personnel?${paramStr}`);
};

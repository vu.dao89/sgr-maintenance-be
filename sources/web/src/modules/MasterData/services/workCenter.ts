import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getWorkCenters = (params?: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.get(`/masterdata/work-centers?${paramStr}`);
};

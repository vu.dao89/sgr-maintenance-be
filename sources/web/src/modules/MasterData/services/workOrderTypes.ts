import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getWorkOrderTypes = (params: any) => {
  const paramStr = convertObjectToQueryParams({ ...params, maintenancePlantCodes: 'M003,M046' });

  return apiService.get(`/masterdata/work-order-types?${paramStr}`);
};

import { LoadingButton } from '@mui/lab';
import { Box, Chip, IconButton, Stack, Typography } from '@mui/material';
import { ConfirmationDialog, Icon, LeftCardContent } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import { COMMON_STATUS_STAT } from 'common/constants/commonStatus';
import { convertStringToDateTime } from 'common/helpers';
import dayjs from 'dayjs';
import { ModalCorrectiveAction } from 'modules/Notification';
import { IParamCloseNotification } from 'modules/Notification/services';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import Image from 'next/image';
import Router from 'next/router';
import { useState } from 'react';

export const LeftCardComponent: React.FC = () => {
  const { data, workOrderDetail, handleCloseNotification, loadingCloseNotificationItem } = useNotificationContext();
  const [openModalConfirm, setOpenModalConfirm] = useState<boolean>(false);
  const [openModalCorrectiveAction, setopenModalCorrectiveAction] = useState<boolean>(false);

  const handleCloseNotificationAction = async () => {
    const isCanCloseStatus = [
      String(COMMON_STATUS_STAT.DONE),
      String(COMMON_STATUS_STAT.CLOSE),
      String(COMMON_STATUS_STAT.LOCK),
      String(COMMON_STATUS_STAT.CALCULATED)
    ];

    if (workOrderDetail) {
      if (isCanCloseStatus.includes(String(workOrderDetail.workOrder.commonStatus.status))) {
        const params = {
          id: data?.header?.id,
          referDate: dayjs().format('DDMMYYYY'),
          referDateTime: dayjs().format('HHmmss')
        } as IParamCloseNotification;

        await handleCloseNotification(params);

        return;
      }
      setOpenModalConfirm(true);
      return;
    }

    setopenModalCorrectiveAction(true);
  };

  const handleViewWorkOrder = () => {
    Router.push('/work-order');
  };

  return (
    <>
      <LeftCardContent image={data?.image || '/iss/assetmanagement/static/images/no-image.png'}>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography component="h4" variant="h4">
            {data?.header?.desc}
          </Typography>
          <IconButton sx={{ p: 0 }}>
            <Icon name="pencil" width={40} height={40} />
          </IconButton>
        </Box>
        <Box mt="6px" display="flex">
          {data?.header?.id && (
            <Typography
              component="span"
              variant="body2"
              sx={{
                display: 'flex',
                alignItems: 'center',
                '&::after': {
                  content: '""',
                  width: '2px',
                  height: '70%',
                  bgcolor: 'grey.600',
                  display: 'block',
                  mx: '6px'
                }
              }}
            >
              {data?.header?.id}
            </Typography>
          )}
          <Typography component="span" variant="body2">
            {convertStringToDateTime(String(data?.header?.requestStart))}
          </Typography>
          {data?.header?.requestEnd && (
            <Typography
              component="span"
              variant="body2"
              sx={{
                display: 'flex',
                alignItems: 'center',
                '&::before': {
                  content: '"-"',
                  display: 'block',
                  mx: '6px'
                }
              }}
            >
              {convertStringToDateTime(data?.header?.requestEnd)}
            </Typography>
          )}
        </Box>
        <Box mt={2} display="flex" flexWrap="wrap" gap={2}>
          {data?.header?.priorityType && (
            <Chip label={data?.header?.priorityType} color="info" className="bg-pink" sx={{ fontSize: 14 }} />
          )}
          {data?.header?.commonStatus?.description && (
            <Chip
              label={data?.header?.commonStatus?.description}
              color="info"
              sx={{
                fontSize: 14,
                boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
                bgcolor: data?.header?.commonStatus?.status === COMMON_STATUS_STAT.COMPLETE ? '#00C48C' : '#3765DE'
              }}
            />
          )}
          {data?.header?.type && <Chip label={data?.header?.type} color="info" sx={{ fontSize: 14 }} />}
        </Box>
        <Box mt={3}>
          <Typography variant="body2" color="grey.300">
            Mã thiêt bị
          </Typography>
          <Typography fontWeight="bold">
            {data?.header?.equipmentDesc}
            {data?.header?.equipmentId && ` (${data?.header?.equipmentId})`}
          </Typography>
        </Box>
        <Box mt={4} p={'8px 16px'} borderRadius={'20px'} className="flex " sx={{ backgroundColor: 'primary.light' }}>
          <Image
            src="/iss/assetmanagement/static/icons/location.svg"
            alt="Icon"
            width={17}
            height={20}
            className="mr-[10px]"
          />
          <Typography ml={3} fontSize={14}>
            {data?.header?.functionalLocationDesc || NOT_FOUND_DATA.EMPTY}
          </Typography>
        </Box>
        <Box display="flex" alignItems="center" mt={3}>
          <Image
            src="/iss/assetmanagement/static/images/avatarDefault.png"
            alt="Icon"
            width={40}
            height={40}
            className="mr-[10px] rounded-full"
          />
          <Stack>
            <Typography variant="body2" color="grey.300">
              Được tạo bởi
            </Typography>
            <Typography fontWeight="bold" variant="body2">
              {data?.header?.reportByName || NOT_FOUND_DATA.EMPTY}
            </Typography>
          </Stack>
        </Box>
        {data?.header?.commonStatus.status != COMMON_STATUS_STAT.COMPLETE && (
          <LoadingButton
            loading={loadingCloseNotificationItem ? true : false}
            onClick={handleCloseNotificationAction}
            fullWidth
            disableRipple
            sx={{ backgroundColor: 'primary', fontSize: 16, textTransform: 'none', mt: 4 }}
            type="button"
            variant="contained"
          >
            Hoàn Thành
          </LoadingButton>
        )}
      </LeftCardContent>
      <ConfirmationDialog
        id="confirm-notification"
        keepMounted
        btnCancelText="Quay lại"
        btnOKText="Xem lệnh bảo trì"
        open={openModalConfirm}
        onCancel={() => setOpenModalConfirm(false)}
        onOk={handleViewWorkOrder}
        title="Lệnh bảo trì chưa hoàn thành"
        content="Thông báo này có lệnh bảo trì chưa hoàn tất, vui lòng hoàn thành lệnh bảo trì"
      />

      <ModalCorrectiveAction
        notificationDetail={data}
        open={openModalCorrectiveAction}
        onSubmit={handleCloseNotification}
        onClose={() => setopenModalCorrectiveAction(false)}
      />
    </>
  );
};

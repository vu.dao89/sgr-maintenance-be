import { BoxProps } from '@mui/material';
import { RightCardContent } from 'common/components';
import { NOTIFICATION_TAB_CONTENT, NOTIFICATION_TAB_DATA } from 'modules/Notification/constants';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { useState } from 'react';
import { ActionTabComponent, DetailTabComponent, DocumentTabComponent, PreviewTabComponent } from './tabContent';

export const RightTabComponent: React.FC<BoxProps> = () => {
  const { data } = useNotificationContext();
  const [currentTab, setCurrentTab] = useState<number>(0);

  const renderTab = () => {
    switch (currentTab) {
      case NOTIFICATION_TAB_CONTENT.DETAIL:
        return <DetailTabComponent />;
      case NOTIFICATION_TAB_CONTENT.ACTION:
        return <ActionTabComponent />;
      case NOTIFICATION_TAB_CONTENT.DOCUMENT:
        return <DocumentTabComponent />;
      default:
        return <PreviewTabComponent data={data} />;
    }
  };

  const handleChange = (_event: React.SyntheticEvent, newValue: number) => {
    setCurrentTab(newValue);
  };

  return (
    <RightCardContent tabs={NOTIFICATION_TAB_DATA} currentTab={currentTab} handleChange={handleChange}>
      {renderTab()}
    </RightCardContent>
  );
};

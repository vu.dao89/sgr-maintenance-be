import { yupResolver } from '@hookform/resolvers/yup';
import { validationCorrectiveSchema } from '@modules/Notification/containers/detail/validateCorrective';
import { Box, BoxProps, Button, FormGroup, Grid, Typography } from '@mui/material';
import { CardItem, FormLabelCustom, Icon, Modal, RHFSelect, RHFTextarea } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import dayjs from 'dayjs';
import { INotificationDetail, IParamCloseNotification } from 'modules/Notification';
import { useFieldArray, useForm } from 'react-hook-form';

interface DataProp extends BoxProps {
  open: boolean;
  onClose?: any;
  onSubmit?: any;
  notificationDetail?: INotificationDetail;
}

const initDefaultData = {
  activityGroup: '',
  activityCode: '',
  notificationLongText: ''
};

export const ModalCorrectiveAction = ({ open, onClose, onSubmit, notificationDetail }: DataProp) => {
  const { control, handleSubmit, reset } = useForm<any>({
    defaultValues: {
      correctiveAction: [initDefaultData]
    },
    resolver: yupResolver(validationCorrectiveSchema),
    mode: 'all',
    reValidateMode: 'onChange'
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'correctiveAction'
  });

  const onCloseForm = () => {
    reset({ correctiveAction: [initDefaultData] });
    onClose();
  };

  const onSubmitForm = async (data: any) => {
    const params = {
      id: notificationDetail?.header?.id,
      referDate: dayjs().format('DDMMYYYY'),
      referDateTime: dayjs().format('HHmmss'),
      notificationActivities: data?.correctiveAction
    } as IParamCloseNotification;

    onCloseForm();
    await onSubmit(params);
  };

  return (
    <form onSubmit={handleSubmit(onSubmitForm)}>
      <Modal
        disablePortal
        open={open}
        onClose={onCloseForm}
        title="Thêm hành động khắc phục"
        btnResetText="Quay lại"
        btnSubmitText="Thêm"
        sx={{
          '& .MuiDialogContent-root': {
            bgcolor: 'grey.700',
            pt: '20px!important'
          }
        }}
      >
        <Box>
          <CardItem>
            <Box display="flex" alignItems="center">
              <Button sx={{ p: 0, minWidth: 24 }}>
                <Icon name="add-blue" width={24} height={24} />
                <Typography
                  ml={3}
                  fontWeight="500"
                  onClick={() => append(initDefaultData)}
                  sx={{ color: theme => theme.palette.info.main, textTransform: 'capitalize' }}
                >
                  Thêm hành động khắc phục
                </Typography>
              </Button>
            </Box>
          </CardItem>
        </Box>
        {fields &&
          fields.map((_item: any, index) => (
            <Box key={index} className="[&:not(:first-child)]:mt-3">
              <CardItem
                title={`Hành động khắc phục ${index + 1}`}
                clearable
                handleRemove={() => {
                  if (fields?.length > 1) remove(index);
                }}
              >
                <Grid container rowSpacing={4} columnSpacing={4} mt={2}>
                  <Grid item xs={6}>
                    <FormGroup sx={{ mt: 0 }}>
                      <FormLabelCustom required>Nhóm khắc phục</FormLabelCustom>
                      <RHFSelect
                        id={`correctiveAction[${index}].activityGroup`}
                        name={`correctiveAction[${index}].activityGroup`}
                        variant="outlined"
                        fullWidth
                        control={control}
                        options={[]}
                        placeholder={PLACEHOLDER_SELECT_INPUT}
                        displayEmpty
                        isPlaceholderOption
                      />
                    </FormGroup>
                  </Grid>
                  <Grid item xs={6}>
                    <FormGroup sx={{ mt: 0 }}>
                      <FormLabelCustom required>Chi tiết khắc phục</FormLabelCustom>
                      <RHFSelect
                        id={`correctiveAction[${index}].activityCode`}
                        name={`correctiveAction[${index}].activityCode`}
                        variant="outlined"
                        fullWidth
                        control={control}
                        options={[]}
                        placeholder={PLACEHOLDER_SELECT_INPUT}
                        displayEmpty
                        isPlaceholderOption
                      />
                    </FormGroup>
                  </Grid>
                  <Grid item xs={12}>
                    <FormGroup sx={{ mt: 0 }}>
                      <FormLabelCustom required>Mô tả chi tiết</FormLabelCustom>
                      <RHFTextarea
                        rows={3}
                        id={`correctiveAction[${index}].notificationLongText`}
                        name={`correctiveAction[${index}].notificationLongText`}
                        variant="outlined"
                        placeholder="Nhập mô tả chi tiết"
                        fullWidth
                        control={control}
                      />
                    </FormGroup>
                  </Grid>
                </Grid>
              </CardItem>
            </Box>
          ))}
      </Modal>
    </form>
  );
};

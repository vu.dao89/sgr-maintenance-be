import { yupResolver } from '@hookform/resolvers/yup';
import { INotificationItemReq } from '@modules/Notification/types/create';
import { Box, BoxProps, FormGroup, Grid } from '@mui/material';
import { FormLabelCustom, Modal, RHFSelect, RHFTextarea } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { convertDataOption } from 'common/helpers';
import { validationSchema } from 'modules/Notification/containers/detail/validate';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';

interface DataProp extends BoxProps {
  open: boolean;
  onClose?: any;
}

const initFormData: INotificationItemReq = {
  part: '',
  partCode: '',
  problem: '',
  dame: '',
  idText: '',
  cause: '',
  causeCode: '',
  causeText: ''
};

export const ModalCreateNotificationItemComponent = ({ open, onClose }: DataProp) => {
  const {
    data,
    notificationType,
    parts,
    partCodes,
    problems,
    dames,
    causes,
    causeCodes,
    handleGetParts,
    handleGetPartCodes,
    handleGetCauses,
    handleGetDames,
    handleGetCauseCodes,
    handleGetProblems,
    handleSubmitCreateNotificationItem,
    loadingGetParts,
    loadingGetProblems,
    loadingGetCauses,
    loadingGetPartCodes,
    loadingDames,
    loadingGetCauseCodes
  } = useNotificationContext();

  const { control, setValue, handleSubmit, reset, clearErrors } = useForm<any>({
    defaultValues: initFormData,
    resolver: yupResolver(validationSchema)
  });

  useEffect(() => {
    if (open) {
      handleGetParts({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.objectParts });
      handleGetProblems({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.problems });
      handleGetCauses({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.causes });
    }
  }, [open]);

  const handleChangePart = (value: any) => {
    setValue('part', value);
    setValue('partCode', '');
    clearErrors('part');
    clearErrors('partCode');
    handleGetPartCodes({ codeGroups: value });
  };

  const handleChangeProblem = (value: any) => {
    setValue('problem', value);
    setValue('dame', '');
    clearErrors('problem');
    clearErrors('dame');
    handleGetDames({ codeGroups: value });
  };

  const handleChangeCause = (value: any) => {
    setValue('cause', value);
    setValue('causeCode', '');
    handleGetCauseCodes({ codeGroups: value });
  };

  const handleOnClose = () => {
    reset(initFormData);
    onClose();
  };

  const submitForm = (data: any) => {
    handleSubmitCreateNotificationItem(data);
    handleOnClose();
  };

  return (
    <form onSubmit={handleSubmit(submitForm)}>
      <Modal
        disablePortal
        open={open}
        title="Thêm Notification Item"
        onClose={handleOnClose}
        btnResetText="Huỷ"
        btnSubmitText="Tạo"
      >
        <Box display="flex" alignItems="center" justifyContent="space-between" mt="18px">
          <Grid container columnSpacing={6}>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nhóm bộ phận</FormLabelCustom>
                <RHFSelect
                  loading={loadingGetParts}
                  disabled={loadingGetParts}
                  id="part"
                  name="part"
                  variant="outlined"
                  fullWidth
                  control={control}
                  options={convertDataOption('codeGroupDesc', 'codeGroup', parts)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangePart(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Bộ phận</FormLabelCustom>
                <RHFSelect
                  id="partCode"
                  name="partCode"
                  variant="outlined"
                  loading={loadingGetPartCodes}
                  disabled={loadingGetPartCodes}
                  fullWidth
                  options={convertDataOption('codeDesc', 'code', partCodes)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng</FormLabelCustom>
                <RHFSelect
                  id="problem"
                  name="problem"
                  variant="outlined"
                  loading={loadingGetProblems}
                  disabled={loadingGetProblems}
                  fullWidth
                  options={convertDataOption('codeGroupDesc', 'codeGroup', problems)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangeProblem(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng chi tiết</FormLabelCustom>
                <RHFSelect
                  id="dame"
                  name="dame"
                  variant="outlined"
                  loading={loadingDames}
                  disabled={loadingDames}
                  fullWidth
                  options={convertDataOption('codeDesc', 'code', dames)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Mô tả chi tiết</FormLabelCustom>
                <RHFTextarea id="idText" name="idText" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nhóm nguyên nhân lỗi</FormLabelCustom>
                <RHFSelect
                  id="cause"
                  name="cause"
                  variant="outlined"
                  loading={loadingGetCauses}
                  disabled={loadingGetCauses}
                  fullWidth
                  options={convertDataOption('codeGroupDesc', 'codeGroup', causes)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangeCause(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng chi tiết</FormLabelCustom>
                <RHFSelect
                  id="causeCode"
                  name="causeCode"
                  loading={loadingGetCauseCodes}
                  disabled={loadingGetCauseCodes}
                  variant="outlined"
                  fullWidth
                  control={control}
                  options={convertDataOption('codeDesc', 'code', causeCodes)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nguyên nhân lỗi chi tiết</FormLabelCustom>
                <RHFTextarea id="causeText" name="causeText" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </form>
  );
};

import { yupResolver } from '@hookform/resolvers/yup';
import { INotificationItemReq } from '@modules/Notification/types/create';
import { Box, BoxProps, FormGroup, Grid } from '@mui/material';
import { FormLabelCustom, Modal, RHFSelect, RHFTextarea } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { convertDataOption } from 'common/helpers';
import { INotificationItemDetail } from 'modules/Notification';
import { validationSchema } from 'modules/Notification/containers/detail/validate';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { useEffect, useRef } from 'react';
import { useForm } from 'react-hook-form';

interface DataProp extends BoxProps {
  notificationItem?: INotificationItemDetail;
  open: boolean;
  onClose?: any;
}

export const ModalUpdateNotificationItemComponent = ({ open, onClose, notificationItem }: DataProp) => {
  const initFormData: INotificationItemReq = {
    part: notificationItem?.part,
    partCode: notificationItem?.partCode,
    problem: notificationItem?.problem,
    dame: notificationItem?.dame,
    idText: notificationItem?.idText,
    cause: notificationItem?.cause,
    causeCode: notificationItem?.causeCode,
    causeText: notificationItem?.causeText,
    item: notificationItem?.item
  };

  const { control, setValue, handleSubmit, reset, clearErrors } = useForm<any>({
    defaultValues: initFormData,
    resolver: yupResolver(validationSchema)
  });

  const {
    data,
    notificationType,
    loadingGetParts,
    loadingGetProblems,
    loadingGetCauses,
    loadingGetPartCodes,
    loadingDames,
    loadingGetCauseCodes,
    parts,
    partCodes,
    problems,
    dames,
    causes,
    causeCodes,
    handleGetParts,
    handleGetPartCodes,
    handleGetProblems,
    handleGetCauses,
    handleGetDames,
    handleGetCauseCodes,
    handleUpdateNotificationItem
  } = useNotificationContext();

  const formUpdateModal = useRef<HTMLButtonElement>(null);

  const handleChangePart = (value: any) => {
    setValue('part', value);
    setValue('partCode', '');
    clearErrors('part');
    clearErrors('partCode');
    handleGetPartCodes({ codeGroups: value });
  };

  const handleChangeProblem = (value: any) => {
    setValue('problem', value);
    setValue('dame', '');
    clearErrors('problem');
    clearErrors('dame');
    handleGetDames({ codeGroups: value });
  };

  const handleChangeCause = (value: any) => {
    setValue('cause', value);
    setValue('causeCode', '');
    handleGetCauseCodes({ codeGroups: value });
  };

  const handleOnClose = () => {
    reset(initFormData);
    onClose();
  };

  const submitForm = (data: any) => {
    handleUpdateNotificationItem(data);
    handleOnClose();
  };

  useEffect(() => {
    if (open) {
      console.log(notificationItem);
      handleGetParts({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.objectParts });
      handleGetProblems({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.problems });
      handleGetCauses({ catalogProfiles: data?.header?.categoryProfile, catalogs: notificationType?.causes });
      if (notificationItem?.part) handleGetPartCodes({ codeGroups: notificationItem?.part });
      if (notificationItem?.problem) handleGetDames({ codeGroups: notificationItem?.problem });
      if (notificationItem?.cause) handleGetCauseCodes({ codeGroups: notificationItem?.cause });

      reset(initFormData);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <Modal
      open={open}
      title={`Chỉnh sửa thông báo ${notificationItem?.item}`}
      onClose={handleOnClose}
      onSubmit={() => formUpdateModal?.current?.click()}
      btnResetText="Huỷ"
      btnSubmitText="Sửa"
    >
      <form onSubmit={handleSubmit(submitForm)}>
        <button hidden={true} ref={formUpdateModal} type={'submit'} />
        <Box display="flex" alignItems="center" justifyContent="space-between" mt="18px">
          <Grid container columnSpacing={6}>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nhóm bộ phận</FormLabelCustom>
                <RHFSelect
                  id="part"
                  name="part"
                  variant="outlined"
                  loading={loadingGetParts}
                  disabled={loadingGetParts}
                  fullWidth
                  control={control}
                  options={convertDataOption('codeGroupDesc', 'codeGroup', parts)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangePart(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Bộ phận</FormLabelCustom>
                <RHFSelect
                  id="partCode"
                  name="partCode"
                  loading={loadingGetPartCodes}
                  disabled={loadingGetPartCodes}
                  variant="outlined"
                  fullWidth
                  options={convertDataOption('codeDesc', 'code', partCodes)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng</FormLabelCustom>
                <RHFSelect
                  id="problem"
                  name="problem"
                  loading={loadingGetProblems}
                  disabled={loadingGetProblems}
                  variant="outlined"
                  fullWidth
                  control={control}
                  options={convertDataOption('codeGroupDesc', 'codeGroup', problems)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangeProblem(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng chi tiết</FormLabelCustom>
                <RHFSelect
                  id="dame"
                  name="dame"
                  loading={loadingDames}
                  disabled={loadingDames}
                  variant="outlined"
                  fullWidth
                  control={control}
                  options={convertDataOption('codeDesc', 'code', dames)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Mô tả chi tiết</FormLabelCustom>
                <RHFTextarea id="idText" name="idText" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nhóm nguyên nhân lỗi</FormLabelCustom>
                <RHFSelect
                  id="cause"
                  name="cause"
                  loading={loadingGetCauses}
                  disabled={loadingGetCauses}
                  variant="outlined"
                  fullWidth
                  options={convertDataOption('codeGroupDesc', 'codeGroup', causes)}
                  control={control}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                  onChange={(e: any) => handleChangeCause(e.target.value)}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Phân loại hư hỏng chi tiết</FormLabelCustom>
                <RHFSelect
                  id="causeCode"
                  name="causeCode"
                  loading={loadingGetCauseCodes}
                  disabled={loadingGetCauseCodes}
                  variant="outlined"
                  fullWidth
                  control={control}
                  options={convertDataOption('codeDesc', 'code', causeCodes)}
                  placeholder={PLACEHOLDER_SELECT_INPUT}
                  displayEmpty
                  isPlaceholderOption
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormGroup sx={{ mt: 4 }}>
                <FormLabelCustom>Nguyên nhân lỗi chi tiết</FormLabelCustom>
                <RHFTextarea id="causeText" name="causeText" variant="outlined" fullWidth control={control} />
              </FormGroup>
            </Grid>
          </Grid>
        </Box>
      </form>
    </Modal>
  );
};

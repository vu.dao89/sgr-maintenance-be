import { Box, BoxProps, Button, CardMedia, Chip, Stack, Typography } from '@mui/material';
import { CardItem, Icon } from 'common/components';
import { convertStringToDateTime } from 'common/helpers';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
interface DataProp extends BoxProps {
  handleSubmitForm?: (data?: any) => void;
}

export const ActionTabComponent: React.FC<DataProp> = () => {
  const { data, workOrderDetail } = useNotificationContext();

  return (
    <Box mt={5}>
      <Box className="[&:not(:first-child)]:mt-3">
        {!workOrderDetail ? (
          <Box>
            <CardItem>
              <Box display="flex" alignItems="center">
                <Button sx={{ p: 0, minWidth: 24, color: 'red' }}>
                  <Icon name="add-blue" width={24} height={24} />
                  <Typography
                    ml={3}
                    fontWeight="500"
                    sx={{ color: theme => theme.palette.info.main, textTransform: 'capitalize' }}
                  >
                    Thêm lệnh bảo trì
                  </Typography>
                </Button>
              </Box>
            </CardItem>
          </Box>
        ) : (
          <CardItem title={workOrderDetail?.workOrder?.workOrderDescription}>
            <Box display="flex" alignItems="center" justifyContent="space-between" mt="18px">
              <Stack>
                <Typography>
                  {workOrderDetail?.workOrder?.equipmentDescription}
                  {workOrderDetail?.workOrder?.equipmentId && ` (${workOrderDetail?.workOrder?.equipmentId})`}
                </Typography>
                <Box display="flex" mt="6px">
                  <Typography color="grey.300" component="span" variant="body1">
                    Giao cho:
                  </Typography>
                  <Typography component="span" variant="body1" ml={1}>
                    {data?.header?.assignName}
                  </Typography>
                </Box>
                {/* confirm */}
                <Box mt="6px" display="flex">
                  <Typography color="grey.300" component="span" variant="body1">
                    {convertStringToDateTime(String(workOrderDetail?.workOrder?.woStartDate))}
                  </Typography>
                  {workOrderDetail?.workOrder?.woStartDate && (
                    <Typography
                      color="grey.300"
                      component="span"
                      variant="body1"
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        '&::before': {
                          content: '"-"',
                          display: 'block',
                          mx: '6px'
                        }
                      }}
                    >
                      {convertStringToDateTime(String(workOrderDetail?.workOrder?.woFinishDate))}
                    </Typography>
                  )}
                </Box>
                <Box mt={2} display="flex" flexWrap="wrap" gap={2}>
                  {workOrderDetail?.workOrder?.priority && (
                    <Chip
                      label={workOrderDetail?.workOrder?.priority}
                      color="info"
                      className="bg-pink"
                      sx={{ fontSize: 14, boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px' }}
                    />
                  )}
                  {workOrderDetail?.workOrder?.commonStatus?.description && (
                    <Chip
                      label={workOrderDetail?.workOrder?.commonStatus?.description}
                      sx={{
                        fontSize: 14,
                        bgcolor: `#${workOrderDetail?.workOrder?.commonStatus?.colorCode}`,
                        boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px'
                      }}
                    />
                  )}
                  {workOrderDetail?.workOrder?.workOrderTypeDescription && (
                    <Chip
                      label={workOrderDetail?.workOrder?.workOrderTypeDescription}
                      color="info"
                      sx={{ fontSize: 14, boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px' }}
                    />
                  )}
                </Box>
              </Stack>
              <Stack alignItems="center">
                <CardMedia
                  sx={{ borderRadius: 2, objectFit: 'cover', width: 80, height: 80 }}
                  component="img"
                  image={data?.image || '/iss/assetmanagement/static/images/no-image.jpg'}
                />
                {/* <Typography mt={5} variant="body1" color="grey.300">
                  {convertStringToDateTime(String(data?.header?.createOn))}
                </Typography> */}
              </Stack>
            </Box>
          </CardItem>
        )}
      </Box>
    </Box>
  );
};

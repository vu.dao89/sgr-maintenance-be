import { Box, BoxProps, Button, Grid, Typography } from '@mui/material';
import { CardItem, FormDataValue, Icon, Loading } from 'common/components';
import { NOT_FOUND_DATA } from 'common/constants';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { INotificationItemDetail } from 'modules/Notification/types';
import { useState } from 'react';
import { ModalCreateNotificationItemComponent, ModalUpdateNotificationItemComponent } from '../modal';

interface DataProp extends BoxProps {
  handleSubmitForm?: (data?: any) => void;
}

export const DetailTabComponent: React.FC<DataProp> = () => {
  const {
    data,
    loadingCreateNotificationItem,
    loadingDeleteNotificationItem,
    loadingUpdateNotificationItem,
    handleDeleteNotificationItem
  } = useNotificationContext();

  const [openModalCreate, setOpenModalCreate] = useState<boolean>(false);
  const [openModalUpdate, setOpenModalUpdate] = useState<boolean>(false);
  const [notificationSelected, setNotificationSelected] = useState<INotificationItemDetail>();

  const handleCloseModalCreate = () => {
    setOpenModalCreate(false);
  };

  const handleCloseModalUpdate = () => {
    setOpenModalUpdate(false);
  };

  const handleEdit = (data: INotificationItemDetail) => {
    setNotificationSelected(data);
    setOpenModalUpdate(true);
  };

  const handleRemove = (data: INotificationItemDetail) => {
    setNotificationSelected(data);
    handleDeleteNotificationItem(data?.item);
  };

  return (
    <>
      {!(loadingCreateNotificationItem || loadingDeleteNotificationItem || loadingUpdateNotificationItem) ? (
        <>
          <Box mt={5}>
            <Box>
              <CardItem>
                <Box display="flex" alignItems="center">
                  <Button sx={{ p: 0, minWidth: 24, color: 'red' }} onClick={() => setOpenModalCreate(true)}>
                    <Icon name="add-blue" width={24} height={24} />
                    <Typography
                      ml={3}
                      fontWeight="500"
                      sx={{ color: theme => theme.palette.info.main, textTransform: 'capitalize' }}
                    >
                      Thêm Notification items
                    </Typography>
                  </Button>
                </Box>
              </CardItem>
            </Box>
            {data?.items?.notificationItems?.map((item: INotificationItemDetail, index: number) => (
              <Box key={index} className="[&:not(:first-child)]:mt-3">
                <CardItem
                  title={`Thông báo ${item?.item}`}
                  data={item}
                  handleEdit={handleEdit}
                  handleRemove={handleRemove}
                  action
                >
                  <Grid container>
                    <Grid item xs={6}>
                      <FormDataValue label="Nhóm bộ phận" value={item?.partDesc || NOT_FOUND_DATA.EMPTY} />
                    </Grid>
                    <Grid item xs={6}>
                      <FormDataValue label="Bộ phận" value={item?.partCodeDesc || NOT_FOUND_DATA.EMPTY} />
                    </Grid>
                    <Grid item xs={6}>
                      <FormDataValue label="Phân loại hư hỏng" value={item?.problemDesc || NOT_FOUND_DATA.EMPTY} />
                    </Grid>
                    <Grid item xs={6}>
                      <FormDataValue
                        label="Phân loại hư hỏng chi tiết"
                        value={item?.dameDesc || NOT_FOUND_DATA.EMPTY}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FormDataValue label="Mô tả chi tiết" value={item?.idText || NOT_FOUND_DATA.EMPTY} />
                    </Grid>
                    <Grid item xs={6}>
                      <FormDataValue label="Nhóm nguyên nhân lỗi" value={item?.causeDesc || NOT_FOUND_DATA.EMPTY} />
                    </Grid>
                    <Grid item xs={6}>
                      <FormDataValue
                        label="Nhóm nguyên nhân lỗi"
                        value={item?.causeCodeDesc || NOT_FOUND_DATA.EMPTY}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FormDataValue
                        label="Nhóm nguyên nhân lỗi"
                        value={item?.causeText || NOT_FOUND_DATA.EMPTY}
                      />
                    </Grid>
                  </Grid>
                </CardItem>
              </Box>
            ))}
          </Box>
          <ModalCreateNotificationItemComponent 
            open={openModalCreate} 
            onClose={handleCloseModalCreate} 
          />
          <ModalUpdateNotificationItemComponent
            open={openModalUpdate}
            notificationItem={notificationSelected}
            onClose={handleCloseModalUpdate}
          />
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

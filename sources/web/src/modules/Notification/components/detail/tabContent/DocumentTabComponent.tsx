import { INotificationDocument } from '@modules/Notification';
import { Box, BoxProps, Card, CardContent, Grid } from '@mui/material';
import { FileDownloadComponent, ImageCardComponent, Loading, ModalConfirmDelete, RHFFile } from 'common/components';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

export const DocumentTabComponent: React.FC<BoxProps> = () => {
  const {
    data,
    loadingUpload,
    loadingCloseNotificationItemDocument,
    handleUploadDocument,
    handleCloseNotificationDocument
  } = useNotificationContext();
  const documents = data?.groupDocuments;
  const { control } = useForm<any>({
    defaultValues: {}
  });
  const [modalConfirmDelete, setModalConfirmDelete] = useState<boolean>(false);
  const [selectedDocument, setSelectedDocument] = useState<INotificationDocument>();

  const onConfirmDelete = async () => {
    await handleCloseNotificationDocument({
      notificationId: data?.header?.id,
      documentId: selectedDocument?.globalDocument?.id
    });
  };

  const onDownloadFile = (item: INotificationDocument) => {
    if (!item) return;

    const file = document.createElement('a');
    file.href = item.globalDocument.thumbnailUrl;
    file.download = item.globalDocument.fileName;
    file.click();
  };

  return (
    <Box mt={5}>
      <Box>
        <RHFFile
          id="files"
          name="files"
          loading={loadingUpload}
          control={control}
          handleUpload={handleUploadDocument}
          multiple
        />
      </Box>
      <>
        {!loadingUpload ? (
          <>
            {documents?.files?.length ? (
              <>
                <Box mt={3}>
                  <Grid container rowSpacing={2} columnSpacing={2}>
                    {documents?.files?.map((item: INotificationDocument, index: number) => (
                      <Grid item key={index} xs={6} md={4}>
                        <FileDownloadComponent
                          className="[&:not(:first-child)]:mt-3"
                          data={item.globalDocument}
                          loading={item.id === selectedDocument?.id ? loadingCloseNotificationItemDocument : false}
                          allowClear
                          onDownload={() => onDownloadFile(item)}
                          onDelete={() => {
                            setModalConfirmDelete(!modalConfirmDelete);
                            setSelectedDocument(item);
                          }}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </Box>
              </>
            ) : null}
            {documents?.images?.length ? (
              <Card variant="outlined" sx={{ mt: 3 }}>
                <CardContent>
                  <Grid container columnSpacing={2} rowGap={4}>
                    {documents?.images?.map((item: INotificationDocument, key: number) => (
                      <Grid key={key} item xs={12} sm={6} md={4} lg={3}>
                        <ImageCardComponent
                          key={key}
                          loading={item.id === selectedDocument?.id ? loadingCloseNotificationItemDocument : false}
                          data={item.globalDocument}
                          onDelete={() => {
                            setModalConfirmDelete(!modalConfirmDelete);
                            setSelectedDocument(item);
                          }}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </CardContent>
              </Card>
            ) : null}
          </>
        ) : (
          <Loading />
        )}
      </>
      <ModalConfirmDelete
        open={modalConfirmDelete}
        onClose={() => setModalConfirmDelete(!modalConfirmDelete)}
        onConfirm={onConfirmDelete}
      />
    </Box>
  );
};

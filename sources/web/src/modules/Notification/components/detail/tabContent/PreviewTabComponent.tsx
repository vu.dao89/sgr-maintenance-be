import { Box, BoxProps, Grid } from '@mui/material';
import { CardItem, FormDataValue } from 'common/components';
import { DATE_FORMAT, NOT_FOUND_DATA } from 'common/constants';
import { convertStringToDateTime } from 'common/helpers';
import { INotificationActiviti, INotificationDetail } from 'modules/Notification/types';

interface DataProp extends BoxProps {
  data?: INotificationDetail;
  handleSubmitForm?: (data?: any) => void;
}

export const PreviewTabComponent: React.FC<DataProp> = ({ data }) => {
  return (
    <Box mt={5}>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Thông tin tổng quan">
          <Grid container>
            <Grid item xs={6}>
              <FormDataValue label="Nơi bảo trì" value={data?.header?.maintenancePlant || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Tổ đội thực hiện" value={data?.header?.workCenter || NOT_FOUND_DATA.EMPTY} />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue label="Chi tiết" value={data?.header?.ltext || NOT_FOUND_DATA.EMPTY} />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem title="Thời gian xử lý yêu cầu">
          <Grid container>
            <Grid item xs={12}>
              <FormDataValue
                label="Ngày phát hiện sự cố"
                value={
                  convertStringToDateTime(
                    String(`${data?.header?.date} ${data?.header?.time}`),
                    DATE_FORMAT.DATE_TIME
                  ) || NOT_FOUND_DATA.EMPTY
                }
              />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue
                label="Yêu cầu xử vào ngày"
                value={
                  convertStringToDateTime(
                    String(`${data?.header?.requestStart} ${data?.header?.requestStartTime}`),
                    DATE_FORMAT.DATE_TIME
                  ) || NOT_FOUND_DATA.EMPTY
                }
              />
            </Grid>
            <Grid item xs={6}>
              <FormDataValue
                label="Đến ngày"
                value={
                  convertStringToDateTime(
                    String(`${data?.header?.requestEnd} ${data?.header?.requestEndTime}`),
                    DATE_FORMAT.DATE_TIME
                  ) || NOT_FOUND_DATA.EMPTY
                }
              />
            </Grid>
          </Grid>
        </CardItem>
      </Box>
      <Box className="[&:not(:first-child)]:mt-3">
        <CardItem
          title={
            data?.header?.malfunctionStart && data?.header?.malfunctionEnd
              ? 'Thời gian dừng vận hành'
              : 'Không có dừng vận hành'
          }
        >
          <Grid container>
            {data?.header?.malfunctionStart ? (
              <Grid item xs={6}>
                <FormDataValue
                  label="Từ ngày"
                  value={
                    convertStringToDateTime(
                      String(`${data?.header?.malfunctionStart} ${data?.header?.malfunctionStartTime}`),
                      DATE_FORMAT.DATE_TIME
                    ) || NOT_FOUND_DATA.EMPTY
                  }
                />
              </Grid>
            ) : null}
            {data?.header?.malfunctionEnd ? (
              <Grid item xs={6}>
                <FormDataValue
                  label="Đến ngày"
                  value={
                    convertStringToDateTime(
                      String(`${data?.header?.malfunctionEnd} ${data?.header?.malfunctionEndTime}`),
                      DATE_FORMAT.DATE_TIME
                    ) || NOT_FOUND_DATA.EMPTY
                  }
                />
              </Grid>
            ) : null}
          </Grid>
        </CardItem>
      </Box>
      {data?.activities?.notificationActivities?.length && (
        <>
          {data?.activities?.notificationActivities?.map((item: INotificationActiviti, key: number) => (
            <Box key={key} className="[&:not(:first-child)]:mt-3">
              <CardItem title={item?.activity}>
                <Grid container>
                  <Grid item xs={6}>
                    {/* confirm */}
                    <FormDataValue label="Nhóm khắc phục" value={`${item?.activity} (${item?.activityCode})`} />
                  </Grid>
                  <Grid item xs={6}>
                    <FormDataValue
                      label="Chi tiết khắc phục"
                      value={`${item?.activityDesc} (${item?.activityCodeDesc})`}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FormDataValue label="Mô tả chi tiết" value={item?.activityText} />
                  </Grid>
                </Grid>
              </CardItem>
            </Box>
          ))}
        </>
      )}
    </Box>
  );
};

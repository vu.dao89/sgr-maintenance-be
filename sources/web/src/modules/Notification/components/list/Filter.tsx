import { NOTIFICATION_FILTER_FIELD } from '@modules/Notification/constants';
import { IFormFilterNotification, INotificationFilterReqDto } from '@modules/Notification/types';
import { FormGroup, FormLabel, Grid } from '@mui/material';
import { FilterInput, RHFSelect } from 'common/components';
import { RHFCalendar } from 'common/components/RHForm/RHFCalendar';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { compact, isEmpty } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';

interface IFilterProps {
  filter: INotificationFilterReqDto | undefined;
  filterCount?: number;
  status: IOption[];
  plannerGroup: IOption[];
  mainPlant: IOption[];
  type: IOption[];
  priority: IOption[];
  reportBy: IOption[];
  assignTo: IOption[];
  onFilter: (value: any) => any;
  onGetPriority: (value: any) => any;
}

const defaultValues: IFormFilterNotification = {
  description: '',
  createFrom: '',
  createTo: '',
  reportBy: '',
  assignTo: '',
  plants: '',
  priorities: '',
  types: '',
  plannerGroups: '',
  status: ''
};

export const FilterNotification = ({
  filter,
  filterCount,
  status,
  mainPlant,
  type,
  priority,
  plannerGroup,
  reportBy,
  assignTo,
  onFilter,
  onGetPriority
}: IFilterProps) => {
  const { control, handleSubmit, watch, reset, setValue } = useForm<IFormFilterNotification>({
    defaultValues
  });

  const formValues = watch([
    NOTIFICATION_FILTER_FIELD.DESCRIPTION,
    NOTIFICATION_FILTER_FIELD.CREATE_FROM,
    NOTIFICATION_FILTER_FIELD.CREATE_TO,
    NOTIFICATION_FILTER_FIELD.REPORT_BY,
    NOTIFICATION_FILTER_FIELD.ASSIGN_TO,
    NOTIFICATION_FILTER_FIELD.PLANTS,
    NOTIFICATION_FILTER_FIELD.PRIORITY,
    NOTIFICATION_FILTER_FIELD.TYPE,
    NOTIFICATION_FILTER_FIELD.PLANNER_GROUP,
    NOTIFICATION_FILTER_FIELD.STATUS
  ]);
  const isDisabled = useMemo(() => isEmpty(compact(formValues)), [formValues]);

  const onSubmit = handleSubmit((formData: IFormFilterNotification) => {
    onFilter(formData);
  });

  const onReset = handleSubmit(() => {
    reset(defaultValues);
    onFilter({ description: filter?.description });
  });

  useEffect(() => {
    reset({
      description: filter?.description || '',
      createFrom: filter?.createFrom || '',
      createTo: filter?.createTo || '',
      reportBy: filter?.reportBy || '',
      assignTo: filter?.assignTo || '',
      plants: filter?.plants || '',
      priorities: filter?.priorities || '',
      types: filter?.types || '',
      plannerGroups: filter?.plannerGroups || '',
      status: filter?.status || ''
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <FilterInput
      title="Lọc thông báo"
      filterCount={filterCount}
      isDisabled={isDisabled}
      onSubmit={onSubmit}
      onReset={onReset}
      maxWidth={800}
    >
      <Grid container spacing={2} columnSpacing={6}>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Nơi bảo trì</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.PLANTS}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={mainPlant || []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Từ ngày thông báo</FormLabel>
            <RHFCalendar name={NOTIFICATION_FILTER_FIELD.CREATE_FROM} control={control} />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Đến ngày</FormLabel>
            <RHFCalendar name={NOTIFICATION_FILTER_FIELD.CREATE_TO} control={control} />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Trạng thái</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.STATUS}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={status || []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Người tạo</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.REPORT_BY}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={reportBy || []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Người nhận</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.ASSIGN_TO}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={assignTo || []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Loại thông báo</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.TYPE}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={type ?? []}
              control={control}
              onChange={(e: any) => {
                setValue(NOTIFICATION_FILTER_FIELD.TYPE, e.target.value);
                onGetPriority(e.target.value || '');
              }}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Mức độ ưu tiên</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.PRIORITY}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={priority ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={4}>
          <FormGroup>
            <FormLabel>Bộ phận kế hoạch</FormLabel>
            <RHFSelect
              name={NOTIFICATION_FILTER_FIELD.PLANNER_GROUP}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={plannerGroup ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
      </Grid>
    </FilterInput>
  );
};

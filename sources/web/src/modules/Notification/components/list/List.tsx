import { INotificationItem } from '@modules/Notification/types';
import { Box, Button, FormControl, Paper, Stack, TableContainer, Typography } from '@mui/material';
import { Pagination, SelectDisplayColumn } from 'common/components';
import { DataTable, IColumnType } from 'common/components/DataTable';
import { Icon } from 'common/components/Icon';
import { PLACEHOLDER_DISPLAY_COLUMN } from 'common/constants';

interface INotificationProps {
  items: INotificationItem[];
  pagination?: IPagination;
  handleChangePaging: any;
  columns: IColumnType<INotificationItem>[];
  setColumns: (value: any) => any;
  setIsOpenCreateModel: (value: boolean) => any;
}

export const NotificationList = ({
  items,
  pagination,
  handleChangePaging,
  columns,
  setColumns,
  setIsOpenCreateModel
}: INotificationProps) => {
  const onGoToPage = (e: any) => {
    if (e.keyCode == 13) {
      handleChangePaging(e, e.target.value);
    }
  };

  return (
    <Stack p={4} sx={{ borderRadius: 3, backgroundColor: '#FFF' }}>
      <TableContainer component={Paper} elevation={0}>
        <Box display="flex" justifyContent="space-between" mb={5}>
          <Typography variant="h5">Danh sách thông báo</Typography>
          <Box display="flex" justifyContent="space-between">
            <FormControl sx={{ width: '180px' }}>
              <FormControl sx={{ width: '180px' }}>
                <SelectDisplayColumn
                  id="display-column"
                  options={columns}
                  placeholder={PLACEHOLDER_DISPLAY_COLUMN}
                  onSelect={setColumns}
                />
              </FormControl>
            </FormControl>
            <Button
              disableRipple
              sx={{ backgroundColor: 'primary', fontSize: 16, textTransform: 'none', ml: 4 }}
              type="button"
              variant="contained"
              onClick={() => setIsOpenCreateModel(true)}
            >
              <Icon name="add" className="mr-[6px]" width={16} height={16} />
              Tạo thông báo
            </Button>
          </Box>
        </Box>
        <DataTable
          data={items}
          columns={columns}
          hasActionColumn={true}
          canViewDetail={true}
          url={'/notification'}
          noDataText="Không tìm thấy thông báo"
        />
      </TableContainer>

      <Pagination
        page={pagination?.page ?? 0}
        limit={pagination?.size ?? 0}
        totalPages={pagination?.totalPages ?? 0}
        totalItems={pagination?.totalItems ?? 0}
        onGoToPage={onGoToPage}
        onChange={handleChangePaging}
      />
    </Stack>
  );
};

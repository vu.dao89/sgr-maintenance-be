import { yupResolver } from '@hookform/resolvers/yup';
import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { validationSchema } from '@modules/Notification/containers/create/validation';
import { ICreateNotificationReq, INotificationGeneralForm } from '@modules/Notification/types/create';
import { Box, Checkbox, FormControlLabel, FormGroup, Grid, Stack, Switch, Typography } from '@mui/material';
import {
  BpCheckedIcon,
  BpIcon,
  CardItem,
  FormLabelCustom,
  Loading,
  RHFSelect,
  RHFTextarea,
  RHFTextInput
} from 'common/components';
import { RHFDateTimePicker } from 'common/components/RHForm/RHFDateTimePicker';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { isEmpty } from 'lodash';
import { useCallback, useEffect, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { defaultValues as defaultValuesItemNotification } from './NotificationTab';

interface Props {
  step: NOTIFICATION_CREATE_TABS;
  data: ICreateNotificationReq;
  type: any[];
  priority: IOption[];
  functionLocation: IOption[];
  equipment: IOption[];
  workCenter: IOption[];
  mainPlant: IOption[];
  assigner: IOption[];
  isOnlineAssign: boolean;
  isSubmitted: boolean;
  setIsSubmitted: (value: boolean) => any;
  changeAssignType: (value: boolean) => any;
  handleGetPriority: (value: any) => any;
  handleGetAssigner: (value: any) => any;
  handleGetFunctionLocation: (value?: any) => any;
  handleGetWorkCenter: (value?: any) => any;
  handleGetEquipment: (value?: any) => any;
  setAssigner: (value: any) => any;
  setPriority: (value: any) => any;
  setStep: (value: number) => any;
  setData: (data: any) => any;
  loading: boolean;
}

export const GeneralTabComponent = ({
  step,
  data,
  type,
  priority,
  functionLocation,
  equipment,
  workCenter,
  mainPlant,
  assigner,
  isOnlineAssign,
  isSubmitted,
  setIsSubmitted,
  changeAssignType,
  handleGetPriority,
  handleGetAssigner,
  handleGetFunctionLocation,
  handleGetWorkCenter,
  handleGetEquipment,
  setAssigner,
  setPriority,
  setStep,
  setData,
  loading
}: Props) => {
  const defaultValues: INotificationGeneralForm = {
    type: data?.header?.type || '',
    desc: data?.header?.desc || '',
    maintenancePlant: data?.header?.maintenancePlant || '',
    workCenter: data?.header?.workCenter || '',
    functionalLocationId: data?.header?.functionalLocationId || '',
    equipmentId: data?.header?.equipmentId || '',
    priority: data?.header?.priority || '',
    assignTo: data?.header?.assignTo || '',
    date: data?.header?.date || '',
    requestStart: data?.header?.requestStart || '',
    requestEnd: data?.header?.requestEnd || '',
    breakDown: data?.header?.breakDown || false,
    malfunctionStart: data?.header?.malfunctionStart || '',
    malfunctionEnd: data?.header?.malfunctionEnd || ''
  };
  const refSubmitButton = useRef<HTMLButtonElement>(null);
  const {
    control,
    handleSubmit,
    setValue,
    clearErrors,
    formState: { isValid }
  } = useForm<INotificationGeneralForm>({
    defaultValues,
    resolver: yupResolver(validationSchema)
  });

  const onSubmit = (formData: any) => {
    if (step === NOTIFICATION_CREATE_TABS.GENERAL && isValid) {
      setData({ ...data, header: { ...formData } });
      setIsSubmitted(false);
      setStep(NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION);
    }
  };

  // Get priority by notification type
  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.GENERAL && !isEmpty(data?.header?.type)) {
      const responsePriority = async () => {
        const selectedType = type.filter((item: any) => item.notiType === data?.header?.type);

        if (!selectedType.length) return;

        await handleGetPriority({ types: selectedType[0]?.priorityTypeId });
      };

      responsePriority();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, data?.header?.type]);

  // Get assigner by work center
  // TODO: get assign via online status
  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.GENERAL && !isEmpty(data?.header?.workCenter)) {
      const responseAssigner = async () => {
        await handleGetAssigner({ workCenterIds: data?.header?.workCenter });
      };

      responseAssigner();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, data?.header?.workCenter]);

  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.GENERAL && isSubmitted) {
      refSubmitButton?.current?.click();
    }

    setIsSubmitted(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSubmitted]);

  const onSelectType = useCallback(async (e: any) => {
    setValue(`type`, e.target.value);
    setValue('priority', '');
    clearErrors('type');

    if (!e.target.value) {
      setPriority([]);
      return;
    }

    const selectedType = type.filter((item: any) => item.notiType === e.target.value);

    if (!selectedType.length) return;

    // reset notification items
    const createItemNotificationReq: any = {
      notificationItems: [defaultValuesItemNotification]
    };
    setData({ ...data, items: createItemNotificationReq });

    await handleGetPriority({ types: selectedType[0]?.priorityTypeId });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectWorkCenter = useCallback(async (e: any) => {
    setValue(`workCenter`, e.target.value);
    setValue('assignTo', '');
    clearErrors('workCenter');

    if (!e.target.value) {
      setAssigner([]);
      return;
    }

    await handleGetAssigner({ workCenterIds: e.target.value });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectMaintenancePlant = useCallback(async (e: any) => {
    setValue(`maintenancePlant`, e.target.value);
    setValue('functionalLocationId', '');
    setValue('workCenter', '');
    clearErrors('maintenancePlant');

    if (!e.target.value) {
      await handleGetFunctionLocation();
      await handleGetWorkCenter();
      return;
    }

    await handleGetFunctionLocation({ maintenancePlantCodes: e.target.value });
    await handleGetWorkCenter({ maintenancePlantCodes: e.target.value });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectFunctionLocation = useCallback(async (e: any) => {
    setValue(`functionalLocationId`, e.target.value);
    setValue('equipmentId', '');
    clearErrors('functionalLocationId');

    if (!e.target.value) {
      await handleGetEquipment();
      return;
    }

    await handleGetEquipment({ functionalLocationCodes: e.target.value });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading)
    return (
      <Box className="flex justify-center items-center">
        <Loading />
      </Box>
    );

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <button hidden={true} ref={refSubmitButton} type={'submit'} />
      <Stack>
        <Box>
          <CardItem title="" collapsible={false}>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Grid container columnSpacing={6}>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Tiêu đề thông báo</FormLabelCustom>
                    <RHFTextInput
                      id="desc"
                      name="desc"
                      variant="outlined"
                      fullWidth
                      placeholder={'Nhập tiêu đề'}
                      inputProps={{ maxLength: 40 }}
                      control={control}
                      size="small"
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Phân loại thông báo</FormLabelCustom>
                    <RHFSelect
                      id="type"
                      name="type"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={type || []}
                      control={control}
                      onChange={onSelectType}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={12}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom>Mô tả thông báo</FormLabelCustom>
                    <RHFTextarea
                      id="lText"
                      name="lText"
                      variant="outlined"
                      rows={2}
                      inputProps={{ maxLength: 5000 }}
                      fullWidth
                      placeholder="Nhập mô tả thông báo"
                      control={control}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Nơi bảo trì</FormLabelCustom>
                    <RHFSelect
                      id="maintenancePlant"
                      name="maintenancePlant"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={mainPlant || []}
                      control={control}
                      onChange={onSelectMaintenancePlant}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Tổ đội thực hiện</FormLabelCustom>
                    <RHFSelect
                      id="workCenter"
                      name="workCenter"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={workCenter || []}
                      control={control}
                      onChange={onSelectWorkCenter}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Khu vực chức năng</FormLabelCustom>
                    <RHFSelect
                      id="functionalLocationId"
                      name="functionalLocationId"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={functionLocation || []}
                      control={control}
                      onChange={onSelectFunctionLocation}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Mã thiết bị</FormLabelCustom>
                    <RHFSelect
                      id="equipmentId"
                      name="equipmentId"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={equipment || []}
                      control={control}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Mức độ ưu tiên</FormLabelCustom>
                    <RHFSelect
                      id="priority"
                      name="priority"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={priority || []}
                      control={control}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Thông báo tới</FormLabelCustom>
                    <RHFSelect
                      id="assignTo"
                      name="assignTo"
                      variant="outlined"
                      fullWidth
                      placeholder={PLACEHOLDER_SELECT_INPUT}
                      displayEmpty
                      isPlaceholderOption
                      options={assigner || []}
                      control={control}
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} mt={6}>
                  <FormGroup sx={{ mt: 0 }}>
                    <FormControlLabel
                      control={<Checkbox size="small" checkedIcon={<BpCheckedIcon />} icon={<BpIcon />} />}
                      label={<Typography fontWeight={300}>Chỉ tìm người đang trực</Typography>}
                      checked={isOnlineAssign ?? false}
                      onChange={() => {
                        changeAssignType(!isOnlineAssign);
                      }}
                    />
                  </FormGroup>
                </Grid>
              </Grid>
            </Box>
          </CardItem>
        </Box>
        <Box mt={3}>
          <CardItem title="Thời gian yêu cầu xử lý" sx={{ mt: 3 }}>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Grid container columnSpacing={6}>
                <Grid item xs={12} md={12}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Ngày phát hiện sự cố</FormLabelCustom>
                    <RHFDateTimePicker name="date" disableFuture control={control} />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Yêu cầu xử lý vào ngày</FormLabelCustom>
                    <RHFDateTimePicker name="requestStart" control={control} />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom required>Đến ngày</FormLabelCustom>
                    <RHFDateTimePicker name="requestEnd" control={control} />
                  </FormGroup>
                </Grid>
              </Grid>
            </Box>
          </CardItem>
        </Box>
        <Box mt={3}>
          <CardItem title="" collapsible={false} sx={{ mt: 3 }}>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Grid container columnSpacing={6}>
                <Grid item xs={12} md={12}>
                  <FormGroup sx={{ mt: 0 }}>
                    <FormControlLabel
                      control={<Switch name="breakDown" defaultChecked={false} />}
                      label="Dừng vận hành"
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom>Từ ngày</FormLabelCustom>
                    <RHFDateTimePicker name="malfunctionStart" control={control} />
                  </FormGroup>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormGroup sx={{ mt: 4 }}>
                    <FormLabelCustom>Đến ngày</FormLabelCustom>
                    <RHFDateTimePicker name="malfunctionEnd" clearable control={control} />
                  </FormGroup>
                </Grid>
              </Grid>
            </Box>
          </CardItem>
        </Box>
      </Stack>
    </form>
  );
};

import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { INotificationItemForm } from '@modules/Notification/types/create';
import { Box, FormGroup, Grid } from '@mui/material';
import { CardItem, FormLabelCustom, RHFSelect, RHFTextarea } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { isEmpty } from 'lodash';
import { useCallback, useEffect, useState } from 'react';
import { Control } from 'react-hook-form';

interface IProps {
  index: number;
  step: number;
  item: INotificationItemForm;
  control: Control<INotificationItemForm, any>;
  objectPart: IOption[];
  problem: IOption[];
  cause: IOption[];
  handleRemove: (data: any) => any;
  handleGetObjectPartCode: (value: any) => any;
  handleGetProblemCode: (value: any) => any;
  handleGetCauseCode: (value: any) => any;
  setValue: any;
  clearErrors: (value: any) => any;
}

export const NotificationItem = ({
  index,
  step,
  item,
  control,
  objectPart,
  problem,
  cause,
  handleRemove,
  handleGetObjectPartCode,
  handleGetProblemCode,
  handleGetCauseCode,
  setValue,
  clearErrors
}: IProps) => {
  const [objectPartCode, setObjectPartCode] = useState<any>([]);
  const [problemCode, setProblemCode] = useState<any>([]);
  const [causeCode, setCauseCode] = useState<any>([]);

  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION && !isEmpty(item?.part)) {
      const responsePartCode = async () =>
        await handleGetObjectPartCode({ codeGroups: item?.part }).then((res: any) => setObjectPartCode(res));
      responsePartCode();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, item?.part]);

  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION && !isEmpty(item?.problem)) {
      const responseProblemCode = async () =>
        await handleGetProblemCode({ codeGroups: item?.problem }).then((res: any) => setProblemCode(res));
      responseProblemCode();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, item?.problem]);

  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION && !isEmpty(item?.cause)) {
      const responseCauseCode = async () =>
        await handleGetCauseCode({ codeGroups: item?.cause }).then((res: any) => setCauseCode(res));
      responseCauseCode();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, item?.cause]);

  const onSelectPart = useCallback(async (e: any) => {
    setValue(`notificationItems[${index}].part`, e.target.value);
    setValue(`notificationItems[${index}].partCode`, '');
    clearErrors(`notificationItems[${index}].part`);
    clearErrors(`notificationItems[${index}].partCode`);

    if (!e.target.value) {
      setObjectPartCode([]);
      return;
    }

    const responsePartCode = await handleGetObjectPartCode({ codeGroups: e.target.value });

    if (!isEmpty(responsePartCode)) setObjectPartCode(responsePartCode);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectProblem = useCallback(async (e: any) => {
    setValue(`notificationItems[${index}].problem`, e.target.value);
    setValue(`notificationItems[${index}].dame`, '');
    clearErrors(`notificationItems[${index}].problem`);
    clearErrors(`notificationItems[${index}].dame`);

    if (!e.target.value) {
      setProblemCode([]);
      return;
    }

    const responseProblemCode = await handleGetProblemCode({ codeGroups: e.target.value });

    if (!isEmpty(responseProblemCode)) setProblemCode(responseProblemCode);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectCause = useCallback(async (e: any) => {
    setValue(`notificationItems[${index}].cause`, e.target.value);
    setValue(`notificationItems[${index}].causeCode`, '');
    clearErrors(`notificationItems[${index}].cause`);
    clearErrors(`notificationItems[${index}].causeCode`);

    if (!e.target.value) {
      setCauseCode([]);
      return;
    }

    const responseCauseCode = await handleGetCauseCode({ codeGroups: e.target.value });

    if (!isEmpty(responseCauseCode)) setCauseCode(responseCauseCode);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <CardItem title={`Thông báo ${index + 1}`} clearable handleRemove={handleRemove}>
      <Box display="flex" alignItems="center" justifyContent="space-between" mt="18px">
        <Grid container columnSpacing={6}>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Nhóm bộ phận</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].part`}
                name={`notificationItems[${index}].part`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={objectPart || []}
                control={control}
                onChange={onSelectPart}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Bộ phận</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].partCode`}
                name={`notificationItems[${index}].partCode`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={objectPartCode || []}
                control={control}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Phân loại hư hỏng</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].problem`}
                name={`notificationItems[${index}].problem`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={problem || []}
                control={control}
                onChange={onSelectProblem}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Phân loại hư hỏng chi tiết</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].dame`}
                name={`notificationItems[${index}].dame`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={problemCode || []}
                control={control}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Nhóm nguyên nhân lỗi</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].cause`}
                name={`notificationItems[${index}].cause`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={cause || []}
                control={control}
                onChange={onSelectCause}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={6}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Nguyên nhân lỗi chi tiết</FormLabelCustom>
              <RHFSelect
                id={`notificationItems[${index}].causeCode`}
                name={`notificationItems[${index}].causeCode`}
                variant="outlined"
                fullWidth
                placeholder={PLACEHOLDER_SELECT_INPUT}
                displayEmpty
                isPlaceholderOption
                options={causeCode || []}
                control={control}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={12}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Mô tả nguyên nhân lỗi</FormLabelCustom>
              <RHFTextarea
                id={`notificationItems[${index}].causeText`}
                name={`notificationItems[${index}].causeText`}
                variant="outlined"
                fullWidth
                control={control}
                rows={2}
                placeholder="Mô tả"
                inputProps={{ maxLength: 40 }}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12} md={12}>
            <FormGroup sx={{ mt: 4 }}>
              <FormLabelCustom>Mô tả chi tiết</FormLabelCustom>
              <RHFTextarea
                id={`notificationItems[${index}].idText`}
                name={`notificationItems[${index}].idText`}
                variant="outlined"
                fullWidth
                control={control}
                rows={2}
                placeholder="Nhập mô tả chi tiết"
                inputProps={{ maxLength: 5000 }}
              />
            </FormGroup>
          </Grid>
        </Grid>
      </Box>
    </CardItem>
  );
};

import { yupResolver } from '@hookform/resolvers/yup';
import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { validateNotificationItemSchema } from '@modules/Notification/containers/create/validation';
import { ICreateNotificationReq, INotificationItemForm } from '@modules/Notification/types/create';
import { Box, Button, Stack, Typography } from '@mui/material';
import { CardItem, Icon } from 'common/components';
import { useEffect, useRef } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { NotificationItem } from './NotificationItem';

interface Props {
  step: NOTIFICATION_CREATE_TABS;
  data: ICreateNotificationReq;
  objectPart: any[];
  problem: IOption[];
  cause: IOption[];
  isSubmitted: boolean;
  setIsSubmitted: (value: boolean) => any;
  setStep: (value: number) => any;
  setData: (data: any) => any;
  handleGetObjectPartCode: (data: any) => any;
  handleGetProblemCode: (data: any) => any;
  handleGetCauseCode: (data: any) => any;
  loading: boolean;
}

export const defaultValues: INotificationItemForm = {
  part: '',
  partCode: '',
  problem: '',
  dame: '',
  idText: '',
  cause: '',
  causeCode: '',
  causeText: ''
};

export const AddNotificationComponent = ({
  step,
  setStep,
  data,
  setData,
  objectPart,
  problem,
  cause,
  isSubmitted,
  setIsSubmitted,
  handleGetObjectPartCode,
  handleGetCauseCode,
  handleGetProblemCode
}: Props) => {
  const refSubmitButton = useRef<HTMLButtonElement>(null);

  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
    clearErrors
  } = useForm<any>({
    defaultValues: {
      notificationItems: data?.items?.notificationItems || [defaultValues]
    },
    resolver: yupResolver(validateNotificationItemSchema),
    mode: 'all',
    reValidateMode: 'onChange'
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'notificationItems'
  });

  const handleAddItem = () => {
    append(defaultValues);
  };

  const handleRemoveItem = (index: number) => {
    remove(index);
  };

  const onSubmit = (formData: any) => {
    if (step === NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION && errors) {
      setData({ ...data, items: { ...formData } });
      setStep(NOTIFICATION_CREATE_TABS.UPLOAD);
      setIsSubmitted(false);
    }
  };

  useEffect(() => {
    if (step === NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION && isSubmitted) {
      refSubmitButton?.current?.click();
    }

    setIsSubmitted(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSubmitted]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <button hidden={true} ref={refSubmitButton} type={'submit'} />
      <Stack>
        <CardItem>
          <Button sx={{ p: 0, minWidth: 24, color: 'red' }} fullWidth onClick={handleAddItem}>
            <Icon name="add-blue" width={24} height={24} />
            <Typography ml={3} fontWeight="500" sx={{ color: theme => theme.palette.info.main }}>
              Thêm Notification items
            </Typography>
          </Button>
        </CardItem>
        <Box mt={3}>
          {fields &&
            fields.map((item: any, index) => {
              return (
                <NotificationItem
                  key={index}
                  index={index}
                  item={item}
                  step={step}
                  control={control}
                  objectPart={objectPart}
                  problem={problem}
                  cause={cause}
                  handleRemove={() => handleRemoveItem(index)}
                  handleGetObjectPartCode={handleGetObjectPartCode}
                  handleGetProblemCode={handleGetProblemCode}
                  handleGetCauseCode={handleGetCauseCode}
                  setValue={setValue}
                  clearErrors={clearErrors}
                />
              );
            })}
        </Box>
      </Stack>
    </form>
  );
};

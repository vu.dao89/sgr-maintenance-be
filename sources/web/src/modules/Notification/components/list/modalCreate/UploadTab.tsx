import { Box, Card, CardContent, Grid } from '@mui/material';
import { FileDownloadComponent, ImageCardComponent, Loading, ModalConfirmDelete, RHFFile } from 'common/components';
import { IGlobalDocument } from 'common/dto';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

interface IProps {
  documentGroup: any;
  loadingUpload: boolean;
  onUploadDocument: (value: any) => any;
  onDeleteDocument: (value: any) => any;
}

export const UploadTabComponent = ({ documentGroup, loadingUpload, onUploadDocument, onDeleteDocument }: IProps) => {
  const { control } = useForm<any>({
    defaultValues: {}
  });
  const [deleteId, setDeleteId] = useState<string>('');
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);
  const handleOpenModal = (id?: string) => {
    if (!confirmDelete && id) {
      setDeleteId(id);
    } else {
      setDeleteId('');
    }
    setConfirmDelete(!confirmDelete);
  };

  const handleDeleteDocument = async () => {
    await onDeleteDocument(deleteId);
    setConfirmDelete(false);
  };

  return (
    <Box mt={5}>
      <Box>
        <RHFFile
          id="files"
          name="files"
          loading={loadingUpload && false}
          control={control}
          handleUpload={onUploadDocument}
          multiple
        />
      </Box>
      <>
        {!loadingUpload ? (
          <>
            {documentGroup?.files?.length ? (
              <>
                <Box mt={3}>
                  <Grid container rowSpacing={2} columnSpacing={2}>
                    {documentGroup?.files?.map((item: IGlobalDocument, index: number) => (
                      <Grid item key={index} xs={6} md={4}>
                        <FileDownloadComponent
                          className="[&:not(:first-child)]:mt-3"
                          data={item}
                          allowClear
                          onDelete={() => handleOpenModal(item.id)}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </Box>
              </>
            ) : null}
            {documentGroup?.images?.length ? (
              <Card variant="outlined" sx={{ mt: 3 }}>
                <CardContent>
                  <Grid container columnSpacing={2} rowGap={4}>
                    {documentGroup?.images?.map((item: IGlobalDocument, key: number) => (
                      <Grid key={key} item xs={12} sm={6} md={4} lg={3}>
                        <ImageCardComponent key={key} data={item} onDelete={() => handleOpenModal(item.id)} />
                      </Grid>
                    ))}
                  </Grid>
                </CardContent>
              </Card>
            ) : null}
          </>
        ) : (
          <Loading />
        )}
      </>
      <ModalConfirmDelete open={confirmDelete} handleOpenModal={handleOpenModal} handleDelete={handleDeleteDocument} />
    </Box>
  );
};

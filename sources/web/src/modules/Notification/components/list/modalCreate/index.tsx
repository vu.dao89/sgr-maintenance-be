import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { ICreateNotificationReq } from '@modules/Notification/types/create';
import { Box, BoxProps, Tab, Tabs, Typography, useTheme } from '@mui/material';
import { Icon } from 'common/components';
import { GeneralTabComponent } from './GeneralTab';
import { AddNotificationComponent } from './NotificationTab';
import { UploadTabComponent } from './UploadTab';

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      className="w-full shrink"
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`
  };
}

const TABS = [
  { id: 0, name: 'Nhập thông tin chính' },
  { id: 1, name: 'Thêm thông báo chi tiết' },
  { id: 2, name: 'Đính kèm tài liệu' }
];
interface INotificationCreateForm extends BoxProps {
  step: number;
  data: ICreateNotificationReq;
  type: any[];
  priority: IOption[];
  functionLocation: IOption[];
  equipment: IOption[];
  workCenter: IOption[];
  mainPlant: IOption[];
  assigner: IOption[];
  objectPart: IOption[];
  problem: IOption[];
  cause: IOption[];
  isOnlineAssign: boolean;
  isSubmitted: boolean;
  setIsSubmitted: (value: boolean) => any;
  changeAssignType: (value: boolean) => any;
  setStep: (value: number) => any;
  setData: (data: any) => any;
  handleGetObjectPartCode: (value: any) => any;
  handleGetProblemCode: (value: any) => any;
  handleGetCauseCode: (value: any) => any;
  handleGetPriority: (value: any) => any;
  handleGetFunctionLocation: (value?: any) => any;
  handleGetWorkCenter: (value?: any) => any;
  handleGetEquipment: (value?: any) => any;
  setPriority: (value: any) => any;
  handleGetAssigner: (value: any) => any;
  setAssigner: (value: any) => any;
  loadingGeneralTab: boolean;
  loadingAddNotificationTab: boolean;
  loadingUpload: boolean;
  handleUploadDocument: (value: any) => any;
  handleDeleteDocument: (value: any) => any;
  handleCreateNotification: (value: any) => any;
  documentGroup: any;
}

export const NotificationCreateForm: React.FC<INotificationCreateForm> = ({
  step = 0,
  data,
  type,
  priority,
  functionLocation,
  equipment,
  workCenter,
  mainPlant,
  assigner,
  objectPart,
  problem,
  cause,
  isOnlineAssign,
  isSubmitted,
  setIsSubmitted,
  changeAssignType,
  setStep,
  setData,
  handleGetObjectPartCode,
  handleGetProblemCode,
  handleGetCauseCode,
  handleGetPriority,
  handleGetAssigner,
  handleGetFunctionLocation,
  handleGetWorkCenter,
  handleGetEquipment,
  setPriority,
  setAssigner,
  loadingGeneralTab,
  loadingAddNotificationTab,
  loadingUpload,
  handleUploadDocument,
  handleDeleteDocument,
  handleCreateNotification,
  documentGroup,
  ...props
}) => {
  const theme = useTheme();

  return (
    <Box display="flex" width="100%" pt={6} sx={{ height: 'calc(100vh - 215px)' }} {...props}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={step}
        className="min-w-[270px] mr-6"
        sx={{
          '& .MuiTabs-indicator': {
            display: 'none'
          }
        }}
      >
        {TABS.map(item => (
          <Tab
            key={item.id}
            disableRipple
            disabled
            {...a11yProps(item.id)}
            className={`mx-0 p-4 justify-start ${item.id < step ? 'Mui-done' : ''}`}
            sx={{
              '&.Mui-selected': {
                borderLeft: `2px solid ${theme.palette.primary.main}`,
                borderRight: 0,
                '& .step': {
                  color: '#2B2512',
                  borderColor: theme.palette.primary.main,
                  backgroundColor: theme.palette.primary.main
                }
              },
              '&.Mui-done': {
                borderRight: 0,
                '& .step': {
                  backgroundColor: theme.palette.primary.main
                }
              }
            }}
            label={
              <Box className="flex items-center w-full">
                <Typography
                  width={40}
                  height={40}
                  className="step flex items-center justify-center rounded-full mr-3"
                  sx={{ border: `1px solid ${theme.palette.grey[600]}`, color: theme.palette.grey[300] }}
                >
                  {item.id < step ? <Icon name="tick-done"></Icon> : item.id + 1}
                </Typography>
                <Typography className="text-base normal-case font-medium">{item.name}</Typography>
              </Box>
            }
          />
        ))}
      </Tabs>
      <TabPanel value={step} index={NOTIFICATION_CREATE_TABS.GENERAL}>
        <GeneralTabComponent
          step={step}
          data={data}
          type={type}
          priority={priority}
          functionLocation={functionLocation}
          equipment={equipment}
          workCenter={workCenter}
          mainPlant={mainPlant}
          assigner={assigner}
          isOnlineAssign={isOnlineAssign}
          isSubmitted={isSubmitted}
          setIsSubmitted={setIsSubmitted}
          changeAssignType={changeAssignType}
          handleGetPriority={handleGetPriority}
          handleGetAssigner={handleGetAssigner}
          handleGetFunctionLocation={handleGetFunctionLocation}
          handleGetWorkCenter={handleGetWorkCenter}
          handleGetEquipment={handleGetEquipment}
          setStep={setStep}
          setData={setData}
          loading={loadingGeneralTab}
          setPriority={setPriority}
          setAssigner={setAssigner}
        />
      </TabPanel>
      <TabPanel value={step} index={NOTIFICATION_CREATE_TABS.ADD_NOTIFICATION}>
        <AddNotificationComponent
          step={step}
          setStep={setStep}
          data={data}
          setData={setData}
          isSubmitted={isSubmitted}
          setIsSubmitted={setIsSubmitted}
          objectPart={objectPart}
          problem={problem}
          cause={cause}
          handleGetObjectPartCode={handleGetObjectPartCode}
          handleGetProblemCode={handleGetProblemCode}
          handleGetCauseCode={handleGetCauseCode}
          loading={loadingAddNotificationTab}
        />
      </TabPanel>
      <TabPanel value={step} index={NOTIFICATION_CREATE_TABS.UPLOAD}>
        <UploadTabComponent
          documentGroup={documentGroup}
          onUploadDocument={handleUploadDocument}
          onDeleteDocument={handleDeleteDocument}
          loadingUpload={loadingUpload}
        />
      </TabPanel>
    </Box>
  );
};

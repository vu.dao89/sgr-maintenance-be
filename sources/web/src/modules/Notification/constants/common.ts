export enum PriorityType {
  VERY_IMPORTANT = 1,
  IMPORTTANT = 2,
  MEDIUM = 3,
  LOW = 4
}

export const PriorityLabel: { [key: number | string]: string } = {
  [PriorityType.VERY_IMPORTANT]: 'Rất quan trọng',
  [PriorityType.IMPORTTANT]: 'Quan trọng',
  [PriorityType.MEDIUM]: 'Trung bình',
  [PriorityType.LOW]: 'Thấp'
};

export const PriorityColor: { [key: number | string]: string } = {
  [PriorityType.VERY_IMPORTANT]: 'FF6600',
  [PriorityType.IMPORTTANT]: 'FFCC00',
  [PriorityType.MEDIUM]: 'FFFF00',
  [PriorityType.LOW]: 'CCFF00'
};

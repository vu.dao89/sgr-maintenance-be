export enum NOTIFICATION_TAB_CONTENT {
  PREVIEW = 0,
  DETAIL = 1,
  ACTION = 2,
  DOCUMENT = 3
}

export const NOTIFICATION_TAB_DATA: { [key: string]: string } = {
  [NOTIFICATION_TAB_CONTENT.PREVIEW]: 'Tổng quan',
  [NOTIFICATION_TAB_CONTENT.DETAIL]: 'Chi tiết',
  [NOTIFICATION_TAB_CONTENT.ACTION]: 'Hành động',
  [NOTIFICATION_TAB_CONTENT.DOCUMENT]: 'Tài liệu'
};

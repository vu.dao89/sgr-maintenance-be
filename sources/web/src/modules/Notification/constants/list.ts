export enum NOTIFICATION_FILTER_FIELD {
  PLANTS = 'plants',
  CREATE_FROM = 'createFrom',
  CREATE_TO = 'createTo',
  STATUS = 'status',
  REPORT_BY = 'reportBy',
  ASSIGN_TO = 'assignTo',
  PRIORITY = 'priorities',
  TYPE = 'types',
  PLANNER_GROUP = 'plannerGroups',
  DESCRIPTION = 'description'
}

export const NOTIFICATION_FILTER_FIELD_LABEL: { [key: string]: string } = {
  [NOTIFICATION_FILTER_FIELD.PLANTS]: 'Nơi bảo trì',
  [NOTIFICATION_FILTER_FIELD.CREATE_FROM]: 'Ngày tạo',
  [NOTIFICATION_FILTER_FIELD.CREATE_TO]: 'Kết thúc',
  [NOTIFICATION_FILTER_FIELD.STATUS]: 'Trạng thái',
  [NOTIFICATION_FILTER_FIELD.REPORT_BY]: 'Người tạo',
  [NOTIFICATION_FILTER_FIELD.ASSIGN_TO]: 'Người nhận',
  [NOTIFICATION_FILTER_FIELD.PRIORITY]: 'Mức độ ưu tiên',
  [NOTIFICATION_FILTER_FIELD.TYPE]: 'Loại thông báo',
  [NOTIFICATION_FILTER_FIELD.PLANNER_GROUP]: 'Bộ phận kế hoạch'
};

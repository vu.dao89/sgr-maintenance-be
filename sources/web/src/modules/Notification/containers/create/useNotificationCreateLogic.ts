import { getFunctionLocations, getWorkCenters } from '@modules/MasterData/services';
import { getCauseCodes, getCauses } from '@modules/MasterData/services/cause';
import { getEquipments } from '@modules/MasterData/services/equipment';
import { deleteGlobalDocument, uploadGlobalDocument } from '@modules/MasterData/services/globalDocument';
import { getMainPlants } from '@modules/MasterData/services/mainPlant';
import { getTypes } from '@modules/MasterData/services/notificationType';
import { getObjectPartCodes, getObjectParts } from '@modules/MasterData/services/objectPart';
import { getPriorities } from '@modules/MasterData/services/priority';
import { getProblemCodes, getProblems } from '@modules/MasterData/services/problem';
import { getAssignTo } from '@modules/MasterData/services/user';
import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { createNotification } from '@modules/Notification/services/create';
import { ICreateNotificationReq, INotificationGeneralForm } from '@modules/Notification/types/create';
import { convertStringToDateTime, groupFileDocument } from 'common/helpers';
import { useApiCaller } from 'common/hooks';
import { cloneDeep, uniqBy } from 'lodash';
import { useSelectUserStore } from 'modules/User';
import { useEffect, useMemo, useState } from 'react';

const useNotificationCreateLogic = () => {
  const { user } = useSelectUserStore();
  const [isSubmitted, setIsSubmitted] = useState<boolean>(false);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [step, setStep] = useState<NOTIFICATION_CREATE_TABS>(NOTIFICATION_CREATE_TABS.GENERAL);
  const [data, setData] = useState<ICreateNotificationReq>({} as ICreateNotificationReq);
  const [type, setType] = useState<any[]>([]);
  const [mainPlant, setMainPlant] = useState<IOption[]>([]);
  const [workCenter, setWorkCenter] = useState<IOption[]>([]);
  const [functionLocation, setFunctionLocation] = useState<IOption[]>([]);
  const [equipment, setEquipment] = useState<IOption[]>([]);
  const [assigner, setAssigner] = useState<IOption[]>([]);
  const [priority, setPriority] = useState<IOption[]>([]);
  const [isOnlineAssign, setIsOnlineAssign] = useState<boolean>(false);
  const [objectPart, setObjectPart] = useState<IOption[]>([]);
  const [problem, setProblem] = useState<IOption[]>([]);
  const [cause, setCause] = useState<IOption[]>([]);
  const [documentGroup, setDocumentGroup] = useState<any>({});

  const { request, loading } = useApiCaller<any>({
    apiCaller: createNotification,
    messageFail: false,
    messageSuccess: 'Tạo thông báo thành công'
  });

  const { request: requestType, loading: loadingType } = useApiCaller<IOption[]>({
    apiCaller: getTypes
  });

  const { request: requestMainPlant, loading: loadingPlant } = useApiCaller<IOption[]>({
    apiCaller: getMainPlants
  });

  const { request: requestFunctionLocation, loading: loadingFunctionLocation } = useApiCaller<IOption[]>({
    apiCaller: getFunctionLocations
  });

  const { request: requestEquipment, loading: loadingEquipment } = useApiCaller<IOption[]>({
    apiCaller: getEquipments
  });

  const { request: requestWorkCenter, loading: loadingWorkCenter } = useApiCaller<IOption[]>({
    apiCaller: getWorkCenters
  });

  const { request: requestAssigner, loading: loadingAssigner } = useApiCaller<IOption[]>({
    apiCaller: getAssignTo
  });

  const { request: requestPriority, loading: loadingPriority } = useApiCaller<IOption[]>({
    apiCaller: getPriorities
  });

  const { request: requestObjectPart, loading: loadingObjectPart } = useApiCaller<IOption[]>({
    apiCaller: getObjectParts
  });

  const { request: requestObjectPartCode } = useApiCaller<IOption[]>({
    apiCaller: getObjectPartCodes
  });

  const { request: requestProblem, loading: loadingProblem } = useApiCaller<IOption[]>({
    apiCaller: getProblems
  });

  const { request: requestProblemCode } = useApiCaller<IOption[]>({
    apiCaller: getProblemCodes
  });

  const { request: requestCause, loading: loadingCause } = useApiCaller<IOption[]>({
    apiCaller: getCauses
  });

  const { request: requestCauseCode } = useApiCaller<IOption[]>({
    apiCaller: getCauseCodes
  });

  const { request: requestUploadDocument, loading: loadingUpload } = useApiCaller<any[]>({
    apiCaller: uploadGlobalDocument
  });

  const { request: requestDeleteDocument } = useApiCaller<any[]>({
    apiCaller: deleteGlobalDocument
  });

  const handleCreate = async (formData: INotificationGeneralForm) => {
    await request(formData);
  };

  const handleGetType = async () => {
    const result: any = await requestType();

    if (result?.data?.items) {
      setType(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.notiType, ...item })));
    }
  };

  const handleGetPriority = async (params: any) => {
    const result: any = await requestPriority(params);

    if (result?.data?.items) {
      setPriority(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.priority })));
    }
  };

  const handleGetMainPlant = async () => {
    const result: any = await requestMainPlant({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      setMainPlant(result?.data?.items?.map((item: any) => ({ label: item?.plantName, value: item?.code })));
    }
  };

  const handleGetAssigner = async (params: any) => {
    const result: any = await requestAssigner({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      setAssigner(result?.data?.items?.map((item: any) => ({ label: item?.name || item?.id, value: item?.code })));
    }
  };

  const handleGetWorkCenter = async (params?: any) => {
    const result: any = await requestWorkCenter({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      setWorkCenter(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.code })));
    }
  };

  const handleGetEquipment = async (params?: any) => {
    const result: any = await requestEquipment({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      setEquipment(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.equipmentId })));
    }
  };

  const handleGetFunctionLocation = async (params?: any) => {
    const result: any = await requestFunctionLocation(params);

    if (result?.data?.items) {
      setFunctionLocation(
        result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.functionalLocationId }))
      );
    }
  };

  const handleGetObjectPart = async (params: any) => {
    const result: any = await requestObjectPart(params);

    if (result?.data?.items) {
      const objectParts = uniqBy(
        result?.data?.items?.map((item: any) => ({ label: item?.codeGroupDesc, value: item?.codeGroup } as IOption)),
        (item: IOption) => item.value
      );
      setObjectPart(objectParts);
    }
  };

  const handleGetObjectPartCode = async (params: any) => {
    const result: any = await requestObjectPartCode({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      return result?.data?.items?.map((item: any) => ({ label: item?.codeDesc, value: item?.code }));
    }

    return [];
  };

  const handleGetProblem = async (params: any) => {
    const result: any = await requestProblem(params);

    if (result?.data?.items) {
      const problems = uniqBy(
        result?.data?.items?.map((item: any) => ({ label: item?.codeGroupDesc, value: item?.codeGroup } as IOption)),
        (item: IOption) => item.value
      );

      setProblem(problems);
    }
  };

  const handleGetProblemCode = async (params: any) => {
    const result: any = await requestProblemCode({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      return result?.data?.items?.map((item: any) => ({ label: item?.codeDesc, value: item?.code }));
    }

    return [];
  };

  const handleGetCause = async (params: any) => {
    const result: any = await requestCause(params);

    if (result?.data?.items) {
      const causes = uniqBy(
        result?.data?.items?.map((item: any) => ({ label: item?.codeGroupDesc, value: item?.codeGroup } as IOption)),
        (item: IOption) => item.value
      );
      setCause(causes);
    }
  };

  const handleGetCauseCode = async (params: any) => {
    const result: any = await requestCauseCode({ ...params, maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result?.data?.items) {
      return result?.data?.items?.map((item: any) => ({ label: item?.codeDesc, value: item?.code }));
    }

    return [];
  };

  const handleUploadDocument = async (files: any) => {
    if (!files.length) return;

    const formData = new FormData() as FormData;

    files.forEach((file: any) => {
      formData.append('files', file);
    });

    const response = await requestUploadDocument(formData);

    if (response.statusCode === 200) {
      const groupDocuments = groupFileDocument(response.data);
      groupDocuments.files = [...(groupDocuments.files || [])];
      groupDocuments.images = [...(groupDocuments.images || [])];

      setDocumentGroup(groupDocuments);
    }
  };

  const handleDeleteDocument = async (id: string) => {
    const result = await requestDeleteDocument(id);

    if (result.statusCode === 200) {
      const files = documentGroup?.files?.filter((item: any) => item.id !== id);
      const images = documentGroup?.images?.map((item: any) => item.id !== id);

      setDocumentGroup({
        files,
        images
      });
    }
  };

  const handleCreateNotification = async (formData: ICreateNotificationReq) => {
    const data: ICreateNotificationReq = cloneDeep<ICreateNotificationReq>(formData);
    if (formData?.header?.date) {
      data.header.date = convertStringToDateTime(data?.header?.date, 'DDMMYYYY');
      data.header.time = convertStringToDateTime(data?.header?.date, 'hhmmss');
    }

    if (formData?.header?.requestStart) {
      data.header.requestStart = convertStringToDateTime(data?.header?.date, 'DDMMYYYY');
      data.header.requestStartTime = convertStringToDateTime(data?.header?.date, 'hhmmss');
    }

    if (formData?.header?.requestEnd) {
      data.header.requestEnd = convertStringToDateTime(data?.header?.date, 'DDMMYYYY');
      data.header.requestEndTime = convertStringToDateTime(data?.header?.date, 'hhmmss');
    }

    if (formData?.header?.malfunctionStart) {
      data.header.malfunctionStart = convertStringToDateTime(data?.header?.date, 'DDMMYYYY');
      data.header.malfunctionStartTime = convertStringToDateTime(data?.header?.date, 'hhmmss');
    }

    if (formData?.header?.malfunctionEnd) {
      data.header.malfunctionEnd = convertStringToDateTime(data?.header?.date, 'DDMMYYYY');
      data.header.malfunctionEndTime = convertStringToDateTime(data?.header?.date, 'hhmmss');
    }

    if (documentGroup) {
      const documentIds: any[] = [];
      documentGroup?.files?.map((item: any) => {
        documentIds.push(item.id);
      });

      documentGroup?.images?.map((item: any) => {
        documentIds.push(item.id);
      });

      if (documentIds.length) data.documentIds = documentIds;
    }

    return await request(data);
  };

  useEffect(() => {
    handleGetType();
    handleGetWorkCenter();
    handleGetFunctionLocation({ maintenancePlantCodes: user?.maintenancePlantCodes });
    handleGetEquipment();
    handleGetMainPlant();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!data?.header?.type && !type) {
      return;
    }

    const selectedType = type?.filter(item => item?.notiType === data?.header?.type);

    if (selectedType && !selectedType.length) {
      return;
    }

    handleGetPriority({
      types: selectedType[0].type
    });

    handleGetObjectPart({
      catalogProfiles: selectedType[0]?.catalogProfile,
      catalogs: selectedType[0]?.objectParts
    });

    handleGetProblem({
      catalogProfiles: selectedType[0]?.catalogProfile,
      catalogs: selectedType[0]?.problems
    });

    handleGetCause({
      catalogProfiles: selectedType[0]?.catalogProfile,
      catalogs: selectedType[0]?.causes
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.header?.type]);

  const loadingGeneralTab = useMemo(() => {
    return (
      loadingType ||
      loadingAssigner ||
      loadingEquipment ||
      loadingWorkCenter ||
      loadingFunctionLocation ||
      loadingPlant ||
      loadingPriority
    );
  }, [
    loadingType,
    loadingAssigner,
    loadingEquipment,
    loadingWorkCenter,
    loadingFunctionLocation,
    loadingPlant,
    loadingPriority
  ]);

  const loadingAddNotificationTab = useMemo(() => {
    return loadingObjectPart || loadingProblem || loadingCause;
  }, [, loadingObjectPart, loadingCause, loadingProblem]);

  return {
    loading,
    data,
    setData,
    step,
    setStep,
    type,
    priority,
    functionLocation,
    equipment,
    workCenter,
    mainPlant,
    assigner,
    isOnlineAssign,
    setIsOnlineAssign,
    isSubmitted,
    setIsSubmitted,
    isOpen,
    setIsOpen,
    objectPart,
    handleGetObjectPartCode,
    problem,
    handleGetProblemCode,
    cause,
    handleGetCauseCode,
    handleCreate,
    handleGetPriority,
    setPriority,
    handleGetAssigner,
    setAssigner,
    handleGetFunctionLocation,
    handleGetWorkCenter,
    handleGetEquipment,
    loadingUpload,
    handleUploadDocument,
    handleDeleteDocument,
    loadingGeneralTab,
    loadingAddNotificationTab,
    handleCreateNotification,
    documentGroup
  };
};

export default useNotificationCreateLogic;

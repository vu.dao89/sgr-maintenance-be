import dayjs from 'dayjs';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  type: Yup.string().required('Vui lòng nhập phân loại thông báo'),
  desc: Yup.string().required('Vui lòng nhập tiêu đề thông báo').max(40, 'Tiều đề thông báo phải nhỏ hơn 40 kí tự'),
  lText: Yup.string().max(5000, 'Mô tả thông báo phải nhỏ hơn 5000 kí tự'),
  maintenancePlant: Yup.string().required('Vui lòng nhập nơi bảo trì'),
  workCenter: Yup.string().required('Vui lòng nhập tổ đội thực hiện'),
  functionalLocationId: Yup.string().required('Vui lòng nhập khu vực chức năng'),
  equipmentId: Yup.string().required('Vui lòng nhập mã thiết bị'),
  priority: Yup.string().required('Vui lòng nhập độ ưu tiên'),
  date: Yup.string().required('Vui lòng nhập ngày phát hiện sự cố'),
  assignTo: Yup.string().required('Vui lòng nhập người nhận xử lí'),
  requestStart: Yup.string()
    .required('Vui lòng nhập yêu cầu xử lí từ ngày')
    .when(['date'], {
      is: (date: string) => date,
      then: Yup.string().test(
        'is-valid-date',
        'Ngày yêu xử lí sự cố phải lớn hơn ngày phát hiện sự cố',
        (value, ctx) => {
          const date = ctx.parent.date;
          if (!value || !date) {
            return true;
          }

          return dayjs(date) <= dayjs(value);
        }
      )
    }),
  requestEnd: Yup.string()
    .required('Vui lòng nhập yêu cầu xử lí đến ngày')
    .when(['requestStart'], {
      is: (requestStart: string) => requestStart,
      then: Yup.string().test(
        'is-valid-date',
        'Ngày yêu cầu xử lí kết thúc phải lớn hơn ngày bắt đầu',
        (value, ctx) => {
          const requestStart = ctx.parent.requestStart;
          if (!value || !requestStart) {
            return true;
          }

          return dayjs(requestStart) <= dayjs(value);
        }
      )
    }),
  malfunctionStart: Yup.string().required('Vui lòng nhập yêu cầu xử lí từ ngày')
});

export const validateNotificationItemSchema = Yup.object().shape({
  notificationItems: Yup.array(
    Yup.object().shape({
      part: Yup.string().required('Vui lòng chọn giá trị'),
      partCode: Yup.string().required('Vui lòng chọn giá trị'),
      problem: Yup.string().when(['part'], {
        is: (part: string) => isEmpty(part),
        then: Yup.string().test('is-valid-problem', 'Vui lòng chọn giá trị', (value, ctx) => {
          return isEmpty(value) || (!isEmpty(ctx.parent.part) && !isEmpty(value));
        })
      }),
      dame: Yup.string().when(['problem'], {
        is: (problem: string) => !isEmpty(problem),
        then: Yup.string().test('is-valid-dame', 'Vui lòng chọn giá trị', (value, ctx) => {
          return !isEmpty(ctx.parent.problem) && !isEmpty(value);
        })
      }),
      cause: Yup.string().when(['problem', 'part'], {
        is: (problem: string, part: string) => isEmpty(problem) || isEmpty(part),
        then: Yup.string().test('is-valid-cause', 'Vui lòng chọn giá trị', (value, ctx) => {
          return isEmpty(value) && (isEmpty(ctx.parent.problem) || isEmpty(ctx.parent.part));
        })
      })
    })
  )
});

import { Grid } from '@mui/material';
import { Loading } from 'common/components';
import useNotificationContext from 'modules/Notification/stores/useNotificationContext';
import { LeftCardComponent, RightTabComponent } from '../../components';

export default function NotificationDetailContainer() {
  const { loading } = useNotificationContext();

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <Grid container columnSpacing={5}>
          <Grid item xs={12} lg={5} xl={4} className="xl:flex-[0_0_500px]">
            <LeftCardComponent />
          </Grid>
          <Grid item xs={12} lg={7} xl={8} sx={{ maxWidth: { xl: '100%' } }}>
            <RightTabComponent />
          </Grid>
        </Grid>
      )}
    </>
  );
}

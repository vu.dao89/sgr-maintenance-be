import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
    part: Yup.string().required('Vui lòng chọn giá trị'),
    partCode: Yup.string().required('Vui lòng chọn giá trị'),
    dame: Yup.string().when(['problem', 'cause'], {
        is: (problem: string, cause: string) => problem || cause,
        then: Yup.string().required('Vui lòng chọn giá trị'),
    }),
    problem: Yup.string().when(['cause'], {
        is: (cause: string) => cause,
        then: Yup.string().required('Vui lòng chọn giá trị'),
    }),
});

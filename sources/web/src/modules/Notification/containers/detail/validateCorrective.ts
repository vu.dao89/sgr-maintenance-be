import * as Yup from 'yup';

export const validationCorrectiveSchema = Yup.object().shape({
  correctiveAction: Yup.array(
    Yup.object().shape({
      activityGroup: Yup.string().required('Vui lòng chọn giá trị'),
      activityCode: Yup.string().required('Vui lòng chọn giá trị'),
      notificationLongText: Yup.string().required('Vui lòng nhập')
    })
  )
});
``;

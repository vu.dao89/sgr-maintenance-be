import { FilterNotification, NotificationList } from '@modules/Notification/components';
import { NotificationCreateForm } from '@modules/Notification/components/list/modalCreate';
import { NOTIFICATION_FILTER_FIELD_LABEL } from '@modules/Notification/constants';
import { NOTIFICATION_CREATE_TABS } from '@modules/Notification/constants/create';
import { Box, Stack } from '@mui/material';
import { FilterList, Loading, Modal, SearchInput } from 'common/components';
import { handleRedirect } from 'common/helpers';
import { size } from 'lodash';
import useNotificationCreateLogic from '../create/useNotificationCreateLogic';
import useNotificationListLogic from './useNotificationListLogic';

const NotificationListContainer = () => {
  const {
    items,
    filter,
    filterCount,
    pagination,
    loading,
    columns,
    mainPlant,
    priority,
    type,
    plannerGroup,
    status,
    reportBy,
    assignTo,
    selectedFilter,
    handleChangePaging,
    removeFilter,
    handleFilter,
    setColumns
  } = useNotificationListLogic();

  const {
    data,
    setData,
    step,
    setStep,
    type: createType,
    priority: createPriority,
    assigner,
    functionLocation,
    equipment,
    workCenter,
    isOnlineAssign,
    setIsOnlineAssign,
    isSubmitted,
    setIsSubmitted,
    isOpen,
    setIsOpen,
    objectPart,
    problem,
    cause,
    handleGetObjectPartCode,
    handleGetProblemCode,
    handleGetCauseCode,
    handleGetPriority,
    setPriority,
    handleGetAssigner,
    handleGetFunctionLocation,
    handleGetWorkCenter,
    handleGetEquipment,
    setAssigner,
    loadingUpload,
    handleUploadDocument,
    handleDeleteDocument,
    loadingGeneralTab,
    loadingAddNotificationTab,
    handleCreateNotification,
    documentGroup
  } = useNotificationCreateLogic();

  const onChangeStep = async (type: 'next' | 'prev' = 'next') => {
    if (type === 'next') {
      if (step === NOTIFICATION_CREATE_TABS.UPLOAD) {
        const result = await handleCreateNotification(data);
        if (result.statusCode === 200) {
          setIsOpen(false);
          handleRedirect();
        }

        return;
      }

      setIsSubmitted(true);
    } else {
      return step > 0 ? setStep(step - 1) : setIsOpen(false);
    }
  };

  return (
    <Stack>
      <Stack>
        <Box display="flex" mb={5}>
          <SearchInput name="notification" onSearch={(description: any) => handleFilter({ ...filter, description })} />
          <FilterNotification
            filter={filter}
            filterCount={filterCount}
            status={status}
            plannerGroup={plannerGroup}
            mainPlant={mainPlant}
            type={type}
            priority={priority}
            reportBy={reportBy}
            assignTo={assignTo}
            onFilter={handleFilter}
            onGetPriority={handleGetPriority}
          />
        </Box>
        {size(selectedFilter) > 0 && (
          <FilterList data={selectedFilter} label={NOTIFICATION_FILTER_FIELD_LABEL} onRemove={removeFilter} />
        )}
      </Stack>
      {loading ? (
        <Loading />
      ) : (
        <>
          <NotificationList
            items={items}
            pagination={pagination}
            handleChangePaging={handleChangePaging}
            columns={columns}
            setColumns={setColumns}
            setIsOpenCreateModel={setIsOpen}
          />

          {isOpen && (
            <Modal
              id="create-notification"
              title="Tạo thông báo"
              maxWidth="lg"
              keepMounted
              open={isOpen}
              onSubmit={() => onChangeStep('next')}
              onReset={() => onChangeStep('prev')}
              btnResetText={step === 0 ? 'Huỷ bỏ' : 'Quay lại'}
              btnSubmitText={step === 2 ? 'Tạo' : 'Tiếp theo'}
              PaperProps={{
                sx: {
                  maxWidth: 1165,
                  mx: 'auto',
                  '& .MuiDialogContent-root': {
                    backgroundColor: '#F2F3F5'
                  }
                }
              }}
            >
              <NotificationCreateForm
                step={step}
                data={data}
                type={createType}
                priority={createPriority}
                functionLocation={functionLocation}
                equipment={equipment}
                workCenter={workCenter}
                mainPlant={mainPlant}
                assigner={assigner}
                isOnlineAssign={isOnlineAssign}
                isSubmitted={isSubmitted}
                setIsSubmitted={setIsSubmitted}
                changeAssignType={setIsOnlineAssign}
                setStep={setStep}
                setData={setData}
                objectPart={objectPart}
                problem={problem}
                cause={cause}
                handleGetObjectPartCode={handleGetObjectPartCode}
                handleGetProblemCode={handleGetProblemCode}
                handleGetCauseCode={handleGetCauseCode}
                handleGetPriority={handleGetPriority}
                handleGetFunctionLocation={handleGetFunctionLocation}
                handleGetWorkCenter={handleGetWorkCenter}
                handleGetEquipment={handleGetEquipment}
                setPriority={setPriority}
                loadingGeneralTab={loadingGeneralTab}
                loadingUpload={loadingUpload}
                handleUploadDocument={handleUploadDocument}
                handleDeleteDocument={handleDeleteDocument}
                loadingAddNotificationTab={loadingAddNotificationTab}
                handleCreateNotification={handleCreateNotification}
                setAssigner={setAssigner}
                handleGetAssigner={handleGetAssigner}
                documentGroup={documentGroup}
              />
            </Modal>
          )}
        </>
      )}
    </Stack>
  );
};

export default NotificationListContainer;

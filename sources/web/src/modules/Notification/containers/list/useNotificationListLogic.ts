import { getPlannerGroups } from '@modules/MasterData/services';
import { getStatus } from '@modules/MasterData/services/commonStatus';
import { getMainPlants } from '@modules/MasterData/services/mainPlant';
import { getTypes } from '@modules/MasterData/services/notificationType';
import { getPriorities } from '@modules/MasterData/services/priority';
import { getAssignTo, getReportBy } from '@modules/MasterData/services/user';
import { NOTIFICATION_FILTER_FIELD, PriorityColor, PriorityLabel } from '@modules/Notification/constants';
import { deleteNotificationById, getNotifications } from '@modules/Notification/services';
import {
  IFormFilterNotification,
  INotificationFilterReqDto,
  INotificationItem,
  INotificationResDto
} from '@modules/Notification/types';
import { useSelectUserStore } from '@modules/User';
import { IColumnType } from 'common/components/DataTable';
import { DEFAULT_PER_PAGE } from 'common/constants';
import { toDateString } from 'common/helpers';
import { useApiCaller } from 'common/hooks';
import dayjs from 'dayjs';
import { cloneDeep, isUndefined } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const initColumns: IColumnType<INotificationItem>[] = [
  {
    key: 'description',
    title: 'Tiêu đề',
    enable: true,
    width: 200,
    required: true
  },
  {
    key: 'equipmentDescription',
    title: 'Tên thiết bị',
    enable: true,
    width: 200,
    required: true
  },
  {
    key: 'functionalLocationDesc',
    title: 'Khu vực',
    width: 200,
    enable: true,
    required: true
  },
  {
    key: 'reportedByName',
    title: 'Người tạo',
    width: 150,
    enable: true,
    type: (_, { reportedByName }) =>
      reportedByName ? { type: 'image', value: '/iss/assetmanagement/static/images/no-image.jpg' } : { type: 'label' }
  },

  {
    key: 'assignName',
    title: 'Người nhận',
    width: 150,
    enable: true,
    type: (_, { assignName }) =>
      assignName ? { type: 'image', value: '/iss/assetmanagement/static/images/no-image.jpg' } : { type: 'label' }
  },

  {
    key: 'notificationDate',
    title: 'Ngày tạo thông báo',
    width: 220,
    enable: true,
    render: (_, { notificationDate }) => (notificationDate ? toDateString(notificationDate) : '')
  },
  {
    key: 'priority',
    title: 'Mức độ ưu iên',
    width: 130,
    enable: true,
    type: (_, { priority }) => (priority ? { type: 'label', color: PriorityColor[priority] } : { type: 'text' }),
    render: (_, { priority }) => (priority ? PriorityLabel[priority] : '')
  },
  {
    key: 'commonStatus',
    title: 'Trạng thái',
    width: 200,
    enable: true,
    type: (_, { commonStatus }) =>
      commonStatus ? { type: 'label', color: commonStatus?.colorCode } : { type: 'text' },
    render: (_, { commonStatus }) => (commonStatus ? commonStatus?.description : '')
  }
];

const initialFilter: INotificationFilterReqDto = {
  page: 1,
  size: DEFAULT_PER_PAGE
};

const initPagination: IPagination = {
  page: 1,
  size: DEFAULT_PER_PAGE,
  totalPages: 0,
  totalItems: 0
};

const useNotificationListLogic = () => {
  const { user } = useSelectUserStore();
  const router = useRouter();
  const [items, setItems] = useState<INotificationItem[]>([]);
  const [filter, setFilter] = useState<INotificationFilterReqDto>(initialFilter);
  const [filterCount, setFilterCount] = useState<number>(0);
  const [pagination, setPagination] = useState<IPagination>(initPagination);
  const [columns, setColumns] = useState<IColumnType<INotificationItem>[]>(initColumns);
  const [mainPlant, setMainPlant] = useState<any[]>([]);
  const [status, setStatus] = useState<any[]>([]);
  const [reportBy, setReportBy] = useState<any[]>([]);
  const [assignTo, setAssignTo] = useState<any[]>([]);
  const [priority, setPriority] = useState<any[]>([]);
  const [type, setType] = useState<any[]>([]);
  const [plannerGroup, setPlannerGroup] = useState<any[]>([]);
  const [selectedFilter, setSelectedFilter] = useState<IOption[]>([]);

  const { request, loading } = useApiCaller<INotificationResDto>({
    apiCaller: getNotifications,
    resDto: {} as INotificationItem
  });
  const { request: requestDelete } = useApiCaller({
    apiCaller: deleteNotificationById
  });
  const { request: requestPriority } = useApiCaller<IOption[]>({
    apiCaller: getPriorities
  });
  const { request: requestMainPlant } = useApiCaller<IOption[]>({
    apiCaller: getMainPlants
  });
  const { request: requestType } = useApiCaller<IOption[]>({
    apiCaller: getTypes
  });
  const { request: requestPlannerGroup } = useApiCaller<IOption[]>({
    apiCaller: getPlannerGroups
  });
  const { request: requestStatus } = useApiCaller<IOption[]>({
    apiCaller: getStatus
  });

  const { request: requestReportBy } = useApiCaller<IOption[]>({
    apiCaller: getReportBy
  });

  const { request: requestAssignTo } = useApiCaller<IOption[]>({
    apiCaller: getAssignTo
  });

  const handleGetData = async (params: any) => {
    const result: any = await request(params);

    if (result) {
      setItems(result?.data?.items);
      setPagination(result?.data?.pagination);
    }
  };

  const handleDelete = async (id: string) => {
    const result = await requestDelete(id);

    if (result?.statusCode === 200) {
      router.push('/notification');
    }
  };

  const handleGetPriority = async (types: string) => {
    if (!types) {
      return;
    }

    const result: any = await requestPriority({ types });

    if (result) {
      setPriority(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.priority })));
    }
  };

  const handleGetPlannerGroup = async () => {
    const result: any = await requestPlannerGroup({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setPlannerGroup(result?.data?.items?.map((item: any) => ({ label: item?.name, value: item?.plannerGroupId })));
    }
  };

  const handleGetMainPlant = async () => {
    const result: any = await requestMainPlant({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setMainPlant(result?.data?.items?.map((item: any) => ({ label: item?.plantName, value: item?.code })));
    }
  };

  const handleGetType = async () => {
    const result: any = await requestType({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setType(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.notiType, ...item })));
    }
  };

  const handleGetStatus = async () => {
    const result: any = await requestStatus();

    if (result) {
      setStatus(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.status })));
    }
  };

  const handleGetReportBy = async () => {
    const result: any = await requestReportBy({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setReportBy(result?.data?.items?.map((item: any) => ({ label: item?.name || item?.code, value: item?.code })));
    }
  };

  const handleGetAssignTo = async () => {
    const result: any = await requestAssignTo({ maintenancePlantCodes: user?.maintenancePlantCodes });

    if (result) {
      setAssignTo(
        result?.data?.items?.map((item: any) => ({ label: item?.name || item?.code, value: item?.code || item?.id }))
      );
    }
  };

  useEffect(() => {
    if (!router.isReady) {
      return;
    }
    const data = router?.query as any;

    data.page = Number(data.page) || initialFilter.page;
    data.size = Number(data.size) || initialFilter.size;
    data.plants = user?.maintenancePlantCodes;

    handleGetData(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  useEffect(() => {
    const selected: any = {};

    if (filter?.createFrom) {
      selected[NOTIFICATION_FILTER_FIELD.CREATE_FROM] = toDateString(filter?.createFrom);
    }

    if (filter?.createTo) {
      selected[NOTIFICATION_FILTER_FIELD.CREATE_TO] = toDateString(filter?.createTo);
    }

    if (filter?.reportBy) {
      selected[NOTIFICATION_FILTER_FIELD.REPORT_BY] = reportBy.filter(item => item.value === filter?.reportBy)[0];
    }

    if (filter?.assignTo) {
      selected[NOTIFICATION_FILTER_FIELD.ASSIGN_TO] = assignTo.filter(item => item.value === filter?.assignTo)[0];
    }

    if (filter?.plants) {
      selected[NOTIFICATION_FILTER_FIELD.PLANTS] = mainPlant.filter(item => item.value === filter?.plants)[0];
    }

    if (filter?.priorities) {
      selected[NOTIFICATION_FILTER_FIELD.PRIORITY] = priority.filter(item => item.value === filter?.priorities)[0];
    }

    if (filter?.types) {
      selected[NOTIFICATION_FILTER_FIELD.TYPE] = type.filter(item => item.value === filter?.types)[0];
    }

    if (filter?.plannerGroups) {
      selected[NOTIFICATION_FILTER_FIELD.PLANNER_GROUP] = plannerGroup.filter(
        item => item.value === filter?.plannerGroups
      )[0];
    }

    if (filter?.status) {
      selected['status'] = status.filter(item => item.value === filter?.status)[0];
    }

    setSelectedFilter(selected);

    setFilterCount(Object.keys(selected).length);

    handleGetData({ ...filter, plants: user?.maintenancePlantCodes });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  useEffect(() => {
    // handleGetPriority();
    handleGetMainPlant();
    handleGetType();
    handleGetPlannerGroup();
    handleGetStatus();
    handleGetReportBy();
    handleGetAssignTo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    handleGetPriority(filter?.types || '');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter?.types]);

  const handleChangePaging = (_event: any, page: number) => {
    setFilter({ ...filter, page });
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  };

  const handleFilter = (value: IFormFilterNotification) => {
    const dataFilter = {} as INotificationFilterReqDto;

    if (value.description && !isUndefined(value.description)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.DESCRIPTION] = value.description;
    }

    if (value.createFrom && !isUndefined(value.createFrom)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.CREATE_FROM] = dayjs(value.createFrom).format('YYYYMMDD');
    }

    if (value.createTo && !isUndefined(value.createTo)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.CREATE_TO] = dayjs(value.createTo).format('YYYYMMDD');
    }

    if (value.plannerGroups && !isUndefined(value.plannerGroups)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.PLANNER_GROUP] = value.plannerGroups;
    }

    if (value.plants && !isUndefined(value.plants)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.PLANTS] = value.plants;
    }

    if (value.priorities && !isUndefined(value.priorities)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.PRIORITY] = value.priorities;
    }

    if (value.types && !isUndefined(value.types)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.TYPE] = value.types;
    }

    if (value.status && !isUndefined(value.status)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.STATUS] = value.status;
    }

    if (value.reportBy && !isUndefined(value.reportBy)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.REPORT_BY] = value.reportBy;
    }

    if (value.assignTo && !isUndefined(value.assignTo)) {
      dataFilter[NOTIFICATION_FILTER_FIELD.ASSIGN_TO] = value.assignTo;
    }

    setFilter({ ...initialFilter, ...dataFilter });
  };

  const removeFilter = (filterKey: NOTIFICATION_FILTER_FIELD) => {
    const newFilter = cloneDeep(filter);
    if (!isUndefined(newFilter)) {
      delete newFilter[filterKey];
      setFilter({ ...newFilter });
    }
  };

  return {
    loading,
    pagination,
    items: items ?? [],
    filter,
    filterCount,
    columns,
    mainPlant,
    priority,
    type,
    plannerGroup,
    status,
    reportBy,
    assignTo,
    selectedFilter,
    handleChangePaging,
    handleGetPriority,
    handleGetData,
    removeFilter,
    handleFilter,
    setColumns,
    handleDelete
  };
};

export default useNotificationListLogic;

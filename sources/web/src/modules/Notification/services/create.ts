import { apiService } from 'common/app';
import { ICreateNotificationReq, INotificationItemReq } from '../types/create';

export const createNotification = (data: ICreateNotificationReq) => {
  return apiService.post('/notification/post/notification-post', data);
};

export const createNotificationItem = (data: INotificationItemReq) => {
  return apiService.post('/notification/post/notification-item', data);
};

export const updateNotificationItem = (data: INotificationItemReq) => {
  return apiService.put('/notification/update/notification-item', data);
};

import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';
import { INotificationActiviti } from 'modules/Notification';

export interface IParamCloseNotification {
  id: string;
  referDate: string;
  referDateTime: string;
  notificationActivities?: INotificationActiviti[]
}

export interface IParamCloseNotificationDocument {
  notificationId: string;
  documentId: string;
}

export const getNotificationDetail = (id: string) => {
  return apiService.get(`/notification/get/notification?notificationId=${id}`);
};

export const uploadDocument = ({ id, formData }: { id: string; formData: FormData }) => {
  return apiService.post(`/notification-document/uploads?notificationId=${id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
};

export const deleteNotificationItem = (data: any) => {
  return apiService.delete('/notification/delete/notification-item', { data });
};

export const closeNotification = (data: IParamCloseNotification) => {
  return apiService.post('/notification/post/notification-close', { ...data });
};

export const closeNotificationDocument = (params: IParamCloseNotificationDocument) => {
  const paramStr = convertObjectToQueryParams({ ...params });

  return apiService.delete(`/notification-document/delete?${paramStr}`);
};

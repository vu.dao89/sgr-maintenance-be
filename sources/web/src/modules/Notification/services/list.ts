import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';
import dayjs from 'dayjs';

export const getNotifications = (params?: any) => {
  const paramStr = convertObjectToQueryParams({ ...params });
  return apiService.get(`/notification/get/notification-search?${paramStr}`);
};

export const deleteNotificationById = (id: string) => {
  return apiService.post(`/notification/post/notification-close`, {
    id: id,
    referDate: dayjs().format('YYYYMMDD'),
    referDateTime: dayjs().format('YYYYMMDD')
  });
};

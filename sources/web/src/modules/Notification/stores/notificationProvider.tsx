import useLogicWorkOrderDetail from '@modules/WorkOrder/containers/detail/useLogicWorkOrderDetail';
import { COMMON_STATUS_STAT, FILE_TYPE, TEXT_SUCCESS_ACTION } from 'common/constants';
import { COMMON_STATUS_STAT_NAME } from 'common/constants/commonStatus';
import { ICodeItem, IGroupDocument } from 'common/dto';
import { findItemInArrayByKey, groupFileDocument } from 'common/helpers';
import { useApiCaller } from 'common/hooks';
import { groupBy, has } from 'lodash';
import useLogicMasterData from 'modules/MasterData/hooks/useLogicMasterData';
import {
  closeNotification,
  closeNotificationDocument,
  createNotificationItem,
  deleteNotificationItem,
  getNotificationDetail,
  INotificationDetail,
  INotificationDocument,
  INotificationItemDetail,
  INotificationItemReq,
  INotificationType,
  IParamCloseNotification,
  IParamCloseNotificationDocument,
  updateNotificationItem,
  uploadDocument
} from 'modules/Notification';
import { IWorkOrderDetail } from 'modules/WorkOrder';
import { useRouter } from 'next/router';
import { createContext, useEffect, useState } from 'react';
import { toast } from 'react-toastify';

export interface INotificationContext {
  data?: INotificationDetail;
  loading?: boolean;
  loadingUpload?: boolean;
  loadingCreateNotificationItem?: boolean;
  loadingDeleteNotificationItem?: boolean;
  loadingUpdateNotificationItem?: boolean;
  loadingCloseNotificationItem?: boolean;
  loadingGetWorkOrderDetail?: boolean;
  loadingGetParts?: boolean;
  loadingGetProblems?: boolean;
  loadingGetCauses?: boolean;
  loadingGetPartCodes?: boolean;
  loadingDames?: boolean;
  loadingGetCauseCodes?: boolean;
  loadingCloseNotificationItemDocument?: boolean;
  notificationType?: INotificationType;
  parts?: ICodeItem[];
  partCodes?: ICodeItem[];
  problems?: ICodeItem[];
  dames?: ICodeItem[];
  causes?: ICodeItem[];
  causeCodes?: ICodeItem[];
  workOrderDetail?: IWorkOrderDetail;
  handleGetParts?: any;
  handleGetPartCodes?: any;
  handleGetProblems?: any;
  handleGetDames?: any;
  handleGetCauses?: any;
  handleGetCauseCodes?: any;
  handleGetNotificationType?: any;
  handleUploadDocument?: any;
  handleSubmitCreateNotificationItem?: any;
  handleDeleteNotificationItem?: any;
  handleUpdateNotificationItem?: any;
  handleGetWorkOrderDetail?: any;
  handleCloseNotification?: any;
  handleCloseNotificationDocument?: any;
}

export const NotificationContext = createContext<INotificationContext>(null!);

export default function NotificationProvider({ children }: { children: any }) {
  const router = useRouter();
  const [data, setData] = useState<INotificationDetail>();
  const {
    notificationType,
    parts,
    partCodes,
    problems,
    dames,
    causes,
    causeCodes,
    loadingGetParts,
    loadingGetProblems,
    loadingGetCauses,
    loadingGetPartCodes,
    loadingDames,
    loadingGetCauseCodes,
    handleGetNotificationType,
    handleGetParts,
    handleGetPartCodes,
    handleGetProblems,
    handleGetDames,
    handleGetCauses,
    handleGetCauseCodes
  } = useLogicMasterData();

  const { workOrderDetail, loadingGetWorkOrderDetail, handleGetWorkOrderDetail } = useLogicWorkOrderDetail();

  const { request, loading } = useApiCaller<INotificationDetail>({
    apiCaller: getNotificationDetail,
    resDto: {} as INotificationDetail
  });

  const { request: requestUploadDocument, loading: loadingUpload } = useApiCaller<INotificationDocument[]>({
    apiCaller: uploadDocument,
    resDto: [] as INotificationDocument[]
  });

  const { request: requestCreateNotificationItem, loading: loadingCreateNotificationItem } = useApiCaller<any>({
    apiCaller: createNotificationItem
  });

  const { request: requestUpdateNotificationItem, loading: loadingUpdateNotificationItem } = useApiCaller<any>({
    apiCaller: updateNotificationItem
  });

  const { request: requestDeleteNotificationItem, loading: loadingDeleteNotificationItem } = useApiCaller<any>({
    apiCaller: deleteNotificationItem
  });

  const { request: requestCloseNotification, loading: loadingCloseNotificationItem } = useApiCaller<any>({
    apiCaller: closeNotification
  });

  const { request: requestCloseNotificationDocument, loading: loadingCloseNotificationItemDocument } =
    useApiCaller<any>({
      apiCaller: closeNotificationDocument
    });

  const handleConvertDataNotification = (params: INotificationItemReq) => {
    const newParams = {} as INotificationItemReq;
    const dataNotification = {} as INotificationItemDetail;
    newParams.id = data?.header?.id;

    if (params?.part) {
      newParams.part = params?.part;
      dataNotification.part = params?.part;
      if (parts?.length)
        dataNotification.partDesc = findItemInArrayByKey('codeGroup', params?.part, parts).codeGroupDesc;
    }
    if (params?.partCode) {
      newParams.partCode = params?.partCode;
      dataNotification.partCode = params?.partCode;
      if (partCodes?.length)
        dataNotification.partCodeDesc = findItemInArrayByKey('code', params?.partCode, partCodes).codeDesc;
    }
    if (params?.problem) {
      newParams.problem = params?.problem;
      dataNotification.problem = params?.problem;
      if (problems?.length)
        dataNotification.problemDesc = findItemInArrayByKey('codeGroup', params?.problem, problems).codeGroupDesc;
    }
    if (params?.dame) {
      newParams.dame = params?.dame;
      dataNotification.dame = params?.dame;
      if (dames?.length) dataNotification.dameDesc = findItemInArrayByKey('code', params?.dame, dames).codeDesc;
    }
    if (params?.cause) {
      newParams.cause = params?.cause;
      dataNotification.cause = params?.cause;
      if (causes?.length)
        dataNotification.causeDesc = findItemInArrayByKey('codeGroup', params?.cause, causes).codeGroupDesc;
    }
    if (params?.causeCode) {
      newParams.causeCode = params?.causeCode;
      dataNotification.causeCode = params?.causeCode;
      if (causeCodes?.length)
        dataNotification.causeCodeDesc = findItemInArrayByKey('code', params?.causeCode, causeCodes).codeDesc;
    }
    if (params?.idText && params?.problem) {
      newParams.idText = params?.idText;
      dataNotification.idText = params?.idText;
    }
    if (params?.causeText && params?.cause) {
      newParams.causeText = params?.causeText;
      dataNotification.causeText = params?.causeText;
    }

    return { newParams, dataNotification };
  };

  const handleGetNotification = async (id: string) => {
    const response = await request(id);
    const { statusCode, data } = response;

    if (statusCode) {
      const newData = { ...data } as INotificationDetail;
      const documentGroups = {
        files: [],
        images: []
      } as any;
      const group = data?.notificationDocuments?.length
        ? groupBy(data?.notificationDocuments, 'globalDocument.fileType')
        : (null as any);

      if (has(group, FILE_TYPE.APPLICATION_PDF))
        documentGroups.files = [...documentGroups.files, ...group[FILE_TYPE.APPLICATION_PDF]];
      if (has(group, FILE_TYPE.APPLICATION_XML))
        documentGroups.files = [...documentGroups.files, ...group[FILE_TYPE.APPLICATION_XML]];
      if (has(group, FILE_TYPE.IMAGE_JPG))
        documentGroups.images = [...documentGroups.images, ...group[FILE_TYPE.IMAGE_JPG]];
      if (has(group, FILE_TYPE.IMAGE_PNG))
        documentGroups.images = [...documentGroups.images, ...group[FILE_TYPE.IMAGE_PNG]];

      newData.groupDocuments = documentGroups;

      setData(newData);
    }
  };

  const handleUploadDocument = async (files: any) => {
    if (!files.length) return;
    if (!data) return;

    const formData = new FormData() as FormData;

    files.forEach((file: any) => {
      formData.append('files', file);
    });

    const response = await requestUploadDocument({ id: data?.header?.id, formData });

    if (response.statusCode === 200) {
      const groupDocuments = groupFileDocument(response.data) as IGroupDocument<INotificationDocument>;
      groupDocuments.files = [...(data?.groupDocuments?.files || []), ...(groupDocuments.files || [])];
      groupDocuments.images = [...(data?.groupDocuments?.images || []), ...(groupDocuments.images || [])];

      setData({ ...data, groupDocuments });
    }
  };

  const handleSubmitCreateNotificationItem = async (params: INotificationItemReq) => {
    const { newParams, dataNotification } = handleConvertDataNotification(params);

    const response = await requestCreateNotificationItem(newParams);
    if (response.statusCode === 200) {
      dataNotification.item = response.data?.notificationItemId;
      const newItems = [...(data?.items?.notificationItems || []), dataNotification];
      const newData = {
        ...data,
        items: {
          notificationItems: newItems
        }
      } as INotificationDetail;

      toast.success(response.message || TEXT_SUCCESS_ACTION);
      setData(newData);
    }
  };

  const handleUpdateNotificationItem = async (params: INotificationItemReq) => {
    const { newParams, dataNotification } = handleConvertDataNotification(params);
    newParams.item = dataNotification.item = params.item || '';

    const response = await requestUpdateNotificationItem(newParams);

    if (response.statusCode === 200) {
      const newItems = data?.items?.notificationItems.map((item: any) => {
        return item.item === dataNotification.item ? dataNotification : item;
      });

      const newData = {
        ...data,
        items: {
          notificationItems: newItems
        }
      } as INotificationDetail;

      toast.success(response.message || TEXT_SUCCESS_ACTION);
      setData(newData);
    }
  };

  const handleDeleteNotificationItem = async (id: string) => {
    if (!data) return;

    const params = {
      notificationId: data?.header?.id,
      notificationItemId: id
    };

    const response = await requestDeleteNotificationItem(params);

    if (response.statusCode === 200) {
      const newData = { ...data };
      const arrayNotifications = newData.items.notificationItems;
      newData.items.notificationItems = arrayNotifications.filter(item => {
        return item.item != id;
      });

      toast.success(response.message || 'Xóa thành công');

      setData(newData);
    }
  };

  const handleCloseNotification = async (params: IParamCloseNotification) => {
    const response = await requestCloseNotification(params);

    if (response.statusCode === 200) {
      toast.success(response.message || TEXT_SUCCESS_ACTION);

      const newData = { ...data } as INotificationDetail;
      newData.header.commonStatus.status = COMMON_STATUS_STAT.COMPLETE;
      newData.header.commonStatus.description = COMMON_STATUS_STAT_NAME[COMMON_STATUS_STAT.COMPLETE];
    }
  };

  const handleCloseNotificationDocument = async (params: IParamCloseNotificationDocument) => {
    const response = await requestCloseNotificationDocument(params);

    if (response.statusCode === 200) {
      toast.success(response.message || TEXT_SUCCESS_ACTION);
      const groupDocuments = { ...data?.groupDocuments } as IGroupDocument<INotificationDocument>;
      groupDocuments.files = groupDocuments?.files?.filter(
        (item: INotificationDocument) => item.globalDocument.id !== params.documentId
      );
      groupDocuments.images = groupDocuments?.images?.filter(
        (item: INotificationDocument) => item.globalDocument.id !== params.documentId
      );

      const newData = { ...data, groupDocuments } as INotificationDetail;

      setData(newData);
    }
  };

  useEffect(() => {
    const id = router.query.id as String;
    if (!id) router.push('/404');

    handleGetNotification(String(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!data) return;

    handleGetNotificationType(data?.header?.type);

    if (data?.header?.order) handleGetWorkOrderDetail(data?.header?.order);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <NotificationContext.Provider
      value={{
        data,
        loading,
        loadingCreateNotificationItem,
        loadingDeleteNotificationItem,
        loadingUpdateNotificationItem,
        loadingGetWorkOrderDetail,
        loadingCloseNotificationItem,
        loadingGetParts,
        loadingGetProblems,
        loadingGetCauses,
        loadingGetPartCodes,
        loadingDames,
        loadingGetCauseCodes,
        loadingCloseNotificationItemDocument,
        notificationType,
        parts,
        partCodes,
        problems,
        dames,
        causes,
        causeCodes,
        workOrderDetail,
        loadingUpload,
        handleGetNotificationType,
        handleGetParts,
        handleGetPartCodes,
        handleGetProblems,
        handleGetDames,
        handleGetCauses,
        handleUploadDocument,
        handleGetCauseCodes,
        handleSubmitCreateNotificationItem,
        handleDeleteNotificationItem,
        handleUpdateNotificationItem,
        handleCloseNotification,
        handleCloseNotificationDocument
      }}
    >
      {children}
    </NotificationContext.Provider>
  );
}

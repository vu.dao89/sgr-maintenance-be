export interface ICreateHeaderNotificationReq {
  id: string;
  type: string;
  desc: string;
  lText: string;
  priorityType: string;
  priority: string;
  createOn: string;
  createAt: string;
  equipmentId: string;
  equipmentDesc: string;
  functionalLocationId: string;
  date: string;
  time: string;
  reportBy: string;
  reportByName: string;
  requestStart: string;
  requestStartTime: string;
  requestEnd: string;
  requestEndTime: string;
  malfunctionStart: string;
  malfunctionStartTime: string;
  malfunctionEnd: string;
  malfunctionEndTime: string;
  breakDown: boolean;
  breakDownDuration: string;
  breakDownDurationUnit: string;
  order: string;
  completedDate: string;
  completedTime: string;
  categoryProfile: string;
  plannerGroup: string;
  sortField: string;
  workCenter: string;
  workCenterDesc: string;
  maintenancePlant: string;
  equipmentStatId: string;
  equipmentStatIdDesc: string;
  maintenancePlantDesc: string;
  assignTo: string;
  assignName: string;
  typeDesc: string;
  functionalLocationDesc: string;
}

export interface ICreateItemNotificationReq {
  notificationItems: INotificationDetailItem[];
}

export interface ICreateActivityNotificationReq {
  notificationActivities: any[];
}

export interface ICreateNotificationReq {
  header: ICreateHeaderNotificationReq;
  items: ICreateItemNotificationReq;
  activities: ICreateActivityNotificationReq;
  documentIds: any[];
}

export interface INotificationDetailItem {
  item: string;
  part: string;
  partDesc: string;
  partCode: string;
  partCodeDesc: string;
  problem: string;
  problemDesc: string;
  dame: string;
  dameDesc: string;
  cause: string;
  causeDesc: string;
  causeCode: string;
  causeCodeDesc: string;
}

export interface INotificationItemReq {
  id?: string;
  item?: string;
  part?: string;
  partDesc?: string;
  partCode?: string;
  partCodeDesc?: string;
  problem?: string;
  problemDesc?: string;
  dame?: string;
  dameDesc?: string;
  cause?: string;
  causeDesc?: string;
  causeCode?: string;
  causeCodeDesc?: string;
  idText?: string;
  causeText?: string;
}

export interface INotificationAttachment {
  id?: string;
  name?: string;
  path: string;
}

export interface INotificationGeneralForm {
  type: string;
  desc: string;
  maintenancePlant: string;
  workCenter: string;
  functionalLocationId: string;
  equipmentId: string;
  priority: string;
  assignTo: string;
  date: string;
  requestStart: string;
  requestEnd: string;
  breakDown: boolean;
  malfunctionStart: string;
  malfunctionEnd: string;
}

export interface INotificationItemForm {
  id?: string;
  part?: string;
  partCode?: string;
  problem?: string;
  dame?: string;
  idText?: string;
  cause?: string;
  causeCode?: string;
  causeText?: string;
}

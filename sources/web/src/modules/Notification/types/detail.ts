import { IDocumentBase, IGroupDocument } from 'common/dto';
import { ICommonStatus } from 'common/dto';

export interface INotificationItem extends IBaseEntity {
  notificationType: string;
  notificationTypeDesc: string;
  description: string;
  priority: string;
  createdOn: string;
  createdAt: string;
  equipmentId: string;
  equipmentDescription: string;
  functionalLocationId: string;
  functionalLocationDesc: string;
  notificationDate: string;
  notificationTime: string;
  notificationReportedBy: string;
  reportedByName: string;
  requestStart: string;
  requestStartTime: string;
  requestEnd: string;
  requestEndTime: string;
  maifStart: string;
  maifStartTime: string;
  maifEnd: string;
  maifEndTime: string;
  completeDate: string;
  completeTime: string;
  plannerGroup: string;
  plannerGroupDesc: string;
  workCenter: string;
  maintenancePlant: string;
  equipmentStatId: string;
  equipmentStatDesc: string;
  assignTo: string;
  assignName: string;
  causeText: string;
  priorityType: string;
  commonStatus: ICommonStatus;
  notificationDocuments: string;
}

export interface INotificationDetail {
  code: string;
  image: string;
  header: INotificationHeader;
  items: {
    notificationItems: INotificationItemDetail[];
  };
  activities: {
    notificationActivities: INotificationActiviti[];
  };
  notificationDocuments: INotificationDocument[];
  groupDocuments?: IGroupDocument<INotificationDocument>;
}

export interface INotificationHeader {
  id: string;
  type: string;
  desc: string;
  priorityType: string;
  priority: string;
  createOn: string;
  createAt: string;
  equipmentId: string;
  equipmentDesc: string;
  functionalLocationId: string;
  date: string;
  time: string;
  reportBy: string;
  reportByName: string;
  requestStart: string;
  requestStartTime: string;
  requestEnd: string;
  requestEndTime: string;
  malfunctionStart: string;
  malfunctionStartTime: string;
  malfunctionEnd: string;
  malfunctionEndTime: string;
  breakDown: string;
  breakDownDuration: string;
  breakDownDurationUnit: string;
  order: string;
  completedDate: string;
  completedTime: string;
  categoryProfile: string;
  plannerGroup: string;
  sortField: string;
  workCenter: string;
  workCenterDesc: string;
  maintenancePlant: string;
  equipmentStatId: string;
  equipmentStatIdDesc: string;
  maintenancePlantDesc: string;
  assignTo: string;
  assignName: string;
  typeDesc: string;
  functionalLocationDesc: string;
  priorityTypeObject: string;
  commonStatus: ICommonStatus;
  ltext: string;
}

export interface INotificationItemDetail {
  item: string;
  part: string;
  partDesc: string;
  partCode: string;
  partCodeDesc: string;
  problem: string;
  problemDesc: string;
  dame: string;
  dameDesc: string;
  cause: string;
  causeDesc: string;
  causeCode: string;
  causeCodeDesc: string;
  idText: string;
  causeText: string;
  id: string;
}

export interface INotificationType {
  id: string;
  notiType: string;
  description: string;
  catalogProfile: string;
  problems: string;
  causes: string;
  activities: string;
  objectParts: string;
  workOrderTypeId: string;
  priorityTypeId: string;
  personResponsible: string;
}

export interface INotificationActiviti {
  activityId: string;
  activity: string;
  activityDesc: string;
  activityCode: string;
  activityCodeDesc: string;
  activityText: string;
  id: string;
}

export type TNotificationTab = {
  preview?: any;
  detail?: any;
  action?: any;
  document?: any;
};

export interface INotificationDocument extends IDocumentBase {
  notificationCode: string;
}
import { INotificationItem } from './detail';

export interface INotificationStoreState {
  entities: INotificationItem[];
  loading: TLoading;
  error: any;
}

export interface INotificationState extends INotificationStoreState {
  currentRequestId: TCurrentRequest;
}

export interface INotificationResDto {
  statusCode: string;
  items: INotificationItem[];
  pagination: IPagination;
}

export type INotificationColumn = {
  id: string | number;
  key: string;
  name: string;
  checked: boolean;
  required: boolean;
  field: string;
};

export interface INotificationFilterReqDto extends IPaginationReq {
  description?: string;
  createFrom?: string;
  createTo?: string;
  dateFrom?: string;
  dateTo?: string;
  requestStartDateFrom?: string;
  requestStartDateTo?: string;
  requestEndDateFrom?: string;
  requestEndDateTo?: string;
  reportBy?: string;
  assignTo?: string;
  qrCode?: string;
  plants?: string;
  priorities?: string;
  types?: string;
  plannerGroups?: string;
  status?: string;
  equipments?: string;
  functionalLocations?: string;
  employeeId?: string;
  id?: string;
}



export interface IFormFilterNotification {
  description?: string;
  createFrom?: any;
  createTo?: any;
  reportBy?: string;
  assignTo?: string;
  plants?: string;
  priorities?: string;
  types?: string;
  plannerGroups?: string;
  status?: string;
}

export const USER_API = {
  GET_USER_INFO: '/users/user-info'
};

export const USER_ACTION = {
  GET_USER_INFO: '/users/getUserInfo'
};

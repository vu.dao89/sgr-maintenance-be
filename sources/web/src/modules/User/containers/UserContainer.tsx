import { Container, Grid, TextField, Typography } from '@mui/material';
import { Fragment } from 'react';
import { useSelectUserStore } from '../redux';

export default function UserContainer() {
  const { user } = useSelectUserStore();

  if (!user) return <Fragment />;
  return (
    <Container maxWidth="md" className="ml-0 mt-10">
      <Grid container spacing={6} alignItems="center">
        <Grid item xs={12} md={3}>
          <Typography>Tên</Typography>
        </Grid>
        <Grid item xs={12} md={8}>
          <TextField size="small" fullWidth variant="outlined" disabled defaultValue={user.name} />
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography>Email</Typography>
        </Grid>
        <Grid item xs={12} md={8}>
          <TextField size="small" fullWidth variant="outlined" disabled defaultValue={user.email} />
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography>Phòng Ban</Typography>
        </Grid>
        <Grid item xs={12} md={8}>
          <TextField
            size="small"
            fullWidth
            variant="outlined"
            disabled
            defaultValue={user.departments[0]?.name || ''}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

import { getUser, IUser, USER_ACTION } from '@modules/User';
import { ActionReducerMapBuilder, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { ELoading } from 'common/constants';
import { handleGetMaintenanPlanCode } from 'common/helpers';
import { set } from 'lodash';

export const getUserInfo = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  USER_ACTION.GET_USER_INFO,
  async (_args: undefined, _thunkAPI) => {
    // const { appService } = get(thunkAPI, 'extra') as APIMapping;

    const response = await getUser();
    response.data.maintenancePlantCodes = handleGetMaintenanPlanCode(response.data);

    return response.data;
  }
);

export const getUserInfoBuilder = (builder: ActionReducerMapBuilder<any>) => {
  builder.addCase(getUserInfo.pending, (state: IUser) => {
    set(state, 'user', null);
    set(state, 'loading', ELoading.PENDING);
    set(state, 'error', null);
  });
  builder.addCase(getUserInfo.fulfilled, (state: IUser, action: PayloadAction<IUser>) => {
    const { payload } = action;
    set(state, 'user', payload);
    set(state, 'loading', ELoading.IDLE);
  });
  builder.addCase(getUserInfo.rejected, (state: IUser, action: any) => {
    const { error } = action;
    set(state, 'user', null);
    set(state, 'loading', ELoading.IDLE);
    set(state, 'error', error.message);
  });
};

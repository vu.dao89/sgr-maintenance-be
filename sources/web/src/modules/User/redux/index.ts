import { createSlice } from '@reduxjs/toolkit';
import { getUserInfo, getUserInfoBuilder } from './api';
import reducers, { defaultState } from './reducers';

const { actions, reducer } = createSlice({
  name: 'users',
  initialState: { ...defaultState },
  reducers,
  extraReducers: builder => {
    getUserInfoBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getUserInfo
};

export * from './selectors';
export { extraActions as userActions, reducer as userReducer };

import { IUserState } from '@modules/User';
import { ELoading } from 'common/constants';
import { set } from 'lodash';

export const defaultState: IUserState = {
  user: null,
  loading: ELoading.IDLE,
  currentRequestId: undefined,
  error: null
};

const reducers = {
  logout: (state: any) => {
    set(state, 'user', null);
  }
};

export default reducers;

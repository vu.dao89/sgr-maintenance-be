import { IUserState, IUserStore } from '@modules/User';
import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'common/app';
import { useSelector } from 'react-redux';

/**
 * Get Info user
 */
const userSelector = createSelector(
  (state: RootState) => state.users,
  (data: IUserState) => {
    const { user, loading } = data || undefined;
    return { user, loading };
  }
);

export const useSelectUserStore = (): IUserStore => {
  return useSelector<RootState, IUserStore>(userSelector);
};

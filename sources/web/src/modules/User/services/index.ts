import { apiService } from 'common/app';
import { USER_API } from '../constants';

export const getUser = () => {
  return apiService.get(USER_API.GET_USER_INFO);
};

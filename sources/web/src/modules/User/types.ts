export interface IUserState {
  user: IUser | null;
  loading: TLoading;
  currentRequestId: TCurrentRequest;
  error: any;
}

export type IUserStore = Pick<IUserState, 'user' | 'loading'>;
export interface IUser {
  employeeId?: number | string;
  employeeCode: string;
  token: string;
  name: string;
  picture: string;
  email: string;
  departmentCode: string;
  departmentRoles: IDepartmentRole[];
  departments: IDepartment[];
  maintenancePlantCodes: string[];
}
export interface IDepartment {
  code: string;
  name: string;
  completeName: string;
}
export interface IDepartmentRole {
  code: string;
  name: string;
  roles: IRole[];
}
export interface IRole {
  name: string;
  permissions: IPermission[];
}

export interface IPermission {
  appCode: string;
  code: string;
  name: string;
}

import { IFormFilterEquipment } from '@modules/Equipment/types';
import { WORK_ORDER_FILTER_FIELD } from '@modules/WorkOrder/constants';
import { IWorkOrderFilterReqDto, IWorkOrderFormFilter } from '@modules/WorkOrder/types';
import { FormGroup, FormLabel, Grid } from '@mui/material';
import { FilterInput, RHFSelect } from 'common/components';
import { PLACEHOLDER_SELECT_INPUT } from 'common/constants';
import { compact, isEmpty } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';

interface IFilterProps {
  filter: IWorkOrderFilterReqDto | undefined;
  filterCount?: number;
  workCenters: IOption[];
  priorities: IOption[];
  equipments: IOption[];
  workOrderTypes: IOption[];
  mainPersonnel: IOption[];
  status: IOption[];
  mainPlants: IOption[];
  functionLocations: IOption[];
  onFilter: (value: any) => any;
}

const defaultValues: IWorkOrderFormFilter = {
  workCenterIds: '',
  functionalLocationIds: '',
  maintenancePlantIds: '',
  statusIds: '',
  priorityIds: '',
  equipmentIds: '',
  workOrderTypeIds: '',
  mainPersonIds: ''
};

export const WorkOrderFilter = ({
  filter,
  filterCount,
  workCenters,
  priorities,
  equipments,
  workOrderTypes,
  mainPersonnel,
  status,
  mainPlants,
  functionLocations,
  onFilter
}: IFilterProps) => {
  const { control, handleSubmit, watch, reset } = useForm<IWorkOrderFormFilter>({
    defaultValues
  });

  const formValues = watch([
    WORK_ORDER_FILTER_FIELD.MAIN_PLANT,
    WORK_ORDER_FILTER_FIELD.WORK_CENTER,
    WORK_ORDER_FILTER_FIELD.WO_TYPE,
    WORK_ORDER_FILTER_FIELD.EQUI_ID,
    WORK_ORDER_FILTER_FIELD.FUNC_LOC_ID,
    WORK_ORDER_FILTER_FIELD.STATUS,
    WORK_ORDER_FILTER_FIELD.PRORITY,
    WORK_ORDER_FILTER_FIELD.MAIN_PERSON
  ]);
  const isDisabled = useMemo(() => isEmpty(compact(formValues)), [formValues]);

  const onSubmit = handleSubmit((formData: IFormFilterEquipment) => {
    onFilter(formData);
  });

  const onReset = handleSubmit(() => {
    reset(defaultValues);
    onFilter({ filterText: filter?.filterText });
  });

  useEffect(() => {
    reset({
      workCenterIds: filter?.workCenterIds || '',
      maintenancePlantIds: filter?.maintenancePlantIds || '',
      functionalLocationIds: filter?.functionalLocationIds || '',
      statusIds: filter?.statusIds || '',
      priorityIds: filter?.priorityIds || '',
      equipmentIds: filter?.equipmentIds || '',
      workOrderTypeIds: filter?.workOrderTypeIds || '',
      mainPersonIds: filter?.mainPersonIds || ''
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <FilterInput
      title="Lọc thiết bị"
      filterCount={filterCount}
      isDisabled={isDisabled}
      onSubmit={onSubmit}
      onReset={onReset}
    >
      <Grid container spacing={2} columnSpacing={6}>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo nơi bảo trì</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.MAIN_PLANT}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={mainPlants ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo tổ đội</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.WORK_CENTER}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={workCenters ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo loại lệnh bảo trì</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.WO_TYPE}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={workOrderTypes ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo thiết bị</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.EQUI_ID}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={equipments ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo khu vực chức năng</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.FUNC_LOC_ID}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={functionLocations ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo trạng thái</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.STATUS}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={status ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo mức độ ưu tiên</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.PRORITY}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={priorities ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup>
            <FormLabel>Theo người chịu trách nhiệm</FormLabel>
            <RHFSelect
              name={WORK_ORDER_FILTER_FIELD.MAIN_PERSON}
              placeholder={PLACEHOLDER_SELECT_INPUT}
              displayEmpty
              isPlaceholderOption
              options={mainPersonnel ?? []}
              control={control}
            />
          </FormGroup>
        </Grid>
      </Grid>
    </FilterInput>
  );
};

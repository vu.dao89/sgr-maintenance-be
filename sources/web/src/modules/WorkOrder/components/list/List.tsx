import { IWorkOrderDto } from '@modules/WorkOrder/types';
import { Box, Button, FormControl, Paper, Stack, TableContainer, Typography } from '@mui/material';
import { Pagination, SelectDisplayColumn } from 'common/components';
import { DataTable, IColumnType } from 'common/components/DataTable';
import { Icon } from 'common/components/Icon';
import { PLACEHOLDER_DISPLAY_COLUMN } from 'common/constants';

interface IWorkOrderListProps {
  items: IWorkOrderDto[];
  pagination?: IPagination;
  handleChangePaging?: any;
  columns: IColumnType<IWorkOrderDto>[];
  setColumns: any;
}

export const WorkOrderList = ({ items, pagination, handleChangePaging, columns, setColumns }: IWorkOrderListProps) => {
  const onGoToPage = (e: any) => {
    if (e.keyCode == 13) {
      handleChangePaging(e, e.target.value);
    }
  };

  return (
    <Stack p={4} sx={{ borderRadius: 3, backgroundColor: '#FFF' }}>
      <TableContainer component={Paper} elevation={0}>
        <Box display="flex" justifyContent="space-between" mb={5}>
          <Typography variant="h5">Danh sách lệnh bảo trì</Typography>
          <Box display="flex" justifyContent="space-between">
            <FormControl sx={{ width: '180px' }}>
              <SelectDisplayColumn
                id="display-column"
                options={columns}
                placeholder={PLACEHOLDER_DISPLAY_COLUMN}
                onSelect={setColumns}
              />
            </FormControl>
            <Button
              disableRipple
              sx={{ backgroundColor: 'primary', fontSize: 16, textTransform: 'none', ml: 4 }}
              type="button"
              variant="contained"
            >
              <Icon name="add" className="mr-[6px]" width={16} height={16} />
              Tạo lệnh bảo trì
            </Button>
          </Box>
        </Box>
        <DataTable
          data={items}
          columnId={'workOrderId'}
          columns={columns}
          hasActionColumn={true}
          canViewDetail={true}
          canDelete={true}
          handleDelete={(value: any) => {
            console.log(value);
          }}
          url={'/work-order'}
          noDataText="Không tìm thấy lệnh bảo trì"
        />
      </TableContainer>
      <Pagination
        page={pagination?.page ?? 0}
        limit={pagination?.size ?? 0}
        totalPages={pagination?.totalPages ?? 0}
        totalItems={pagination?.totalItems ?? 0}
        onGoToPage={onGoToPage}
        onChange={handleChangePaging}
      ></Pagination>
    </Stack>
  );
};

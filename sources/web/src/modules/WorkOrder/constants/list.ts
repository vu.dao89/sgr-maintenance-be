export enum WORK_ORDER_FILTER_FIELD {
  MAIN_PLANT = 'maintenancePlantIds',
  WORK_CENTER = 'workCenterIds',
  WO_TYPE = 'workOrderTypeIds',
  EQUI_ID = 'equipmentIds',
  FUNC_LOC_ID = 'functionalLocationIds',
  STATUS = 'statusIds',
  PRORITY = 'priorityIds',
  MAIN_PERSON = 'mainPersonIds',
  FILTER_TEXT = 'filterText'
}

export const WORK_ORDER_FILTER_FIELD_LABEL: { [key: string]: string } = {
  [WORK_ORDER_FILTER_FIELD.MAIN_PLANT]: 'Nơi bảo trì',
  [WORK_ORDER_FILTER_FIELD.WORK_CENTER]: 'Tổ đội',
  [WORK_ORDER_FILTER_FIELD.WO_TYPE]: 'Loại lệnh bảo trì',
  [WORK_ORDER_FILTER_FIELD.EQUI_ID]: 'Thiết bị',
  [WORK_ORDER_FILTER_FIELD.FUNC_LOC_ID]: 'Khu vực chức năng',
  [WORK_ORDER_FILTER_FIELD.STATUS]: 'Trạng thái',
  [WORK_ORDER_FILTER_FIELD.PRORITY]: 'Mức độ ưu tiên',
  [WORK_ORDER_FILTER_FIELD.MAIN_PERSON]: 'Người chịu trách nhiệm',
  [WORK_ORDER_FILTER_FIELD.FILTER_TEXT]: 'Keyword'
};

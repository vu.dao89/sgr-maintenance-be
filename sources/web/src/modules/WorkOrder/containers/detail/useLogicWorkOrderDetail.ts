import { useApiCaller } from 'common/hooks';
import { getWorkOrderDetail, IWorkOrderDetail } from 'modules/WorkOrder';
import { useState } from 'react';

const useLogicWorkOrderDetail = () => {
  const [workOrderDetail, setWorkOrderDetail] = useState<IWorkOrderDetail>();

  const { request: requestGetWorkOrderDetail, loading: loadingGetWorkOrderDetail } = useApiCaller<IWorkOrderDetail>({
    apiCaller: getWorkOrderDetail,
    resDto: {} as IWorkOrderDetail
  });

  const handleGetWorkOrderDetail = async (id: string) => {
    const response = await requestGetWorkOrderDetail(id);

    if (response.statusCode) {
      setWorkOrderDetail(response.data);
    }
  };

  return {
    loadingGetWorkOrderDetail,
    workOrderDetail,
    handleGetWorkOrderDetail
  };
};

export default useLogicWorkOrderDetail;

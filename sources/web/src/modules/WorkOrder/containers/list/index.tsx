import { WorkOrderFilter, WorkOrderList } from '@modules/WorkOrder/components';
import { WORK_ORDER_FILTER_FIELD_LABEL } from '@modules/WorkOrder/constants';
import { Box, Stack } from '@mui/material';
import { FilterList, Loading, SearchInput } from 'common/components';
import { size } from 'lodash';
import useWorkOrderListLogic from './useWorkOrderListLogic';

const WorkOrderListContainer = () => {
  const {
    items,
    filter,
    filterCount,
    pagination,
    loading,
    columns,
    workCenters,
    priorities,
    equipments,
    workOrderTypes,
    mainPersonnel,
    status,
    mainPlants,
    functionLocations,
    selectedFilter,
    handleChangePaging,
    removeFilter,
    setColumns,
    handleFilter
  } = useWorkOrderListLogic();

  const onSearch = (filterText: string) => {
    handleFilter({ ...filter, filterText });
  };

  return (
    <Stack>
      {/* <Stack>
        <Box display="flex" mb={5}>
          <WorkOrderSummary />
        </Box>
      </Stack> */}
      <Stack>
        <Box display="flex" mb={5}>
          <SearchInput name="equipment" onSearch={onSearch} />
          <WorkOrderFilter
            filter={filter}
            filterCount={filterCount}
            workCenters={workCenters}
            priorities={priorities}
            equipments={equipments}
            workOrderTypes={workOrderTypes}
            mainPersonnel={mainPersonnel}
            status={status}
            mainPlants={mainPlants}
            functionLocations={functionLocations}
            onFilter={handleFilter}
          />
        </Box>
        {size(selectedFilter) > 0 && (
          <FilterList data={selectedFilter} label={WORK_ORDER_FILTER_FIELD_LABEL} onRemove={removeFilter} />
        )}
      </Stack>

      {loading ? (
        <Loading />
      ) : (
        <WorkOrderList
          items={items}
          pagination={pagination}
          handleChangePaging={handleChangePaging}
          columns={columns}
          setColumns={setColumns}
        />
      )}
    </Stack>
  );
};

export default WorkOrderListContainer;

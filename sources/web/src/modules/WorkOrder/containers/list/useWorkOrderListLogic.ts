import { getEquipments } from '@modules/Equipment';
import {
  getFunctionLocations,
  getMainPlants,
  getPersonnel,
  getPriorities,
  getStatus,
  getWorkCenters,
  getWorkOrderTypes
} from '@modules/MasterData';
import { getWorkOrders } from '@modules/WorkOrder/services';
import {
  IWorkOrderDto,
  IWorkOrderFilterReqDto,
  IWorkOrderFormFilter,
  IWorkOrderResDto
} from '@modules/WorkOrder/types';
import { IColumnType } from 'common/components/DataTable';
import { DEFAULT_PER_PAGE } from 'common/constants';
import { toDateString } from 'common/helpers';
import { useApiCaller } from 'common/hooks';
import { cloneDeep, isEmpty, isUndefined } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const initColumns: IColumnType<IWorkOrderDto>[] = [
  {
    key: 'workOrderId',
    title: 'Mã lệnh',
    width: 200,
    enable: true,
    required: true,
    render: (_, { workOrderId }) => `#${workOrderId}`
  },
  {
    key: 'workOrderDescription',
    title: 'Tên lệnh bảo trì',
    width: 400,
    enable: true,
    required: true,
    type: (_, { workOrderDescription, thumbnailUrl }) =>
      workOrderDescription ? { type: 'image', value: thumbnailUrl } : { type: 'label' }
  },
  {
    key: 'priorityType',
    title: 'Mức độ ưu tiên',
    width: 100,
    enable: true,
    type: (_, { priorityType }) =>
      priorityType ? { type: 'label', color: priorityType?.colorCode } : { type: 'text' },
    render: (_, { priorityType }) => (priorityType ? priorityType?.description : '')
  },
  {
    key: 'personnelName',
    title: 'Giám sát',
    width: 300,
    enable: true
  },
  {
    key: 'equipmentDescription',
    title: 'Thiết bị ',
    width: 400,
    enable: true
  },
  {
    key: 'functionalLocationDescription',
    title: 'Khu vực ',
    width: 400,
    enable: true
  },
  {
    key: 'workOrderTypeDescription',
    title: 'Phân loại bảo trì ',
    width: 300,
    enable: true
  },
  {
    key: 'commonStatus',
    title: 'Trạng thái',
    width: 200,
    enable: true,
    type: (_, { commonStatus }) =>
      commonStatus ? { type: 'label', color: commonStatus?.colorCode } : { type: 'text' },
    render: (_, { commonStatus }) => (commonStatus ? commonStatus?.description : '')
  },
  {
    key: 'woStartDate',
    title: 'Thời gian ',
    width: 600,
    enable: true,
    render: (_, { woStartDate, woFinishDate }) => {
      if (!isEmpty(woFinishDate)) {
        return `${toDateString(woStartDate)} - ${toDateString(woFinishDate)}`;
      }

      return `${toDateString(woStartDate)}`;
    }
  }
];

const initialFilter: IWorkOrderFilterReqDto = {
  page: 1,
  size: DEFAULT_PER_PAGE
};

const initPagination = {
  page: 0,
  size: DEFAULT_PER_PAGE,
  total: 0
};

const useWorkOrderListLogic = () => {
  const router = useRouter();
  const [items, setItems] = useState<IWorkOrderDto[]>([]);
  const [filter, setFilter] = useState<IWorkOrderFilterReqDto>(initialFilter);
  const [filterCount, setFilterCount] = useState<number>(0);
  const [pagination, setPagination] = useState<IPagination>(initPagination);
  const [columns, setColumns] = useState<IColumnType<IWorkOrderDto>[]>(initColumns);
  const [workCenters, setWorkCenters] = useState<any[]>([]);
  const [priorities, setPriorities] = useState<any[]>([]);
  const [equipments, setEquipments] = useState<any[]>([]);
  const [workOrderTypes, setWorkOrderTypes] = useState<any[]>([]);
  const [mainPersonnel, setMainPersonnel] = useState<any[]>([]);
  const [status, setStatus] = useState<any[]>([]);
  const [mainPlants, setMainPlants] = useState<any[]>([]);
  const [functionLocations, setFunctionLocations] = useState<any[]>([]);
  const [selectedFilter, setSelectedFilter] = useState<IOption[]>([]);

  const { request, loading } = useApiCaller<IWorkOrderResDto>({
    apiCaller: getWorkOrders,
    resDto: {} as IWorkOrderResDto
  });

  const { request: requestWorkCenter } = useApiCaller<any[]>({
    apiCaller: getWorkCenters
  });

  const { request: requestFunctionLocation } = useApiCaller<any[]>({
    apiCaller: getFunctionLocations
  });

  const { request: requestMainPlants } = useApiCaller<any[]>({
    apiCaller: getMainPlants
  });

  const { request: requestStatus } = useApiCaller<any[]>({
    apiCaller: getStatus
  });

  const { request: requestPriorities } = useApiCaller<any[]>({
    apiCaller: getPriorities
  });

  const { request: requestEquiments } = useApiCaller<any[]>({
    apiCaller: getEquipments
  });

  const { request: requestWorkOrderTypes } = useApiCaller<any[]>({
    apiCaller: getWorkOrderTypes
  });

  const { request: requestMainPersonnel } = useApiCaller<any[]>({
    apiCaller: getPersonnel
  });

  const handleGetData = async (params: IWorkOrderFilterReqDto) => {
    const result: any = await request(params);

    if (result) {
      setItems(result?.data?.items);
      setPagination(result?.data?.pagination);
    }
  };

  const handleGetWorkCenter = async () => {
    const result: any = await requestWorkCenter();

    if (result) {
      setWorkCenters(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.code })));
    }
  };

  const handleGetFunctionLocation = async () => {
    const result: any = await requestFunctionLocation();

    if (result) {
      setFunctionLocations(
        result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.functionalLocationId }))
      );
    }
  };

  const handleGetMainPlants = async () => {
    const result: any = await requestMainPlants();

    if (result) {
      setMainPlants(result?.data?.items?.map((item: any) => ({ label: item?.plantName, value: item?.code })));
    }
  };

  const handleGetStatus = async () => {
    const result: any = await requestStatus();

    if (result) {
      setStatus(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.status })));
    }
  };

  const handleGetPriorities = async () => {
    const result: any = await requestPriorities();

    if (result) {
      setPriorities(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.priority })));
    }
  };

  const handleGetEquipments = async () => {
    const result: any = await requestEquiments();

    if (result) {
      setEquipments(result?.data?.items?.map((item: any) => ({ label: item?.description, value: item?.equipmentId })));
    }
  };

  const handleGetWorkOrderTypes = async () => {
    const result: any = await requestWorkOrderTypes();

    if (result) {
      setWorkOrderTypes(result?.data?.items?.map((item: any) => ({ label: item?.name, value: item?.type })));
    }
  };

  const handleGetMainPersonnel = async () => {
    const result: any = await requestMainPersonnel();

    if (result) {
      setMainPersonnel(result?.data?.items?.map((item: any) => ({ label: item?.name, value: item?.code })));
    }
  };

  useEffect(() => {
    if (!router.isReady) {
      return;
    }
    const data = router?.query as any;

    data.page = Number(data.page) || initialFilter.page;
    data.size = Number(data.size) || initialFilter.size;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  useEffect(() => {
    const selected: any = {};

    if (filter?.workCenterIds) {
      selected['workCenterIds'] = workCenters.filter(
        (item: { value: any }) => item.value === filter?.workCenterIds?.toString()
      )[0];
    }

    if (filter?.maintenancePlantIds) {
      selected['maintenancePlantIds'] = mainPlants.filter(
        (item: { value: any }) => item.value === filter?.maintenancePlantIds?.toString()
      )[0];
    }

    if (filter?.functionalLocationIds) {
      selected['functionalLocationIds'] = functionLocations.filter(
        (item: { value: any }) => item.value === filter?.functionalLocationIds?.toString()
      )[0];
    }

    if (filter?.statusIds) {
      selected['statusIds'] = status.filter((item: { value: any }) => item.value === filter?.statusIds?.toString())[0];
    }

    if (filter?.priorityIds) {
      selected['priorityIds'] = priorities.filter(
        (item: { value: any }) => item.value === filter?.priorityIds?.toString()
      )[0];
    }

    if (filter?.equipmentIds) {
      selected['equipmentIds'] = equipments.filter(
        (item: { value: any }) => item.value === filter?.equipmentIds?.toString()
      )[0];
    }

    if (filter?.workOrderTypeIds) {
      selected['workOrderTypeIds'] = workOrderTypes.filter(
        (item: { value: any }) => item.value === filter?.workOrderTypeIds?.toString()
      )[0];
    }

    if (filter?.mainPersonIds) {
      selected['mainPersonIds'] = mainPersonnel.filter(
        (item: { value: any }) => item.value === filter?.mainPersonIds?.toString()
      )[0];
    }

    setSelectedFilter(selected);

    setFilterCount(Object.keys(selected).length);

    handleGetData(filter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  useEffect(() => {
    handleGetWorkCenter();
    handleGetFunctionLocation();
    handleGetMainPlants();
    handleGetStatus();
    handleGetPriorities();
    handleGetEquipments();
    handleGetWorkOrderTypes();
    handleGetMainPersonnel();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChangePaging = (_event: any, page: number) => {
    setFilter({ ...filter, page });
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  };

  const removeFilter = (
    filterKey:
      | 'workCenterIds'
      | 'functionalLocationIds'
      | 'maintenancePlantIds'
      | 'statusIds'
      | 'priorityIds'
      | 'equipmentIds'
      | 'workOrderTypeIds'
      | 'mainPersonIds'
  ) => {
    const newFilter = cloneDeep(filter);
    if (!isUndefined(newFilter)) {
      delete newFilter[filterKey];
      setFilter({ ...newFilter });
    }
  };

  const handleFilter = (value: IWorkOrderFormFilter) => {
    const dataFilter = {} as IWorkOrderFilterReqDto;

    if (value.filterText && !isUndefined(value.filterText)) {
      dataFilter['filterText'] = value.filterText;
    }

    if (value.workCenterIds && !isUndefined(value.workCenterIds)) {
      dataFilter['workCenterIds'] = value.workCenterIds;
    }

    if (value.functionalLocationIds && !isUndefined(value.functionalLocationIds)) {
      dataFilter['functionalLocationIds'] = value.functionalLocationIds;
    }

    if (value.maintenancePlantIds && !isUndefined(value.maintenancePlantIds)) {
      dataFilter['maintenancePlantIds'] = value.maintenancePlantIds;
    }

    if (value.statusIds && !isUndefined(value.statusIds)) {
      dataFilter['statusIds'] = value.statusIds;
    }

    if (value.priorityIds && !isUndefined(value.priorityIds)) {
      dataFilter['priorityIds'] = value.priorityIds;
    }

    if (value.equipmentIds && !isUndefined(value.equipmentIds)) {
      dataFilter['equipmentIds'] = value.equipmentIds;
    }

    if (value.workOrderTypeIds && !isUndefined(value.workOrderTypeIds)) {
      dataFilter['workOrderTypeIds'] = value.workOrderTypeIds;
    }

    if (value.mainPersonIds && !isUndefined(value.mainPersonIds)) {
      dataFilter['mainPersonIds'] = value.mainPersonIds;
    }

    setFilter({ ...initialFilter, ...dataFilter });
  };

  return {
    loading,
    pagination,
    items: items ?? [],
    filter,
    filterCount,
    columns,
    selectedFilter,
    workCenters,
    priorities,
    equipments,
    workOrderTypes,
    mainPersonnel,
    status,
    mainPlants,
    functionLocations,
    handleGetMainPlants,
    handleChangePaging,
    handleGetData,
    removeFilter,
    handleFilter,
    setColumns
  };
};

export default useWorkOrderListLogic;

import { apiService } from 'common/app';

export const getWorkOrderDetail = (id: string) => {
  return apiService.get(`/api/work-order?workOrderId=${id}`);
};
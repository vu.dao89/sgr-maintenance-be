import { apiService } from 'common/app';
import { convertObjectToQueryParams } from 'common/helpers';

export const getWorkOrders = (params: any) => {
  let paramStr = convertObjectToQueryParams({
    ...params
  });

  if (!params.maintenancePlantIds) {
    const mainPlantDefaultFilter = `maintenancePlantIds=M003,M046`;
    paramStr = !!paramStr ? `${paramStr}&${mainPlantDefaultFilter}` : mainPlantDefaultFilter;
  }

  return apiService.get(`/api/work-order/search?${paramStr}`);
};

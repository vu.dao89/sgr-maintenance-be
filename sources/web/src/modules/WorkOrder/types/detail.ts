import { ICommonStatus, IPriorityType } from 'common/dto';

export interface IWorkOrderDetail {
  workOrder: IWorkOrder;
  operation: {
    item: IWorkOrderItem[];
  };
}

export interface IWorkOrder {
  workOrderId: string;
  workOrderDescription: string;
  workOrderLongDescription: string;
  workOrderType: string;
  workOrderTypeDescription: string;
  mainActKey: string;
  mainActivityKeyDescription: string;
  priority: string;
  priorityType: IPriorityType;
  priorityText: string;
  equipmentId: string;
  equipmentDescription: string;
  functionalLocationId: string;
  functionalLocationDescription: string;
  status: any;
  workOrderSystemStatus: string;
  workOrderSystemStatusDescription: string;
  mainPerson: string;
  mainPlant: string;
  maintenancePlantDescription: string;
  workCenter: string;
  workCenterDescription: string;
  plannerGroup: string;
  plannerGroupDescription: string;
  systemCondition: string;
  systemConditionText: string;
  woStartDate: string;
  woFinishDate: string;
  sortField: any;
  notificationId: string;
  personnelName: string;
  thumbnailUrl: string;
  originalUrl: string;
  latitude: string;
  longitude: string;
  commonStatus: ICommonStatus;
}

export interface IWorkOrderItem {
  workOrderId: string;
  operationId: string;
  superOperationId: string;
  operationDescription: string;
  operationLongText: string;
  equipmentId: string;
  equipmentDescription: string;
  functionalLocationId: string;
  functionalLocationDescription: string;
  controlKey: string;
  personnel: string;
  personnelName: string;
  operationSystemStatus: string;
  operationSystemStatusDescription: string;
  estimate: string;
  unit: string;
  reservation: string;
  reservationWithdraw: string;
  prtRequire: string;
  prtConfirm: string;
  subOperationRequire: string;
  subOperationConfirm: string;
  result: string;
  actualStart: string;
  actualStarTime: string;
  actualFinish: string;
  actualFinishTime: string;
  documents: [];
  operationHistories: [];
  varianceReason: any;
}

import { ICommonStatus, IPriorityType } from 'common/dto';
import { IWorkOrderItem } from './detail';

export interface IWorkOrderResDto {
  statusCode: string;
  items: IWorkOrderItem[];
  pagination: IPagination;
}

export interface IWorkOrderFilterReqDto extends IPaginationReq {
  filterText?: string;
  workCenterIds?: string;
  functionalLocationIds?: string;
  maintenancePlantIds?: string;
  statusIds?: string;
  priorityIds?: string;
  equipmentIds?: string;
  workOrderTypeIds?: string;
  mainPersonIds?: string;
}

export interface IWorkOrderFormFilter {
  filterText?: string;
  workCenterIds?: string;
  functionalLocationIds?: string;
  maintenancePlantIds?: string;
  statusIds?: string;
  priorityIds?: string;
  equipmentIds?: string;
  workOrderTypeIds?: string;
  mainPersonIds?: string;
}

export interface IWorkOrderDto extends IBaseEntity {
  workOrderId: string;
  workOrderDescription: string;
  workOrderTypeDescription: string;
  mainActivityKeyDescription: string;
  priorityType: IPriorityType;
  equipmentDescription: string;
  functionalLocationDescription: string;
  status: any;
  workOrderSystemStatusDescription: string;
  mainPerson: string;
  mainPlant: string;
  maintenancePlantDescription: string;
  workCenterDescription: string;
  plannerGroupDescription: string;
  systemConditionText: string;
  woStartDate: string;
  woFinishDate: string;
  personnelName: string;
  thumbnailUrl: string;
  originalUrl: string;
  commonStatus: ICommonStatus;
}

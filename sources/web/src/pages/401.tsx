import { Box, Button, Typography } from '@mui/material';
import { BlankLayout } from 'common/components/Layout/BlankLayout';
import Image from 'next/image';
import Link from 'next/link';

export default function Error401Page() {
  return (
    <BlankLayout className="flex justify-center items-center bg-white h-screen">
      <Box className="flex flex-col items-center" mb={30}>
        <Image src="/iss/assetmanagement/static/images/404.png" alt="404" width={300} height={170} />
        <Typography variant="h5" align="center" my={6}>
          XIN LỖI! BẠN KHÔNG CÓ QUYỀN TRUY CẬP
        </Typography>
        <Button color="primary" component={Link} href={`${process.env.NEXT_PUBLIC_PORTAL_DOMAIN}`} variant="contained">
          Login with Azure
        </Button>
      </Box>
    </BlankLayout>
  );
}

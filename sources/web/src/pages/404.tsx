import { Box, Button, Typography } from '@mui/material';
import { BlankLayout } from 'common/components/Layout/BlankLayout';
import Image from 'next/image';
import Link from 'next/link';

export default function Error404Page() {
  return (
    <BlankLayout className="flex justify-center items-center bg-white h-screen">
      <Box className="flex flex-col items-center" mb={30}>
        <Image src="/iss/assetmanagement/static/images/404.png" alt="404" width={300} height={170} />
        <Typography variant="h5" align="center" my={6}>
          XIN LỖI NHƯNG TRANG BẠN ĐANG TÌM KIẾM KHÔNG TỒN TẠI
        </Typography>
        <Button color="primary" component={Link} href="/" variant="contained">
          Quay lại
        </Button>
      </Box>
    </BlankLayout>
  );
}

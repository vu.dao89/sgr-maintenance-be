import { Box, Button, Typography } from '@mui/material';
import { BlankLayout } from 'common/components/Layout/BlankLayout';
import Link from 'next/link';

export default function Error500Page() {
  return (
    <BlankLayout className="flex justify-center items-center bg-white h-screen">
      <Box className="flex flex-col items-center" mb={30}>
        <Typography variant="h1" component="h1" align="center" my={6}>
          500 Internal Server Error.
        </Typography>
        <Button color="primary" component={Link} href="/" variant="contained">
          Quay lại
        </Button>
      </Box>
    </BlankLayout>
  );
}

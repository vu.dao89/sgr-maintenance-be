import 'nprogress/nprogress.css';
import 'reflect-metadata';
import 'simplebar/dist/simplebar.min.css';

import { CacheProvider, EmotionCache } from '@emotion/react';
import { AppContainer } from '@modules/App';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { createEmotionCache, store } from 'common/app';
import { useScrollTop } from 'common/hooks';
import 'common/styles/main.css';
import { themes } from 'common/themes';
import { NextPage } from 'next';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { ReactElement, ReactNode } from 'react';
import { Provider as ReduxProvider } from 'react-redux';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode;
};

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
  Component: NextPageWithLayout;
}

export default function MyApp(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  useScrollTop();

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <ReduxProvider store={store}>
          <ThemeProvider theme={themes}>
            <CssBaseline />
            <AppContainer>
              <Component {...pageProps} />
            </AppContainer>
          </ThemeProvider>
        </ReduxProvider>
      </LocalizationProvider>
    </CacheProvider>
  );
}

import { IDataEquipmentTab } from 'modules/Equipment';
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { IEquipmentInfo } from 'modules/Equipment';
import type { NextApiRequest, NextApiResponse } from 'next';
import { equipmentInfo, equipmentTab } from '_mocks_';

type Data = {
  statusCode: number;
  success: true;
  data: {
    item: IEquipmentInfo;
    tab: IDataEquipmentTab[];
  };
};
export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { method } = req;

  switch (method) {
    case 'GET':
      res.json({ statusCode: 200, success: true, data: { item: equipmentInfo, tab: equipmentTab } });
      break;
    default:
      res.setHeader('Allow', ['GET', 'POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}

import { INotificationItem } from 'modules/Notification';
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { notificationDetail } from '_mocks_';

type Data = {
  statusCode: number;
  success: true;
  data: INotificationItem;
};
export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { method } = req;

  switch (method) {
    case 'GET':
      res.json({ statusCode: 200, success: true, data: notificationDetail });
      break;
    default:
      res.setHeader('Allow', ['GET', 'POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}

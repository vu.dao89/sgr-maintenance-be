// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { INotificationItem } from 'modules/Notification';
import type { NextApiRequest, NextApiResponse } from 'next';
import { NOTIFICATION_MOCK_DATA } from '_mocks_';

type Data = {
  method: string;
  endpoint: {
    list: INotificationItem[];
    pagination: {
      total: number;
      size: number;
      page: number;
    };
  };
};

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { method, query } = req;
  const page = query.page && typeof query.page == 'string' ? parseInt(query.page) - 1 : 1;
  const limit = query.size && typeof query.size == 'string' ? parseInt(query.size) : 10;

  switch (method) {
    case 'GET':
      res.status(200).json({
        method: 'POST',
        endpoint: {
          list: NOTIFICATION_MOCK_DATA.slice(page * limit, page * limit + limit),
          pagination: {
            total: NOTIFICATION_MOCK_DATA.length,
            page: query.page && typeof query.page == 'string' ? parseInt(query.page) : 1,
            size: 10
          }
        }
      });
      break;

    default:
      res.setHeader('Allow', ['GET', 'POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}

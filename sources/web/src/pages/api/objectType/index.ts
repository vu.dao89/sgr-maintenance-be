// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { OBJECT_TYPE } from '_mocks_';

type Data = {
  method: string;
  endpoint: {
    list: IOption[];
  };
};

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { method } = req;

  switch (method) {
    case 'GET':
      res.status(200).json({
        method: 'POST',
        endpoint: {
          list: OBJECT_TYPE
        }
      });
      break;

    default:
      res.setHeader('Allow', ['GET', 'POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
      break;
  }
}

import { Card, CardContent, Grid } from '@mui/material'
import { CardHeading, FormDataValue } from 'common/components'

const MOCK_DATA = [
  { label: 'Đặc tính', value: 'Nội dung' },
  { label: 'Đặc tính', value: 'Nội dung' },
  { label: 'Đặc tính', value: 'Nội dung' },
  { label: 'Đặc tính', value: 'Nội dung' }
]

export default function CardComponent() {
  return (
    <Card variant="outlined">
      <CardContent>
        <CardHeading>Điểm đo: Khe hở con lăn</CardHeading>
        <Grid container>
          {MOCK_DATA.map(e => (
            <Grid key={e.label} item xs={6}>
              <FormDataValue {...e} />
            </Grid>
          ))}
        </Grid>
      </CardContent>
    </Card>
  )
}

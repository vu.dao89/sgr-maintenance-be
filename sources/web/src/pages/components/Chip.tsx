import { Chip, Paper, Stack } from '@mui/material'

export default function ChipComponent() {
  return (
    <Stack direction="row" component={Paper} p={10} gap={2}>
      <Chip label="primary" color="primary" />
      <Chip label="info" color="info" />
      <Chip label="success" color="success" />
      <Chip label="error" color="error" />
      <Chip label="warning" color="warning" />
      <Chip label="default" />
    </Stack>
  )
}

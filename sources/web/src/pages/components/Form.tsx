import { Button, FormGroup, FormLabel, Grid, Paper, Stack } from '@mui/material';
import { RHFSelect, RHFTextarea, RHFTextInput } from 'common/components';
import { useForm } from 'react-hook-form';

export default function FormComponent() {
  const { control, handleSubmit } = useForm<any>({
    defaultValues: {}
    // resolver: yupResolver(schemaValidateSignIn)
  });

  const onSubmit = () => {
    //TODO
  };

  return (
    <Stack component={Paper} p={10} gap={2}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
        <Grid container columnSpacing={6}>
          <Grid item xs={6}>
            <FormGroup>
              <FormLabel>Theo tổ đội</FormLabel>
              <RHFTextInput id="email" name="email" variant="outlined" fullWidth control={control} />
            </FormGroup>
          </Grid>
          <Grid item xs={6}>
            <FormGroup>
              <FormLabel>Theo tổ đội</FormLabel>
              <RHFSelect
                id="email"
                name="email"
                variant="outlined"
                fullWidth
                options={[{ label: 'Test', value: 'test' }]}
                control={control}
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12}>
            <FormGroup>
              <FormLabel>Theo tổ đội</FormLabel>
              <RHFTextarea id="email" name="email" variant="outlined" fullWidth control={control} />
            </FormGroup>
          </Grid>
        </Grid>
        <FormGroup className="flex-row gap-4 justify-end">
          <Button disableRipple size="large" color="info" type="submit" variant="contained">
            Cancel
          </Button>
          <Button disableRipple size="large" color="primary" type="submit" variant="contained">
            Bắt đầu
          </Button>
          <Button disableRipple size="large" color="success" type="button" variant="contained">
            Cancel
          </Button>
          <Button disableRipple size="large" color="warning" type="submit" variant="contained">
            Bắt đầu
          </Button>
          <Button disableRipple size="large" color="error" type="submit" variant="contained">
            Bắt đầu
          </Button>
          <Button disableRipple size="large" color="inherit" type="submit" variant="contained">
            Bắt đầu
          </Button>
        </FormGroup>
      </form>
    </Stack>
  );
}

import { Button, Typography } from '@mui/material';
import { Modal } from 'common/components';
import { useState } from 'react';

export default function ModalComponent() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <Button color="primary" variant="contained" onClick={() => setIsOpen(true)}>
        Open modal
      </Button>
      <Modal open={isOpen} title="Modal Title" onClose={() => setIsOpen(false)}>
        <Typography>Điểm đo: Khe hở con lăn</Typography>
      </Modal>
    </>
  );
}

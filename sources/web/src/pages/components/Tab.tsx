import { Tab, Tabs } from '@mui/material';
import { useState } from 'react';

export default function TabComponent() {
  const [value, setValue] = useState(1);

  const handleChange = (_event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Tabs value={value} onChange={handleChange} aria-label="tabs">
      <Tab disableRipple label="Tổng quan" className="capitalize" />
      <Tab disableRipple label="Điểm đo" className="capitalize" />
      <Tab disableRipple label="Đặc tính" className="capitalize" />
      <Tab disableRipple label="Phân cấp" className="capitalize" />
      <Tab disableRipple label="Tài liệu" className="capitalize" />
    </Tabs>
  );
}

import { Paper, Typography } from '@mui/material'
import { Stack } from '@mui/system'

export default function TypographyComponent() {
  return (
    <Stack component={Paper} p={10}>
      <Typography component="h1" variant="h1">
        Heading 1
      </Typography>
      <Typography component="h2" variant="h2">
        Heading 2
      </Typography>
      <Typography component="h3" variant="h3">
        Heading 3
      </Typography>
      <Typography component="h4" variant="h4">
        Heading 4
      </Typography>
      <Typography component="h5" variant="h5">
        Heading 5
      </Typography>
      <Typography variant="body1">Body 1 (16px)</Typography>
      <Typography variant="body2">Body 2 (14px</Typography>
      <Typography variant="subtitle1">Subtitle 1 (16px)</Typography>
      <Typography variant="subtitle2">Subtitle 2 (14px)</Typography>
      <Typography variant="overline">Overline 2 (14px)</Typography>
      <Typography variant="button">Button</Typography>
    </Stack>
  )
}

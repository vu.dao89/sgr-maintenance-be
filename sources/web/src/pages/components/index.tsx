import { Grid, Typography } from '@mui/material';
import { MainLayout } from 'common/components';
import CardComponent from './Card';
import ChipComponent from './Chip';
import FormComponent from './Form';
import ModalComponent from './Modal';
import TabComponent from './Tab';
import TableComponent from './Table';
import TypographyComponent from './Typography';

export default function ComponentPage() {
  return (
    <MainLayout seoData={{ title: 'Component Page' }} mb={20}>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          <Typography component="h1" variant="h1">
            Components
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Typography</Typography>
          <TypographyComponent />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Chip or Label</Typography>
          <ChipComponent />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Table</Typography>
          <TableComponent />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Tab</Typography>
          <TabComponent />
        </Grid>
        <Grid item xs={6}>
          <Typography variant="h3">Card</Typography>
          <CardComponent />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Form</Typography>
          <FormComponent />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h3">Modal</Typography>
          <ModalComponent />
        </Grid>
      </Grid>
    </MainLayout>
  );
}

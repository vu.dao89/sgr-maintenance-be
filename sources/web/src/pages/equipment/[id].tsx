import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';
import EquipmentDetailComponent from 'modules/Equipment/containers/detail';

const EquipmentDetailPage = () => {
  return (
    <MainLayout seoData={{ title: 'Thiết bị vật tư' }}>
      <EquipmentDetailComponent />
    </MainLayout>
  );
};

const IndexAuthenticated = withAuthentication(EquipmentDetailPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

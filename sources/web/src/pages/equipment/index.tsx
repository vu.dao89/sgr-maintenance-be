import { EquipmentListContainer } from '@modules/Equipment';
import { MainLayout } from 'common/components/Layout';
import { withAuthentication } from 'common/hocs';

const EquipmentListPage = () => {
  return (
    <MainLayout seoData={{ title: 'Thiết bị' }}>
      <EquipmentListContainer />
    </MainLayout>
  );
};
const IndexAuthenticated = withAuthentication(EquipmentListPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

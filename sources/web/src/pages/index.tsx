import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';
import Dashboard from 'modules/Dashboard/containers/index';

const DashboardPage = () => {
  return (
    <MainLayout seoData={{ title: 'Dashboard' }}>
      <Dashboard />
    </MainLayout>
  );
};

const IndexAuthenticated = withAuthentication(DashboardPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

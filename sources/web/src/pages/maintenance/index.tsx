import { Card, CardContent, Typography } from '@mui/material';
import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';

const MaintenanceListPage = () => {
  return (
    <MainLayout seoData={{ title: 'Lệnh bảo trì' }}>
      <Card>
        <CardContent>
          <Typography>Coming soon...</Typography>
        </CardContent>
      </Card>
    </MainLayout>
  );
};

const IndexAuthenticated = withAuthentication(MaintenanceListPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

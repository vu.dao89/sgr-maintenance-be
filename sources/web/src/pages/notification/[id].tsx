import { NotificationDetailContainer } from '@modules/Notification';
import NotificationProvider from 'modules/Notification/stores/notificationProvider';
import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';

const NotificationDetailPage = () => {
  return (
    <MainLayout seoData={{ title: 'Thông tin chi tiết' }}>
      <NotificationProvider>
        <NotificationDetailContainer />
      </NotificationProvider>
    </MainLayout>
  );
};
const IndexAuthenticated = withAuthentication(NotificationDetailPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

import { NotificationListContainer } from '@modules/Notification';
import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';

const NotificationListPage = () => {
  return (
    <MainLayout seoData={{ title: 'Thông báo' }}>
      <NotificationListContainer />
    </MainLayout>
  );
};
const IndexAuthenticated = withAuthentication(NotificationListPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

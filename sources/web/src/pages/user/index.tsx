import { UserContainer } from '@modules/User';
import { MainLayout } from 'common/components';

export default function UserInfoPage() {
  return (
    <MainLayout seoData={{ title: 'Thông tin tài khoản' }}>
      <UserContainer />
    </MainLayout>
  );
}

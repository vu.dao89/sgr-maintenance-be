import { WorkOrderListContainer } from '@modules/WorkOrder/containers';
import { MainLayout } from 'common/components';
import { withAuthentication } from 'common/hocs';

const WorkOrderListPage = () => {
  return (
    <MainLayout seoData={{ title: 'Lệnh bảo trì' }} mb={20}>
      <WorkOrderListContainer />
    </MainLayout>
  );
};

const IndexAuthenticated = withAuthentication(WorkOrderListPage);
IndexAuthenticated.layout = 'private';

export default IndexAuthenticated;

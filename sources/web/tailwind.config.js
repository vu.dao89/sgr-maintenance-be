/** @type {import('tailwindcss').Config} */
const tailwind = require('./src/common/themes/tailwind');

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/common/components/**/*.{js,ts,jsx,tsx}',
    './src/modules/**/*.{js,ts,jsx,tsx}',
    './src/modules/**/*.{js,ts,jsx,tsx}'
  ],
  important: true,
  theme: tailwind.theme,
  variants: tailwind.variants,
  plugins: [require('@tailwindcss/line-clamp')]
};
